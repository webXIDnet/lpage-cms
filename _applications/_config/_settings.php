<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Налаштування системи
 *
***/

_define_homepage_vars('lp', 'home',
	array(
		'partner',
		'utm_source',
		'utm_source',
		'utm_medium',
		'utm_campaign'
	)
); //визначаємо головну сторінку, не враховуючи вказані елементи масиву $_GET

return array(

    'multilang' => true,

	'set_db' => require_once __DIR__ . '/db.php',

	'set_page_types' => array(	// Визначення допустимих типів сторінок
		'homepage' => '',
		'api'  => '',
		'page' => '',
		'post' => '',
		'admin' => '',
		'lp' => '',
		'payment' => '',
		'redirect' => '',
		'tag' => '',
		'ajax' => '',
		'profile' => '',//
		'category' => '',// Сторінка Категорії
	),

	'set_include_files' => array(	// Підключає додаткові файли з дерикторії аплікацій
		#APP_FOLDER
		'applications' => array('admin-panel/_admin-panel.php'),
		#APP_FOLDER.PAGE_FOLDER.'/'
		'page_files' => array(),
		#__DIR__.'/'
		'links' => array(
			APP_FOLDER.'../vendor/autoload.php'
		)
	),

	'set_error404' => HOMEPAGE, //Посилання на сторінку помилки 404

	'set_classes' => array( // підключення файлів класів на відповідних сторінках
		'language' => array(), // підключений для всіх типів сторінок
		'sql-query' => array(),
		'html-templates' => array('admin'),
		'content-tpl' => array(),
        'phpmailer/PHPMailerAutoload' => array(),
	),


	'set_libs' => array(
		// підключення файлів бібліотек
	),

	'set_modules' =>array(	// підключення файлів модулів
        'logs' => array(),
        'menus' => array(),
		'meta-datas' => array(),
		'blog' => array('admin', 'page', 'post', 'category','tag'),
		'users' => array(),
		'redirect-system' => array(
			'admin','redirect'
		),
        'categories' => array(),
		'products' => array(),
		'crm' => array(),
        'languages' => array(),
		'lpage' => array(
			'admin','lp','homepage','ajax','post', 'page', 'payment'
		),

		'online-payments' => array(
			'admin','page','lp', 'payment','ajax','api'
		),
		'profile' => array(
			'profile','lp','ajax','admin','post', 'page','api'
		),
		'ajax_module' => array()
	),

	'SQLQuery' => array(
		// налаштування класу
	),

	'lang' => 'ru',
);