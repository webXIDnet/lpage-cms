<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу

require_once __DIR__ . '/shortcode.php';
/**
 *
 * Компонента АдмінПанелі "Блог"
 *
 ***/

_add_action('defined_modules', 'Blog_AP_defined_modules');

function Blog_AP_defined_modules()
{

    if (PAGE_TYPE == 'admin') {
        global $sql;

        $result = $sql->db_table_installer('Blog', '12.08.2015',
            array(
                '_categories' => array(
                    'id' => "`id` INT(11) NOT NULL",
                    'c_id' => "`c_id` INT(11) NOT NULL",
                    'c_parent_id' => "`c_parent_id` INT(11) NULL DEFAULT '1'",
                    'c_title' => "`c_title` VARCHAR(200) NOT NULL",
                    'c_desc' => "`c_desc` TEXT NOT NULL",
                    'c_url' => "`c_url` VARCHAR(200) NOT NULL",
                    'c_post_of_day_title' => "`c_post_of_day_title` TEXT NOT NULL",
                    'c_option' => "`c_option` TEXT NOT NULL",
                    'c_type' => "`c_type` VARCHAR(20) NOT NULL",
                    'c_lang' => "`c_lang` TINYINT(4) NOT NULL DEFAULT '0'",
                    'CREATE' => "`id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`id`)"
                ),
                '_posts' => array(
                    'id' => "`id` INT(11) NOT NULL AUTO_INCREMENT",
                    'p_id' => "`p_id` INT(11) NOT NULL",
                    'p_title' => "`p_title` VARCHAR(200) NULL DEFAULT NULL",
                    'p_article' => "`p_article` VARCHAR(200) NULL DEFAULT NULL",
                    'p_text' => "`p_text` TEXT NOT NULL",
                    'p_preview' => "`p_preview` TEXT NOT NULL",
                    'p_imgs' => "`p_imgs` TEXT NOT NULL",
                    'p_cat_id' => "`p_cat_id` INT(11) NULL DEFAULT NULL",
                    'p_parent_id' => "`p_parent_id` VARCHAR(512) NOT NULL",
                    'p_pablic_date' => "`p_pablic_date` INT(11) NULL DEFAULT NULL",
                    'p_lang' => "`p_lang` TINYINT(4) NOT NULL DEFAULT '1'",
                    'CREATE' => "`id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`id`),
                    `p_article` VARCHAR(200) NULL DEFAULT NULL, UNIQUE INDEX `p_article` (`p_article`)"
                )
            )
        );
    }
}

if (PAGE_TYPE == 'admin') {

    _add_filter('adminPanel_sidebar_menu', 'Blog_admin_sidebar_menu', 20);
    _add_filter('Blog_admin_sidebar_menu', 'Blog_admin_sidebar_menu_submenu');
    require_once __DIR__.'/blog_fn.php';
}


function Blog_admin_sidebar_menu($result)
{
    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Блог'), //ім"я пункта меню
            'url' => FALSE, //посилання
            'submenu_filter' => 'Blog_admin_sidebar_menu', //хук для підключення пунктів підменю
            'active_pages' => _run_filter('Blog_admin_sidebar_menu_active_items',
                array('posts-list', 'post-add', 'post-edit', 'categories', 'pages-list', 'page-add', 'page-edit', 'category-edit',
                    'blog/comments', 'blog/comments/edit',
                    'blog/reviews', 'blog/add-review', 'blog/edit-review',
                    'blog/faq', 'blog/add-faq','blog/edit-faq'
                )
            ), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => TRUE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(3, 4, 5, 6, 100), $menu);

    return $result . @$menu;
}

function Blog_admin_sidebar_menu_submenu($result)
{
    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Все Посты'), //ім"я пункта меню
            'url' => getURL('admin', 'posts-list'), //посилання
            'active_pages' => array('posts-list', 'post-add','post-edit'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => getURL('admin', 'post-add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
        array(
            'title' => _lang('Категории'), //ім"я пункта меню
            'url' => getURL('admin', 'categories'), //посилання
            'active_pages' => array('categories', 'category-edit'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
        array(
            'title' => _lang('Все Страницы'), //ім"я пункта меню
            'url' => getURL('admin', 'pages-list'), //посилання
            'active_pages' => array('pages-list', 'page-add', 'page-edit'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => getURL('admin', 'page-add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(3, 4, 5, 6, 100), $menu);

    return $result . $menu;
}

/** Роутер Адмінки **/

_add_hook('adminPanel_controller', 'Blog_admin_controller');

function Blog_admin_controller()
{

    global $PData,
           $admin,
           $user,
           $sql,
           $sqlTPL,
           $contTPL,
           $htmlTPL,
           $meta;
    $blog_options = $meta->val('apSet_blog','mod_option');
    $blog_options['miniatures']['width'] = isset($blog_options['miniatures']['width']) ? $blog_options['miniatures']['width'] : 330;
    switch (@$_GET['admin']) {
        case'post-add':
            require_once __DIR__ . '/apps_admin/a_post_add.php';
            return TRUE;
            break;
        case'post-edit':
            require_once __DIR__ . '/apps_admin/a_post_edit.php';
            return TRUE;
            break;
        case'posts-list':
            require_once __DIR__ . '/apps_admin/a_posts_list.php';
            return TRUE;
            break;
        case'pages-list':
            require_once __DIR__ . '/apps_admin/a_pages_list.php';
            return TRUE;
            break;
        case'page-add':
            require_once __DIR__ . '/apps_admin/a_page_add.php';
            return TRUE;
            break;
        case'page-edit':
            require_once __DIR__ . '/apps_admin/a_page_edit.php';
            return TRUE;
            break;
        case'categories':
            require_once __DIR__ . '/apps_admin/a_categories.php';
            return TRUE;
            break;
        case'category-edit':
            require_once __DIR__ . '/apps_admin/a_category_edit.php';
            return TRUE;
            break;
    }
}

// if (PAGE_TYPE)
// echo PAGE_TYPE;
$PData->setTPL(PAGE_TYPE);