<?php

if (PAGE_TYPE == 'admin') {
	_add_action('adminPanel_controller', 'comments_admin_router');
	_add_filter('Blog_admin_sidebar_menu', 'com_comments_submenu', 3);
	_add_action('defined_modules', 'comments_admin_init');
} else {
	_add_action('defined_modules', 'comments_frontend_init');
}

function comments_admin_router() {
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products, $sql;
 	switch (@$_GET['admin']) {
 		case 'blog/comments': {
 			require_once __DIR__.'/admin/comments_list.php';
 			return true;
 			break;
 		}

 		case 'blog/comments/edit': {
 			require_once __DIR__.'/admin/comment_edit.php';
 			return true;
 			break;
 		}
 	}
}

function comments_admin_init()
{
	global $sql, $PData;

	$sql->db_table_installer('com_comments','08.07.2014',
		array(
			'_posts_comments' => array(
				'pc_id' => '`pc_id` int(11) NOT NULL AUTO_INCREMENT',
				'pc_p_id' => '`pc_p_id` int(11) NOT NULL',
				'pc_parent_id' => '`pc_parent_id` int(11) NOT NULL',
				'pc_username' => '`pc_username` varchar(50) NOT NULL',
				'pc_email' => '`pc_email` varchar(50) NOT NULL',
				'pc_comment' => '`pc_comment` text NOT NULL',
				'pc_date' => '`pc_date` varchar(11) NOT NULL',
				'pc_status' => '`pc_status` int(11) NOT NULL',
				'pc_ip' => '`pc_ip` varchar(20) NOT NULL',
				'CREATE' => '`pc_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`pc_id`)'
			)
		)
	);
}

function com_comments_submenu($result) {
	global $PData;
	$vars_array = array(
        array(
            'title' => _lang('Комментарии'), //ім"я пункта меню
            'url' => getURL('admin', 'blog/comments'), //посилання
            'active_pages' => array('blog/comments', 'blog/comments/edit'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        )
    );

 	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item','admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100,4,5,6), $menu);

    return $result . $menu;
}

function comments_frontend_init() {
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products, $sql;

	if (PAGE_TYPE == 'post' || PAGE_TYPE == 'page') {
		require __DIR__.'/site/comments_list.php';
	}

}