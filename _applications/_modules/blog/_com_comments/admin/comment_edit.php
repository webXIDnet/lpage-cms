<?php

// global $PData;

if (isset($_POST['editComment'])) {
	if (isset($_POST['comm'])) {
		$data = $_POST['comm'];

		$data['status'] = isset($data['status']) ? '1' : '0';

		$sql->update('_posts_comments', 'pc_id='._protect(@$_GET['id']), $data, 'pc_');
		$PData->content(_lang('Комментарий успешно сохранен!'), 'message', true);
	}
}

$comment = $sql->query("SELECT * FROM _posts_comments LEFT JOIN _posts ON (pc_p_id=p_id) WHERE pc_id="._protect(@$_GET['id']), 'value');


$PData->content('Редактировать комментарий', 'title');
$PData->content($PData->getModTPL('admin/com_comments/comment_edit','blog', $comment));