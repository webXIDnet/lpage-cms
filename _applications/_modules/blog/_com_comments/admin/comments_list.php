<?php
global $sql;

if (isset($_POST['delete_comm'])) {
	$del_arr = array();
	foreach ($_POST['del'] as $id) {
		$del_arr []= 'pc_id='.$id;
	}
	$sql->delete('_posts_comments', implode(' OR ',$del_arr), 'pc_');
	$PData->content(_lang('Выбранные комментарии успешно удалены!'), 'message', true);
}

$where = array();
$where_str = '';

//where filling


if (count($where) > 0) {
	$where_str = " WHERE ".implode(' AND ', $where);
}

$pages = _pagination('_posts_comments LEFT JOIN _posts ON (pc_p_id=p_id) ', 'pc_id', $where_str);

$comments = $sql->query("SELECT * FROM _posts_comments LEFT JOIN _posts ON (pc_p_id=p_id) ".$where_str.' '.$pages['limit']);
$data = array(
	'comments' => $comments,
	'_pagination' => $pages['html']
);

$PData->content('Комментарии', 'title');
$PData->content($PData->getModTPL('admin/com_comments/comments_list','blog', $data));