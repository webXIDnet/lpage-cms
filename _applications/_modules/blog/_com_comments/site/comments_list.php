<?php


_add_filter('post_init', 'comments_post_init');
function comments_post_init($post) {
	global $PData, $sql;
	$data = array('error');
	if (isset($_POST['add_comm']) && isset($_POST['addComment'])) {
		include_once ROOT . '/plugins/securimage/securimage.php';

		$securimage = new Securimage();
		if ($securimage->check($_POST['captcha'])) {
			$data = $_POST['addComment'];
			$data['parent_id'] = '0';
			$data['p_id'] = $post['p_id'];
			$data['date'] = date('d.m.Y H:i');
			$data['ip'] = $_SERVER['REMOTE_ADDR'];
			$data['status'] = '0';
			if (trim($data['username']) == '' || trim($data['email']) == '') {
				$data['error'] .= '
					<br><div class="col-md-12"><div class="alert alert-danger">
				        <span type="button" class="close" data-dismiss="alert">×</span>
						'._lang('Все поля обязательны!').'
					</div></div>';
			} else {
				$sql->insert('_posts_comments', $data, 'pc_');
			}
		} else {
			$data['error'] .= '
					<br><div class="col-md-12"><div class="alert alert-danger">
				        <span type="button" class="close" data-dismiss="alert">×</span>
						'._lang('Неверно введена капча!').'
					</div></div>';
		}

	}

	// _dump($post);
	$comments = $sql->query("SELECT * FROM _posts_comments LEFT JOIN _posts on (pc_p_id = p_id) WHERE p_id = ".$post['p_id']." AND pc_status=1");

	$data['comments'] = $comments;

	$PData->content($PData->getModTPL('site/com_comments/comments_list','blog', $data),'comments');
	return $post;
}

_add_filter('page_init', 'comments_page_init');
function comments_page_init($post) {
	global $PData;
	// _dump($post);
	$PData->content($PData->getModTPL('site/com_comments/comments_list','blog'),'comments');
	return $post;
}



