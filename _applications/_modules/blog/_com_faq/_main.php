<?php
/**
 * Created for temp.
 * User: BoHand
 * Date: 12.07.2015
 * Time: 12:20
 */
if (PAGE_TYPE=='admin') {
    _add_action('adminPanel_controller', 'com_faq_init_admin',10);
    _add_filter('Blog_admin_sidebar_menu', 'com_faq_submenu', 30);
    _add_action('defined_modules', 'com_faq_table_installer');
}

require_once __DIR__ . '/faq_fn.php';

function com_faq_init_admin() {
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;

    switch (@$_GET['admin']) {
        case 'blog/faq': {
            require_once __DIR__ . '/faq_list.php';
            return true;
            break;
        }

        case 'blog/add-faq': {
            require_once __DIR__ . '/faq_add.php';
            return true;
            break;
        }

        case 'blog/edit-faq': {
            require_once __DIR__ . '/faq_edit.php';
            return true;
            break;
        }


    }
}

function com_faq_table_installer()
{
    global $sql;

    $sql->db_table_installer('com_faq', '01.07.2015',
        array(
            '_faq' => array(
                'f_id'      => '`f_id` int(11) NOT NULL AUTO_INCREMENT',
                'f_name'    => '`f_name` varchar(50) NOT NULL',
                'f_question' => '`f_question` text NOT NULL',
                'f_answer' => '`f_answer` text NOT NULL',
                'f_status'   => '`f_status` tinyint(1) NOT NULL',
                'CREATE'     => "`f_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`f_id`)"
            )
        )
    );
}



function com_faq_submenu($result) {
    global $PData;
    $vars_array = array(
        array(
            'title' => _lang('FAQ'), //ім"я пункта меню
            'url' => getURL('admin', 'blog/faq'), //посилання
            'active_pages' => array('blog/faq', 'blog/edit-faq', 'blog/add-faq'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => getURL('admin','blog/add-faq') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        )
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100,4,5,6), $menu);

    return $result . $menu;
}