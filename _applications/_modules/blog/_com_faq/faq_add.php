<?php
/**
 * Created for temp.
 * User: BoHand
 * Date: 12.07.2015
 * Time: 12:20
 */
global $PData, $sql, $admin;

$PData->content(_lang('Добавить Вопрос Ответ'), 'title');

if (isset($_POST['addFaq']) && isset($_POST['add-faq-submit'])) {
    $data = $_POST['addFaq'];
    if ($data['name'] != '' && $data['question'] != '' && $data['answer'] != '') {
        $data['status'] = isset($data['status']) ? '1' : '0';
        $sql->insert('_faq', $data, 'f_');
        $PData->content(_lang('Вопрос Ответ добавлен!'), 'message', true);
    } else {
        $PData->content(_lang('Все поля должны быть заполнены!'), 'message', false);
    }
}

//Languages list
$langs = $sql->query("SELECT * FROM _languages ");

$languages = '<option value="0">' . _lang("Не отображать") . '</option>';
foreach ($langs as $key => $value) {
    $data['langs'][$value['l_id']] = $value;
    $languages .= '<option value="' . $value['l_id'] . '">' . $value['l_title'] . '</option>';
}

$html = '
<form class="list" method="POST" enctype="multipart/form-data">

    <p>
        <label>' . _lang('Имя') . ':</label>
        <input class="input" name="addFaq[name]" value=""/>
    </p>
    <p>
        <label><input type="checkbox" checked class="input" name="addFaq[status]" value="1"/> ' . _lang('Включен') . '</label>

    </p>
    <p>
        <label> ' . _lang('Язык') . '</label>
        <select name="addFaq[lang]" >
        ' . $languages . '
        </select>
    </p>
    <p>
        <label>' . _lang('Вопрос') . ':</label>
       	' . $admin->getWIZIWIG('addFaq[question]', '', 'elm1') . '
    </p>

    <p>
        <label>' . _lang('Ответ') . ':</label>
        	' . $admin->getWIZIWIG('addFaq[answer]', '', 'elm1') . '
    </p>
    <input class="btn btn-primary button" type="submit" value="' . _lang('Сохранить') . '" name="add-faq-submit"/>
</form>';

$PData->content($html);