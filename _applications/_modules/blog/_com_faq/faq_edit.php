<?php
/**
 * Created for temp.
 * User: BoHand
 * Date: 12.07.2015
 * Time: 12:20
 */
global $PData, $sql;

$PData->content(_lang('Редактировать faq'), 'title');

if (isset($_POST['editFaq']) && isset($_POST['edit-faq-submit'])) {
    $data = $_POST['editFaq'];
    if ($data['name'] != '' && $data['question'] != '' && $data['answer'] != '') {
        $data['status'] = isset($data['status']) ? '1' : '0';
        $sql->update('_faq', 'f_id=' . _protect($_GET['id']), $data, 'f_');
        $PData->content(_lang('Вопрос обновлен!'), 'message', true);
    } else {
        $PData->content(_lang('Все поля должны быть заполнены!'), 'message', false);
    }
}


$faq = $sql->query("SELECT * FROM _faq WHERE f_id=" . _protect($_GET['id']), 'value');

//Languages list
$langs = $sql->query("SELECT * FROM _languages ");

$languages = '<option value="0">' . _lang("Не отображать") . '</option>';
foreach ($langs as $key => $value) {
    $data['langs'][$value['l_id']] = $value;
    $languages .= '<option  ' . ($value['l_id'] == $faq['f_lang'] ? 'selected="selected"' : '') . ' value="' . $value['l_id'] . '">' . $value['l_title'] . '</option>';
}

$html = '
<form class="list" method="POST" enctype="multipart/form-data">

    <p>
        <label>' . _lang('Имя') . ':</label>
        <input class="input" name="editFaq[name]" value="' . @$faq['f_name'] . '"/>
    </p>
    <p>
        <label><input type="checkbox" class="input" ' . (@$faq['f_status'] == '1' ? 'checked' : '') . ' name="editFaq[status]" value="1"/> ' . _lang('Включен') . '</label>
    </p>
    <p>
    <label> ' . _lang('Язык') . '</label>
        <select name="editFaq[lang]" >
        ' . $languages . '
        </select>
        </p>
    <p>
        <label>' . _lang('Вопрос') . ':</label>
        ' . $admin->getWIZIWIG('editFaq[question]', @$faq['f_question'], 'elm1') . '
    </p>
    <p>
        <label>' . _lang('Ответ') . ':</label>
        ' . $admin->getWIZIWIG('editFaq[answer]', @$faq['f_answer'], 'elm1') . '
    </p>

    <input class="btn btn-primary button" type="submit" value="' . _lang('Сохранить') . '" name="edit-faq-submit"/>
</form>';

$PData->content($html);