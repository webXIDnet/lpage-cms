<?php
/**
 * Created for temp.
 * User: BoHand
 * Date: 12.07.2015
 * Time: 22:54
 */
function FAQ_SC()
{
    global $PData, $sql,$user;

    //Languages list
    $langs = $sql->query("SELECT * FROM _languages ");

    $languages = '<option value="0">' . _lang("Не отображать") . '</option>';
    foreach ($langs as $key => $value) {
        $langs_id[$value['l_code']] = $value['l_id'];
    }

    $vars_array['faq'] = $sql->query("SELECT * FROM _faq WHERE f_status=1 AND f_lang=" . _protect($langs_id[$user->lang]));
    //вызывать шаблон для вывода
    return $PData->getModTPL('site/com_faq/display_faq', 'blog', @$vars_array);
}