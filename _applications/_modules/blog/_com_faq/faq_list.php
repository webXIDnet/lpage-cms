<?php
/**
 * Created for temp.
 * User: BoHand
 * Date: 12.07.2015
 * Time: 12:20
 */

global $PData, $sql;

$PData->content(_lang('Список Вопрос Ответ'), 'title');
if (isset($_POST['faq-delete']) && count($_POST['del']) > 0) {
    foreach ($_POST['del'] as $del) {
        $sql->delete('_faq', 'f_id=' . $del);
    }
    $PData->content(_lang('Выбранные елементы удалены!'), 'message', true);
}

$text = '';
$where_str = ' ORDER BY f_name DESC ';
$pagination = _pagination('_faq', 'f_id', $where_str);
$faqs = $sql->query("SELECT * FROM _faq ");

//Languages list
$langs = $sql->query("SELECT * FROM _languages ");

foreach ($langs as $key => $value) {
    $data['langs'][$value['l_id']] = $value;
}

if (is_array($faqs)) {
    foreach ($faqs AS $cval) {

        if (mb_strlen($cval['f_question']) > 150) {
            $cval['f_question'] = mb_substr(strip_tags($cval['f_question']), 0, 150) . '...';
        }
        $text .= '<tr >
            <td>
                <input type="checkbox" value ="' . $cval['f_id'] . '" name="del[]"/>
            </td>
            <td>
                <a title="' . _lang('Редактировать') . '" href="' . getURL('admin', 'blog/edit-faq', 'id=' . $cval['f_id']) . '">
                ' . $cval['f_name'] . '</a>
            </td>
            <td>
                <a title="' . _lang('Редактировать') . '" href="' . getURL('admin', 'blog/edit-faq', 'id=' . $cval['f_id']) . '">
                    ' . $cval['f_question'] . '
            </td>
            <td >
                <span class="grey">' . _lang('Язык') . ':</span> ' . (($cval['f_lang'] == 0) ? _lang('Не указан') : $data['langs'][$cval['f_lang']]['l_title']) . '<br/>

            </td>

            <td >
                <input type="checkbox" disabled checked="' . $cval['f_status'] . '">
            </td>

        </tr>';

    }
    $text =
        '<form class="list" method="POST">
            <div class="table-holder">
            <table class="table">
              <thead>
                    <th class="numer">' . _lang('Выбрать') . '</th>
                    <th>' . _lang('Имя') . '</th>
                    <th>' . _lang('Вопрос') . '</th>
                    <th>' . _lang('Язык') . '</th>
                    <th>' . _lang('Включено') . '</th>
              </thead>
              <tbody>
                    ' . $text . '
              </tbody>
            </table>
            </div>
            <input class="button" type="submit" value="' . _lang('Удалить') . '" name="faq-delete">
        </form>'
        . $pagination['html'];

} else {
    $text = _lang('Не найдено! =\ ');
}

$PData->content($text);