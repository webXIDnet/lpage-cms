<?php

require_once __DIR__ . '/reviews_fn.php';


if (PAGE_TYPE=='admin') {
    _add_action('adminPanel_controller', 'com_reviews_init_admin',10);
    _add_filter('Blog_admin_sidebar_menu', 'com_reviews_submenu', 20);
    _add_action('defined_modules', 'com_reviews_table_installer');
}

function com_reviews_table_installer() {
    global $sql;

    $sql->db_table_installer('com_reviews', '08.07.2015',
        array(
            '_reviews' => array(
                'r_id' => '`r_id` int(11) NOT NULL AUTO_INCREMENT',
                'r_name' => '`r_name` varchar(50) NOT NULL',
                'r_from' => '`r_from` varchar(255) NOT NULL',
                'r_email' => '`r_email` varchar(50) NOT NULL',
                'r_message' => '`r_message` text NOT NULL',
                'r_answer' => '`r_answer` text NOT NULL',
                'r_create_date' => '`r_create_date` varchar(50) NOT NULL',
                'r_status' => '`r_status` tinyint(1) NOT NULL',
                'r_ip' => '`r_ip` varchar(50) NOT NULL',
                'r_lang' => '`r_lang` TINYINT(4) NOT NULL DEFAULT \'0\'',
                'CREATE' => "`r_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`r_id`)"
            )
        )
    );
}

function com_reviews_init_admin()
{
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;

    switch (@$_GET['admin']) {
        case 'blog/reviews': {
            require_once __DIR__ . '/reviews_list.php';
            return true;
            break;
        }

        case 'blog/add-review': {
            require_once __DIR__ . '/reviews_add.php';
            return true;
            break;
        }

        case 'blog/edit-review': {
            require_once __DIR__ . '/reviews_edit.php';
            return true;
            break;
        }


    }
}

function com_reviews_submenu($result)
{
    global $PData;
    $vars_array = array(
        array(
            'title' => _lang('Отзывы'), //ім"я пункта меню
            'url' => getURL('admin', 'blog/reviews'), //посилання
            'active_pages' => array('blog/reviews', 'blog/edit-review', 'blog/add-review'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => getURL('admin', 'blog/add-review') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        )
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100, 4, 5, 6), $menu);

    return $result . $menu;
}