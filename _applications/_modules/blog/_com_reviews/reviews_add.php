<?php

global $PData, $sql, $admin;

$PData->content(_lang('Добавить отзыв'), 'title');

if (isset($_POST['addReview']) && isset($_POST['add-review-submit'])) {
    $data = $_POST['addReview'];
    if ($data['name'] != '' && $data['create_date'] != '' && $data['message'] != '' && $data['from'] != '') {
        $data['create_date'] = date('Y-m-d H:i:s', strtotime($data['create_date']));
        $data['from'] = $data['from'];
        $data['status'] = isset($data['status']) ? '1' : '0';
        $sql->insert('_reviews', $data, 'r_');
        $PData->content(_lang('Отзыв добавлен!'), 'message', true);
    } else {
        $PData->content(_lang('Все поля должны быть заполнены!'), 'message', false);
    }
}
//Languages list
$langs = $sql->query("SELECT * FROM _languages ");

$languages = '<option value="0">' . _lang("Не отображать") . '</option>';
foreach ($langs as $key => $value) {
    $data['langs'][$value['l_id']] = $value;
    $languages .= '<option value="' . $value['l_id'] . '">' . $value['l_title'] . '</option>';
}

$html = '
<form class="list" method="POST" enctype="multipart/form-data">
    
    <p>
        <label>' . _lang('От') . ':</label>
        <input class="input" name="addReview[name]" value=""/>
    </p>
    <p>
        <label>' . _lang('Откуда') . ':</label>
        <input class="input" name="addReview[from]" value=""/>
    </p>
    <p>
        <label>' . _lang('Дата') . ':</label>
        <input style="display: block; width: 400px;" class="input datetime" name="addReview[create_date]" value=""/>
    </p>
    <p>
        <label><input type="checkbox" checked class="input" name="addReview[status]" value="1"/> ' . _lang('Включен') . '</label>
        
    </p>
    <p>
        <label> ' . _lang('Язык') . '</label>
        <select name="addReview[lang]" >
        ' . $languages . '
        </select>

    </p>
    <p>
        <label>' . _lang('Текст') . '</label>
       	' . $admin->getWIZIWIG('addReview[message]', '', 'elm1') . '
    </p>
    
    <input class="btn btn-primary button" type="submit" value="' . _lang('Сохранить') . '" name="add-review-submit"/>
</form>';

$PData->content($html);