<?php

global $PData, $sql;

$PData->content(_lang('Редактировать отзыв'), 'title');

if (isset($_POST['editReview']) && isset($_POST['edit-review-submit'])) {
    $data = $_POST['editReview'];
    if ($data['name'] != '' && $data['create_date'] != '' && $data['message'] != '') {
        $data['create_date'] = date('Y-m-d H:i:s', strtotime($data['create_date']));
        $data['status'] = isset($data['status']) ? '1' : '0';
        $sql->update('_reviews', 'r_id=' . _protect($_GET['id']), $data, 'r_');
        $PData->content(_lang('Отзыв обновлен!'), 'message', true);
    } else {
        $PData->content(_lang('Все поля должны быть заполнены!'), 'message', false);
    }
}

$review = $sql->query("SELECT * FROM _reviews WHERE r_id=" . _protect($_GET['id']), 'value');


//Languages list
$langs = $sql->query("SELECT * FROM _languages ");

$languages = '<option value="0">' . _lang("Не отображать") . '</option>';
foreach ($langs as $key => $value) {
    $data['langs'][$value['l_id']] = $value;
    $languages .= '<option  ' . ($value['l_id'] == $review['r_lang'] ? 'selected="selected"' : '') . ' value="' . $value['l_id'] . '">' . $value['l_title'] . '</option>';
}


$html = '
<form class="list" method="POST" enctype="multipart/form-data">
    
    <p>
        <label>' . _lang('От') . ':</label>
        <input class="input" name="editReview[name]" value="' . @$review['r_name'] . '"/>
    </p>
    <p>
        <label>' . _lang('Откуда') . ':</label>
        <input class="input" name="editReview[from]" value="' . @$review['r_from'] . '"/>
    </p>
    <p>
        <label>' . _lang('Дата') . ':</label>
        <input style="display: block; width: 400px;" class="input datetime" name="editReview[create_date]" value="' . date('d.m.Y H:s:i', strtotime(@$review['r_create_date'])) . '"/>
    </p>
    <p>
        <label><input type="checkbox" class="input" ' . (@$review['r_status'] == '1' ? 'checked' : '') . ' name="editReview[status]" value="1"/> ' . _lang('Включен') . '</label>
        
    </p>
    <p>
    <label> ' . _lang('Язык') . '</label>
        <select name="editReview[lang]" >
        ' . $languages . '
        </select>
        </p>
    <p>
        <label>' . _lang('Текст') . '</label>
       	' . $admin->getWIZIWIG('editReview[message]', @$review['r_message'], 'elm1') . '
    </p>
    
    <input class="btn btn-primary button" type="submit" value="' . _lang('Сохранить') . '" name="edit-review-submit"/>
</form>';

$PData->content($html);