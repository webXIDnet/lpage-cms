<?php
/**
 * Created for temp.
 * User: godex_000
 * Date: 10.07.2015
 * Time: 16:59
 */

/**
 * @param $params - list of parameters, firs cat id, second number of product to show
 */
function REVIEWS_SC()
{
    global $PData, $sql, $user;
    //Languages list
    $langs = $sql->query("SELECT * FROM _languages ");

    $languages = '<option value="0">' . _lang("Не отображать") . '</option>';
    foreach ($langs as $key => $value) {
        $langs_id[$value['l_code']] = $value['l_id'];
    }

    $vars_array['reviews'] = $sql->query("SELECT * FROM _reviews WHERE r_status=1 AND r_lang=" . $langs_id[$user->lang]);

    //вызывать шаблон для вывода
    return $PData->getModTPL('site/com_reviews/display_reviews', 'blog', @$vars_array);
}