<?php

global $PData, $sql;

$PData->content(_lang('Список отзывов'), 'title');
if (isset($_POST['reviews-delete']) && count($_POST['del']) > 0) {
    foreach ($_POST['del'] as $del) {
        $sql->delete('_reviews', 'r_id=' . _protect($del));
    }
    $PData->content(_lang('Выбранные отзывы успешно удалены!'), 'message', true);
}

//Languages list
$langs = $sql->query("SELECT * FROM _languages ");

foreach ($langs as $key => $value) {
    $data['langs'][$value['l_id']] = $value;
}


$text = '';
$where_str = ' ORDER BY r_create_date DESC ';
$pagination = _pagination('_reviews', 'r_id', $where_str);
$reviews = $sql->query("SELECT * FROM _reviews " . $where_str . " " . $pagination['limit']);
if (is_array($reviews)) {
    foreach ($reviews AS $cval) {

        if (mb_strlen($cval['r_message']) > 150) {
            $cval['r_message'] = mb_substr(strip_tags($cval['r_message']), 0, 150) . '...';
        }

        $text .= '<tr >
            <td>
                <input type="checkbox" value ="' . $cval['r_id'] . '" name="del[]"/>
            </td>
            <td>
                <a title="' . _lang('Редактировать') . '" href="' . getURL('admin', 'blog/edit-review', 'id=' . $cval['r_id']) . '">' . $cval['r_name'] . '</a>
            </td>
            <td>
                <a title="' . _lang('Редактировать') . '" href="' . getURL('admin', 'blog/edit-review', 'id=' . $cval['r_id']) . '">
                    ' . $cval['r_message'] . '
            </td>


            <td >
                <span class="grey">' . _lang('Дата') . ':</span> ' . date(_lang('Y-m-d H:i:s'), strtotime($cval['r_create_date'])) . '<br/>

            </td>
            <td >
                <span class="grey">' . _lang('Язык') . ':</span> ' . (($cval['r_lang'] == 0) ? _lang('Не указан') : $data['langs'][$cval['r_lang']]['l_title']) . '<br/>

            </td>

        </tr>';

    }
    $text =
        '<form class="list" method="POST">
            <div class="table-holder">
            <table class="table">
              <thead>
                    <th class="numer">' . _lang('Выбрать') . '</th>
                    <th>' . _lang('От') . '</th>
                    <th>' . _lang('Текст отзыва') . '</th>
                    <th>' . _lang('Дата') . '</th>
                    <th>' . _lang('Язык') . '</th>
              </thead>
              <tbody>
                    ' . $text . '
              </tbody>
            </table>
            </div>
            <input class="button" type="submit" value="' . _lang('Удалить') . '" name="reviews-delete">
        </form>'
        . $pagination['html'];

} else {
    $text = _lang('Не найдено! =\ ');
}

$PData->content($text);
