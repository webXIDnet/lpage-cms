<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Додавання категорій товарів
 *
***/

global $meta;
$category_type='posts';

if(isset($_GET['category_type']) && !empty($_GET['category_type']))
	$category_type=$_GET['category_type'];

$_GET['category_type'] = $category_type;

/** Опрацювання Пост запитів **/

if(isset($_POST['delCat']) && is_array($_POST['delCat'])){

	$where2 = $where1 = $where = '';

	foreach($_POST['delCat'] AS $key=>$val){

		if(!empty($where)){

			$where .= ' OR ';
			$where1 .= ' OR ';
			$where2 .= ' OR ';

		}

		$where .= "c_id='"._protect($key)."'";
		$where1 .= "p_cat_id='"._protect($key)."'";
		$where2 .= "c_parent_id='"._protect($key)."'";

	}

	if(!empty($where)){

		$sql->update( '_posts', $where1, array('p_cat_id'=>0) );
		$sql->update( '_categories', $where2, array('c_parent_id'=>0) );
		$sql->query("DELETE FROM `_categories` WHERE {$where}",'query');

		$PData->content('Запись удалена','message',TRUE);

	}else{

		$PData->content('Произошел сбой алгоритма. Запись небыла удалена. Обратитесь пожалуйста к разработчикам.','message');

	}

}elseif(isset($_POST['newCat']) && is_array($_POST['newCat'])){

	$id = $sql->query("SELECT MAX(p_id) FROM _posts", 'value');
	$id = $id['MAX(p_id)'];
	$id++;

	if(empty($_POST['newCat']['url'])){
		$url = $_POST['newCat']['url'] = _textToURL($_POST['newCat']['title']);
	}else{
		$url = $_POST['newCat']['url'] = _textToURL($_POST['newCat']['url']);
	}

	$cval = $sql->query("
		SELECT c_id
		FROM _categories
		WHERE c_url='"._protect($url)."' AND c_type='"._protect($category_type)."'
	",'value');

	while($cval){

		$_POST['newCat']['url']=$url.'-'.$i;
		$i++;

		$cval = $sql->query("
			SELECT c_id
			FROM _categories
			WHERE c_url='"._protect($_POST['newCat']['url'])."' AND c_type='"._protect($category_type)."'
		",'value');

	}

	$_POST['newCat']['title'] = trim($_POST['newCat']['title']);

	$array = $_POST['newCat'];
	$array['type']=$category_type;
	$array['lang'] = $meta->val('md_lang');
	$array['id'] = $id;
	$sql->insert('_categories',$array,'c_');

	$PData->content('Запись Сохранена','message',TRUE);

}


/** Формування списку батьківських категорій **/

$sqlTPL->getAllCategories(@$category_type);


/** Пагінація **/

$parent_select=$text='';

$where = "WHERE c_parent_id='0' AND c_type='"._protect(@$category_type)."'";

if(!empty($_GET['filter'])){
    $where .= " AND c_title LIKE '%"._protect($_GET['filter'])."%'";
}

$where = _run_filter('Blog_category_QUERY_WHERE',$where,@$category_type);

$nav = _pagination(
	'_categories',
	'c_id',
	$where,
	getURL('admin','categories',http_build_query($_GET))
);


/** Формування основного списку категорій **/


global 	$categories,
		$list;

$categories = $sql->query("
	SELECT *
	FROM _categories
	{$where}
	ORDER BY c_title ASC
	".@$nav['limit']."
");


/** Формування основного HTML коду **/

$PData->content('Категории ('.$category_type.')','title');

$PData->content( $PData->getModTPL('admin/a_categories', 'blog') . $nav['html'], 'main', FALSE, FALSE );