<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Додавання постів
 *
 ***/


global $list, $meta, $blog_options;

$postType='page';

if(!empty($_GET['type']))
    $postType = $_GET['type'];

if(isset($_POST['postNewsubmit'])){
    // $mas=$_POST['pageAdd'];
    $counter = 0; $p_id = 0;
    foreach ($_POST['pageAdd'] as $key => $mas) {
        $counter++;
        
        if(!empty($mas['title'])){


            $url=$mas['article'];
            if(empty($mas['article'])){

                $url=$mas['article'] = _textToURL($mas['title']);
            }
                $i=1;

                $cval = $sql->query("SELECT `p_id` FROM `_posts` WHERE p_article='"._protect($url)."' AND p_cat_id='-1'",'value');

                while($cval){

                    $mas['article']=$url.'-'.$i;
                    $i++;


                    $cval = $sql->query("SELECT `p_id` FROM `_posts` WHERE p_article='"._protect($mas['article'])."' AND p_cat_id='-1'",'value');

                }



            $mas['pablic_date'] = @$mas['pablic_date'] == '' ? time() : $mas['pablic_date'];



            $mas = _run_filter('Blog_post_update_array',$mas);

            $mas['lang'] = $key;
            $mas['cat_id'] = '-1';
            // var_dump($mas); exit;
            if ($counter > 1) {
                $mas['id'] = $p_id;
            }
            
            $post_id = $sql->insert('_posts', $mas,'p_', false, true);
            if ($counter == 1) {
                $p_id = $post_id;                
            }   
            $mas['id'] = $p_id;
            if( writeImg($postType.$p_id,'images') ){

                $newImgURL = _getRandStr();

                while(_checkURL(HOMEPAGE.'media/files/miniatures/'.$newImgURL.'.png')){

                    $newImgURL = _getRandStr();

                }

                $array=array($blog_options['miniatures']['width']=>'media/files/miniatures/'.$newImgURL.'.png');
                $mas['imgs']=json_encode($array);

                $PData->content('Обрезать изображение','title');

                $PData->content(

                    cropImgForm(HOMEPAGE.'media/files/miniatures/big/page'.$p_id.'.png',
                        $array=array(
                            $blog_options['miniatures']['width']=>CODE_FOLDER.'../media/files/miniatures/'.$newImgURL.'.png'
                        ),
                        PAGE_URL,
                        'cropbox',
                        $blog_options['miniatures']
                    )
                );

                $mas = _run_filter('Blog_post_update_array',$mas);
                $sql->insert('_posts', $mas,'p_', true, false);

                $admin->getAdminTheme('small'); return;
            }

            _run_action('Blog_post_update_include_id',$_REQUEST);
            $sql->insert('_posts', $mas,'p_', true, false);
            $PData->content('Запись сохранена. <a target="_blank" href="'.getURL('page',$mas['article']).'">'._lang('Посмотреть страницу').'</a>','message',TRUE);

            unset($mas);
            unset($_POST['pageAdd']);

        }else{

            $PData->content('Нужно указать Заголовок','message', false);

        }
    }
}


//Languages list
if ($PData->GetSettings('multilang') != true) $where = 'WHERE l_id='.$meta->val('md_lang'); else $where = '';
$langs = $sql->query("SELECT * FROM _languages ".$where);

$data = array('posts');

$data['pages'] = $sql->query("SELECT * FROM _posts WHERE p_cat_id=-1");

foreach ($langs as $key => $value) {
        $data['langs'][$value['l_id']] = $value;
    }

    // $data['langs'] = $langs;
    $data['default_lang'] = $meta->val('md_lang');
//   _dump($data);
    $PData->content( 'Добавить страницу', 'title' );
    $PData->content( $PData->getModTPL('admin/a_page_add', 'blog', $data), 'main', FALSE, FALSE );
    // unset($posts);



    
