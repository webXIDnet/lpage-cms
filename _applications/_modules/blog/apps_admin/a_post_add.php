﻿<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Додавання постів
 *
 ***/

$sqlTPL->getCategories('posts');

global $list, $meta;

$postType = 'post';

if (!empty($_GET['type']))
    $postType = $_GET['type'];

if (isset($_POST['postNewsubmit'])) {

    $id = $sql->query("SELECT MAX(p_id) FROM _posts", 'value');
    $id = $id['MAX(p_id)'];
    $id++;

    foreach ($_POST['postAdd'] as $key => $mas) {

        if (!empty($mas['title'])) {

            // $mas=$_POST['postAdd'];
            $url = $mas['article'];
            if (empty($mas['article'])) { $url = $mas['article'] = _textToURL($mas['title']); }
            $i = 1;

            $cval = $sql->query("
            SELECT p_id
            FROM _posts
            WHERE p_article='" . _protect($url) . "'
        ", 'value');

            while ($cval) {

                $mas['article'] = $url . '-' . $i;
                $i++;

                $cval = $sql->query("
                SELECT p_id
                FROM _posts
                WHERE p_article='" . _protect($mas['article']) . "'", 'value');

            }


            $mas['pablic_date'] = @$mas['pablic_date'] != '' ? $mas['pablic_date'] : time();

            $mas = _run_filter('Blog_post_insert_array', $mas);


            $mas['id'] = $id;
            $mas['lang'] = $key;
            $tagsArray = explode(',', $mas['taxTags']);
            unset($mas['taxTags']);
            $old_val['id'] = $sql->insert('_posts', $mas, 'p_', false, true);
            if($tagsArray[0]!='') {
                global $taxonomy;
                $taxonomy->updatePostTags($old_val['id'], $tagsArray);
            }

            _run_action('Blog_post_insert_include_id', $_REQUEST + array('id' => $id));

            if (writeImg($postType . $id, 'images')) {

                $newImgURL = _getRandStr();

                while (_checkURL(HOMEPAGE . 'media/files/miniatures/' . $newImgURL . '.png')) {

                    $newImgURL = _getRandStr();

                }

                $array = array($blog_options['miniatures']['width'] => 'media/files/miniatures/' . $newImgURL . '.png');


                $sql->update('_posts', "p_id='" . _protect($id) . "' AND p_lang=" . $key, array('p_imgs' => json_encode($array)));

                $PData->content('Обрезать изображение', 'title');

                $PData->content(

                    cropImgForm(HOMEPAGE . 'media/files/miniatures/big/post' . $id . '.png',
                        $array = array(
                            $blog_options['miniatures']['width'] => CODE_FOLDER . '../media/files/miniatures/' . $newImgURL . '.png'
                        ),
                        getURL('admin', 'post-edit', 'id=' . $id),
                        'cropbox',
                        $blog_options['miniatures']
                    )
                );

                $admin->getAdminTheme('small'); return;
            }

            $PData->content(_lang('Запись сохранена') . '. <a href="' . getURL('admin', 'post-edit', 'id=' . $id) . '">' . _lang('Редактировать страницу') . '</a>', 'message', TRUE);
            // return;

        } else {

            $PData->content('Нужно указать Заголовок', 'message');

        }
    }
} elseif (isset($_POST['small'])) {

    $imgs = json_decode(urldecode($_POST['imgs']), TRUE);

    if (is_array($imgs)) {

        foreach ($imgs AS $key => $val) {

            $PData->content('Обрезать изображение', 'title');

            $PData->content(

                cropImgForm(HOMEPAGE . 'media/files/miniatures/big/post' . $_GET['post'] . '.png',
                    $array = array(
                        $key => CODE_FOLDER . '../' . $val
                    )
                )
            );
            $admin->getAdminTheme('small');

        }
    }
} elseif (isset($_POST['revomeImages'])) {

    unlink(CODE_FOLDER . '../media/files/miniatures/big/post' . $_GET['post'] . '.png');

    $imgs = json_decode(urldecode($_POST['imgs']), TRUE);

    if (is_array($imgs)) {

        foreach ($imgs AS $key => $val) {

            unlink(CODE_FOLDER . '../' . $val);

        }
    }
}

//Languages list
if ($PData->GetSettings('multilang') != true) $where = 'WHERE l_id=' . $meta->val('md_lang'); else $where = '';
$langs = $sql->query("SELECT * FROM _languages " . $where);
foreach ($langs as $key => $value) {
    $data['langs'][$value['l_id']] = $value;
}
// $data['langs'] = $langs;
$data['default_lang'] = $meta->val('md_lang');

$PData->content('Новый пост', 'title');

$PData->content($PData->getModTPL('admin/a_post_add', 'blog', $data), 'main', FALSE, FALSE);