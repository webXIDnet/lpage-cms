﻿<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Додавання постів
 *
 ***/

$sqlTPL->getCategories('posts');

global $list, $meta;
$postType = 'post';

if (!empty($_GET['type']))
    $postType = $_GET['type'];

if (isset($_POST['postNewsubmit'])) {
//    print_r($_POST['postEdit']); exit;
    // $mas=$_POST['postEdit'];
    foreach ($_POST['postEdit'] as $key => $mas) {

        if (!empty($mas['title'])) {

            $url = $mas['article'];

            if (empty($mas['article'])) { $url = $mas['article'] = _textToURL($mas['title']); }

            $i = 1;

            $cval = $sql->query("SELECT `p_id` FROM `_posts` WHERE p_article='" . _protect($url) . "' AND p_cat_id>0 AND p_id!='" . _protect($_GET['id']) . "' AND p_lang=" . $key, 'value');

            while ($cval) {

                $mas['article'] = $url . '-' . $i;
                $i++;

                $cval = $sql->query("SELECT `p_id` FROM `_posts` WHERE p_article='" . _protect($mas['article']) . "' AND p_cat_id>0 AND p_id!='" . _protect($_GET['id']) . "' AND p_lang=" . $key, 'value');

            }



            $mas['pablic_date'] = @$mas['pablic_date'] == '' ? time() : $mas['pablic_date'];
            $tagsArray = explode(',', $mas['taxTags']);
            unset($mas['taxTags']);
            if (writeImg($postType . $_GET['id'], 'images')) {

                $newImgURL = _getRandStr();

                while (_checkURL(HOMEPAGE . 'media/files/miniatures/' . $newImgURL . '.png')) {

                    $newImgURL = _getRandStr();

                }

                $array = array($blog_options['miniatures']['width'] => 'media/files/miniatures/' . $newImgURL . '.png');
                $mas['imgs'] = json_encode($array);

                $PData->content('Обрезать изображение', 'title');

                $PData->content(

                    cropImgForm(HOMEPAGE . 'media/files/miniatures/big/post' . $_GET['id'] . '.png',
                        $array = array(
                            $blog_options['miniatures']['width'] => CODE_FOLDER . '../media/files/miniatures/' . $newImgURL . '.png'
                        ),
                        PAGE_URL,
                        'cropbox',
                        $blog_options['miniatures']
                    )
                );

                $mas = _run_filter('Blog_post_update_array', $mas);
                $mas['id'] = _protect(@$_GET['id']);
                $mas['lang'] = $key;
                $sql->insert('_posts', $mas, 'p_', true, false);

                $admin->getAdminTheme('small'); return;
            }

            $mas = _run_filter('Blog_post_update_array', $mas);
            $mas['id'] = _protect(@$_GET['id']);
            $mas['lang'] = $key;
//            var_dump($mas); exit;
            $old_val = $sql->query("SELECT * FROM _posts WHERE p_id=" . _protect($mas['id']) . " AND p_lang=" . _protect($mas['lang']), 'value');
            if ($old_val) {
                $sql->update('_posts', "p_id=" . $mas['id'] . " AND p_lang=" . $mas['lang'], $mas, 'p_');
            } else {
                $old_val['id']=$sql->insert('_posts', $mas, 'p_',false,true);
            }

                global $taxonomy;
                $taxonomy->updatePostTags($old_val['id'], $tagsArray);
            _run_action('Blog_post_update_include_id', $_REQUEST);

            $PData->content('Запись сохранена. <a target="_blank" href="' . getURL('post', $mas['article'], 'category=' . $list->category_index[$key][$mas['cat_id']]['c_title']) . '">' . _lang('Посмотреть страницу') . '</a>', 'message', TRUE);

            unset($mas);
            unset($_POST['postEdit']);

        } else {

            $PData->content('Нужно указать Заголовок', 'message', false);

        }
    }
} elseif (isset($_POST['small'])) {

    $imgs = json_decode(urldecode($_POST['imgs']), TRUE);

    if (_checkURL(HOMEPAGE . 'media/files/miniatures/big/post' . $_GET['id'] . '.png')) {

        if (!is_array($imgs)) {

            $newImgURL = _getRandStr();

            while (_checkURL(HOMEPAGE . 'media/files/miniatures/' . $newImgURL . '.png')) {

                $newImgURL = _getRandStr();

            }

            $imgs = array(330 => 'media/files/miniatures/' . $newImgURL . '.png');

            $sql->update('_posts', "p_id='" . _protect($_GET['id']) . "'", array('p_imgs' => json_encode($imgs)));

        }

        foreach ($imgs AS $key => $val) {

            $PData->content('Обрезать изображение', 'title');

            $PData->content(

                cropImgForm(HOMEPAGE . 'media/files/miniatures/big/post' . $_GET['id'] . '.png',
                    $array = array(
                        $key => CODE_FOLDER . '../' . $val
                    )
                )
            );
            $admin->getAdminTheme('small');

        }

    }
} elseif (isset($_POST['revomeImages'])) {

    if (file_exists(CODE_FOLDER . '../media/files/miniatures/big/post' . $_GET['id'] . '.png'))
        unlink(CODE_FOLDER . '../media/files/miniatures/big/post' . $_GET['id'] . '.png');

    $imgs = json_decode(urldecode(@$_POST['imgs']), TRUE);

    if (is_array($imgs)) {

        foreach ($imgs AS $key => $val) {

            unlink(CODE_FOLDER . '../' . $val);

        }
    }
}

{
//Languages list
    if ($PData->GetSettings('multilang') != true) $where = 'WHERE l_id=' . $meta->val('md_lang'); else $where = '';
    $langs = $sql->query("SELECT * FROM _languages " . $where);

    $cval = $sql->query("
	SELECT *
	FROM _posts 
	WHERE p_id='" . _protect(@$_GET['id']) . "'
");
    $data = array('posts');
    if ($cval) {
        // global $posts;
        // $posts = $cval;
        foreach ($cval as $key => $value) {
            $data['posts'][$value['p_lang']] = $value;
        }

        foreach ($langs as $key => $value) {
            $data['langs'][$value['l_id']] = $value;
        }

        // $data['langs'] = $langs;
        $data['default_lang'] = $meta->val('md_lang');
        // _dump($data);
        $PData->content('Редактирование поста', 'title');
        $PData->content($PData->getModTPL('admin/a_post_edit', 'blog', $data), 'main', FALSE, FALSE);
        // unset($posts);

    } else {

        $PData->redirect('', '404');

    }
}

	
