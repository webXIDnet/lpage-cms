<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/
global $meta;
$post_type = 'posts';

if(!empty($_GET['post_type'])){
	$post_type = $_GET['post_type'];
}
$_GET['post_type'] = $post_type;

$sqlTPL->getCategories($post_type);

global 	$list,
		$posts;

$title = '(Blog)';

if(isset($_POST['del_post'])){

	$where='';

	foreach($_POST['del_post'] AS $key=>$val){

		if( !empty($where) ) $where.=' OR ';

		$where .= "p_id='"._protect($key)."'";

	}

	$sql->query( "DELETE FROM _posts WHERE $where;", 'query' );

	$PData->content('Запись удалена','message',TRUE);

}


/** Пагінація **/

$text = $where = '';


/** Формування основного списку **/

//Multilang fix
$where .= " AND p_lang=".$meta->val('md_lang')." ";

//$lang_count=count(_getLang());

/*$where = "
	INNER JOIN _categories
	ON p_cat_id=c_id
	WHERE p_cat_id>0
	ORDER BY p_pablic_date DESC";*/


if($post_type){
	switch($post_type){

		case'posts':
			$where = "
				INNER JOIN _categories
				ON p_cat_id=c_id
				WHERE p_cat_id>0 ".$where;


		break;
		default:
			$where = _run_filter('Blog_posts_MAIN_QUERY_WHERE','',$post_type);
	}
}

if(!empty($_GET['filter'])){
    if(!empty($where))$where.=' AND ';
    $where .= " p_title LIKE '%"._protect($_GET['filter'])."%'";
}

if(!empty($_GET['filter_cat'])){
	if(!empty($where))$where.=' AND ';
	$where .= " p_cat_id = '"._protect($_GET['filter_cat'])."'";
}

$where = _run_filter('Blog_posts_QUERY_WHERE',$where,$post_type);


$nav = _pagination('_posts','p_id',$where);

$posts = $sqlTPL->getPosts('10_posts', $where.' '.$nav['limit'] );



$PData->content('Все записи '.$title,'title');

$PData->content( $PData->getModTPL('admin/a_posts_list', 'blog') . $nav['html'], 'main', FALSE, FALSE);