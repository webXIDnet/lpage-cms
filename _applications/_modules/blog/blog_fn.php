<?php
_add_filter('blog_admin-panel_mod_settings','blog_adminpanel_mod_settings');

function blog_adminpanel_mod_settings($result,$set){

    global $meta;
//    $result['title'] = 'Blog';
//    var_dump($set);
    $set['miniatures']['width'] = isset($set['miniatures']['width']) ? $set['miniatures']['width'] : 330;
    $set['miniatures']['height'] = isset($set['miniatures']['height']) ? $set['miniatures']['height'] : 330;

    $result['text'] = '<p>'._lang('Размеры миниатюр').'
			<label style="display: block;">
				'._lang('Ширина').'<input class="input" maxlength="10" placeholder="'._lang('Ширина').'" type="text" name="modSet[apSet_blog][miniatures][width]" value="'.$set['miniatures']['width'].'"/> px
			</label>
			<label style="display: block;">
				'._lang('Высота').'<input class="input" placeholder="'._lang('Высота').'" type="text" name="modSet[apSet_blog][miniatures][height]" value="'.$set['miniatures']['height'].'"/> px
			</label>
			<label style="display: block;">
				<input '.(isset($set['miniatures']['use_crop_thumb']) && $set['miniatures']['use_crop_thumb'] == '1' ? 'checked' : '').' type="checkbox" class="input" placeholder="'._lang('Высота').'" name="modSet[apSet_blog][miniatures][use_crop_thumb]" value="1"/> '._lang('Использовать привязку области выбора миниатюры').'
			</label>

		</p><input class="btn button btn-primary" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit"><br/><hr/><br/>';



    return $result;
}