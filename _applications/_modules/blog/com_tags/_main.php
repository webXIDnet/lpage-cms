<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 31.07.2015
 * Time: 16:35
 */

_add_action('defined_modules', 'show_tags');

function show_tags()
{
    global $taxonomy,$sql;
    if($taxonomy==null){
        $taxonomy= new Tags;
        $taxonomy->getAllTags();
    }
    if (PAGE_TYPE == 'tag') {
        require_once 'tag_posts.php';

    }

}