<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 31.07.2015
 * Time: 16:36
 */
global $PData,$taxonomy,$sql;
if (PAGE_TYPE != 'tag' || $_GET['tag']=='') {
    header('HTTP/1.0 404 not found');
    exit();
}
$tag=$_GET['tag'];

$where=implode(',',$taxonomy->name_id[$tag]->posts_id);
$query="SELECT * FROM _posts AS P WHERE P.id IN ("._protect($where).");";
$vars_array['posts']=$sql->query($query);
$PData->content( $PData->getModTPL('site/com_tags/post_list', 'blog', $vars_array), 'main', FALSE, FALSE );