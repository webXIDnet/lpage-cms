<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 *
 * Модуль Підключення таксономії
 *
 * v1.1
 *
 ***/

require_once __DIR__ . '/tags_class.php';

_add_action('defined_modules', 'blog_tax_defined_modules');

if (PAGE_TYPE == 'admin') {
    _add_filter('Blog_page_Add_Page', 'add_TaxTagSelector');
    _add_filter('Blog_page_Edit_Page', 'edit_TaxTagSelector');

    _add_action('Blog_page_insert_include_id', 'update_PageTaxTag');
    _add_action('Blog_page_update_include_id', 'update_PageTaxTag');
    _add_action('Blog_post_insert_include_id', 'update_PageTaxTag');
    _add_action('Blog_post_update_include_id', 'update_PageTaxTag');
}

function blog_tax_defined_modules()
{

    if (PAGE_TYPE == 'admin') {

        global $sql,
               $taxonomy;

        $sql->db_table_installer('Blog_Tags', '05.07.2015',
            array(
                '_posts_tags' => array(
                    'ptag_p_id' => "`ptag_p_id` int(11) NOT NULL",
                    'ptag_tag_id' => "`ptag_tag_id` int(11) NOT NULL",
                    'CREATE' => "`ptag_p_id` int(11) NOT NULL"
                ),
                '_tags' => array(
                    'tag_id' => "`tag_id` int(11) NOT NULL AUTO_INCREMENT",
                    'tag_key' => "`tag_key` varchar(50) NOT NULL",
                    'CREATE' => "`tag_id` int(11) NOT NULL AUTO_INCREMENT, `tag_key` varchar(50) NOT NULL, PRIMARY KEY (`tag_id`), UNIQUE INDEX `tag_key` (`tag_key`)"
                )
            )
        );

        $taxonomy = new Tags;
        $taxonomy->getAllTags();


    }

}

function add_TaxTagSelector($result)
{
    $lang_id = $result['lang_id'];
    blog_tax_defined_modules();

    global $taxonomy;

    $res = '';
    if (!defined('TAGIT')) {
        define('TAGIT', 'TRUE');

        $res .= '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.js"></script>' . "\n";

        $res .= '<script type="text/javascript" src="' . HOMEPAGE . '/plugins/tag-it/tag-it.js"></script>' . "\n";
        $res .= '<link href="' . HOMEPAGE . '/plugins/tag-it/jquery.tagit.css" type="text/css" rel="stylesheet"/>' . "\n";
        $res .= '<link href="' . HOMEPAGE . '/plugins/tag-it/tagit.ui-zendesk.css" type="text/css" rel="stylesheet"/>' . "\n";
    }

    $avalTags = $taxonomy->getUniqueTags();
    if ($avalTags) {
        $avalTags = 'availableTags: ' . json_encode($avalTags) . ',';
    } else {
        $avalTags = '';
    }

    $currInput = '';
    $currTags = '';


    $res .= '<p><label>' . _lang('Теги') . '</label>
			<input type="hidden" name="postAdd[' . $lang_id . '][taxTags]" id="taxTags_' . $lang_id . '" value="' . $currInput . '">
			<input type="hidden" id="taxTagsPost_' . $lang_id . '" value="' . $currInput . '">
			<script>
				(function($){
					$("#taxTags_' . $lang_id . '").tagit({' . $avalTags . 'allowSpaces: true});
					$("FORM").submit(function(){ $("#taxTagsPost_' . $lang_id . '").val($("#taxTags_' . $lang_id . '").tagit("assignedTags_' . $lang_id . '")); });
				})(jQuery);
    		</script>
			</p><br>
			<div class="clear"></div>' . "\n";

    return $res;
}

function edit_TaxTagSelector($result)
{
    $lang_id = $result['lang_id'];
    $p_id = $result['p_id'];
    blog_tax_defined_modules();

    global $taxonomy;

    $res = '';
    if (!defined('TAGIT')) {
        define('TAGIT', 'TRUE');

        $res .= '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.js"></script>' . "\n";

        $res .= '<script type="text/javascript" src="' . HOMEPAGE . '/plugins/tag-it/tag-it.js"></script>' . "\n";
        $res .= '<link href="' . HOMEPAGE . '/plugins/tag-it/jquery.tagit.css" type="text/css" rel="stylesheet"/>' . "\n";
        $res .= '<link href="' . HOMEPAGE . '/plugins/tag-it/tagit.ui-zendesk.css" type="text/css" rel="stylesheet"/>' . "\n";
    }

    $avalTags = $taxonomy->getUniqueTags();
    if ($avalTags) {
        $avalTags = 'availableTags: ' . json_encode($avalTags) . ',';
    } else {
        $avalTags = '';
    }

    $currTags = false;
    if (isset($p_id)) {
        $currTags = $taxonomy->getTagsByPost($p_id);
    }

    $currInput = '';
    if ($currTags) {
        $currInput = implode(',', $currTags);
        $currTags = 'assignedTags: ' . json_encode($currTags) . ',';
    } else {
        $currTags = '';
    }

    $res .= '<p><label>' . _lang('Теги') . '</label>
			<input type="hidden" name="postEdit[' . $lang_id . '][taxTags]" id="taxTags_' . $lang_id . '" value="' . $currInput . '">
			<input type="hidden" id="taxTagsPost_' . $lang_id . '" value="' . $currInput . '">
			<script>
				(function($){
					$("#taxTags_' . $lang_id . '").tagit({' . $avalTags . 'allowSpaces: true});
					$("FORM").submit(function(){ $("#taxTagsPost_' . $lang_id . '").val($("#taxTags_' . $lang_id . '").tagit("assignedTags_' . $lang_id . '")); });
				})(jQuery);
    		</script>
			</p><br>
			<div class="clear"></div>' . "\n";

    return $res;
}

// _add_filter('Blog_page_insert_array_id', 'update_PageTaxTag');
// _add_filter('Blog_page_update_array', 'update_PageTaxTag');
// _add_filter('', 'update_PageTaxTag');

function update_PageTaxTag($result)
{

    blog_tax_defined_modules();

    global $taxonomy;

    if ($result && isset($result['taxTagsPost'])) {
        $taxTags = explode(',', $result['taxTagsPost']);
        if (is_array($taxTags)) {
            $taxonomy->updatePostTags($result['id'], $taxTags);
        }
    }
//	return $result;
}