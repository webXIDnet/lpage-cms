<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 *
 * Модель Тегов (таксономия)
 *
 * v1.1
 *
 ***/
class Tags
{
    public $all;
    public $name_id;

    public function getUniqueTags()
    {
        $ret = array();
        if (is_array($this->all)) {
            foreach ($this->all as $val) {
                if (!in_array($val['tag_key'], $ret)) {
                    $ret[] = $val['tag_key'];
                }
            }
        }
        return count($ret) > 0 ? $ret : false;
    }

    public function getTagsByPost($pId, $withID = false)
    {
        $ret = array();
        if (is_array($this->all)) {
            foreach ($this->all as $val) {
                if ($val['ptag_p_id'] == $pId) {
                    if ($withID) {
                        $ret[] = array('tag_id' => $val['tag_id'], 'tag_key' => _unprotect($val['tag_key'],'input'));
                    } else {
                        $ret[] = _unprotect($val['tag_key'],'input');
                    }

                }
            }
        }
        return count($ret) > 0 ? $ret : false;
    }

    public function updatePostTags($post_id, array $tags)
    {
        global $sql;
        //add tags to db
        foreach ($tags as $tag) {
            if($tag=='') continue;
            $tag=_protect($tag,true);
            $query = "INSERT IGNORE INTO _tags (tag_key) VALUES ('" . $tag . "');";
            $sql->query($query, 'query');
            $query = "SELECT tag_id FROM _tags WHERE tag_key='" . $tag. "';";
            $tag_id = $sql->query($query, 'value');
            $tags_name_id[$tag] = $tag_id['tag_id'];
        }
        //delete tag links
        $query = "DELETE FROM _posts_tags WHERE `ptag_p_id` =" . _protect($post_id);
        $sql->query($query, 'query');
        //add tag links
        $query_queue = '';
        if(isset($tags_name_id)) {
            foreach ($tags_name_id as $tag => $tag_id) {
                $query_queue .= " INSERT INTO `_posts_tags` (`ptag_p_id`, `ptag_tag_id`) VALUES (" . _protect($post_id) . ", " . $tag_id . ");";
            }
            $sql->query($query_queue, 'query', false, true);
        }
    }


    public function add($taxTag)
    {
        global $sql;

        $where = '';
        $ins = array();
        if (is_array($taxTag)) {
            foreach ($taxTag AS $tag) {
                if (isset($tag['tag_id'])) {
                    $ins['tag_id'] = $tag['tag_id'];
                } else {
                    $ins['tag_id'] = $sql->insert('_tags', array('tag_key' => $tag['tag_key']), '', FALSE, TRUE);
                }
                $ins['ptag_p_id'] = $tag['ptag_p_id'];

                if (isset($ins['ptag_tag_id'])) {
                    $sql->insert('_posts_tags', $ins);
                }
            }
        }
    }

    public function delete($taxTag)
    {
        global $sql;

        $where = '';
        if (is_array($taxTag)) {
            if (isset($taxTag['ptag_p_id'])) {
                $where = "(ptag_p_id=" . _protect($taxTag['ptag_p_id']) . " AND ptag_tag_id=" . _protect($taxTag['ptag_tag_id']) . ")";
            } else {
                foreach ($taxTag AS $tag) {
                    if (!empty($where)) {
                        $where .= ' OR ';
                    }
                    $where .= "(ptag_p_id=" . _protect($tag['ptag_p_id']) . " AND ptag_tag_id=" . _protect($tag['ptag_tag_id']) . ")";
                }
            }

            if (!empty($where)) {
                $sql->delete('_posts_tags', $where);
                $sql->delete('_tags', "tag_id NOT IN (SELECT ptag_tag_id FROM _posts_tags GROUP BY ptag_tag_id)");
            }
            return TRUE;
        } else {
            if ($taxTag) {
                $sql->delete('_tags', "tag_key='" . _protect($taxTag) . "'");
                $sql->delete('_posts_tags', "ptag_tag_id NOT IN (SELECT tag_id FROM _tags)");
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getAllTags()
    {
        global $sql;

        $result = $sql->query("
			SELECT *
			FROM _tags
			LEFT JOIN _posts_tags ON ptag_tag_id=tag_id
		");

        $this->all = $result;
        foreach($result as $tag){
            if(!isset($this->name_id[_unprotect($tag['tag_key'])])) {
                $this->name_id[_unprotect($tag['tag_key'])] = new Tag($tag['tag_id'], _unprotect($tag['tag_key']));
            }
            $this->name_id[_unprotect($tag['tag_key'])]->posts_id[]=_protect($tag['ptag_p_id']);
        }
    }
}
class Tag{
    public $id;
    public $name;
    public $posts_id=array();

    /**
     * Tag constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

}