<?php if (!defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

global $admin, $contTPL, $post, $list, $categories, $PData;
$img_dir = SITE_FOLDER . 'media/_admin/';

$categories_select_list = '';

if (isset($categories)) {
    $categories_select_list = $contTPL->catHierarchTree($categories, 0, '', '', 'table');
}

?>
<form class="list" method="POST">
    <p><b>+ <?php echo _lang('Добавить категорию'); ?>:</b></p>
    <input placeholder="<?php echo _lang('Заголовок'); ?>" type="text" name="newCat[title]" value=""/>
    <input placeholder="<?php echo _lang('Текстовая ссылка'); ?>" type="text" name="newCat[url]" value=""/>
    <select name="newCat[parent_id]">
        <option value="">- <?php echo _lang('Родительская категория'); ?> -</option>
        <?php echo $contTPL->catHierarchTree(@$list->category[$_GET['category_type']]); ?>
    </select>
    <!-- Filter: app_admin_newCat_addField -->
    <?php echo _run_filter('app_admin_newCat_addField', '', $_GET['category_type']); ?>
    <br/>
    <input type="submit" class="button" value="<?php echo _lang('Добавить'); ?>"/>
</form>
<div class="line"></div>

<form class="list">
    <p><b><?php echo _lang('Поиск'); ?></b>:</p>
    <input type="hidden" name="admin" value="<?php echo $PData->_GET('admin'); ?>"/>
    <input placeholder="<?php echo _lang('Заголовок'); ?>" type="text" name="filter"
           value="<?php echo $PData->_GET('filter'); ?>"/><br/>
    <input type="submit" class="button" value="<?php echo _lang('Искать'); ?>"/>
    <!-- Filter: app_admin_newCat_addField -->
    <?php echo _run_filter('app_admin_filter_of_Cat_addField', '', $PData->_GET('category_type')); ?>
</form>
<div class="line"></div>

    <form class="list" method="POST">
        <table class="table">
            <thead>
                <th class="numer"><?php echo _lang('Выбрать'); ?></th>
                <th><?php echo _lang('Заголовок'); ?></th>
                <th><?php echo _lang('Текстовая ссылка'); ?></th>
                <th><?php echo _lang('Доп. данные'); ?></th>
            </thead>
            <tbody>
            <?php echo $categories_select_list; ?>
            </tbody>
        </table>
        <input class="button" type="submit" value="<?php echo _lang('Удалить'); ?>" name="postssubmit">
    </form>