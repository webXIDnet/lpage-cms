<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу 

global $admin, $contTPL, $post, $list, $category;

if(is_array($list->category[$category['c_type']])){
    $parent_select = $contTPL->catHierarchTree($list->category[$category['c_type']],0,$category['c_parent_id'],$category['c_id']);
}

?>
<form class="list" method="POST">
        <table>
            <tr>
                <td>
                    <label><b><?php echo _lang('Заголовок'); ?></b>:</label>
                    <input placeholder="<?php echo _lang('Заголовок'); ?>" type="text" name="editCat[title]" value="<?php echo $category['c_title']; ?>"/>
                    
                    <label><b><?php echo _lang('Текстовая ссылка'); ?></b>:</label>
                    <input placeholder="<?php echo _lang('Текстовая ссылка'); ?>" type="text" name="editCat[url]" value="<?php echo $category['c_url']; ?>"/>
                    
                    <label><b><?php echo _lang('Родительская категория'); ?></b>:</label>
                    <select name="editCat[parent_id]">
                        <option value="">- <?php echo _lang('Родительская категория'); ?> -</option>
                        <?php echo @$parent_select; ?>
                    </select>
                    
                    <!-- Filter: app_admin_editCat_addField -->
                    <?php echo _run_filter('app_admin_editCat_addField', '', $category); ?>    
                    <label><b><?php echo _lang('Тип категории'); ?></b>: <?php echo _lang($category['c_type']); ?></label>
                    <input type="hidden" name="category_type" value="<?php echo $category['c_type']; ?>"/>
                            
                    <br/><br/>
                    <input type="submit" class="btn" value="<?php echo _lang('Сохранить'); ?>"/>
                </td>
            </tr>
        </table>
    </form>
    <hr/>