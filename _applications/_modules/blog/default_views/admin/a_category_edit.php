<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
}
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 06.07.2015
 * Time: 22:41
 */
?>
<form method="post">
    <div class="tab-box" role="tabpanel">
        <ul class="nav nav-tabs" id="myTab">
            <?php

            foreach ($vars_array['langs'] as $lang) {
                if ($lang['l_id'] == $vars_array['main_lang']) {
                    echo '<li class="active" role="presentation">';
                } else {
                    echo '<li role="presentation">';
                }
                echo '<a href="#' . $lang['l_id'] . '" data-toggle="tab">' . $lang['l_title'] . '</a></li>';
            }
            ?>
        </ul>

        <div class="tab-content">
            <?php
            $category = $vars_array['category'];
            $c_type = $vars_array['c_type'];
            $main_lang_id = $vars_array['main_lang'];
            foreach ($vars_array['langs'] as $lang) {
                echo '<div class="tab-pane ' . ($lang['l_id'] == $vars_array['main_lang'] ? "active" : "") . '" id="' . $lang['l_id'] . '">';
                echo 'Title<input type="text" name="lang[' . $lang['l_id'] . '][c_title]" value="' . @$category[$lang['l_id']]['c_title'] . '">';
                echo 'Url<input type="text" name="lang[' . $lang['l_id'] . '][c_url]" value="' . @$category[$lang['l_id']]['c_url'] . '">';
                echo 'c_desc<input type="text" name="lang[' . $lang['l_id'] . '][c_desc]" value="' . @$category[$lang['l_id']]['c_desc'] . '">';
                echo 'Parrent<select name="lang[' . $lang['l_id'] . '][c_parent_id]">';
                echo '<option  value="0">Not selected</option>';
                foreach ($vars_array['categorys'][$c_type] as $cat) {
                    if (isset($cat[$lang['l_id']]))
                        if ($category[$lang['l_id']]['c_parent_id'] == $cat[$lang['l_id']]['c_id']) {
                            echo '<option selected value="' . $cat[$lang['l_id']]['c_id'] . '">' . $cat[$lang['l_id']]['c_title'] . '</option>';
                        } else {
                            echo '<option value="' . $cat[$lang['l_id']]['c_id'] . '">' . $cat[$lang['l_id']]['c_title'] . '</option>';
                        }
                }
                echo '</select>';
                echo '</div>';
            }
            echo '<input type="hidden" value="' . $c_type . '" name="c_type">';
            echo '<input type="hidden" value="' . $_GET['id'] . '" name="c_id">';
            ?>
        </div>
    </div>
    <button class="button" name="save" type="submit">Save</button>
</form>
