<?php if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу

global $admin, $contTPL, $list, $langs_list, $PData;

$pageAdd = $PData->_POST('pageAdd');

?>
<form class="list" method="POST" enctype="multipart/form-data" id="author_form">
    <p><?php echo _lang('Миниатюра'); ?>:</p><input type="file" name="images[]" accept="image/jpeg,image/png"/>
<div class="tab-box" role="tabpanel">

    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($vars_array['langs'] as $lang) { ?>
            <li role="presentation" <?php echo $vars_array['default_lang'] == $lang['l_id'] ? "class='active'" : ""; ?> ><a href="#lang_<?php echo $lang['l_id']; ?>" aria-controls="lang_<?php echo $lang['l_id']; ?>" role="tab" data-toggle="tab"><?php echo $lang['l_title']; ?></a></li>
        <?php } ?>
    </ul>




        <div class="tab-content">
            <?php foreach ($vars_array['langs'] as $key => $lang) { ?>
                <div role="tabpanel" class="tab-pane <?php echo $vars_array['default_lang'] == $key ? "active" : ""; ?>" id="lang_<?php echo $key; ?>">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <p><?php echo _lang('Заголовок'); ?></p>
                            <input type="text" class="input-xlarge" name="pageAdd[<?php echo $key; ?>][title]"
                                   value="<?php echo isset($pageAdd[$key]['title']) ? $pageAdd[$key]['title']:  ''; ?>"/>

                            <p><?php echo _lang('Артикул (латинськими)'); ?>:</p>
                            <input type="text" class="input-xlarge" name="pageAdd[<?php echo $key; ?>][article]"
                                   value="<?php echo isset($pageAdd[$key]['article']) ? $pageAdd[$key]['article']:  ''; ?>"/>
                            <p>
                                <label><?php echo _lang('Батьківська сторінка'); ?>:</label>
                                <select name="pageAdd[<?php echo $key; ?>][parent_id]" id="" class="chosen-selectbox">
                                    <option value="<?php echo isset($page['p_id']) ? $page['p_id'] : ''; ?>">-- <?php echo _lang('не выбрано'); ?> --</option>
                                    <?php foreach ($vars_array['pages'] as $page) { ?>
                                        <option value="<?php echo $page['p_id']?>"><?php echo $page['p_title']?>  [<?=strtoupper($langs_list[$page['p_lang']]);?>]</option>
                                    <?php } ?>
                                </select>
                            </p>
                            <p><?php echo _lang('Превью текст'); ?>:</p>
            <textarea type="text" class="input-xlarge"
                      name="pageAdd[<?php echo $key; ?>][preview]"><?php echo isset($pageAdd[$key]['preview']) ? $pageAdd[$key]['preview']:  ''; ?></textarea>

                        </div>

                        <!-- Filter: Blog_post_Add_Page_2 -->
                        <?php echo _run_filter('Blog_post_Add_Page_2'); ?>


                    </div>

                    <!-- Filter: Blog_post_Add_Page_1 -->
                    <?php echo _run_filter('Blog_post_Add_Page_1'); ?>

                    <p>
                        <label><?php echo _lang('Текст'); ?>:</label>
                        <?php
                        $pageAdd_text = isset($pageAdd[$key]['preview']) ? $pageAdd[$key]['preview']:  '';
                        echo $admin->getWIZIWIG('pageAdd['.$key.'][text]', @$pageAdd_text, 'elm1'); ?>
                    </p>
                    <!-- Filter: Blog_post_Add_Page_3 -->
                    <?php echo _run_filter('Blog_post_Add_Page_3'); ?>


                </div>
            <?php } ?>
            <input class="button" type="submit" value="<?php echo _lang('Сохранить'); ?>" name="postNewsubmit"/>
    </form>