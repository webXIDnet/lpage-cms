<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

global $admin, $contTPL, $list, $langs_list;
?>
<form class="list" method="POST" enctype="multipart/form-data">

<p>
    <label><?php echo _lang('Миниатюра'); ?>:</label>
    <input type="file" name="images[]" accept="image/jpeg,image/png"/>
    <?php $key = 1;
    if(_checkURL(SITE_FOLDER.'media/files/miniatures/big/page'.@$vars_array['posts'][$key]['p_id'].'.png',FALSE)){

        if(!empty($vars_array['posts'][$key]['p_imgs'])){
            ?>
            <input type="hidden" name="imgs" value="<?php echo urlencode(@$vars_array['posts'][$key]['p_imgs']); ?>" />
            <?php
        }
        $img = json_decode(@$vars_array['posts'][$key]['p_imgs'],TRUE);
        ?>

        <a target="_blank" href="<?php echo SITE_FOLDER; ?>media/files/miniatures/big/page<?php echo @$vars_array['posts'][$key]['p_id']; ?>.png">
            <img src="<?php echo SITE_FOLDER.$img[330]; ?>"/></a><br/>
        <!--        <input type="submit" name="small" value=""<?php echo _lang('Изменить миниатюру'); ?>/>-->
        <input style="background-color:rgb(204, 0, 0);" type="submit" name="revomeImages" value="<?php echo _lang('Удалить'); ?>" />
        <?php
    }

    ?>
</p>
<div class="tab-box" role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <?php foreach ($vars_array['langs'] as $lang) { ?>
            <li role="presentation" <?php echo $vars_array['default_lang'] == $lang['l_id'] ? "class='active'" : ""; ?> ><a href="#lang_<?php echo $lang['l_id']; ?>" aria-controls="lang_<?php echo $lang['l_id']; ?>" role="tab" data-toggle="tab"><?php echo $lang['l_title']; ?></a></li>
        <?php } ?>
    </ul>

        <div class="tab-content">
            <?php foreach ($vars_array['langs'] as $key => $lang) { ?>
                <div role="tabpanel" class="tab-pane <?php echo $vars_array['default_lang'] == $key ? "active" : ""; ?>" id="lang_<?php echo $key; ?>">
                    <input style="display:none;" type="submit" name="postNewsubmit"/>

                    <div>
                        <p>
                            <label><?php echo _lang('Заголовок'); ?></label>
                            <input type="text" class="input-xlarge" name="postEdit[<?php echo $key; ?>][title]" value="<?php echo (isset($vars_array['posts'][$key]['p_title']) ? $vars_array['posts'][$key]['p_title'] : '') ; ?>"/>
                        </p>
                        <p>
                            <label><?php echo _lang('Артикул (латинськими)'); ?>:</label>
                            <input type="text" class="input-xlarge" name="postEdit[<?php echo $key; ?>][article]" value="<?php
                            echo (isset($vars_array['posts'][$key]['p_article']) ? $vars_array['posts'][$key]['p_article'] : ''); ?>"/>
                        </p>
                        <p>
                            <label><?php echo _lang('Батьківська сторінка'); ?>:</label><br>
                            <select name="postEdit[<?php echo $key; ?>][parent_id]" id="" class="chosen-selectbox">
                                <option value="<?php echo (!empty($page['p_id'])? $page['p_id'] : '');?>">-- <?php echo _lang('не выбрано'); ?> --</option>
                                <?php
                                if (isset($vars_array['pages']) && is_array($vars_array['pages']) ) {
                                foreach ($vars_array['pages'] as $page) { ?>
                                    <option <?php echo  ((isset($vars_array['posts'][$key]['p_parent_id']) && $vars_array['posts'][$key]['p_parent_id'] == $page['p_id'] ) ? 'selected' : '' ); ?> value="<?php echo $page['p_id']?>"><?php echo $page['p_title']?> [<?=strtoupper($langs_list[$page['p_lang']]);?>]</option>
                                <?php }
                                } ?>
                            </select>
                        </p>
                        <p>
                            <label><?php echo _lang('Превью текст'); ?>:</label>
                            <textarea type="text" class="input-xlarge" name="postEdit[<?php echo $key; ?>][preview]"><?php echo
                            (isset($vars_array['posts'][$key]['p_preview']) ? $vars_array['posts'][$key]['p_preview'] : ''); ?></textarea>
                        </p>

                    </div>
                    <div class="clear"></div>
                    <p>
                        <label><?php echo _lang('Текст'); ?></label>
                        <?php
                        $p_text = (isset($vars_array['posts'][$key]['p_text']) ? $vars_array['posts'][$key]['p_text'] : '');
                        echo $admin->getWIZIWIG('postEdit['.$key.'][text]', $p_text, 'elm1'); ?>
                    </p>
                    <!-- Filter: Blog_post_Edit_Page_3 -->
                    <?php // echo _run_filter('Blog_page_Edit_Page','',$page); ?>



                </div>
            <?php } ?>
            <input class="btn btn-primary button" type="submit" value="<?php echo _lang('Сохранить'); ?>" name="postNewsubmit"/>


        </div>
    </form>
</div>