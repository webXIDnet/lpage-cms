<?php if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу

global $admin, $contTPL, $pages, $list, $PData;

$i = 0;
$text = '';
foreach ($pages AS $cval) {

    $i++;

    $text .= '<tr>
        <td>
            <input type="checkbox" name="del_post[' . $cval['p_id'] . ']"/>
            <a title="' . _lang('Перейти') . '" href="' . getURL('page', $cval['p_article']) . '" target="_blank">
                ' . $cval['p_id'] . '</a>
        </td>
        <td>
            <a title="' . _lang('Редактировать') . '" href="' . getURL('admin', 'page-edit', 'id=' . $cval['p_id']) . '">
                ' . $cval['p_title'] . '</a>
        </td>
        <td class="greyText">
            <span class="grey">' . _lang('Дата изменения') . ':</span>' . date('Y-m-d', $cval['p_pablic_date']) . '<br/>
            <!-- Filter: Blog_page_table_additional_data -->
            ' . _run_filter('Blog_page_table_additional_data', '', $cval) . '
        </td>
    </tr>';

}

?>
<form class="list">
    <p><?php echo _lang('Поиск по заголовку'); ?>:</p>
    <input type="hidden" name="admin" value="<?php echo $PData->_GET('admin'); ?>"/>
    <input type="text" placeholder="<?php echo _lang('Поиск'); ?>..." name="filter"
           value="<?php echo $PData->_GET('filter'); ?>"/>
    <input type="hidden" name="post_type" value="<?php echo $PData->_GET('post_type'); ?>"/>
    <input type="submit" class="button" value="<?php echo _lang('Искать'); ?>"/>
    <!-- Filter: Blog_page_filter_form -->
    <?php echo _run_filter('Blog_page_filter_form'); ?>
</form>

<div class="line"></div>
<!-- //todo розмер шрифта-->
<form class="list" method="POST">
    <div class="table-holder">
        <table class="table">
            <thead>
                <th class="table-10"><?php echo _lang('Выбрать'); ?></th>
                <th class="table-45"><?php echo _lang('Заголовок'); ?></th>
                <th class="table-45"><?php echo _lang('Доп. поля'); ?></th>
            </thead>
            <tbody>
            <?php echo $text; ?>
            </tbody>
        </table>
    </div>
    <input class="button" type="submit" value="<?php echo _lang('Удалить'); ?>" name="postssubmit">

</form>
</div>