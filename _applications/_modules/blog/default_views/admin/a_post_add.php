<?php if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу

global $admin, $contTPL, $list, $PData;

?>
<form class="list" method="POST" enctype="multipart/form-data" id="author_form">
    <p><?php echo _lang('Миниатюра'); ?>:</p>
    <input type="file" name="images[]" accept="image/jpeg,image/png"/>
<div class="tab-box" role="tabpanel">
<ul class="nav nav-tabs" role="tablist">
    <?php foreach ($vars_array['langs'] as $lang) { ?>
        <li role="presentation" <?php echo $vars_array['default_lang'] == $lang['l_id'] ? "class='active'" : ""; ?> ><a href="#lang_<?php echo $lang['l_id']; ?>" aria-controls="lang_<?php echo $lang['l_id']; ?>" role="tab" data-toggle="tab"><?php echo $lang['l_title']; ?></a></li>
    <?php } ?>
</ul>

<div class="tab-content">
<?php

if (is_array($vars_array['langs'])) {
    $postAdd = $PData->_POST('postAdd');

    foreach ($vars_array['langs'] as $key => $lang) { ?>
<div role="tabpanel" class="tab-pane <?php echo $vars_array['default_lang'] == $key ? "active" : ""; ?>" id="lang_<?php echo $key; ?>">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <p><?php echo _lang('Заголовок'); ?></p>
            <input type="text" class="input-xlarge" name="postAdd[<?php echo $key; ?>][title]"
                   value="<?php echo (isset($postAdd[$key]['title']) ? $postAdd[$key]['title'] : ''); ?>"/>

            <p><?php echo _lang('Артикул (латинськими)'); ?>:</p>
            <input type="text" class="input-xlarge" name="postAdd[<?php echo $key; ?>][article]"
                   value="<?php echo (isset($postAdd[$key]['article']) ? $postAdd[$key]['article'] : ''); ?>"/>

            <p><?php echo _lang('Превью текст'); ?>:</p>
            <textarea type="text" class="input-xlarge"
                      name="postAdd[<?php echo $key; ?>][preview]"><?php echo (isset($postAdd[$key]['preview']) ? $postAdd[$key]['preview'] : ''); ?></textarea>

        </div>

        <!-- Filter: Blog_post_Add_Page_2 -->
        <?php echo _run_filter('Blog_post_Add_Page_2'); ?>

        <div class="col-lg-6 col-md-6">
            <p><?php echo _lang('Категория'); ?>:</p>
            <select name="postAdd[<?php echo $key; ?>][cat_id]" class="chosen-selectbox">
                <?php
                if (isset($list->category['posts'][$key])) {
                    echo $contTPL->catHierarchTree($list->category['posts'][$key], 0, 0);
                }
                ?>
            </select>


        </div>
    </div>

    <!-- Filter: Blog_post_Add_Page_1 -->
    <?php echo _run_filter('Blog_post_Add_Page_1'); ?>

    <p>
        <label><?php echo _lang('Текст'); ?>:</label>
        <?php echo $admin->getWIZIWIG('postAdd['.$key.'][text]', (isset($postAdd[$key]['text']) ? $postAdd[$key]['text'] : ''), 'elm1'); ?>
    </p>
    <!-- Filter: Blog_post_Add_Page_3 -->
    <?php
    $post['lang_id']=$lang['l_id'];
    $post['text']='';
    echo _run_filter('Blog_page_Add_Page',@$post); ?>


    </div>
<?php }
} ?>
<input class="button" type="submit" value="<?php echo _lang('Сохранить'); ?>" name="postNewsubmit"/>
</form>