<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу 

global $admin, $contTPL, $list, $langs_list;

?>
<form class="list" method="POST" enctype="multipart/form-data">
<p>
    <label><?php echo _lang('Миниатюра'); ?>:</label>
    <input type="file" name="images[]" accept="image/jpeg,image/png"/>
    <?php $key = 1;
    $post = $vars_array['posts'];
    $post = array_shift($post);
    if(_checkURL(SITE_FOLDER.'media/files/miniatures/big/post'.@$post['p_id'].'.png',FALSE)){

        if(!empty($post['p_imgs'])){
            ?>
            <input type="hidden" name="imgs" value="<?php echo urlencode(@$post['p_imgs']); ?>" />
            <?php
        }
        $img = json_decode(@$post['p_imgs'],TRUE);
        ?>

        <a target="_blank" href="<?php echo SITE_FOLDER; ?>media/files/miniatures/big/post<?php echo @$post['p_id']; ?>.png">
            <img src="<?php echo SITE_FOLDER.$img[330]; ?>"/></a><br/>
<!--        <input type="submit" name="small" value=""<?php echo _lang('Изменить миниатюру'); ?>/>-->
        <input style="background-color:rgb(204, 0, 0);" type="submit" name="revomeImages" value="<?php echo _lang('Удалить'); ?>" />
        <?php
    }
//    unset($post);

    ?>
</p>
<div class="tab-box" role="tabpanel">
<ul class="nav nav-tabs" role="tablist">
    <?php foreach ($vars_array['langs'] as $lang) { ?>
        <li role="presentation" <?php echo $vars_array['default_lang'] == $lang['l_id'] ? "class='active'" : ""; ?> ><a href="#lang_<?php echo $lang['l_id']; ?>" aria-controls="lang_<?php echo $lang['l_id']; ?>" role="tab" data-toggle="tab"><?php echo $lang['l_title']; ?></a></li>
    <?php } ?>
</ul>

<div class="tab-content">
<?php foreach ($vars_array['langs'] as $key => $lang) { ?>
<div role="tabpanel" class="tab-pane <?php echo $vars_array['default_lang'] == $key ? "active" : ""; ?>" id="lang_<?php echo $key; ?>">
            <input style="display:none;" type="submit" name="postNewsubmit"/>
            <div style="float: right;width:322px;">
                <input class="btn btn-primary" type="submit" value="<?php echo _lang('Сохранить'); ?>" name="postNewsubmit" style="display: none;"/>
                <p>
                    <label><?php echo _lang('Категория'); ?>:</label>
                    <select name="postEdit[<?php echo $key; ?>][cat_id]" class="chosen-selectbox">
                        <?php echo $contTPL->catHierarchTree(@$list->category['posts'][$key],0,@$vars_array['posts'][$key]['p_cat_id']); ?>
                    </select>
                </p>
                

                <!-- Filter: Blog_post_Edit_Page_2 -->
                 <?php // echo _run_filter('Blog_post_Edit_Page_2','',$post); ?>
            </div>
            <div>
                <p>
                    <label><?php echo _lang('Заголовок'); ?></label> 
                    <input type="text" class="input-xlarge" name="postEdit[<?php echo $key; ?>][title]" value="<?php echo @$vars_array['posts'][$key]['p_title']; ?>"/>
                </p>
                <p>
                    <label><?php echo _lang('Артикул (латинськими)'); ?>:</label>
                    <input type="text" class="input-xlarge" name="postEdit[<?php echo $key; ?>][article]" value="<?php echo @$vars_array['posts'][$key]['p_article']; ?>"/>
                </p>
                
                <p>
                    <label><?php echo _lang('Превью текст'); ?>:</label>
                    <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="postEdit[<?php echo $key; ?>][preview]"><?php echo @$vars_array['posts'][$key]['p_preview']; ?></textarea>
                </p>
                
                <!-- Filter: Blog_post_Edit_Page_1 -->
                <?php //echo _run_filter('Blog_post_Edit_Page_1','',$post); ?>
                
            </div>
            <div class="clear"></div>
            <p>
                <label><?php echo _lang('Текст'); ?></label>
                <?php echo $admin->getWIZIWIG('postEdit['.$key.'][text]',@$vars_array['posts'][$key]['p_text'],'elm1'); ?>
            </p>
            <!-- Filter: Blog_post_Edit_Page_3 -->
                    <?php
                    $post['lang_id']=$lang['l_id'];
//                    $post['text']='';
                    $post['p_id']=@$vars_array['posts'][$key]['id'];
                    echo _run_filter('Blog_page_Edit_Page',@$post);
                    ?>
            

        
</div>
    <?php } ?>
    <input class="btn btn-primary button" type="submit" value="<?php echo _lang('Сохранить'); ?>" name="postNewsubmit"/>

    
    </div>
    </form>
    </div>