<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

global $admin, $contTPL, $posts, $list, $meta, $PData;

$i=0;
$text = '';

if(is_array($posts)){
    foreach($posts AS $cval){

        $i++;

        $text.='<tr >
            <td>
                <input type="checkbox" name="del_post['.$cval['p_id'].']"/>
                <a href="'.getURL('post',$cval['p_article']).'" target="_blank">'.$cval['p_id'].'</a>
            </td>
            <td>
                <a title="'._lang('Редактировать').'" href="'.getURL('admin','post-edit','id='.$cval['p_id']).'">
                    '.$cval['p_title'].'</a>
            </td>
            <td>
                '.$cval['c_title'].'
            </td>

            <td >
                <span class="grey">'._lang('Дата изменения').':</span>'.date(_lang('Y-m-d'),$cval['p_pablic_date']).'<br/>
                <!-- Filter: Blog_posts_table_additional_data -->
                '._run_filter('Blog_posts_table_additional_data').'
                <!-- // Filter: Blog_posts_table_additional_data -->
            </td>

        </tr>';

    }
    $text =
        '<form class="list" method="POST">
            <div class="table-holder">
            <table class="table">
              <thead>
                    <th class="numer">'._lang('Выбрать').'</th>
                    <th>'._lang('Заголовок').'</th>
                    <th>'._lang('Категория').'</th>
                    <th>'._lang('Доп. данные').'</th>
              </thead>
              <tbody>
                    '.$text.'
              </tbody>
            </table>
            </div>
            <input class="button" type="submit" value="'._lang('Удалить').'" name="postssubmit">
        </form>';

}else{
    $text = _lang('Не найдено! =\ ');
}

$categories_select_list = '';

if (isset($list->category[$PData->_GET('post_type')][$meta->val('md_lang')])) {
    $categories_select_list = $contTPL->catHierarchTree($list->category[$PData->_GET('post_type')][$meta->val('md_lang')], 0, $PData->_GET('filter_cat'));
}

?>
<form class="list">
                        <p><?php echo _lang('Поиск по заголовку'); ?>:</p>
                        <input type="hidden" name="admin" value="<?php echo $PData->_GET('admin'); ?>"/>
                        <input type="text" name="filter" value="<?php echo $PData->_GET('filter'); ?>"/>
                        <select name="filter_cat">
                            <option value="">- <?php echo _lang('Категория не выбрана'); ?> -</option>
                            <?php echo $categories_select_list; ?>
                        </select>

    <input type="submit" class="button" value="<?php echo _lang('Искать'); ?>"/>
                    <!-- Filter: Blog_posts_filter_form -->
                    <?php echo _run_filter('Blog_posts_filter_form'); ?>
                    <!-- // Filter: Blog_posts_filter_form -->

        </form>

<div class="line"></div>

    <div class="well">
        <?php echo $text; ?>
    </div>