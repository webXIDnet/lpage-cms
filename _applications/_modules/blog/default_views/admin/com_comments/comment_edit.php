<?php global $admin; ?>
<form class="list" method="POST">
    
    <p>
        <label><?php echo _lang('Юзер'); ?>:</label>
        <input type="text" class="input-xlarge" name="comm[username]" value="<?php echo $vars_array['pc_username']; ?>"/>
    </p>
    <p>
        <label><?php echo _lang('E-mail'); ?>:</label>
        <input type="text" class="input-xlarge" name="comm[email]" value="<?php echo $vars_array['pc_email']; ?>"/>
    </p>
    <p>
        <label><?php echo _lang('Дата'); ?>:</label>
        <input type="text" class="input-xlarge" name="comm[date]" value="<?php echo $vars_array['pc_date']; ?>"/>
    </p>
    <p>
        <label><input type="checkbox" <?php echo $vars_array['pc_status'] == '1' ? 'checked' : ''; ?> name="comm[status]" value="1"/> <?php echo _lang('Включен'); ?></label>        
    </p>
    <p>
        <label><?php echo _lang('Материал'); ?>:</label>
    </p>
    <p>
        <label><?php echo _lang('Текст'); ?></label>
        <?php echo $admin->getWIZIWIG('comm[comment]',_unprotect(htmlspecialchars($vars_array['pc_comment']),'textarea'),'elm1'); ?>
    </p>
    
    <input class="btn btn-primary button" type="submit" value="<?php echo _lang('Сохранить'); ?>" name="editComment"/>
</form>