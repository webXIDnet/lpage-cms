<form action="" method="post">
	<table class="table">
		<tr>
			<th></th>
			<th>ID</th>
			<th>Юзер</th>
			<th>Комментарий</th>
			<th>Материал</th>
			<th>Дата</th>
			<th>Статус</th>	
		</tr>
		<?php if ($vars_array['comments']) { ?>
			<?php foreach ($vars_array['comments'] as $comment) { ?>
			<tr>
				<td><input type="checkbox" name="del[]" value="<?php echo $comment['pc_id']; ?>"></td>
				<td><?php echo $comment['pc_id']; ?></td>
				<td><?php echo $comment['pc_username']; ?>, <?php echo $comment['pc_email']; ?></td>
				<td><a href="<?php echo getURL('admin', 'blog/comments/edit', 'id='.$comment['pc_id']); ?>"><?php echo $comment['pc_comment']; ?></a></td>
				<td><?php echo $comment['p_title']; ?></td>
				<td><?php echo $comment['pc_date']; ?></td>
				<td><?php echo $comment['pc_status'] == 1 ? _lang('Включен') : _lang('Отключен'); ?></td>	
			</tr>
			<?php } ?>
			<?php } else { ?>
			<tr>
				<td colspan=7><?php echo _lang('Нет комментариев'); ?></td>					
			</tr>
			<?php } ?>
	</table>
	<input type="submit" class="button" name="delete_comm" value="<?php echo _lang('Удалить'); ?>">
	<?php echo $vars_array['navigation']; ?>
</form>