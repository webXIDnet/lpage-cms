<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 08.07.2015
 * Time: 14:02
 */
/*
 * На в ході в шоткод має вказуватися ід категорії, і кількість позицій,
 * які туди потрібно виводити (якщо вказано 0 або не передано параметра,
 *  то виводити всі товари з категорії
 */

/**
 * @param $params - list of parameters, firs cat id, second number of product to show
 */
// [:POSTS_SC cat_id=2&count=3:]
function POSTS_SC($params)
{
    if(PAGE_TYPE=='admin') return '[:POSTS_SC '.$params.':]';
    global $sqlTPL, $PData, $sql, $products, $categories, $posts, $user;
    parse_str($params, $data);
    $cat_id = _protect($data['cat_id']);
    $count = _protect($data['count']);
    $vars_array['posts'] = $sql->query("
	SELECT *
	FROM _posts AS P
	INNER JOIN _categories AS C
	ON P.p_cat_id = C.c_id
	WHERE P.p_cat_id = '"._protect($cat_id)."' AND P.p_lang=" . _protect($user->lang_id) . " AND C.c_lang=" . _protect($user->lang_id) . "
	ORDER BY P.p_pablic_date DESC
	LIMIT  ".__protect($count));
    /*
     * Потрібно написати шоткод, який буде виводити пости на сторінку
     */

    //вызывать шаблон для вывода
    if ($cat_id == 18) {
        return $PData->getModTPL('site/display_news', 'blog', @$vars_array);
    } else {
        return $PData->getModTPL('site/display_posts', 'blog', @$vars_array);
    }
}
