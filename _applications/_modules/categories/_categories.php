<?php
require_once 'categories_class.php';
_add_action('defined_modules', 'categories_defined_modules', 1);

if (PAGE_TYPE == 'api') {
    require_once 'categories_api.php';
}

function categories_defined_modules()
{
    global $categories, $sql;
    $categories = new Categories();

    if (PAGE_TYPE == 'admin')
    {
        $sql->db_table_installer('Categories', '19.02.2015',
            array(
                '_categories' => array(
                    'id'                  => "`id` int(11) NOT NULL AUTO_INCREMENT",
                    'c_id'                => "`c_id` int(11) NOT NULL",
                    'c_parent_id'         => '`c_parent_id` int(11) DEFAULT 0',
                    'c_title'             => '`c_title` varchar(200) NOT NULL',
                    'c_desc'              => '`c_desc` text NOT NULL',
                    'c_url'               => '`c_url` varchar(200) NOT NULL',
                    'c_post_of_day_title' => '`c_post_of_day_title` text NOT NULL',
                    'c_option'            => '`c_option` text NOT NULL',
                    'c_type'              => '`c_type` varchar(20) NOT NULL',
                    'CREATE'              => "`id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`id`) "
                )
            )
        );
    }

}
