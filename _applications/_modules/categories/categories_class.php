<?php

class Categories
{
    public $prefix = 'c_';
    public $table = '_categories';

    function __construct($table = '', $prefix = '')
    {
        if ($prefix) {
            $this->prefix = $prefix;
        }
        if ($table) {
            $this->table = $table;
        }
    }

    public function get($params = '', $sort ='', $single = false)
    {

        global $sql;
        $result = '';
        $where = array();
        $where_str = '';
        if (is_array($params)) {
            foreach ($params as $param) {
                $where[] = $param;
            }
        } else if ($params != '') {

            $params = intval($params);
            if (!is_int($params)) throw new Exception('Parameter in product->get must be an INT!');
            else
            $where [] = $this->prefix."id=" . $params;
        }

        $where = array_diff($where,array(''));

        if (!empty($where)) {
            $where_str = "WHERE " . implode(' AND ' , $where);
        } else {
            $where_str = '';
        }
        $result_data = $sql->query("SELECT * FROM " . $this->table . " " . $where_str . " " . _protect($sort), 'array');
        $result = array();
        foreach ($result_data as $row) {
            $result[$row[$this->prefix.'id']] = $row;
        }
        if (!$single)
        return $result;
        else {
            $result = array_shift($result);
            return isset($result) ? $result : false;
        }
    }

    public function add($data, $type='lpage')
    {
        global $sql;

        $data['type'] = $type;

        return $sql->insert($this->table, $data, $this->prefix, false, true);
    }

    public function delete($ids)
    {
        global $sql;

        if (is_array($ids)) {
            foreach ($ids as $id) {
                $sql->update($this->table,$this->prefix.'parent_id="'.$id.'"',array('parent_id' => '0'),$this->prefix);
                $sql->delete($this->table, $this->prefix . 'id=' . $id, $this->prefix);
            }
        } else
        {
            $sql->update($this->table,$this->prefix.'parent_id="'.$ids.'"',array('parent_id' => '0'),$this->prefix);
            $sql->delete($this->table, $this->prefix . 'id=' . $ids, $this->prefix);
        }

        return true;
    }

    public function update($data)
    {
        global $sql;
        foreach ($data as $key => $val) {
            $sql->update($this->table, $this->prefix . "id='" . _protect($key) . "'", $val, $this->prefix);
        }
        return true;
    }
}
