<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модуль CRM системи для продажу MBA
 *
***/


/** Формування меню в адмінці  **/



_add_filter('adminPanel_sidebar_menu', 'CRM_system_admin_sidebar_menu');

function CRM_system_admin_sidebar_menu($result){

	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('CRM система'), //ім"я пункта меню
			'url' => FALSE, //посилання
			'submenu_filter' => 'CRM_system_admin_sidebar_menu', //хук для підключення пунктів підменю
			'active_pages' => _run_filter('CRM_system_admin_sidebar_menu_active_items', array('orders', 'clients', 'referrals', 'stat-orders', 'stat-lp', 'stat-meneger')), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => TRUE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_item','admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(2,3,4,5,6,100), $menu);

	return $result . @$menu;
}

/** Роутер Адмінки **/

_add_hook('adminPanel_controller', 'CRM_system_admin_controller');

function CRM_system_admin_controller(){

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL,
			$htmlTPL;

	switch(@$_GET['admin']){

		case'-----------------------------------':

			require_once __DIR__ . '/com_referrals/a_referrals.php';
			return TRUE;

		break;

	}
}

_add_action('defined_modules', 'CRM_system_defined_modules',3);

function CRM_system_defined_modules(){
	if(PAGE_TYPE=='admin'){
		global 	$meta,
				$sql;

		$module_db = $meta->val('apSet_CRM-system','mod_option');
		if($module_db){
			$sql->replace_str('_meta_datas', array('md_key','md_value'), 'apSet_CRM-system', 'apSet_crm-system');
		}
	}
}