<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Компонента відправки вхідних заявок на Bitrix24
 *  
***/

require_once __DIR__ . '/b24_fn.php';

$LData->addTranslations(array(
	'ua' => array(
		'Имя домена на B24'=>'Ім\'я користувача на B24',
		'Логин пользователя на B24' => 'Логін користувача на B24',
		'пользовать должен иметь право создавать лиды' => 'користувач має мати право створювати ліди',
		'Пароль пользователя на B24' => 'Пароль користувача на B24',
		'Метод отправки данных' => 'Метод надсилання даних',
		'Порт соединения с B24' => 'Порт з\'єднання з B24',
		'Страница взаимодействия с B24' => 'Сторінка взаємодії з B24',
	),
	'en' => array(
		'Имя домена на B24'=>'Domain name on B24',
		'Логин пользователя на B24' => 'User ID on the B24',
		'пользовать должен иметь право создавать лиды' => 'the user must have the rights to create leads ',
		'Пароль пользователя на B24' => 'Password of the B24',
		'Метод отправки данных' => 'The method of sending data',
		'Порт соединения с B24' => 'Connection port B24',
		'Страница взаимодействия с B24' => 'Interaction page with B24',
	)
));

/** Відправка даних по замовленню на Bitrix 24 **/
if(isset($_POST['ordersubmit']) || isset($_POST['formsubmit-add'])){
//	_add_filter('meta_datas_SQL_query', 'CRM_bitrix24_meta_datas_SQL_query');
	_add_action('ord_add_new_order', 'CRM_bitrix24_send_order');
}

if(PAGE_TYPE=='admin'){
	_add_filter('crm-system_admin-panel_mod_settings','CRM_bitrix24_adminpanel_mod_settings',15);
}

function CRM_bitrix24_send_order($order){

	$array = array();
		
	if(empty($order['title'])) $order['title'] = 'Order ID: ' . $order['o_id'];
	
	if(empty($order['name'])) $order['name'] = 'Client ID: ' . $order['cl_id'];
	
	if(!empty($order['lp_article']) && !_checkURL($order['lp_article'])) $order['lp_article'] = getURL('lp',$order['lp_article']);
	
	if(!_checkEmail(@$order['email']))$order['email'] = '';
	
	$order['price'] = preg_replace('/[^\d.]/','', str_replace(',','.',@$order['price']) );
	
	@$order['note'] = str_replace("\n", '<br/>', @$order['note']);
		
	$array = array(
		'TITLE' => @$order['title'],
		'OPPORTUNITY' => $order['price'],	//ціна товару
		'NAME' => @$order['name'],
		'PHONE_MOBILE' => @$order['phone'],
		'EMAIL_WORK' => @$order['email'],
		'SOURCE_DESCRIPTION' => 'mark: ' . @$order['mark'] . "\nLP: " . @$order['lp_article'], //мітка
		'COMMENTS' => _lang('Дата создания') . ': ' . date( _lang('d.m.Y H:i'), @$order['date_create'] ).'<br/><br/>'.$order['note'], // Додаткова інформація
	);
	
	b24_send_order($array);
}