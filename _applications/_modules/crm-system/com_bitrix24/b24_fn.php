<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Компонента відправки вхідних заявок на Bitrix24
 *  
***/

function b24_send_order($postData){
	
	global $meta;
	
	$set = $meta->val('apSet_crm-system','mod_option');
	
	if(!isset($set['b24']) || empty($set['b24']) || !is_array($set['b24']) || count($set['b24'])==0) return;
	
	$set = $set['b24'];
	
	
	
	// CRM server conection data
	//define('CRM_HOST', 'domain.bitrix24.ua'); // your CRM domain name
	//define('CRM_PORT', '443'); // CRM server port
	//define('CRM_PATH', '/crm/configs/import/lead.php'); // CRM server REST service path

	// CRM server authorization data
	//define('CRM_LOGIN', 'email@dom.net'); // login of a CRM user able to manage leads
	//define('CRM_PASSWORD', '12345678'); // password of a CRM user
	// OR you can send special authorization hash which is sent by server after first successful connection with login and password
	//define('CRM_AUTH', 'e54ec19f0c5f092ea11145b80f465e1a'); // authorization hash

	/********************************************************************************************/

	// get lead data from the form
/*	$postData = array(
		'TITLE' => $leadData['TITLE'],
		'NAME' => $leadData['NAME'],
		'PHONE_MOBILE' => $leadData['PHONE'],
		'EMAIL_WORK' => $leadData['EMAIL'],
		'OPPORTUNITY' => $leadData['PRICE'],
//		'UF_CRM_1410276156' => $leadData['CUSTOM'],
	);
	print_r($postData);
	*/
	// append authorization data

	$postData['LOGIN'] = $set['CRM_LOGIN'];
	$postData['PASSWORD'] = $set['CRM_PASSWORD'];


	// open socket to CRM
	 
	if($set['send_method']=='fsockopen'){
		$fp = @fsockopen("ssl://".$set['CRM_HOST'], $set['CRM_PORT'], $errno, $errstr, 30);
	}else{
		$fp = @stream_socket_client("ssl://".$set['CRM_HOST'].":".$set['CRM_PORT'], $errno, $errstr, 30);
	}
	
	if($fp){
		
		// prepare POST data
		$strPostData = '';
		
		foreach ($postData as $key => $value)
			$strPostData .= ($strPostData == '' ? '' : '&').$key.'='.urlencode($value);

		// prepare POST headers
		$str = "POST ".$set['CRM_PATH']." HTTP/1.0\r\n";
		$str .= "Host: ".$set['CRM_HOST']."\r\n";
		$str .= "Content-Type: application/x-www-form-urlencoded\r\n";
		$str .= "Content-Length: ".strlen($strPostData)."\r\n";
		$str .= "Connection: close\r\n\r\n";

		$str .= $strPostData;

		// send POST to CRM
		fwrite($fp, $str);

		// get CRM headers
		$result = '';
		
		while (!feof($fp)){
			$result .= fgets($fp, 128);
		}
		
		fclose($fp);

		// cut response headers
		$response = explode("\r\n\r\n", $result);

		
				
		if(str_replace("'error':'201'",'',$response[1])==$response[1]){
			_eMail($meta->val('contact_email'), _lang('ERROR - Заявка не добавлена на Bitrix24'),print_r($response[1], TRUE));
		}else{
			//echo $output = '<pre>'.print_r($response[1], 1).'</pre>';
		}
	}else{
		_eMail($meta->val('contact_email'), _lang('ERROR - Заявка не добавлена на Bitrix24'),print_r($postData, TRUE));
	//	echo 'Connection Failed! '.$errstr.' ('.$errno.')';
	}

}

function CRM_bitrix24_adminpanel_mod_settings($result,$set){
	
	global $meta;
	$access = $bookmarks_select = $status_select = '';
	
	
	$result['title'] .= 
		'
		<li role="presentation">
		    <a href="#tab_ref_b24" aria-controls="b24" role="tab" data-toggle="tab">'._lang('API Bitrix24').'</a>
		</li>
		';

	
	$val = @$set['b24'];
	
	if(empty($val['CRM_PORT'])) $val['CRM_PORT'] = '443';
	if(empty($val['CRM_PATH'])) $val['CRM_PATH'] = '/crm/configs/import/lead.php';
	
	$result['text'] .= 
		'<div role="tabpanel" class="tab-pane fade" id="tab_ref_b24">
<form>
			<p>
				<label>
					<b>'._lang('Имя домена на B24').'</b> (mybomain.bitrix24.ru)<br/>
					<input class="input" type="text" name="modSet[apSet_crm-system][b24][CRM_HOST]" value="'.@$val['CRM_HOST'].'"/>
				</label>
			</p>
			<p>
				<label>
					<b>'._lang('Логин пользователя на B24').'</b> ('._lang('пользовать должен иметь право создавать лиды').')<br/>
					<input class="input" type="email" name="modSet[apSet_crm-system][b24][CRM_LOGIN]" value="'.@$val['CRM_LOGIN'].'"/>
				</label>
			</p>
			<p>
				<label>
					<b>'._lang('Пароль пользователя на B24').'</b><br/>
					<input class="input" type="password" name="modSet[apSet_crm-system][b24][CRM_PASSWORD]" value="'.@$val['CRM_PASSWORD'].'"/>
				</label>
			</p>
			<p>
				<label>
					<b>'._lang('Метод отправки данных').'</b><br/>
					<select name="modSet[apSet_crm-system][b24][send_method]">
					
						<option value="stream_socket_client" '.
							(@$val['send_method']=='stream_socket_client'?'selected="selected"':'').
						'>'._lang('stream_socket_client').'</option>
						
						<option value="fsockopen" '.
							(@$val['send_method']=='fsockopen'?'selected="selected"':'').
						'>'._lang('fsockopen').'</option>
						
					</select>
				</label>
			</p>
			<p>
				<label>
					<b>'._lang('Порт соединения с B24').'</b><br/>
					<input class="input" type="text" name="modSet[apSet_crm-system][b24][CRM_PORT]" value="'.@$val['CRM_PORT'].'"/>
				</label>
			</p>
			<p>
				<label>
					<b>'._lang('Страница взаимодействия с B24').'</b><br/>
					<input class="input" type="text" name="modSet[apSet_crm-system][b24][CRM_PATH]" value="'.@$val['CRM_PATH'].'"/>
				</label>
			</p>
			<input type="submit" class="button" value="'._lang('Сохранить').'"/>
			</form>
		</div>';
    //todo доделать кнопку сохранения
	return $result;
}

function CRM_bitrix24_meta_datas_SQL_query($result,$vars){
	
	if(!empty($result)) $result .= ' OR ';
	
	$result .= ' ';
	
}