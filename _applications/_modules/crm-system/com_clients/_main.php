<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента "Клієнти" для CRM системи
 *
***/

require_once __DIR__ . '/cl_fn.php';

_add_action('defined_modules', 'CRM_order_com_client_defined_modules');

function CRM_order_com_client_defined_modules(){
	if(PAGE_TYPE=='admin'){
		global $sql;

		$result = $sql->db_table_installer('CRM-system_Clients','12.09.2015',
			array(
				'_clients' => array(
					'cl_id' => "`cl_id` int(11) NOT NULL AUTO_INCREMENT",
					'cl_fullname' => "`cl_fullname` varchar(256) NOT NULL",
					'cl_tel' => "`cl_tel` varchar(25) NOT NULL",
					'cl_email' => "`cl_email` varchar(100) NOT NULL",
					'cl_attachment' => "`cl_attachment` text NOT NULL",
					'cl_adress' => "`cl_adress` varchar(512) NOT NULL",
					'cl_date_update' => "`cl_date_update` int(11) NOT NULL",
					'cl_brawser_lang' => "`cl_brawser_lang` varchar(5) NOT NULL DEFAULT 'NAN'",
					'cl_additional_data' => "`cl_additional_data` text NOT NULL",
					'CREATE' => "`cl_id` int(11) NOT NULL AUTO_INCREMENT, `cl_tel` varchar(25) NOT NULL, `cl_email` varchar(100) NOT NULL, PRIMARY KEY (`cl_id`),  UNIQUE KEY `cl_tel` (`cl_tel`),  UNIQUE KEY `cl_email` (`cl_email`)"
				)
			)
		);
	}
}

if(PAGE_TYPE=='admin'){

	_add_filter('CRM_system_admin_sidebar_menu', 'CRM_system_com_client_admin_sidebar_menu', 11);//віведення пунктів меню
	_add_hook('adminPanel_controller', 'CRM_order_MBA_com_client_admin_controller');//модифікація роутера адмінки

	function CRM_system_com_client_admin_sidebar_menu($result){

		global $PData;

	    $vars_array = array(
	        array(
	            'title' => _lang('Клиенты'), //ім"я пункта меню
	            'url' => getURL('admin','clients'), //посилання
	            'active_pages' => array('clients'), //на яких сторінках має бути активний (розгорнутий)
	            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
	        ),
	    );

	    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

	    $menu = u_ifRights(array(3,4,5,6,100), $menu);

	    return $result . $menu;
	}

	function CRM_order_MBA_com_client_admin_controller(){

		global 	$PData,
				$admin,
				$user,
				$sql,
				$sqlTPL,
				$contTPL,
				$htmlTPL;

		switch(@$_GET['admin']){

			case'clients':

				require_once __DIR__ . '/apps_admin/a_clients.php';
				return TRUE;
			break;

		}
	}
}