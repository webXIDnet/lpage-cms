<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента статистики для CRM системи
 *
***/


if(isset($_POST['delSubmit']) && isset($_POST['itemID']) && is_array($_POST['itemID'])){

	$where = $where1 = '';
	$att_array = array();

	foreach($_POST['itemID'] AS $key=>$val){

		if(!empty($where)){

			$where1 .= ' OR ';
			$where .= ' OR ';
		}

		if(!empty($_POST['editClient'][$key]['attachment'])){
			$atts = json_decode(_strCode($_POST['editClient'][$key]['attachment'],FALSE,FALSE), TRUE);
			if(is_array($atts)){
				foreach($atts AS $att){
					$att_array[] = $att;
				}
			}
		}

		$where1 .= "o_cl_id='"._protect($key)."'";
		$where .= "cl_id='"._protect($key)."'";

	}

	if(!empty($where)){

		$sql->delete('_orders',$where1);
		$sql->delete('_clients',$where);

		if(!empty($att_array) && is_array($att_array)){
			foreach($att_array AS $att){

				if(_checkURL($att))
					unlink(str_replace(HOMEPAGE,CODE_FOLDER.'../',$att));
			}
		}

		$PData->content('Запись удалена','message',TRUE);

	}else{

		$PData->content(
			'Произошел сбой алгоритма. Запись небыла удалена.
			<br/>Код ошибки: <b>#del_clients</b>',
			'message'
		);

	}

}elseif(isset($_POST['newCl'])){

	if(!empty($_POST['newCl']['fullname']) && !empty($_POST['newCl']['tel'])){

		$array = array(
			'fullname' => @$_POST['newCl']['fullname'],
			'tel' => @$_POST['newCl']['tel'],
			'email' => @$_POST['newCl']['email'],
			'date_update' => time(),
			'brawser_lang' => 'NAN'
		);

		if(!isset($_POST['refrash_cl_date'])) unset($array['date_update']);

		$sql->insert('_clients',$array,'cl_',TRUE);

		$PData->redirect(getURL('admin','clients','search='.urlencode($_POST['newCl']['tel']).'&find_tel=true'),'');

	}else{
		$PData->content(
			'Нужно заполнить все поля с *',
			'message'
		);
	}

}elseif(isset($_POST['formsubmit-edit']) && is_array($_POST['editClient'])){

	$cl_id = '';

	foreach($_POST['editClient'] AS $key => $val){
		$cl_id = $key;
		break;
	}

	$_POST['editClient'][$cl_id]['additional_data'] = json_encode($_POST['editClient'][$cl_id]['additional_data']);


	$newImgURL = $attachment = array();

	$att_array = _strCode($_POST['editClient'][$cl_id]['attachment'],FALSE);

	foreach($_FILES['images']['error'] AS $i => $error){

		$att_array[$i] = @$att_array[$i];

		if($error!=0)continue;

		$newImgURL[$i] = _getRandStr();

		while(_checkURL(HOMEPAGE.'media/clients-docs/'.$newImgURL[$i].'.png')){

			$newImgURL[$i] = _getRandStr();

		}

		writeImg( $newImgURL[$i], 'images', $i, 'real_size', CODE_FOLDER.'../media/clients-docs/');


		if(_checkURL($att_array[$i])){

			unlink(str_replace(HOMEPAGE,CODE_FOLDER.'../',$att_array[$i]));
		}

		$attachment[$i] = HOMEPAGE . 'media/clients-docs/' . $newImgURL[$i] . '.png';
	}

	if(is_array($attachment) && !empty($attachment)){

		foreach($attachment AS $i => $val){
			$att_array[$i] = $attachment[$i];
		}
	}

	$_POST['editClient'][$cl_id]['attachment'] = json_encode($att_array);

	$sql->update('_clients',"cl_id='"._protect($cl_id)."'",$_POST['editClient'][$cl_id],'cl_');

	$PData->content('Запись Сохранена','message',TRUE);

}

/** Пагінація **/

$parent_select = $text = $where = '';

if(!empty($_GET['search'])){

	if(!empty($_GET['find_tel'])){
		$where = " cl_tel LIKE '%"._protect($_GET['search'])."'";
	}else{
		$where = " cl_fullname LIKE '%"._protect($_GET['search'])."%'
		OR
			cl_email LIKE '%"._protect($_GET['search'])."%'
		OR
			cl_tel LIKE '%"._protect($_GET['search'])."%'";
	}

}

if(!empty($where)) $where = 'WHERE '.$where;

$nav = _pagination(
	'_clients',
	'cl_id',
	$where,
	getURL('admin','clients','search='.@$_GET['search']));


/** Формування основного списку категорій **/

$result = $sql->query("
	SELECT *
	FROM _clients
	{$where}
	ORDER BY cl_fullname ASC
	".@$nav['limit']."
");

if(!is_array($result) || empty($result)){

	$text = '<b>Клиентов нет</b><br/> =(';

}else{

	foreach($result AS $cval){

		$cl_id = $cval['cl_id'];

		$attachment = array('','','','');
		$att_count = 0;
		$att_array = json_decode($cval['cl_attachment'],TRUE);

		if(is_array($att_array)){
			foreach($att_array AS $key=>$att){

				$attachment[$key] = '';

				if(_checkURL($att)){
					$attachment[$key] = '<a href="'.$att.'" target="_blank"><img style="max-width:300px;max-height:300px" src="'.$att.'"/></a><br/>';
					$att_count++;
				}

			}
		}

		$text .=
			'<tr>
				<td id="tableList0" name="o_id" class=" col0 tableList0">
					<label>
						<input type="checkbox" name="itemID['.$cl_id.']">
						<span title="'._lang('Внутренний номер заказа').'">'.$cl_id.'</span>
					</label>'.
					cl_clients_editing_form($cval,$attachment).'
			</td>

				<td id="tableList1" name="o_id" class=" col col1 tableList1" style="color:#888">

					<b style="color:#000">'.$cval['cl_fullname'].'</b><br/>

				</td>
				<td id="tableList2" name="o_prod" class=" col col2 tableList2" style="color:#888">
					'._lang('Тел').': <span style="color:#000">'.$cval['cl_tel'].'</span><br/>
					'._lang('Email').': <span style="color:#000">'.$cval['cl_email'].'</span>
				</td>
				<td id="tableList3" name="o_lp_id" class=" col col3 tableList3" style="color:#888">
					'._lang('Паспорт').': <span style="color:'.($att_count<4?'red':'#000').'">'.$att_count.'/4</span><br/>

					'._lang('Адрес').': <span style="color:'.(!empty($cval['cl_adress'])?'#000">'._lang('заполнен').'':'red">'._lang('не указан').'').'</span>
				</td>

			</tr>';
	}

	$text = '
	<form method="POST">
		<table id="tableEdit" class="table">
			<tbody>
				<tr>
					<th id="head-tableList" class="head head0 tableList0">'._lang('Выбрать').'</th>
					<th id="head-tableList" class="head head1 tableList0">'._lang('ФИО').'</th>
					<th id="head-tableList" class="head head2 tableList1">'._lang('Контакты').'</th>
					<th id="head-tableList" class="head head3 tableList2">'._lang('Примечание').'</th>

				</tr>
				'.$text.'
			</tbody>
		</table>

		<input type="submit" name="delSubmit" value="'._lang('Удалить').'"/>
	</form>
	<p class="greyText">
		! - '._lang('удаляя Клиента Вы удалите все его заказы').'
	</p>
	';
}

/** Формування основного HTML коду **/

$PData->content('Клиент','title');

$_GET['tab'] = 'search';
if(!empty($_POST['tab'])) $_GET['tab'] = 'addClient';


$PData->content('

<div role="tabpanel" class="tab-box">

      <!-- tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#search" aria-controls="home" role="tab" data-toggle="tab">Поиск</a>
        </li>
        <li role="presentation">
            <a href="#clients" aria-controls="profile" role="tab" data-toggle="tab">+ Добавить клиента</a>
        </li>
      </ul>

      <!-- panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="search">
          <form method="">
            <p><b>Поиск:</b></p>
            <input placeholder="ФИО или телефон, или email" type="text" name="search" value="">
            <label>
              <input type="checkbox" name="find_tel" value="true"><span class="grey"> - Искать по последним цифрам телефона (количество не имеет значения)</span>
            </label>
            <br>
            <input type="submit" class="button" value="Искать">
          </form>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="clients">
          <form method="POST">
            <input placeholder="ФИО *" type="text" name="newCl[fullname]" value="">
            <input placeholder="Телефон *" type="text" name="newCl[tel]" value="">
            <input placeholder="Email" type="text" name="newCl[email]" value="">
            <input type="submit" class="button" value="Добавить">
          </form>
        </div>
      </div>
    </div>


	<div class="tabs_block">
		<ul class="tabs_list">
			<li switchTab="tab_0" class="'._getActive($_GET['tab'],'search').'"><a>'._lang('Поиск').'</a></li>
			<li switchTab="tab_1" class="'._getActive($_GET['tab'],'addClient').'"><a>+ '._lang('Добавить Клиента').'</a></li>
		</ul>
		<div id="tab_0" class="tab_box '._getActive($_GET['tab'],'search').'">
			<!-- Пошук -->
			<form class="list">
				<input type="hidden" name="tab" value="search"/>
				<label><b>'._lang('Поиск').'</b>:</label>
				<input placeholder="'._lang('ФИО или телефон, или email').'" type="text" name="search" value="'.@$_GET['search'].'"/>
				<p>
					<label>
						<input type="checkbox" '.(@$_GET['find_tel']=='true'?'checked="checked"':'').' name="find_tel" value="true"/><span class="greyText"> - '._lang('Искать по последним цифрам телефона (количество не имеет значения)').'</span>
					</label>
				</p>
				<input type="submit" class="btn" value="'._lang('Искать').'"/>
			</form>

			<!-- // Пошук -->
		</div>
		<div id="tab_1" class="tab_box '._getActive(@$_GET['tab'],'addClient').'">
			<form class="list" method="POST">
				<input type="hidden" name="tab" value="addClient"/>
				<input placeholder="'._lang('ФИО').' *" type="text" name="newCl[fullname]" value="'.@$_POST['newCl']['fullname'].'"/>
				<input placeholder="'._lang('Телефон').' *" type="text" name="newCl[tel]" value="'.@$_POST['newCl']['tel'].'"/>
				<input placeholder="'._lang('Email').'" type="text" name="newCl[email]" value="'.@$_POST['newCl']['email'].'"/>

				<br/>
				<input type="submit" class="btn" value="'._lang('Добавить').'"/>
			</form>
		</div>


	</div>


	<hr/>
	<br/>
	<div class="well">
	    '.$text.'
	</div>

'.$nav['html']);