<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Функції компоненти "Клієнти" Модуля "CRM система"
 *  
***/

function cl_brawser_lang($val=''){
	
	$array=array(
		'NAN'=>_lang('не извесно'),
		'uk'=>_lang('украинский'),
		'ru'=>_lang('русский'),
		'en'=>_lang('английский'),
	);
	
	if(isset($array[$val]))
		return $array[$val];
	elseif(!$val)
		return $array;
	else
		return $val;
}

function cl_clients_editing_form($cval,$attachment){
	
	if(empty($cval))return '';
	
	$cl_id = $cval['cl_id'];
	$lang_select = '';
	
	foreach(cl_brawser_lang() AS $key => $payment){
		
		$selected = '';
		if($key==$cval['cl_brawser_lang'])$selected = 'selected="selected"';
		
		$lang_select .= '<option '.$selected.' value="'.$key.'">'.$payment.'</option>';
		
	}
	
	/* Опрацювання додаткових даних клієнта */
	$cl_additional_data_array = json_decode($cval['cl_additional_data'],TRUE);
	$cl_additional_data = '';
	
	if(is_array($cl_additional_data_array)){
		foreach($cl_additional_data_array AS $key=>$val){
			$cl_additional_data .= 
			'<p>
				<label>
					<b>'._lang($key).'</b>:<br>
					<input class="input" type="text" name="editClient['.$cl_id.'][additional_data]['.$key.']" value="'.$val.'"/>
				</label>
			</p>';
		}
	}
	
	$cl_additional_data = _run_filter('editing_form_cl_additional_data',$cl_additional_data,$cval);
		

	$text = '
		<div class="tableEdit_form">
					
			<h2 class="h1" style="text-align:center;">'._lang('Клиент').'</h2>
			
			<div class="right_colmn">
				<p>	
					<label>
						<b>'._lang('Язык').'</b>: <span style="color: #888;">(по умолчанию - данные браузера клиента)</span><br>
						<select style=" height: auto;" class="input" name="editClient['.$cl_id.'][brawser_lang]">
							'.$lang_select.'
						</select>
					</label>
				</p>
				<input class="input" type="hidden" name="editClient['.$cl_id.'][attachment]" value="'._strCode($cval['cl_attachment'],TRUE,FALSE).'"/>
				<p>	
					<label>
						<b>'._lang('Паспорт - Страница 1').'</b>:<br>
						'.$attachment[0].'
						<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
					</label>
				</p>
				<p>	
					<label>
						<b>'._lang('Паспорт - Страница 2').'</b>:<br>
						'.$attachment[1].'
						<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
					</label>
				</p>
				<p>	
					<label>
						<b>'._lang('Паспорт - Прописка').'</b>:<br>
						'.$attachment[2].'
						<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
					</label>
				</p>
				<p>	
					<label>
						<b>'._lang('ИНН').'</b>:<br>
						'.$attachment[3].'
						<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
					</label>
				</p>
			</div>
			<p>	
				<label>
					<b>'._lang('ФИО').'</b>:<br>
					<input class="input" type="text" name="editClient['.$cl_id.'][fullname]" value="'.$cval['cl_fullname'].'"/>
				</label>
			</p>
			<p>	
				<label>
					<b>'._lang('Тел').'</b>:<br>
					<input class="input" type="text" name="editClient['.$cl_id.'][tel]" value="'.$cval['cl_tel'].'"/>
				</label>
			</p>
			<p>	
				<label>
					<b>'._lang('Email').'</b>:<br>
					<input class="input" type="text" name="editClient['.$cl_id.'][email]" value="'.$cval['cl_email'].'"/>
				</label>
			</p>
			<p>	
				<label>
					<b>'._lang('Примечание').'</b>:<br>
					<textarea style="height: 80px;" class="input" name="editClient['.$cl_id.'][adress]">'.$cval['cl_adress'].'</textarea>
				</label>
			</p>
			'.$cl_additional_data.'
			<p>	
				<label>
					'._lang('Данные клиента обновлялись').':
					<b>'.date('d.m.Y H:i',$cval['cl_date_update']).'</b>
				</label>
			</p>
			<p>	
				<label class="greyText">
					<input type="checkbox" name="refrash_cl_date" value="true" > '._lang('<b class="blackText">Обновить дату</b> актуальности данных клиента').'
				</label>
			</p>
			<div class="clear"></div>
		</div>';
	return $text;
}