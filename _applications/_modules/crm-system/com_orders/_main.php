<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента Опрацювання замовлень для CRM системи
 *
 ***/

require_once __DIR__ . '/ord_fn.php';



$LData->addTranslations(array(
    'ua' => array(
        '(по умолчанию - данные из браузера клиента)'=>'За замовчуванням - дані з браузера клієнта',
        'Название заказа' => 'Замовлення',
        'Дата выполнения заказа' => 'Дата виконання замовлення',
        'Дата выполнения'=>'Дата виконання',
        'Дата следующего контакта'=>'Дата наступного контакту',
        'Дата, когда нужно сконтактироваться с клиентом в следующий раз'=>'Дата, коли потрібно зв\'язатися з клієнтом',
        'Связаться'=>'Зв\'язатися'
    )
));


_add_action('defined_modules', 'CRM_order_defined_modules');

function CRM_order_defined_modules(){
    if(PAGE_TYPE=='admin'){
        global $sql;

        $sql->db_table_installer('CRM-system_Orders','05.09.2014',
            array(
                '_orders' => array(
                    'o_id' => "`o_id` int(11) NOT NULL AUTO_INCREMENT",
                    'o_prod' => "`o_prod` varchar(256) NOT NULL",
                    'o_lp_article' => "`o_lp_article` varchar(500) NOT NULL",
                    'o_price' => "`o_price` varchar(256) NOT NULL",
                    'o_date_create' => "`o_date_create` int(11) NOT NULL",
                    'o_date_done' => "`o_date_done` int(11) NOT NULL",
                    'o_date_next_touch' => "`o_date_next_touch` int(11) NOT NULL",
                    'o_status' => "`o_status` varchar(10) NOT NULL",
                    'o_mark' => "`o_mark` varchar(50) NOT NULL",
                    'o_payment_type' => "`o_payment_type` int(1) NOT NULL",
                    'o_date_group_start' => "`o_date_group_start` int(11) NOT NULL",
                    'o_note' => "`o_note` text NOT NULL",
                    'o_payment_note' => "`o_payment_note` text NOT NULL",
                    'o_cl_id' => "`o_cl_id` int(11) NOT NULL",
                    'o_u_id' => "`o_u_id` int(11) NOT NULL",
                    'o_log' => "`o_log` text NOT NULL",
                    'o_additional_data' => "`o_additional_data` text NOT NULL",
                    'CREATE' => "`o_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`o_id`)"
                )
            )
        );
    }
}

/** Формує підпункти розділу "CRM система" меню адмінки у правій колонці **/

_add_filter('CRM_system_admin_sidebar_menu', 'CRM_system_com_orders_admin_sidebar_menu');

function CRM_system_com_orders_admin_sidebar_menu($result){

    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Очередь заявок'), //ім"я пункта меню
            'url' => getURL('admin', 'orders', ord_get_admin_homepage()), //посилання
            'active_pages' => array('orders'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(2,3,4,5,6,100), $menu);

    return $result . $menu;
}


/** Роутер Адмінки **/

_add_hook('adminPanel_controller', 'CRM_order_MBA_orders_admin_controller');

function CRM_order_MBA_orders_admin_controller(){

    global 	$PData,
              $admin,
              $user,
              $sql,
              $sqlTPL,
              $contTPL,
              $htmlTPL;

    switch(@$_GET['admin']){
        case'orders':
            require_once __DIR__ . '/apps_admin/a_orders.php';
            return TRUE;
            break;
        case'order-edit':
            require_once __DIR__ . '/apps_admin/a_order_edit.php';
            return TRUE;
            break;
        case'orders-archive':
            require_once __DIR__ . '/apps_admin/a_orders_archive.php';
            return TRUE;
            break;
        case'':
            if(u_ifRights(array(3,4,5,6,100),TRUE)){
                $PData->redirect(getURL('admin','orders',ord_get_admin_homepage()));
            }
            break;
    }
}


/** Запис в БД даних про нове замовлення **/

_add_action('LPage_init_done', 'CRM_order_MBA_orders_write_db');
_add_action('page_init', 'CRM_order_MBA_orders_write_db');
_add_action('post_init', 'CRM_order_MBA_orders_write_db');

function CRM_order_MBA_orders_write_db(){

    if(isset($_POST['ordersubmit'])){

        global	$sql, $order_set;

        $time = time();

        if(empty($_POST['rf_phone'])){
            $_POST['rf_phone'] = 'NON_'.$time;
        }

        if(empty($_POST['rf_email'])){
            $_POST['rf_email'] = 'NON_'.$time;
        }

        $cl_ar = array(
            'fullname' => @$_POST['rf_name'],
            'tel' => @$_POST['rf_phone'],
            'email' => @$_POST['rf_email'],
            'date_update' => $time,
            'brawser_lang' => substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2),
        );

        if(!empty($_POST['rf_adress'])) $cl_ar['adress'] = $_POST['rf_adress'];

        $cl_id = $sql->insert('_clients', $cl_ar, 'cl_', TRUE, TRUE);

        if(empty($cl_id)){
            $cval = $sql->query(
                "SELECT cl_id
				FROM _clients
				WHERE cl_tel='"._protect($_POST['rf_phone'])."' || cl_email='"._protect($_POST['rf_email'])."';
				",
                'value'
            );
            $cl_id = $cval['cl_id'];
        }

        if(!empty($_POST['rf_phone']) || !empty($_POST['rf_email'])){

            $set = _strCode(@$_GET['set'],FALSE);

            if(is_array($set)){

                $article = @$set['lp_article'];
                $prod = @$set['prod_title'];
                $price = @$set['prod_price'];

            }else{
                if (isset($_REQUEST['prod_lp'])){
                    $article = getURL('lp',$_REQUEST['prod_lp']);
                }elseif (isset($_GET['lp'])){
                    $article = getURL('lp',$_GET['lp']);
                }else{
                    $article = PAGE_URL;
                }
            }

            if(isset($_POST['prod_title'])) $prod = $_POST['prod_title'];
            if(isset($_POST['prod_price'])) $price = $_POST['prod_price'];

            $set['rf_name']=@$_POST['rf_name'];
            $set['rf_phone']=@$_POST['rf_phone'];
            $set['rf_email']=@$_POST['rf_email'];

        }else{
            global $PData;
            $PData->redirect(HOMEPAGE);
        }

        $note = '';

        if(isset($_POST['field'])){
            if(is_array($_POST['field'])){
                foreach($_POST['field'] AS $key=>$val){
                    if(!empty($note)) $note .= "\n";
                    $note .= $key.': '.$val;
                }
            }else{
                $note = $_POST['field'];
            }
        }

        $ord_ar = array(
            'lp_article' => $article,
            'prod' => @$prod . ' *',
            'price' => @$price . ' *',
            'date_create' => $time,
            'mark' => @$_COOKIE['mark'],
            'note' => @$note,
            'cl_id' => $cl_id,
        );

		$ord_ar = _run_filter('CRM_order_orders_write_db','',$ord_ar);

        if(isset($_POST['order_status'])) $ord_ar['status'] = (int)$_POST['order_status'];

        $order_set['o_id'] = $sql->insert('_orders',$ord_ar,'o_',FALSE,TRUE);

        _run_action('ord_add_new_order',
            array(
                'o_id' => $order_set['o_id'],
                'cl_id' => $cl_id,

                'phone' => @$_POST['rf_phone'],
                'email' => @$_POST['rf_email'],
                'name' => @$_POST['rf_name'],
                'title' => @$prod,
                'price' => @$price,
                'mark' => @$_COOKIE['mark'],
                'lp_article' => $article,
                'date_create' => $time,
                'note' => $note
            )
        );

        if(!empty($_GET['set'])){
            $set = _strCode($_GET['set'],FALSE);
            $set['o_id'] = $order_set['o_id'];
        }else{
            $set = $order_set;
        }

        $_GET['set'] = _strCode($set);
        return $order_set;
    }else{
        //print_r($_POST);
    }

}