<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента Опрацювання замовлень для CRM системи
 *
***/



if(isset($_POST['delSubmit']) && isset($_POST['itemID']) && is_array($_POST['itemID'])){

	$where = '';

	foreach($_POST['itemID'] AS $key=>$val){

		if(!empty($where)){

			$where .= ' OR ';

		}

		$where .= "o_id='"._protect($key)."'";
	}

	if(!empty($where)){

		$sql->delete('_orders',$where);

		$PData->content('Запись удалена','message',TRUE);

	}else{

		$PData->content('Произошел сбой алгоритма. Запись небыла удалена. Обратитесь пожалуйста к разработчикам.','message');

	}

}elseif(isset($_POST['not_valideSubmit']) && isset($_POST['itemID']) && is_array($_POST['itemID'])){

	$where = '';

	foreach($_POST['itemID'] AS $key=>$val){

		if(!empty($where)){

			$where .= ' OR ';

		}

		$where .= "o_id='"._protect($key)."'";

	}

	if(!empty($where)){

		$sql->update('_orders',$where,array('o_status'=>-1));

		$PData->content('Изменения сохранены','message',TRUE);

	}else{

		$PData->content('Произошел сбой алгоритма. Изменения небыли внесены. Обратитесь пожалуйста к разработчикам.','message');

	}

}elseif(isset($_POST['formsubmit-add']) && is_array($_POST['addOrder']) && is_array($_POST['addClient'])){

	$array_requires = 1;

	if(!empty($_POST['addClient']['tel']) || !empty($_POST['addClient']['email'])){
		if(empty($_POST['addClient']['tel'])){
			$_POST['addClient']['tel'] = 'NON_'.time();
		}
		if(empty($_POST['addClient']['email'])){
			$_POST['addClient']['email'] = 'NON_'.time();
		}
	}

	if(!empty($_POST['addOrder']['date_next_touch']) || !empty($_POST['addOrder']['date_next_touch_time'])){
		$_POST['addOrder']['date_next_touch'] = strtotime($_POST['addOrder']['date_next_touch'].' '.$_POST['addOrder']['date_next_touch_time']);
	}
	unset($_POST['addOrder']['date_next_touch_time']);


	if(!empty($_POST['addOrder']['date_create']) || !empty($_POST['addOrder']['date_create_time'])){
		$_POST['addOrder']['date_create'] = strtotime($_POST['addOrder']['date_create'].' '.$_POST['addOrder']['date_create_time']);
	}else{
		$_POST['addOrder']['date_create'] = time();
	}
	unset($_POST['addOrder']['date_create_time']);


	$Order_requires = array('prod','price','additional_data');
	$Client_requires = array('fullname','tel','email');//,'brawser_lang'

	foreach($Order_requires AS $val){
		if($val=='additional_data'){
			foreach($_POST['addOrder']['additional_data'] AS $v){
				if(empty($v)){
					$array_requires='';
				}
			}
		}elseif(empty($_POST['addOrder'][$val])){
			$array_requires='';
		}
	}

	foreach($Client_requires AS $val){
		if(empty($_POST['addClient'][$val])){
			$array_requires='';
		}
	}

	if($array_requires==1 || 1==1){

		$_POST['addOrder']['additional_data'] = json_encode(@$_POST['addOrder']['additional_data']);
		$_POST['addClient']['additional_data'] = json_encode(@$_POST['addClient']['additional_data']);

		$cl_id = $sql->insert('_clients',$_POST['addClient'],'cl_',TRUE,TRUE);

		if(empty($cl_id)){
			$cval = $sql->query(
				"SELECT cl_id
				FROM _clients
				WHERE cl_tel='"._protect($_POST['addClient']['tel'])."' || cl_email='"._protect($_POST['addClient']['email'])."';
				",
				'value'
			);
			$cl_id = $cval['cl_id'];
		}

		$_POST['addOrder']['cl_id'] = $cl_id;



		$o_id = $sql->insert('_orders',$_POST['addOrder'],'o_',FALSE,TRUE);

		_run_action('ord_add_new_order',
			array(
				'o_id' => $o_id,
				'cl_id' => $cl_id,

				'phone' => @$_POST['addClient']['tel'],
				'email' => @$_POST['addClient']['email'],
				'name' => @$_POST['addClient']['fullname'],
				'title' => @$_POST['addOrder']['prod'],
				'price' => @$_POST['addOrder']['price'],
				'mark' => @$_POST['addOrder']['mark'],
				'lp_article' => 'added by manager',
				'date_create' => $_POST['addOrder']['date_create'],
				'note' => $_POST['addOrder']['note']
			)
		);

		$PData->content('Запись Сохранена','message',TRUE);
		$PData->content('<a href="'.getURL('admin','orders','tab=search&search='.$_POST['addClient']['tel']).'">'._lang('Перейти к заказу').'</a>','message',TRUE);
		unset($_POST);
	}else{
		$PData->content('Нужно заполнить все объязательные поля','message');
	}

}elseif(isset($_POST['formsubmit-edit']) && is_array($_POST['editOrder']) && is_array($_POST['editClient'])){

	$o_id = $cl_id = '';

	foreach($_POST['editOrder'] AS $key => $val){
		$o_id = $key;
		break;
	}

	foreach($_POST['editClient'] AS $key => $val){
		$cl_id = $key;
		break;
	}

	$_POST['editOrder'][$o_id]['additional_data'] = json_encode(@$_POST['editOrder'][$o_id]['additional_data']);
	$_POST['editClient'][$cl_id]['additional_data'] = json_encode(@$_POST['editClient'][$cl_id]['additional_data']);


	$newImgURL = $attachment = array();

	$att_array = json_decode(_strCode($_POST['editClient'][$cl_id]['attachment'],FALSE,FALSE), TRUE);

	foreach($_FILES['images']['error'] AS $i => $error){

		$att_array[$i] = @$att_array[$i];

		if($error!=0)continue;

		$newImgURL[$i] = _getRandStr();

		while(_checkURL(HOMEPAGE.'media/clients-docs/'.$newImgURL[$i].'.png')){

			$newImgURL[$i] = _getRandStr();

		}

		writeImg( $newImgURL[$i], 'images', $i, 'real_size', CODE_FOLDER.'../media/clients-docs/');


		if(_checkURL($att_array[$i])){
			unlink(str_replace(HOMEPAGE,CODE_FOLDER.'../',$att_array[$i]));
		}

		$attachment[$i] = HOMEPAGE . 'media/clients-docs/' . $newImgURL[$i] . '.png';
	}

	if(is_array($attachment) && !empty($attachment)){

		foreach($attachment AS $i => $val){
			$att_array[$i] = $attachment[$i];
		}
	}

	switch($_POST['editOrder'][$o_id]['status']){
		case'_1':
			$managers = u_getUsers('rights_5_6',"u_rights = '5' OR u_rights = '6' ORDER BY u_rights ASC, u_fullname ASC");

			if(
				!empty($_POST['editOrder'][$o_id]['u_id']) &&
				!empty($managers[$_POST['u_id']]['u_email']) &&
				_checkEmail($managers[$_POST['u_id']]['u_email']) &&
				(
					$_POST['u_id']!=$_POST['editOrder'][$o_id]['u_id'] ||
					$_POST['o_status']!=$_POST['editOrder'][$o_id]['status']
				)
			){

				$text = 'У Вас новая заявка в очереди со статусом "'.ord_statuses($_POST['editOrder'][$o_id]['status']).'": '.getURL('admin','orders','status[]='.$_POST['editOrder'][$o_id]['status']);

				_eMail($managers[$_POST['u_id']]['u_email'], 'Новая заявка на обработку', $text,'no-reply@m-mba.com.ua');
			}
		break;
		case'_4':case'_5':
			$managers = u_getUsers('rights_5_6',"u_rights = '5' OR u_rights = '6' ORDER BY u_rights ASC, u_fullname ASC");

			if(
				!empty($_POST['editOrder'][$o_id]['u_id']) &&
				!empty($managers[$_POST['u_id']]['u_email']) &&
				_checkEmail($managers[$_POST['u_id']]['u_email'])
			){

				$text = 'У Вас новая заявка в очереди со статусом "'.ord_statuses($_POST['editOrder'][$o_id]['status']).'": '.getURL('admin','orders','status[]='.$_POST['editOrder'][$o_id]['status']);

				_eMail($managers[$_POST['u_id']]['u_email'], 'Новая заявка на обработку', $text,'no-reply@m-mba.com.ua');
			}
		break;
		case'_6':
			$managers = u_getUsers('rights_4',"u_rights = '4' ORDER BY u_rights ASC, u_fullname ASC");

			foreach($managers AS $val){
				if(
					!empty($val['u_email']) &&
					_checkEmail($val['u_email'])
				){
					$text = 'У Вас новая заявка в очереди: '.getURL('admin','orders','status[]=6');
					_eMail($val['u_email'], 'Новая заявка на обработку', $text,'no-reply@m-mba.com.ua');
				}
			}
		break;
		case'_3':
			$managers = u_getUsers('rights_3',"u_rights = '3' ORDER BY u_rights ASC, u_fullname ASC");

			foreach($managers AS $val){
				if(
					!empty($val['u_email']) &&
					_checkEmail($val['u_email'])
				){
					$text = 'У Вас новая заявка в очереди: '.getURL('admin','orders','status[]=3');
					_eMail($val['u_email'], 'Новая заявка на обработку', $text,'no-reply@m-mba.com.ua');
				}
			}
		break;
	}



	$_POST['editClient'][$cl_id]['attachment'] = json_encode($att_array);

	if(!empty($_POST['log'])){

		$log = explode('|',$_POST['log']);

		if( (is_array($log) && isset($log[count($log)-1]) && $log[count($log)-1]!=$_POST['editOrder'][$o_id]['status'] ) ){
			$_POST['editOrder'][$o_id]['log'] = $_POST['log'].'|'.$_POST['editOrder'][$o_id]['status'];
		}else{
			$_POST['editOrder'][$o_id]['log'] = $_POST['editOrder'][$o_id]['status'];
		}

	}else{
		$_POST['editOrder'][$o_id]['log'] = $_POST['editOrder'][$o_id]['status'];
	}


	$_POST['editOrder'][$o_id]['date_group_start'] = strtotime($_POST['editOrder'][$o_id]['date_group_start']);
	$_POST['editOrder'][$o_id]['date_done'] = strtotime($_POST['editOrder'][$o_id]['date_done']);

	if(!empty($_POST['editOrder'][$o_id]['date_next_touch']) || !empty($_POST['editOrder'][$o_id]['date_next_touch_time']))
	$_POST['editOrder'][$o_id]['date_next_touch'] = strtotime($_POST['editOrder'][$o_id]['date_next_touch'].' '.$_POST['editOrder'][$o_id]['date_next_touch_time']);



	unset($_POST['editOrder'][$o_id]['date_next_touch_time']);

	if(isset($_POST['refrash_cl_date'])) $_POST['editClient'][$cl_id]['date_update']=time();

	$sql->update('_orders',"o_id='"._protect($o_id)."'",$_POST['editOrder'][$o_id],'o_');

	$sql->update('_clients',"cl_id='"._protect($cl_id)."'",$_POST['editClient'][$cl_id],'cl_');

	$PData->content('Запись Сохранена','message',TRUE);

}


/** Пагінація **/

$parent_select = $text = $where = '';
$managers = array();

$where = ord_filter_query(
	""
);

$nav = _pagination(
	'_referrals RIGHT JOIN _orders ON o_mark=r_mark INNER JOIN _clients ON o_cl_id=cl_id',
	'o_id',
	$where);


/** Отримання даних про замовлення з бази **/

$result = $sql->query("
	SELECT *
	FROM _referrals RIGHT JOIN _orders ON o_mark=r_mark
	INNER JOIN _clients ON o_cl_id=cl_id
	{$where}
	"._run_filter('a_ord_setORDER_BY','ORDER BY o_date_create DESC')."

	".@$nav['limit']."
");


if(!is_array($result) || empty($result)){

	if(empty($_GET['tab'])){
		$text = _lang('<b>Заказов больше нет</b><br/>Отличная работа! =)');
	}else{
		$text = _lang('Не найдено! =\ ');
	}
}else{

	foreach($result AS $key => $cval){
		$text .= ord_orders_editing_form($cval);
	}

	$text = '
	<form method="POST">
		<table id="tableEdit" class="table">
			<tbody>
				<tr>
					<th id="head-tableList" class="head head0 tableList0">'._lang('Выбрать').'</th>
					<th id="head-tableList" class="head head1 tableList0">'._lang('Клиент').'</th>
					<th id="head-tableList" class="head head2 tableList1">'._lang('Заказ').'</th>
					<th id="head-tableList" class="head head3 tableList2">'._lang('Доп. данные').'</th>
					<th id="head-tableList" class="head head4 tableList2">'._lang('Примечание').'</th>

				</tr>
				'.$text.'
			</tbody>
		</table>
		<input type="submit" name="not_valideSubmit" value="'._lang('Не валидные').'">
		<input type="submit" name="delSubmit" value="'._lang('Удалить').'">
	</form>
	<p class="greyText">
		* - '._lang('не проверенные менеджером данные. Информация взята с базы автоматически').'
	</p>
	<style>
	.head1,.head3{width: 20%;}
	.head2,.head4{width: 25%;}
	</style>
	';
}


/** __________ Формування основного HTML коду __________ **/

$PData->content('Заказы','title');


/** // Отримуємо список Посадок і міток для Фільтра **/


$PData->content(

		ord_filter_html().'


		<hr/>
		<br/>
		<div class="well">
		    '.$text.'
		</div>


<script type="text/javascript">

</script>
'.$nav['html']);