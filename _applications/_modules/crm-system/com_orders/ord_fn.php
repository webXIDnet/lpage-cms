<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента Опрацювання замовлень для CRM системи
 *
***/


function ord_statuses($val=FALSE, $type='select'){

	global 	$meta,
			$user;

	$statuses = $meta->val('apSet_crm-system','mod_option');

	$array = $statuses['statuses'];

	if(isset($array[$val])){
		return $array[$val];
	}elseif(!$val){
		return '---';
	}else{
		switch($type){
			case'select':
				if(
					isset($statuses['rights']['select'][$user->rights]) &&
					is_array($statuses['rights']['select'][$user->rights])
				){
					unset($array);
					foreach($statuses['rights']['select'][$user->rights] AS $key){
						if(empty($statuses['statuses'][$key]))continue;
						$array[$key] = $statuses['statuses'][$key];
					}
				}
				if( $user->rights == 100){
					unset($array['NAN']);
					$array[0] = _lang('Не определен');
				}
			break;
			case'filter':
				if(
					isset($statuses['rights']['bookmark'][$user->rights]) &&
					is_array($statuses['rights']['bookmark'][$user->rights])
				){
					unset($array);
					foreach($statuses['rights']['bookmark'][$user->rights] AS $key){
						if(empty($statuses['statuses'][$key]))continue;
						$array[$key] = $statuses['statuses'][$key];
					}
				}
			break;
		}
		if(is_array($array))
			asort($array,SORT_NUMERIC);
		return $array;
	}
}
function ord_payment_type($val=FALSE){

	$array=array(
		1=>_lang('Полная оплата'),
		3=>_lang('Оплата частями'),
		2=>_lang('Кредит'),
	);

	$array = _run_filter('ord_payment_type_array',$array);

	if(isset($array[$val]))
		return $array[$val];
	elseif(!$val)
		return '---';
	else
		return $array;

}

function ord_filter_query($where=''){


	if(!empty($_GET['search'])){

		if(!empty($_GET['find_tel'])){
			$where = " cl_tel LIKE '%"._protect($_GET['search'])."'";
		}else{
			$where = " cl_fullname LIKE '%"._protect($_GET['search'])."%'
			OR
				cl_email LIKE '%"._protect($_GET['search'])."%'
			OR
				cl_tel LIKE '%"._protect($_GET['search'])."%'";
		}

	}else{

		$where1[0] = '';
		if(!empty($_GET['dateCreate1']) && strtotime($_GET['dateCreate1'])!=0)
			$where1[0] = "o_date_create >='"._protect( strtotime($_GET['dateCreate1']) )."'";

		$where1[1] = '';
		if(!empty($_GET['dateCreate2']) && strtotime($_GET['dateCreate2'])!=0)
			$where1[1] = "o_date_create <'"._protect( strtotime($_GET['dateCreate2']) )."'";


		$where1[2] = '';
		if(!empty($_GET['dateDone1']) && strtotime($_GET['dateDone1'])!=0)
			$where1[2] = "o_date_done >='"._protect( strtotime($_GET['dateDone1']) )."'";

		$where1[3] = '';
		if(!empty($_GET['dateDone2']) && strtotime($_GET['dateDone2'])!=0)
			$where1[3] = "o_date_done <'"._protect( strtotime($_GET['dateDone2'])+24*60*60 )."'";

		$where1[4] = '';
		if(!empty($_GET['referral']) && is_array($_GET['referral'])){
			foreach($_GET['referral'] AS $val){
				if(!empty($val)){
					if(!empty($where1[4]))$where1[4] .= ' OR ';
					$where1[4] .= "o_mark='"._protect($val)."'";
				}
			}
			if(!empty($where1[4])) $where1[4] = '('.$where1[4].')';
		}

		$where1[5] = '';
		if(!empty($_GET['lpage']) && is_array($_GET['lpage'])){
			foreach($_GET['lpage'] AS $val){
				if(!empty($val)){
					if(!empty($where1[5]))$where1[5] .= ' OR ';
					$where1[5] .= "o_lp_article='"._protect($val)."'";
				}
			}
			if(!empty($where1[5])) $where1[5] = '('.$where1[5].')';
		}

		$where1[6] = '';
		if(!empty($_GET['status']) && is_array($_GET['status'])){
			foreach($_GET['status'] AS $val){
				if(!empty($val)){
					if(!empty($where1[6]))$where1[6] .= ' OR ';

					if($val=='NAN'){
						$where1[6] .= "( o_status='' OR o_status='0' OR  (o_u_id='0' AND o_status>'0') )";
					}elseif($val<0){
						$where1[6] .= "(o_status='"._protect($val)."')";
					}else{
						$where1[6] .= "(o_status='"._protect($val)."' AND (o_u_id!='0' OR o_payment_note!='') )";
					}
				}
			}
			if(!empty($where1[6])) $where1[6] = '('.$where1[6].')';
		}

		$where1[7] = '';
		if(!empty($_GET['manager']) && is_array($_GET['manager'])){
			foreach($_GET['manager'] AS $val){
				if(!empty($val)){
					if(!empty($where1[7]))$where1[7] .= ' OR ';
					$where1[7] .= "o_u_id='"._protect($val)."'";
				}
			}
			if(!empty($where1[7])) $where1[7] = '('.$where1[7].')';
		}

		$where1[8] = '';
		if(!empty($_GET['payType']) && is_array($_GET['payType'])){
			foreach($_GET['payType'] AS $val){
				if(!empty($val)){
					if(!empty($where1[8]))$where1[8] .= ' OR ';
					$where1[8] .= "o_payment_type='"._protect($val)."'";
				}
			}
			if(!empty($where1[8])) $where1[8] = '('.$where1[8].')';
		}

		foreach($where1 AS $val){
			if(!empty($val)){
				if(!empty($where))$where.=' AND ';
				$where .= $val;
			}
		}
	}

	if( (empty($_GET['tab']) || $_GET['tab']=='bookmark' || $_GET['tab']=='filter')){
		global $user;
		switch($user->rights){
			case'5':
				if(!empty($where)) $where .=' AND ';

				$where .= " o_u_id='"._protect($user->id)."' ";
			break;
			case'2':
				if(!empty($where)) $where .=' AND ';

				$where .= " (r_emails LIKE '% "._protect($user->email)."%' OR r_emails LIKE '%,"._protect($user->email)."%' OR r_emails='"._protect($user->email)."')";
			break;
		}
	}

	if(!empty($where)){
		$where = 'WHERE '.$where;
	}
	return $where;
}
function ord_filter_html(){

	global $sql;
	$lp_array = $ref_array = array();
	$lp_select = $ref_select = $status_select = $managers_select = $payType_select = '';

	$managers = u_getUsers('rights_5_6',"u_rights = '5' OR u_rights = '6' ORDER BY u_rights ASC, u_fullname ASC");

	if(empty($_GET['tab'])) $_GET['tab'] = 'bookmark';

	/** Отримуємо список Посадок і міток для Фільтра **/

	$result = $sql->query("
		SELECT
			lp_article, lp_title,
			o_mark, r_title
		FROM _landing_pages, _orders LEFT JOIN _referrals ON o_mark = r_mark
		GROUP BY o_mark
		"
	);

	foreach($result AS $cval){

		if(!empty($cval['lp_article']) && !isset($lp_array[$cval['lp_article']])){

			$lp_array[$cval['lp_article']] = $cval['lp_title'];

			$selected = '';
			if(!empty($_GET['lpage']) && is_array($_GET['lpage']) && in_array($cval['lp_article'],$_GET['lpage'])) $selected = 'selected="selected"';

			$lp_select .= '<option '.$selected.' value="'.$cval['lp_article'].'">'.$cval['lp_title'].'</option>';

		}

		if(!empty($cval['o_mark'])  && !isset($ref_array[$cval['o_mark']])){

			$ref_array[$cval['o_mark']] = (!empty($cval['r_title'])?$cval['r_title']:$cval['o_mark']);

			$selected = '';
			if(!empty($_GET['referral']) && is_array($_GET['referral']) && in_array($cval['o_mark'],$_GET['referral'])) $selected = 'selected="selected"';

			$ref_select .= '<option '.$selected.' value="'.$cval['o_mark'].'">'.$ref_array[$cval['o_mark']].'</option>';

		}
	}

	if(is_array($managers)){
		foreach($managers AS $key => $val){

			$selected = '';
			if( !empty($_GET['manager']) && is_array($_GET['manager']) && in_array($key,$_GET['manager']) )
			$selected = 'selected="selected"';

			$managers_select .= '<option '.$selected.' value="'.$key.'">'.$val['u_fullname'].'</option>';
		}
	}


	$statuses = ord_statuses('array', 'filter');
	if(is_array($statuses)){
		foreach($statuses AS $key => $status){

			$style = '';
			if($key==-1)$style='style="color:red;"';

			$selected = '';
			if( !empty($_GET['status']) && is_array($_GET['status']) && in_array($key,$_GET['status']) )
				$selected = 'selected="selected"';

			$status_select .= '<option '.$style.' '.$selected.' value="'.$key.'">'._lang($status).'</option>';

		}
	}

	$payment_type = ord_payment_type('array');

	if(is_array($payment_type)){
		foreach($payment_type AS $key => $payType){

			$selected = '';
			if( !empty($_GET['payType']) && is_array($_GET['payType']) && in_array($key,$_GET['payType']) )
				$selected = 'selected="selected"';

			$payType_select .= '<option '.$selected.' value="'.$key.'">'.$payType.'</option>';
		}
	}

	return '
		<div class="tab-box" role="tabpanel">
		<ul class="nav nav-tabs" role="tablist">
	        <li role="presentation" class=""><a aria-expanded="false" href="#tabs" aria-controls="home" role="tab" data-toggle="tab">'._lang('Закладки').'</a></li>
	        <li role="presentation"><a aria-expanded="true" href="#filter" aria-controls="home" role="tab" data-toggle="tab">'._lang('Фильтр').'</a></li>
	        <li role="presentation"><a href="#search" aria-controls="home" role="tab" data-toggle="tab">'._lang('Поиск').'</a></li>
	        <li role="presentation"><a href="#add-order" aria-controls="profile" role="tab" data-toggle="tab">+ '._lang('Добавить клиента').'</a></li>
      	</ul>
			<!--<ul role="presentation" class="">
				<li switchTab="tab_0" class="'._getActive($_GET['tab'],'bookmark').'"><a>'._lang('Закладки').'</a></li>
				<li switchTab="tab_1" class="'._getActive($_GET['tab'],'filter').'"><a>'._lang('Фильтр').'</a></li>
				<li switchTab="tab_2" class="'._getActive($_GET['tab'],'search').'"><a>'._lang('Поиск').'</a></li>
				<li switchTab="tab_3" class="'._getActive($_GET['tab'],'addOrder').'"><a>+ '._lang('Добавить заказ').'</a></li>
			</ul>-->
			<div class="tab-content">
			<div id="tabs" class="tab-pane fade '._getActive($_GET['tab'],'bookmark').'">'.
				ord_bookmarks_list()
			.'
			</div>
			<div id="filter" class="tab-pane fade '._getActive($_GET['tab'],'filter').'">
				<!-- Фільтр -->
				<form class="list">

					<input type="hidden" name="tab" value="filter"/>

					<label class="right_colmn" style="margin:0;">
						<b>'._lang('Дата оплаты').':</b><br/>
						<input type="date" name="dateDone1" value="'.@$_GET['dateDone1'].'"/> до <input type="date" name="dateDone2" value="'.@$_GET['dateDone2'].'"/>
					</label>

					<label >
						<b>'._lang('Дата поступления').':</b><br/>
						<input type="date" name="dateCreate1" value="'.@$_GET['dateCreate1'].'"/> до <input type="date" name="dateCreate2" value="'.@$_GET['dateCreate2'].'"/>
					</label>

					<p class="right_colmn">
						<label>
							<b>'._lang('Статусы').':</b><br/>
							<select class="multiselect" name="status[]" multiple="multiple">
								<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>

								'.$status_select.'
							</select>
						</label>

					</p>

					<p>
						<label>
							<b>'._lang('Метки').':</b><br/>
							<select class="multiselect" name="referral[]" multiple="multiple">
								<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
								'.$ref_select.'
							</select>
						</label>
					</p>
					<div class="clear"></div>
					<p class="right_colmn">
						<label>
							<b>'._lang('Тип оплаты').':</b><br/>
							<select class="multiselect" name="payType[]" multiple="multiple">
								<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
								'.$payType_select.'
							</select>
						</label>
					</p>

					'.
					u_ifRights(
						array(6,100),
						'<p >
							<label>
								<b>'._lang('Менеджеры').':</b><br/>
								<select class="multiselect" name="manager[]" multiple="multiple">
									<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
									'.$managers_select.'
								</select>
							</label>

						</p>'
					)
					.'




					<p>
						<label>
							<b>'._lang('Посадки').':</b><br/>
							<select class="multiselect" name="lpage[]" multiple="multiple">
								<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
								'.$lp_select.'
							</select>
						</label>
					</p>

					<input type="submit" class="btn" value="'._lang('Искать').'"/>
					<p class="greyText">
						! - '._lang('Зажмите Ctrl или Shift, чтобы выбрать несколько пунктов').'<br/>
						! - '._lang('Чтобы найти нужный пункт мультименю, кликните в любом месте нужного вам меню, и нажмите на клавиатуре первую букву нужного вам пункта. Вы можете перемещаться по пунктах с одинаковой первой буквой, нажимая несколько раз на клавишу').'
					</p>
				</form>
				<!-- // Фільтр -->
			</div>
			<div id="search" class="tab-pane fade '._getActive($_GET['tab'],'search').'">
				<!-- Пошук -->
				<form class="list">
					<label><b>'._lang('Поиск').'</b>:</label>
					<input type="hidden" name="tab" value="search"/>
					<input placeholder="'._lang('ФИО или телефон, или email').'" type="text" name="search" value="'.@$_GET['filter'].'"/>
					<p>
						<label>
							<input type="checkbox" '.(@$_GET['find_tel']=='true'?'checked="checked"':'').' name="find_tel" value="true"/><span class="greyText"> - '._lang('Искать по последним цифрам телефона (количество не имеет значения)').'</span>
						</label>
					</p>
					<input type="submit" class="btn" value="'._lang('Искать').'"/>
				</form>
				<!-- // Пошук -->
			</div>

			<div id="add-order" class="tab-pane fade '._getActive($_GET['tab'],'addOrder').'">
				<!-- Додати замовлення -->
				'.ord_addOrder().'
				<!-- // Додати замовлення -->
			</div>
			</div>
	';
}

function ord_addOrder(){
	/** Формування основного списку Менеджерів **/

	$managers = u_getUsers('rights_5_6',"u_rights = '5' OR u_rights = '6' ORDER BY u_rights ASC, u_fullname ASC");

	$status_select = $pay_select = $managers_select = $lang_select = '';

	if(is_array($managers)){
		foreach($managers AS $key => $val){
			$selected = '';
			if($key==@$_POST['addOrder']['u_id'])$selected = 'selected="selected"';
			$managers_select .= '<option '.$selected.' value="'.$key.'">'.$val['u_fullname'].'</option>';
		}
	}

	foreach(cl_brawser_lang() AS $key => $payment){
		$selected = '';
		if($key==@$_POST['addClient']['brawser_lang'])$selected = 'selected="selected"';
		$lang_select .= '<option '.$selected.' value="'.$key.'">'.$payment.'</option>';

	}

	foreach(ord_statuses('array') AS $key => $status){
		$style = '';
		if($key==-1)$style='style="color:red;"';

		$selected = '';
		if($key==@$_POST['addOrder']['status'])$selected = 'selected="selected"';

		$status_select .= '<option '.$style.' '.$selected.' value="'.$key.'">'._lang($status).'</option>';

	}

	$o_additional_data_array = @$_POST['addOrder']['additional_data'];
	$o_additional_data = '';

	if(is_array($o_additional_data_array)){
		foreach($o_additional_data_array AS $key=>$val){
			$o_additional_data .=
			'<p>
				<label>
					<b>'._lang($key).'</b>:<br>
					<input class="input" type="text" name="addOrder[additional_data]['.$key.']" value="'.$val.'"/>
				</label>
			</p>';
		}
	}
	$o_additional_data = _run_filter('addOrder_form_o_additional_data',$o_additional_data);

//	if(empty($_POST['addOrder']['date_create'])) $_POST['addOrder']['date_create']= time();

	$text =
	'<form class="list" method="POST" action="'.getURL('admin','orders','tab=addOrder').'">
		<div class="right_colmn">
			<p style="margin-top:0;">
				<label>
					<b class="ord_status">'._lang('Статус').':</b><br>
					<select style=" height: auto; width: 325px;" class="input" name="addOrder[status]">
						'.$status_select.'
					</select>
				</label>
			</p>
			'.u_ifRights(
				array(6,100),
				'<p style=" ">
					<label>
						<b>'._lang('Выбрать менеджера').'</b>:<br>
						<select style=" height: auto; width: 325px;" class="input" name="addOrder[u_id]">
							<option value="">- '._lang('не указано').' -</option>
							'.$managers_select.'
						</select>
					</label>
				</p>'
			).'
			<p>
				<label>
					<b>'._lang('Дата поступления').'</b>:<br>
					<input style="width: 50%;" class="input" type="date" name="addOrder[date_create]" value="'.(!empty($_POST['addOrder']['date_create'])?date('Y-m-d',$_POST['addOrder']['date_create']):'').'"/>
					<input style="width: 30%;" class="input" type="time" name="addOrder[date_create_time]" value="'.(!empty($_POST['addOrder']['date_create'])?date('H:i',$_POST['addOrder']['date_create']):'').'"/>
				</label>
			</p>
			<p>
				<label>
					<b>'._lang('Дата следующего контакта').'</b>:<br>
					<input style="width: 50%;" class="input" type="date" name="addOrder[date_next_touch]" value="'.(!empty($_POST['addOrder']['date_next_touch'])?date('Y-m-d',$_POST['addOrder']['date_next_touch']):'').'"/>
					<input style="width: 30%;" class="input" type="time" name="addOrder[date_next_touch_time]" value="'.(!empty($_POST['addOrder']['date_next_touch'])?date('H:i',$_POST['addOrder']['date_next_touch']):'').'"/>
				</label>
			</p>
			<p>
				<label>
					<b>'._lang('Метка').'</b>:<br>
					<input class="input" type="text" name="addOrder[mark]" value="'.@$_POST['addOrder']['mark'].'">
				</label>
			</p>

			<p>
				<label>
					<b>'._lang('Комментарий').'</b>:<br>
					<textarea style="height: 100px;" class="input" name="addOrder[note]">'.@$_POST['addOrder']['note'].'</textarea>
				</label>
			</p>
		</div>

		<p>
			<label>
				<b>1. '._lang('ФИО').' *</b>:<br>
				<input class="input" type="text" name="addClient[fullname]" value="'.@$_POST['addClient']['fullname'].'">
			</label>
		</p>
		<p>
			<label>
				<b>2. '._lang('Тел').' *</b>:<br>
				<input class="input" type="text" name="addClient[tel]" value="'.@$_POST['addClient']['tel'].'">
			</label>
		</p>
		<p>
			<label>
				<b>3. '._lang('Email').' *</b>:<br>
				<input class="input" type="email" name="addClient[email]" value="'.@$_POST['addClient']['email'].'">
			</label>
		</p>
		<p>
			<label>
				<b>4. '._lang('Название заказа').' *</b>:<br>
				<input class="input" type="text" name="addOrder[prod]" value="'.@$_POST['addOrder']['prod'].'"/>
			</label>
		</p>
		<p>
			<label>
				<b>5. '._lang('Цена').' *</b>:<br>
				<input class="input" type="text" name="addOrder[price]" value="'.@$_POST['addOrder']['price'].'"/>
			</label>
		</p>
		'.$o_additional_data.'
		<!--p>
			<label>
				<b>7. '._lang('Язык').' *</b>:<br>
				<select style=" height: auto;" class="input" name="addClient[brawser_lang]">
					'.$lang_select.'
				</select>
			</label>
		</p-->
		<div class="clear"></div>
		<input type="submit" class="btn" name="formsubmit-add" value="'._lang('Сохранить').'"/>
	</form>';

	return $text;
}

function ord_bookmarks_list(){

	$active = '';

	if( !empty($_GET['status']) && is_array($_GET['status']) && in_array('NAN',$_GET['status']))
			$active = 'active';

	$bookmarks_list = '<a class="zakladka '.$active.'" href="'.getURL('admin','orders','status[]=NAN').'">'._lang('Не определен').'</a>';

	$statuses = ord_statuses('array', 'filter');

	if(is_array($statuses)){
		foreach($statuses AS $key => $status){


			$active = '';
			if( !empty($_GET['status']) && is_array($_GET['status']) && in_array($key,$_GET['status']) )
				$active = 'active';

			$bookmarks_list .= '<a class="zakladka '.$active.'" href="'.getURL('admin','orders','status[]='.$key).'">'._lang($status).'</a>';

		}
	}
	return $bookmarks_list;
}

function ord_orders_editing_form($cval){

	if(empty($cval))return '';

	/** Формування основного списку Менеджерів **/

	$managers = u_getUsers('rights_5_6',"u_rights = '5' OR u_rights = '6' ORDER BY u_rights ASC, u_fullname ASC");

	$o_id = $cval['o_id'];
	$cl_id = $cval['cl_id'];
	$status_select = $pay_select = $managers_select = $lang_select = '';

	if(is_array($managers)){
		foreach($managers AS $key => $val){

			$selected = '';
			if($key==$cval['o_u_id'])$selected = 'selected="selected"';

			$managers_select .= '<option '.$selected.' value="'.$key.'">'.$val['u_fullname'].'</option>';
		}
	}

	foreach(cl_brawser_lang() AS $key => $payment){


		$selected = '';
		if($key==$cval['cl_brawser_lang'])$selected = 'selected="selected"';

		$lang_select .= '<option '.$selected.' value="'.$key.'">'.$payment.'</option>';

	}

	foreach(ord_payment_type('array') AS $key => $payment){

		$selected = '';
		if($key==$cval['o_payment_type'])$selected = 'selected="selected"';

		$pay_select .= '<option '.$selected.' value="'.$key.'">'.$payment.'</option>';

	}

	foreach(ord_statuses('array') AS $key => $status){

		$style = '';
		if($key==-1)$style='style="color:red;"';

		$selected = '';
		if($key==$cval['o_status'])$selected = 'selected="selected"';

		$status_select .= '<option '.$style.' '.$selected.' value="'.$key.'">'._lang($status).'</option>';

	}

	/* Опрацювання даних прикліплдених файлів клієнта */
	$attachment = array('','','','','');
	$att_count = 0;
	$att_array = json_decode($cval['cl_attachment'],TRUE);

	if(is_array($att_array)){
		foreach($att_array AS $key=>$att){

			$attachment[$key] = '';

			if(_checkURL($att)){
				$attachment[$key] = '<a href="'.$att.'" target="_blank"><img style="max-width:300px;max-height:300px" src="'.$att.'"/></a><br/>';
				if($key!=4)
					$att_count++;
			}

		}
	}

	/* Опрацювання додаткових даних клієнта */
	$cl_additional_data_array = json_decode($cval['cl_additional_data'],TRUE);
	$cl_additional_data = '';

	if(is_array($cl_additional_data_array)){
		foreach($cl_additional_data_array AS $key=>$val){
			$cl_additional_data .=
			'<p>
				<label>
					<b>'._lang($key).'</b>:<br>
					<input class="input" type="text" name="editClient['.$cl_id.'][additional_data]['.$key.']" value="'.$val.'"/>
				</label>
			</p>';
		}
	}
	$cl_additional_data = _run_filter('editing_form_cl_additional_data',$cl_additional_data,$cval);

	/* Опрацювання додаткових даних замовлення */
	$o_additional_data_array = json_decode($cval['o_additional_data'],TRUE);
	$o_additional_data = '';


	if(is_array($o_additional_data_array)){
		foreach($o_additional_data_array AS $key=>$val){

			$alt_name = '';
			if (strripos($key,'Название') === true)
				$alt_name = 'nazvanie-tovara';
			if (strripos($key,'Цена') === true)
				$alt_name = 'tsena-tovara';
			if (strripos($key,'Количество') === true)
				$alt_name = 'kolichestvo';

			$o_additional_data .=
			'<p class="addition-fields field-'.$alt_name.'">
				<label>
					<b>'._lang($key).'</b>:<br>
					<input class="input" type="text" name="editOrder['.$o_id.'][additional_data]['.$key.']" value="'.$val.'"/>
				</label>
			</p>';
		}
	}


	$o_additional_data = _run_filter('editing_form_o_additional_data',$o_additional_data,$cval);

	$payment_note = $currency = '';
	$sum = 0;

	if(!empty($cval['o_payment_note'])){

		$cval['o_payment_note'] = json_decode($cval['o_payment_note'],TRUE);

		if(is_array($cval['o_payment_note'])){
			foreach($cval['o_payment_note'] AS $val){
				$sum += $val['client_money'];
				$currency = @$val['currency'];

				$payment_note .= '
					<p style="border: 1px solid green;padding: 10px 20px 10px 10px;">
						<b style="color:#000;display: block;text-align: center;padding-bottom: 9px;">'._lang('On-line Платеж').'</b>
						'._lang('№ платежа').': <b style="color:#000">'.$val['payment_id'].'</b><br/>
						'._lang('Оплачено').': <b style="color:#000">'.$val['client_money'].' '.@$val['currency'].'</b><br/>
						'._lang('Насчитано').': <b style="color:#000">'.$val['shop_money'].' '.@$val['currency'].'</b><br/>
						'._lang('Метод').': <b style="color:#000">'.$val['pay_method'].'</b><br/>
						'._lang('Статус платежа').': <b style="color:#000">'.$val['pay_status'].'</b><br/>

					</p>
				';
			}
			if(!empty($payment_note)){
				$currency = '
					<p style="border: 0px solid green;padding: 10px 0px 10px">
						<b style="color:#000;display: block;text-align: center;padding-bottom: 9px;">'._lang('On-line Платежи').'</b>

						<span style="color:#000;">'._lang('Количество платежей').': <b >'.count($cval['o_payment_note']).'</b></span><br/>

						<span style="color:#000;">'._lang('Клиент оплатил').': <b >'.$sum.' '.$currency.'</b></span><br/>



					</p>';
			}
		}

	}else{

	}


	global $user;
	$form = '';



	if($user->rights>2){
		$form = '<div class="tableEdit_form">
					<input type="hidden" name="log" value="'.$cval['o_log'].'"/>
					'.

				u_ifRights( //селект зі списком менеджерів і вибраним менеджером, якщо він призначений для дазої заявки

					array(6,100), //список виводиться лище для кервівника відділу продажів

					'<p style=" display: table; margin: 0px auto 5px; text-align: center; ">
						<label>
							<b>'._lang('Выбрать менеджера').'</b>:<br>
							<select style=" height: auto; width: 325px;" class="input" name="editOrder['.$o_id.'][u_id]">
								<option value="">- '._lang('не указано').' -</option>
								'.$managers_select.'
							</select>
							<input type="hidden" name="u_id" value="'.$cval['o_u_id'].'"/>
						</label>
					</p>'
				).

				_run_filter('ord_orders_editing_form_order_information',

					'<h2 class="h1" style=" text-align: center; ">'._lang('Информация по заказу').'</h2>

					<div class="right_colmn">
						<p class="right-fields field-1" style="margin-top:0;">
							<label>
								<b class="ord_status ord_status_'.$cval['o_status'].'">'._lang('Статус').':</b><br>
								<select style=" height: auto; width: 325px;" class="input" name="editOrder['.$o_id.'][status]">
									'.$status_select.'
								</select>
								<input type="hidden" name="o_status" value="'.$cval['o_status'].'"/>
							</label>
						</p>
						<p class="right-fields field-2">
							<label>
								<b>'._lang('Тип оплаты').'</b>:<br>
								<select style=" height: auto; width: 325px;" class="input" name="editOrder['.$o_id.'][payment_type]">
									<option value="">- '._lang('не указано').' -</option>
									'.$pay_select.'
								</select>
							</label>
						</p>
						<p class="right-fields field-3">
							<label>
								<b>'._lang('Дата полной оплаты').'</b>:<br>
								<input class="input" type="date" name="editOrder['.$o_id.'][date_done]" value="'.(!empty($cval['o_date_done'])?date('Y-m-d',$cval['o_date_done']):'').'"/>
							</label>
						</p>
						'.$o_additional_data.'
						<p class="right-fields field-4">
							<label>
								<b>'._lang('Комментарий').'</b>:<br>
								<textarea style="height: 100px;" class="input" name="editOrder['.$o_id.'][note]">'.$cval['o_note'].'</textarea>
							</label>
						</p>

					</div>

					<p class="left-fields field-5">
						<label>
							<b>'._lang('Название заказа').'</b>:<br>
							<input class="input" type="text" name="editOrder['.$o_id.'][prod]" value="'.$cval['o_prod'].'"/>
						</label>
					</p>
					<p class="left-fields field-6">
						<label>
							<b>'._lang('Цена').'</b>:<br>
							<input class="input" type="text" name="editOrder['.$o_id.'][price]" value="'.$cval['o_price'].'"/>
						</label>
					</p>
					<p class="left-fields field-7">
						<label>
							<b>'._lang('Дата выполнения заказа').'</b>:<br>
							<input class="input" type="date" name="editOrder['.$o_id.'][date_group_start]" value="'.(!empty($cval['o_date_group_start'])?date('Y-m-d',$cval['o_date_group_start']):'').'"/>
						</label>
					</p>
					<p class="left-fields field-8">
						<label>
							<b>'._lang('Дата следующего контакта').'</b>:<br>
							<input style="width: 50%;" class="input" type="date" name="editOrder['.$o_id.'][date_next_touch]" value="'.(!empty($cval['o_date_next_touch'])?date('Y-m-d',$cval['o_date_next_touch']):'').'"/>
							<input style="width: 30%;" class="input" type="time" name="editOrder['.$o_id.'][date_next_touch_time]" value="'.(!empty($cval['o_date_next_touch'])?date('H:i',$cval['o_date_next_touch']):'').'"/>
						</label>
					</p>
					<p class="left-fields field-9">
						'._lang('Метка').': <b style="color:#000">'.(empty($cval['o_mark'])?'---':_lang($cval['o_mark'])).'</b><br/>

						'._lang('Посадка').': <b style="color:#000">[<a target="_block" href="'.$cval['o_lp_article'].'" style="color:#000">'._lang('Ссылка').'</a>]</b><br/>
						'._lang('Дата поступления').': <b>'.date(_lang('d.m.Y в H:i'), $cval['o_date_create']).'</b>

					</p>
					'.$currency,
					array(
						'cval' => $cval,
						'o_id' => $o_id,
						'o_additional_data' => $o_additional_data,
						'pay_select' => $pay_select,
						'status_select' => $status_select,
						'currency' => $currency
					)
				)
					.'
					<div class="clear"></div>
					<h2 class="h1" style=" text-align: center; ">'._lang('Клиент').'</h2>

					<div class="right_colmn">
						<p class="right-fields field-10">
							<label>
								<b>'._lang('Язык').'</b>: <span style="color: #888;">('._lang('по умолчанию - данные из браузера клиента').')</span><br>
								<select style=" height: auto;" class="input" name="editClient['.$cl_id.'][brawser_lang]">
									'.$lang_select.'
								</select>
							</label>
						</p>
						<input class="input" type="hidden" name="editClient['.$cl_id.'][attachment]" value="'._strCode($cval['cl_attachment'],TRUE,FALSE).'"/>
						<p class="right-fields field-11">
							<label>
								<b>'._lang('Паспорт - Страница 1').'</b>:<br>
								'.$attachment[0].'
								<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
							</label>
						</p>
						<p class="right-fields field-12">
							<label>
								<b>'._lang('Паспорт - Страница 2').'</b>:<br>
								'.$attachment[1].'
								<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
							</label>
						</p>
						<p class="right-fields field-13">
							<label>
								<b>'._lang('Паспорт - Прописка').'</b>:<br>
								'.$attachment[2].'
								<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
							</label>
						</p>
						<p class="right-fields field-14">
							<label>
								<b>'._lang('ИНН').'</b>:<br>
								'.$attachment[3].'
								<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
							</label>
						</p>
						<p class="right-fields field-15">
							<label>
								<b>'._lang('Фото').'</b>:<br>
								'.$attachment[4].'
								<input class="input" type="file" name="images[]" accept="image/jpeg,image/png"/>
							</label>
						</p>
					</div>
					<p class="left-fields field-16">
						<label>
							<b>'._lang('ФИО').'</b>:<br>
							<input class="input" type="text" name="editClient['.$cl_id.'][fullname]" value="'.$cval['cl_fullname'].'"/>
						</label>
					</p>
					<p class="left-fields field-17">
						<label>
							<b>'._lang('Тел').'</b>:<br>
							<input class="input" type="text" name="editClient['.$cl_id.'][tel]" value="'.$cval['cl_tel'].'"/>
						</label>
					</p>
					<p class="left-fields field-18">
						<label>
							<b>'._lang('Email').'</b>:<br>
							<input class="input" type="text" name="editClient['.$cl_id.'][email]" value="'.$cval['cl_email'].'"/>
						</label>
					</p>
					<p class="left-fields field-19">
						<label>
							<b>'._lang('Примечание').'</b>:<br>
							<textarea style="height: 80px;" class="input" name="editClient['.$cl_id.'][adress]">'.$cval['cl_adress'].'</textarea>
						</label>
					</p>
					'.$cl_additional_data.'
					<p class="left-fields field-20">
						<label>
							'._lang('Данные клиента обновлялись').':
							<b>'.date('d.m.Y H:i',$cval['cl_date_update']).'</b>
						</label>
					</p>
					<p class="left-fields field-21">
						<label class="greyText">
							<input type="checkbox" name="refrash_cl_date" value="true" > '._lang('<b class="blackText">Обновить дату</b> актуальности данных клиента').'
						</label>
					</p>
					'._run_filter('ord_orders_editing_form','',$cval).'
					'.$payment_note.'
					<div class="clear"></div>
				</div>';
	}


	$text = '
		<tr>
			<td id="tableList0" name="o_id" class=" col0 tableList0">
				<label>
					'.u_ifRights(
						array(6,100),
						'<input type="checkbox" name="itemID['.$o_id.']">'
					).'
					<span title="'._lang('Внутренний номер заказа').'">'.$cval['o_id'].'</span>
				</label>
				'.$form.'
			</td>

			<td id="tableList1" name="o_id" class=" col col1 tableList1" style="color:#888">

				<b style="color:#000">'.$cval['cl_fullname'].'</b><br/>

				'._lang('Тел').': <span style="color:#000">'.$cval['cl_tel'].'</span><br/>
				'._lang('Email').': <span style="color:#000">'.$cval['cl_email'].'</span><br/>
				<span class="field-11 field-12 field-13 field-14 field-15">'._lang('Паспорт').': <span style="color:'.($att_count<4?'red':'#000').'">'.$att_count.'/4</span></span><br/>

			</td>
			<td id="tableList2" name="o_prod" class=" col col2 tableList2" style="color:#888">

				'._lang('Статус').': <b class="ord_status ord_status_'.$cval['o_status'].'">'._lang(ord_statuses($cval['o_status'])).'</b><br/>
				'._lang('Заказ').': <b style="color:#000">'.$cval['o_prod'].'</b><br/>
				'._lang('Цена').': <span style="color:#000">'.$cval['o_price'].'</span><br/>
				'._lang('Поступила').': <span style="color:#000">'.date(_lang('d.m.Y в H:i'), $cval['o_date_create']).'</span><br/>
				'._lang('Менедежер').': '.(isset($managers[$cval['o_u_id']]['u_fullname'])?'<span style="color:#000">'.$managers[$cval['o_u_id']]['u_fullname'].'</span>':'<span style="color:red">'._lang('не назначен').'</span>').' <br/>
			</td>
			<td id="tableList3" name="o_lp_id" class=" col col3 tableList3" style="color:#888">
				'._lang('Метка').': <span style="color:#000">'.(empty($cval['o_mark'])?'---':_lang($cval['o_mark'])).'</span><br/>

				'._lang('Дата выполнения').': <span style="color:#000">'.(!empty($cval['o_date_group_start'])?date('Y-m-d',$cval['o_date_group_start']):'---').'</span><br/>
				'._lang('Тип оплаты').': <span style="color:#000">'.@''.ord_payment_type($cval['o_payment_type']).'</span><br/>

				'._lang('Язык').': <span style="color:#000">'.cl_brawser_lang($cval['cl_brawser_lang']).'</span><br/>
			</td>
			<td id="tableList3" name="o_lp_id" class="col col3 tableList3 greyText">
				<span title="'._lang('Дата, когда нужно сконтактироваться с клиентом в следующий раз').'" class="field-8">'.
				_lang('Связаться').': <span style="color:#000">'.(empty($cval['o_date_next_touch'])?'---':date(_lang('d.m.Y в H:i'),$cval['o_date_next_touch'])).'</span></span><br/>
				'._lang('Комментарий').':<br/>
				<span class="blackText">'.$cval['o_note'].'</span>
			</td>


		</tr>
	';

	return $text;
}
function ord_get_admin_homepage(){

	global $user;

	switch($user->rights){

		case'3':
			return 'status[]=3';
		break;
		case'4':
			return 'status[]=6';
		break;
		case'5':
			return 'status[]=1';
		break;
		case'6':
			return 'status[]=NAN';
		break;

	}
}

_add_filter('crm-system_admin-panel_mod_settings','CRMs_orders_adminpanel_mod_settings');

function CRMs_orders_adminpanel_mod_settings($result,$set){

	global $meta;
	$access = $bookmarks_select = $status_select = '';
	$rights_select = array('select','bookmark');

	$result['title'] .=
		'
		<li role="presentation" class="active">
		    <a href="#tab_ref_1" aria-controls="home" role="tab" data-toggle="tab">'._lang('Статусы').'</a>
		</li>
		<li role="presentation" class="">
		    <a href="#tab_ref_2" aria-controls="profile" role="tab" data-toggle="tab">'._lang('Права доступа к статусам').'</a>
		</li>';

	$status = '
<p>
			<b>'._lang('Новый статус').'</b><br/>
			<label>
				<input class="input" maxlength="10" placeholder="'._lang('Ключ').'" type="text" name="newModSet[apSet_crm-system][statuses][1]" value=""/> - '._lang('Максимум 10 символов').'
			</label>
			<label>
				<input class="input" placeholder="'._lang('Название').'" type="text" name="newModSet[apSet_crm-system][statuses][0]" value=""/> - '._lang('Заголовок').'
			</label>

		</p><input class="button" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit"><br/><hr/><br/>

		';

	if(is_array($set)){

		foreach($set AS $key_com=>$array){
			switch($key_com){
				case'statuses':
					if(is_array($array)){
						foreach($array AS $key=>$val){
							$status_select[$key] = $val;

							$status .= '
									<b>'._lang($val).'</b> ('.$key.')
									<input class="input" type="text" name="modSet[apSet_crm-system]['.$key_com.']['.$key.']" value="'.$val.'"/>
									<input type="checkbox" name="delApSet[apSet_crm-system]['.$key_com.']['.$key.']" value="'.$key.'"> - '._lang('Удалить').'
                                    <br/>
							';
						}
						$status .= '<hr/>';
					}
				break;
				case'rights':
					if(isset($array['select']))
						$rights_select['select'] = $array['select'];

					if(isset($array['bookmark']))
						$rights_select['bookmark'] = $array['bookmark'];

				break;

			}
		}
	}

	$rights = $meta->val('apSet_users','mod_option');

	if(is_array($rights['rights'])){

		foreach($rights['rights'] AS $key=>$val){
			if($key==100)continue;

			$select1 = $select2 = '';
			if(is_array($status_select)){
				foreach($status_select AS $key1 => $stat){

					$selected1 = $selected2 = '';

					if(isset($rights_select['select'][$key]) && in_array($key1,$rights_select['select'][$key])) $selected1 = 'selected="selected"';
					if(isset($rights_select['bookmark'][$key]) && in_array($key1,$rights_select['bookmark'][$key])) $selected2 = 'selected="selected"';

					$select1 .= '<option '.$selected1.' value="'.$key1.'">'.$stat.'</option>';
					$select2 .= '<option '.$selected2.' value="'.$key1.'">'.$stat.'</option>';
				}
			}
			$access .='
					'._lang($val).' ('.$key.')
					<br>
					<b>'._lang('Может присваивать статусы').'</b>
					<select name="modSet[apSet_crm-system][rights][select]['.$key.'][]" multiple="multiple" style="width:200px;">
						'.$select1.'
					</select>
					<b>'._lang('Может видить статусы').'</b>
					<select name="modSet[apSet_crm-system][rights][bookmark]['.$key.'][]" multiple="multiple" style="width:200px;">
						'.$select2.'
					</select>
				';
		}
	}




	$result['text'] .=
		'
<div role="tabpanel" class="tab-pane fade in active" id="tab_ref_1">
			'.$status.'
		</div>
<div role="tabpanel" class="tab-pane fade" id="tab_ref_2">
1
			'.$access.'
		</div>';
	return $result;
}