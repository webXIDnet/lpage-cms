<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модель Реферальної системи
 *
***/

_add_action('defined_modules', 'CRM_order_com_referrals_defined_modules');

function CRM_order_com_referrals_defined_modules(){
	if(PAGE_TYPE=='admin'){
		global $sql;

		$sql->db_table_installer('CRM-system_Referrals','05.09.2014',
			array(
				'_referrals' => array(
					'r_id' => "`r_id` int(11) NOT NULL AUTO_INCREMENT",
					'r_title' => "`r_title` varchar(500) NOT NULL",
					'r_mark' => "`r_mark` varchar(100) NOT NULL",
					'r_emails' => "`r_emails` text NOT NULL",
					'CREATE' => "`r_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`r_id`)"
				)
			)
		);
	}
}


/** Формування підменю Сайдбару Адмінки **/

_add_filter('CRM_system_admin_sidebar_menu', 'CRM_system_com_referrals_admin_sidebar_menu',14);

function CRM_system_com_referrals_admin_sidebar_menu($result){

	global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Рефералы'), //ім"я пункта меню
            'url' => getURL('admin', 'referrals'), //посилання
            'active_pages' => array('referrals'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100), $menu);

    return $result . $menu;
}

/** Роутер Адмінки **/

_add_hook('adminPanel_controller', 'referrals_admin_controller');

function referrals_admin_controller(){

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL,
			$htmlTPL;

	switch(@$_GET['admin']){

		case'referrals':

			require_once __DIR__ . '/admin_apps/a_referrals.php';
			return TRUE;

		break;

	}
}

_add_hook('init_processes', 'referrals_controller');

function referrals_controller(){

	global $PData;

	require_once __DIR__ . '/ref_fn.php';
	require_once __DIR__ . '/ref_plugin.php';

	ref_SetCookies();

}