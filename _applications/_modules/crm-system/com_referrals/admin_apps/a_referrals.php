<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Додавання рефералів
 *
***/


if(isset($_POST['delRow']) && is_array($_POST['delRow'])){

	$where = '';

	foreach($_POST['delRow'] AS $key=>$val){

		if(!empty($where)){

			$where .= ' OR ';

		}

		$where .= "r_id='"._protect($key)."'";

	}

	if(!empty($where)){

		$sql->delete('_referrals',$where);

		$PData->content('Запись удалена','message',TRUE);

	}else{

		$PData->content('Произошол сбой алгоритма. Запись небыла удалена. Обратитесь пожалуйста к разработчикам.','message');

	}

}elseif(isset($_POST['newRef']) && is_array($_POST['newRef'])){

	$sql->insert('_referrals',$_POST['newRef'],'r_');

	$PData->content('Запись сохранена','message',TRUE);

}elseif(isset($_POST['editRow']) && is_array($_POST['editRow'])){

	$sql->update('_referrals',"r_id='"._protect($_POST['editRowId'])."'",$_POST['editRow']);

	$PData->content('Запись сохранена','message',TRUE);

}


/** Пагінація **/

$parent_select = $text = $where = '';

if(!empty($_GET['filter'])){
	$where = "WHERE r_title LIKE '%"._protect($_GET['filter'])."%'";
}

$nav = _pagination(
	'_referrals',
	'r_id',
	$where,
	getURL('admin','referrals','filter='.@$_GET['filter']));


/** Формування основного списку категорій **/


$result = $sql->query("
	SELECT *
	FROM _referrals
	{$where}
	ORDER BY r_title ASC
	".@$nav['limit']."
");


$text = $htmlTPL->table(
			$result,
			array(_lang('Вибрати'), _lang('Название'), _lang('Метка'), _lang('Email')),
			'table',
			'tableList',
			array('delRow'=>1),
			 _lang('Удалить')
		);


global $list;

/** Отримання ієрархії категорій для селекта **/

$parent_select = $contTPL->catHierarchTree(@$list->category[$category_type]);


/** Формування основного HTML коду **/

$PData->content('Рефералы','title');

$PData->content('

	<form class="list" method="POST">
		<table>
			<tr>
				<td>
					<label><b>+ '._lang('Добавить реферала').'</b>:</label>
					<input placeholder="'._lang('Название').'" type="text" name="newRef[title]" value=""/>
					<input placeholder="'._lang('Метка').'" type="text" name="newRef[mark]" value=""/>
					<textarea placeholder="'._lang('Email партнера').'" type="text" name="newRef[emails]"></textarea>

					<br/>
					<input type="submit" class="btn" value="'._lang('Добавить').'"/>
				</td>
			</tr>
		</table>
	</form>
	<hr/>

');

$PData->content('


		<form class="list">
			<table>
				<tr>
					<td>
						<label><b>'._lang('Поиск').'</b>:</label>
						<input type="hidden" name="admin" value="'.@$_GET['admin'].'"/>
						<input placeholder="'._lang('Название').'" type="text" name="filter" value="'.@$_GET['filter'].'"/><br/>
						<input type="submit" class="btn" value="'._lang('Искать').'"/>
					</td>
				</tr>
			</table>
		</form>
		<hr/>
		<br/>
		<div class="well">
		    '.$text.'
		</div>

'.$nav['html']);