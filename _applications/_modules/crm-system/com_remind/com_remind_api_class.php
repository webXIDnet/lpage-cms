<?php

/**
 * Remind component API class
 */
class remind_api
{
    function __construct()
    {

    }

    public function check_remind()
    {
        global $sql;
        $managers = $sql->query("SELECT * FROM _users WHERE u_rights=5 OR u_rights=6");
        $mans = array();
        foreach ($managers as $man) {
            $mans[$man['u_id']] = array(
                'email' => $man['u_email'],
                'orders' => ''
            );
        }

        $orders = $sql->query("SELECT * FROM _orders INNER JOIN _clients ON (_orders.o_cl_id=_clients.cl_id) WHERE DATE_FORMAT(CURDATE(),'%d.%m.%Y') = DATE_FORMAT(FROM_UNIXTIME(_orders.o_date_next_touch),'%d.%m.%Y') OR _clients.cl_birthday = SUBDATE(CURDATE(), 1) OR _clients.cl_birthday = CURDATE()");
        foreach ($orders as $order) {
            $mail_birthday = '';
            $mail_order = '';
            $birthday = new DateTime($order['cl_birthday']);
            $birthday_1 = new DateTime($order['cl_birthday']);
            $birthday_1->modify('-1 day');
            if (date('d.m.Y', $order['o_date_next_touch']) == date('d.m.Y')) {
                $mail_order = '<strong>' . _lang('Заявка №') . ' ' . $order['o_id'] . '</strong><br>' .
                    _lang('Название товара') . ': ' . $order['o_prod'] . '<br>' .
                    _lang('Цена') . ': ' . $order['o_price'] . '<br>' .
                    _lang('Дата поступления заявки') . ': ' . date('d.m.Y h:i', $order['o_date_create']) . '<br>' .
                    _lang('Дата следующего контакта') . ': ' . date('d.m.Y h:i', $order['o_date_next_touch']) . '<br>' .
                    _lang('Имя клиента') . ': ' . $order['cl_fullname'] . '<br>' .
                    _lang('E-mail клиента') . ': ' . $order['cl_email'] . '<br>' .
                    '<a href="http://panchakarma.shambala.ua/admin/orders/?tab=search&id=' . $order['o_id'] . '">' . _lang('Перейти к заявке') . '</a>'
                    . '<hr>';
            }

            if (date('d.m.Y') == $birthday->format('d.m.Y') || date('d.m.Y') == $birthday_1->format('d.m.Y')) {
                $mail_birthday = '<strong>' . _lang('День рождения') . '</strong><br>' .
                    _lang('Имя клиента') . ': ' . $order['cl_fullname'] . '<br>' .
                    _lang('E-mail клиента') . ': ' . $order['cl_email'] . '<br><hr>';
            }


            $mans[$order['o_u_id']]['orders'] .= $mail_birthday . '<hr>' . $mail_order;
        }
        
        foreach ($mans as $key => $val) {
            if (!empty($val['orders']))
                _eMail($val['email'], _lang('Напоминания!'), $val['orders']);
        }

        echo _lang('Success!');
        exit;
    }

    public function testremind() {
        echo 'testremind!';
//        exit;
    }
}

global $api;
$api->accept('remind', 'check_remind', true);
$api->accept('remind', 'testremind', true);