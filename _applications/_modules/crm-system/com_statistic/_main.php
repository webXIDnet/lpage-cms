<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента статистики для CRM системи
 *
***/

if($user->rights<2){
	require_once __DIR__ . '/stat_counter.php';//Збір статистики
}

_add_action('defined_modules', 'CRM_order_com_stat_defined_modules');

function CRM_order_com_stat_defined_modules(){
	if(PAGE_TYPE=='admin'){
		global $sql;

		$sql->db_table_installer('CRM-system_Statistic','05.09.2014',
			array(
				'_statistic' => array(
					'st_key' => "`st_key` double NOT NULL",
					'st_lp_id' => "`st_lp_id` int(11) NOT NULL",
					'st_date' => "`st_date` int(11) NOT NULL",
					'st_hits' => "`st_hits` int(11) NOT NULL DEFAULT '1'",
					'st_hosts' => "`st_hosts` int(11) NOT NULL DEFAULT '1'",
					'CREATE' => "`st_key` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`st_key`)"
				)
			)
		);
	}
}

if(PAGE_TYPE=='admin'){

	_add_filter('CRM_system_admin_sidebar_menu', 'CRM_system_com_stat_admin_sidebar_menu',12);//віведення пунктів меню
	_add_action('adminPanel_controller', 'CRM_system_com_stat_admin_controller');//модифікація роутера адмінки

	function CRM_system_com_stat_admin_sidebar_menu($result){

		global $PData;

		$vars_array = array(
			array(
				'title' => _lang('Статистика Заказов'), //ім"я пункта меню
				'url' => getURL('admin', 'stat-orders'), //посилання
				'active_pages' => array('stat-orders'), //на яких сторінках має бути активний (розгорнутий)
				'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
			),
			array(
				'title' => _lang('Конверсия посадок'), //ім"я пункта меню
				'url' => getURL('admin', 'stat-lp'), //посилання
				'active_pages' => array('stat-lp'), //на яких сторінках має бути активний (розгорнутий)
				'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
			),
			array(
				'title' => _lang('Конверсия менеджеров'), //ім"я пункта меню
				'url' => getURL('admin', 'stat-meneger'), //посилання
				'active_pages' => array('stat-meneger'), //на яких сторінках має бути активний (розгорнутий)
				'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
			),
		);

		$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

		$menu = u_ifRights(array(6,100), $menu);

		return $result . $menu;
	}

	function CRM_system_com_stat_admin_controller(){

		global 	$PData,
				$admin,
				$user,
				$sql,
				$sqlTPL,
				$contTPL,
				$htmlTPL;

		switch(@$_GET['admin']){
			case'stat-orders':
				require_once __DIR__ . '/apps_admin/a_stat_orders.php';
				return TRUE;
			break;
			case'stat-lp':
				require_once __DIR__ . '/apps_admin/a_stat_lp.php';
				return TRUE;
			break;
			case'stat-meneger':
				require_once __DIR__ . '/apps_admin/a_stat_meneger.php';
				return TRUE;
			break;
		}
	}
}