<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Компонента статистики для CRM системи
 *  
***/

$lp_select = $where = '';
$where_st = $where_ord = array('','','');

if(!empty($_GET['dateCreate1']) && strtotime($_GET['dateCreate1'])!=0){
	$where_st[0] = "st_date >='"._protect( strtotime($_GET['dateCreate1']) )."'";
	$where_ord[0] = "o_date_create >='"._protect( strtotime($_GET['dateCreate1']) )."'";
}

if(!empty($_GET['dateCreate2']) && strtotime($_GET['dateCreate2'])!=0){
	
	$where_st[1] = "st_date <'"._protect( strtotime($_GET['dateCreate2'])+24*60*60 )."'";
	$where_ord[1] = "o_date_create <'"._protect( strtotime($_GET['dateCreate2'])+24*60*60 )."'";
	
}

if(!empty($_GET['lpage']) && is_array($_GET['lpage'])){
	foreach($_GET['lpage'] AS $val){
		
		if(!empty($val)){
			
			if(!empty($where_ord[2]))
				$where_ord[2] .= ' OR ';
			$where_ord[2] .= "lp_id='"._protect($val)."'";
			
			if(!empty($where_st[2]))
				$where_st[2] .= ' OR ';
			$where_st[2] .= "st_lp_id='"._protect($val)."'";
		
		}
	}
	if(!empty($where_ord[2])) $where_ord[2] = '('.$where_ord[2].')';
	if(!empty($where_st[2])) $where_st[2] = '('.$where_st[2].')';
}

foreach($where_st AS $val){
	if(!empty($val)){
		if(!empty($where))
			$where .= ' AND ';
		else 
			$where .= 'WHERE ';
			
		$where .= $val;
	}
}

$st = $sql->query("
	SELECT 
		SUM(st_hits), SUM(st_hosts) 
	FROM 
		_statistic
	{$where}
	",'value'
);

$where='';
foreach($where_ord AS $val){
	if(!empty($val)){
		if(!empty($where))
			$where .= ' AND ';
			
		$where .= $val;
	}
}

if(!empty($where))$where =' AND '.$where;

$ord = $sql->query("
	SELECT 
		COUNT(o_id) 
	FROM 
		_landing_pages 
	LEFT JOIN 
		_orders ON lp_article = o_lp_article
	
	{$where}
	",'value'
);
//WHERE o_status != 'NAN' AND o_status != '-1' AND o_status != ''


$count = array(
	'orders'=>$ord['COUNT(o_id)'],
	'hits' => $st['SUM(st_hits)'],
	'hosts' => $st['SUM(st_hosts)']
);

/** Отримуємо список Посадок для Фільтра **/
	
$result = $sql->query("
	SELECT 
		lp_id, lp_title
	FROM _landing_pages
	"
);
	
foreach($result AS $cval){
		
	$lp_array[$cval['lp_id']] = $cval['lp_title'];
	
	$selected = '';
	if(!empty($_GET['lpage']) && is_array($_GET['lpage']) && in_array($cval['lp_id'],$_GET['lpage'])) $selected = 'selected="selected"';
	
	$lp_select .= '<option '.$selected.' value="'.$cval['lp_id'].'">'.$cval['lp_title'].'</option>';
	
}

/** __________ Формування основного HTML коду __________ **/

$PData->content('Конверсия посадок','title');

$PData->content('

	<form class="list">
		<label class="right_colmn">
			<b>'._lang('Период').':</b><br/>
			<input type="date" name="dateCreate1" value="'.@$_GET['dateCreate1'].'"/> '._lang('до').' <input type="date" name="dateCreate2" value="'.@$_GET['dateCreate2'].'"/>
		</label>
		<label >
			<b>'._lang('Посадки').':</b><br/>
			<select class="multiselect" name="lpage[]" multiple="multiple">
				<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
				'.$lp_select.'
			</select>
		</label>
		
		
		<input type="submit" class="btn" value="'._lang('Применить').'"/>
		<p class="greyText">
			! - '._lang('Зажмите Ctrl или Shift, чтобы выбрать несколько пунктов').'.<br>
			! - '._lang('Чтобы найти нужный пункт мультименю, кликните в любом месте нужного вам меню, и нажмите на клавиатуре первую букву нужного вам пункта. Вы можете перемещаться по пунктах с одинаковой первой буквой, нажимая несколько раз на клавишу').'.
		</p>
	</form>
	
	<div class="clear"></div>
	<hr>
	
	'._lang('Количество заказов').': <b>'.$count['orders'].'</b><br/> 
	'._lang('Ун. просмотров / конверсия').': <b>'.$count['hosts'].' / '.(empty($count['hosts'])?0:round(($count['orders']/$count['hosts'])*100,2)).' %</b><br/>
	'._lang('Всего просмотров / конверсия').': <b>'.$count['hits'].' / '.(empty($count['hits'])?0:round(($count['orders']/$count['hits'])*100,2)).' %</b><br/>
');