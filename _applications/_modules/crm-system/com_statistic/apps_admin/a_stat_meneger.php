<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента статистики для CRM системи
 *
***/

$lp_select = $where = '';
$where1 = array();

$where1[0] = '';
if(!empty($_GET['dateCreate1']) && strtotime($_GET['dateCreate1'])!=0){
	$where1[0] = "
		o_date_create >='"._protect( strtotime($_GET['dateCreate1']) )."'
	AND
		o_date_create >='"._protect( strtotime($_GET['dateCreate1']) )."'
	";
}


$where1[1] = '';
if(!empty($_GET['dateCreate2']) && strtotime($_GET['dateCreate2'])!=0){
	$where1[1] = "
		o_date_create <'"._protect( strtotime($_GET['dateCreate2']) )."'
	AND
		st_date <'"._protect( strtotime($_GET['dateCreate2']) )."'
	";
}


$where1[2] = '';
if(!empty($_GET['manager']) && is_array($_GET['manager'])){
	foreach($_GET['manager'] AS $val){
		if(!empty($val)){
			if(!empty($where1[2]))$where1[2] .= ' OR ';
			$where1[2] .= "o_u_id='"._protect($val)."'";
		}
	}
	if(!empty($where1[2])) $where1[2] = '('.$where1[2].')';
}

foreach($where1 AS $val){
	if(!empty($val)){
		if(!empty($where))$where.=' AND ';
		$where .= $val;
	}
}

if(!empty($where))$where =' AND '.$where;

$result = $sql->query("
	SELECT
		o_status
	FROM
		_orders
	WHERE
		( o_status = '9' OR o_status = '8' )
	{$where}
	"
);

$lp = $ord = array();
$count = array(
	'оплачен'=>0,
	'отказ' => 0,
);

foreach($result AS $cval){
	if($cval['o_status']=='9'){
		$count['отказ']++;
	}else{
		$count['оплачен']++;
	}
}


/** Отримуємо список Посадок для Фільтра **/

$managers = u_getUsers('rights_5_6',"u_rights = '5' OR u_rights = '6' ORDER BY u_rights ASC, u_fullname ASC");

$managers_select = '';

if(is_array($managers)){
	foreach($managers AS $key => $val){

		$selected = '';
		if( !empty($_GET['manager']) && is_array($_GET['manager']) && in_array($key,$_GET['manager']) )
		$selected = 'selected="selected"';

		$managers_select .= '<option '.$selected.' value="'.$key.'">'.$val['u_fullname'].'</option>';
	}
}


/** __________ Формування основного HTML коду __________ **/

$PData->content('Конверсия менеджеров','title');

$PData->content('

	<form class="list">
		<label class="right_colmn">
			<b>'._lang('Период').':</b><br/>
			<input type="date" name="date-create1" value="'.@$_GET['date-done1'].'"/> '._lang('до').' <input type="date" name="date-done2" value="'.@$_GET['date-create2'].'"/>
		</label>
		<label>
			<b>'._lang('Менеджеры').':</b><br/>
			<select class="multiselect" name="manager[]" multiple="multiple">
				<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
				'.$managers_select.'
			</select>
		</label>


		<input type="submit" class="btn" value="'._lang('Применить').'"/>
		<p class="greyText">
			! - '._lang('Зажмите Ctrl или Shift, чтобы выбрать несколько пунктов').'.<br>
			! - '._lang('Чтобы найти нужный пункт мультименю, кликните в любом месте нужного вам меню, и нажмите на клавиатуре первую букву нужного вам пункта. Вы можете перемещаться по пунктах с одинаковой первой буквой, нажимая несколько раз на клавишу').'.
		</p>
	</form>

	<div class="clear"></div>
	<hr><br/>
	<p>
		'._lang('Количество оплачегих заказов').': <b>'.$count['оплачен'].'</b><br/>
		'._lang('Количество отказов').': <b>'.$count['отказ'].'</b><br/>
		 '._lang('Всего заказов / конверсия').': <b>'.($count['отказ']+$count['оплачен']).' / '.
		(!empty($count['оплачен'])
		?
			round(( $count['оплачен']/($count['отказ']+$count['оплачен']) )*100, 2)
		:
		0
		)
		.' %</b><br/>
	</p><br/>
	<hr>
	<p class="greyText">
		! - '._lang('в рейтинге принимают участие только заказы со статусами "Оплачен" и "Отказ"').'
	</p>
');