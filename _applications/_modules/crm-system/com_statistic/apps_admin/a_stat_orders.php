<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента статистики для CRM системи
 *
***/

$parent_select = $where = '';
$text = 0;
$managers = u_getUsers('rights_5_6',"u_rights = '5' OR u_rights = '6' ORDER BY u_rights ASC, u_fullname ASC");

$where = ord_filter_query(
	""
);


/** Отримання даних про замовлення з бази **/

$result = $sql->query("
	SELECT *
	FROM _orders INNER JOIN _clients ON o_cl_id=cl_id
	{$where}
");

if(!is_array($result) || empty($result)){

	$text = '<hr/>
	<br/>'._lang('Нет заказов! =\ ');

}else{

	if(isset($_POST['export'])){
		ini_set('display_errors','Off');
		//error_reporting(E_ALL);

		$csv_ar = $csv =
			_lang('Заказ').";".
			_lang('ФИО').";".
			_lang('Телефон').";".
			_lang('Email').";".
			_lang('Статус').";".
			_lang('Тип оплаты').";".
			_lang('Метка').";".
			_lang('Цена').";".
			_lang('Дата поступления').";".
			_lang('Дата оплаты').";".
			_lang('Группа стартует').";".
			_lang('Менеджер').";".
			_lang('Комментарий')."\n";



		foreach($result AS $cval){

			unset($csv_ar);

			$csv_ar['Курс'] = $cval['o_prod'];

			$csv_ar['ФИО'] = $cval['cl_fullname'];
			$csv_ar['Телефон'] = $cval['cl_tel'];
			$csv_ar['email'] = $cval['cl_email'];

			$csv_ar['Статус'] = !empty($cval['o_status'])?ord_statuses($cval['o_status']):'---';
			$csv_ar['Тип оплаты'] = !empty($cval['o_payment_type'])?ord_payment_type($cval['o_payment_type']):'---';

			$csv_ar['Метка'] = !empty($cval['o_mark'])?$cval['o_mark']:'---';
			//$csv_ar['Посадка'] = !empty($cval['o_lp_article'])?getURL('lp',$cval['o_lp_article']):'---';

			$csv_ar['Цена'] = $cval['o_price'];

			$csv_ar['Дата поступления'] = $cval['o_date_create']>100?date('d.m.Y H:i',$cval['o_date_create']):'---';
			$csv_ar['Дата оплаты'] = $cval['o_date_done']>100?date('d.m.Y',$cval['o_date_done']):'---';
			$csv_ar['Група стартує'] = $cval['o_date_group_start']>100?date('d.m.Y',$cval['o_date_group_start']):'---';

			$csv_ar['Менеджер'] = !isset($managers[$cval['o_u_id']]['u_fullname'])?'---':$managers[$cval['o_u_id']]['u_fullname'];
			$cval['o_note'] = str_replace("\n",' ',$cval['o_note']);
			$cval['o_note'] = str_replace("\t",' ',$cval['o_note']);
			$csv_ar['Комментарий'] = str_replace("\r",' ',$cval['o_note']);


			$csv .= implode(';',$csv_ar)."
";

		}
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=export_".date('d.m.Y_H.i').".csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		print iconv('UTF-8','cp1251//TRANSLIT',$csv);
		$PData->phpExit();
	}

	$text = '
	<hr/>
	<br/>
	'._lang('Количество заказов').': <b>'.count($result).'</b>
	<br/><br/>
	<form method="POST">
		<input name="export" type="submit" value="'._lang('Экспортировать .CSV').'">
	</form>
	';

}

/** Отримання даних для фільтра **/

$lp_array = $ref_array = array();
$lp_select = $ref_select = $status_select = $managers_select = $payType_select = '';

if(empty($_GET['tab'])) $_GET['tab'] = 'bookmark';

/** Отримуємо список Посадок і міток для Фільтра **/

$result = $sql->query("
	SELECT
		o_mark, r_title
	FROM _orders LEFT JOIN _referrals ON o_mark = r_mark
	GROUP BY o_mark
	"
);

foreach($result AS $cval){

	if(!empty($cval['o_mark'])  && !isset($ref_array[$cval['o_mark']])){

		$ref_array[$cval['o_mark']] = (!empty($cval['r_title'])?$cval['r_title']:$cval['o_mark']);

		$selected = '';
		if(!empty($_GET['referral']) && is_array($_GET['referral']) && in_array($cval['o_mark'],$_GET['referral'])) $selected = 'selected="selected"';

		$ref_select .= '<option '.$selected.' value="'.$cval['o_mark'].'">'.$ref_array[$cval['o_mark']].'</option>';

	}
}

$result = $sql->query("
	SELECT
		lp_article, lp_title
	FROM _landing_pages
	"
);

foreach($result AS $cval){

	if(!empty($cval['lp_article']) && !isset($lp_array[$cval['lp_article']])){

		$lp_array[$cval['lp_article']] = $cval['lp_title'];

		$selected = '';
		if(!empty($_GET['lpage']) && is_array($_GET['lpage']) && in_array($cval['lp_article'],$_GET['lpage'])) $selected = 'selected="selected"';

		$lp_select .= '<option '.$selected.' value="'.$cval['lp_article'].'">'.$cval['lp_title'].'</option>';

	}
}


if(is_array($managers)){
	foreach($managers AS $key => $val){

		$selected = '';
		if( !empty($_GET['manager']) && is_array($_GET['manager']) && in_array($key,$_GET['manager']) )
		$selected = 'selected="selected"';

		$managers_select .= '<option '.$selected.' value="'.$key.'">'.$val['u_fullname'].'</option>';
	}
}



foreach(ord_statuses('array', 'filter') AS $key => $status){

	$style = '';
	if($key==-1)$style='style="color:red;"';

	$selected = '';
	if( !empty($_GET['status']) && is_array($_GET['status']) && in_array($key,$_GET['status']) )
		$selected = 'selected="selected"';

	$status_select .= '<option '.$style.' '.$selected.' value="'.$key.'">'._lang($status).'</option>';

}

foreach(ord_payment_type('array') AS $key => $payType){

	$selected = '';
	if( !empty($_GET['payType']) && is_array($_GET['payType']) && in_array($key,$_GET['payType']) )
		$selected = 'selected="selected"';

	$payType_select .= '<option '.$selected.' value="'.$key.'">'.$payType.'</option>';

}

/** __________ Формування основного HTML коду __________ **/

$PData->content('Статистика Заказов','title');

$PData->content('
	<!-- Фільтр -->
	<form class="list">

		<input type="hidden" name="tab" value="filter"/>

		<label class="right_colmn" style="margin:0;">
			<b>'._lang('Дата оплаты').':</b><br/>
			<input type="date" name="date-done1" value="'.@$_GET['date-done1'].'"/> '._lang('до').' <input type="date" name="date-done2" value="'.@$_GET['date-done2'].'"/>
		</label>

		<label >
			<b>'._lang('Дата поступления').':</b><br/>
			<input type="date" name="date-create1" value="'.@$_GET['date-create1'].'"/> '._lang('до').' <input type="date" name="date-create2" value="'.@$_GET['date-create2'].'"/>
		</label>

		<p class="right_colmn">
			<label>
				<b>'._lang('Статусы').':</b><br/>
				<select class="multiselect" name="status[]" multiple="multiple">
					<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>

					'.$status_select.'
				</select>
			</label>

		</p>

		<p>
			<label>
				<b>'._lang('Метки').':</b><br/>
				<select class="multiselect" name="referral[]" multiple="multiple">
					<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
					'.$ref_select.'
				</select>
			</label>
		</p>
		<div class="clear"></div>
		<p class="right_colmn">
			<label>
				<b>'._lang('Тип оплаты').':</b><br/>
				<select class="multiselect" name="payType[]" multiple="multiple">
					<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
					'.$payType_select.'
				</select>
			</label>
		</p>

		'.
		u_ifRights(
			array(6,100),
			'<p >
				<label>
					<b>'._lang('Менеджер').':</b><br/>
					<select class="multiselect" name="manager[]" multiple="multiple">
						<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
						'.$managers_select.'
					</select>
				</label>

			</p>'
		)
		.'




		<!--p>
			<label>
				<b>'._lang('Посадки').':</b><br/>
				<select class="multiselect" name="lpage[]" multiple="multiple">
					<option style="background: #FFC;" value="">- '._lang('не указано').' -</option>
					'.$lp_select.'
				</select>
			</label>
		</p-->

		<input type="submit" class="btn" value="'._lang('Искать').'"/>
		<p class="greyText">
			! - '._lang('Зажмите Ctrl или Shift, чтобы выбрать несколько пунктов').'.<br/>
			! - '._lang('Чтобы найти нужный пункт мультименю, кликните в любом месте нужного вам меню, и нажмите на клавиатуре первую букву нужного вам пункта. Вы можете перемещаться по пунктах с одинаковой первой буквой, нажимая несколько раз на клавишу').'.
		</p>
	</form>
	<!-- // Фільтр -->

'.$text);