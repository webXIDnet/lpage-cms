<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Компонента статистики для CRM системи
 *  
***/

_add_hook('LPage_init_done', 'CRM_order_MBA_com_stat_LPage_init_page');
	
function CRM_order_MBA_com_stat_LPage_init_page($lp){
	
	global $sql;
	
	$stat = array();
	
	if(!empty($_COOKIE['stat'])){
		
		$stat = json_decode( base64_decode( urldecode( $_COOKIE['stat'] ) ) , TRUE);
		
		if(!is_array($stat)) $stat=array();
	}
	
	$today = strtotime(date('Y-m-d'));
	
	$st_key =  $today .  $lp['lp_id'];
	
	$st_hosts= '';
	
	if(!in_array($lp['lp_id'],$stat)){
		
		$st_hosts = ', st_hosts=st_hosts+1';
		
	}
	
	$sql->query("
		INSERT INTO `_statistic` 
			(st_key, st_lp_id, st_date) 
		VALUES 
			('"._protect($st_key)."','"._protect($lp['lp_id'])."','"._protect($today)."') 
		ON DUPLICATE KEY 
		UPDATE 
			st_hits=st_hits+1 {$st_hosts};
		",
		'query'
	);		
	
		
	$stat = array_merge($stat,array($lp['lp_id']));
	
	setcookie('stat', urlencode( base64_encode( json_encode($stat) ) ), ($today+24*60*60) );

}