<?php
if (PAGE_TYPE=='admin') {
    _add_action('adminPanel_controller', 'com_dbfie_init_admin', 1);
    _add_filter('crm_sidebar_submenu', 'com_dbfie_submenu', 3);
}

function com_dbfie_init_admin() {
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;
    switch (@$_GET['admin']) {
        case 'crm/dbfie':
            require_once __DIR__ . '/dbfie_select.php';
            return true;
            break;
        case 'crm/dbfie/import':
            require_once __DIR__ . '/dbfie_import.php';
            return true;
            break;
        case 'crm/dbfie/export':
            require_once __DIR__ . '/dbfie_export.php';
            return true;
            break;
    }
}

function com_dbfie_submenu($result) {
	global $PData;
	$vars_array = array(
        array(
            'title' => _lang('Импорт/Експорт DBF'), //ім"я пункта меню
            'url' => getURL('admin', 'crm/dbfie'), //посилання
            'active_pages' => array('crm/dbfie'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        )
    );

 	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100,4,5,6), $menu);

    return $result . $menu;
}