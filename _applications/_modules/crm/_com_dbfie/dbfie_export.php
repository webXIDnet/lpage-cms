<?php
/**
 * dbf file export
 * @author godex__ <godexxxx@gmail.com>
 * @version 1.0
 * @package com_dbfile
 */
global $sql;
$vars_array = array();

/**
 * Convert input string from 'windiows1215' to 'UTF-8'
 * @param $string input string
 * @return string output string
 */
function conUtf8to1251($string)
{
    return iconv('UTF-8', 'CP1251', $string);
}

/**
 * @param $File string DBF file
 * @param $encode integer codepage page code
 * "cp1251", # Russian Windows  = 201
 * "cp866", # Russian MS–DOS    = 101
 * if need more see https://github.com/cbaines/ruby-dbf/blob/master/lib/dbf/table/encodings.rb
 */
function writeDBFFileEncode($File, $encode)
{
    $db_handler = fopen($File, 'r+');
    fseek($db_handler, 29);
    fwrite($db_handler, chr($encode));
    fclose($db_handler);
}

//todo try save file to server use php://output
$db_file = CODE_FOLDER . '../media/files/export.dbf';//db file location
unlink($db_file);

$db_columns = array(//db headers config
    array("DATE", "D"),                 //дата документу
    array("CUSTOMER", "C", 100),        //Найменування клиента
    array("PHONE", "C", 30),            //Телефон клиента
    array("EMAIL", "C", 70),            //Е-мейл клиента
    array("KINDOFSERV", "C", 26),       //вид усклуги
    array("SERVICE", "C", 50),          //имя услуги
    array("AMOUNT", "N", 10, 3),        //количество
    array("DISCOUNT", "N", 12, 2),      //скидка
    array("SUM", "N", 12, 2),           //сума
    array("TOTALDISCO", "N", 12, 2),    //скидка всего по документу
    array("TOTALSUM", "N", 12, 2)       //сума всего по документу
);

$vars_array['url'] = $db_file;
dbase_create($db_file, $db_columns);//create db
$dbf_db = dbase_open($db_file, 2);//open db
$vars_array['count']=0;

if ($dbf_db) {
    $data_from_db = $sql->query('SELECT O.o_cl_id,C.cl_id,C.cl_tel,C.cl_email,C.cl_fullname,O.o_date_create,O.o_additional_data,O.o_prod,O.o_price ' .
        'FROM _orders as O LEFT OUTER JOIN _clients as C ON O.o_cl_id = C.cl_id; ', $type = 'array', $error = FALSE);//load data from db
    foreach ($data_from_db as $var_origin) {
        $var_converted = array();//converted to win1251
        $var_origin['o_additional_data'] = (array)json_decode($var_origin['o_additional_data']);//decript json
        $var_origin['DISCOUNT'] = $var_origin['o_additional_data']['Скидка'];
        $var_origin['vid_uslugi'] = $var_origin['o_additional_data']['Вид услуги'];

        foreach ($var_origin as $key => $value) {//convert
            if (!is_array($value)) {//to lose additional data
                $var_converted[$key] = conUtf8to1251($value);//convert
            }
        }
        $var_converted['o_date_create'] = date('Ymd', $var_converted['o_date_create']);
        $var_converted['TOTALDISCO'] = $var_converted['DISCOUNT'];
        $var_converted['TOTALSUM'] = $var_converted['o_price'] - $var_converted['TOTALDISCO'];

        dbase_add_record($dbf_db, array(//add record to db
            date($var_converted['o_date_create']),
            $var_converted['cl_fullname'],
            $var_converted['cl_tel'],
            $var_converted['cl_email'],
            $var_converted['vid_uslugi'],
            $var_converted['o_prod'],
            0,
            $var_converted['DISCOUNT'],
            $var_converted['o_price'],
            $var_converted['TOTALDISCO'],
            $var_converted['TOTALSUM'],
        ));
        $vars_array['count']++;
    }
    writeDBFFileEncode($db_file, 201);
}
dbase_close($dbf_db);

$PData->content(_lang('DBF Експорт'), 'title');
$PData->content($PData->getModTPL('admin/com_dbfie/dbfie_export', 'crm', $vars_array));