<?php

/**
 * dbf file import
 * @author godex__ <godexxxx@gmail.com>
 * @version 1.0
 * @package com_dbfile
 */

global $sql;

$dbf_file = $_FILES['dbf_file']['tmp_name'];
$vars_array = array();
/**
 * Convert input string from 'windiows1215' to 'UTF-8'
 * @param $string input string
 * @return string output string
 */
function con1251toUtf8($string)
{
    return trim(iconv('Windows-1251', 'UTF-8', $string));
}

$db = dbase_open($dbf_file, 0);//open DB

/*
 * Сделать перестройку
 */

$db_columns = array(//db headers config
    array("DATE", "D"),                 //дата документу
    array("CUSTOMER", "C", 100),        //Найменування клиента
    array("PHONE", "C", 30),            //Телефон клиента
    array("EMAIL", "C", 70),            //Е-мейл клиента
    array("KINDOFSERV", "C", 26),       //вид усклуги
    array("SERVICE", "C", 50),          //имя услуги
    array("AMOUNT", "N", 10, 3),        //количество
    array("DISCOUNT", "N", 12, 2),      //скидка
    array("SUM", "N", 12, 2),           //сума
    array("TOTALDISCO", "N", 12, 2),    //скидка всего по документу
    array("TOTALSUM", "N", 12, 2)       //сума всего по документу
);
$begin_time = time() - 1272000000 + floatval(microtime());

if ($db) {
    $record_numbers = dbase_numrecords($db);
    $vars_array['import_number_sum'] = $record_numbers;
    $vars_array['import_number'] = 0;
    $conflicts_id_arr = array();

    //load users from DB
    function get_users()
    {
        global $sql;
        $users_arr = $sql->query('SELECT C.cl_id,C.cl_fullname,C.cl_tel,C.cl_email ' .
            'FROM _clients AS C', $type = 'array', $error = FALSE);//load data from db
        $users_arr_by_id = array();
        foreach ($users_arr as $user) {
            $users_arr_by_id[$user['cl_id']]['cl_email'] = trim($user['cl_email']);
            $users_arr_by_id[$user['cl_id']]['cl_tel'] = trim($user['cl_tel']);
            $users_arr_by_id[$user['cl_id']]['cl_fullname'] = trim($user['cl_fullname']);
            $users_arr_by_id[$user['cl_id']]['cl_id'] = trim($user['cl_id']);
        }
        return $users_arr_by_id;
    }

    $users_arr_by_id = get_users();

    //Add line by line file
    for ($i = 1; $i <= $record_numbers; $i++) {
        $record_converted = array();
        $record = dbase_get_record_with_names($db, $i);//load record fom DB
        foreach ($record as $key => $value) {//convert
            $record_converted[$key] = con1251toUtf8($value);
        }

        //Search client if not found create
        //Ищем
        $found = array();
        foreach ($users_arr_by_id as $tmp_user) {
            if ($tmp_user['cl_tel'] == $record_converted['PHONE']) {
                $found[$tmp_user['cl_id']]['cl_email'] = $tmp_user['cl_email'];
                $found[$tmp_user['cl_id']]['cl_tel'] = $tmp_user['cl_tel'];
                $found[$tmp_user['cl_id']]['cl_fullname'] = $tmp_user['cl_fullname'];
                $found[$tmp_user['cl_id']]['cl_id'] = $tmp_user['cl_id'];
                continue;
            }
            if ($tmp_user['cl_email'] == $record_converted['EMAIL']) {
                $found[$tmp_user['cl_id']]['cl_email'] = $tmp_user['cl_email'];
                $found[$tmp_user['cl_id']]['cl_tel'] = $tmp_user['cl_tel'];
                $found[$tmp_user['cl_id']]['cl_fullname'] = $tmp_user['cl_fullname'];
                $found[$tmp_user['cl_id']]['cl_id'] = $tmp_user['cl_id'];
                continue;
            }
        }

        $rows = count($found);
        if ($rows < 1) {//if customer code not set create new customer
            $columns_value_array = array(
                'fullname' => $record_converted['CUSTOMER'],
                'tel' => $record_converted['PHONE'],
                'email' => $record_converted['EMAIL'],
                'date_update' => time()
            );
            $sql->insert($table = '_clients', $columns_value_array, $prefix = 'cl_');// create new record in $table

            $last_u_id = $sql->query('SELECT MAX(cl_id) FROM _clients', $type = 'array', $error = FALSE);//get id of created user
            $customID = array_shift($last_u_id);
            $customID = $customID['MAX(cl_id)'];

            $users_arr_by_id[$customID]['cl_email'] = trim($record_converted['EMAIL']);
            $users_arr_by_id[$customID]['cl_tel'] = trim($record_converted['PHONE']);
            $users_arr_by_id[$customID]['cl_fullname'] = trim($record_converted['CUSTOMER']);
            $users_arr_by_id[$customID]['cl_id'] = $customID;
        } elseif ($rows > 1) {
            foreach ($found as $tmp_user2) {
                $row = $tmp_user2;
                if (!in_array($row['cl_id'], $conflicts_id_arr)) {
                    $conflicts_id_arr[] = $row['cl_id'];
                    $vars_array['errors'][] = '<a target="_blank" href="' . SITE_FOLDER . 'admin/crm/clients/?id=' . $row['cl_id'] . '">Конфликтующие записи ID=' . $row['cl_id'] . ' ФИО=' . $row['cl_fullname'] . ' Телефон=' . $row['cl_tel'] . ' E-mail=' . $row['cl_email'] . '</a>';
                }
            }
            continue;
        } else {
            foreach ($found as $tmp_user3) {
                $customID = $tmp_user3['cl_id'];
            }
        }
        //Add date
        if (trim($record_converted['DATE']) == '') {//if date not set set current time
            $record_converted['DATE'] = time();
        } else {
            $record_converted['DATE'] = strtotime($record_converted['DATE']);//date convert to unix time
        }
        //collect additional array
        $additional['Вид услуги'] = $record_converted['KINDOFSERV'];
        $additional['Скидка'] = $record_converted['DISCOUNT'];

        $columns_value_array = array(
            'date_create' => $record_converted['DATE'],
            'date_done' => $record_converted['DATE'],
            'cl_id' => $customID,
            'prod' => $record_converted['SERVICE'],
            'price' => $record_converted['SUM'],
            'additional_data' => json_encode($additional),
            'status' => 0
        );

        $sql->insert($table = '_orders', $columns_value_array, $prefix = 'o_');// create new record in $table
        $vars_array['import_number']++;
        $vars_array['status'] = 'OK';
        /**/
    }
}
dbase_close($db);
unlink($dbf_file);
$end_time = time() - 1272000000 + floatval(microtime()) - $begin_time;
$vars_array['run_time'] = $end_time;
$PData->content(implode('<br>',$vars_array['errors']), 'message', FALSE);
$PData->content(_lang('DBF Импорт'), 'title');
$PData->content($PData->getModTPL('admin/com_dbfie/dbfie_import', 'crm', $vars_array));