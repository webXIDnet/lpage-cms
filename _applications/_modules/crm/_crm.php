<?php

if (PAGE_TYPE == 'api') require_once 'crm_api.php'; //_add_action('defined_modules', 'crm_api_defined');

/*function crm_api_defined() {

    require_once 'crm_api.php';
}*/

require_once 'crm_fn.php';

_add_action('defined_modules', 'CRM_defined_modules');

function CRM_defined_modules()
{
    if (PAGE_TYPE == 'admin') {
        global $sql;

        $sql->db_table_installer('CRM_clients', '12.09.2015',
            array(
                '_clients' => array(
                    'cl_id' => "`cl_id` int(11) NOT NULL AUTO_INCREMENT",
                    'cl_fullname' => "`cl_fullname` varchar(256) NOT NULL",
                    'cl_tel' => "`cl_tel` varchar(25) NOT NULL",
                    'cl_email' => "`cl_email` varchar(100) NOT NULL",
                    'cl_attachment' => "`cl_attachment` text NOT NULL",
                    'cl_adress' => "`cl_adress` varchar(512) NOT NULL",
                    'cl_date_update' => "`cl_date_update` int(11) NOT NULL",
                    'cl_brawser_lang' => "`cl_brawser_lang` varchar(5) NOT NULL DEFAULT 'NAN'",
                    'cl_additional_data' => "`cl_additional_data` text NOT NULL",
                    'cl_birthday' => "`cl_birthday` date NOT NULL",
                    'CREATE' => "`cl_id` int(11) NOT NULL AUTO_INCREMENT, `cl_tel` varchar(25) NOT NULL, `cl_email` varchar(100) NOT NULL, PRIMARY KEY (`cl_id`),  UNIQUE KEY `cl_tel_AND_cl_email` (`cl_tel`, `cl_email`)"
                )
            )
        );
    }
}

_add_hook('adminPanel_controller', 'CRM_admin_controller');

function CRM_admin_controller()
{
    global $PData,
           $admin,
           $user,
           $sql,
           $sqlTPL,
           $contTPL,
           $htmlTPL;

    switch (@$_GET['admin']) {
        case '': {
            if (u_ifRights(array(3, 4, 5, 6, 100), TRUE)) {
                $PData->redirect(getURL('admin', 'crm/orders'));
            }
            break;
        }
    }
}

if (PAGE_TYPE == 'admin') {
    _add_filter('adminPanel_sidebar_menu', 'crm_sidebar_menu', 1);
}

function crm_sidebar_menu($result)
{
    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('CRM'), //ім"я пункта меню
            'url' => FALSE, //посилання
            'submenu_filter' => 'crm_sidebar_submenu', //хук для підключення пунктів підменю
            'active_pages' => _run_filter('crm_active_sidebar_menu',
                array(
                    'crm',
                    'crm/orders',
                    'crm/clients',
                    'crm/stat',
                    'crm/referrals',
                    'crm/dbfie'
                )
            ), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => TRUE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100, 5, 6, 4), $menu);

    return $result . @$menu;
}

_add_filter('admin_panel_header', 'crm_header');

function crm_header($result)
{

    $text = "
    <link rel='stylesheet' href='" . SITE_FOLDER . "js/chosen/chosen.css'/>
    <link rel='stylesheet' href='" . SITE_FOLDER . "js/jquery-ui/jquery-ui.css'/>
    <script src='" . SITE_FOLDER . "js/chosen/chosen.jquery.js'></script>
    <script src='" . SITE_FOLDER . "js/jquery-ui/jquery-ui.js'></script>
    <link rel='stylesheet' href='" . SITE_FOLDER . "js/jquery-ui/jquery-ui-timepicker-addon.css'/>
    <script src='" . SITE_FOLDER . "js/jquery-ui/jquery-ui-timepicker-addon.js'></script>
    <script type='text/javascript'>
        $(document).ready(function(){
            $('.datetime').datetimepicker({
                dateFormat: 'dd.mm.yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '1970:2015'
            });
            $('.datepicker').datepicker({
                dateFormat: 'dd.mm.yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '1970:2015'
            });
            $('.multiselect').chosen({
                width: '100%',
                placeholder_text_single: 'Выберите опцию',
                placeholder_text_multiple: 'Выберите опции'
            });
        $('.delete_button').click(function(e){
            e.preventDefault();
            var delete_array = [];
            $('.delete:checked').each(function(){
                delete_array.push($(this).val());
            });
            $('.delete_vals').val(delete_array.join(','));
            // console.log(delete_array.join(','));
            $(this).parents('form').submit();
        });
        $('.combine_button').click(function(e){
        if (confirm('Внимание в случае конфликтов данных пользователей предпочтение будет отдано данным пользователя что обновлялся самым последним. Продолжить?')) {
            e.preventDefault();
            var delete_array = [];
            $('.delete:checked').each(function(){
                delete_array.push($(this).val());
            });
            $('.combine_vals').val(delete_array.join(','));
            // console.log(delete_array.join(','));
            $(this).parents('form').submit();
            } else {
              alert('Вы нажали кнопку отмена')
            }
        });
        });
    </script>
    ";

    return $result . $text;

}
