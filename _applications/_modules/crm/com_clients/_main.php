<?php

if (PAGE_TYPE=='admin') {
    _add_action('adminPanel_controller', 'com_clients_init_admin', 1);
    _add_filter('crm_sidebar_submenu', 'com_clients_submenu', 2);
}

function com_clients_init_admin() {
	global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;

	switch (@$_GET['admin']) {
        case 'crm/clients': {
        	if (isset($_POST['editClient'])) {
            	include __DIR__ . '/clients_edit.php';
    		}
    		if (isset($_POST['addClient'])) {
            	include __DIR__ . '/clients_add.php';
    		}
        	require_once __DIR__ . '/clients_list.php';
            return true;
            break;
        }


    }
}

function com_clients_submenu($result) {
	global $PData;
	$vars_array = array(
        array(
            'title' => _lang('Клиенты'), //ім"я пункта меню
            'url' => getURL('admin', 'crm/clients'), //посилання
            'active_pages' => array('crm/clients'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        )
    );

 	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100,4,5,6), $menu);

    return $result . $menu;
}