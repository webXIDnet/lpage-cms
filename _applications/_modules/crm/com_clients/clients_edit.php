<?php

global $sql, $PData;

if (isset($_POST['editClient'])) {	
	$data = $_POST['editClient'];
	$client_id = $data['client_id'];
	unset($data['client_id']);
	$data['birthday'] = date('Y-m-d',strtotime($data['birthday']));
	if (isset($_POST['refresh_cl_date']))
		$data['date_update'] = time();
	$data['additional_data'] = json_encode($data['additional_data']);

	$sql->update('_clients','cl_id='.$client_id,$data,'cl_');

	$PData->content(_lang('Данные клиента успешно обновлены!'),'message', true);
	
}

if (isset($_POST['formsubmit-add'])) {
	$data = $_POST['addOrder'];
	$data['date_create'] = time();
	$data['date_next_touch'] = strtotime($data['date_next_touch']);
	
	$sql->insert('_orders', $data, 'o_');

	$PData->content(_lang('Заявка успешно добавлена!'),'message', true);
}