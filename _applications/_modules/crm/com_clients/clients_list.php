<?php
global $sql, $meta;
//vars init
$where = array();
$where_str = '';
// _dump($_POST);
if (isset($_POST['delete_vals'])) {

	$sql->delete('_clients','cl_id IN ('._protect($_POST['delete_vals']).')');
}

if (isset($_GET['search'])) {
	//add new elements to filter array
	$where []= "cl_fullname LIKE '%".$_GET['search']."%'";
	$where []= "cl_tel LIKE '%".$_GET['search']."%'";
	$where []= "cl_email LIKE '%".$_GET['search']."%'";
}

if (isset($_GET['id'])) {
	$where = array("cl_id='"._protect($_GET['id'])."'");
}

if (count($where) > 0)
	$where_str = ' WHERE '.implode(' OR ',$where);

//creatie pagination query and limit
$pagination = _pagination('_clients', 'cl_id', $where_str);
//main query
$query = "SELECT * FROM _clients ".$where_str." ".$pagination['limit'];
// _dump($query);

$clients_list = $sql->query($query);
foreach ($clients_list as $key => $client) {
	$clients_list[$key]['cl_additional_data'] = json_decode($client['cl_additional_data'],true);
	$clients_list[$key]['cl_birthday'] = strtotime($client['cl_birthday']);
	// $clients_list[$key]['cl_date_update'] = json_decode($client['cl_date_update']);
}

$orders_list = $sql->query("SELECT * FROM _orders LEFT JOIN _users ON (o_u_id=u_id) ");

$statuses = $meta->val('apSet_crm-system','mod_option');
//sort statuses array by value with saving keys
asort($statuses['statuses']);

$managers = $sql->query("SELECT * FROM _users WHERE u_rights='6'");

$vars_array = array(
	'clients' => $clients_list,
	'pagination' => $pagination['html'],
	'orders' => $orders_list,
	'statuses' => $statuses['statuses'],
	'managers' => $managers
);

$PData->content(_lang('Список клиентов'), 'title');
$PData->content($PData->getModTPL('admin/com_clients/clients_list','crm', $vars_array));