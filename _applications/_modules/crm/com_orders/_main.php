<?php

if (PAGE_TYPE=='admin') {
    _add_action('adminPanel_controller', 'com_orders_init_admin', 1);
    _add_filter('crm_sidebar_submenu', 'com_orders_submenu', 1);
}

function com_orders_init_admin() {
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;

    switch (@$_GET['admin']) {
        case 'crm/orders': {
            if (isset($_POST['formsubmit-add'])) {
                include __DIR__ . '/order_add.php';
            }
            if (isset($_GET['editOrder']) || isset($_POST['editClient'])) {
                    include __DIR__ . '/order_edit.php';
            }
            require_once __DIR__ . '/orders_list.php';
            return true;
            break;
        }
    }

}
function com_orders_submenu($result) {
	global $PData;
	$vars_array = array(
        array(
            'title' => _lang('Очередь заявок'), //ім"я пункта меню
            'url' => getURL('admin', 'crm/orders'), //посилання
            'active_pages' => array('crm/orders'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        )
    );

 	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100,4,5,6), $menu);

    return $result . $menu;
}
