<?php
global $sql, $PData, $new_order_id;
$client_id = 0;
$time = time();
if (isset($_POST['addClient'])) {

	$data = $_POST['addClient'];
	
	if ($data['email'] == '') $data['email'] = 'NON_'.$time;
	if ($data['tel'] == '') $data['tel'] = 'NON_'.$time;
	if (!isset($data['birthday'])) $data['birthday'] = '0000-00-00';
	$data['additional_data'] = json_encode($data['additional_data']);
	$sql->insert('_clients',$data,'cl_',true,false);
	// if (empty($client_id)) { = 
		$cval = $sql->query("SELECT cl_id, cl_email, cl_tel FROM _clients WHERE cl_email='".$data['email']."' OR cl_tel='".$data['tel']."'",'value');
		$client_id = $cval['cl_id'];
	// }
	// _dump($client_id);
	$PData->content(_lang('Клиент успешно добавлен!'),'message', true);
}
unset($data);
if (isset($_POST['addOrder'])) {
	$data = $_POST['addOrder'];
	$data['date_create'] = $time;
	$data['date_next_touch'] = strtotime($data['date_next_touch']);
	// _dump($data['additional_data']);
	$data['additional_data'] = json_encode($data['additional_data']);
	$data['cl_id'] = $client_id;
	// _dump($data);
	$new_order_id = $sql->insert('_orders',$data,'o_',false,true);
	$PData->content(_lang('Заявка успешно добавлена!'),'message', true);
}
