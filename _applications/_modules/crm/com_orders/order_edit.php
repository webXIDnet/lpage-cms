<?php
global $sql, $PData;
$client_id = 0;
// _dump($_POST);
if (isset($_POST['editClient'])) {	
	$data = $_POST['editClient'];
	$client_id = $data['client_id'];
	unset($data['client_id']);
	if (isset($_POST['refresh_cl_date']))
		$data['date_update'] = time();
	$data['birthday'] = date('Y-m-d',strtotime($data['birthday']));

	$data['additional_data'] = json_encode(array_protect($data['additional_data']));
	// _dump($data);
	$sql->update('_clients','cl_id='.$client_id,$data,'cl_');
	$PData->content(_lang('Данные клиента успешно обновлены!'),'message', true);
	
}
unset($data);
if (isset($_POST['editOrder'])) {	
	$data = $_POST['editOrder'];
	$order_id = $data['order_id'];
	unset($data['order_id']);
	$data['date_group_start'] = strtotime($data['date_group_start']);
	$data['date_done'] = strtotime($data['date_done']);
	$data['date_next_touch'] = strtotime($data['date_next_touch']);
	// _dump($data);
	$data['additional_data'] = json_encode(array_protect($data['additional_data']));
	// _dump($data);
	$sql->update('_orders','o_id='.$order_id,$data,'o_');
	$PData->content(_lang('Заявка успешно обновлена!'),'message', true);
}
