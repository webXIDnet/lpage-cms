<?php
global $sql, $meta;
//vars init
$where = array();
$where_str = '';
$mark_list = array();
if (isset($_POST['delete_vals'])) {
	// var_dump(explode(',',$_POST['delete_vals']));
	if ($_POST['delete_vals'] != '') {
		$sql->delete('_orders','o_id IN ('._protect($_POST['delete_vals']).')');
		$PData->content(_lang('Выбранные заявки успешно удалены!'),'message',true);
		// return true;
	} else {
		$PData->content(_lang('Выберите хотя бы одну заявку!'),'message',false);
		// return true;
	}
}
//if isset search string in GET
if (isset($_GET['search'])) {
	//add new elements to filter array
	$where []= "cl_fullname LIKE '%".$_GET['search']."%'";
	$where []= "cl_tel LIKE '%".$_GET['search']."%'";
	$where []= "cl_email LIKE '%".$_GET['search']."%'";
	$where []= "o_id LIKE '%".$_GET['search']."%'";
}
//same for status id
if (isset($_GET['status'])) {
	$where []= "o_status='".$_GET['status']."'";
}
//if has filter array
if (isset($_POST['filters'])) {

	$filter_tmp_val = array();
	//filter by date
	if ($_POST['filters']['date_create'][0] != '')
	$filter_tmp_val []= "o_date_create > '".strtotime($_POST['filters']['date_create'][0])."'";
	if ($_POST['filters']['date_create'][1] != '')
	$filter_tmp_val []= "o_date_create < '".strtotime($_POST['filters']['date_create'][1])."'";

	if ($_POST['filters']['date_done'][0] != '')
	$filter_tmp_val []= "o_date_create > '".strtotime($_POST['filters']['date_done'][0])."'";
	if ($_POST['filters']['date_done'][1] != '')
	$filter_tmp_val []= " o_date_done < '".strtotime($_POST['filters']['date_done'][1])."'";

	if ($_POST['filters']['date_done'][0] != '' && $_POST['filters']['date_done'][1] != '')
	$filter_tmp_val []= "o_date_done BETWEEN '".strtotime($_POST['filters']['date_done'][0])."' AND '".strtotime($_POST['filters']['date_done'][1])."'";
	unset($_POST['filters']['date_create']);
	unset($_POST['filters']['date_done']);


	foreach ($_POST['filters'] as $filter_key => $filter_value) {
		if (is_array($filter_value)) {
			foreach ($filter_value as $key => $value) {
				$filter_tmp_val []= 'o_'.$filter_key."='".$value."'";
			}
		}


	}
	if (count($filter_tmp_val) > 0)
	$where []= implode(' AND ',$filter_tmp_val);
}

if (isset($_GET['id'])) {
	$where = array("o_id='"._protect($_GET['id'])."'");
}


//imploding where array into string
if (count($where) > 0)
	$where_str = ' WHERE '.implode(' OR ',$where);

$where_str .= " ORDER BY o_date_create DESC ";
//creatie pagination query and limit
$pagination = _pagination('_orders LEFT JOIN _clients ON (o_cl_id=cl_id) LEFT JOIN _users ON (o_u_id=u_id)', 'o_id', $where_str);
//main query
$query = "SELECT * FROM _orders LEFT JOIN _clients ON (o_cl_id=cl_id) LEFT JOIN _users ON (o_u_id=u_id) ".$where_str." ".$pagination['limit'];
// _dump($query);

$orders_list = $sql->query($query);
//get managers list
$managers = $sql->query("SELECT * FROM _users WHERE u_rights='6'");
//get statuses list
$statuses = $meta->val('apSet_crm-system','mod_option');
//sort statuses array by value with saving keys
@asort($statuses['statuses']);
foreach ($orders_list as $key => $value) {
	if ($value['o_mark'] == '')
		$orders_list[$key]['o_mark'] = '---';
	else
		$mark_list []= $value['mark'];
	// _dump(json_decode($value['o_additional_data'],true));
	$orders_list[$key]['o_additional_data'] = json_decode($value['o_additional_data'],true);
	$orders_list[$key]['cl_additional_data'] = json_decode($value['cl_additional_data'],true);
	$orders_list[$key]['o_date_create'] = $value['o_date_create'] % 100000000 == $value['o_date_create'] ? 0 : $value['o_date_create'];
	$orders_list[$key]['o_date_done'] = $value['o_date_done'] % 100000000 == $value['o_date_done'] ? 0 : $value['o_date_done'];
	$orders_list[$key]['o_date_group_start'] = $value['o_date_group_start'] % 100000000 == $value['o_date_group_start'] ? 0 : $value['o_date_group_start'];
	$orders_list[$key]['o_date_next_touch'] = $value['o_date_next_touch'] % 100000000 == $value['o_date_next_touch'] ? 0 : $value['o_date_next_touch'];
	$orders_list[$key]['cl_birthday'] = strtotime($value['cl_birthday']);

}
//format unique mark list
array_unique($mark_list);
//get landings list
$titles_temp = $sql->query("SELECT * FROM _orders GROUP BY o_prod");
$titles = array();
foreach ($titles_temp as $key => $value) {
	$titles []= $value['o_prod'];
}


$vars_array = array(
	'orders' => $orders_list,
	'statuses' => $statuses['statuses'],
	'managers' => $managers,
	'titles' => $titles,
	'marks' => $mark_list,
	'pagination' => $pagination['html']
);


$PData->content(_lang('Заказы'),'title');
$PData->content($PData->getModTPL('admin/com_orders/orders_list','crm', $vars_array));