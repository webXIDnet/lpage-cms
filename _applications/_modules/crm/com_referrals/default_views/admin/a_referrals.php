<?php
global $PData;
$result = $vars_array['result'];
$text = '';

if (!is_array($result) || empty($result)) {
    $text = '<b>' . _lang('Реферралов не найдено') . '</b><br/> =\ ';
} else {

    foreach ($result AS $cval) {
        $r_id = $cval['r_id'];
        $text .=
            '
<tr>
            <td>
              <label>
                <input type="checkbox" name="itemID[' . $r_id . ']"> ' . $r_id . '
              </label>
            </td>
            <td class="td-link" data-toggle="modal" data-target=".user-modal' . $r_id . '">
              <b>' . $cval['r_title'] . '</b>
            </td>
            <td class="td-link" data-toggle="modal" data-target=".user-modal' . $r_id . '">
              ' . $cval['r_mark'] . '
            </td>
            <td class="td-link" data-toggle="modal" data-target=".user-modal' . $r_id . '">
              ' . $cval['r_emails'] . '
            </td>
        </tr>

		<tr>
			<div class="modal fade user-modal' . $r_id . '" tabindex="-1" role="dialog" aria-hidden="true">
            	<div class="modal-dialog">
              		<div class="modal-content">
                		<div class="modal-header">
                  			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  				<span aria-hidden="true">&times;</span>
                  			</button>
                  			<h4 class="modal-title">'._lang('Редактирование параметров метки').'</h4>
                		</div>
                		<div class="modal-body">
                  			<form method="POST">
                  				<input type="hidden" name="itemID" value="'.$r_id.'"/>
                  				<p><b>'._lang('Название метки').'</b></p>
                    			<input type="text" name="editItem[title]" value="' . $cval['r_title'] . '"/>

                  				<p><b>'._lang('Метка').'</b></p>
                    			<input type="text" name="editItem[mark]" value="' . $cval['r_mark'] . '"/>

								<p><b>'._lang('Email').':</p></b>
								<textarea name="editItem[emails]">'.$cval['r_emails'].'</textarea>

								<input class="button" name="formsubmit-edit" type="submit" value="Save">
                			</form>
                		</div>
              		</div>
            	</div>
          	</div>

		</tr>';
    }

    $text = '
	<form method="POST">
	<div class="table-holder">
		<table id="tableEdit" class="table">
			<thead>
					<th id="head-tableList" class="table-10">' . _lang('Выбрать') . '</th>
					<th id="head-tableList" class="table-40">' . _lang('Название') . '</th>
					<th id="head-tableList" class="table-10">' . _lang('Метка') . '</th>
					<th id="head-tableList" class="table-40">' . _lang('Email') . '</th>
				</thead>
            <tbody>
				' . $text . '
			</tbody>
		</table>
		</div>
		<input type="submit" class="button" name="delSubmit" value="' . _lang('Удалить') . '"/>
	</form>
	';
}

echo '
	<form class="list" method="POST">
		<p><b>+ '._lang('Добавить реферала').':</b></p>
		<input placeholder="'._lang('Название').'" type="text" name="newRef[title]" value=""/>
		<input placeholder="'._lang('Метка').'" type="text" name="newRef[mark]" value=""/>
		<textarea placeholder="'._lang('Email партнера').'" type="text" name="newRef[emails]"></textarea>
		<input type="submit" class="btn" value="'._lang('Добавить').'"/>
	</form>
	<div class="line"></div>
	<!-- Пошук -->
	<form class="list">
		<p><b>' . _lang('Поиск') . ':</b></p>
		<input placeholder="' . _lang('ФИО / email / уровень доступа') . '" type="text" name="search" value="' . $PData->_GET('search') . '"/>
<input type="checkbox" ' . ($PData->_GET('find_rights') == 'true' ? 'checked="checked"' : '') . ' name="find_rights" value="true"/>
			<label>
				- ' . _lang('Искать только по уровню доступа') . '
			</label>
			<br>
		<input type="submit" class="button" value="' . _lang('Искать') . '"/>
	</form>
	<!-- // Пошук -->
	<div class="line"></div>
	    ' . $text . '

'.$vars_array['pagination'];