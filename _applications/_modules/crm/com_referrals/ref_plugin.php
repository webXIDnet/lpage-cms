<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модель Реферальної системи
 *
***/

_add_filter('trig_OrderResiving', 'ref_SendOrder');

function ref_SendOrder($result,$vars){

	if($vars=='false')return;

	parse_str($vars,$array);

	$page_url=PAGE_URL;

	if(!empty($_SERVER['HTTP_REFERER']))$page_url=@$_SERVER['HTTP_REFERER'];

	if(isset($_POST['ordersubmit']) && !defined('RF_ISSET_POST')){

		define('RF_ISSET_POST','TRUE');

		global 	$sql,
				$meta;
		$userSubject = $userMessage = '';

		if(!empty($array['lp_id'])){

			$cval = $sql->query(
				"SELECT lp_html,lp_title
				FROM _landing_pages
				WHERE lp_id = '"._protect($array['lp_id'])."'
				",
				'value'
			);
			$userSubject = @$cval['lp_title'];
			$userMessage = @$cval['lp_html'];
		}elseif(!empty($array['p_id'])){

			$cval = $sql->query(
				"SELECT p_text,p_title
				FROM _posts
				WHERE p_id = '"._protect($array['p_id'])."'
				",
				'value'
			);
			$userSubject = @$cval['p_title'];
			$userMessage = @$cval['p_text'];
		}

		$userSubject = _shortCode($userSubject);
		$userMessage = _shortCode($userMessage);

		if(empty($_POST['rf_name']))$_POST['rf_name'] = _lang('Будущий Студент');

		$userSubject = _sharpCode(
				$userSubject,
				array(
					'#NAME#'=>@$_POST['rf_name'],
					'#PHONE#'=>@$_POST['rf_phone'],
					'#EMAIL#'=>@$_POST['rf_email'],
				)
			);

		$userMessage = _sharpCode(
				$userMessage,
				array(
					'#NAME#'=>@$_POST['rf_name'],
					'#PHONE#'=>@$_POST['rf_phone'],
					'#EMAIL#'=>@$_POST['rf_email'],
				)
			);

		$adminMessage = 'Страница, на которой была оставлена заявка: '.$page_url.'<br/><br/>';

		$wpor = $_POST;

		if(isset($wpor) && is_array($wpor)){
		    foreach($wpor as $key => $value){

				if(!stristr($key,'rf_') || stristr($key,'submit'))continue;

				$adminMessage .= '<b>'._lang(str_replace('rf_','',$key)).':</b> '.$value.'<br/>';

		    }
	    }

		$adminMessage .=
			'<br>Пользователь пришел на сайт по ссылке: "'.@$_COOKIE['url'].'"'.
			'<br><br>Переход осуществлен с адреса "'.@$_COOKIE['referral'].'"';


		/** Відправка листа адміністрації про заявку **/

		$to = ref_getMails();

		_eMail($to, _lang('Поступила новая заявка'), $adminMessage,$meta->val('contact_name')." <no-reply@"._getDomane(HOMEPAGE).">");


		/** Відправка листа клієнту про заявку **/


		if(empty($userSubject))$userSubject = _lang('Вы успешно отправили форму');

		if(!empty($userMessage))
			_eMail($_POST['rf_email'], $userSubject, $userMessage,$meta->val('contact_name')." <no-reply@"._getDomane(HOMEPAGE).">");


	}elseif(!isset($_POST['ordersubmit'])){

		global 	$user,
				$PData;

		if($user->rights<2){

			$url = HOMEPAGE;

			if(!empty($_SERVER['HTTP_REFERER']))$url = $_SERVER['HTTP_REFERER'];

//			$PData->redirect($url,'301');
		}
	}
}

function ref_SetCookies(){

	global $PData;

	$url = parse_url(PAGE_URL);

	if(isset($url['query'])){
		parse_str($url['query'],$query);
		unset($query['partner']);
		$query = http_build_query($query);
		if(!empty($query))$query = '?'.$query;
		if(!empty($url['fragment']))$url['fragment'] = '#'.$url['fragment'];
		$url1 = @$url['scheme'].'://'.@$url['host'].@$url['path'].@$query.@$url['fragment'];
	}

	$time = time()+60*60*24*30;

	if( !empty($_GET['actionpay']) ){

		setcookie('mark', 'actionpay', $time);
		setcookie('referral', $_SERVER['HTTP_REFERER'], $time);
		setcookie('url', PAGE_URL, $time);

		setcookie('actionpay', 'actionpay='.$_GET['actionpay'], $time);

		if(!empty($url))
			$PData->redirect($url,'301');

	}elseif(isset($_COOKIE['mark']) && $_COOKIE['mark']!='actionpay' || !isset($_COOKIE['mark'])){

		if( !empty($_GET['partner']) && (empty($_COOKIE['mark']) || (isset($_COOKIE['mark']) && $_COOKIE['mark']!=$_GET['partner'])) ){

			setcookie('mark', $_GET['partner'], $time);
			setcookie('referral', @$_SERVER['HTTP_REFERER'], $time);
			setcookie('url', PAGE_URL, $time);
		}
	}
	if(!empty($url1) && isset($url1) && $url1!=PAGE_URL){
		$PData->redirect($url1,'301');
	}
}

_add_hook('defined_all_processes', 'ref_SetCookies');

function echoJSActionpay(){

	if(isset($_COOKIE['mark']) && $_COOKIE['mark']=='actionpay'){

		echo '<script language="JavaScript" type="text/javascript">
			var id=4618;
			var actionpay=\''.str_replace('actionpay=', '', $_COOKIE['actionpay']).'\';
			</script>
			<script language="JavaScript" type="text/javascript" src="http://n.actionpay.ru/js/ok.js"></script>';

	}
}