<?php

if (PAGE_TYPE=='admin') {
    _add_action('adminPanel_controller', 'com_stat_init_admin', 1);
    _add_filter('crm_sidebar_submenu', 'com_stat_submenu', 2);
}

function com_stat_init_admin() {
	global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;

	switch (@$_GET['admin']) {
        case 'crm/stat': {
        	require_once __DIR__ . '/stat.php';
            return true;
            break;
        }


    }
}

function com_stat_submenu($result) {
	global $PData;
	$vars_array = array(
        array(
            'title' => _lang('Статистика'), //ім"я пункта меню
            'url' => getURL('admin', 'crm/stat'), //посилання
            'active_pages' => array('crm/stat'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        )
    );

 	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100,5,6), $menu);

    return $result . $menu;
}