<?php

global $PData, $sql;
$where = array();
$where_str = '';
if (isset($_POST['date_start']) && $_POST['date_start'] != '') {
	$where []= "(o_date_create > '" . strtotime($_POST['date_start']) . "' OR o_date_create = '" . strtotime($_POST['date_start']) . "')";
}
if (isset($_POST['date_end']) && $_POST['date_end'] != '') {
	$where []= "(o_date_create < '" . (strtotime($_POST['date_end']) + (24*60*60 - 1) ) . "' OR o_date_create = '" . (strtotime($_POST['date_end']) + (24*60*60 - 1) ) . "')";
}

if (!empty($where))
$where_str = " WHERE " . implode(' AND ', $where);

$orders = array();
if (isset($_POST['find'])) {
	$query = "SELECT COUNT(*) FROM _orders " . $where_str;
	// _dump($query);
	$orders = $sql->query($query, 'value');
}

if(isset($_POST['export'])){

		$query = "SELECT * FROM _orders LEFT JOIN _clients ON (o_cl_id=cl_id) " . $where_str;
		$managers = u_getUsers('rights_5_6',"u_rights = '5' OR u_rights = '6' ORDER BY u_rights ASC, u_fullname ASC");
		$orders = $sql->query($query);

		$csv =
			_lang('Заказ').";".
				_lang('Вид услуг').";".
			_lang('ФИО').";".
			_lang('Телефон').";".
			_lang('Email').";".
			_lang('Статус').";".
			_lang('Цена').";".
			_lang('Дата поступления').";".
			_lang('Менеджер').";".
				_lang('Откуда узнал').";".
				_lang('Скидка').";".
				_lang('Вид связи').";".
				_lang('Город').";".
				_lang('Домашний адрес').";".
			_lang('Комментарий')."\n";


		foreach($orders AS $cval){

			$csv_ar = array();
			$cval['o_additional_data'] = json_decode($cval['o_additional_data'], TRUE);
			$cval['cl_additional_data'] = json_decode($cval['cl_additional_data'], TRUE);

			$csv_ar['Курс'] = $cval['o_prod'];
			$csv_ar['Вид услуг'] = @$cval['o_additional_data']['Вид услуг'];
			$csv_ar['ФИО'] = $cval['cl_fullname'];
			$csv_ar['Телефон'] = $cval['cl_tel'];
			$csv_ar['email'] = $cval['cl_email'];

			$csv_ar['Статус'] = !empty($cval['o_status'])?ord_statuses($cval['o_status']):'---';


			$csv_ar['Цена'] = $cval['o_price'];

			$csv_ar['Дата поступления'] = $cval['o_date_create']>100?date('d.m.Y H:i',$cval['o_date_create']):'---';


			$csv_ar['Менеджер'] = !isset($managers[$cval['o_u_id']]['u_fullname'])?'---':$managers[$cval['o_u_id']]['u_fullname'];

			$csv_ar['Откуда узнал'] = @$cval['o_additional_data']['Откуда узнал'];

			$csv_ar['Скидка'] = @$cval['o_additional_data']['Скидка'];
			$csv_ar['Вид связи'] = @$cval['o_additional_data']['Вид связи'];

			$csv_ar['Город'] = @$cval['cl_additional_data']['Город'];
			$csv_ar['Домашний адрес'] = @$cval['cl_additional_data']['Домашний адрес'];

			$cval['o_note'] = str_replace("\n",' ',$cval['o_note']);
			$cval['o_note'] = str_replace("\t",' ',$cval['o_note']);
			$csv_ar['Комментарий'] = str_replace("\r",' ',$cval['o_note']);

			$csv .= implode(';',$csv_ar)."\n";

		}
		// _dump(mb_convert_encoding($csv, 'cp1251', 'UTF-8'));exit;
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=export_".date('d.m.Y_H.i').".csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		// print $csv; exit;
		print mb_convert_encoding($csv, 'cp1251', 'UTF-8');
		// print iconv('utf-8','cp1251//TRANSLIT',$csv);
		$PData->phpExit();
	}

if (!isset($orders['COUNT(*)'])) {
	$orders['COUNT(*)'] = 0;
}

$vars_array = array(
	'date_start' => _protect($PData->_POST('date_start')),
	'date_end' => _protect($PData->_POST('date_end')),
	'count' => $orders['COUNT(*)']
	);

$PData->content(_lang('Статистика'), 'title');
$PData->content($PData->getModTPL('admin/com_stat/stat', 'crm', $vars_array));