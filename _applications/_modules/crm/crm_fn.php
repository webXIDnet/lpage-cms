<?php

function ord_statuses($val=FALSE, $type='select'){

	global 	$meta,
			$user;

	$statuses = $meta->val('apSet_crm-system','mod_option');

	$array = $statuses['statuses'];

	if(isset($array[$val])){
		return $array[$val];
	}elseif(!$val){
		return '---';
	}else{
		switch($type){
			case'select':
				if(
					isset($statuses['rights']['select'][$user->rights]) &&
					is_array($statuses['rights']['select'][$user->rights])
				){
					unset($array);
					foreach($statuses['rights']['select'][$user->rights] AS $key){
						if(empty($statuses['statuses'][$key]))continue;
						$array[$key] = $statuses['statuses'][$key];
					}
				}
				if( $user->rights == 100){
					unset($array['NAN']);
					$array[0] = _lang('Не определен');
				}
			break;
			case'filter':
				if(
					isset($statuses['rights']['bookmark'][$user->rights]) &&
					is_array($statuses['rights']['bookmark'][$user->rights])
				){
					unset($array);
					foreach($statuses['rights']['bookmark'][$user->rights] AS $key){
						if(empty($statuses['statuses'][$key]))continue;
						$array[$key] = $statuses['statuses'][$key];
					}
				}
			break;
		}
		if(is_array($array))
			asort($array,SORT_NUMERIC);
		return $array;
	}
}
function ord_payment_type($val=FALSE){

	$array=array(
		1=>_lang('Полная оплата'),
		3=>_lang('Оплата частями'),
		2=>_lang('Кредит'),
	);

	$array = _run_filter('ord_payment_type_array',$array);

	if(isset($array[$val]))
		return $array[$val];
	elseif(!$val)
		return '---';
	else
		return $array;

}

/** Запис в БД даних про нове замовлення **/


_add_action('post_init', 'CRM_orders_write_db');
_add_filter('trig_OrderResiving', 'CRM_orders_write_db');

function CRM_orders_write_db(){
global  $sql, $order_set;
        $time = time();

    if(isset($_POST['ordersubmit'])){
//        var_dump($_POST); exit;
        @$_POST['rf_phone'] = preg_replace("/[^0-9]/i",'',$_POST['rf_phone']);
        if(empty($_POST['rf_phone'])){
            $_POST['rf_phone'] = 'NON_'.$time;
        }

        if(empty($_POST['rf_email']) || !_checkEmail($_POST['rf_email'])){
            $_POST['rf_email'] = 'NON_'.$time;
        }

        $cl_ar = array(
            'fullname' => @$_POST['rf_name'],
            'tel' => @$_POST['rf_phone'],
            'email' => @$_POST['rf_email'],
            'date_update' => $time,
            'brawser_lang' => substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2),
        );

        //todo refactoring next 15 line of code
        if(!empty($_POST['rf_adress'])) $cl_ar['adress'] = $_POST['rf_adress'];
        $cval = $sql->query(
            "SELECT cl_id
				FROM _clients
				WHERE cl_tel='"._protect($_POST['rf_phone'])."' || cl_email='"._protect($_POST['rf_email'])."';
				",
            'value'
        );

        if(empty($cval)){
            $cl_id = $sql->insert('_clients', $cl_ar, 'cl_', TRUE, TRUE);
        }
        $cval = $sql->query(
            "SELECT cl_id
				FROM _clients
				WHERE cl_tel='"._protect($_POST['rf_phone'])."' || cl_email='"._protect($_POST['rf_email'])."';
				",
            'value'
        );
        $cl_id = $cval['cl_id'];



        if(!empty($_POST['rf_phone']) || !empty($_POST['rf_email'])){

            $set = _strCode(@$_GET['set'],FALSE);

            if(is_array($set)){

                $article = @$set['lp_article'];
                $prod = @$set['prod_title'];
                $price = @$set['prod_price'];

            }else{
                if (isset($_REQUEST['prod_lp'])){
                    $article = $_REQUEST['prod_lp'];
                }elseif (isset($_GET['lp'])){
                    $article = $_GET['lp'];
                }else{
                    $article = PAGE_URL;
                }
            }

            if(isset($_POST['prod_title'])) $prod = $_POST['prod_title'];
            if(isset($_POST['prod_price'])) $price = $_POST['prod_price'];

            $set['rf_name']=@$_POST['rf_name'];
            $set['rf_phone']=@$_POST['rf_phone'];
            $set['rf_email']=@$_POST['rf_email'];

        }else{
            global $PData;
            $PData->redirect(HOMEPAGE);
        }

        $note = '';

        if(isset($_POST['field'])){
            if(is_array($_POST['field'])){
                foreach($_POST['field'] AS $key=>$val){
                    if(!empty($note)) $note .= "\n";
                    $note .= $key.': '.$val;
                }
            }else{
                $note = $_POST['field'];
            }
        }
//        var_dump($note);exit;
        $file='';
        if(isset($_FILES["add_file"])&&$_FILES["add_file"]['name']!=''){
            $target_dir='media/files/upload/';
            $target_file=$target_dir.time().'_'.$_FILES['add_file']['name'];
            if(move_uploaded_file($_FILES['add_file']['tmp_name'],$target_file)){
                $file=$target_file;
            }else{
                echo "Sorry, there was an error uploading your file.";
                exit;
            }
        }

        $ord_ar = array(
            'lp_article' => $article,
            'prod' => @$prod . ' *',
            'price' => @$price . ' *',
            'date_create' => $time,
            'mark' => @$_COOKIE['mark'],
            'note' => @$note,
            'cl_id' => $cl_id,
            'file' =>$file
        );
//		 $ord_ar = _run_filter('CRM_order_orders_write_db','',$ord_ar);

        global $new_order_id;
        if(isset($_POST['order_status'])) $ord_ar['status'] = (int)$_POST['order_status'];
//        var_dump($ord_ar);exit;

        $order_set['o_id'] = $sql->insert('_orders',$ord_ar,'o_',FALSE,TRUE);
        $new_order_id = $order_set['o_id'];
        _run_action('ord_add_new_order',
            array(
                'o_id' => $order_set['o_id'],
                'cl_id' => $cl_id,

                'phone' => @$_POST['rf_phone'],
                'email' => @$_POST['rf_email'],
                'name' => @$_POST['rf_name'],
                'title' => @$prod,
                'price' => @$price,
                'mark' => @$_COOKIE['mark'],
                'lp_article' => $article,
                'date_create' => $time,
                'note' => $note
            )
        );

        if(!empty($_GET['set'])){
            $set = _strCode($_GET['set'],FALSE);
            $set['o_id'] = $order_set['o_id'];
        }else{
            $set = $order_set;
        }

        $_GET['set'] = _strCode($set);
        return array(
            'status' => 'success',
            'order' => $ord_ar
        );
    }else{
        //print_r($_POST);
    }

}