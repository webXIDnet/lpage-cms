<?php
global $PData;
?><div role="tabpanel" class="tab-box">

      <!-- tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#search" aria-controls="home" role="tab" data-toggle="tab"><?php echo _lang('Поиск') ?></a></li>
        <li role="presentation"><a href="#clients" aria-controls="profile" role="tab" data-toggle="tab">+ <?php echo _lang('Добавить клиента') ?></a></li>
      </ul>

      <!-- panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="search">

          <form method="get" action="">

            <p><b><?php echo _lang('Поиск') ?>:</b></p>
            <input placeholder="<?php echo _lang('ФИО или телефон, или email') ?>" type="text" name="search" value="<?php echo $PData->_GET('search'); ?>">
            <input type="submit" class="button" value="<?php echo _lang('Искать') ?>">
          </form>

        </div>
        <div role="tabpanel" class="tab-pane fade" id="clients">
          <form method="POST">
            <input placeholder="ФИО *" type="text" name="addClient[fullname]" value="">
            <input placeholder="Телефон *<?php echo _lang('Искать') ?>" type="text" name="addClient[tel]" value="">
            <input placeholder="Email" type="text" name="addClient[email]" value="">
            <input type="submit" class="button" value="Добавить">
          </form>
        </div>
      </div>

    </div>
    <div class="line"></div>

    <div class="table-holder">
        <table class="table">

          <thead>
            <th class="table-10">Выбрать</th>
            <th class="table-30">ФИО</th>
            <th class="table-30">Контакты</th>
            <th class="table-30">Примечание</th>
          </thead>
          <?php foreach ($vars_array['clients'] as $client) { ?>
            <tr>
              <td>
              <input class="delete" type="checkbox" name="delete" value="<?php echo $client['cl_id']; ?>">
                <?php echo $client['cl_id']; ?>
              </td>
              <td class="td-link" data-toggle="collapse" data-target="#client-detail-<?php echo $client['cl_id']; ?>">
                <?php echo $client['cl_fullname']; ?>
              </td>
              <td class="td-link" data-toggle="collapse" data-target="#client-detail-<?php echo $client['cl_id']; ?>">
                <span class="grey">Тел:</span> <?php echo $client['cl_tel']; ?><br>
                <span class="grey">Email:</span> <?php echo $client['cl_email']; ?><br>
              </td>
              <td class="td-link" data-toggle="collapse" data-target="#client-detail-<?php echo $client['cl_id']; ?>">
                <span class="grey">Адрес:</span> <span class="<?php echo $client['cl_email'] != '' ? '' : 'red'; ?>"><?php echo @$client['cl_additional_data']['Город']; ?> <?php echo $client['cl_adress']; ?></span><br>
              </td>
            </tr>
            <tr class="collapse" id="client-detail-<?php echo $client['cl_id']; ?>">
              <td colspan="5">
              <div role="tabpanel" class="tab-box">
              <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#client-edit-<?php echo $client['cl_id']; ?>" aria-controls="profile" role="tab" data-toggle="tab">Клиент</a></li>
                        <li role="presentation"><a href="#client-orders-<?php echo $client['cl_id']; ?>" aria-controls="settings" role="tab" data-toggle="tab">Заявки клиента</a></li>
                        <li role="presentation"><a href="#add-order-<?php echo $client['cl_id']; ?>" aria-controls="settings" role="tab" data-toggle="tab">Добавить заявку</a></li>

                      </ul>
                      <form method="post" action="">
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="client-edit-<?php echo $client['cl_id']; ?>">

                            <input type="hidden" name="editClient[client_id]" value="<?php echo $client['cl_id']; ?>">
                              <div>
                                <div class="row">
                                  <div class="col-lg-6 col md-6">


                                    <p><b>ФИО:</b></p>
                                    <input type="text" name="editClient[fullname]" value="<?php echo $client['cl_fullname']; ?>">

                                    <p><b>Дата рождения:</b></p>
                                    <input class="datepicker" type="text" name="editClient[birthday]" value="<?php echo $client['cl_birthday'] > 0 ? date('d.m.Y',$client['cl_birthday']) : ''; ?>">

                                    <p><b>Тел:</b></p>
                                    <input type="text" name="editClient[tel]" value="<?php echo $client['cl_tel']; ?>">

                                    <p><b>Email:</b></p>
                                    <input type="text" name="editClient[email]" value="<?php echo $client['cl_email']; ?>">

                                    <p><b>Примечание:</b></p>
                                    <textarea name="editClient[additional_data][Примечание]"><?php echo @$client['cl_additional_data']['Примечание']; ?></textarea>

                                    <p><b>Домашний адрес:</b></p>
                                    <input type="text" name="editClient[adress]" value="<?php echo $client['cl_adress']; ?>">

                                    <p><b>Город:</b></p>
                                    <input type="text" name="editClient[additional_data][Город]" value="<?php echo @$client['cl_additional_data']['Город']; ?>">

                                    <p>Данные клиента обновлялись: <b><?php echo $client['cl_date_update'] > 0 ? date('d.m.Y',$client['cl_date_update']) : _lang('Никогда'); ?></b></p>

                                    <label>
                                      <input type="checkbox" name="refresh_cl_date" value="true"> <span class="grey"> - Обновить дату актуальности данных клиента</span>
                                    </label>

                                  </div>
                                  <div class="col-lg-6 col md-6">

                                    <p><b>Язык:</b> <span class="grey">(по умолчанию - данные браузера клиента)</span></p>
                                    <select name="">
                                      <option value="NAN">не извесно</option>
                                      <option selected="selected" value="ru">русский</option>
                                      <option value="uk">украинский</option>
                                      <option value="en">английский</option>
                                    </select>


                                  </div>
                                </div>

                                <input class="button" type="submit" value="Save" name="formsubmit-edit"></form>
                              </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="client-orders-<?php echo $client['cl_id']; ?>">
                          <table>
                          <?php $cl_i = 1; foreach ($vars_array['orders'] as $order_client) if ($order_client['o_cl_id'] == $client['cl_id']) { ?>
                          <tr>
                              <td>
                                <input type="checkbox">
                                <?php echo $cl_i; ?>
                              </td>

                              <td class="td-link">
                              <a target="_blank" href="<?php echo getURL('admin', 'crm/orders'); ?>?tab=search&id=<?php echo $order_client['o_id']; ?>">
                                <span class="grey">Статус:    </span> <span class="blue"><b><?php echo @$vars_array['statuses'][$order_client['o_status']]; ?></b></span> <br>
                                <span class="grey">Заказ:     </span> <b><?php echo $order_client['o_prod']; ?></b> <br>
                                <span class="grey">Цена:      </span> <?php echo $order_client['o_price']; ?> <br>
                                <span class="grey">Поступила: </span> <?php echo date('d.m.Y в H:i',$order_client['o_date_create']); ?> <br>
                                <span class="grey">Менедежер: </span> <?php echo $order_client['u_fullname']; ?>
                              </a>
                              </td>
                              <td class="td-link">
                              <a target="_blank" href="<?php echo getURL('admin', 'crm/orders'); ?>?tab=search&id=<?php echo $order_client['o_id']; ?>">
                                <span class="grey">Метка: </span> <?php echo $order_client['o_mark']; ?> <br>
                                <span class="grey">Дата выполнения: </span> <?php echo $order_client['o_date_group_start'] > 0 ? date('d.m.Y в H:i',$order_client['o_date_group_start']) : ''; ?> <br>
                                <span class="grey">Тип оплаты: </span> --- <br>
                                <span class="grey">Язык: </span> не извесно
                              </a>
                              </td>
                              <td class="td-link">
                              <a target="_blank" href="<?php echo getURL('admin', 'crm/orders'); ?>?tab=search&id=<?php echo $order_client['o_id']; ?>">
                                <span class="grey">Связаться: </span> --- <br>
                                <span class="grey">Комментарий: </span> <br>
                                <?php echo $order_client['o_note']; ?>
                              </a>
                              </td>
                            </tr>
                            <?php $cl_i++; } ?>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="add-order-<?php echo $client['cl_id']; ?>">
                          <div class="">
                            <input type="hidden" name="addOrder[cl_id]" value="<?php echo $client['cl_id']; ?>">
                            <p><b>Название заявки:</b></p>
                            <input name="addOrder[prod]" value="" type="text">

                            <p><b>Цена:</b></p>
                            <input name="addOrder[price]" value="" type="text">

                            <p class="blue"><b>Статус:</b></p>
                            <select name="addOrder[status]">
                              <option selected="selected" value="">Не определен</option>
                              <?php foreach ($vars_array['statuses'] as $status_key => $status) { ?>
                                  <option value="<?php echo $status_key; ?>"><?php echo $status; ?></option>
                                <?php } ?>
                            </select>

                            <p><b>Выбрать менеджера:</b></p>
                            <select name="addOrder[u_id]">
                              <option value="0">- не указано -</option>
                              <?php foreach ($vars_array['managers'] as $manager) { ?>
                                  <option value="<?php echo $manager['u_id']; ?>"><?php echo $manager['u_fullname']; ?></option>
                                <?php } ?>
                            </select>

                            <p><b>Дата следующего контакта:</b></p>
                            <input name="addOrder[date_next_touch]" class="datetime" value="" type="text">

                            <p><b>Метка:</b></p>
                            <input name="addOrder[mark]" value="" type="text">

                            <p><b>Комментарий:</b></p>
                            <textarea name="addOrder[note]"></textarea>
                            <p>
                              <input class="button" type="submit" value="<?php echo _lang('Добавить'); ?>" name="formsubmit-add"></form>
                            </p>
                          </div>
                        </div>
                      </div>
                      </form>
                </div>
              </td>
            </tr>
          <?php } ?>
        </table>
    </div>
    <form method="post" action="">
      <input class="delete_vals" type="hidden" name="delete_vals" value="">
      <input value="Удалить" class="button delete_button" name="delete_button" type="submit">
    </form>
    <?php echo $vars_array['pagination']; ?>
