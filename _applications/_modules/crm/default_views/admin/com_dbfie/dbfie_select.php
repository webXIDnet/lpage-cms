<div class="container-fluid">
    <div>
        <?php
        if (!extension_loaded('dbase')){//check for module to work with dBace
            ?>
            <div class="alert alert-danger">
                <h1>You need enable php extension 'dbase' on you histing to work with DBF files</h1>
            </div>
        <?php
        }else{
        ?>
        <h1>Импорт</h1>

        <form method="post" action="<?php echo getURL('admin','crm/dbfie/import'); ?>" enctype="multipart/form-data">
            <input name="dbf_file" type="file" accept=""/>
            <br>
            <input type="submit" class="button" value="Загрузить"/>
            <input type="hidden" name="test" value="001">
        </form>
    </div>
    <div>
        <h1>Експорт</h1>
        <br>
        <a href="<?php echo getURL('admin','crm/dbfie/export'); ?>">
            <button class="button">Експорт</button>
        </a>
    </div>
    <?php
    }
    ?>
</div>
