<div class="tab-box" role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="<?php
				if (!isset($_GET['tab'])) $_GET['tab'] = '';
				echo @$_GET['tab'] == 'bookmarks' || @$_GET['tab'] == '' || empty($_GET['tab']) ? 'active' : ''; ?>"><a href="#tabs" aria-controls="home" role="tab" data-toggle="tab">Закладки</a></li>
				<li role="presentation" class="<?php echo @$_GET['tab'] == 'filter' ? 'active' : ''; ?>"><a href="#filter" aria-controls="home" role="tab" data-toggle="tab">Фильтр</a></li>
				<li role="presentation" class="<?php echo @$_GET['tab'] == 'search' ? 'active' : ''; ?>"><a href="#search" aria-controls="home" role="tab" data-toggle="tab">Поиск</a></li>
				<li role="presentation" class="<?php echo @$_GET['tab'] == 'add-order' ? 'active' : ''; ?>"><a href="#add-order" aria-controls="profile" role="tab" data-toggle="tab">+ Добавить заявку</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade <?php echo @$_GET['tab'] == 'bookmarks' || @$_GET['tab'] == '' || !isset($_GET['tab']) ? 'active in' : ''; ?>" id="tabs">
				<a href="<?php echo CURRENT_PAGE; ?>?status=" class="zakladka">Не определен</a>
				<?php
				if($vars_array['statuses'])
				foreach ($vars_array['statuses'] as $status_key => $status) { ?>
					<a href="<?php echo CURRENT_PAGE; ?>?status=<?php echo $status_key; ?>" class="zakladka"><?php echo $status; ?></a>
					<?php } ?>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="filter">

					<form method="post">

						<div class="row">
							<div class="col-lg-6 col-md-6">
								<p><b>Дата поступления:</b></p>
								<input class="datetime" name="filters[date_create][0]" value="" type="text"> до <input class="datetime" name="filters[date_create][1]" value="" type="text">

								<p><b>Метки:</b></p>
								<select class="multiselect" multiple="" name="filters[mark][]">
									<option value="">- не указано -</option>
									<?php foreach ($vars_array['marks'] as $mark) { ?>
									<option value="<?php echo $mark; ?>"><?php echo $mark; ?></option>
									<?php } ?>
								</select>

								<p><b>Менеджеры:</b></p>
								<select class="multiselect" multiple="" name="filters[u_id][]">
									<option value="">- не указано -</option>
									<?php foreach ($vars_array['managers'] as $manager) { ?>
										<option value="<?php echo $manager['u_id']; ?>"><?php echo $manager['u_fullname']; ?></option>
									<?php } ?>
								</select>

								<p><b>Заказ:</b></p>
								<select class="multiselect" multiple name="filters[prod][]">
									<option value="">- не указано -</option>
									<?php foreach ($vars_array['titles'] as $title) { ?>
										<option value="<?php echo $title; ?>"><?php echo $title; ?></option>
									<?php } ?>
								</select>


							</div>
							<div class="col-lg-6 col-md-6">

								<p><b>Дата оплаты:</b></p>
								<input name="filters[date_done][0]" class="datetime" value="" type="text"> до <input class="datetime" name="filters[date_done][1]" value="" type="text">

								<p><b>Статусы:</b></p>
								<select class="multiselect" multiple="" name="filters[status][]">
									<option value="">- не указано -</option>
									<?php foreach ($vars_array['statuses'] as $status_key => $status) { ?>
										<option value="<?php echo $status_key; ?>"><?php echo $status; ?></option>
									<?php } ?>
								</select>

								<p><b>Тип оплаты:</b></p>
								<select class="multiselect" multiple="" name="filters[payment_type][]">
									<option value="0">- не указано -</option>
									<option value="1">Полная оплата</option>
									<option value="2">Оплата частями</option>
									<option value="3">Кредит</option>
								</select>

							</div>
						</div>

						<input class="button" value="Искать" type="submit">

						<p class="grey">
							! - Зажмите Ctrl или Shift, чтобы выбрать несколько пунктов<br>
							! - Чтобы найти нужный пункт мультименю, кликните в любом месте нужного вам меню, и нажмите на клавиатуре первую букву нужного вам пункта. Вы можете перемещаться по пунктах с одинаковой первой буквой, нажимая несколько раз на клавишу
						</p>

					</form>

				</div>
				<div role="tabpanel" class="tab-pane fade <?php echo @$_GET['tab'] == 'search' ? 'active in' : ''; ?>" id="search">

					<form method="get">

						<p><b>Поиск:</b></p>
						<input placeholder="ФИО или телефон, или email" name="search" value="" type="text">
						<label>
							<input name="search" value="true" type="checkbox"><span class="grey"> - Искать по последним цифрам телефона (количество не имеет значения)</span>
						</label>

						<br>
						<input class="button" value="Искать" type="submit">

					</form>

				</div>
				<div role="tabpanel" class="tab-pane fade <?php echo @$_GET['tab'] == 'add-order' ? 'active in' : ''; ?>" id="add-order">

					<form method="post" action="">

					<div class="row">
						<div class="col-lg-6 col-md-6">

							<p><b>1. ФИО:</b></p>
							<input name="addClient[fullname]" value="" type="text">

							<p><b>2. Тел *:</b></p>
							<input name="addClient[tel]" value="" type="text">

							<p><b>3. Email *:</b></p>
							<input name="addClient[email]" value="" type="email">

							<p><b>4. Название заказа *:</b></p>
							<input name="addOrder[prod]" value="" type="text">

							<p><b>5. Цена *:</b></p>
							<input name="addOrder[price]" value="" type="text">

							<p><b>Домашний адрес:</b></p>
							<input name="addClient[adress]" value="" type="text">

							<p><b>Город:</b></p>
							<input name="addClient[additional_data][Город]" value="" type="text">

							<p><b>Оплата:</b></p>
							<input name="addOrder[additional_data][Оплата]" value="" type="text">

							<p><b>Скидка:</b></p>
							<input name="addOrder[additional_data][Скидка]" value="" type="text">

							<p><b>Вид услуг:</b></p>
							<input name="addOrder[additional_data][Вид услуг]" value="" type="text">

							<p><b>Откуда узнал:</b></p>
							<input name="addOrder[additional_data][Откуда узнал]" value="" type="text">

							<p><b>Вид связи:</b></p>
							<input name="addOrder[additional_data][Вид связи]" value="" type="text">

						</div>
						<div class="col-lg-6 col-md-6">

							<p class="blue"><b>Статус:</b></p>
							<select name="addOrder[status]">
								<option selected="selected" value="">Не определен</option>
								<?php foreach ($vars_array['statuses'] as $status_key => $status) { ?>
										<option value="<?php echo $status_key; ?>"><?php echo $status; ?></option>
									<?php } ?>
							</select>

							<p><b>Выбрать менеджера:</b></p>
							<select name="addOrder[u_id]">
								<option value="0">- не указано -</option>
								<?php foreach ($vars_array['managers'] as $manager) { ?>
										<option value="<?php echo $manager['u_id']; ?>"><?php echo $manager['u_fullname']; ?></option>
									<?php } ?>
							</select>

							<p><b>Дата следующего контакта:</b></p>
							<input name="addOrder[date_next_touch]" class="datetime" value="" type="text">

							<p><b>Метка:</b></p>
							<input name="addOrder[mark]" value="" type="text">

							<p><b>Комментарий:</b></p>
							<textarea name="addOrder[note]"></textarea>

						</div>
					</div>

					<input class="button" name="formsubmit-add" value="Сохранить" type="submit">
					</form>

				</div>
			</div>

			<div class="table-holder large-table-holder">

					<table class="table">

				<thead>
					<tr><th class="table-5">Выбрать</th>
					<th class="table-20">Клиент</th>
					<th class="table-30">Заказ</th>
					<th class="table-20">Доп. данные</th>
					<th class="table-25">Примечание</th>
				</tr></thead>

				<!-- tr item -->
				<tbody>
				<?php $i = 1; foreach ($vars_array['orders'] as $order) { ?>

				<tr>
						<td>
							<input class="delete" type="checkbox" name="delete" value="<?php echo $order['o_id']; ?>">
							<?php echo $order['o_id']; ?>
						</td>
						<td class="td-link" data-toggle="collapse" data-target="#order-detail-<?php echo $order['o_id']; ?>">
							<b><?php echo $order['cl_fullname']; ?></b> <br>
							<span class="grey">Тел:</span> <?php echo $order['cl_tel']; ?> <br>
							<span class="grey">Email:</span> <?php echo $order['cl_email']; ?> <br>
						</td>
						<td class="td-link" data-toggle="collapse" data-target="#order-detail-<?php echo $order['o_id']; ?>">
							<span class="grey">Статус:    </span> <span class="blue"><b><?php echo isset($vars_array['statuses'][$order['o_status']]) ? $vars_array['statuses'][$order['o_status']] : 'не определен'; ?></b></span> <br>
							<span class="grey">Заказ:     </span> <b><?php echo $order['o_prod']; ?></b> <br>
							<span class="grey">Цена:      </span> <?php echo $order['o_price']; ?> <br>
							<span class="grey">Поступила: </span> <?php	echo $order['o_date_create'] > 0 ? date('d.m.Y в H:i',$order['o_date_create']) : ''; ?> <br>
							<span class="grey">Менедежер: </span> <?php echo $order['u_fullname']; ?>
						</td>
								<td class="td-link" data-toggle="collapse" data-target="#order-detail-<?php echo $order['o_id']; ?>">
										<span class="grey">Метка: </span> <?php echo $order['o_mark']; ?> <br>
										<span class="grey"><?php echo _lang('Город'); ?>: </span>
										<?php
										if (!empty($order['cl_additional_data']['Город'])) {
												echo($order['cl_additional_data']['Город']);
										} else {
												echo _lang('не указан');
										}
										?> <br>
										<span class="grey"><?php echo _lang('Адрес'); ?>: </span>
										<?php
										if (!empty($order['cl_adress'])) {
												echo($order['cl_adress']);
										} else {
												echo _lang('не указан');
										}
										?> <br>
								</td>
						<td class="td-link" data-toggle="collapse" data-target="#order-detail-<?php echo $order['o_id']; ?>">
							<span class="grey">Связаться: </span> --- <br>
							<span class="grey">Комментарий: </span> <br>
							<?php echo $order['o_note']; ?>
									<br>
									<?php if(!empty($order['o_file'])){?>
									<span class="grey"><?php echo _lang('Файл');?>: </span>
									<a target="_blank" href="<?php echo HOMEPAGE.$order['o_file']; ?>"><span class="glyphicon glyphicon-download-alt"></span></a>
									<?php }?>
							</td>
					</tr>
					<tr class="collapse" id="order-detail-<?php echo $order['o_id']; ?>">
						<td colspan="5">
						<form method="post" action="">
						<input type="hidden" name="editOrder[order_id]" value="<?php echo $order['o_id']; ?>">
						<input type="hidden" name="editClient[client_id]" value="<?php echo $order['o_cl_id']; ?>">
							<div>
								 <p><b>Выбрать менеджера:</b></p>
										<select name="editOrder[u_id]">
										<option value="">- не указано -</option>
										<?php foreach ($vars_array['managers'] as $manager) { ?>
											<option <?php echo $order['o_u_id'] == $manager['u_id'] ? 'selected' : ''; ?> value="<?php echo $manager['u_id']; ?>"><?php echo $manager['u_fullname']; ?></option>
										<?php } ?>
										</select>
										<div role="tabpanel">

											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="active"><a href="#order-edit-<?php echo $order['o_id']; ?>" aria-controls="home" role="tab" data-toggle="tab">Заявка</a></li>
												<li role="presentation"><a href="#client-edit-<?php echo $order['o_id']; ?>" aria-controls="profile" role="tab" data-toggle="tab">Клиент</a></li>
<!--                         <li role="presentation"><a href="#bill-edit-<?php echo $order['o_id']; ?>" aria-controls="messages" role="tab" data-toggle="tab">Счет</a></li>
 -->                        <li role="presentation"><a href="#client-orders-<?php echo $order['o_id']; ?>" aria-controls="settings" role="tab" data-toggle="tab">Заявки клиента</a></li>
											</ul>

											<!-- Tab panes -->
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane active" id="order-edit-<?php echo $order['o_id']; ?>">
													<h1>Информация по заказу</h1>

														<div class="row">
															<div class="col-lg-6 col-md-6">

																<p><b>Название заказа:</b></p>
																<input name="editOrder[prod]" value="<?php echo $order['o_prod']; ?>" type="text">

																<p><b>Цена:</b></p>
																<input name="editOrder[price]" value="<?php echo $order['o_price']; ?>" type="text">

																<p><b>Дата выполнения заказа:</b></p>
																<input name="editOrder[date_group_start]" class="datetime" value="<?php echo $order['o_date_group_start'] > 0 ? date('d.m.Y H:i',$order['o_date_group_start']) : ''; ?>" type="text">

																<p><b>Дата следующего контакта:</b></p>
																<input name="editOrder[date_next_touch]" class="datetime" value="<?php echo $order['o_date_next_touch'] > 0 ? date('d.m.Y H:i',$order['o_date_next_touch']) : ''; ?>" type="text">

																<p>
																	Метка: <b>---</b><br>
																	Посадка: <b>[<a target="_block" href="<?php echo getURL('lp', $order['o_lp_article']); ?>"><?php echo $order['o_lp_article']; ?></a>]</b><br>
																	Дата поступления: <b><?php echo  $order['o_date_create'] > 0 ? date('d.m.Y в H:i',$order['o_date_create']) : ''; ?></b>
																</p>
																<?php if ($order['o_file'] != '') { ?>
																<p>
																 <a href="<?php echo $order['o_file']; ?>">Опросный лист</a>
																</p>
																<?php } ?>

															</div>
															<div class="col-lg-6 col-md-6">

																<p><b>Статус:</b></p>
																<select name="editOrder[status]">
																<option value="0">Не определен</option>
																<?php foreach ($vars_array['statuses'] as $status_key => $status_value) { ?>
																	<option <?php echo $order['o_status'] == $status_key ? 'selected' : ''; ?> value="<?php echo $status_key; ?>"><?php echo $status_value; ?></option>
																<?php } ?>
																</select>

																<p><b>Тип оплаты:</b></p>
																<select name="editOrder[payment_type]">
																	<option value="">- не указано -</option>
																	<option <?php echo $order['o_payment_type'] == '1' ? 'selected' : ''; ?> value="1">Полная оплата</option>
																	<option <?php echo $order['o_payment_type'] == '2' ? 'selected' : ''; ?> value="3">Оплата частями</option>
																	<option <?php echo $order['o_payment_type'] == '3' ? 'selected' : ''; ?> value="2">Кредит</option>
																</select>

																<p><b>Дата полной оплаты:</b></p>
																<input name="editOrder[date_done]" class="datetime" value="<?php echo $order['o_date_done'] > 0 ? date('d.m.Y H:i',$order['o_date_done']) : ''; ?>" type="text">
																<p><b>Оплата:</b></p>
																<input name="editOrder[additional_data][Оплата]" value="<?php echo @$order['o_additional_data']['Оплата']; ?>" type="text">

																<p><b>Скидка:</b></p>
																<input name="editOrder[additional_data][Скидка]" value="<?php echo @$order['o_additional_data']['Скидка']; ?>" type="text">

																<p><b>Вид услуг:</b></p>
																<input name="editOrder[additional_data][Вид услуг]" value="<?php echo @$order['o_additional_data']['Вид услуг']; ?>" type="text">

																<p><b>Откуда узнал:</b></p>
																<input name="editOrder[additional_data][Откуда узнал]" value="<?php echo @$order['o_additional_data']['Откуда узнал']; ?>" type="text">

																<p><b>Вид связи:</b></p>
																<input name="editOrder[additional_data][Вид связи]" value="<?php echo @$order['o_additional_data']['Вид связи']; ?>" type="text">

																<p><b>Комментарий:</b></p>
																<textarea class="input" name="editOrder[note]"><?php echo $order['o_note']; ?></textarea>

															</div>
														</div>
												</div>
												<div role="tabpanel" class="tab-pane" id="client-edit-<?php echo $order['o_id']; ?>">
													<h1>Клиент</h1>

													<div class="row">
														<div class="col-lg-6 col-md-6">

																<p><b>ФИО:</b></p>
																<input name="editClient[fullname]" value="<?php echo $order['cl_fullname']; ?>" type="text">

																<p><b>Дата рождения:</b></p>
																<input class="datepicker" name="editClient[birthday]" value="<?php echo $order['cl_birthday'] > 0 ? date('d.m.Y',$order['cl_birthday']) : ''; ?>" type="text">

																<p><b>Тел:</b></p>
																<input name="editClient[tel]" value="<?php echo $order['cl_tel']; ?>" type="text">

																<p><b>Email:</b></p>
																<input name="editClient[email]" value="<?php echo $order['cl_email']; ?>" type="text">

																<p><b>Примечание:</b></p>
																<textarea name="editClient[additional_data][Примечание]"><?php echo @$order['cl_additional_data']['Примечание']; ?></textarea>

																<p>Данные клиента обновлялись: <b><?php echo $order['cl_date_update'] > 0 ? date('d.m.Y в H:i',$order['cl_date_update']) : _lang('Никогда'); ?></b>

																<label class="grey">
																	<input name="refresh_cl_date" value="true" type="checkbox"> <b style="color: #000;">Обновить дату</b> актуальности данных клиента
																</label>

														</p></div>
														<div class="col-lg-6 col-md-6">

															<p><b>Язык</b>: <span class="grey">(по умолчанию - данные из браузера клиента)</span></p>
															<select name="editClient[brawser_lang]">
																<option selected="selected" value="NAN">не извесно</option>
																<option value="uk">украинский</option>
																<option value="ru">русский</option>
																<option value="en">английский</option>
															</select>

															<p><b>Домашний адрес:</b></p>
															<input name="editClient[adress]" value="<?php echo @$order['cl_adress']; ?>" type="text">

															<p><b>Город:</b></p>
															<input name="editClient[additional_data][Город]" value="<?php echo @$order['cl_additional_data']['Город']; ?>" type="text">

														</div>
													</div>
												</div>
												<!-- <div role="tabpanel" class="tab-pane" id="bill-edit-<?php echo $order['o_id']; ?>">
													<h1>Выставить счет</h1>

													<p><b>Сумма счета</b></p>
													<input value="" name="set[prod_price]" type="text">

													<p><b>Описание платежа</b></p>
													<textarea name="set[prod_title]">СПА-пакет 7 часов</textarea>

													<label class="grey">
														<input name="sendPaymentForm" value="" type="checkbox"> <b style="color: #000;">Отправить счет</b> клиенту
													</label>
												</div> -->
												<div role="tabpanel" class="tab-pane" id="client-orders-<?php echo $order['o_id']; ?>">
												<table>
													<?php $cl_i = 1; foreach ($vars_array['orders'] as $order_client) if ($order_client['o_cl_id'] == $order['o_cl_id']) { ?>
													<tr>
															<td>
																<input class="delete" type="checkbox" name="delete" value="<?php echo $order_client['o_id']; ?>">
																<?php echo $order_client['o_id']; ?>
															</td>
															<td class="td-link">
															<a target="_blank" href="<?php echo CURRENT_PAGE; ?>?tab=search&id=<?php echo $order_client['o_id']; ?>">
																<b><?php echo $order_client['cl_fullname']; ?></b> <br>
																<span class="grey">Тел:</span> <?php echo $order_client['cl_tel']; ?> <br>
																<span class="grey">Email:</span> <?php echo $order_client['cl_email']; ?> <br>
															</a>
															</td>
															<td class="td-link">
															<a target="_blank" href="<?php echo CURRENT_PAGE; ?>?tab=search&id=<?php echo $order_client['o_id']; ?>">
																<span class="grey">Статус:    </span> <span class="blue"><b><?php
																if (!empty($vars_array['statuses'][$order_client['o_status']])) {
																	echo $vars_array['statuses'][$order_client['o_status']];
																}

																	?></b></span> <br>
																<span class="grey">Заказ:     </span> <b><?php echo $order_client['o_prod']; ?></b> <br>
																<span class="grey">Цена:      </span> <?php echo $order_client['o_price']; ?> <br>
																<span class="grey">Поступила: </span> <?php echo $order_client['o_date_create'] > 0 ? date('d.m.Y в H:i',$order_client['o_date_create']) : ''; ?> <br>
																<span class="grey">Менедежер: </span> <?php echo $order_client['u_fullname']; ?>
															</a>
															</td>
															<td class="td-link">
															<a target="_blank" href="<?php echo CURRENT_PAGE; ?>?tab=search&id=<?php echo $order_client['o_id']; ?>">
																<span class="grey">Метка: </span> <?php echo $order['o_mark']; ?> <br>
																<span class="grey">Дата выполнения: </span> <?php echo $order['o_date_group_start'] > 0 ? date('d.m.Y в H:i',$order['o_date_group_start']) : ''; ?> <br>
																<span class="grey">Тип оплаты: </span> --- <br>
																<span class="grey">Язык: </span> не извесно
															</a>
															</td>
															<td class="td-link">
															<a target="_blank" href="<?php echo CURRENT_PAGE; ?>?tab=search&id=<?php echo $order_client['o_id']; ?>">
																<span class="grey">Связаться: </span> --- <br>
																<span class="grey">Комментарий: </span> <br>
																<?php echo $order_client['o_note']; ?>
															</a>
															</td>
														</tr>
														<?php $cl_i++; } ?>
														</table>
												</div>
											</div>

										</div>
										<br>
										<input class="button" value="Сохранить" name="formsubmit-edit" type="submit">


							</div>
							</form>
						</td>
					</tr>

				<?php $i++; } ?>
				<!-- client modal -->

					<!-- // -->


				</tbody></table>
			</div>

			<form method="post" action="">
				<input class="delete_vals" type="hidden" name="delete_vals" value="">
				<input value="Удалить" class="button delete_button" name="delete_button" type="submit">
			</form>

			<p class="grey">* - не проверенные менеджером данные. Информация взята с базы автоматически</p>

<?php echo $vars_array['pagination']; ?>



</div>