<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Тест на знання англійської мови
 * 
 * 
***/


function EnglishTest($val=''){
		
	if(PAGE_TYPE=='admin')return '[:EnglishTest '.$val.':]'; 
	
	global 	$PData,
			$enT_url;
			
	$enT_url = $val;
	
	if(isset($_POST['startText'])){
		if(!empty($_POST['cl']['name']) && !empty($_POST['cl']['phone']) && !empty($_POST['cl']['email']) ){
			$_SESSION['text_started'] = $_POST['cl'];
		}else{
			$PData->content('Нужно правильно заполнить все объязательные поля','message');
		}
	}
	
	if(!isset($_SESSION['text_started']) || !is_array($_SESSION['text_started'])){
		return '<form method="POST" class="start_test">
				<b class="start_test_title">'._lang('Что бы начать тест, заполните форму ниже').'</b>
				<p>
					<label>
						<b>'._lang('Имя').'*</b><br/>
						<input placeholder="" name="cl[name]" type="text" required="required"/>
					</label>
				</p>
				<p>
					<label>
						<b>'._lang('Телефон').'*</b><br/>
						<input placeholder="" name="cl[phone]" type="tel" required="required" />
					</label>
				</p>
                                <p>
					<label>
						<b>'._lang('Email').'*</b><br/>
						<input placeholder="" name="cl[email]" type="email" required="required" />
					</label>
				</p>
				<input name="startText" type="submit" value="'._lang('Начать тест').'"/>
			</form>
		';
	}elseif(is_array($_SESSION['text_started'])){
		
		$_POST['ordersubmit'] = '1';
		$_POST['rf_phone'] = $_SESSION['text_started']['phone'];
		$_POST['rf_name'] = $_SESSION['text_started']['name'];
        $_POST['rf_email'] = @$_SESSION['text_started']['email'];
		$_POST['prod_title'] = _lang('Прохождение теста на уровень знания анг. языка');
		$_POST['order_status'] = -3;
		
		if(!isset($_SESSION['text_started']['o_id'])){
			
			CRM_order_MBA_orders_write_db();
			global $order_set;
			
			$_SESSION['text_started']['o_id'] = $order_set['o_id'];
		}
		
		if(!isset($_SESSION['text_started']['o_id'])){
			unset($_SESSION['text_started']);
			$PData->redirect(PAGE_URL);
		}
		
		return require_once __DIR__ . '/ent_algoritm.php';
			
	}else{
		unset($_SESSION['text_started']);
		$PData->redirect(PAGE_URL);
	}
}

function enTest_Result(){
	
	if(PAGE_TYPE=='admin')return '[:enTest_Result:]';
		
	unset($_SESSION['text_started']);
	
	return _lang('Количество набранных баллов').': '.@$_SESSION['summ'].'<br/>'._lang('Ваш уровень знаний').': <b>'.@$_SESSION['result'].'</b>';
}

function enT_circle($n){
	$text = '<div class="steps">';
	for($i=0;$i<$n;$i++){
	
	 	$text .= "<img src='".HOMEPAGE."images/EnTest/checked_1.png'/>";
	
	}
	
	for($n;$n<38;$n++){
	
	 	$text .= "<img src='".HOMEPAGE."images/EnTest/checked_0.png'/>";
	
	}
	return $text.'</div>';
}


function enT_lett($zz){
	switch($zz){
		case 1:
			$l='a';
		break;
		case 2:
			$l='b';
		break;
		case 3:
			$l='c';
		break;
		case 4:
			$l='d';
		case 5:
			$l='e';
		case 6:
			$l='f';
	}
	return $l;
}