<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента FAQs
 *
***/

require_once __DIR__ . '/faqs_languages.php'; //підключення мовного файлу компоненти


_add_action('defined_modules', 'FAQs_defined_modules');

function FAQs_defined_modules(){
	if(PAGE_TYPE=='admin'){
		global $sql;

		$sql->db_table_installer('FAQs','05.09.2014',
			array(
				'_answer' => array(
					'answer_id' => "`answer_id` int(11) NOT NULL AUTO_INCREMENT",
					'answer_text' => "`answer_text` varchar(200) NOT NULL",
					'answer_question_id' => "`answer_question_id` int(11) NOT NULL",
					'CREATE' => "`answer_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`answer_id`)"
				),
				'_question' => array(
					'question_id' => "`question_id` int(11) NOT NULL AUTO_INCREMENT",
					'question_user_name' => "`question_user_name` varchar(200) NOT NULL",
					'question_text' => "`question_text` text",
					'question_cat_id' => "`question_cat_id` int(11) DEFAULT NULL",
					'question_public_date' => "`question_public_date` int(11) DEFAULT NULL",
					'question_lang' => "`question_lang` varchar(5) NOT NULL",
					'question_user_email' => "`question_user_email` varchar(200) NOT NULL",
					'question_status_id' => "`question_status_id` tinyint(4) NOT NULL",
					'question_accept' => "`question_accept` int(11) NOT NULL",
					'CREATE' => "`question_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`question_id`)"
				)
			)
		);
	}
}

function FAQs(){
    global  $sql,
			$contTPL,
			$user,
			$sqlTPL;

    $message = '';

    if(isset($_POST['sendQuestion'])){
        $question_post = $_POST['sendQuestion'];

        if($question_post['captcha'] == $_SESSION['capcha'] && !empty($question_post['email']) &&! empty($question_post['name'])){

			$id = $sql->insert('_question',
				array(
					'user_name'=>$question_post['name'],
					'user_email'=>$question_post['email'],
					'cat_id'=>$question_post['cat_id'],
					'text'=>$question_post['text']
				),'question_',FALSE,TRUE);

            $sql->insert('_answer',array('answer_question_id'=>$id));

			$message = _lang('Ваш вопрос отправлен!').'<br>';
        }else{
            if($question_post['captcha'] != $_SESSION['capcha']) $message .= _lang('Неверно введена captcha!').'<br>';
            if(empty($question_post['name'])) $message .= _lang('Введите имя!').'<br>';
            if(empty($question_post['email'])) $message .= _lang('Введите Email!').'<br>';
        }
    }

    $sqlTPL->getCategories('FAQs');

	global $list;

    $questions_array = $sql->query("
         SELECT *
         FROM _question q
         JOIN _answer a ON q.question_id = a.answer_question_id
         WHERE q.question_status_id=1
         ORDER BY question_id DESC
        ");


    $questions = '<table>';
    foreach($questions_array as $question){
        $questions .= '<tr><td><b>'.$question['question_user_name'].':</b> '.$question['question_text'].'<p>'.$question['answer_text'].'</p></td></tr>';
    }
    $questions .= '</table>';

    $form = '
    <form class="list" method="POST">
		<table>
			<tr>
				<td>
					<label><b>'._lang('Имя').'</b>:</label><br>
					<input placeholder="'._lang('Имя').'" type="text" value="'.(isset($question_post['name'])?$question_post['name']:'').'" name="sendQuestion[name]"/>
                </td>
			</tr>
            <tr>
                <td>
                    <label><b>'._lang('Email').'</b>:</label><br>
					<input placeholder="'._lang('Email').'" type="text" value="'.(isset($question_post['email'])?$question_post['email']:'').'" name="sendQuestion[email]"/>
                 </td>
			</tr>
			<tr>
			    <td>
                    <label><b>'._lang('Тема вопроса').'</b>:</label><br>
			        <select name="sendQuestion[cat_id]">
			            '.$contTPL->catHierarchTree(@$list->category['FAQs']).'
                    </select>
			    </td>
			</tr>
            <tr>
				<td>
                    <label><b>'._lang('Вопрос').'</b>:</label><br>
					<textarea name="sendQuestion[text]">'.(isset($question_post['text'])?$question_post['text']:'').'</textarea>
                </td>
			</tr>
            <tr>
				<td>
                    <label><b>'._lang('Введите код с картинки').'</b>:</label><br>
                    <input type="text" name="sendQuestion[captcha]"/>
                    <img src="'.SITE_FOLDER.'plugins/captcha/" id="captcha" onclick="UpdateCaptcha()">
                </td>
			</tr>
			<tr>
				<td>
					<input type="submit" class="btn" value="'._lang('Отправить').'"/>
				</td>
			</tr>
		</table>
	</form>
	<script type="text/javascript">
	    var i = 1;
	    function UpdateCaptcha(){
	        i++;
            document.getElementById("captcha").src="";
            document.getElementById("captcha").src="'.SITE_FOLDER.'plugins/captcha/?"+i;
        }
    </script>
    ';

    $display = $message . $questions . $form;

    return $display;
}

_add_filter('adminPanel_sidebar_menu', 'FAQs_AP_sidebar_menu');

function FAQs_AP_sidebar_menu($result){//модифікація меню адмін панелі в правій колонці
	return $result . u_ifRights(
		 array(100),
		'<div id="menu" class="'._getActive(@$_GET['admin'],'FAQs').'">
			<a href="'.getURL('admin','FAQs_questions').'" id="menua">'._lang('FAQs').'</a>
						<ul class="submenu">
				<li>
					<a href="'.getURL('admin','FAQs_questions').'">'._lang('Вопросы').'</a>
				</li>
                <li>
					<a href="'.getURL('admin','FAQs_questions_answer').'">'._lang('Отвеченные').'</a>
				</li>
				<li>
					<a href="'.getURL('admin','categories','category_type=FAQs').'">
						'._lang('Категории').'
					</a>
				</li>
			</ul>
		</div>'
	);
}

_add_action('adminPanel_controller', 'FAQs_admin_controller');

function FAQs_admin_controller(){ //доповнення роутера AdminPanel'і

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL;

	switch(@$_GET['admin']){
		case'FAQs_questions':
			require_once __DIR__ . '/a_faqs_questions.php';
			return TRUE;
		break;
        case'FAQs_questions_answer':
			require_once __DIR__ . '/a_faqs_questions_answer.php';
			return TRUE;
		break;
        case'FAQs_send_answer':
			require_once __DIR__ . '/a_faqs_send_answer.php';
			return TRUE;
		break;
		case'FAQs-answer-edit':
			require_once __DIR__ . '/a_faqs_answer_edit.php';
			return TRUE;
		break;

	}
}

function FAQs_hierarchTree($arrays,$level=0,$theme='table'){ //вертає іерархічну структуру категорій

	$text='';

    if(is_array($arrays)){

		global $list;
		$pre='';

		switch($theme){

			case'table':

				for($i=1; $i<$level;$i++){
					$pre .= '|&rarr; ';
				}
                foreach($arrays as $array)
				$text.='
				<tr>
					<td>
						'.$array['question_user_name'].'
					</td>
					<td>
						'.$array['question_text'].'
					</td>
					<td>
                        '.$array['question_user_email'].'
					</td>
                    <td>
						<input type="checkbox" '.(($array['question_status_id']==1)?'checked':'').' name="publishedQuestion['.$array['question_id'].']"/> &nbsp;&nbsp;
					    <input type="hidden" name="publishedQuestion_all['.$array['question_id'].']" value="text">
					</td>
                    <td>
                        '.$pre.'<a title="'._lang('Ответить').'" href="'.getURL('admin','FAQs_send_answer','question='.$array['question_id']).'">'._lang('Ответить').'</a>
					</td>
				</tr>';

			break;

            case'table_answer':

				for($i=1; $i<$level;$i++){
					$pre .= '|&rarr; ';
				}
                foreach($arrays as $array)
				$text.='
				<tr>
					<td>
						'.$array['question_user_name'].'
					</td>
					<td>
						'.$array['question_text'].'
					</td>
                    <td>
						'.$array['answer_text'].'
					</td>
                    <td>
						<input type="checkbox" '.(($array['question_status_id']==1)?'checked':'').' name="publishedQuestion['.$array['question_id'].']"/> &nbsp;&nbsp;
					    <input type="hidden" name="publishedQuestion_all['.$array['question_id'].']" value="text">
					</td>
				</tr>';

			break;

		}
	}
	return $text;
}