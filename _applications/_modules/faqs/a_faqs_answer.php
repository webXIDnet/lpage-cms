<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Відправка листа відповіді
 * 
***/


/** Формування форми відповіді **/

if(isset($_POST['sendMail'])){
    $sendMail = $_POST['sendMail'];
    $text = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>'.$sendMail['topic'].'</title></head><body>'.$sendMail['text'].'</body></html>';
    if(mail($sendMail['email'], $sendMail['topic'], $text, "Content-type: text/plain; charset=utf-8")){

        $PData->content('Отправлено','message',TRUE);

    }else{

        $PData->content('Произошел сбой алгоритма. Сообщение не отправилось. Обратитесь пожалуйста к разработчикам.','message');

    }
}

if(isset($_GET['question']) && !empty($_GET['question']))
    $question_id=$_GET['question'];

$qval = $sql->query("
		SELECT *
		FROM _question
		WHERE question_id='"._protect($question_id)."'
	",'value');

$PData->content(_lang('Ответ на вопрос').' №'.$question_id,'title');

$PData->content('

	<form class="list" method="POST">
		<table>
            <tr>
		        <td>
		          <b>'._lang('Имя').':</b> '.$qval['question_user_name'].'
		        </td>
		    </tr>
            <tr>
		        <td>
		          <b>'._lang('Email').':</b> '.$qval['question_user_email'].'
		        </td>
		    </tr>
		    <tr>
		        <td>
		          <b>'._lang('Вопрос').':</b> '.$qval['question_text'].'
		        </td>
		    </tr>
			<tr>
				<td>
					<label><b>'._lang('Тема').'</b>:</label>
					<input placeholder="'._lang('Тема').'" type="text" name="sendMail[topic]"/>
                    <label><b>'._lang('Текст сообщения').'</b>:</label>
					'.$admin->getWIZIWIG('sendMail[text]',_unprotect(_lang('Текст сообщения'),'textarea'),'elm1').'
                    <br>
                    <input type="hidden" name="sendMail[email]" value="'.$qval['question_user_email'].'">
					<input type="submit" class="btn" value="'._lang('Отправить').'"/>
				</td>
			</tr>
		</table>
	</form>
	<hr/>

');