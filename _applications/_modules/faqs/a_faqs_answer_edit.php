<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Відправка листа відповіді
 * 
***/


/** Формування форми відповіді **/

$sqlTPL->getCategories('FAQs');

global 	$list;

if(isset($_POST['answerSubmit'])){
	
	$sql->update( '_question', "question_id='"._protect($_GET['id'])."'", $_POST['question'],'question_');
	$sql->update( '_answer', "answer_question_id='"._protect($_GET['id'])."'", $_POST['answer'],'answer_');
	$PData->content('Запись сохранена','message',TRUE);
}

if(isset($_GET['id']) && !empty($_GET['id']))
    $question_id=$_GET['id'];

$qval = $sql->query("
		SELECT *
	    FROM _question q
	    JOIN _answer a ON q.question_id = a.answer_question_id
		WHERE answer_question_id='"._protect($question_id)."'
	",'value');

$PData->content(_lang('Ответ на вопрос').' №'.$question_id,'title');

$PData->content('

	<form class="list" method="POST">

        <p>
          <b>'._lang('Имя').':</b> '.$qval['question_user_name'].'
        </p>

        <p>
          <b>'._lang('Email').':</b> '.$qval['question_user_email'].'
        </p>

        <p>
        	 <label><b>'._lang('Текст вопроса').'</b>:</label>
			'.$admin->getWIZIWIG('question[text]',_unprotect($qval['question_text'],'textarea'),'elm1').'
        </p>

		<p>
            <label><b>'._lang('Текст ответа').'</b>:</label>
			'.$admin->getWIZIWIG('answer[text]',_unprotect($qval['answer_text'],'textarea'),'elm2').'
		</p>

		<input name="answerSubmit" type="submit" class="btn" value="'._lang('Сохранить').'"/>
	</form>
	<hr/>

');