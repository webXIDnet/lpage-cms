<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/

$sqlTPL->getCategories('FAQs');

global $list;

if(isset($_GET['category_url']) && !empty($_GET['category_url']))
    $category_url=$_GET['category_url'];

if(isset($_POST['submit'])){

	$mes = '';

	if(isset($_POST['unPubFAQs_all']) && is_array($_POST['unPubFAQs_all'])){
		$where = '';
        foreach($_POST['unPubFAQs_all'] as $key=>$val){
        	if(!empty($where))$where .=' OR ';
            $where .= "question_id='"._protect($key)."'";
        }
        $sql->update( '_question', $where, array('question_status_id'=>0) );
        $mes = '1';
    }


	if(!empty($_POST['pubFAQs']) &&  is_array($_POST['pubFAQs'])){
		$where = '';
	    foreach($_POST['pubFAQs']  as $key=>$val){

			if(!empty($where))$where .=' OR ';

			$where .= "question_id='"._protect($key)."'";
	    }

    	$sql->update( '_question', $where, array('question_status_id'=>1) );
    	$mes = '1';
	}

    if(isset($_POST['delFAQs']) && is_array($_POST['delFAQs'])){
		$where = '';
		foreach($_POST['delFAQs'] AS $key => $val){
			if(!empty($where))$where .=' OR ';

			$where .= "question_id='"._protect($key)."'";
		}
		$sql->delete( '_question', $where );
		$PData->content('Запись удалена','message',TRUE);
	}

    if(!empty($mes)){
        $PData->content('Запись сохранена','message',TRUE);
    }
}

if(is_array($list->category['FAQs'])){
	foreach($list->category['FAQs'] AS $key=>$val){
		if($val['c_url']==@$_GET['category_url']){
			$question_cat_id = $key;
		}
	}
}else{
	$PData->content('FAQs Вопросы','title');

	$PData->content('
		<div class="well">
		'._lang('Вы еще не создали категорию вопросов').'
		<a href="'.getURL('admin','categories','category_type=faq').'">'._lang('Создать').'</a>
		</div>
	');

	return;
}


$text=$where='';
$where = 'WHERE question_accept=1';

if(!empty($_GET['filter'])){
	$where .= " AND question_text='"._protect($_GET['filter'])."'";
}
if(!empty($_GET['filter-cat'])){
	$where .= " AND question_cat_id='"._protect($_GET['filter-cat'])."'";
}

$where = _run_filter('FAQs_where_answer_filter',$where);

$nav = _pagination(
    '_question',
    'question_id',
    $where);

$result = $sql->query("
	SELECT *
    FROM _question q
    JOIN _answer a ON q.question_id = a.answer_question_id
	{$where}
	ORDER BY q.question_id DESC
	".@$nav['limit']."
");


$title = @$list->category_index[@$question_cat_id]['c_title'];
if(!$title){
	$title = _lang('Все категории');
}


foreach($result as $cval){
	$cat = $list->category_index[$cval['question_cat_id']]['c_title'];

	$text.='
	<tr>
		<td>
			<label><input type="checkbox" name="delFAQs['.$cval['question_id'].']"/> '.$cval['question_id'].'</label>
		</td>
		<td>
			<a href="'.getURL('admin','FAQs-answer-edit','id='.$cval['question_id']).'" title="'._lang('Редактировать').'">'.$cval['question_text'].'</a>
		</td>
        <td>
        	<a href="'.getURL('admin','FAQs-answer-edit','id='.$cval['question_id']).'" title="'._lang('Редактировать').'">'.$cval['answer_text'].'</a>
		</td>
		<td style="color:#999">
			'._lang('Категория').': <span style="color:#333">'.$cat.'</span><br/>
			'._lang('Имя').': <span style="color:#333">'.$cval['question_user_name'].'</span><br/>
			'._lang('Email').': <span style="color:#333">'.$cval['question_user_email'].'</span><br/>
			'._run_filter('FAQs_table_position','',$cval).'
		</td>
		<td>
			<input type="checkbox" '.(($cval['question_status_id']==1)?'checked="checked"':'').' name="pubFAQs['.$cval['question_id'].']"/>
		    <input type="hidden" name="unPubFAQs_all['.$cval['question_id'].']" value="text">
		</td>
	</tr>';
}






$PData->content('FAQs Отвеченные ('.$title.')','title');

$PData->content('
		<form class="list">
			<table>
				<tr>
					<td>
						<label>'._lang('Поиск по заголовку').':</label>
						<input type="hidden" name="admin" value="'.@$_GET['admin'].'"/>
						<input type="text" name="filter" value="'.@$_GET['filter'].'"/><br/>
						<input type="submit" class="btn" value="'._lang('Искать').'"/>
					</td>
					<td>
						<label>'._lang('Категории').'</label>
						<select name="filter-cat">
							<option value="">- '._lang('Категория не выбрана').' -</option>
							'.$contTPL->catHierarchTree(@$list->category['FAQs'],0,@$_GET['filter-cat']).'
						</select>
					</td>
					<!-- Filter: FAQs_answer_filter_form -->
					'._run_filter('FAQs_answer_filter_form').'
				</tr>
			</table>
		</form>
		<div class="well">
		<form class="list" method="POST">
		    <table class="table">
		      <thead>
		        <tr>
		        	<th style="width:20px">'._lang('Удалить').'</th>
					<th>'._lang('Вопрос').'</th>
					<th>'._lang('Ответ').'</th>
					<th>'._lang('Доп. данные').'</th>
					<th style="width:20px">'._lang('Опубликован').'</th>
		        </tr>
		      </thead>
		      <tbody>
		        	'.$text.'
		      </tbody>
		    </table>
		    <input class="btn btn-primary" name="submit" type="submit" value="'._lang('Сохранить').'">
		</form>
	</div>

'.$nav['html']);