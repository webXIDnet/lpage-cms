<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Відправка листа відповіді
 * 
***/


/** Формування форми відповіді **/

$sqlTPL->getCategories('FAQs');

global 	$list,
		$meta;

if(isset($_POST['sendMail'])){
    $sendMail = $_POST['sendMail'];
    $text = $sendMail['text'].'<br/><br/>---<br/><blockquote>'.$sendMail['question'].'</blockquote>';
    
    if(_eMail($sendMail['email'], 'Re: '.$sendMail['topic'], $text, $meta->val('contact_email'))){
		
        $PData->content('Письмо отправлено','message',TRUE);

        $sql->update( '_question', "question_id='"._protect($_GET['question'])."'", array('question_accept'=>1));
        $sql->update( '_answer', "answer_question_id='"._protect($_GET['question'])."'", array('answer_text'=>$sendMail['text']));
		
		$PData->content('Запись сохранена','message',TRUE);
    }else{
        $PData->content('Произошел сбой алгоритма. Сообщение не отправилось. Обратитесь пожалуйста к разработчикам.','message');
    }
}

if(isset($_GET['question']) && !empty($_GET['question']))
    $question_id=$_GET['question'];

$qval = $sql->query("
		SELECT *
		FROM _question
		WHERE question_id='"._protect($question_id)."'
	",'value');

$PData->content(_lang('Ответ на вопрос').' №'.$question_id,'title');

$PData->content('

	<form class="list" method="POST">

        <p>
          <b>'._lang('Имя').':</b> '.$qval['question_user_name'].'
        </p>

        <p>
          <b>'._lang('Email').':</b> '.$qval['question_user_email'].'
        </p>

        <p>
          <b>'._lang('Вопрос').':</b> '.$qval['question_text'].'
        </p>

		<p>
            <label><b>'._lang('Текст ответа').'</b>:</label>
			'.$admin->getWIZIWIG('sendMail[text]',_unprotect(_lang('Текст ответа'),'textarea'),'elm1').'
            <br>
            <input type="hidden" name="sendMail[email]" value="'.$qval['question_user_email'].'">
            <input type="hidden" name="sendMail[topic]" value="'.$list->category_index[@$qval['question_cat_id']]['c_title'].'">
            <input type="hidden" name="sendMail[question]" value="'.$qval['question_text'].'">
		
		</p>
		<input type="submit" class="btn" value="'._lang('Отправить').'"/>
	</form>
	<hr/>

');