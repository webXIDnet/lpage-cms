<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 02.07.2015
 * Time: 11:29
 */
if (PAGE_TYPE == 'admin') {
    _add_action('defined_modules', 'lang_table_install', 1);
}

if ($PData->GetSettings('multilang') === true) {
    _add_action('defined_modules', 'languages_userlang_fill', 5);
    _add_action('defined_modules', 'languages_redirect', 6);
} else {
    _add_action('defined_modules', 'default_lang', 1);
}
function default_lang() {
    global $user, $meta, $sql;
     $default_lang = $sql->query("SELECT * FROM _languages WHERE l_id=".$meta->val('md_lang'), 'value');
     $user->lang = $default_lang['l_code'];
     $user->lang_id = $default_lang['l_id'];
     unset($default_lang);
}
//
///** Создатель базы **/

// exit;
function languages_userlang_fill()
{
    global $meta, $user, $sql, $langs_list;
    $langs_list = array();
    $langs = $sql->query("SELECT * FROM _languages");
    foreach ($langs as $l) {
        $langs_list [$l['l_id']] = $l['l_code'];
    }

    if (isset($_GET['lang']) && in_array($_GET['lang'], $langs_list)) {
        $lang = $_GET['lang'];
        $user->lang = $lang;

        $user->lang_id = get_lang_id($lang);
        setcookie('lang', $lang, 0, '/');

    } else {
        if (!isset($_COOKIE['lang'])) {
            $default_lang = $sql->query("SELECT l_code FROM _languages WHERE l_id='" . $meta->val('md_lang') . "'", 'value');
            if (!isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $lang = $default_lang['l_code'];

            } else {
                $lang = $sql->query("SELECT l_code FROM _languages WHERE l_code='" . substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) . "'", 'value');
                if (!isset($lang['l_code'])) {
                    $lang = $default_lang['l_code'];
                } else {
                    $lang = $lang['l_code'];
                }
            }
            $user->lang = $lang;
            $user->lang_id = get_lang_id($lang);
            setcookie('lang', $lang, 0, '/');
        } else {
            $user->lang = $_COOKIE['lang'];
            $user->lang_id = get_lang_id($_COOKIE['lang']);
        }

    }


}

function languages_redirect() {
    global $meta, $user, $sql, $PData, $langs_list;

    $url = explode(HOMEPAGE, PAGE_URL);
    $first = array_shift($url);
    $url = array_shift($url);
    preg_match_all("/\?.*/i",$url, $vars);
    $vars = array_shift($vars);
    $vars = array_shift($vars);

    switch(PAGE_TYPE) {
        case 'post': {
            $old_post = $sql->query("SELECT * FROM _posts LEFT JOIN _categories ON (p_cat_id=c_id) LEFT JOIN _languages ON (p_lang=l_id) WHERE p_cat_id > 0 AND p_article='".@$_GET['post']."'", 'value');
            if ($old_post['l_code'] != $user->lang) {
                $post = $sql->query("SELECT * FROM _posts LEFT JOIN _categories ON (p_cat_id=c_id) WHERE p_cat_id > 0 AND p_id='".$old_post['p_id']."' AND p_lang='".$user->lang_id."' AND c_lang='".$user->lang_id."'", 'value');
                if (count($post) > 1) {
                    $url = HOMEPAGE.$user->lang."/".$post['c_url']."/".$post['p_article'].".html".$vars;
                    $PData->redirect($url, '301');
                } /*else {
                    $url = HOMEPAGE.$old_post['l_code']."/".$old_post['c_url']."/".$old_post['p_article'].".html";
                    $PData->redirect($url, '301');
                }*/

            } else {
                if (!isset($_GET['lang'])) {
                   $url = HOMEPAGE.$old_post['l_code']."/".$old_post['c_url']."/".$old_post['p_article'].".html".$vars;
                   $PData->redirect($url, '301');
                }
            }
            break;
        }
        case 'page': {
//            var_dump($user->lang);
            $old_page = $sql->query("SELECT * FROM _posts LEFT JOIN _categories ON (p_cat_id=c_id) LEFT JOIN _languages ON (p_lang=l_id) WHERE p_cat_id='-1' AND p_article='".@$_GET['page']."'", 'value');

            if ($old_page['l_code'] != $user->lang) {
                $page = $sql->query("SELECT * FROM _posts LEFT JOIN _categories ON (p_cat_id=c_id) WHERE p_cat_id='-1' AND p_id='".$old_page['p_id']."' AND p_lang='".$user->lang_id."'", 'value');
//                _dump($user->lang_id);
                if (count($page) > 1) {
                    $url = HOMEPAGE.$user->lang."/".$page['p_article']."/".$vars;
                    $PData->redirect($url, '301');
                } /*else {
                    $url = HOMEPAGE.$old_page['l_code']."/".$old_page['c_url']."/".$old_page['p_article']."/";
                    $PData->redirect($url, '301');
                }*/

            } else {
                if (!isset($_GET['lang'])) {
                    $url = HOMEPAGE.$old_page['l_code']."/".$old_page['p_article']."/".$vars;
                    $PData->redirect($url, '301');
                }
            }
            break;
        }
        case 'category': {

            $old_cat = $sql->query("SELECT * FROM _categories LEFT JOIN _languages ON (c_lang=l_id) WHERE c_url='".@$_GET['category']."'", 'value');
            if ($old_cat['l_code'] != $user->lang) {
                $cat = $sql->query("SELECT * FROM _categories LEFT JOIN _languages ON (c_lang=l_id) WHERE c_id='".$old_cat['c_id']."' AND c_lang='".$user->lang_id."'", 'value');

                if (count($cat) > 1) {
                    $url = HOMEPAGE.$user->lang."/category/".$cat['c_url']."/".$vars;
                    $PData->redirect($url, '301');
                } /*else {
                    $url = HOMEPAGE.$old_cat['l_code']."/category/".$old_cat['c_url']."/";
                    $PData->redirect($url, '301');
                }*/

            } else {
                if (!isset($_GET['lang'])) {
                    $url = HOMEPAGE.$old_cat['l_code']."/category/".$old_cat['c_url']."/".$vars;

                    $PData->redirect($url, '301');
                }
            }

            break;
        }
        case 'homepage': {

            if (!isset($_GET['lang']) && $_GET['homepage'] != 'home' || !in_array(@$_GET['lang'], $langs_list)) {
                $PData->redirect(HOMEPAGE.$user->lang."/".$vars, '301');
            }

            break;
        }
        case 'lp': {
            if (!isset($_GET['lang']) && $_GET['lp'] == 'home') {
                    $PData->redirect(HOMEPAGE.$user->lang."/".$vars, '301');
            }
            break;
        }
    }

}

function lang_table_install() {
        global $sql;

        $sql->db_table_installer('languages', '02.06.2015',
            array(
                '_languages' => array(
                    'l_id' => "`l_id`  TINYINT(4) NOT NULL AUTO_INCREMENT",
                    'l_title' => "`l_title` varchar(50) NOT NULL",
                    'l_code' => "`l_code` varchar(10) NOT NULL",
                    'CREATE' => "`l_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`l_id`), `l_code` VARCHAR(10) NOT NULL,UNIQUE INDEX `l_code` (`l_code`)"
                )
            )
        );
}

if (PAGE_TYPE == 'admin') {
    _add_filter('adminPanel_sidebar_menu', 'languages_sidebar_menu_settings', 100);
}

function languages_sidebar_menu_settings($result)
{//модифікація меню адмін панелі в правій колонці
    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Мультиязычность'), //ім"я пункта меню
            'url' => getURL('admin', 'languages'), //посилання
            'submenu_filter' => 'languages_sidebar_menu', //хук для підключення пунктів підменю
            'active_pages' => _run_filter('languages_admin_sidebar_menu_settings',
                array(
                    'languages',
                    'languages/orders',
                    'languages/clients',
                    'languages/stat'
                )), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100), $menu);

    return $result . @$menu;
}
if (PAGE_TYPE=='admin') {
//    _add_filter('languages_sidebar_menu', 'languages_categories_sidebar_menu_settings',100);
}

function languages_categories_sidebar_menu_settings($result)
{//модифікація меню адмін панелі в правій колонці
    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Категории'), //ім"я пункта меню
            'url' => getURL('admin','languages/category_list'), //посилання
            'submenu_filter' => 'languages_com_product_category_sidebar_menu', //хук для підключення пунктів підменю
            'active_pages' => _run_filter('languages_admin_sidebar_menu_settings', array('languages')), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100), $menu);

    return $result . @$menu;
}


/** Роутер Адмінки для мультиблоків **/

_add_action('adminPanel_controller', 'languages_admin_controller');

function languages_admin_controller()
{ //доповнення роутера AdminPanel'і

    global $PData,
           $sql,
           $meta;

    switch (@$_GET['admin']) {
        case'languages':
            require_once __DIR__ . '/languages_editor.php';
            return TRUE;
            break;
        case'languages/category_list':
            require_once __DIR__ . '/category_list.php';
            return TRUE;
            break;
        case'languages/category_edit':
            require_once __DIR__ . '/category_edit.php';
            return TRUE;
            break;
        case'languages/install':
            require_once __DIR__ . '/languages_install.php';
            return TRUE;
            break;
    }
}

function get_lang_id($code) {
    global $sql;
    $l_id = $sql->query("SELECT l_id FROM _languages WHERE l_code='".$code."'", 'value');

    return isset($l_id['l_id']) ? $l_id['l_id'] : '';
}
/*
 * Show languages on the site
 * [:ViewLangs:]
 */
function ViewLangs() {
    global $sql, $user,$PData;

    $html = '';
    if($PData->GetSettings('multilang')) {//if multilanguage turned off not show langs

        $langs = $sql->query("SELECT * FROM _languages");
        $lang = $user->lang;
        $vars_array['langs']=$langs;
        $vars_array['lang']=$lang;
        return $PData->getModTPL('site/show_langs', 'languages', @$vars_array);
    }else{
        return '<!--multilanguages turned off-->';
    }
}