<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Мов
 *  
***/

global 	$language;

require_once __DIR__ . '/lang_fn.php';


if(isset($_GET['lang'])){
	if($_GET['lang']=='uk')$_GET['lang']='ua';
    $user->lang = $language = $_GET['lang']; //язык по умолчанию   
}elseif(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
	$_GET['lang'] = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	if($_GET['lang']=='uk')$_GET['lang']='ua';
	
    $user->lang = $language = $_GET['lang'];
}else{
    $user->lang = $language = $PData->GetSettings('lang');
}

_add_filter('getCategories_SQL_query','Language_getCategories_SQL_query');


if(PAGE_TYPE=='lp' || PAGE_TYPE=='page' || PAGE_TYPE=='post'){ 
	//переадресовує на правильне посилання, якщо в посиланні вказано не правильне значення мови
	//пріоритетом є артикул сторінки
	_add_action('LPage_init_done','Language_URL_Change');
	_add_action('page_init','Language_URL_Change');
	_add_action('post_init','Language_URL_Change');
}

if(isset($_GET['lang-change']) && !empty($_GET['lang-change'])){ 
	//переключае мову користувача
	_add_action('LPage_init_done','Language_Switch');
	_add_action('page_init','Language_Switch');
	_add_action('post_init','Language_Switch');
}

if(PAGE_TYPE=='admin'){
	
	
	_add_filter('User_profile_update_array', 'Language_User_profile_update_array'); //змінює дані, що записуються в базу
	_add_filter('User_profile_edit_form', 'Language_User_profile_edit_form'); //виводить стисок мов у налаштуваннях профілю в адмінці
	
	
	_add_filter('Blog_page_insert_array', 'Language_Blog_page_insert_array'); //змінює дані, що записуються в базу по новій сторінці блогу
	_add_filter('Blog_post_insert_array', 'Language_Blog_page_insert_array'); //змінює дані, що записуються в базу по новій поста блогу
	_add_filter('LPage_page_insert_array', 'Language_Blog_page_insert_array'); //змінює дані, що записуються в базу по новій сторінці LP
	
	
	_add_filter('Blog_page_update_array', 'Language_Blog_page_insert_array'); //змінює дані, що записуються в базу по вже існуючій сторінці блогу
	_add_filter('Blog_post_update_array', 'Language_Blog_page_insert_array'); //змінює дані, що записуються в базу по вже існуючій поста блогу
	_add_filter('LPage_page_update_array', 'Language_Blog_page_insert_array'); //змінює дані, що записуються в базу по вже існуючії сторінки LP
	
	
	
	_add_filter('LPage_QUERY_WHERE','Language_LPage_QUERY_WHERE'); //змінює строку запиту в базу для виводу записів лише з вказаною формою на сторінці списку лендінгів в адмінці
	_add_filter('Blog_posts_QUERY_WHERE','Language_Blog_posts_QUERY_WHERE'); //змінює строку запиту в базу для виводу записів лише з вказаною формою на сторінці списку постів блогу в адмінці
	_add_filter('Blog_page_QUERY_WHERE','Language_Blog_page_QUERY_WHERE'); //змінює строку запиту в базу для виводу записів лише з вказаною формою на сторінці списку сторінок блогу в адмінці
	_add_filter('Blog_category_QUERY_WHERE','Language_Blog_category_QUERY_WHERE'); //змінює строку запиту в базу для виводу записів лише з вказаною формою на сторінці списку категорій в адмінці
	
		
	_add_filter('LPage_table_additional_data','Language_LPage_table_additional_data'); //виводить дані у комірку "Додаткові дані" на сторінці списку лендінгів в адмінці
	_add_filter('Blog_posts_table_additional_data','Language_LPage_table_additional_data'); //виводить дані у комірку "Додаткові дані" на сторінці списку постів блогу в адмінці
	_add_filter('app_admin_CatList_addData','Language_app_admin_CatList_addData'); //виводить дані у комірку "Додаткові дані" на сторінці списку категорій в адмінці
	
	
	
	_add_filter('LPage_filter_form','Language_LPage_filter_form'); //виводить додаткове полі у фільтр на сторінці списку посадок
	_add_filter('Blog_posts_filter_form','Language_LPage_filter_form'); //виводить додаткове полі у фільтр на сторінці списку постів блогу
	_add_filter('Blog_page_filter_form','Language_LPage_filter_form'); //виводить додаткове полі у фільтр на сторінці списку сторінок блогу
	_add_filter('app_admin_filter_of_Cat_addField','Language_LPage_filter_form'); //виводить додаткове полі у фільтр на сторінці списку категорій
	
	_add_filter('LPage_Add_Page','Language_LPage_Add_Page'); //виводить додаткові поля на сторінку додавання нової сторінки LP
	_add_filter('Blog_post_Add_Page_3','Language_Blog_post_Add_Page_1'); //виводить додаткові поля на сторінку додавання нового поста блога в адмінці
	_add_filter('Blog_page_Add_Page','Language_Blog_page_Add_Page'); //виводить додаткові поля на сторінку додавання нового поста блога в адмінці
	_add_filter('app_admin_newCat_addField','Language_app_admin_newCat_addField'); //виводить додаткові поля у форму додавання нової категорії в адмінці
	
	
	_add_filter('LPage_Edit_Page_1','Language_LPage_Edit_Page_1'); //виводить додаткові поля на сторінку редагування сторінки LP
	_add_filter('Blog_post_Edit_Page_3','Language_Blog_post_Edit_Page_1'); //виводить додаткові поля на сторінку редагування поста блогу в адмінці
	_add_filter('Blog_page_Edit_Page','Language_Blog_page_Edit_Page'); //виводить додаткові поля на сторінку редагування сторінки блога в адмінці
	_add_filter('LPage_Edit_Page_3','Language_LPage_Edit_Page_3'); //виводить додаткові поля на сторінку редагування сторінки блога в адмінці
	
	
	_add_filter('Blog_page_table_additional_data','Language_Blog_page_table_additional_data'); //виводить додаткові поля на сторінку редагування поста блога в адмінці
	
	_add_filter('app_admin_editCat_addField','Language_app_admin_editCat_addField'); //виводить додаткові поля на сторінку редагування категорії в адмінці
	
	_add_filter('languages_admin-panel_mod_settings','Languages_adminpanel_mod_settings'); //виведення налаштувань модуля в адмінці 
}