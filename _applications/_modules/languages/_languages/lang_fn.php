<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента Опрацювання замовлень для CRM системи
 *
 ***/

function Language_front_select(){
	
	if(PAGE_TYPE=='admin')return '[:Language_front_select:]';
	
    global 	$PData,
           	$sql,
			$user;

	$langs = getLangs('array');
	
    $sep = '';
    $select = '<div class="lang-switcher">';
    
    if(is_array($langs)){
	    foreach($langs as $kay=>$lang){
	    	
	    	$url = PAGE_URL.'?lang-change='.$lang['article'];
	    	$active = '';
	    	
	    	if($user->lang == $kay){
	    		$active = 'active';
	    		$url = PAGE_URL;
	    	}
	    		
			
			if(PAGE_TYPE=='homepage'){
				$url = getURL('lp',$_GET['lp'],'lang-change='.$lang['article']);
				if($user->lang == $lang['article']) $url = getURL('lp',$_GET['lp']);
			}
	    	
	        $select .= $sep.'<a class="lang-item lang-'.$lang['article'].' '.$active.'" href="'.$url.'" >'._lang($lang['article']).'</a>';
	        $sep = '<span class="separator"> | </span>';
	    }
    }
    
    return $select.'</div>';
}

function getLangs($val=FALSE,$selected_key=''){
	
	global	$meta;	
	
	$langs = $meta->val('apSet_languages','mod_option');
	$array = $langs['langs'];
		
	if(isset($array[$val]))
		return _lang($array[$val]);
	elseif(!$val)
		return '---';
	elseif($val=='select'){
		$select = '';
		if(is_array($array))
			foreach($array AS $key=>$lang){
				$selected = '';
				if($selected_key==$key)$selected = 'selected="selected"';
				$select .= '<option value="'.$key.'" '.$selected.'>'._lang($lang).'</option>'; 
			}
		return $select;
	}else{
		return $array;
	}
}

function Language_User_profile_update_array($result){
	
    global $admin;
	
	$m=date('m')+1;
	$Y=date('Y');
	
	if($m==13){
		$m='01';
		$Y=$Y+1;
	}
	
	setcookie('adminLang', $result['lang'], strtotime($Y.'-'.$m.date('-d')),'/');

    return $result;
}

function Language_User_profile_edit_form(){
	
    global $admin;
	
    $select = '
        <p>
            <h3>'._lang('Язык админки').'</h3>
            <select name="user[lang]">
	            <option value="">- '._lang('Не выбран').' -</option>
		      	'.getLangs('select',$_COOKIE['adminLang']).'
	        </select>
        </p>
    ';

    return $select;
}

function Languages_adminpanel_mod_settings($result,$set){
	
	$result['title'] .= 
		'<li switchtab="tab_u_1" class=""><a>'._lang('Языки').'</a></li>';
	
	$langs = '<p>
			<b>'._lang('Новый язык').'</b><br/>
			<label>
				<input class="input" placeholder="'._lang('Сокращение языка').'" type="text" name="newModSet[apSet_languages][langs][1]" value=""/> - '._lang('Например: ua').'
			</label>
			<label>
				<input class="input" placeholder="'._lang('Название языка').'" type="text" name="newModSet[apSet_languages][langs][0]" value=""/> - '._lang('Например: украинский').'
			</label>
			
		</p><input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit"><br/><hr/><br/>';
    
    if(is_array($set)){
		
		foreach($set AS $key_com=>$array){
			if(is_array($array)){
				foreach($array AS $key=>$val){
					if($key==100){
						$langs .= '
						<p>
							<label>
								<b>'._lang($val).'</b> ('.$key.') <label>
								<input disabled="disabled" class="input" type="text"  value="'.$val.'"/>
								<input type="hidden" name="modSet[apSet_languages]['.$key_com.']['.$key.']" value="'.$val.'"/>
							</label>
						</p>
						';
					}else{
						$langs .= '
						<p>
							<label>
								<b>'._lang($val).'</b> ('.$key.') <label>
								<input class="input" type="text" name="modSet[apSet_languages]['.$key_com.']['.$key.']" value="'.$val.'"/>
								<input type="checkbox" name="delApSet[apSet_languages]['.$key_com.']['.$key.']" value="'.$key.'"> - '._lang('Удалить').'
							</label>
						</p>
						';
					}
				}
				$langs .= '<hr/>';
			}
		}
	
	}
		
	$result['text'] .= 
		'<div id="tab_u_1" class="tab_box">
			'.$langs.'
		</div>';

    return $result;
}

function Language_URL_Change($cval){
	
	global	$PData;
	
	if((!isset($_GET['lang']) || (isset($cval['lp_lang']) && $cval['lp_lang']!=$_GET['lang']) )  && PAGE_TYPE=='lp'){
		
		$PData->redirect(getURL('lp',$cval['article'],'lang='.$cval['lp_lang']));
	
	}elseif( (!isset($_GET['lang']) || (isset($cval['p_lang']) && $cval['p_lang']!=$_GET['lang']) )  && (PAGE_TYPE=='page' || PAGE_TYPE=='post')){
	
		$PData->redirect(getURL(PAGE_TYPE,$cval['p_article'],'lang='.$cval['p_lang']));
	
	}elseif( (!isset($_GET['lang']) || (isset($cval['c_lang']) && $cval['c_lang']!=$_GET['lang']) )  && (PAGE_TYPE=='category')){
	
		$PData->redirect(getURL(PAGE_TYPE,$cval['c_url'],'lang='.$cval['c_lang']));
	
	}
}

function Language_Switch($cval){
	
	global	$PData,
			$sql;
	
	switch(PAGE_TYPE){
		case'lp':
			if($_GET['lang-change']!=$cval['lp_lang']){
	
				$lp_connect = $sql->query('
					SELECT * 
					FROM _landing_pages 
					WHERE lp_id IN ('.$cval['lp_connect_ids'].') AND lp_lang="'._protect($_GET['lang-change']).'"
				');
				
		        if(is_array($lp_connect) && !empty($lp_connect)){
		            $PData->redirect(getURL('lp',$lp_connect[0]['lp_article'],'lang='.$cval['lp_lang']));
		        }
		    }
		break;
		case'page':case'post':
			if($_GET['lang-change']!=$cval['p_lang']){
		        
				$page_connect = $sql->query('
					SELECT * 
					FROM _posts 
					WHERE p_id IN ('.$cval['p_connect_ids'].') AND p_lang="'._protect($_GET['lang-change']).'"
				');
				
		        if(is_array($page_connect) && !empty($page_connect)){
		            $PData->redirect(getURL(PAGE_TYPE,$page_connect[0]['p_article'],'lang='.$cval['p_lang']));
		        }
		    }
		break;
		case'category':
			if($_GET['lang-change']!=$cval['c_lang']){
		        
				$cat_connect = $sql->query('
					SELECT * 
					FROM _categories
					WHERE c_id IN ('.$cval['c_connect_ids'].') AND c_lang="'._protect($_GET['lang-change']).'"
				');
				
		        if(is_array($cat_connect) && !empty($cat_connect)){
		            $PData->redirect(getURL(PAGE_TYPE,$cat_connect[0]['c_url'],'lang='.$cval['c_lang']));
		        }
		    }
		break;
	}	
}

function Language_LPage_filter_form($result){
	
	return $result.'<td>
	    <label>'._lang('Язык').'</label>
        <select name="filter_lang">
            <option value="">- '._lang('Не выбран').' -</option>
	      	'.getLangs('select',@$_GET['filter_lang']).'
        </select>
	</td>';
}

function Language_LPage_Add_Page($result){
	
	global	$sql;
	
    $languages = getLangs('array');
    
    $result .=	
		'<p>
	        <label>'._lang('Язык').'</label>
	        <select name="lpAdd[lang]">
	            <option value="">- '._lang('Не выбран').' -</option>
		      	'.getLangs('select').'
	        </select>
	    </p>
		<p><label>'._lang('Языковая привязка страниц').'</label>';
    
    foreach($languages as $key=>$lang){
    	
        $posts = $sql->query("
			SELECT * 
			FROM _landing_pages 
			WHERE lp_lang='".$key."'");

        $options_p='';
        
        foreach($posts as $post){
        	
        	$selected = '';
        	if(is_array(@$_POST['lpAdd']['connect_ids']) && in_array($post['lp_id'], @$_POST['lpAdd']['connect_ids'])) $selected = 'selected="selected"';
        	
            $options_p .= '<option value="'.$post['lp_id'].'" '.$selected.'>'.$post['lp_title'].' ('.$post['lp_lang'].')'.'</option>';
            
        }

        $result .= '
            <select name="lpAdd[connect_ids][]">
                <option value="0">'._lang('- Запись перевода не выбрана -').'</option>
                '.$options_p.'
            </select> - '._lang($lang).'<br>';
    }

    $result .= '</p>';
    
    return $result;
}

function Language_Blog_post_Add_Page_1($result){
	
	global	$sql;
	
    $languages = getLangs('array');
    
    $result .= '
		<p>
			<label>'._lang('Язык').'</label>
			<select name="postAdd[lang]">
			    <option value="">- '._lang('Не выбран').' -</option>
		        '.getLangs('select',@$_POST['postAdd']['lang']).'
			</select>
		</p>
		<p><label>'._lang('Языковая привязка страниц').'</label>';
	
    foreach($languages as $key=>$lang){
    	
        $posts = $sql->query("SELECT * FROM _posts WHERE p_cat_id>0 AND p_lang='".$key."'");

        $options_p='';
        
        foreach($posts as $post){
        	
        	$selected = '';
        	if(is_array(@$_POST['postAdd']['connect_ids']) && in_array($post['p_id'], @$_POST['postAdd']['connect_ids'])) $selected = 'selected="selected"';
        	
            $options_p .= '<option value="'.$post['p_id'].'" '.$selected.'>'.$post['p_title'].' ('.$post['p_lang'].')'.'</option>';
            
        }

        $result .= '
            <select name="postAdd[connect_ids][]">
                <option value="0">'._lang('- Запись перевода не выбрана -').'</option>
                '.$options_p.'
            </select> - '._lang($lang).'<br>';
    }

    $result .= '</p>';
	
	
	return $result;
}
function Language_Blog_page_Add_Page($result){
	
	global	$sql;
	
    $languages = getLangs('array');
    
    $result .= '
		<p>
			<label>'._lang('Язык').'</label>
			<select name="postNew[lang]">
			    <option value="">- '._lang('Не выбран').' -</option>
		        '.getLangs('select',@$_POST['postNew']['lang']).'
			</select>
		</p>
		<p><label>'._lang('Языковая привязка страниц').'</label>';
	
    foreach($languages as $key=>$lang){
    	
        $posts = $sql->query("SELECT * FROM _posts WHERE p_cat_id=-1 AND p_lang='".$key."'");

        $options_p='';
        
        foreach($posts as $post){
        	
        	$selected = '';
        	if(is_array(@$_POST['postNew']['connect_ids']) && in_array($post['p_id'], @$_POST['postNew']['connect_ids'])) $selected = 'selected="selected"';
        	
            $options_p .= '<option value="'.$post['p_id'].'" '.$selected.'>'.$post['p_title'].' ('.$post['p_lang'].')'.'</option>';
            
        }

        $result .= '
            <select name="postNew[connect_ids][]">
                <option value="0">'._lang('- Запись перевода не выбрана -').'</option>
                '.$options_p.'
            </select> - '._lang($lang).'<br>';
    }

    $result .= '</p>';
	
	return $result;
}

function Language_app_admin_newCat_addField($result){
	return $result.'
            <label>'._lang('Язык').'</label>
            <select name="newCat[lang]">
            	<option value="">- '._lang('Не выбран').' -</option>
	      		'.getLangs('select').'
            </select>';
}

function Language_LPage_Edit_Page_1($result,$cval){
	return $result.'<p>
        <label>'._lang('Язык').'</label>
        <select name="lpEdit[lang]">
            <option value="">- '._lang('Не выбран').' -</option>
	        '.getLangs('select',$cval['lp_lang']).'
        </select>
    </p>';
}
function Language_Blog_post_Edit_Page_1($result,$cval){
	
	global	$sql;
	
    $languages = getLangs('array');
    
    $result .= '
		<p>
			<label>'._lang('Язык').'</label>
			<select name="postEdit[lang]">
			    <option value="">- '._lang('Не выбран').' -</option>
		        '.getLangs('select',$cval['p_lang']).'
			</select>
		</p>
		<p><label>'._lang('Языковая привязка страниц').'</label>';
		
    $connect_ids = explode(',', $cval['p_connect_ids']);

    foreach($languages as $key=>$lang){
    	
        $posts = $sql->query("SELECT * FROM _posts WHERE p_cat_id>0 AND p_id!=".$cval['p_id']." AND p_lang='".$key."'");

        $options_p='';
        
        foreach($posts as $post){
        	
        	$selected = '';
        	if(in_array($post['p_id'], $connect_ids)) $selected = 'selected="selected"';
        	
            $options_p .= '<option value="'.$post['p_id'].'" '.$selected.'>'.$post['p_title'].' ('.$post['p_lang'].')'.'</option>';
            
        }

        $result .= '
            <select name="postEdit[connect_ids][]">
                <option value="0">'._lang('- Запись перевода не выбрана -').'</option>
                '.$options_p.'
            </select> - '._lang($lang).'<br>';
    }

    $result .= '</p>';
	
	
	return $result;
	
	return $result.'<p>
	    <label>'._lang('Язык').'</label>
        <select name="postEdit[lang]">
            <option value="">- '._lang('Не выбран').' -</option>
	        '.getLangs('select',$cval['p_lang']).'
        </select>
    </p>';
}

function Language_Blog_page_insert_array($result){
	if(isset($result['connect_ids']) && is_array($result['connect_ids'])){
		$result['connect_ids'] = implode(',', $result['connect_ids']);
	}else{
		$result['connect_ids'] = '';
	}	
	
	return $result;
}

function Language_Blog_page_Edit_Page($result,$cval){
	
	global	$sql;
	
    $languages = getLangs('array');
    
    $result .= '
		<p>
			<label>'._lang('Язык').'</label>
			<select name="postEdit[lang]">
			    <option value="">- '._lang('Не выбран').' -</option>
		        '.getLangs('select',$cval['p_lang']).'
			</select>
		</p>
		<p><label>'._lang('Языковая привязка страниц').'</label>';
		
    $connect_ids = explode(',', $cval['p_connect_ids']);

    foreach($languages as $key=>$lang){
    	
        $posts = $sql->query("SELECT * FROM _posts WHERE p_cat_id=-1 AND p_id!=".$cval['p_id']." AND p_lang='".$key."'");

        $options_p='';
        
        foreach($posts as $post){
        	
        	$selected = '';
        	if(in_array($post['p_id'], $connect_ids)) $selected = 'selected="selected"';
        	
            $options_p .= '<option value="'.$post['p_id'].'" '.$selected.'>'.$post['p_title'].' ('.$post['p_lang'].')'.'</option>';
            
        }

        $result .= '
            <select name="postEdit[connect_ids][]">
                <option value="0">'._lang('- Запись перевода не выбрана -').'</option>
                '.$options_p.'
            </select> - '._lang($lang).'<br>';
    }

    $result .= '</p>';
	
	
	return $result;
	
}
function Language_LPage_Edit_Page_3($result,$cval){
	
	global	$sql;
	
    $languages = getLangs('array');
    
    $result .= '<p><label>'._lang('Языковая привязка страниц').'</label>';
    		
    $connect_ids = explode(',', $cval['lp_connect_ids']);

    foreach($languages as $key=>$lang){
    	
        $posts = $sql->query("
			SELECT * 
			FROM _landing_pages 
			WHERE lp_id!=".$cval['lp_id']." AND lp_lang='".$key."'");

        $options_p='';
        
        foreach($posts as $post){
        	
        	$selected = '';
        	if(in_array($post['lp_id'], $connect_ids)) $selected = 'selected="selected"';
        	
            $options_p .= '<option value="'.$post['lp_id'].'" '.$selected.'>'.$post['lp_title'].' ('.$post['lp_lang'].')'.'</option>';
            
        }

        $result .= '
            <select name="lpEdit[connect_ids][]">
                <option value="0">'._lang('- Запись перевода не выбрана -').'</option>
                '.$options_p.'
            </select> - '._lang($lang).'<br>';
    }

    $result .= '</p>';
	
	
	return $result;
	
}


function Language_app_admin_editCat_addField($result,$cval){	
	return $result.
		'<label><b>'._lang('Язык').'</b>:</label>
	    <select name="editCat[lang]">
	    	<option value="">- '._lang('Не выбран').' -</option>
	        '.getLangs('select',$cval['c_lang']).'
	    </select>';
}

function Language_LPage_QUERY_WHERE($result){
	if(!empty($_GET['filter_lang'])){
		if(!empty($result))$result.=' AND ';
		$result = $result." lp_lang = '"._protect($_GET['filter_lang'])."'";
	}
	return $result;
}
function Language_Blog_posts_QUERY_WHERE($result,$cval){
	if(!empty($_GET['filter_lang'])){
	    if(!empty($result))$result.=' AND ';
	    $result .= "p_lang = '"._protect($_GET['filter_lang'])."'";
	}
	return $result;
}
function Language_Blog_page_QUERY_WHERE($result,$cval){
	if(!empty($_GET['filter_lang'])){
	    if(!empty($result))$result.=' AND ';
	    $result .= "p_lang = '"._protect($_GET['filter_lang'])."'";
	}
	return $result;
}

function Language_Blog_category_QUERY_WHERE($result,$cval){
	if(!empty($_GET['filter_lang'])){
		if(!empty($result))$result .= ' AND ';
	    $result .= "c_lang = '"._protect($_GET['filter_lang'])."'";
	}
	return $result;
}

function Language_LPage_table_additional_data($result,$cval){
	if(empty($cval['lp_lang'])) $cval['lp_lang'] = _lang('Не указан'); 
	else
		$cval['lp_lang'] = getLangs($cval['lp_lang']);
		
	$result .= '<span class="greyText">'._lang('Язык').': <span style="color:#000">'.$cval['lp_lang'].'</span></span>';
	return $result;
}
function Language_Blog_page_table_additional_data($result,$cval){
	if(empty($cval['p_lang'])) $cval['p_lang'] = _lang('Не указан');
	else
		$cval['p_lang'] = getLangs($cval['p_lang']);
		
	return $result.'
        '._lang('Язык').': <span class="blackText">'.$cval['p_lang'].'</span><br/>';
}

function Language_app_admin_CatList_addData($result,$cval){
	if(empty($cval['c_lang'])) $cval['c_lang'] = _lang('Не указан');
	else
		$cval['c_lang'] = getLangs($cval['c_lang']);
	
	$result .= '<span class="greyText">'._lang('Язык').': <span style="color:#000">'.$cval['c_lang'].'</span></span><br>';
	return $result;
}

function Language_getCategories_SQL_query($result){

	if(PAGE_TYPE=='admin') return $result;
	
	global	$user;
	
	if(!empty($result))$result .= ' AND ';
	else $result .= ' WHERE ';
	
	$result .=" c_lang='"._protect($user->lang)."'";

	return $result;
}