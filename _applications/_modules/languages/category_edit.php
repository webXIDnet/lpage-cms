<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 02.07.2015
 * Time: 11:29
 */
global $sql,$meta;

$vars_array['main_lang']=$meta->val('md_lang');
$query="SELECT * FROM _languages";
$langs=$sql->query($query);
foreach($langs as $lang){
    $vars_array['langs'][$lang['l_id']]=$lang;
}

if(isset($_POST['save'])){
    foreach($_POST['lang'] as $key=>$value){
        $query="SELECT C.id FROM _categories AS C WHERE C.c_id="._protect($_GET['id'])." AND C.c_lang="._protect($key);
        $res=$sql->query($query);
        if(count($res)>0){
            if(!trim($value['c_title'])==''&&!trim($value['c_url'])=='') {
                $query = "UPDATE _categories SET c_title='" . _protect($value['c_title']) . "', c_url='" . _protect($value['c_url']) . "', c_parent_id='" . _protect($value['c_parent_id']) . "',c_desc='" . _protect($value['c_desc']) . "' WHERE  id=" . _protect($res[0]['id']);
                $sql->query($query, 'query');
            }
            else{
                //show masage
                $PData->content(_lang('Не удалось сохранить данные на языке').'"'.$vars_array['langs'][$key]['l_title'].'"','message');
            }
            //update
        }else{
            if(!trim($value['c_title'])==''&&!trim($value['c_url'])=='') {
                $query = "INSERT INTO _categories
                  (c_id,c_title, c_url, c_parent_id,c_lang,c_type,c_desc)
                VALUES
                  ('" . _protect($_POST['c_id']) . "','" . _protect($value['c_title']) . "','" . _protect($value['c_url'])
                    . "','" . _protect($value['c_parent_id']) . "','"._protect($key)."','" . _protect($_POST['c_type'])
                    . "','" . _protect($value['c_desc']) . "')";
                $sql->query($query, 'query');
                //insert
            }
        }
    }
}



//todo del '*' in query
//category in diferent languages

$query="SELECT * FROM _categories AS C ORDER BY C.c_id ASC";
$categorys=$sql->query($query);
foreach($categorys as $cat){
    if($cat['c_id']==$_GET['id']){
        $vars_array['category'][$cat['c_lang']]=$cat;
        $cat_type=$cat['c_type'];
    }else {
        $vars_array['categorys'][$cat['c_type']][$cat['c_id']][$cat['c_lang']] = $cat;
    }
}

//list of caterorys
$vars_array['c_type']=$cat_type;
$query="SELECT * FROM _categories AS C WHERE C.c_type='$cat_type' AND C.c_lang=".$vars_array['main_lang']." AND C.c_id!=".$_GET['id'];
$categories=$sql->query($query);
foreach($categories as $category){
    $vars_array['categories_of_type'][$category['c_id']]=$category;
}

$PData->content(_lang('Редактирование категории '), 'title');
$PData->content($PData->getModTPL('admin/category_edit', 'languages', @$vars_array));