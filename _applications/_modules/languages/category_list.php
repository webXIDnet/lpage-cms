<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 02.07.2015
 * Time: 11:29
 */
global $sql,$meta,$sqlTPL;

if(isset($_POST['delete_category'])&&isset($_POST['del_id'])){
    foreach($_POST['del_id'] as $cat_id=>$val){
        if(!empty($val)){
            $query = "DELETE FROM _categories  WHERE c_id=" . $cat_id;
            $sql->query($query);
        }
    }
}
//$sqlTPL->getCategories(@$category_type);
//global $list;
$vars_array['main_lang']=$meta->val('md_lang');
//todo del '*'
$query="SELECT * FROM _categories AS C ORDER BY C.c_id ASC";
$categorys=$sql->query($query);
foreach($categorys as $cat){
    $vars_array['categorys'][$cat['c_id']][$cat['c_lang']]=$cat;
}

$query="SELECT * FROM _languages";
$langs=$sql->query($query);

foreach($langs as $lang){
    $vars_array['langs'][$lang['l_id']]=$lang;
}

$PData->content(_lang('Категории'), 'title');
$PData->content($PData->getModTPL('admin/category_list', 'languages', @$vars_array));
