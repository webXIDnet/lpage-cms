<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
}
/**
 * Created for LPageCMS.
 * User: godex_000
 * Date: 06.07.2015
 * Time: 22:16
 */
//_dump($vars_array);
?>
<form method="post">
    <table class="table answer-table">
        <thead>
        <tr>
            <th class="head head0">ID</th>
            <th>Тип</th>
            <th class="head head0">Titile</th>
            <th class="head head0">URL</th>
            <th class="head head0"> Доп инфо</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $main_lang = $vars_array['main_lang'];
        $langs = $vars_array['langs'];
        foreach ($vars_array['categorys'] as $category) {
            if (isset($category[$main_lang])) {
                echo '<tr>';
                $category_display = $category[$main_lang];
            } else {
                echo '<tr style="background: #ff848c">';
                $category2 = $category;
                $category_display = array_shift($category2);
            }
            $avilable_lang = array();
            foreach ($category as $cat) {
                $avilable_lang[] = $langs[$cat['c_lang']]['l_title'];
            }
            echo "<td><input type='checkbox' name='del_id[" . $category_display['c_id'] . "]'</td>";
            echo "<td>" . $category_display['c_type'] . "</td>";
            echo '<td><a href=' . getURL('admin', 'languages/category_edit', 'id=' . $category_display['c_id']) . '>' . $category_display['c_title'] . '</a></td>';
            echo "<td>" . $category_display['c_url'] . "</td>";
            echo "<td>" . implode(', ', $avilable_lang) . "</td>";
//        echo "<td>".$category['c_id']."</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
    <button class="btn btn-danger" name="delete_category">delete</button>
</form>