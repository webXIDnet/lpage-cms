<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 02.07.2015
 * Time: 11:29
 */

if (!$vars_array['lang_exist']) {
    ?>
    <div class="container-fluid">
        <b><?php echo _lang('Устоновка в базу') ?></b>
        <hr/>
        <a href="<?php echo getURL('admin', 'languages/install'); ?>">
            <button class="button"><?php echo _lang('Установка') ?></button>
        </a>
    </div>
<?php
} else {
    ?>
    <div class="container-fluid">
        <b><?php echo _lang('Менеджер') ?></b>

        <form method="post">
            <table id="lang_table" class="table answer-table">
                <thead>
                <tr>
                    <th class="head head0"><?php echo _lang('ID') ?></th>
                    <th class="head head0"><?php echo _lang('Код') ?></th>
                    <th class="head head0"><?php echo _lang('Название') ?></th>
                    <th class="head head0"><?php echo _lang('Действия') ?></th>
                </tr>
                </thead>

                <?php
                $last = 1;
                foreach ($vars_array['langs'] as $lang) {
                    echo '<tr>';
                    echo '<td class="answer">';
                    if ($lang['l_id'] == $vars_array['main_lang_id']) {
                        echo _lang('Главный');
                    }else {
                        echo '<input title="выбрать" name="delete[' . $lang['l_id'] . ']" value="' . $lang['l_id'] . '" type="checkbox">';
                    }
                    echo '<input class="input-xlarge tits" name="lang[' . $lang['l_id'] . '][l_id]" value="' . $lang['l_id'] . '" type="hidden"></td>';
                    echo '<td class="answer"><input class="input-xlarge tits" name="lang[' . $lang['l_id'] . '][l_code]" value="' . $lang['l_code'] . '" type="text"></td>';
                    echo '<td class="answer"><input class="input-xlarge tits" name="lang[' . $lang['l_id'] . '][l_title]" value="' . $lang['l_title'] . '" type="text"></td>';
                    echo '<td class="answer">';
                    if ($lang['l_id'] != $vars_array['main_lang_id']) {
                        echo '<button type = "submit" class="button" title = "выбрать" name = "set_default" value = "' . $lang['l_id'] . '" type = "button" > По умолчанию </button >';
                    }
                    echo '</td>';
                    echo '</tr>';
                    $last = $lang['l_id'];
                }
                ?>
            </table>
            <div name="saveLangs" onclick="add_new_language()" style="cursor: pointer" class="button"
                 type="submit"><?php echo _lang('Добавить новый') ?></div>
            <input name="saveLangs" class="button" value="<?php echo _lang('Сохранить') ?>" type="submit">
            <input name="deleteLangs" class="button" value="<?php echo _lang('Удалить выбраные') ?>" type="submit">
        </form>
        <script type="text/javascript">
            var last = <?php echo $last ?>+1;
            function add_new_language() {
                jQuery('#lang_table').append('<tr><td class="answer"><input title="выбрать" value="' + last + '" type="checkbox"><input class="input-xlarge tits" name="lang[' + last + '][l_id]" value="" type="hidden"></td><td class="answer"><input class="input-xlarge tits" name="lang[' + last + '][l_code]" value="" type="text"></td><td class="answer"><input class="input-xlarge tits" name="lang[' + last + '][l_title]" value="" type="text"></td></tr>')
                last++;
            }
        </script>
    </div>
<?php
}