<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 02.07.2015
 * Time: 11:29
 */
?>
<div class="container-fluid">
    <div>
        <b><?php echo _lang('Удачные запросы') ?></b>

        <div class="alert alert-success" role="alert">
            <?php
            foreach ($vars_array['querys'] as $res) {
                echo "<p>" . $res . "</p>";
            }
            ?>
        </div>
        <?php
        if (isset($vars_array['errors'])) {
            ?>
            <b><?php echo _lang('Ошибка установки') ?></b>
            <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <?php
                foreach ($vars_array['errors'] as $res) {
                    echo "<p>" . $res . "</p>";
                }
                ?>
            </div>
        <?php
        } else {
            ?>
            <b><?php echo _lang('Установка прошла успешно') ?></b>
        <?php
        }
        ?>
    </div>

    <a href="<?php echo $vars_array['backup_file']; ?>">
        <button class="button"><?php echo _lang('Скачать файл бекап Базы Даных') ?></button>
    </a>
    <br/>
    <a href="<?php echo getURL('admin', 'languages'); ?>">
        <button class="button"><?php echo _lang('Назад') ?></button>
    </a>
</div>