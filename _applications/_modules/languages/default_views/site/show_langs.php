<?php
$langs = $vars_array['langs'];
$lang = $vars_array['lang'];
echo '<div class="lang">';       
foreach ($langs as $key => $value) {
    $url = str_replace("/" . $lang . "/", "/", PAGE_URL);
    $url = str_replace(HOMEPAGE, HOMEPAGE . $value['l_code'] . "/", $url);
    ?>
    <a style="text-transform: uppercase;" class=" <?= ($value['l_code'] == $lang ? ' active ' : '') ?> " href="<?= $url ?>"><?= $value['l_code'] ?></a>
    <?php
}
echo '</div>';