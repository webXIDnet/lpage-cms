<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 02.07.2015
 * Time: 11:29
 */

if (isset($_POST['set_default'])) {
    $def_id=$_POST['set_default'];
    $meta->add_update('md_lang',$def_id);
}
if (isset($_POST['saveLangs'])) {
    if (!empty($_POST)) {
        $langs = $_POST['lang'];
        foreach ($langs as $lang) {
            if (@$lang['l_id']) {
                $_POST['info'][] = 'set';
                $sql->query('UPDATE `_languages` SET `l_code`=\'' . $lang['l_code'] . '\', `l_title`=\'' . $lang['l_title'] . '\' WHERE  `l_id`=' . $lang['l_id'] . ';', 'query');
            } else {
                $sql->query('INSERT INTO `_languages` (`l_code`,`l_title`) VALUES(\'' . $lang['l_code'] . '\',\'' . $lang['l_title'] . '\')', 'query');
                $_POST['info'][] = 'not set';
            }
        }
    }
}

if (isset($_POST['deleteLangs'])) {
    if (!@is_null($_POST['delete']))
        foreach ($_POST['delete'] as $id) {
            $sql->query('DELETE FROM `_languages` WHERE  `l_id`=' . $id . ';', 'query');
        }
}

function mysql_table_seek($tablename)
{
    global $sql;

    // Try a select statement against the table
    // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
    try {
        $result = $sql->pdo->query("SELECT 1 FROM $tablename LIMIT 1");
    } catch (Exception $e) {
        // We got an exception == table not found
        return FALSE;
    }

    // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
    return $result !== FALSE;
}


$vars_array['lang_exist'] = mysql_table_seek('_languages');//существует ли таблица языки

if (!$vars_array['lang_exist']) {

} else {
    $vars_array['langs'] = $sql->query('SELECT * FROM `_languages`');
}
$vars_array['main_lang_id']=$meta->val('md_lang');

//$res=$sql->query("
//SELECT * FROM _tags;
//SELECT * FROM _posts_tags;", 'query',false,true);
//var_dump($res);
$PData->content(_lang('Менеджер языков'), 'title');
$PData->content($PData->getModTPL('admin/languages_editor', 'languages', @$vars_array));