<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 02.07.2015
 * Time: 11:29
 */
//create backup
$vars_array['backup_file']=$sql->create_backup();
$querys = array(
    'CREATE TABLE IF NOT EXISTS `_languages` (`l_id` TINYINT(4) NOT NULL AUTO_INCREMENT,	`l_code` VARCHAR(10) NOT NULL,	`l_title` VARCHAR(50) NOT NULL,	PRIMARY KEY (`l_id`),	UNIQUE INDEX `l_code` (`l_code`));',
    "INSERT IGNORE INTO `_languages` (`l_code`, `l_title`) VALUES ('ru', 'Руский');",
    "INSERT IGNORE INTO `_languages` (`l_code`, `l_title`) VALUES ('ua', 'Украинский');",
    "INSERT IGNORE INTO `_languages` (`l_code`, `l_title`) VALUES ('en', 'Английски');"
);
$lang_id=0;
$lang = $PData->GetSettings('lang');
switch ($lang) {
    case 'ru':
        $lang_id = 1;
        break;
    case 'ua':
        $lang_id = 2;
        break;
    case 'en':
        $lang_id = 3;
        break;
    default :
        $querys[]="INSERT IGNORE INTO `_languages` (`l_code`, `l_title`) VALUES ('$lang', 'Unknown Language');";
        $lang_id=4;
}
array_push($querys ,
"ALTER TABLE `_posts`
	ALTER `p_id` DROP DEFAULT,
	ALTER `p_lang` DROP DEFAULT;",
    "
ALTER TABLE `_posts`
	ADD COLUMN `id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `p_id` `p_id` INT(11) NOT NULL AFTER `id`,
	CHANGE COLUMN `p_lang` `p_lang` TINYINT(4) NOT NULL DEFAULT '$lang_id' AFTER `p_pablic_date`,
	DROP PRIMARY KEY,
	ADD UNIQUE INDEX `p_article` (`p_article`),
	ADD PRIMARY KEY (`id`);"
);

array_push($querys ,
    "UPDATE `_posts` as p SET p.p_lang=$lang_id"
);

array_push($querys ,
"ALTER TABLE `_categories`
	ALTER `c_id` DROP DEFAULT;","
ALTER TABLE `_categories`
	ADD COLUMN `id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `c_id` `c_id` INT(11) NOT NULL AFTER `id`,
	ADD COLUMN `c_lang` TINYINT NOT NULL DEFAULT '$lang_id' AFTER `c_type`,
	DROP PRIMARY KEY,
	ADD PRIMARY KEY (`id`);
"
);
array_push($querys ,
    "UPDATE `_categories` as c SET c.c_lang=$lang_id"
);

try {
    $vars_array['querys'][] = '';
    foreach ($querys as $query) {
        $sql->query($query, 'query');
        $vars_array['querys'][] = $query;
    }
} catch (Exception $e) {
    $vars_array['errors'][] = $e->getMessage() . "\n";
}

$meta->add_update('md_lang',$lang_id);

$PData->content(_lang('Установка'), 'title');
$PData->content($PData->getModTPL('admin/languages_install', 'languages', @$vars_array));