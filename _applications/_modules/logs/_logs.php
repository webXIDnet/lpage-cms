<?php
if (PAGE_TYPE == 'admin')
    _add_action('defined_modules', 'logs_init');
require_once 'logs.class.php';
function logs_init()
{
    global $sql;

    $sql->db_table_installer('Logs', '02.04.2015',
        array(
            '_profile_logs' => array(
                'pl_id'      => '`pl_id` int(11) NOT NULL AUTO_INCREMENT',
                'pl_type'    => '`pl_type` varchar(50) NOT NULL',
                'pl_message' => '`pl_message` varchar(100) NOT NULL',
                'pl_date'    => '`pl_date` varchar(50) NOT NULL',
                'pl_pf_id'   => '`pl_pf_id` int(11) NOT NULL',
                'pl_pr_id'   => '`pl_pr_id` int(11) NOT NULL',
                'CREATE'     => "`pl_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`pl_id`)"
            ),
            '_admin_logs'   => array(
                'al_id'      => '`al_id` int(11) NOT NULL AUTO_INCREMENT',
                'al_type'    => '`al_type` varchar(50) NOT NULL',
                'al_message' => '`al_message` varchar(100) NOT NULL',
                'al_date'    => '`al_date` varchar(50) NOT NULL',
                'CREATE'     => "`al_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`al_id`)"
            )
        )
    );
}