<?php

class logs {

    private $table;
    private $prefix;

    function __construct($table = 'profile', $prefix = 'pl_') {
        $this->table = '_'.$table.'_logs';
        $this->prefix = $prefix;
    }

    /**
     * @param string $type Type of a log record
     * @param string $message Log text
     * @param array $data array('column_name' => 'value') WITHOUT PREFIXES!
     */
    public function add_log($type, $message, $data = array()) {
        global $sql;
        $result = array_merge(array('type' => $type, 'message' => $message, 'date' => date('d.m.Y h:i:s')), $data);
//        print_r($result);
        $sql->insert($this->table, $result, $this->prefix);
    }
}