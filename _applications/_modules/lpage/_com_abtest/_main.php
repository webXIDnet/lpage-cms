<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента АБ тестинга
 *
***/

require_once __DIR__ . '/lp_abtest_class.php';
$abTest = new ABTest;

_add_action('defined_modules', 'com_ABTest_LPage_defined_modules');

function com_ABTest_LPage_defined_modules(){

	if(PAGE_TYPE=='admin'){
		global $sql;

		$sql->db_table_installer('LPage_ABTest','02.09.2014',
			array(
				'_lp_split_test' => array(
					'test_id' => "`test_id` int(11) NOT NULL AUTO_INCREMENT",
					'test_title' => "`test_title` varchar(255) NOT NULL",
					'test_date_start' => "`test_date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP",
					'test_date_end' => "`test_date_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'",
					'test_main_lp' => "`test_main_lp` int(11) NOT NULL",
					'test_lpages' => "`test_lpages` text NOT NULL",
					'CREATE' => "`test_id` int(11) NOT NULL AUTO_INCREMENT, `test_main_lp` int(11) NOT NULL, PRIMARY KEY (`test_id`), KEY `test_main_lp` (`test_main_lp`)"
				)
			)
		);
	}

	global	$abTest;
	$abTest->getAllTests();
}

_add_hook('LPage_init_start', 'com_ABTest_LPage_init_page');
function com_ABTest_LPage_init_page(){
	$lp = $_GET['lp'];
	global $abTest, $sql;

	if (isset($abTest->used_lp[$lp])){
		$test_id = $abTest->used_lp[$lp]['test_id'];
		$lp_id = $abTest->used_lp[$lp]['lpage_id'];

//		print_r($abTest);

		$stat_test = array();
		$st_hosts = '';
		if (isset($abTest->all[$test_id]['stat'])){
			$stat = $abTest->all[$test_id]['stat'];

			if(!empty($_COOKIE['lp_abtest_stat'])){
				$stat_test = json_decode( base64_decode( urldecode( $_COOKIE['lp_abtest_stat'] ) ) , TRUE);

				if(!is_array($stat_test)) $stat_test=array();
			}

			if (!isset($stat_test[$test_id])){
				$min_hosts = $stat[$lp_id];
				foreach($stat as $key=>$val){
					if ($min_hosts>$val){
						$lp_id = $key;
						$min_hosts = $val;
					}
				}
				$stat_test[$test_id] = $lp_id;

				$st_hosts = ', st_hosts=st_hosts+1';
			}else{
				$lp_id = $stat_test[$test_id];
			}


			if ($lp_id != $abTest->used_lp[$lp]['lpage_id']){
				$lp = $abTest->all[$test_id]['test_lpages_arr'][$lp_id]['article'];
			}

//			_add_filter('trig_OrderResiving', 'ref_addLPToOrder');
//			echo $lp;
		}

		$today = strtotime(date('Y-m-d'));
		$st_key =  $today .  $lp_id . $test_id;


		$sql->query("
			INSERT INTO `_statistic`
				(st_key, st_lp_id, st_date)
			VALUES
				('"._protect($st_key)."','"._protect($lp_id)."','"._protect($today)."')
			ON DUPLICATE KEY
			UPDATE st_hits=st_hits+1 {$st_hosts};",
			'query'
		);

		setcookie('lp_abtest_stat', urlencode( base64_encode( json_encode($stat_test) ) ), ($today+24*60*60*7) );

		if($lp!=$_GET['lp']){
			global $PData;
			$url = parse_url(PAGE_URL);
			$PData->redirect(getURL('lp',$lp,@$url['query']));
		}

		$_GET['lp']	= $lp;
	}
}

/*
function ref_addLPToOrder($result){

	return $result .'
			<input type="hidden" name="lp" value="'.$_GET['lp'].'">
			';

}
*/

_add_filter('LPage_admin_sidebar_menu', 'LPage_com_ABTest_admin_sidebar_menu', 11);
function LPage_com_ABTest_admin_sidebar_menu($result){
	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('А/Б тесты'), //ім"я пункта меню
			'url' => getURL('admin', 'ab-tests'), //посилання
			'active_pages' => array('ab-tests'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => getURL('admin','ab-test-add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(2,3,4,5,6,100), $menu);

	return $result . $menu;
}

_add_action('adminPanel_controller', 'LPage_ABTest_admin_controller');
function LPage_ABTest_admin_controller(){

	global 	$PData,
	$admin;

	switch(@$_GET['admin']){
		case'ab-tests':
			require_once __DIR__ . '/apps_admin/lp_abtests.php';
			return true;
			break;

		case'ab-test-add':
			require_once __DIR__ . '/apps_admin/lp_abtest_add.php';
			return true;
			break;
	}

}