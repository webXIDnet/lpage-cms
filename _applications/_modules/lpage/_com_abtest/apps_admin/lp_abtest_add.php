<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/
global $abTest;

if(isset($_POST['abtestNewSubmit']) && isset($_POST['abtest']['test_title']) && isset($_POST['abtest']['test_main_lp']) && isset($_POST['abtest']['test_lpages']) && count($_POST['abtest']['test_lpages'])>0){
	if ($abTest->add_test($_POST['abtest'])){
		$PData->content(_lang('Запись сохранена'),'message',TRUE);
		return true;
	}
}

$opts = '';
//$abTest->getAllUnusedLP();
foreach ($abTest->unused_lp as $lp_key=>$lp){
	$opts.='<option value="'.$lp_key.'"'.(isset($_POST['abtest']['test_main_lp']) && $_POST['abtest']['test_main_lp']==$lp_key?' selected':'').'>'.$lp['lp_title'].'</option>'."\n";
}

$selected_other_lp = '';
if (isset($_POST['abtest']['test_lpages']) && is_array($_POST['abtest']['test_lpages']) && count($_POST['abtest']['test_lpages'])>0){
	$selected_other_lp = implode(',',$_POST['abtest']['test_lpages']);
}elseif (isset($_POST['abtest']['test_lpages'])){
	$selected_other_lp = $_POST['abtest']['test_lpages'];
}
$PData->content(_lang('Создание А/Б теста'),'title');

$PData->content('
		<script type="text/javascript">
			var selected_otherLP = ['.$selected_other_lp.'];
		
			$(document).ready(function(){
				refreshOtherPages();
				$("#main_lp").change(refreshOtherPages);
			});
			
			function refreshOtherPages(){
				$("#other_lp").empty();
				$("#main_lp option").each(function(){
				    if (!$(this).attr("selected")) {
						var opt = $(this).clone();
						if ($.inArray( parseInt($(this).val()), selected_otherLP )>=0){ opt.attr("selected", "true"); }
				        $("#other_lp").append(opt);
				    }
				});
			}
		</script>
');

$PData->content('
	
	<form class="list" method="POST">

	<p>'._lang('Название теста').'</p>
          <input type="text" name="abtest[test_title]" value="'.@$_POST['abtest']['test_title'].'"/>
          <p>'._lang('Главная страница').'</p>
          <select name=""name="abtest[test_main_lp]" id="main_lp">'.$opts.'</select>
          <p>'._lang('Дополнительные  страницы').'</p>
          <select multiple="true" name="abtest[test_lpages][]" id="other_lp">
          </select>
          <input class="button" type="submit" value="'._lang('Сохранить').'" name="abtestNewSubmit"/>
	</form>
	
');