<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/
global $abTest;

if(isset($_POST['postEndSubmit']) && isset($_POST['_post'])){
	
	if ($abTest->finishTest($_POST['_post'])){
		$PData->content(_lang('Тесты завершены'),'message',TRUE);
	}

}

$text='';

$abTest->getOrdersInfo();

if (is_array($abTest->all)){
	foreach($abTest->all AS $tkey=>$tval){
		$konv = 0;
		if (isset($tval['orders'][$tval['test_main_lp']]) && isset($tval['stat'][$tval['test_main_lp']]) && $tval['orders'][$tval['test_main_lp']]>0 && $tval['stat'][$tval['test_main_lp']]>0){
			$konv = round($tval['orders'][$tval['test_main_lp']] / $tval['stat'][$tval['test_main_lp']] * 100, 2);
		}
		$text1 = '<tr style="font-weight:bold;">
						<td>'.$tval['test_main_lp_title'].'</td>
						<td>'.(isset($tval['stat'][$tval['test_main_lp']])?$tval['stat'][$tval['test_main_lp']]:'0').'</td>
						<td>'.(isset($tval['orders'][$tval['test_main_lp']])?$tval['orders'][$tval['test_main_lp']]:'0').'</td>
						<td>'.$konv.'%</td>
					</tr>';
		foreach ($tval['test_lpages_arr'] as $key1=>$val1){
			$konv = 0;
			if (isset($tval['orders'][$key1]) && isset($tval['stat'][$key1]) && $tval['orders'][$key1]>0 && $tval['stat'][$key1]>0){
				$konv = round($tval['orders'][$key1] / $tval['stat'][$key1] * 100, 2);
			}
			$text1 .= '<tr>
						<td>'.$val1['title'].'</td>
						<td>'.(isset($tval['stat'][$key1])?$tval['stat'][$key1]:'0').'</td>
						<td>'.(isset($tval['orders'][$key1])?$tval['orders'][$key1]:'0').'</td>
						<td>'.$konv.'%</td>
					</tr>';
		}
		
// 		if (!isset($_REQUEST['filter']) || !$_REQUEST['filter'] || (strpos($mkey,$_REQUEST['filter']) !== false))
		$text.='<tr>
			<td class="col0 tableList0">
				<input title="'._lang('выбрать').'" type="checkbox" name="_post['.$tkey.']"/>&nbsp;&nbsp;&nbsp;'.$tkey.'
				<div class="tableEdit_form">
					<p>'._lang('Статистика теста:').'</p>
					<hr/>
					<table width="100%">
						<tr>
							<th>'._lang('Страница').'</th>
							<th>'._lang('Показов').'</th>
							<th>'._lang('Заказов').'</th>
							<th>'._lang('Конверсия').'</th>
						</tr>
						'.$text1.'
					</table>
					<br><br>
				</div>
			</td>
			<td class="col2 tableList2 col">'.$tval['test_title'].'</td>
			<td class="col2 tableList2 col">'.$tval['test_date_start'].'</td>
			<td class="col2 tableList2 col">'.$tval['test_date_end'].'</td>
		</tr>';
		
	}
}
$PData->content('А/Б тесты','title');

$img_dir = HOMEPAGE . 'media/_admin/';

//todo fix add A/B tests
$PData->content('
	  <div class="add-new">
        <a href="'.getURL('admin','ab-test-add').'">
        <div class="item">
            <img src="'.$img_dir.'img/add-new.png" alt="add-new">
            <p>'._lang('Добавить А/Б тест').'</p>
        </div>
        </a>
      </div>
      <div class="line"></div>
<!--
	<form class="list">
		<table width="100%">
			<tr>
				<td>
					<label>'._lang('Поиск по странице').':</label> 
					<input type="hidden" name="admin" value="'.@$_GET['admin'].'"/>
					<input type="text" name="filter" value="'.@$_GET['filter'].'"/><br/>
					<input type="submit" class="btn" value="'._lang('Искать').'"/>
				</td>
			</tr>
		</table>
	</form>
	<hr/>
	//todo check test table body output
-->
		<form id="tableEdit" class="table" method="POST">
	    <div class="table-holder">
		    <table id="tableEdit" class="table">
		      <thead>
		        	<th class="table-10">&nbsp;</th>
					<th class="table-10">'._lang('Заголовок').'</th>
					<th class="table-10">'._lang('Дата начала').'</th>
					<th class="table-10">'._lang('Дата конца').'</th>
		      </thead>
		      <tbody>
		        	'.$text.'
		      </tbody>
		    </table>
		    <input class="button" type="submit" value="'._lang('Завершить').'" name="postEndSubmit">

	    </div>
		</form>
	
');