<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модель Тегов (таксономия)
 * 
 * v1.1 
 * 
***/

class ABTest{
	public $all;
	public $unused_lp;
	public $used_lp;
	
	public function getAllTests(){
		global 	$sql;

		$result = $sql->query("SELECT * FROM _lp_split_test");
		$tests = $result;
		
		$result = $sql->query("SELECT lp_id, lp_title, lp_article FROM _landing_pages");
		foreach ($result as $lp){
			$this->unused_lp[$lp['lp_id']] = $lp;
		}
		
		$stat = array();
		$result = $sql->query("SELECT *, RIGHT( st_key, LENGTH( st_key ) - LENGTH( st_date ) - LENGTH( st_lp_id ) ) AS test_id FROM _statistic WHERE st_key<>CONCAT(st_date, st_lp_id);");
		foreach ($result as $st){
			if (isset($stat[$st['test_id']][$st['st_lp_id']])){
				$stat[$st['test_id']][$st['st_lp_id']] = $stat[$st['test_id']][$st['st_lp_id']] + $st['st_hosts'];
			}else{
				$stat[$st['test_id']][$st['st_lp_id']] = $st['st_hosts'];
			}
		}
		
		$finishedTest = false;
		foreach($tests as $key=>$val){
			$finishedTest = $val['test_date_end']!='0000-00-00 00:00:00';
			
			$this->all[$val['test_id']] = $val;
			if (isset($this->unused_lp[$this->all[$val['test_id']]['test_main_lp']])){
				$this->all[$val['test_id']]['test_main_lp_title'] = $this->unused_lp[$val['test_main_lp']]['lp_title']; 
				$this->all[$val['test_id']]['test_main_lp_article'] = $this->unused_lp[$val['test_main_lp']]['lp_article'];
				if (isset($stat[$val['test_id']])){
					$this->all[$val['test_id']]['stat'] = $stat[$val['test_id']];
				}
				
				if (!isset($this->all[$val['test_id']]['stat'][$val['test_main_lp']])){
					$this->all[$val['test_id']]['stat'][$val['test_main_lp']] = 0;
				}
				
				$this->used_lp[$this->all[$val['test_id']]['test_main_lp_article']]['lpages'] = $val['test_lpages'];
				$this->used_lp[$this->all[$val['test_id']]['test_main_lp_article']]['test_id'] = $val['test_id'];
				$this->used_lp[$this->all[$val['test_id']]['test_main_lp_article']]['lpage_id'] = $val['test_main_lp'];
				
				if (!$finishedTest){
					unset($this->unused_lp[$val['test_main_lp']]);
				}
				
			}
			$this->all[$val['test_id']]['test_lpages_titles'] = array();
			foreach (explode(',', $val['test_lpages']) as $val1){
				if (isset($this->unused_lp[$val1])){
					$this->all[$val['test_id']]['test_lpages_arr'][$val1]['title'] = $this->unused_lp[$val1]['lp_title'];
					$this->all[$val['test_id']]['test_lpages_arr'][$val1]['article'] = $this->unused_lp[$val1]['lp_article'];
					
					if (!isset($this->all[$val['test_id']]['stat'][$val1])){
						$this->all[$val['test_id']]['stat'][$val1] = 0;
					}
					
					if (!$finishedTest){
						unset($this->unused_lp[$val1]);
					}
				}
			}
		}
	}
/*	
	public function getAllUnusedLP(){
		global 	$sql;
		
		$where = '0';
		foreach ($this->all as $test){
			if($test['test_lpages']){
				$where.=','.$test['test_lpages'];
			}
			$where.=','.$test['test_main_lp'];
		}
		
		$result = $sql->query("
				SELECT lp_id, lp_title, lp_article
				FROM _landing_pages
				WHERE lp_id NOT IN ("._protect($where).")
		");
		
		$this->unused_lp = $result;
	}
*/
	function add_test($param) {
		global $sql;
		if (isset($param['test_title']) && isset($param['test_main_lp']) && isset($param['test_lpages'])){
			if (isset($param['test_lpages']) && is_array($param['test_lpages'])){
				$param['test_lpages'] = implode(',',$param['test_lpages']);
			}
			$sql->insert('_lp_split_test', $param);
			return TRUE;
		}
		
		return FALSE;
	}
	
	function getOrdersInfo(){
		global $sql;
		
		$sql_text = "";
		if (is_array($this->all)){
			foreach($this->all AS $tkey=>$tval){
				if ($sql_text){ $sql.=" UNION ALL "; }
				$sql_text .= '(SELECT '._protect($tkey).' as test_id, count(o_id) as kol, o_lp_article FROM _orders WHERE o_status<>-1 AND o_date_create>='._protect(strtotime($tval['test_date_start']));
				if (isset($tval['test_date_end']) && $tval['test_date_end']!='0000-00-00 00:00:00'){
					$sql_text .= ' AND o_date_create<'._protect(strtotime($tval['test_date_end']));
				}
				$sql_text .= ' GROUP BY o_lp_article)';
			}
			
			//	echo $sql_text;
			
			$result = $sql->query( $sql_text );
			foreach ($result as $ord){
				if (getURL('lp',$this->all[$ord['test_id']]['test_main_lp_article'])==$ord['o_lp_article']){
					$this->all[$ord['test_id']]['orders'][$this->all[$ord['test_id']]['test_main_lp']] = $ord['kol'];
				}else{
					foreach ($this->all[$ord['test_id']]['test_lpages_arr'] as $key=>$val){
						if (getURL('lp',$val['article'])==$ord['o_lp_article']){
							$this->all[$ord['test_id']]['orders'][$key] = $ord['kol'];
							break;
						}
					}
				}
					
			}
		}
// 		print_r($this->all);
	}
	
	function finishTest($tests){
		global $sql;
		
		$where = '';
		if(is_array($tests)){
			$where = 'test_id IN ('._protect(implode(',',array_keys($tests))).')';
		}elseif($tests){
			$where = 'test_id='._protect($tests).')';
		}
		if ($where){
			$sql->query("UPDATE _lp_split_test SET test_date_end=NOW() WHERE ".$where);
		}
	}
}