<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модель Системи LPage.CMS
 *
 *
 *
***/

_add_action('defined_modules', 'LPage_defined_modules');

function LPage_defined_modules(){
	if(PAGE_TYPE=='admin'){
		global $sql;

		$sql->db_table_installer('LPage','06.09.2014',
			array(
				'_landing_pages' =>array(
					'lp_id' => "`lp_id` int(11) NOT NULL AUTO_INCREMENT",
					'lp_title' => "`lp_title` varchar(500) NOT NULL",
					'lp_article' => "`lp_article` varchar(500) NOT NULL",
					'lp_html' => "`lp_html` longtext NOT NULL",
					'lp_source' => "`lp_source` longtext NOT NULL",
					'lp_head' => "`lp_head` text NOT NULL",
					'lp_cat_id' => "`lp_cat_id` int(11) NOT NULL",
					'lp_tpl_id' => "`lp_tpl_id` int(11) NOT NULL",
					'lp_public_date' => "`lp_public_date` int(11) NOT NULL",
					'lp_lang' => "`lp_lang` varchar(50) NOT NULL",
					'CREATE' => "`lp_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`lp_id`)"
				),
				'_lp_templates' =>array(
					't_id' => "`t_id` int(11) NOT NULL AUTO_INCREMENT",
					't_title' => "`t_title` varchar(500) NOT NULL",
					't_article' => "`t_article` varchar(500) NOT NULL",
					't_head' => "`t_head` text NOT NULL",
					't_html' => "`t_html` longtext NOT NULL",
					'CREATE' => "`t_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`t_id`)"
				)
			)
		);
	}
}

/** Формує підпункти розділу "Landing Page" меню адмінки у правій колонці **/

if (PAGE_TYPE=='admin') {
	_add_filter('adminPanel_sidebar_menu', 'LPage_admin_sidebar_menu');
	_add_filter('LPage_admin_sidebar_menu', 'LPage_admin_sidebar_menu_submenu');
}

function LPage_admin_sidebar_menu($result){//модифікація меню адмін панелі в правій колонці

	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Landing Pages'), //ім"я пункта меню
			'url' => FALSE, //посилання
			'submenu_filter' => 'LPage_admin_sidebar_menu', //хук для підключення пунктів підменю
			'active_pages' => _run_filter('LPage_admin_sidebar_menu_active_items', array('lp', 'categories/?category_type=lp', 'templates')), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => TRUE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_item','admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . @$menu;
}

function LPage_admin_sidebar_menu_submenu($result){
	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Все Страницы'), //ім"я пункта меню
			'url' => getURL('admin', 'lp'), //посилання
			'active_pages' => array('lp'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => getURL('admin','lp_add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
		array(
			'title' => _lang('Категории'), //ім"я пункта меню
			'url' => getURL('admin','categories','category_type=lp'), //посилання
			'active_pages' => array('categories'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
		array(
			'title' => _lang('Шаблоны'), //ім"я пункта меню
			'url' => getURL('admin', 'templates'), //посилання
			'active_pages' => array('templates'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . $menu;
}

/** Роутер Адмінки **/

_add_hook('adminPanel_controller', 'LPage_admin_controller');

function LPage_admin_controller(){

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL;

	$is_LPage = FALSE;

	switch(@$_GET['admin']){

		/** Landing Pages **/

		case'lp':

			$is_LPage = TRUE;
			require_once __DIR__ . '/apps_admin/lpg_lp.php';

		break;

		case'lp_add':

			$is_LPage = TRUE;
			require_once __DIR__ . '/apps_admin/landing_pages/lpg_lp_add.php';

		break;

		case'lp_edit':

			$is_LPage = TRUE;
			require_once __DIR__ . '/apps_admin/landing_pages/lpg_lp_edit.php';

		break;

		case'lp_edit_html':

			$is_LPage = TRUE;
			require_once __DIR__ . '/apps_admin/landing_pages/lpg_lp_edit_html.php';

		break;

		/** Landing Page Templates **/

		case'templates':

			$is_LPage = TRUE;
			require_once __DIR__ . '/apps_admin/lpg_templates.php';

		break;

		case'tpl_edit':

			$is_LPage = TRUE;
			require_once __DIR__ . '/apps_admin/templates/lpg_tpl_edit.php';

		break;

		case'tpl_preview':

			$is_LPage = TRUE;
			require_once __DIR__ . '/apps_admin/templates/lpg_tpl_preview.php';

		break;

	}

	if($is_LPage){
		return TRUE;
	}
}

require_once __DIR__ . '/lpg_trigers.php';

if(isset($_GET['lp']))
	_add_hook('defined_all_processes', 'LPage_controller');

function LPage_controller(){

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL;

	require_once __DIR__ . '/apps_site/_vars.php';
	require_once __DIR__ . '/apps_site/html_lp.php';

}

_add_hook('getURL_rule', 'LPage_getURL');

function LPage_getURL($atr){

	switch($atr['key']){
		case'lp':

			if(!empty($atr['get']))$atr['get'] = '?'.$atr['get'];

			return HOMEPAGE.$atr['val'].'.html'.$atr['get'];
		break;

		default:
			return FALSE;

	}
}

_add_action('ajax_init', 'LPage_ajax_init');

function LPage_ajax_init(){

	global 	$sql,
			$sqlTPL;

	if(isset($_GET['SaveTPL'])){

	 	if(!empty($_POST['tpl'])){

			$sqlTPL->getCategories('lp');

			global $list;

			$cval = $sql->query("
				SELECT
					t_article,
					lp_cat_id,lp_article
				FROM _landing_pages INNER JOIN _lp_templates
				ON lp_tpl_id = t_id
				WHERE lp_id = '"._protect($_GET['SaveTPL'])."'
			",'value');

			$sql->update('_landing_pages',"lp_id='"._protect($_GET['SaveTPL'])."'",
				array(
			//		'lp_html' => trim(backShortCodes(
			//			$_POST['text'],
		//				HOMEPAGE.'images/'.$cval['t_article'].'/',
		//				getURL('lp', $cval['lp_article']),
		//				getURL('ajax','sendOrderForm','id='.$_GET['SaveTPL'])
		//			)),
					'lp_source' => trim(backShortCodes($_POST['tpl'],
						HOMEPAGE.'images/'.$cval['t_article'].'/',
						getURL('lp', $cval['lp_article']),
						getURL('ajax','sendOrderForm','id='.$_GET['SaveTPL'])
					))
				)
			);

		}elseif(!empty($_POST['text'])){
			$sqlTPL->getCategories('lp');

			global $list;

			$cval = $sql->query("
				SELECT
					t_article,
					lp_cat_id,lp_article
				FROM _landing_pages INNER JOIN _lp_templates
				ON lp_tpl_id = t_id
				WHERE lp_id = '"._protect($_GET['SaveTPL'])."'
			",'value');

			$sql->update('_landing_pages',"lp_id='"._protect($_GET['SaveTPL'])."'",
				array(
					'lp_html' => trim(backShortCodes(
						$_POST['text'],
						HOMEPAGE.'images/'.$cval['t_article'].'/',
						getURL('lp', $cval['lp_article']),
						getURL('ajax','sendOrderForm','id='.$_GET['SaveTPL'])
					)),
	//				'lp_source' => trim(backShortCodes($_POST['tpl'],
	//					HOMEPAGE.'images/'.$cval['t_article'].'/',
	//					getURL('ajax','sendOrderForm','id='.$_GET['SaveTPL'])
	//				))
				)
			);



			echo '
				<h1>'._lang('Страница сохранена').'</h1>
				<div>
					<a style="display: table;margin:0 auto 20px;" lpgAttr="lpgGreenButton" href="'.getURL('admin','lp_edit','id='.$_GET['SaveTPL']).'">'._lang('Продолжить редакотрование').'</a>
					<a style="display: table;margin:0 auto;" href="'.getURL('lp', $cval['lp_article']).'">'._lang('Посмотреть страницу').'</a>
				</div>

			';
		}else{
			echo '
				<h1>'._lang('Отправлен пустой запрос. Обратитесь к администратору').'</h1>';
		}
	}
}

_add_filter('LPage_lp_head', '_shortCode');
_add_filter('LPage_lp_html', '_shortCode');
_add_filter('LPage_lp_article', '_shortCode');
_add_filter('LPage_lp_lang', '_shortCode');