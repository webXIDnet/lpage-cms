<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
$img_dir = HOMEPAGE . 'media/_admin/';
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/

$sqlTPL->getCategories('lp');

global $list;

if(isset($_POST['lpNewsubmit'])){

	if(!empty($_POST['lpAdd']['title'])){

		$mas=$_POST['lpAdd'];

		if(empty($mas['article'])){

			$url=$mas['article'] = _textToURL($mas['title']);
			$i=1;

			$cval = $sql->query("
				SELECT lp_id
				FROM _landing_pages
				WHERE lp_article='"._protect($url)."' AND lp_cat_id>0
			",'value');

			while($cval){

				$mas['article']=$url.'-'.$i;
				$i++;

				$cval = $sql->query("
					SELECT lp_id
					FROM _landing_pages
					WHERE lp_article='"._protect($mas['article'])."' AND lp_cat_id>0
				",'value');

			}

		}

		$mas['public_date']=time();

		$cval = $sql->query("
			SELECT t_head,t_html
			FROM _lp_templates
			WHERE t_id='"._protect($_POST['lpAdd']['tpl_id'])."'
		",'value');

		$mas['head'] = $cval['t_head'];
		$mas['source'] = $mas['html'] = $cval['t_html'];

		$mas = _run_filter('LPage_page_insert_array',$mas);

		$id = $sql->insert('_landing_pages',$mas,'lp_');


		$PData->content(_lang('Запись сохранена').'. <a target="_blank" href="'.getURL('lp',$mas['article']).'">'._lang('Посмотреть страницу').'</a>','message',TRUE);

		unset($mas);
		unset($_POST['lpAdd']);

	}else{

		$PData->content('Нужно указать Заголовок','message');

	}
}

$result = $sql->query("
	SELECT *
	FROM _lp_templates
");

$text='';

foreach($result AS $cval){

	$check = '';
	if(empty($text)) $check = 'checked="checked"';

	$text .= '
	<label href="'.getURL('admin','tpl_edit','id='.$cval['t_id']).'"
	class="add-new-radio" >
		<img src="'.HOMEPAGE.'media/_'.$cval['t_article'].'/_screen.png"/>
		<p>'.$cval['t_title'].'</p>
		<input '.$check.' type="radio" name="lpAdd[tpl_id]" value="'.$cval['t_id'].'"/>
	</label>
	';

}

$lpAdd = $PData->_POST('lpAdd');

if (!isset($lpAdd['title'])) {
	$lpAdd['title'] = '';
}

if (!isset($lpAdd['article'])) {
	$lpAdd['article'] = '';
}



if (!isset($lpAdd['cat_id'])) {
	$categories_select_list = $lpAdd['cat_id'] = '';
} elseif (isset($list->category['lp'])) {
	$categories_select_list = $contTPL->catHierarchTree($list->category['lp'], 0, $lpAdd['cat_id']);
}

$PData->content('Создание Landing Page','title');

$PData->content('
	<form class="list" method="POST">
		<p>'._lang('Заголовок').'</p>
		<input type="text" name="lpAdd[title]" value="'.$lpAdd['title'].'"/>

		<p>'._lang('Текстовая ссылка').'</p>
		<input type="text"  name="lpAdd[article]" value="'.$lpAdd['article'].'"/>


		<p>'._lang('Категория').'</p>
		<select name="lpAdd[cat_id]">
			'.$categories_select_list.'
		</select>
	<!-- Filter: LPage_Add_Page -->
	'._run_filter('LPage_Add_Page').'
	<!-- // Filter: LPage_Add_Page -->

	<h2>'._lang('Шаблоны').'</h2>

'.$text.'
	<div class="clear"></div>
	<br/><br/>
	<input class="button" type="submit" value="'._lang('Сохранить').'" name="lpNewsubmit"/>
</form>
');