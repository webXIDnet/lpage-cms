<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/

if(!isset($_GET['id'])) $PData->redirect(getURL('admin'),'403');

$sqlTPL->getCategories('lp');

global 	$list,
		$lp;

$lp = $sql->query("
	SELECT *
	FROM _landing_pages INNER JOIN _lp_templates
	ON lp_tpl_id = t_id
	WHERE lp_id = '"._protect(@$_GET['id'])."'
",'value');

if (isset($_POST['update']) && !empty($_POST['update'])) {
    $data = array(
        'html' => _protect($_POST['update']),
        'source' => _protect($_POST['update'])
    );
    $sql->update('_landing_pages','lp_id='._protect(@$_GET['id']),$data,'lp_');
    exit;
}

if(isset($lp) && is_array($lp)){ //$PData->redirect(getURL('admin'),'403');

	echo '<!DOCTYPE html>
<html>
<head>

'._shortCode($lp['lp_head']).'
<script type="text/javascript">
    if(!window.jQuery){
        document.write("<scr"+"ipt src=\''.SITE_FOLDER.'plugins/lp-edit/js/jquery-1.11.2.min.js\'></scr"+"ipt>");
    }
    var HOMEPAGE = "'.SITE_FOLDER.'";
</script>
<!-- fonts -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- css -->
<link href="'.SITE_FOLDER.'plugins/lp-edit/css/bootstrap.css" rel="stylesheet">
<!--<link href="'.SITE_FOLDER.'plugins/lp-edit/css/style.css" rel="stylesheet">-->


<!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Подключаем библиотеки редактора на старнице ред. -->

<!-- css -->
<link rel="stylesheet" href="'.SITE_FOLDER.'plugins/code-mirror/lib/codemirror.css">
<link rel="stylesheet" href="'.SITE_FOLDER.'plugins/lp-edit/js/sweetalert/sweetalert.css">

<link rel="stylesheet" href="'.SITE_FOLDER.'plugins/lp-edit/css/editor.css">

<!-- js -->

<script src="'.SITE_FOLDER.'plugins/lp-edit/js/bootstrap.min.js"></script>

<script src="'.SITE_FOLDER.'plugins/code-mirror/lib/codemirror.js"></script>
<script src="'.SITE_FOLDER.'plugins/code-mirror/mode/javascript/javascript.js"></script>
<script src="'.SITE_FOLDER.'plugins/code-mirror/mode/xml/xml.js"></script>
<script src="'.SITE_FOLDER.'plugins/code-mirror/mode/javascript/javascript.js"></script>
<script src="'.SITE_FOLDER.'plugins/code-mirror/mode/css/css.js"></script>
<script src="'.SITE_FOLDER.'plugins/code-mirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="'.SITE_FOLDER.'plugins/code-mirror/addon/edit/matchbrackets.js"></script>

<script src="'.SITE_FOLDER.'plugins/lp-edit/js/zclip.js" type="text/javascript"></script>
<script src="'.SITE_FOLDER.'plugins/lp-edit/js/sweetalert/sweetalert-dev.js"></script>

<script src="'.SITE_FOLDER.'plugins/lp-edit/js/editor.js"></script>

<!-- Подключаем библиотеки редактора на старнице ред. -->

</head>
<body>
	'._shortCode($lp['lp_source']).'
	<div start-control="true"></div>
<div id="edit-holder" contenteditable="false">

   <div id="edit-panel">
      <a href="#" id="code" title="'._lang('Показать код').'" data-toggle="modal" data-target="#show-code"><i class="fa fa-code"></i></a>
      <a href="#" id="file-manager" title="'._lang('Файл-менеджер').'" onclick="openKCFinder(this)"><i class="fa fa-download"></i></a>
      <a href="#" id="save" title="'._lang('Сохранить изменения').'"><i class="fa fa-floppy-o"></i></a>
      <a href="#" id="go-back" title="'._lang('Отменить правки').'"><i class="fa fa-undo"></i></a>
   </div>

   <a href="#" id="hide-me" title="Свернуть"></a>

   <div id="file-url"></div>

   <div id="file-control">
      <a href="#" id="url-copy" title="'._lang('Скопировать в буфер').'"><i class="fa fa-files-o"></i></a>
      <a href="#" id="url-close" title="'._lang('Закрыть').'" onclick="closeControl();"><i class="fa fa-times"></i></a>
   </div>

   <div class="modal fade" id="show-code" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
         <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><i class="fa fa-code"></i></h4>
            </div>
            <div class="modal-body">
               <textarea name="code" id="code-editor"></textarea>
            </div>
            <div class="modal-footer">
               <a href="#" id="editor-save">'._lang('Сохранить изменения').'</a>
               <a href="#" data-dismiss="modal">'._lang('Закрыть').'</a>
            </div>
         </div>
      </div>
   </div>

</div>
<div end-control="true"></div>
</body>
</html>';

	$PData->phpExit();

}else{
	$PData->content('Страница не найдена','title');
	$PData->content('Возможно вы перешли по старой ссылке!
	<br><br>
	Поскольку шаблона Посадки, которую вы питаетесь отредактировать, нет в базе данных!');
	$admin->getAdminTheme('small');

}