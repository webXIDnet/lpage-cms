<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/


$sqlTPL->getCategories('lp');

global $list;


if(isset($_POST['lpEditsubmit'])){

	if(!empty($_POST['lpEdit']['title'])){

		$mas=$_POST['lpEdit'];

		if(empty($mas['article'])){

			$url=$mas['article'] = _textToURL($mas['title']);
			$i=1;

			$cval = $sql->query("
				SELECT lp_id
				FROM _landing_pages
				WHERE lp_article='"._protect($url)."' AND lp_cat_id>0
			",'value');

			while($cval){

				$mas['article']=$url.'-'.$i;
				$i++;

				$cval = $sql->query("
					SELECT lp_id
					FROM _landing_pages
					WHERE lp_article='"._protect($url)."' AND lp_cat_id>0
				",'value');

			}

		}

		if(isset($mas['source']))
			$mas['source'] = valInTextarea($mas['source']);

		if(isset($mas['head']))
			$mas['head'] = valInTextarea($mas['head']);

		$mas['public_date']=time();

		$mas = _run_filter('LPage_page_update_array',$mas);

		$sql->update('_landing_pages',"lp_id='"._protect($_GET['id'])."'",$mas,'lp_');


		$PData->content(_lang('Запись сохранена').'. <a target="_blank" href="'.getURL('lp',$mas['article']).'">'._lang('Посмотреть страницу').'</a>','message',TRUE);

		unset($mas);
		unset($_POST['lpEdit']);

	}else{

		$PData->content('Нужно указать Заголовок','message');

	}
}

$cval = $sql->query("
	SELECT *
	FROM _landing_pages LEFT JOIN _lp_templates
	ON lp_tpl_id = t_id
	WHERE lp_id = '"._protect(@$_GET['id'])."'
",'value');

$text='';

if(isset($cval) && is_array($cval)){

	if (empty($cval['lp_cat_id'])) {
		$cval['lp_cat_id'] = 0;
	}

	$categories_select_list = '';
	if (isset($list->category['lp'])) {
		$categories_select_list = $contTPL->catHierarchTree(@$list->category['lp'],0,$cval['lp_cat_id']);
	}

	$text .= '
	<div style="float: right;width:322px;">
		<!-- Filter: LPage_Edit_Page_2 -->
		'._run_filter('LPage_Edit_Page_2','',$cval).'
	</div>
	<p>
		<label>'._lang('Заголовок').'</label>
		<input type="text" class="input-xlarge" name="lpEdit[title]" value="'.$cval['lp_title'].'"/>
	</p>

	<p>
		<label>'._lang('Текстовая ссылка').'</label>
		<input type="text" class="input-xlarge" name="lpEdit[article]" value="'.$cval['lp_article'].'"/>
	</p>
	<p>
		<label>'._lang('Категория').'</label>
		<select name="lpEdit[cat_id]">
			<option value="">- '._lang('Без категории').' -</option>
			'.$categories_select_list.'
		</select>
	</p>
	<!-- Filter: LPage_Edit_Page_1 -->
	'._run_filter('LPage_Edit_Page_1','',$cval).'

	<p>
		<label>'._lang('head').'</label>
		'.$admin->getWIZIWIG('lpEdit[head]" style="width: 90%; height: 140px;',_unprotect($cval['lp_head'],'textarea'),'head','code').'
	</p>
	<p>
		<label>'._lang('Исходный html-код шаблона').'</label>
		'.$admin->getWIZIWIG('lpEdit[source]" style="width: 90%; height: 300px;',_unprotect($cval['lp_source'],'textarea'),'source','code').'
	</p>
	<!-- Filter: LPage_Edit_Page_3 -->
	'._run_filter('LPage_Edit_Page_3','',$cval).'
	';


	$PData->content('Редактирование HTML-кода Страницы','title');

	$PData->content('
		<form class="list" method="POST">



	'.$text.'
		<div class="clear"></div>
		<br/><br/>
		<input class="button btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="lpEditsubmit"/>
	</form>
	','main',FALSE,FALSE);

}else{
	UCode::redirect('','404');
}