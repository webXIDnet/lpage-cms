<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
$img_dir = SITE_FOLDER . 'media/_admin/';
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
 ***/

if (isset($_POST['postDeleteSubmit']) && isset($_POST['_post'])) {

    $where = '';

    foreach ($_POST['_post'] AS $key => $val) {

        if (!empty($where)) $where .= ' OR ';

        $where .= "lp_id='" . _protect($key) . "'";

    }

    $sql->query("DELETE FROM _landing_pages WHERE $where;", 'query');

    $PData->content('Запись удалена', 'message', TRUE);

} elseif (isset($_POST['postsCopySubmit']) && isset($_POST['_post'])) {

    $where = '';

    foreach ($_POST['_post'] AS $key => $val) {

        $cval = $sql->query("
			SELECT *
			FROM _landing_pages
			WHERE lp_id='" . _protect($key) . "'
		", 'value');

        if (isset($cval) && is_array($cval)) {

            unset($cval['lp_id']);

            $cval['lp_article'] .= '-copy';
            $cval['lp_title'] .= '-copy';

            $cval1 = $sql->query("
				SELECT lp_id
				FROM _landing_pages
				WHERE lp_article='" . _protect($cval['lp_article']) . "' AND lp_cat_id=" . _protect($cval['lp_cat_id']) . "
			", 'value');

            while ($cval1) {

                $cval['lp_article'] .= '-copy';
                $cval['lp_title'] .= '-copy';

                $cval1 = $sql->query("
					SELECT lp_id
					FROM _landing_pages
					WHERE lp_article='" . _protect($cval['lp_article']) . "' AND lp_cat_id=" . _protect($cval['lp_cat_id']) . "
				", 'value');

            }


            $cval['lp_public_date'] = time();

            $sql->insert('_landing_pages', $cval);

            $PData->content('Запись  скопирована', 'message', TRUE);

        } else {
            $PData->content('Невозможно скопировать запись', 'message', FALSE);
        }

    }
}

$w = '';
$where = "
	LEFT JOIN _lp_templates
	ON lp_tpl_id = t_id
	";

if (!empty($_GET['filter'])) {
    $w = "lp_title LIKE '%" . _protect($_GET['filter']) . "%'";
}

if (!empty($_GET['filter_cat'])) {
    if (!empty($w)) $w .= ' AND ';
    $w = $w . " lp_cat_id = '" . _protect($_GET['filter_cat']) . "'";
}

if (!empty($w)) {
    $where = "WHERE " . $w;
}

$where = _run_filter('LPage_QUERY_WHERE', $where); //модифікація запиту в базу

$nav = _pagination('_landing_pages', 'lp_id', $where);

$result = $sql->query("
	SELECT *
	FROM _landing_pages
	{$where}
	" . @$nav['limit'] . "
");

$sqlTPL->getCategories('lp');

global $list;

$text = '';
$i = 0;

foreach ($result AS $cval) {
    $i++;
    $text .= '<tr>
		<td >
			<input title="' . _lang('выбрать') . '" type="checkbox"
			name="_post[' . $cval['lp_id'] . ']"/>
			 &nbsp;&nbsp;
			 <a target="_blank" href="' . getURL('lp', $cval['lp_article']) . '"
			 title="' . _lang('Перейти на страницу') . '">' . $cval['lp_id'] . '</a>
		</td>
		<td >
			<a target="_blank" title="Редактировать" href="' .
        getURL('admin', 'lp_edit', 'id=' . $cval['lp_id']) . '">
				' . $cval['lp_title'] . '</a>
		</td>
		<td >
			';

		if(isset ($list->category_index[$cval['lp_cat_id']]['c_title']) ){
			$text .= $list->category_index[$cval['lp_cat_id']]['c_title'];
		}

		$text .= '
		</td>
		<td style="text-align:center;">
			<a title="Редактировать HTML" href="' . getURL('admin', 'lp_edit_html', 'id=' . $cval['lp_id']) . '">
				' . _lang('Редактировать') . '</a>
		</td>
        <td >
        	<!-- Filter: LPage_table_additional_data -->
			' . _run_filter('LPage_table_additional_data', '', $cval) . '
			<!-- // Filter: LPage_table_additional_data -->
		</td>
	</tr>';
}

$PData->content('Landing Pages', 'title');

$categories_select_list = '';

if (isset($list->category['lp'])) {
	$categories_select_list = $contTPL->catHierarchTree($list->category['lp'], 0, $PData->_GET('filter_cat'));
}

$PData->content('
    <div class="add-new">

        <a href="' . getURL('admin', 'lp_add') . '">
        <div class="item">
                <img src="' . $img_dir . 'img/add-new.png" alt="add-new">
                <p>' . _lang('Добавить cтраницу') . '</p>

        </div>
        </a>
        <a href="' . getURL('admin', 'categories', 'category_type=lp') . '">
		<div class="item">
                <img src="' . $img_dir . 'img/add-cat.png" alt="add-cat">
                <p>' . _lang('Добавить категорию') . '</p>
        </div>
        </a>
	</div>
	<div class="line"></div>
	 <form method="POST">
		<div class="row">
            <div class="col-lg-6 col-md-6">
                <p>' . _lang('Поиск по заголовку') . ':</p>
					<input type="hidden" name="admin" value="' . $PData->_GET('admin') . '"/>
					<input type="text" name="filter" value="' . $PData->_GET('filter') . '"/><br/>
					<input type="submit" class="button" value="' . _lang('Искать') . '"/>
				</div>
				<div class="col-lg-6 col-md-6">
                <p>' . _lang('Категории') . ':</p>
					<select name="filter_cat">
						<option value="">- ' . _lang('Категория не выбрана') . ' -</option>
						' . $categories_select_list . '
					</select>
				</div>
				<!-- Filter: LPage_filter_form -->
				' . _run_filter('LPage_filter_form') . '
				<!-- // Filter: LPage_filter_form -->
			</div>
	</form>

	<div class="line"></div>

	<form method="POST">
        <div class="table-holder">
          <table class="table">
          <thead>
		        	<th class="numer">' . _lang('ID') . '</th>
					<th>' . _lang('Заголовок') . '</th>
					<th>' . _lang('Категория') . '</th>
					<th>HTML ' . _lang('Шаблон') . '</th>
					<th>' . _lang('Доп. данные') . '</th>
		      </thead>
		      <tbody>
		        	' . $text . '
		      </tbody>
		    </table>
		    <input type="submit" value="' . _lang('Копировать') . '" class="button" name="postsCopySubmit">
        <input type="submit" value="' . _lang('Удалить') . '" class="button" name="postDeleteSubmit">
		</form>
	</div>

' . $nav['html']);