<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
 ***/

if (isset($_POST['theme_del'])) {

    if (is_array($_POST['theme_del'])) {

        foreach ($_POST['theme_del'] AS $key => $val) {

            $cval = $sql->query("
				SELECT t_title
				FROM _lp_templates
				WHERE t_id = '" . _protect($key) . "'
			", 'value');
            $PData->content('
				<form method="POST">
				
					' . _lang('После удаления шаблона, будут удалены все его файлы, и страници, которые используют данный шаблон будут <b>отображаться некоректно</b>!<br/>Вы уверены, что хотите удалить шаблон') . ' "<b>' . $cval['t_title'] . '</b>"?
					<button href="">' . _lang('Нет') . '</button>
				</form>
				<form method="POST">
					<input class="theme_delete" name="t_del[' . $key . ']" type="submit" value="' . _lang('Удалить') . '">
				</form>
			', 'message', TRUE);
        }

    }

} elseif (isset($_POST['t_del'])) {

    if (is_array($_POST['t_del'])) {

        foreach ($_POST['t_del'] AS $key => $val) {

            $cval = $sql->query("
				SELECT t_article
				FROM _lp_templates
				WHERE t_id = '" . _protect($key) . "'
			", 'value');

            if (!empty($cval['t_article'])) {
                _unDir(CODE_FOLDER . '../media/_' . $cval['t_article'] . '/');
            }

            $cval = $sql->query("DELETE FROM _lp_templates WHERE t_id = '" . _protect($key) . "'", 'query');
            $PData->content('Запись удалена', 'message', TRUE);
        }

    }

} elseif (isset($_POST['templateSubmit'])) {

    if (isset($_FILES['attach']["tmp_name"]) && is_uploaded_file($_FILES['attach']["tmp_name"])) {

        if (empty($_POST['title'])) {

            $_POST['title'] = substr($_FILES['attach']['name'], 0, -4);

            $article = _textToURL($_POST['title']);

        } else {

            $article = _textToURL(substr($_FILES['attach']['name'], 0, -4));

        }

        $i = 0;

        while (is_dir(CODE_FOLDER . '../media/_' . $article . '/')) {

            $article = _textToURL(substr($_FILES['attach']['name'], 0, -4)) . '_' . $i;
            $i++;

        }

        $tpl_folder = CODE_FOLDER . '../media/_' . $article . '/';

        mkdir($tpl_folder);

        $zip = new ZipArchive;
        $zip->open($_FILES['attach']["tmp_name"]);
        $zip->extractTo($tpl_folder);
        $zip->close();

        unlink($_FILES['attach']["tmp_name"]);

        $head = file_get_contents($tpl_folder . 'head.tpl');
        $html = file_get_contents($tpl_folder . 'html.tpl');

        $coding = mb_detect_encoding($head);
        $head = mb_convert_encoding($head, "UTF-8", $coding);

        $coding = mb_detect_encoding($html);
        $html = mb_convert_encoding($html, "UTF-8", $coding);

        $sql->insert('_lp_templates', array(
            'title' => trim($_POST['title']),
            'article' => $article,
            'html' => $html,
            'head' => $head
        ), 't_');

        $PData->content('Шаблон добавлен', 'message', TRUE);

    } else {

        $PData->content('Файл не выбран', 'message', FALSE);

    }

}

$result = $sql->query("
	SELECT *
	FROM _lp_templates
");

$text = '';

foreach ($result AS $cval) {

    $text .= '
	<form method="POST" class="boxPic boxItem" style="background-image:none;">
	<div class="item">
	<a href="' . getURL('admin', 'tpl_edit', 'id=' . $cval['t_id']) . '">
            <img src="' . HOMEPAGE . 'media/_' . $cval['t_article'] . '/_screen.png" alt="name">
            <p>' . $cval['t_title'] . '</p>
          </a>
          <div class="controll">
            <a href="' . getURL('admin', 'tpl_preview', 'id=' . $cval['t_id']) . '" class="button">' . _lang('Превью') . '</a>
           <!-- //todo  <a href="#" class="button button-red">\' . _lang(\'Удалить\') . \'</a>-->
            <input class="button button-red" name="theme_del[' . $cval['t_id'] . ']" type="submit" value="' . _lang('Удалить') . '">
          </div>
	</form>
	';

}

$PData->content('Шаблоны', 'title');

$PData->content('
	<p><b>' . _lang('Загрузить шаблон') . '</b></p>
	
	<form method="POST" enctype="multipart/form-data">
		<input name="title" placeholder="' . _lang('Название шаблона') . '" type="text"/>
		<input name="attach" type="file" accept="application/x-compressed, application/x-zip-compressed"/>
		<span class="grey">.zip</span>
		<br>
		<input type="submit" class="button" name="templateSubmit" value="' . _lang('Отправить') . '"/>
		<p class="grey">
			! - ' . _lang('файлы шаблона должны быть сохранены в кодировке UTF-8') . '
		</p>
	</form>
	<br/>
	<hr/>
	<p><b>' . _lang('Шаблоны') . '</b></p>

	<div class="templates-holder">
' . $text.'
    </div>');