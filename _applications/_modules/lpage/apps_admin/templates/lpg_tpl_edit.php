<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Додавання постів
 * 
***/

if(isset($_POST['tplEditsubmit'])){
	
	if(empty($_POST['tplEdit']['title'])){
	
		unset($_POST['tplEdit']['title']);
		
	}
	
	$sql->update('_lp_templates',"t_id='"._protect($_GET['id'])."'",$_POST['tplEdit'],'t_');
	
}

$cval = $sql->query("
	SELECT 
		*
	FROM _lp_templates
	WHERE t_id='"._protect(@$_GET['id'])."'
",'value');

if($cval && is_array($cval)){
	
	$PData->content( 'Редактирование шаблона', 'title' );
	$PData->content('
		<form class="list" method="POST">
			
			<div style="float: right;width:322px;">
				
			</div>
			<div>
				<p>
					<label>'._lang('Заголовок').'</label> 
					<input type="text" class="input-xlarge" name="tplEdit[title]" value="'.$cval['t_title'].'"/>
				</p>
				<p>
					<label>'._lang('head').'</label>
					'.$admin->getWIZIWIG('tplEdit[head]" style="width: 90%; height: 300px;',_unprotect($cval['t_head']),'head','code').'
				</p>
			</div>
			<div class="clear"></div>
			<p>
				<label>'._lang('html').'</label>
				'.$admin->getWIZIWIG('tplEdit[html]" style="width: 90%; height: 300px;',_unprotect($cval['t_html']),'html','code').'
				
			</p>
			<input class="btn btn-primary" type="submit" value="Сохранить" name="tplEditsubmit"/>
		</form>
	','main',FALSE,FALSE);
	
}else{
	
	UCode::redirect('','404');
	
}