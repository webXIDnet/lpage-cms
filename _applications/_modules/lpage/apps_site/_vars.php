<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Глобальні змінні
 *  
***/
_run_action('LPage_init_start');

$sqlTPL->getCategories('lp');

global 	$list, 
		$lp,
        $user;

if ($_GET['lp'] == 'home' && $user->lang != '') {
    $lp_article = 'home_'.$user->lang;
} else {
    $lp_article = $_GET['lp'];
}

//var_dump($user);
//$lp_article = 'home_ru';
$lp = $sql->query("
	SELECT *
	FROM _landing_pages INNER JOIN _lp_templates
	ON lp_tpl_id = t_id
	WHERE lp_article='".$lp_article."'
",'value');

if(!empty($lp) && is_array($lp)){
	
	$lp['head'] = _run_filter('LPage_lp_head',$lp['lp_head']);
	$lp['html'] = _run_filter('LPage_lp_html',$lp['lp_html']);
	$lp['article'] = _run_filter('LPage_lp_article',$lp['lp_article']);
	$lp['lang'] = _run_filter('LPage_lp_lang',@$lp['lp_lang']);

	_run_action('LPage_init_done',$lp);
	
}else{

	$PData->redirect('', '404');
	
}