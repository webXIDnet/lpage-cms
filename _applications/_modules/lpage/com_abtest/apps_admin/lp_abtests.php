<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/
global $abTest, $sql, $meta;

if(isset($_POST['postEndSubmit']) && isset($_POST['_post'])){

	$abTest->finishTest($_POST['_post']);
	$PData->content('Тесты завершены', 'message', TRUE);

} elseif (isset($_POST['postDeleteSubmit']) && isset($_POST['_post'])) {

	if(is_array($_POST['_post'])){

		$where = 'test_id IN ('._protect(implode(',',array_keys($_POST['_post']))).')';
	}elseif($_POST['_post']){
		$where = 'test_id='._protect($_POST['_post']).')';
	}

	if ($sql->delete('_lp_split_test', $where) ){
		$PData->redirect(PAGE_URL);
	} else {
		$PData->content('Тесты не были удалены!', 'message', FALSE);
	}
}


$text='';

$abTest->getOrdersInfo();
$statuses = $meta->val('apSet_crm-system','mod_option');
$statuses = $statuses['statuses'];
//sort statuses array by value with saving keys
@asort($statuses);

if (isset($abTest->all) && is_array($abTest->all)){

	foreach($abTest->all AS $tkey=>$tval){
		$text1 = '';

		$diagram = '
			google.setOnLoadCallback(drawVisualization'.$tkey.');
			function drawVisualization'.$tkey.'() {
				// Some raw data (not necessarily accurate)
		        var data = google.visualization.arrayToDataTable([';

		$row = 0;

		$diagram_row = array();

		$max_orders = 1; #максимальнакількість заявок у тестування

		foreach ($tval['lp_stat'] as $lp_id => $lp_val) {#сторінка/рядок
			if ($max_orders < $lp_val['orders_count']){
				$max_orders	= $lp_val['orders_count'];
			}

			if ($row==0) {
				$diagram_row[$row][-1] = 'Страницы'; #назва таблиці
				// $diagram_row[$row][0] = 'Количество заявок'; #назва таблиці

				if (is_array($abTest->all_statuses)) {
					foreach ($abTest->all_statuses as $status_id => $status_val) {
						if ($row==0) {
							$diagram_row[0][$status_id] = $status_val; #заголовок статуса
						}
					}
				}
				$row++;
			}

			$diagram_row[$row][-1] = $lp_val['title']; #заголовок сторінки
			// $diagram_row[$row][0] = $lp_val['orders_count']; #загальна статистика

			// if ($diagram_row[$row][0] == 0) {
			// 	$diagram_row[$row][0] = 0.00001;
			// }

			if (is_array($abTest->all_statuses)) {
				foreach ($abTest->all_statuses as $status_id => $status_val) {

					if (!isset($diagram_row[$row][$status_id])) {
						$diagram_row[$row][$status_id] = 0.00001;
					}

					if ($row>0 && isset($lp_val['orders_statuses_count'][$status_id]) && $lp_val['orders_statuses_count'][$status_id]>0) {
						$diagram_row[$row][$status_id] = $lp_val['orders_statuses_count'][$status_id]; #статистика
					}
				}
			}

			$row++;

			$style = '';

			if ($tval['test_main_lp']==$lp_id){
				$style = 'style="font-weight:bold;"';
			}

			$text1 .= '<tr '.$style.'>
				<td>'.$lp_val['title'].'</td>
				<td>'.$lp_val['hosts'].'</td>
				<td>'.$lp_val['orders_count'].'</td>
				<td>'.$lp_val['conversion'].'%</td>
			</tr>';
		}

		$rows = array();

		foreach ($diagram_row as $row) {
			foreach ($row as $key => $value) {
				if (is_string($value)){
					$row[$key] = "'$value'";
				}
			}
			$rows[] = implode(", ",$row);
		}

		$diagram .= '
			['.implode('],
			[',$rows).']';

		// _dump($diagram_row,'diagram_row',1);
		$diagram .= ']);
				var options = {
					title: \''.$tval['test_title'].'\',
			        isStacked: true,
					height: 300,
					legend: {position: \'top\', maxLines: 100},
					vAxis: {title: \''._lang('Количество заявок').'\'},
					seriesType: \'bars\',
					vAxis: {
						minValue: 0,
						ticks: [0, '.$max_orders.']
					},
					series: {5: {type: \'line\'}}
			      };
				var chart = new google.visualization.ComboChart(document.getElementById(\'chart_div'.$tkey.'\'));
				chart.draw(data, options);
			}';

		// if (isset($tval['test_lpages_arr']) && is_array($tval['test_lpages_arr'])) {
		// 	foreach ($tval['test_lpages_arr'] as $key1=>$val1){
		// 		$hosts = 0;
		// 		$ords = 0;
		// 		$konv = 0;

		// 		if (isset($tval['orders'][$key1]) && isset($tval['stat'][$key1]) && count($tval['stat'][$key1])>0){
		// 			$hosts = count($tval['stat'][$key1]);
		// 			$ords = count($tval['orders'][$key1]);
		// 			$konv = round( $ords / $hosts * 100, 2);
		// 		}
		// 		$text1 .= '<tr>
		// 					<td>'.$val1['title'].'</td>
		// 					<td>'.$hosts.'</td>
		// 					<td>'.$ords.'</td>
		// 					<td>'.$konv.'%</td>
		// 				</tr>';
		// 	}
		// }

// 		if (!isset($_REQUEST['filter']) || !$_REQUEST['filter'] || (strpos($mkey,$_REQUEST['filter']) !== false))

		// _dump($diagram,'',1);

		$text.='<tr>
			<td class="col0 tableList0">
				<label><input title="'._lang('выбрать').'" type="checkbox" name="_post['.$tkey.']"/>&nbsp;&nbsp;&nbsp;'.$tkey.'</label>
			</td>
			<td class="col2 tableList2 col">
				'._lang('Тестирование').': <b>'.$tval['test_title'].'</b><br>
				'._lang('Дата начала').': '.$tval['test_date_start'].'<br>
				'._lang('Дата конца').': '.$tval['test_date_end'].'
				<hr>
				<table width="100%">
					<tr>
						<th>'._lang('Страница').'</th>
						<th>'._lang('Показов').'</th>
						<th>'._lang('Заказов').'</th>
						<th>'._lang('Конверсия').'</th>
					</tr>
					'.$text1.'
				</table>
			</td>
			<td class="col2 tableList2 col">
				<div id="chart_div'.$tkey.'"></div>
		   		<script>'.$diagram.'</script>
			</td>
			<tr>
		   		<td colspan="3">

		   		</td>
		  </tr>
		</tr>';

	}
}
$PData->content('А/Б тесты','title');

$img_dir = HOMEPAGE . 'media/_admin/';

//todo fix add A/B tests
$PData->content('
	<script type="text/javascript">
    	google.load("visualization", "1", {packages:["corechart"]});
    </script>


	  <div class="add-new">
        <a href="'.getURL('admin','ab-test-add').'">
        <div class="item">
            <img src="'.$img_dir.'img/add-new.png" alt="add-new">
            <p>'._lang('Добавить А/Б тест').'</p>
        </div>
        </a>
      </div>
      <div class="line"></div>
		<form id="tableEdit" method="POST">
	    <div class="table-holder" style="overflow-x: scroll;">
		    <table id="tableEdit" class="table">
		      <thead>
		        	<th class="table-5">&nbsp;</th>
					<th class="table-35">'._lang('Информация').'</th>
					<th class="table-60">'._lang('Статистика по статувас заявок').'</th>
		      </thead>
		      <tbody>
		        	'.$text.'
		      </tbody>
		    </table>
		    <input class="button" type="submit" value="'._lang('Завершить').'" name="postEndSubmit">
		    <input class="button left" type="submit" value="'._lang('Удалить').'" name="postDeleteSubmit">

	    </div>
		</form>

	<p>! - '._lang('При прощете конверсии учитывабтся все заявки, кроме тех, у которых статус "Не валид" или "Не определен"').'

');