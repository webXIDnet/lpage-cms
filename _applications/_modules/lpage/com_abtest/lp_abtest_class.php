<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модель Тегов (таксономия)
 *
 * v1.1
 *
***/

class ABTest{
	public $all;
	public $unused_lp;
	public $used_lp;
	public $all_statuses; //всі можливі статуси, по яких є заявки в базі

	public function getAllTests(){
		global 	$sql;

		$result = $sql->query("SELECT * FROM _lp_split_test");
		$tests = $result;

		$result = $sql->query("SELECT lp_id, lp_title, lp_article FROM _landing_pages");
		foreach ($result as $lp){
			$this->unused_lp[$lp['lp_id']] = $lp;
		}

		$stat = array();
		$result = $sql->query("SELECT *, RIGHT( st_key, LENGTH( st_key ) - LENGTH( st_date ) - LENGTH( st_lp_id ) ) AS test_id FROM _statistic WHERE st_key<>CONCAT(st_date, st_lp_id);");




		// foreach ($result as $st){

		// 	if (isset($stat[$st['test_id']][$st['st_lp_id']])){

		// 		$stat[$st['test_id']][$st['st_lp_id']] = $stat[$st['test_id']][$st['st_lp_id']] + $st['st_hosts'];

		// 	}else{

		// 		$stat[$st['test_id']][$st['st_lp_id']] = $st['st_hosts'];

		// 	}
		// }

		foreach ($result as $lp){
			$stat[$lp['st_lp_id']] = $lp;
		}

		foreach ($this->unused_lp as $lp_id => $value) {

			if (isset($stat[$lp_id]) && !empty($stat[$lp_id]) && isset($stat[$lp_id]['test_id'])){
				$this->unused_lp[$lp_id]['stat'][$stat[$lp_id]['test_id']] = $stat[$lp_id];
			} elseif (isset($stat[$lp_id]['test_id'])) {
				$this->unused_lp[$lp_id]['stat'][$stat[$lp_id]['test_id']] = array(
		            'st_lp_id' => $lp_id,
		            'st_hits' => 0,
		            'st_hosts' => 0
		        );
			}
		}

		$finishedTest = false;

		foreach($tests as $key=>$val){
			$finishedTest = $val['test_date_end']!='0000-00-00 00:00:00';
			$test_id = $val['test_id'];
			$this->all[$test_id] = $val;
			$test_lpages = explode(',', $this->all[$test_id]['test_lpages']);

			if (!isset($this->all[$test_id]['lp_stat'][$val['test_main_lp']])) {

				if (!isset($this->unused_lp[$val['test_main_lp']]['stat'][$test_id])) {
					$this->unused_lp[$val['test_main_lp']]['stat'][$test_id]['st_hosts'] =  0;
				}

				$this->all[$test_id]['lp_stat'][$val['test_main_lp']] = array(
					'title' => $this->unused_lp[$val['test_main_lp']]['lp_title'],
					'article' => $this->unused_lp[$val['test_main_lp']]['lp_article'],
					'hosts'=> $this->unused_lp[$val['test_main_lp']]['stat'][$test_id]['st_hosts'],
					'orders_count' => 0,
				);
			}

			foreach ($test_lpages as $lp_id) {

				if (!isset($this->unused_lp[$lp_id]['stat'][$test_id])) {
					$this->unused_lp[$lp_id]['stat'][$test_id]['st_hosts'] =  0;
				}

				$this->all[$test_id]['lp_stat'][$lp_id] = array(
					'title' => $this->unused_lp[$lp_id]['lp_title'],
					'article' => $this->unused_lp[$lp_id]['lp_article'],
					'hosts'=> $this->unused_lp[$lp_id]['stat'][$test_id]['st_hosts'],
					'orders_count' => 0,
				);

				$this->unused_lp[$lp_id];

			}

		}

		// foreach($tests as $key=>$val){
		// 	$finishedTest = $val['test_date_end']!='0000-00-00 00:00:00';
		// 	$test_id = $val['test_id'];
		// 	$this->all[$test_id] = $val;

		// 	if (isset($this->unused_lp[$this->all[$test_id]['test_main_lp']])){

		// 		$this->all[$test_id]['test_main_lp_title'] = $this->unused_lp[$val['test_main_lp']]['lp_title'];
		// 		$this->all[$test_id]['test_main_lp_article'] = $this->unused_lp[$val['test_main_lp']]['lp_article'];

		// 		if (isset($stat[$test_id])){
		// 			$this->all[$test_id]['stat'] = $stat[$test_id];
		// 		}


		// 		if (!isset($this->all[$test_id]['stat'][$val['test_main_lp']])){
		// 			$this->all[$test_id]['stat'][$val['test_main_lp']] = 0;
		// 		}

		// 		$this->used_lp[$this->all[$test_id]['test_main_lp_article']]['lpages'] = $val['test_lpages'];
		// 		$this->used_lp[$this->all[$test_id]['test_main_lp_article']]['test_id'] = $test_id;
		// 		$this->used_lp[$this->all[$test_id]['test_main_lp_article']]['lpage_id'] = $val['test_main_lp'];

		// 		if (!$finishedTest){
		// 			unset($this->unused_lp[$val['test_main_lp']]);
		// 		}

		// 	}
		// 	$this->all[$test_id]['test_lpages_titles'] = array();
		// 	foreach (explode(',', $val['test_lpages']) as $val1){
		// 		if (isset($this->unused_lp[$val1])){
		// 			$this->all[$test_id]['test_lpages_arr'][$val1]['title'] = $this->unused_lp[$val1]['lp_title'];
		// 			$this->all[$test_id]['test_lpages_arr'][$val1]['article'] = $this->unused_lp[$val1]['lp_article'];

		// 			if (!isset($this->all[$test_id]['stat'][$val1])){
		// 				$this->all[$test_id]['stat'][$val1] = 0;
		// 			}

		// 			if (!$finishedTest){
		// 				unset($this->unused_lp[$val1]);
		// 			}
		// 		}
		// 	}
		// }

		// _dump($tests,'$tests - _lp_split_test');
		// _dump($this->unused_lp,'$this->unused_lp - _landing_pages');
		// _dump($result,'_statistic');
		// _dump($this->all,'$this->all', 1);

	}
/*
	public function getAllUnusedLP(){
		global 	$sql;

		$where = '0';
		foreach ($this->all as $test){
			if($test['test_lpages']){
				$where.=','.$test['test_lpages'];
			}
			$where.=','.$test['test_main_lp'];
		}

		$result = $sql->query("
				SELECT lp_id, lp_title, lp_article
				FROM _landing_pages
				WHERE lp_id NOT IN ("._protect($where).")
		");

		$this->unused_lp = $result;
	}
*/
	function add_test($param) {
		global $sql;
		if (isset($param['test_title']) && isset($param['test_main_lp']) && isset($param['test_lpages'])){
			if (isset($param['test_lpages']) && is_array($param['test_lpages'])){
				$param['test_lpages'] = implode(',',$param['test_lpages']);
			}
			$sql->insert('_lp_split_test', $param);
			return TRUE;
		}

		return FALSE;
	}

	function getOrdersInfo(){
		global $sql, $meta;

		$sql_text = "";
		if (is_array($this->all)){
			foreach($this->all AS $tkey=>$tval){
				if ($sql_text){
					$sql_text.=" UNION ALL ";
				}

				$sql_text .= '(SELECT '._protect($tkey).' as test_id, o_status, o_lp_article, o_date_create FROM _orders WHERE o_status!=-1 AND o_status!=0 AND o_status!="" AND o_date_create>='._protect(strtotime($tval['test_date_start']));
				if (isset($tval['test_date_end']) && $tval['test_date_end']!='0000-00-00 00:00:00'){
					$sql_text .= ' AND o_date_create<'._protect(strtotime($tval['test_date_end']));
				}
				// $sql_text .= ' GROUP BY o_lp_article)';
				$sql_text .= ' )';
			}

			$result = $sql->query( $sql_text );

			// _dump($result,'$result',1);

			$order_stat_temp = array();

			foreach ($result as $k => $ord){
				$result [$k]['o_date_create'] = date('Y-m-d H:i:s',$ord['o_date_create']);
				$order_stat_temp[$ord['o_lp_article']][] = $ord;
			}

			$order_stat = array();
			$statuses = $meta->val('apSet_crm-system','mod_option'); //отримання всіх статусів з бази
			$statuses = $statuses['statuses'];
			// _dump($order_stat_temp,'$order_stat_temp',1);

			foreach ($order_stat_temp as $lp_article => $stat){
				$order_stat[$stat[0]['test_id']][$lp_article]['count'] = count($stat);

				foreach ($stat as $s) {
					$this->all_statuses[$s['o_status']] = $statuses[$s['o_status']];
					$o_status = $s['o_status'];

					if(!isset($order_stat[$stat[0]['test_id']][$lp_article]['status'][$o_status])) {
						$order_stat[$stat[0]['test_id']][$lp_article]['status'][$o_status] = 0;
					}

					$order_stat[$stat[0]['test_id']][$lp_article]['status'][$o_status]++;
				}
			}

			foreach ($this->all as $test_id => $test){

				foreach ($test['lp_stat'] as $lp_id => $lp_val) {

					if(!isset($order_stat[$test_id][$lp_val['article']]['status'])){
						$order_stat[$test_id][$lp_val['article']]['status'] = array();
					}
					$this->all[$test_id]['lp_stat'][$lp_id]['orders_statuses_count'] = $order_stat[$test_id][$lp_val['article']]['status'];


					if (!isset($order_stat[$test_id][$lp_val['article']]['count'])){
						$order_stat[$test_id][$lp_val['article']]['count'] = 0;
					}
					$this->all[$test_id]['lp_stat'][$lp_id]['orders_count'] = $order_stat[$test_id][$lp_val['article']]['count'];

					$this->all[$test_id]['lp_stat'][$lp_id]['conversion'] = 0;

					if ($this->all[$test_id]['lp_stat'][$lp_id]['orders_count']!=0) {
						$this->all[$test_id]['lp_stat'][$lp_id]['conversion'] = round( $this->all[$test_id]['lp_stat'][$lp_id]['orders_count'] / $this->all[$test_id]['lp_stat'][$lp_id]['hosts'] * 100, 2);
					}
				}
			}
		}

	}

	function finishTest($tests){
		global $sql;

		$where = '';
		if(is_array($tests)){

			$where = 'test_id IN ('._protect(implode(',',array_keys($tests))).')';
		}elseif($tests){
			$where = 'test_id='._protect($tests).')';
		}
		if ($where){
			$query = "UPDATE _lp_split_test SET test_date_end=NOW() WHERE ".$where;

			$sql->query($query,'query');
		}
	}
}