<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Компонента опрацювання замовлень для CRM системи
 *  
***/ 

function FormActionURL($get=''){
	parse_str($get,$array);
	
	$article = '';
	$prod = @$array['prod'];
	$price = @$array['price'];
	
	if(isset($array['lpage'])){
		$article = @$array['lpage'];
		$p_type = 'lp';
	}elseif(isset($array['page'])){
		$article = @$array['page'];
		$p_type = 'page';
	}
	
	if(PAGE_TYPE!='admin'){
	
		global $lp;
		
		if(!empty($article)){
			$set = _strCode( 
				array(
					'lp_article' => $lp['lp_article'],
					'prod_title' => $prod,
					'prod_price' => $price,
				) 
			);
			return _shortCode( getURL($p_type, $article, 'set='.$set ) );
		}
		
		return PAGE_URL;	
	}
	return '[:FormActionURL '.$get.':]';
}


_add_hook('init_processes','recivingForm_get_order');

function recivingForm_get_order(){
	
	if(isset($_POST['ordersubmit'])){
		
		$text = '';
		
		foreach($_POST AS $key => $val){
			
			if($key=='ordersubmit')continue;
			
			if(substr($key, 0, 3)=='rf_'){
				 if(empty($val)){
					global $PData;
					
					$PData->content('Нужно заполнить все обязательные поля!','message');
					
					_add_filter('trig_OrderResiving','rf_empty_post');
					
					return FALSE;
				}else{
					$key = substr($key, 3);
				}
			}
			if(substr($key, 0, 2)=='f_'){
				 $key = substr($key, 2);
			}
			if(is_array($val)){
				$t = '';
				foreach($val AS $k=>$v){
					$t .= '<br/>'.$k.': '.$v;
				}
				$val = $t;
			}
			$text .= '<b>'.$key.'</b>: '.$val.'<br/>';
		}
		
		foreach($_POST AS $key => $val){
			if(substr($key, 0, 2)=='f_'){
				$key = substr($key, 2);
				if(!isset($_POST['rf_'.$key])){
					$_POST['rf_'.$key] = $val;
					unset($_POST['f_'.$key]);
				}
			}
		}
		
		global 	$meta,
				$PData;
		
		//if(!empty($text))$PData->content('Форма отправлена','message',TRUE);
			
		if(_checkEmail($meta->val('contact_email'))){
			_eMail($meta->val('contact_email'), _lang('На сайте оставлена форма'), $text);//*/
		}
	}
}

function rf_empty_post($result){
	
	if(!defined('RF_ISSET_POST'))
		define('RF_ISSET_POST','TRUE');
	
	return $result .'
	<style>.thanks-text{display:none;}</style>
	<h1>Нужно заполнить все обязательные поля!</h1>
	<a href="'.@$_SERVER['HTTP_REFERER'].'" style=" display: table; margin: 40px auto 0; background: #AAA; color: #fff; padding: 5px 15px; text-decoration: none;">&lt;&lt; Вернуться назад</a>
	';
	
}


_add_hook('LPage_init_done', 'rf_lp_change_name');

function rf_lp_change_name(){
	
	global $lp;
	
	$lp['html'] = _sharpCode(
		$lp['html'],
		array(
			'#NAME#'=>@$_POST['rf_name'],
			'#PHONE#'=>@$_POST['rf_phone'],
			'#EMAIL#'=>@$_POST['rf_email'],
		)
	);
}


if(PAGE_TYPE=='post' || PAGE_TYPE=='page'){
	_add_filter('page_init', 'rf_post_change_name');
	_add_filter('post_init', 'rf_post_change_name');
}

function rf_post_change_name($post){
	
	$post['p_text'] = _sharpCode(
		$post['p_text'],
		array(
			'#NAME#'=>@$_POST['rf_name'],
			'#PHONE#'=>@$_POST['rf_phone'],
			'#EMAIL#'=>@$_POST['rf_email'],
		)
	);
	return $post;	
}