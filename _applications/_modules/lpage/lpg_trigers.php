<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль стандартних тригерів LPage 
 *  
***/

function PROD_LP(){
	$text = '';
	if(PAGE_TYPE=='admin'){
		$text = '[:PROD_LP:]';
	}else{
		$text = $_GET['lp'];;
	}
	return $text;
}

function TPL_FOLDER(){
	
	global 	$lp;
	
	return SITE_FOLDER.'media/_'.$lp['t_article'].'/';
	
}

function PAGE_URL(){
	
	global 	$list,
			$lp;
			
	if(isset($lp['lp_article']) && is_object($list)){
		return getURL('lp', $lp['lp_article']);
	}
		
	return PAGE_URL;
	
}

function HOMEPAGE(){
	
	return HOMEPAGE;
	
}

function SimpleCounter($date){
	
	if(PAGE_TYPE!='admin'){
		
		global 	$lp,
				$JQUERY_COUNTDOWN;
		
		$d = str_replace('_', ' ', $date);
		$text = '';
		
		if(!defined('JQUERY_COUNTDOWN')){

			$lp['head'].= '
				<script src="'.SITE_FOLDER.'plugins/countdown-timer/jquery.countdown.js"></script>';
				$JQUERY_COUNTDOWN = 0;
				define('JQUERY_COUNTDOWN',$JQUERY_COUNTDOWN);
		}
		
		$id = 'countdown'.$JQUERY_COUNTDOWN;
		
		if(!defined('JQUERY_COUNTDOWN'.$date)){
			
			$JQUERY_COUNTDOWN++;
			
			$id = 'countdown'.$JQUERY_COUNTDOWN;
			
			$lp['head'].= '
				<script>TIME_LINIT'.$JQUERY_COUNTDOWN.'='.(int)((int)strtotime($d)-time()).';</script>
				<script>
				
					$(document).ready(function(){
						i=0;
						$(\'.'.$id.'\').each(function(){
							i++;
							$(this).attr(\'id\',\''.$id.'\'+i);
							var note = $(\'#note\'),
							ts = new Date(2012, 0, 1),
							newYear = true;
							
							if((new Date()) > ts){
								// Задаем точку отсчета для примера. Пусть будет очередной Новый год или дата через 10 дней.
								// Обратите внимание на *1000 в конце - время должно задаваться в миллисекундах
								ts = (new Date()).getTime()+ TIME_LINIT'.$JQUERY_COUNTDOWN.'*1000;
								newYear = false;
							}
							
							
							
							$(\'#'.$id.'\'+i).countdown({
								timestamp	: ts,
								callback	: function(days, hours, minutes, seconds){
									
									var message = "";
									
									message += "Дней: " + days +", ";
									message += "часов: " + hours + ", ";
									message += "минут: " + minutes + " и ";
									message += "секунд: " + seconds + " <br />";
									
									if(newYear){
										message += "осталось до Нового года!";
									}
									else {
										message += "осталось до момента через 10 дней!";
									}
									
									note.html(message);
								}
							});
						});
					});
				
				</script>
			';
			define('JQUERY_COUNTDOWN'.$date,'1');
		}
		
		
		return'	<!-- '.$date.' -->
			
			<div class="'.$id.'"></div>
			
			<div class="cl">
				<span>'._lang('дней').'</span><span>'._lang('часов').'</span><span>'._lang('минут').'</span><span>'._lang('секунд').'</span>
			</div>'.$text.'
			<script>
				
			</script>';
	}
	
	return '[:SimpleCounter '.$date.' :]';
}

function CyclicCounter($date){
	
	if(PAGE_TYPE!='admin'){
		
		$ar = explode('_', $date);
		
		$stap = @$ar[0]*60*60*24;
		$time = @$ar[1];
		
		if(!empty($ar[2])){
			$start = @$ar[2];
		}else{
			$start = strtotime(date('Y-m-d'));
		}
		
		
		$start_time = strtotime($start.' '.$time);
		
		$end_time = (floor((time()-$start_time)/$stap)+1)*$stap+$start_time;
		
		
		
		$text = '';
		$id = _getRandStr();
		
		if(!defined('CYCLIC_COUNTER')){
	
			global $lp;
			
			$lp['head'].= '<script>TIME_LINIT='.((int)$end_time-time()).';</script>
				<script src="'.SITE_FOLDER.'plugins/countdown-timer/jquery.countdown.js"></script>
				<script>
				
					$(document).ready(function(){
						i=0;
						$(\'.'.$id.'\').each(function(){
							i++;
							$(this).attr(\'id\',\'ddd\'+i);
							var note = $(\'#note\'),
							ts = new Date(2012, 0, 1),
							newYear = true;
							
							if((new Date()) > ts){
								// Задаем точку отсчета для примера. Пусть будет очередной Новый год или дата через 10 дней.
								// Обратите внимание на *1000 в конце - время должно задаваться в миллисекундах
								ts = (new Date()).getTime()+ TIME_LINIT*1000;
								newYear = false;
							}
							
							
							
							$(\'#ddd\'+i).countdown({
								timestamp	: ts,
								callback	: function(days, hours, minutes, seconds){
									
									var message = "";
									
									message += "Дней: " + days +", ";
									message += "часов: " + hours + ", ";
									message += "минут: " + minutes + " и ";
									message += "секунд: " + seconds + " <br />";
									
									if(newYear){
										message += "осталось до Нового года!";
									}
									else {
										message += "осталось до момента через 10 дней!";
									}
									
									note.html(message);
								}
							});
						});
					});
				
				</script>
			';
			define('CYCLIC_COUNTER','1');
		}
		
		
		
		return'
			
			<div class="'.$id.'"></div>
			
			<div class="cl">
				<span>'._lang('дней').'</span><span>'._lang('часов').'</span><span>'._lang('минут').'</span><span>'._lang('секунд').'</span>
			</div>'.$text.'
			<script>
				
			</script>';
	}
	
	return '[:CyclicCounter '.$date.' :]';
}

function OrderResiving($vars){
	
	$text = '';
	
	if(PAGE_TYPE=='admin'){
		
		$text = '[:OrderResiving '.$vars.':]';
		
	}else{
		$text = _run_filter('trig_OrderResiving','',$vars);
	}
	return $text;
}

function PaymentButtons($vars){
	
	$text = '[:PaymentButtons '.$vars.':]';
	
	if(PAGE_TYPE!='admin'){
				
		$text = _run_filter('trig_PaymentButtons','',$vars);
		
	}
	return $text;
}

function TRIG_COUNT($at){
	
	if(PAGE_TYPE=='admin')return '[:TRIG_COUNT '.$at.':]';
	
	global $sql;
	
	$TRIG_COUNT = '';
	parse_str($at,$attr);
	
/**
$attr = array(
	'days', // количество дней с первого захода на страницу
	'id', // id следующей страници
	'prev' => 'main' // id первой страницы
	'next_price' // Цена на следующей период
);
**/
	
	if(!isset($attr) || !is_array($attr) || !isset($attr['days']) || !isset($attr['id']) || !isset($attr['prev']))return '[:TRIG_COUNT '.$at.':]';
	
	$id = (int)base64_decode(@$_COOKIE['_xup']);
	
	if(empty($_COOKIE['_xup']) || empty($id)){
		
		$cval = $sql->query("
			SELECT 
				u_id,
				m_value
			FROM _users LEFT JOIN _meta_and_options
			ON u_id = m_article AND m_type = 'trig_count_time'
			WHERE u_ip = '"._protect(_getIP())."'
		",'value');
		
		if(isset($cval) && is_array($cval)){
			
			setcookie('_xup',base64_encode($cval['u_id']));
			
			$time = $cval['m_value'];
						
		}else{
			
			$id = $sql->insert('_users',array('u_ip'=>_getIP(),'u_rights'=>'-1'));
			$time = time();
			setcookie('_xup',base64_encode($id));
			
			$sql->insert('_meta_and_options',array(
				'm_article'=>$id,
				'm_value'=>$time,
				'm_type'=>'trig_count_time'
			));
			
			if( $attr['prev']!='main'){
		
				$cval = $sql->query("
					SELECT 
						lp_article,
						c_url
					FROM _landing_pages INNER JOIN _categories
					ON lp_cat_id = c_id
					WHERE lp_id = '"._protect($attr['prev'])."'
				",'value');
				
				UCode::redirect(getURL('lp', $cval['lp_article'], 'category='.$cval['c_url']),'403');
				
			}
		}
	}else{
		
		$cval = $sql->query("
			SELECT 
				u_id,u_ip,
				m_value
			FROM _users LEFT JOIN _meta_and_options
			ON u_id = m_article AND m_type = 'trig_count_time'
			WHERE u_id = '"._protect($id)."'
		",'value');
		
		if(isset($cval) && is_array($cval)){
			
			if($cval['u_ip']!=_getIP()){
				
				$sql->update('_users',array('u_ip'=>_getIP()));
				
			}
			
			$time = $cval['m_value'];
			
		}else{
			
			$id = $sql->insert('_users',array('u_ip'=>_getIP(),'u_rights','-1'));
			$time = time();
			setcookie('_xup',base64_encode($id));
			
			$sql->insert('_meta_and_options',array(
				'm_article'=>$id,
				'm_value'=>$time,
				'm_type'=>'trig_count_time'
			));
			
			if( $attr['prev']!='main'){
		
				$cval = $sql->query("
					SELECT 
						lp_article,
						c_url
					FROM _landing_pages INNER JOIN _categories
					ON lp_cat_id = c_id
					WHERE lp_id = '"._protect($attr['prev'])."'
				",'value');
				
				UCode::redirect(getURL('lp', $cval['lp_article'], 'category='.$cval['c_url']),'403');
				
			}	
		}		
	}
	
	$time = $time+60*60*24*$attr['days'];
	
	if( strtotime(date('Y-m-d')) > strtotime(date('Y-m-d',$time)) ){
		
		$cval = $sql->query("
			SELECT 
				lp_article,
				c_url
			FROM _landing_pages INNER JOIN _categories
			ON lp_cat_id = c_id
			WHERE lp_id = '"._protect($attr['id'])."'
		",'value');
		
		UCode::redirect(getURL('lp', $cval['lp_article'], 'category='.$cval['c_url']),'403');
		
	}
	
	if(!defined('TRIG_COUNT')){
	
		$TRIG_COUNT .= '
<style type="text/css">
@import "'.HOMEPAGE.'css/jquery.countdown.css";

.defaultCountdown { width: 240px; height: 45px; }
</style>
<script type="text/javascript">
if(!window.jQuery){
document.write(\'<script type="text/javascript" src="'.HOMEPAGE.'js/jq.lib.js"></s\'+\'cript>\');
}
</script>
<script type="text/javascript" src="'.HOMEPAGE.'js/jquery.countdown.js"></script>
<script type="text/javascript" src="'.HOMEPAGE.'js/jquery.countdown-ru.js"></script>
	';
		define('TRIG_COUNT','TRUE');
	}
	
	$TRIG_COUNT .= '
	<span class="defaultCountdown"></span>
<script type="text/javascript">
$(function () {
	var austDay = new Date(\''.date('Y-m-d',$time).'\');
	$(\'.defaultCountdown\').countdown({until: austDay});
});
</script>
	';
	
	if(!empty($attr['next_price'])){
		
		$TRIG_COUNT .= '
			<span class="next_price">'._lang('Следующая цена').' <strike>'.$attr['next_price'].'</strike></span>
			';
		
	}
	
	return $TRIG_COUNT;
}