<?php
if (PAGE_TYPE == 'admin') {
    _add_action('defined_modules', 'menus_init');
} else {
    _add_action('defined_modules', 'menu_frontend_init');
}

function menus_init()
{
    global $sql;

    $sql->db_table_installer('Menus', '21.05.2015',
        array(
            '_menu_items' => array(
                'mi_id'      => '`mi_id` int(11) NOT NULL AUTO_INCREMENT',
                'mi_parent_id'    => '`mi_parent_id` varchar(50) NOT NULL',
                'mi_title' => '`mi_title` varchar(255) NOT NULL',
                'mi_url'    => '`mi_url` varchar(255) NOT NULL',
                'mi_sort'   => '`mi_sort` int(11) NOT NULL',
                'mi_lang'   => '`mi_lang` int(11) NOT NULL',
                'CREATE'     => "`mi_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`mi_id`)"
            )
        )
    );
}

/** Формування меню в адмінці  **/
if (PAGE_TYPE=='admin') {
    _add_action('adminPanel_controller', 'com_menus_init_admin',1);
    _add_filter('meta_datas_admin_sidebar_menu_elenemts', 'com_menus_submenu');
    _add_filter('meta_datas_admin_sidebar_menu_elenemts_active_items', 'menu_active_items');
} else {
    _add_action('defined_modules', 'menus_site_init');
}

function menu_active_items($res) {
    $res []= 'menus';
    $res []= 'menus/edit';
    return $res;
}

function com_menus_init_admin() {
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;
    switch (@$_GET['admin']) {
        case 'menus/add':
            require_once __DIR__ . '/apps_admin/menus_add.php';
            return true;
            break;
        case 'menus/edit':
            require_once __DIR__ . '/apps_admin/menus_edit.php';
            return true;
            break;
        case 'menus':
            require_once __DIR__ . '/apps_admin/menus_list.php';
            return true;
            break;
    }
}

function menus_site_init() {
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;
    require_once __DIR__.'/apps_site/menu.php';
}

function com_menus_submenu($result) {
    global $PData;
    $vars_array = array(
        array(
            'title' => _lang('Меню'), //ім"я пункта меню
            'url' => getURL('admin', 'menus'), //посилання
            'active_pages' => array('menus','menus/edit'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        )
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100,4,5,6), $menu);

    return $result . $menu;
}

function menu_frontend_init() {
    global $meta, $sql, $PData;
    require_once __DIR__.'/apps_site/menu.php';
}

_add_filter('admin_panel_header', 'products_headers');

function menu_headers($result)
{
    $result .= '
         <link href="' . HOMEPAGE . 'js/jquery-ui/jquery-ui.min.css" type="text/css" rel="stylesheet"/>
    <link href="' . HOMEPAGE . 'js/jquery-ui/font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet"/>
    <link href="' . HOMEPAGE . 'js/chosen/chosen.admin.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="' . HOMEPAGE . 'js/chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="' . HOMEPAGE . 'js/jquery-ui/jquery-ui.min.js"></script>
    ';
    return $result;
}
