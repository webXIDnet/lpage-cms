<?php
/**
 * User: BoHand
 * Date: 30.05.2015
 * Time: 1:30
 */
global $sql;

//структура меню сформирована
$query = "SELECT * FROM _menu_items";
$result = $sql->query($query, $type = 'array', $error = FALSE);

$mi_struct = array();//menus structure
foreach ($result as $val) {
    $mi_struct[$val['mi_id']] = $val;// new MenuItem($val['mi_id'],$val['mi_parent_id'],$val['mi_title'],$val['mi_url'],$val['mi_sort'],$val['mi_lang']);
    $mi_struct[$val['mi_id']]['sub'] = array();
}

foreach ($mi_struct as $val) {
    if (!empty($val['mi_parent_id'])) {
        $mi_struct[$val['mi_parent_id']]['sub'][$val['mi_id']] = $mi_struct[$val['mi_id']];
        $mi_struct[$val['mi_id']] =& $mi_struct[$val['mi_parent_id']]['sub'][$val['mi_id']];
    }
}
foreach ($result as $val) {
    if (!empty($val['mi_parent_id'])) {
        unset($mi_struct[$val['mi_id']]);
    }
}
//структура меню сформирована


function mi_get_submenus($pre, $struct)
{
    $text = '';
    foreach ($struct as $item) {//if have submenu add too
                $text .= '<option value="' . $item['mi_id'] . '">' . $pre . $item['mi_title'] . '</option>';
            if (count($item['sub']) > 0) {
                foreach ($item['sub'] as $sub) {
                    $text .= mi_get_submenus($pre . $pre, $item['sub']);
                }
            }
    }
    return $text;
}

function mi_get_select($struct)
{
    $menus_list = '<select name="menu[mi_parent_id]" size="3">' . mi_get_submenus('-', $struct) . '</select>';
    return $menus_list;
}

/**/
//_dump(mi_get_select(1, $mi_struct));

$PData->content(_lang('Добавить меню'), 'title');
$PData->content('
<form class="list" method="POST" action="'. getURL('admin', 'menus') .'">
<input type="hidden" name="menu[mi_id]" class="input-xlarge" value="' . (isset($edited_menu['mi_id']) ? $edited_menu['mi_id'] : '') . '"/>
		<p>
			<label>' . _lang('Название') . '</label>
			<input type="text" name="menu[mi_title]" class="input-xlarge" value=""/>
		</p>
		<p>
			<label>' . _lang('Артикул') . '</label>
			<input type="text" name="menu[mi_url]" class="input-xlarge" value=""/>
		</p>
		<input class="button" type="submit" value="' . _lang('Сохранить') . '" name="menuNewSubmit"/>
	</form>
');