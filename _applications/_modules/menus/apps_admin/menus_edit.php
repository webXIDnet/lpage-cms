<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 29.05.2015
 * Time: 13:32
 */
global $sql;

if (empty($_REQUEST['id'])) $PData->redirect(getURL('admin'), '403');

$max_m_id = $sql->query('SELECT MAX(mi_id) FROM _menu_items');
//var_dump($max_m_id);
$max_m_id = $max_m_id[0]['MAX(mi_id)'];
//var_dump($max_m_id);
if (isset($_POST['menuNewSubmit'])) {
	$save_menu['mi_id'] = _protect($_POST['menu']['mi_id']);
	$save_menu['mi_title'] = _protect($_POST['menu']['mi_title']);
	$save_menu['mi_url'] = _protect($_POST['menu']['mi_url']);
	$query = "UPDATE _menu_items SET `mi_title`='" . $save_menu['mi_title'] . "', `mi_url`='" . $save_menu['mi_url']
		. "' WHERE  `mi_id`=" . $save_menu['mi_id'] . ";";
	$sql->query($query, $type = 'query', $error = FALSE);
	$PData->content(_lang('Запись обновлена'), 'message', TRUE);
}

if (isset($_POST['menu-delete'])) {
	$menu_delete = json_decode($_POST['menu-delete'], true);
	foreach ($menu_delete['id'] as $menu_id) {
		$query = "DELETE FROM _menu_items WHERE  `mi_id`=" . $menu_id . ";";
		$sql->query($query, $type = 'query', $error = FALSE);
	}
}

if (isset($_POST['menu-structure'])) {
	$menu_structure = json_decode($_POST['menu-structure'], true);
	//_dump($menu_structure);
	if (is_array($menu_structure) ) {
		foreach ($menu_structure as $menu_item) {


			$save_menu['mi_id'] = _protect($menu_item['mi_id']);
			$save_menu['mi_title'] = _protect($menu_item['mi_title']);
			$save_menu['mi_url'] = _protect($menu_item['mi_url']);
			$save_menu['mi_sort'] = _protect($menu_item['mi_sort']);
			$save_menu['mi_parent_id'] = _protect($menu_item['mi_parent_id']);

			$query = "INSERT INTO _menu_items (mi_parent_id, mi_title, mi_url, mi_sort, mi_id)
	VALUES ('" . $save_menu['mi_parent_id'] . "', '" . $save_menu['mi_title'] . "','" . $save_menu['mi_url'] . "','" . $save_menu['mi_sort'] . "','" . $save_menu['mi_id'] . "')
	ON DUPLICATE KEY UPDATE
	mi_parent_id='" . $save_menu['mi_parent_id'] . "', mi_title='" . $save_menu['mi_title'] . "', mi_url='" . $save_menu['mi_url'] . "', mi_sort='" . $save_menu['mi_sort'] . "', mi_id='" . $save_menu['mi_id'] . "'";

			$sql->query($query, $type = 'query', $error = FALSE);
		}
	}
}

//структура меню сформирована
$query = "SELECT * FROM _menu_items ORDER BY `mi_sort`";
$result = $sql->query($query, $type = 'array', $error = FALSE);

global $mi_struct;
$mi_struct = array();//menus structure
foreach ($result as $val) {
	$mi_struct[$val['mi_id']] = $val;// new MenuItem($val['mi_id'],$val['mi_parent_id'],$val['mi_title'],$val['mi_url'],$val['mi_sort'],$val['mi_lang']);
	$mi_struct[$val['mi_id']]['sub'] = array();
}

$edited_menu = $mi_struct[$_REQUEST['id']];//get element menu wich edit

foreach ($mi_struct as $val) {
	if (!empty($val['mi_parent_id'])) {
		$mi_struct[$val['mi_parent_id']]['sub'][$val['mi_id']] = $mi_struct[$val['mi_id']];
		$mi_struct[$val['mi_id']] =& $mi_struct[$val['mi_parent_id']]['sub'][$val['mi_id']];
	}
}

foreach ($result as $val) {
	if (!empty($val['mi_parent_id'])) {
		unset($mi_struct[$val['mi_id']]);
	}
}
//структура меню сформирована


#Формуємо селект длякопіювання меню
$menu_select_list = '';

foreach ($mi_struct as $key => $value) {
	if (isset($value['mi_title']) && $_REQUEST['id']!=$key) {
		unset($mi_struct[$key]['sub']);
		$menu_select_list .= '<option value="'.$value['mi_id'].'">'.$value['mi_title'].'</option>';
	}
}

# Копіювання меню
if (isset($_POST['copyCurrentMenu']) && $_POST['copy_menu_id']>0 && @$_POST['protectOfCopyCurrentMenu']==1){
	foreach ($mi_struct[$_REQUEST['id']]['sub'] as $key => $value) {

		$sub = $value['sub'];
		unset($value['sub']);
		unset($value['mi_id']);
		$value['mi_parent_id'] = $_POST['copy_menu_id'];
		$id = $sql->insert('_menu_items', $value, '', FALSE, TRUE);

		if (!empty($sub)) {
			insert_submenu($sub, $id);
		}
	}
	$PData->content('Меню успешно скопировано', 'message', TRUE);
}

function insert_submenu($struct, $parent_id)
{
	global $sql;

	foreach ($struct as $key => $value) {
		$sub = $value['sub'];
		unset($value['sub']);
		unset($value['mi_id']);
		$value['mi_parent_id'] = $parent_id;
		$id = $sql->insert('_menu_items', $value, '', FALSE, TRUE);

		if (!empty($sub)) {
			insert_submenu($sub,$id);
		}
	}
}


function mi_get_submenus($struct)
{
	$text = '';
	foreach ($struct as $item) {//if have submenu add too
		$text .= '<li class="route">

			<div class="menu_line">
			<input type="hidden" name="mi_id" value="' . $item['mi_id'] . '">
			<input type="text"  class="" name="mi_title" value="' . $item['mi_title'] . '">
			<input name="mi_url"  class="" type="text" value="' . $item['mi_url'] . '">
				<div onclick="delete_menu(this,' . $item['mi_id'] . ');" class="btn btn-danger glyphicon glyphicon-remove"> </div>

				</div>
			<span class="glyphicon glyphicon-move" aria-hidden="true"></span>
			<ul class="space">
			<div name="start-sub"></div>
				';
		if (count($item['sub']) > 0) {
//                foreach ($item['sub'] as $sub) {
			$text .= mi_get_submenus($item['sub']);
			//              }
		}
		$text .= '
<div name="end-sub"></div>
</ul>
		</li>';
	}
	return $text;
}

function get_main_menu_items($id)
{
	global $mi_struct;
	$text = mi_get_submenus($mi_struct[$id]['sub']);
	return $text;
}


$PData->content(_lang('Редактор меню'), 'title');
$PData->content('
<form class="list" method="POST">
	<div class="col-lg-6 col-md-6 col-sm-12">
		<input type="hidden" name="menu[mi_id]" class="input-xlarge" value="' . @$edited_menu['mi_id'] . '"/>
		<p>
			<label>' . _lang('Название') . '</label>
			<input type="text" name="menu[mi_title]" class="input-xlarge" value="' . @$edited_menu['mi_title'] . '"/>
		</p>
		<p>
			<label>' . _lang('Артикул') . '</label>
			<input type="text" name="menu[mi_url]" class="input-xlarge" value="' . @$edited_menu['mi_url'] . '"/>
		</p>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12">
		<p>
			<label>' . _lang('Скопировать меню в') . ':</label>
			<select name="copy_menu_id">
				<option value="0">- '._lang('Не копировать').' -</option>
				'.$menu_select_list.'
			</select>
			<span class="grey">'._lang('Если в меню, в которое вы хотите скопировать, уже заполнено, то текущие пункты добавляться к уже существующим').'</span>
		</p>
		<p>
		<input class="button" type="submit" value="' . _lang('Копировать') . '" name="copyCurrentMenu"/>
		<label>' . _lang('Я точно хочу скопировать меню') . '
			<input type="checkbox" value="1" name="protectOfCopyCurrentMenu"/></label>


		</p>
	</div>
	<br><br>
	<div class="col-lg-12 col-md-12 col-sm-12 menu_container">
		<ul class="space first-space" id="space0">
		' . get_main_menu_items($edited_menu['mi_id']) . '
		</ul>
	</div>

	<input type="hidden" name="menu-structure" value=""/>
	<input type="hidden" name="menu-delete" value=\'{"id":[]}\'/>

	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="btn btn-info route_add" onclick="add_menu_item()">' . _lang('Добавить елемент') . '</div>
		<br/>
		<input class="button" type="submit" onmouseover="struct()" value="' . _lang('Сохранить') . '" name="menuNewSubmit"/>
	</div>
</form>

	<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css"/>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<style>
		.menu_container {
			position: relative;
			background: #eee;
			display:table;
		}
		.mi_placeholder{
		border 2px ;
		border-style: dashed;
		border-radius: 2px;
		}

		.route {
			position: relative;
			list-style-type: none;
			border: 0;
			margin: 0;
			padding: 0;
			top: 0px;
			margin-top: 0px;
			max-height: 100% !important;
			width: 100%;
			background: #d2d2d2;
			border-radius: 2px;
			z-index: -1;
		}

		.route span {
			cursor:pointer;
			position: absolute;
			top: 10px;
			left: 15px;
			transform: scale(1.5);
			z-index: 10;
		}


		.first-title {
			margin-left: 10px;
		}

		.space {
			position: relative;
			list-style-type: none;
			border: 0;
			margin: 0;
			padding: 0;
			margin-left: 70px;
			width: 60px;
			top: 68px;
			padding-bottom: 68px;
			z-index: 1;
		}

		.space input {
			position: relative;
			margin-left: 50px;
			height:30px;
		}
		.first-space {
			margin-left: 10px;
			top: 0px;
		}
		.menu_line{
			display: inline-flex;
			position: absolute;
			border: 0;
			padding-top: 0px;
			height: 30px;
			width: 700px;
			background: #a5a5a5;
			border-radius: 4px;
			box-shadow: 0px 0px 0px 2px #979797;
		}
	</style>
<script>
	//собрать структуру и записат в JSON обект
	//todo нужно отрефакторить
	function struct() {
		var el = $("[name|=start-sub], [name|=end-sub], .container [name|=mi_id], .container [name|=mi_title], .container [name|=mi_url]");
		var level=0;//уровень на котором елементы
		var level_id=[' . $edited_menu['mi_id'] . '];//масив ид родителей

		var level_counter=[0];
		var tmp_item={};//временый обект пункт меню

		var items=[];//масив елементов
		var last_id;//ид последнего елемента нужно знать при спеске глубже на уровень
		tmp_item["mi_parent_id"]=level_id[level];//записываем ид родителя
//считать елементы и записывать при смене уровня вниз запоминать счлетчик в отдельном масиве и сбро с на 0
//при подеме загружать слечтчик с масива
		tmp_item["mi_sort"]=level_counter[level];
		for (var i = 0; i < el.length; i++) {
		if("start-sub"==$(el[i]).attr("name")){//начало субменю опускаемся на уровень глубже
			level++;
			level_id[level]=last_id;
			tmp_item["mi_parent_id"]=level_id[level];
			level_counter[level]=0;
			tmp_item["mi_sort"]=level_counter[level];
			continue;
		}
		if("end-sub"==$(el[i]).attr("name")){//конец субменю выходим на уровень выше
			level--;
			tmp_item["mi_parent_id"]=level_id[level];
			tmp_item["mi_sort"]=level_counter[level];
			continue;
		}

		  //console.log(level_id);

		  //tmp_item["mi_parent_id"]=level_id[level];

		  tmp_item[$(el[i]).attr("name")]=$(el[i]).val();//переписываем значение параметров

		  //console.log(level + $(el[i]).attr( "name" )+"="+$(el[i]).val());

		  if("mi_url"==$(el[i]).attr("name")){//юрл поледний из параметров поетому после его нахожденния записываем сформировавшыйся обект в масив
			//console.log(tmp_item);
			last_id=tmp_item["mi_id"];
			items.push(tmp_item);
			level_counter[level]++;
			tmp_item={};
			tmp_item["mi_sort"]=level_counter[level];
			tmp_item["mi_parent_id"]=level_id[level];
		  }
		}
		 //console.log(items);
		 //console.log(JSON.stringify(items));
		 $("[name=menu-structure]").attr("value",JSON.stringify(items));//преобразуем в JSON и записываем в переменую
	}

var add_id=' . $max_m_id . '
</script>
	<script src="' . HOMEPAGE . '_applications/_modules/menus/form.js">    </script>
');