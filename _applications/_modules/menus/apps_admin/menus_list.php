<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 29.05.2015
 * Time: 13:32
 */
global $sql;


if (isset($_POST['menuNewSubmit'])) {
    $save_menu['mi_title'] = _protect($_POST['menu']['mi_title']);
    $save_menu['mi_url'] = _protect($_POST['menu']['mi_url']);

    $query = "INSERT INTO  _menu_items SET `mi_title`='".$save_menu['mi_title']."', `mi_url`='".$save_menu['mi_url']
        ."'";
    $sql->query($query, $type = 'query', $error = FALSE);
    $PData->content(_lang('Запись добавлена'),'message',TRUE);
}

//Delete is selected
if(isset($_POST['postDeleteSubmit']) && isset($_POST['_post'])) {
    foreach($_POST['_post'] as $key=>$value){
        $query="DELETE FROM _menu_items WHERE  `mi_id`=$key;";//todo переписать
        $sql->query($query,$type='query',$error=FALSE);
        $PData->content(_lang('Запись удалена'),'message',TRUE);
    }
}

//структура меню сформирована
$query = "SELECT * FROM _menu_items";
$result = $sql->query($query, $type = 'array', $error = FALSE);

$mi_struct = array();//menus structure
foreach ($result as $val) {
    $mi_struct[$val['mi_id']] = $val;// new MenuItem($val['mi_id'],$val['mi_parent_id'],$val['mi_title'],$val['mi_url'],$val['mi_sort'],$val['mi_lang']);
    $mi_struct[$val['mi_id']]['sub'] = array();
}

foreach ($mi_struct as $val) {
    if (!empty($val['mi_parent_id'])) {
        $mi_struct[$val['mi_parent_id']]['sub'][$val['mi_id']] = $mi_struct[$val['mi_id']];
        $mi_struct[$val['mi_id']] =& $mi_struct[$val['mi_parent_id']]['sub'][$val['mi_id']];
    }
}
//структура меню сформирована

$text = '';
if (is_array($result)) {
    foreach ($result AS $mkey => $mval) {
        if(empty($mval['mi_parent_id']))
            $text .= '<tr>
			<td class="td-link">
				<input title="' . _lang('выбрать') . '" type="checkbox" name="_post[' . $mval['mi_id'] . ']"/>
			</td>
			<td class="td-link"><a title="' . _lang('Редактировать') . '" href="' . getURL('admin', 'menus/edit', 'id=' . $mval['mi_id']) . '">' . $mval['mi_title'] . '</a></td>
			<td class="td-link">' . $mval['mi_url'] . '</td>
		</tr>';
    }
}

$img_dir = SITE_FOLDER . 'media/_admin/';

$PData->content(_lang('Меню'),'title');
$PData->content('
<div class="add-new">
        <a href="' . getURL('admin', 'menus/add') . '">
        <div class="item">
                <img src="' . $img_dir . 'img/add-new.png" alt="add-new">
                <p>' . _lang('Добавить Меню') . '</p>
        </div>
        </a>
      </div>

	<div class="line"></div>

	<!--<form class="list">
					<label>' . _lang('Поиск по коду') . ':</label>
					<input type="hidden" name="admin" value="' . $PData->_GET('admin') . '"/>
					<input type="text" placeholder="' . _lang('Поиск') . '..." name="filter" value="' . $PData->_GET('filter') . '"/><br/>
					<input type="submit" class="button" value="' . _lang('Искать') . '"/>
	</form>-->

	<div class="line"></div>

		<form id="tableEdit" method="POST">
		<div class="table-holder">
		    <table id="tableEdit" class="table">
		      <thead>
		        	<th class="table-10"> </th>
					<th class="table-40">' . _lang('Название') . '</th>
					<th class="table-40">' . _lang('Артикул') . '</th>
		      </thead>
		      <tbody>
		        	' . $text . '
		      </tbody>
		    </table>
        </div>
		    <input class="button" type="submit" value="' . _lang('Удалить') . '" name="postDeleteSubmit">
		</form>

');