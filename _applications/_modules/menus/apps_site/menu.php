<?php
/**
 * User: Ryllaz
 * Date: 14.07.2015
 * Time: 13:25
 */

/**
 * Echoes menu in template .../module-name/site/main_menu
 * @param string $str Params
 * <code>
 * <?php PrintMenu('key=homepage'); ?>
 * </code>
 * @return void
 */
//// [:PrintMenu key=homepage&template=menu:]
function PrintMenu($str) {
    global $sql, $PData, $menu_items;
    parse_str($str, $vars);
    if (!isset($vars['template'])) $vars['template'] = 'main_menu';
    $menu_items_array = $sql->query("SELECT * FROM _menu_items ORDER BY mi_parent_id DESC, mi_sort ASC");
    $menu_items = array();
    $parent_id = 1;
    $data = array();
    foreach ($menu_items_array as $item) {
        $menu_items[$item['mi_id']] = array(
            'parent' => $item['mi_parent_id'],
            'url' => $item['mi_url'],
            'title' => $item['mi_title'],
            'sort' => $item['mi_sort']
        );
        if ($item['mi_url'] == $vars['key'] || (isset($vars['id']) && $item['mi_id'] == $vars['id'])) {
            $parent_id = $item['mi_id'];
        }
    }
    $data = fill_menu_index($parent_id);
    // print_r($data);
    // $data = isset($data['children']) ? $data['children'] : array();
    return $PData->getModTPL('site/'.$vars['template'],'menus', $data);
}


/**
 * Returns recursive menu items array
 * @param int $id ID menu
 * @param bool $menu_array Contains current menu element in recursive. DO NOT TOUCH.
 * @return array
 */
function fill_menu_index($id, $menu_array = false) {
    global $menu_items;

    $menu_array = $menu_items[$id];
    $pos = substr_count(PAGE_URL,$menu_array['url']);
    if($pos==1){
        $menu_array['active']=true;
    }else{
        $menu_array['active']=false;
    }
    foreach ($menu_items as $key => $item) {
        if ($item['parent'] == $id) {
            $menu_array['children'][$key] = fill_menu_index($key);
        }
    }
    return $menu_array;
}

