<?php $this_page = $_SERVER["REQUEST_URI"]; ?>
<p id="menu-button"><a href="#">���� <i class="fa fa-angle-double-down"></i></a></p>
<ul id="menu" class="lvl-1">
	<?php if (isset($vars_array['children'])) foreach ($vars_array['children'] as $item) { ?>
		<?php if (isset($item['children'])) { ?>
			<li class="has-sub <?php if ($this_page == $item['url']) echo 'open active'; ?>" open-data="1">
				<a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
				<ul class="lvl-2">
					<?php foreach ($item['children'] as $child) { ?>
						<?php if (isset($child['children'])) { ?>
							<li class="has-sub <?php if ($this_page == $child['url']) echo 'open active'; ?>" open-data="2">
								<a href="<?php echo $child['url']; ?>"><?php echo $child['title']; ?></a>
								<ul class="lvl-3">
									<?php foreach ($child['children'] as $child2) { ?>
										<?php if (isset($child2['children'])) { ?>
											<li class="has-sub <?php if ($this_page == $child2['url']) echo 'open active'; ?>" open-data="3">
												<a href="<?php echo $child2['url']; ?>"><?php echo $child2['title']; ?></a>
												<ul class="lvl-4">
													<?php foreach ($child2['children'] as $child3) { ?>
														<li class="<?php if ($this_page == $child3['url']) echo 'open active'; ?>" open-data="4"><a href="<?php echo $child3['url']; ?>"><?php echo $child3['title']; ?></a></li>
													<?php } ?>
												</ul>
											</a>
										<?php } else { ?>
											<li class="<?php if ($this_page == $child2['url']) echo 'active'; ?>" open-data="3"><a href="<?php echo $child2['url']; ?>"><?php echo $child2['title']; ?></a></li>
										<?php } ?>
									<?php } ?>
								</ul>
							</li>
						<?php } else { ?>
							<li class="<?php if ($this_page == $child['url']) echo 'active'; ?>" open-data="2"><a href="<?php echo $child['url']; ?>"><?php echo $child['title']; ?></a></li>
						<?php } ?>
					<?php } ?>
				</ul>
			</li>
		<?php } else { ?>
			<li class="<?php if ($this_page == $item['url']) echo 'active'; ?>" open-data="1"><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></li>
		<?php } ?>
	<?php } ?>
</ul>

<?php

	global $user;
	
	switch ($user->lang) {
		case 'ru':
			$title = '������';
			$link = '/new-site/ru/uslugi/';
		break;
		case 'ua':
			$title = '�������';
			$link = '/new-site/ua/uslugi-1/';
		break;
		case 'en':
			$title = 'Services';
			$link = '/new-site/en/uslugi-2/';
		break;
	}
?>

<script>

$(document).ready(function() {
	
	var type = $('#menu li.active').attr('open-data');
	var target = $('#menu li.active');
	var child = $('#menu li.active');
	
	switch (type) {
		
	    case '1':
		
			// bred lvl - 1
			var bread_title;
			bread_title = $('#page-title').html();
			$('#bread-maker').append('<li id="page-select" class="no-line">' + '<a>' + bread_title + '</a>' + '</li>');
			
			// bottom form
			$('#bottom-form').addClass('hidden');
			
		break
  
		default:
			
			// bred lvl - 1
			var bread_title;
			bread_title = $('#page-title').html();
			$('#bread-maker').append('<li id="page-select" class="no-link">' + '<a>' + bread_title + '</a>' + '</li>');
			
			// bottom form
			$('#bottom-form').addClass('hidden');
	}
	
	// action select
	var action_select = $('#action-select a').attr('href');
	
	if ( 
		action_select == '/new-site/ru/yuridicheskie-uslugi/' ||
		action_select == '/new-site/ua/yuridicheskie-uslugi-1/' ||
		action_select == '/new-site/en/yuridicheskie-uslugi-2/'
		) {	
		$('#law-action').removeClass('hidden');
	} else {
		$('#buh-action').removeClass('hidden');
	}
	
});	

</script>