<?php global $user;?>

<?php 
	switch ($user->lang) {
		case 'ru':
			$collapse = 'Меню';
			$callback = 'Заказать звонок';
		break;
		case 'ua':
			$collapse = 'Меню';
			$callback = 'Замовити дзвінок';
		break;
		case 'en':
			$collapse = 'Menu';
			$callback = 'Callback';
		break;
	}
?>
<div class="navbar-header">
	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" id="collapse-fix">
	  <span class="sr-only">Navbar</span>
	  <span id="menu-text"><?=$collapse;?></span>
	</button>
	<a class="navbar-brand" href="/new-site/"><img src="<?php echo SITE_FOLDER; ?>/templates/img/logo.jpg" alt="logo"><span class="hidden" id="sk">+38 044 237 7276</span></a>
    <li id="number-hidden" class="hidden">+38 044 237 7276<span><a href="#" class="callback" data-toggle="modal" data-target="#callback"><?=$callback?></a></span></li>
 </div>

<div class="collapse navbar-collapse" id="navbar">
    <ul class="nav navbar-nav navbar-left">

	<?php if (isset($vars_array['children'])) foreach ($vars_array['children'] as $item) { ?>
		<?php if (isset($item['children'])) { ?>
		
			<li class="has-child">
				<a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
				<div class="sub-menu-holder">
                    <div class="sub-menu">
						<?php foreach ($item['children'] as $child) { ?>
							<?php if (isset($child['children'])) { ?>
							<div class="item">
								<ul>
									<li class="main"><a href="<?php echo $child['url']; ?>"><?php echo $child['title']; ?></a></li>
									<?php foreach ($child['children'] as $child2) { ?>
										<li><a href="<?php echo $child2['url']; ?>"><?php echo $child2['title']; ?></a></li>
									<?php } ?> 
								</ul>
							</div>
							<?php } else { ?>
								<div class="item">
									<ul>
										<li class="main"><a href="<?php echo $child['url']; ?>"><?php echo $child['title']; ?></a></li>
									</ul>
								</div>
							<?php } ?>
						<?php } ?> 
					</div>
				</div>
			</li>
						
		<?php } else { ?>
			<li><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></li>
		<?php } ?> 
	<?php } ?> 
	
	</ul>
	<ul class="nav navbar-nav navbar-right">
	   <li class="number">+38 044 237 7276<span><a href="#" class="callback" data-toggle="modal" data-target="#callback"><?=$callback?></a></span></li>
	</ul>
</div>
