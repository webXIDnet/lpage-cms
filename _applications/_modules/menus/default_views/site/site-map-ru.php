<ul class="site-map">
	<li>
		<a href="/new-site/ru/uslugi/">Услуги</a>
		<ul>
			<?php if (isset($vars_array['children'])) foreach ($vars_array['children'] as $item) { ?>
				<?php if (isset($item['children'])) { ?>
					<li>
						<a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
						<ul>
							<?php foreach ($item['children'] as $child) { ?>
								<?php if (isset($child['children'])) { ?>
									<li>
										<a href="<?php echo $child['url']; ?>"><?php echo $child['title']; ?></a>
										<ul>
											<?php foreach ($child['children'] as $child2) { ?>
												<?php if (isset($child2['children'])) { ?>
													<li>
														<a href="<?php echo $child2['url']; ?>"><?php echo $child2['title']; ?></a>
														<ul>
															<?php foreach ($child2['children'] as $child3) { ?>
																<li><a href="<?php echo $child3['url']; ?>"><?php echo $child3['title']; ?></a></li>
															<?php } ?>
														</ul>
													</a>
												<?php } else { ?>
													<li><a href="<?php echo $child2['url']; ?>"><?php echo $child2['title']; ?></a></li>
												<?php } ?>
											<?php } ?>
										</ul>
									</li>
								<?php } else { ?>
									<li><a href="<?php echo $child['url']; ?>"><?php echo $child['title']; ?></a></li>
								<?php } ?>
							<?php } ?>
						</ul>
					</li>
				<?php } else { ?>
					<li><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></li>
				<?php } ?>
			<?php } ?>
		</ul>
	</li>
	<li><a href="/new-site/ru/klienty/">Клиенты</a></li>
	<li>
		<a href="/new-site/ru/o-kompanii/">О компании</a>
		<ul>
			<li><a href="/new-site/ru/vakansii/">Вакансии</a></li>
			<li><a href="/new-site/ru/priznanie/">Признание</a></li>
		</ul>
	</li>
	<li><a href="/new-site/ru/contact_ru/">Контакты</a></li>
</ul>