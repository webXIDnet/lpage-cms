jQuery(document).ready(function () {
    calcWidth(jQuery('#title0'));
    window.onresize = function (event) {
        //console.log("window resized");
        //method to execute one time after a timer
    };
//recursively calculate the Width all titles
    function calcWidth(obj) {
        //console.log('---- calcWidth -----');
        var titles = jQuery(obj).siblings('.space').children('.route').children('.title');

        jQuery(titles).each(function (index, element) {
            var pTitleWidth = parseInt(jQuery(obj).css('width'));
            var leftOffset = parseInt(jQuery(obj).siblings('.space').css('margin-left'));
            var newWidth = pTitleWidth - leftOffset;
            if (jQuery(obj).attr('id') == 'title0') {
                //console.log("called");
                newWidth = newWidth - 10;
            }
            jQuery(element).css({
                'width': newWidth
            })
            calcWidth(element);
        });
    }

    jQuery('.space').sortable({
        connectWith: '.space',
        tolerance: 'intersect',
        over: function (event, ui) {
        },
        receive: function (event, ui) {
            calcWidth(jQuery(this).siblings('.title'));
        }
    });

    jQuery('.route span').mousedown(function () {
        console.log(1);
        jQuery('.menu_container').css("height", jQuery('.menu_container').height());
    });
    jQuery('.route span').mouseup(function () {
        jQuery('.menu_container').removeAttr("style")
    });
});

function add_menu_item() {
    add_id++;
    jQuery("#space0").append('<li class="route route_add">\n    <div class="menu_line"><input name="mi_id" value="' + add_id + '" type="hidden">\n        <input class="form-control" name="mi_title" value="new_menu" type="text">\n        <input class="form-control" name="mi_url" value="new_url" type="text">\n\n        <div onclick="delete_menu(this,4);" class="btn btn-danger glyphicon glyphicon-remove route_remove"></div>\n    </div>\n    <span class="glyphicon glyphicon-move" aria-hidden="true"></span>\n    <ul class="space ui-sortable">\n        <div name="start-sub"></div>\n        <div name="end-sub"></div>\n    </ul>\n</li>\n');

    jQuery('.space').sortable({
        connectWith: '.space',
        tolerance: 'intersect',
        over: function (event, ui) {
        },
        receive: function (event, ui) {
            calcWidth(jQuery(this).siblings('.title'));
        }
    });
}

function delete_menu(e, id) {
    var mi_del = JSON.parse(jQuery("[name=menu-delete]").val());
    mi_del["id"].push(id);
    jQuery("[name=menu-delete]").val(JSON.stringify(mi_del));
    jQuery(e).parent().parent().remove();
}

