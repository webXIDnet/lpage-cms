<?php 

if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
global $admin, $meta, $PData;


$r = 0;
$i = 0;
$j = 1;

if($md_key = $meta->all_values('menu')){
	$md_key = max(array_keys($md_key)) + 1;
}else{
	$md_key = 0;
}


if(isset($_POST['menuNewSubmit'])){
// print_r($_POST['menu']['items']);exit;
// print_r($_POST['menu']);
// 	$menu_array = array();
	foreach ($_POST['menu']['items'] as $key => $val) {
// print_r($val);exit;
		if( is_array($val) ) {
			foreach ($val as $key2 => $val2) {
				$menu_array[$key][$key2] = $val2;
				// print_r($menu_array[$key][$key2]);exit;
			}
		}
	}

	foreach ($menu_array as $key => $val) {
		$sort = $val['sort'];
		unset($val['sort']);
		unset($menu_array[$key]);
		$menu_array['items'][$sort.'_'.$key] = $val;
	}


$menu_array['menu_title'] = $_POST['menu']['menu_title'];
	
// echo '<pre>';
// print_r($menu_array);exit;
	$meta->add_update($md_key, $menu_array, 'menu');
	header ("Location: /public_html/admin/menu-add/");
}


	$form = '<script type="text/javascript" src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
		<form class="list" method="POST">
			<p>
				<label>'._lang('Заголовок').'</label>
				<input type="text" class="input-xlarge" name="menu[menu_title]" value="'.@$_POST['menu']['title'].'"/>
			</p><hr>
			<p>
				<label>'._lang('Группы ключевых слов').'</label>

				<table id="tableEdit" class="table answer-table">
					<thead>
						 <tr>
							  <th style="" class="head head0">№</th>
							  <th style="width:200px;" class="head head0">'._lang('Название пунктов').'</th>
							  <th style="width:200px;" class="head head0">'._lang('URL').'</th>
							  <th style="width:200px;" class="head head0">'._lang('Атрибуты').'</th>
							  <th style="width:200px;" class="head head0">'._lang('Сортировка').'</th>
						 </tr>
						 <tr class="answer">
							<td class="number"> '.$j.' </td>         
							<td>
								<input type="text" class="input-xlarge tits" name="menu[items][1][title]" value="'.@$_POST['menu']['title'].'"/>
							</td>
							<td>
								<input type="text" class="input-xlarge keys" name="menu[items][1][url]" value="'.@$_POST['menu']['url'].'"/>
							</td>
							<td>
								<input type="text" class="input-xlarge keys" name="menu[items][1][atribute]" value="'.@$_POST['menu']['atribute'].'"/>
							</td>
							<td>
								<input type="text" class="input-xlarge tits" name="menu[items][1][sort]" value="'.@$_POST['menu']['sort'].'"/>    
							</td>
							<td>
								<input class="btn btn-primary del-answer" type="button" value="'._lang('Удалить').'"/>
							</td>
						</tr>
						
					</thead>
					<tbody id="slides">
						<tr class="add-answer-button">
							<td colspan = 5>
								<p>
									<input class="btn btn-primary add-answer" type="button" value="'._lang('Добавить пункт меню').'"/>
								</p>
							</td>
						</tr>
					</tbody>
				</table><hr>
			</p>

			<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="menuNewSubmit"/>
		</form>';



$PData->content($form);