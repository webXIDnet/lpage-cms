	<?php 

if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

global $meta, $PData;

// function sort_menu($min, $max)
// {
//     return $min['sort'] - $max['sort'];
// }

if(isset($_POST['postDeleteSubmit']) && isset($_POST['_post'])) {
	if ($meta->delete($_POST['_post'], 'menu')) {
		$PData->content(_lang('Запись удалена'),'message',TRUE);
	}
}

$result = $meta->all_values('menu');
// usort($result, "sort_menu");
$text ='';

if (is_array($result)) {
	
	foreach($result as $mkey=>$mval) {
		
		
		if (!isset($_REQUEST['filter']) || !$_REQUEST['filter'] || (strpos($mkey,$_REQUEST['filter']) !== false)) {
			$text.='<tr>
						<td class="col0 tableList0">
							<input title="'._lang('выбрать').'" type="checkbox" name="_post['.$mkey.']"/>
						</td>
						<td class=col2 tableList2 col"><a title="'._lang('Редактировать').'" href="'.getURL('admin','menu-edit','id='.$mkey).'">'.$mkey.'</a></td>
						<td class="col1 tableList1 col"><a title="'._lang('Редактировать').'" href="'.getURL('admin','menu-edit','id='.$mkey).'">'.$result[$mkey]['menu_title'].'</a></td>
						<td class="col1 tableList1 col"><a title="'._lang('Редактировать').'" href="'.getURL('admin','menu-edit','id='.$mkey).'">'._lang('Редактировать').'</a></td>
					</tr>';
		}

	


		$view = '<a href="'.getURL('admin','menu-add').'" class="boxPic add_post">
					'._lang('Добавить Меню').'
				</a>
				<div class="clear"></div><hr/>

				<form class="list">
					<table width="100%">
						<tr>
							<td>
								<label>'._lang('Поиск по коду').':</label> 
								<input type="hidden" name="admin" value="'.@$_GET['admin'].'"/>
								<input type="text" name="filter" value="'.@$_GET['filter'].'"/><br/>
								<input type="submit" class="btn" value="'._lang('Искать').'"/>
							</td>
						</tr>
					</table>
				</form><hr/>

				<div class="well">
					<form id="tableEdit" class="table" method="POST">
					    <table id="tableEdit" class="table">
					      <thead>
					        <tr>
					        	<th class="head head0 tableList0">&nbsp;</th>
								<th class="head head0 tableList0">'._lang('ID menu').'</th>
								<th class="head head0 tableList0">'._lang('Название меню').'</th>
								<th class="head head0 tableList0">'._lang('Редактировать').'</th>	
					        </tr>
					      </thead>
					      <tbody>'.$text.'</tbody>
					    </table>
				    	<input style="float:right;" class="btn btn-primary" type="submit" 
				    		value="'._lang('Удалить').'" name="postDeleteSubmit">
					</form>
				</div>';
	}

}
$PData->content($view);