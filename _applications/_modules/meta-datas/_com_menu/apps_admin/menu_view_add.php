<script type="text/javascript" src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

<form class="list" method="POST">
	<p>
		<label>'._lang('Заголовок').'</label> 
		<input type="text" class="input-xlarge" name="menu[title]" value="'.@$_POST['menu']['title'].'"/>
	</p><hr>
	<p>
		<label>'._lang('Группы ключевых слов').'</label>

	    <table id="tableEdit" class="table answer-table">
			<thead>
			     <tr>
		              <th style="" class="head head0">№</th>
                      <th style="width:200px;" class="head head0">'._lang('Название пунктов').'</th>
				      <th style="width:200px;" class="head head0">'._lang('URL').'</th>
                      <th style="width:200px;" class="head head0">'._lang('Атрибуты').'</th>
                      <th style="width:200px;" class="head head0">'._lang('Сортировка').'</th>
		         </tr>
                 <tr class="answer">
                    <td class="number"> '.$j.' </td>         
                    <td>
                        <input type="text" class="input-xlarge tits" name="menu[tits]['.$i.'][tit]" value=""/>
                    </td>
                    <td>
                        <input type="text" class="input-xlarge keys" name="menu[url]['.$i.'][url]"></input>
                    </td>
                    <td>
                        <input type="text" class="input-xlarge keys" name="menu[atribute]['.$i.'][atribute]" ></input>
                    </td>
                    <td>
                        <input type="text" class="input-xlarge tits" name="menu[sort]['.$i.'][sort]" ></input>    
                    </td>
                    <td>
                        <input class="btn btn-primary del-answer" type="button" value="'._lang('Удалить').'"/>
                    </td>
                </tr>
			</thead>
		    <tbody id="slides">
                <tr class="add-answer-button">
                    <td colspan = 5>
                        <p>
                            <input class="btn btn-primary add-answer" type="button" value="'._lang('Добавить группу').'"/>
                        </p>
                    </td>
                </tr>
            </tbody>
        </table><hr>
    </p>

    <input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="menuNewSubmit"/>
</form>