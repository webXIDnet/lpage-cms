<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 *
 * Модуль Мета тегов
 *
 ***/


/** Формування меню в адмінці  **/

if (PAGE_TYPE == 'admin') {
    _add_filter('adminPanel_sidebar_menu', 'com_meta_tags_admin_sidebar_menu', 30);
}

function com_meta_tags_admin_sidebar_menu($result)
{
    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Мета-теги'), //ім"я пункта меню
            'url' => getURL('admin', 'mtags'), //посилання
            'submenu_filter' => 'com_meta_tags_admin_sidebar_menu', //хук для підключення пунктів підменю
            'active_pages' => _run_filter('com_meta_tags_admin_sidebar_menu_active_items', array('mtags', 'mtags-add')), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => getURL('admin', 'mtags-add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100), $menu);

    return $result . @$menu;
}

_add_action('post_init', 'SetMetaData');
_add_action('page_init', 'SetMetaData');
function SetMetaData($post)
{
    global $meta, $PData;
    $meta_tag = $meta->val(PAGE_URL, 'meta_tag');
    if ($meta_tag) {//если для страиный указаны конкретные метатеги
        $meta->title = $meta_tag['title'];
        $meta->keys = $meta_tag['desc'];
        $meta->desc = $meta_tag['keys'];
    } else {
        $templates = $meta->val('meta_tags_templates', 'mod_option');
        if ($templates) {
            foreach ($templates as $template) {//если есть шаблон
                if ($template['type'] == PAGE_TYPE) {
                    $title = $template['title'];
                    preg_match_all(
                        "/#(.*)#/Uis",
                        $title,
                        $ar
                    );
                    foreach ($ar[1] AS $key => $val) {
                        $title = str_replace($ar[0][$key], $post[$val], $title);
                    }
                    $meta->title = $title;

                    $description = $template['description'];
                    preg_match_all(
                        "/#(.*)#/Uis",
                        $description,
                        $ar
                    );
                    foreach ($ar[1] AS $key => $val) {
                        $description = str_replace($ar[0][$key], $post[$val], $description);
                    }
                    $meta->keys = $description;

                    $keys = $template['keys'];
                    preg_match_all(
                        "/#(.*)#/Uis",
                        $keys,
                        $ar
                    );
                    foreach ($ar[1] AS $key => $val) {
                        $keys = str_replace($ar[0][$key], $post[$val], $keys);
                    }
                    $meta->desc = $keys;
                }
            }
        } else {
            $meta->title = $post['p_title'];//если шаблон и конкретные метатеги отсуцтвуют
        }
    }
    //добавляем теги
    global $taxonomy;
    $tags = $taxonomy->getTagsByPost($post['pid']);
    $tags[] = $meta->keys;
    $meta->keys = implode(', ', $tags);
}

/** Роутер Адмінки **/

_add_action('adminPanel_controller', 'Meta_Tags_admin_controller');

function Meta_Tags_admin_controller()
{

    global $PData,
           $admin;

    switch (@$_GET['admin']) {
        case'mtags':
            require_once __DIR__ . '/apps_admin/metaTags_mtags.php';
            return true;
            break;

        case'mtags-add':
            require_once __DIR__ . '/apps_admin/metaTags_mtags_add.php';
            return true;
            break;
        /*
                case'mtags-edit':
                    require_once __DIR__ . '/apps_admin/metaTags_mtags_edit.php';
                    return true;
                break;
        */

        case'mtags-template-edit':
            require_once __DIR__ . '/apps_admin/metaTags_mtags_template_edit.php';
            return true;
            break;
    }
}
?>