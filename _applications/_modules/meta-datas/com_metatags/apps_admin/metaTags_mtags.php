<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/
global $meta;

if(isset($_POST['postDeleteSubmit']) && isset($_POST['_post'])){

	$where='';

	if ($meta->delete($_POST['_post'], 'meta_tag')){
		$PData->content(_lang('Запись удалена'),'message',TRUE);
	}

}elseif(isset($_POST['formsubmit-edit']) && is_array($_POST['editItem'])){

	if (str_replace('http://', '', $_POST['editItem']['mtag_url'])==$_POST['editItem']['mtag_url']) { $_POST['editItem']['mtag_url'] = 'http://' . $_POST['editItem']['mtag_url']; }

	if ($meta->add_update($_POST['editItem']['mtag_url'], $_POST['editItem']['mtags'], 'meta_tag')){
		$PData->content(_lang('Запись сохранена'),'message',TRUE);
	}
}

// $nav = _pagination('_landing_pages','lp_id',$where);

$result = $meta->all_values('meta_tag');

$text='';
if (is_array($result)){
    $i=0;//modal iterator
	foreach($result AS $mkey=>$mval){
		if ($PData->_GET('filter')=='' || (strpos($mkey, $PData->_GET('filter')) !== false))
		$text.='
<!-- tr item -->
          <tr>
            <td>
              <label>
                <input title="'._lang('выбрать').'" type="checkbox" name="_post['.$mkey.']"/>
              </label>
            </td>
            <td class="td-link" data-toggle="modal" data-target=".meta-modal-'. $i.'">
              '.$mkey.'
            </td>
            <td class="td-link" data-toggle="modal" data-target=".meta-modal-'. $i.'">
              '.$mval['title'].'
            </td>
          </tr>

          <!-- meta modal -->
          <div class="modal fade meta-modal-'.$i.'" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">some title</h4>
                </div>
                <div class="modal-body">
                  <form method="">
                      <p>'._lang('URL страницы').'</p>
                      <input type="text"  name="editItem[mtag_url]" value="'.$mkey.'"/>
                      <p>'._lang('Заголовок').'</p>
                      <input type="text" name="editItem[mtags][title]" value="'.$mval['title'].'"/>
                      <p>'._lang('Описание').'</p>
                      <textarea type="text" name="editItem[mtags][desc]">'.$mval['desc'].'</textarea>
                      <p>'._lang('Ключевые слова').'</p>
                      <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="editItem[mtags][keys]">'.$mval['keys'].'</textarea>
                      <input class="button" name="formsubmit-edit" type="submit" value="Save">
                  </form>
                </div>
              </div>
            </div>
          </div>
		';
	$i++;
    }
}

$img_dir = SITE_FOLDER . 'media/_admin/';

$PData->content('Мета-теги','title');

$PData->content('
    <div class="add-new">
        <div class="item">
          <a href="'.getURL('admin','mtags-add').'">
            <img src="'.$img_dir.'img/add-new.png" alt="add-new">
            <p>'._lang('Добавить мета-тег').'</p>
          </a>
        </div>
        <div class="item">
          <a href="'.getURL('admin','mtags-template-edit').'">
            <img src="'.$img_dir.'img/add-cat.png" alt="add-new">
            <p>'._lang('Редактор мета шаблонов').'</p>
          </a>
        </div>
    </div>

	<div class="line"></div>

	<form class="list">
					<p><b>'._lang('Поиск по странице').':</b></p>
					<input type="hidden"  name="admin" value="'.$PData->_GET('admin').'"/>
					<input type="text" placeholder="'._lang('Поиск').'..." name="filter" value="'.$PData->_GET('filter').'"/>
					<input type="submit" class="button" value="'._lang('Искать').'"/>
	</form>

	<div class="line"></div>

		<form id="tableEdit" class="table-holder" method="POST">
		    <table id="tableEdit" class="table">
		      <thead>
		        	<th class="table-10">&nbsp;</th>
					<th class="table-45">'._lang('Страница').'</th>
					<th class="table-45">'._lang('Заголовок').'</th>
		      </thead>
		      <tbody>
		        	'.$text.'
		      </tbody>
		    </table>
		    <input class="button" type="submit" value="'._lang('Удалить').'" name="postDeleteSubmit">
		</form>
');