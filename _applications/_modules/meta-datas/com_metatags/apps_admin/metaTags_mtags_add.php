<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/
global $meta, $PData;

if(isset($_POST['mtagsNewSubmit']) && isset($_POST['mtag_url'])){
	if (strpos($_POST['mtag_url'], 'http://') === false && strpos($_POST['mtag_url'], 'https://') === false) { $_POST['mtag_url'] = 'http://' . $_POST['mtag_url']; }

	if ($meta->add_update($_POST['mtag_url'], $_POST['mtags'], 'meta_tag')){
		$PData->content(_lang('Запись сохранена'),'message',TRUE);
	}
}

global $list;

$mtag_url = $PData->_POST('mtag_url');

$PData->content(_lang('Создание мета-тега'),'title');

$PData->content('
	<form class="list" method="POST">
			<p>'._lang('URL страницы').'</p>
			<input type="text"  name="mtag_url" value="'.$PData->_POST('mtag_url').'"/>

		    <p>'._lang('Заголовок').'</p>
			<input type="text"  name="mtags[title]" value="'.(isset($mtag_url['title']) ? $mtag_url['title'] : '') .'"/>

			<p>'._lang('Описание').'</p>
			<textarea type="text" name="mtags[desc]">'.(isset($mtag_url['desc']) ? $mtag_url['desc'] : '').'</textarea>

			<p>'._lang('Ключевые слова').'</p>
			<textarea type="text" name="mtags[keys]">'.(isset($mtag_url['keys']) ? $mtag_url['keys'] : '').'</textarea>
		<input class="button" type="submit" value="'._lang('Сохранить').'" name="mtagsNewSubmit"/>
	</form>

');