<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/
global $meta;

if(!isset($_REQUEST['id'])) $PData->redirect(getURL('admin'),'403');

if(isset($_POST['mtagsEditSubmit']) && isset($_POST['mtag_url'])){
	if ($meta->add_update($_POST['mtag_url'], $_POST['mtags'], 'meta')){
		$PData->content(_lang('Запись сохранена'),'message',TRUE);
	}
}else{
	$_POST['mtag_url'] = $_REQUEST['id'];
	$_POST['mtags'] = $meta->val($_REQUEST['id'],'meta');
}

global $list;


$PData->content( _lang('Редактирование мета тега'), 'title' );

$PData->content('
	
	
	
	<form class="list" method="POST">
		<p>
			<label>'._lang('URL страницы').'</label> 
			<input type="text" class="input-xlarge" name="mtag_url" value="'.@$_POST['mtag_url'].'"/>
		</p>
		<p>
			<label>'._lang('Заголовок').'</label> 
			<input type="text" class="input-xlarge" name="mtags[title]" value="'.@$_POST['mtags']['title'].'"/>
		</p>
		<p>
			<label>'._lang('Описание').'</label>
			<textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="mtags[desc]">'.@$_POST['mtags']['desc'].'</textarea> 
		</p>
		<p>
			<label>'._lang('Ключевые слова').'</label>
			<textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="mtags[keys]">'.@$_POST['mtags']['keys'].'</textarea>
		</p>
		
		<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="mtagsEditSubmit"/>
	</form>
	
');