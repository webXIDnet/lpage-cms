<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
 ***/
global $meta;
if (isset($_POST['metaSave'])) {
    $meta->add_update('meta_tags_templates', $_POST['meta'],'mod_option');
}
$tags = $meta->val('meta_tags_templates', 'mod_option');
$i = 0;
$text = '';
if (is_array($tags))
    foreach ($tags as $tag) {
        $text .= '<tr>
		<td><input class=button type=button value=' . _lang('Удалить') . ' onclick=del_row(this)></td>
		<td><input type=text name=meta[' . $i . '][type] 		value="' . $tag['type'] . '"></td>
		 <td><input type=text name=meta[' . $i . '][title] 		value="' . $tag['title'] . '"></td>
		 <td><input type=text name=meta[' . $i . '][description] value="' . $tag['description'] . '"></td>
		 <td><input type=text name=meta[' . $i . '][keys]		value="' . $tag['keys'] . '"></td></tr>';
        $i++;
    }
$PData->content(_lang('Редактирование шаблонов мета тегов страниц'), 'title');
$PData->content('
	<form id="tableEdit" class="table-holder" method="POST">
		    <table id="tableEdit" class="table">
		      <thead>
		        	<th class="table-10">&nbsp;</th>
					<th class="table-20">' . _lang('Тип страницы') . '</th>
					<th class="table-20">' . _lang('Заголовок') . '</th>
					<th class="table-20">' . _lang('Описание') . '</th>
					<th class="table-30">' . _lang('Ключевые слова') . '</th>
		      </thead>
		      <tbody>
		        	' . $text . '
		      </tbody>
		    </table>
		    <input class="button" type="submit" value="' . _lang('Сохранить') . '" name="metaSave">
		    <input class="button" type="button" onclick="add_row()" value="' . _lang('Добавить') . '" name="metaAddRow">
		</form>
		<script type="text/javascript">
		var i=' . $i . ';
		function add_row(){
		newRowContent="<tr>" +
		 "<td><input class=button type=button value=\"' . _lang('Удалить') . '\" onclick=del_row(this)></td>" +
		 "<td><input type=text name=meta["+i+"][type]></td>" +
		 "<td><input type=text name=meta["+i+"][title]></td>" +
		 "<td><input type=text name=meta["+i+"][description]></td>" +
		 "<td><input type=text name=meta["+i+"][keys]></td></tr>";
		jQuery("#tableEdit tbody").append(newRowContent);
		i++;
		}

		function del_row(e){
		console.log(jQuery(e).parent().parent().remove());
		}
		</script>
');