<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента мультиблоки
 *
***/

/** Формування меню "мультиблоки" в адмінці **/

if (PAGE_TYPE=='admin') {
	_add_filter('meta_datas_admin_sidebar_menu_elenemts', 'meta_datas_com_multiblock_admin_sidebar_menu_elenemts',15);
	_add_filter('meta_datas_admin_sidebar_menu_elenemts_active_items', 'meta_datas_com_multiblock_admin_sidebar_menu_elenemts_active_items');
}

function meta_datas_com_multiblock_admin_sidebar_menu_elenemts($result) {

	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Мультиблоки'), //ім"я пункта меню
			'url' => getURL('admin', 'mblocks'), //посилання
			'active_pages' => array('mblocks', 'mblocks-add','mblocks-edit'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => getURL('admin', 'mblocks-add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . $menu;
}

function meta_datas_com_multiblock_admin_sidebar_menu_elenemts_active_items($result)
{//перелік сторінок, для яких має бути активний блок меню Елементи сайту
	$result[] = 'mblocks';
	$result[] = 'mblocks-add';
	$result[] = 'mblocks-edit';
	return $result;
}

/** Додаємо в запит до бд md_type='mblocks' **/

if (PAGE_TYPE=='admin') {
	_add_filter('meta_datas_SQL_query', 'LPage_com_mblocks_AP_meta_datas_SQL_query');
}

function LPage_com_mblocks_AP_meta_datas_SQL_query($result){

	if(!empty($result)) $result .= ' OR ';

	return  $result . " md_type='mblocks' ";
}

/** Роутер Адмінки для мультиблоків **/

if (PAGE_TYPE=='admin') {
	_add_action('adminPanel_controller', 'Mblocks_admin_controller');
}

function Mblocks_admin_controller() {
global $PData, $admin;
	switch(@$_GET['admin']){
		case'mblocks':
			require_once __DIR__ . '/apps_admin/a_multi_blocks.php';
			return true;
		break;

		case'mblocks-add':
			require_once __DIR__ . '/apps_admin/a_multi_blocks_add.php';
			return true;
		break;

		case'mblocks-edit':
			require_once __DIR__ . '/apps_admin/a_multi_blocks_edit.php';
			return true;
		break;
	}

}

/** Формування значення шоткоду [:MultiBlock id=:], в залежності від ютм мітки **/

function MultiBlock($param) {
//    print_r($param);
	if(PAGE_TYPE=='admin') return '[:MultiBlock '.$param.':]';

	$id = explode("=", $param);

	if(isset($id['1'])) {

		global $meta;
		$result = $meta->val($id['1'], 'mblocks');

        if(empty($result)) {
            return _lang('Мультиблок не существует или был удален');
        }

        $utm_term = @$_GET['utm_term'];


        $no  = 0;

        foreach($result['keyss'] as $keyy => $key) {

            if(in_array($utm_term, $key)) {
                return $result['descs'][$keyy]['desc'];
            } else {
                $no++;
            }
         }

         if(!empty($no)) {
            return $result['descdef'];
         }
    }
}