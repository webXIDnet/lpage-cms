<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/
global $meta, $PData;

if(isset($_POST['postDeleteSubmit']) && isset($_POST['_post'])) {

	$where='';

	if ($meta->delete($_POST['_post'], 'mblocks')) {
		$PData->content(_lang('Запись удалена'),'message',TRUE);
	}
}

// $nav = _pagination('_landing_pages','lp_id',$where);

$result = $meta->all_values('mblocks');

$text='';
//var_dump($result[2]);
//var_dump(json_decode($result[2],true));
if (is_array($result)) {
	foreach($result AS $mkey=>$mval) {
		if ($PData->_GET('filter')=='' || (strpos($mkey,$PData->_GET('filter')) !== false) || (preg_match("/^".$PData->_GET('filter')."/", $mval['title']) != false)) {
			$text.='<tr>
				<td class="">
					<input title="'._lang('выбрать').'" type="checkbox" name="_post['.$mkey.']"/>
					'.$mkey.'
				</td>
				<td class="td-link">
                    <a title="'._lang('Редактировать').'" href="'.getURL('admin','mblocks-edit','id='.$mkey).'">
                    '.$mval['title'].'</a>
				</td>
				<td class="td-link">
                    <a title="'._lang('Редактировать').'" href="'.getURL('admin','mblocks-edit','id='.$mkey).'">
                    '.str_replace("\r\n", ", ", count($mval['keyss'])).'</a>
				</td>
			</tr>';
		}
	}
}

$img_dir = SITE_FOLDER . 'media/_admin/';

$PData->content('Мультиблоки','title');

$PData->content('
	<div class="add-new">

        <a href="'.getURL('admin','mblocks-add').'">
        <div class="item">
            <img src="'.$img_dir.'img/add-new.png" alt="add-new">
            <p>'._lang('Добавить Мультиблок').'</p>

        </div>
        </a>
      </div>

    <div class="line"></div>

	<form class="list">
					<p><b>'._lang('Поиск по коду').':</p></b>
					<input type="hidden" name="admin" value="'.$PData->_GET('admin').'"/>
					<input type="text" placeholder="'._lang('Поиск').'..." name="filter" value="'.$PData->_GET('filter').'"/>
					<input type="submit" class="button" value="'._lang('Искать').'"/>
	</form>
	<div class="line"></div>
		<form id="tableEdit"  method="POST">
		<div class="table-holder">
		    <table id="tableEdit" class="table">
		      <thead>
		        	<th class="table-10"">'._lang('Код').'</th>
					<th class="table-60"">'._lang('Название мультиблока').'</th>
					<th class="table-30">'._lang('Количество групп').'</th>
		      </thead>
		      <tbody>
		        	'.$text.'
		      </tbody>
		    </table>
		    </div>
		    <input class="button" type="submit" value="'._lang('Удалить').'" name="postDeleteSubmit">

		</form>
	</div>
');