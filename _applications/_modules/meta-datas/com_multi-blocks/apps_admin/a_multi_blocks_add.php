<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/
global $meta, $PData;

$mblocks = $PData->_POST('mblocks');

$form = '<script type="text/javascript" src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

	<form class="list" method="POST">
		<p>
			<label>'._lang('Заголовок').'</label>
			<input type="text" class="input-xlarge" name="mblocks[title]" value="'. (isset($mblocks['title']) ? $mblocks['title'] : '' ) .'"/>
		</p>

		<p style="height:300px;">
			<label>'._lang('HTML код или текст по умолчанию').'</label>
            ' . $admin->getWIZIWIG('mblocks[descdef]', (isset($mblocks['title']) ? $mblocks['descdef'] : '' ), 'descdef','code') . '
		</p>

        <hr>
		<p>
			<label>'._lang('Группы ключевых слов').'</label>
			<div class="large-table-holder">
		    <table id="tableEdit" class="table multiblock">
			<thead>
		        <th class="table-10">№</th>
                <th class="table-20">'._lang('Название группы').'</th>
				<th class="table-30">'._lang('Ключевые слова <br>(*по одному в строке)').'</th>
                <th class="table-30">'._lang('HTML код или текст').'</th>
                <th class="table-10"></th>
			</thead>
		    <tbody id="slides">';


$r = 0;
$i = 0;
$j = 1;
//print_r($_POST['mblocks']);
//print_r(json_decode($_POST['mblocks'],true));

if(isset($_POST['mblocksNewSubmit']) && isset($_POST['mblocks']['title'])) {


    foreach($_POST['mblocks']['tits'] as $tits) {

        $form = $form . '<tr class="answer">
                <td class="number">
                    '.$j.'
                </td>
                <td>
                    <input type="text" class="input-xlarge tits" name="mblocks[tits]['.$i.'][tit]" value="'.@$tits['tit'].'"/>
                </td>
                <td>
                    <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge keys" name="mblocks[keys]['.$i.'][key]">'.@$_POST['mblocks']['keys'][$r]['key'].'</textarea>
                </td>
                <td>
                <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge descs" name="mblocks[descs]['.$i.'][desc]">'.@$_POST['mblocks']['descs'][$r]['desc'].'</textarea>

                </td>
                <td>
                    <input class="button" type="button" value="'._lang('Удалить').'"/>
                </td>
            </tr>';
        //          63 string      ' .$admin->getWIZIWIG('mblocks[descs]['.$i.'][desc]', @$_POST['mblocks']['descs'][$r]['desc'], 'desc'.$i,'code') . '
          $r++;
          $j++;
          $i++;
    }

} else {
    $form = $form . '';}

$form = $form . '<tr class="add-answer-button">
            </tr>
            </tbody>
			</table>
			</div>
			<input class="button" onclick="addGroup()" type="button" value="'._lang('Добавить группу').'"/>
			<script>
			var num='.$i.';
              function addGroup(){
                 $("#tableEdit").append(\'\
    <tr class="answer" id="group_\'+num+\'">\
                <td class="number">\
                </td>         \
                <td>\
                    <input type="text" class="input-xlarge tits" name="mblocks[tits][\'+num+ \'][tit]" value=""/>\
                </td> \
                <td>\
                    <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge keys" name="mblocks[keys][\'+num+ \'][key]"></textarea>\
                </td>\
                <td>\
                    <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge descs" name="mblocks[descs][\'+num+ \'][desc]"></textarea>\
                </td>\
                <td>\
                    <input class="button" onclick="delGroup(\'+num+\')" type="button" value="'._lang('Удалить').'"/>\
                </td>\
            </tr>\
\');
num++;
              }
              function delGroup(id){
              $("#group_"+id).remove()

              }

          </script>
		</p>
			<div class="line"></div>
		<input class="button" type="submit" value="'._lang('Сохранить').'" name="mblocksNewSubmit"/>
	</form>';

if(isset($_POST['mblocksNewSubmit']) && isset($_POST['mblocks']['title'])) {

    foreach($_POST['mblocks']['keys'] as $key) {

        $k = explode("\r\n", $key['key']);
        $ck1 = array_values(array_unique($k));

        $ck = array_diff($ck1, array(''));

        $_POST['mblocks']['keys'] = array();
        $_POST['mblocks']['keyss'][] = $ck;
    }

    unset($_POST['mblocks']['keys']);



    $array = array();
    $i = 0;
    foreach($_POST['mblocks']['keyss'] as $keyy => $key) {

        for($u = 0; $u < count($key); $u++) {
            $array[$i] = $key[$u];
            $i++;
        }

    }

    $array_new = array_count_values($array);
    $et = 0;
    $t = _lang('Следующие ключевые слова одинаковые: ');
    while (list($key, $val) = each($array_new)) {
        if($val >= 2) {
            $t = $t . $key . ' ';
            $et++;
        }
    }
}

if(isset($_POST['mblocksNewSubmit']) && isset($_POST['mblocks']['title'])) {
    $meta->mblocks = $meta->all_values('mblocks');
    $mblocksMax=0;
    foreach ($meta->mblocks as $key => $data ){
        if($key>$mblocksMax)
            $mblocksMax = $key;
    }

	if ($meta->add_update($mblocksMax+1, $_POST['mblocks'], 'mblocks')) {
		$PData->content(_lang('Запись сохранена.').' <a href="'.getURL('admin','mblocks-edit','id='.$mblocksMax + 1).'">'._lang('Добавить мультиблоки').'</a>','message',TRUE);
	}
}

global $list;

$PData->content(_lang('Создание мультиблока'),'title');

if(!empty($et)) {
    $PData->content($t, 'message', TRUE);
}


$PData->content($form);