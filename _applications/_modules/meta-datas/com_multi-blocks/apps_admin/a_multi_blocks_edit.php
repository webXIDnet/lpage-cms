<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/
global $meta;

if(!isset($_REQUEST['id'])) $PData->redirect(getURL('admin'),'403');

if(isset($_POST['mblocksEditSubmit']) && isset($_POST['mblocks']['title'])) {

    foreach($_POST['mblocks']['keys'] as $key=>$value) {

        $k = explode("\r\n", $value['key']);
        $ck1 = array_values(array_unique($k));

        $ck = array_diff($ck1, array(''));

        $_POST['mblocks']['keys'] = array();
        $_POST['mblocks']['keyss'][$key] = $ck;
    }

    unset($_POST['mblocks']['keys']);

    $array = array();
    $i = 0;
    foreach($_POST['mblocks']['keyss'] as $keyy => $key) {

        for($u = 0; $u < count($key); $u++) {
            $array[$i] = $key[$u];
            $i++;
        }

    }

    $array_new = array_count_values($array);
    $et = 0;
    $t = _lang('Следующие ключевые слова одинаковые: ');
    while (list($key, $val) = each($array_new)) {
        if($val >= 2) {
            $t = $t . $key . ' ';
            $et++;
        }
    }

	if ($meta->add_update($_REQUEST['id'], $_POST['mblocks'], 'mblocks')){
		$PData->content(_lang('Запись сохранена'),'message',TRUE);
        if(!empty($et)) {
            $PData->content($t, 'message', TRUE);
        }
	}
} else {
//    $_POST['mblocks'] = $meta->val($_REQUEST['id'],'mblocks');
}
$block_raw = $meta->val($_REQUEST['id'],'mblocks');
//$b = 0;
$blocks; //bloks array
foreach($block_raw['tits'] as $key=>$value) {
    $block['tits']=$block_raw['tits'][$key]['tit'];
    $block['descs']=$block_raw['descs'][$key]['desc'];
    @$block['keyss']=implode("\r\n", $block_raw['keyss'][$key]);
    $blocks[]=$block;
}
unset ($block);

$PData->content( _lang('Редактирование мультиблока'), 'title' );

$form = '<script type="text/javascript" src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

	<form class="list" method="POST">

        <p>
			<label>'._lang('Шоткод для вставки Мультиблока на страницу').'</label>
			<input type="text" readonly class="input-xlarge" value="[:MultiBlock id='.$_REQUEST['id'].':]"/>
		</p>

		<p>
			<label>'._lang('Заголовок').'</label>
			<input type="text" class="input-xlarge" name="mblocks[title]" value="'.@$block_raw['title'].'"/>
		</p>

		<p style="height:300px;">
			<label>'._lang('HTML код или текст по умолчанию').'</label>
			' . $admin->getWIZIWIG('mblocks[descdef]', @$block_raw['descdef'], 'descdef','code') . '
		</p>

        <hr>
		<p>
			<label>'._lang('Группы ключевых слов').'</label>
		    <table id="tableEdit" class="table answer-table">
			<thead>
			<tr>
		        <th class="head head0">№</th>
                <th class="head head0">'._lang('Название группы').'</th>
				<th class="head head0">'._lang('Ключевые слова (*по одном и сновой строки)').'</th>
                <th class="head head0">'._lang('HTML код или текст').'</th>
		    </tr>
			</thead>
		    <tbody id="slides">';

$r = 0;
$i = 0;
$j = 1;

    foreach($blocks as $block) {
        $form = $form . '<tr class="answer" id="group_'.$j.'">
                <td class="number">
                    '.$j.'
                </td>
                <td>
                    <input type="text" class="input-xlarge tits" name="mblocks[tits]['.$i.'][tit]" value="'.@$block['tits'].'"/>
                </td>
                <td>
                    <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge keys" name="mblocks[keys]['.$i.'][key]">'.@$block['keyss'].'</textarea>
                </td>
                <td>
                    <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge descs" name="mblocks[descs]['.$i.'][desc]">'.@$block['descs'].'</textarea>
                </td>
                <td>
                    <input class="button" onclick="delGroup('.$j.')"  type="button" value="'._lang('Удалить').'"/>
                </td>
            </tr>';
          $r++;
          $j++;
          $i++;
    }


$form = $form . '<tr class="add-answer-button">
            </tr>
            </tbody>
			</table>
			</div>
			<input class="button" onclick="addGroup()" type="button" value="'._lang('Добавить группу').'"/>
			<script>
			var num='.($j-1).';
              function addGroup(){
                 $("#tableEdit").append(\'\
    <tr class="answer" id="group_\'+num+\'">\
                <td class="number">\
                    \'+(num+1)+ \'\
                </td>         \
                <td>\
                    <input type="text" class="input-xlarge tits" name="mblocks[tits][\'+num+ \'][tit]" value=""/>\
                </td> \
                <td>\
                    <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge keys" name="mblocks[keys][\'+num+ \'][key]"></textarea>\
                </td>\
                <td>\
                    <textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge descs" name="mblocks[descs][\'+num+ \'][desc]"></textarea>\
                </td>\
                <td>\
                    <input class="button" onclick="delGroup(\'+num+\')" type="button" value="'._lang('Удалить').'"/>\
                </td>\
            </tr>\
\')
num++
              }
              function delGroup(id){
              $("#group_"+id).remove()

              }
          </script>
		</p>
			<div class="line"></div>
		<input class="button" type="submit" value="'._lang('Сохранить').'" name="mblocksEditSubmit"/>
	</form>';

$PData->content($form);