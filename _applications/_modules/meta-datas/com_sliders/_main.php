<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента слайдер
 *
***/



/** Формування меню в адмінці  **/

if (PAGE_TYPE=='admin') {
	_add_filter('meta_datas_admin_sidebar_menu_elenemts', 'meta_datas_com_sliders_admin_sidebar_menu_elenemts', 11);
	_add_filter('meta_datas_admin_sidebar_menu_elenemts_active_items', 'meta_datas_com_sliders_admin_sidebar_menu_elenemts_active_items');
    _add_filter('admin_panel_header', 'com_sliders_header');
}


function com_sliders_header($result)
{

    $text = "
    <script src='" . SITE_FOLDER . "templates/modules/com_sliders/js/jq.scr.js'></script>
    ";

    return $result . $text;

}

function meta_datas_com_sliders_admin_sidebar_menu_elenemts($result)
{

	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Cлайдеры'), //ім"я пункта меню
			'url' => getURL('admin', 'sliders'), //посилання
			'active_pages' => array('sliders', 'slider-add'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => getURL('admin','slider-add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . $menu;
}

function meta_datas_com_sliders_admin_sidebar_menu_elenemts_active_items($result)
{//перелік сторінок, для яких має бути активний блок меню Елементи сайту
	$result[] = 'sliders';
	$result[] = 'slider-add';
	return $result;
}

/** Роутер Адмінки **/

if (PAGE_TYPE=='admin') {
	_add_action('adminPanel_controller', 'Slider_admin_controller');
}

function Slider_admin_controller(){

	global 	$PData,
			$admin;

	switch(@$_GET['admin']){
		case'sliders':
			require_once __DIR__ . '/apps_admin/metaTags_sliders.php';
			return true;
		break;

		case'slider-add':
			require_once __DIR__ . '/apps_admin/metaTags_slider_add.php';
			return true;
		break;

		case'slider-edit':
			require_once __DIR__ . '/apps_admin/metaTags_slider_edit.php';
			return true;
		break;
	}

}

function Slider($params){
	$res = '';
	parse_str($params, $pars);
	if (isset($pars['id'])){
		if(PAGE_TYPE=='admin') return '[:Slider '.$params.':]';

		global $meta;
		$slider = $meta->val($pars['id'],'slider');

		$res .= '<ul class="bxslider bxslider_'.$pars['id'].'">'."\n";
		if (isset($slider['slides']) && count($slider['slides'])>0){
			foreach($slider['slides'] as $slide){
				$res .= '	<li class="slide">';

				if (!empty($slide['url'])){ $res .= '<a href="'.$slide['url'].'" target="_blank" data-lightbox="slider'.$pars['id'].'" data-title="'.$slide['title'].'">'; }

				if(_checkURL($slide['pic']))
					$res .= '<img src="'.$slide['pic'].'" title=""/>';

				if(!empty($slide['title']))
					$res .= '<div class="slides-content">'.$slide['title'].'</div>';

				if (!empty($slide['url'])){ $res .= '</a>'; }

				$res .= '</li>'."\n";
			}
		}
		$res .= '</ul>'."\n";

		if(!defined('BXSLIDER')){
			define('BXSLIDER',true);
			$res .= '<script type="text/javascript" src="'.SITE_FOLDER.'plugins/bxslider/jquery.bxslider.min.js"></script>'."\n";
			$res .= '<link href="'.SITE_FOLDER.'plugins/bxslider/jquery.bxslider.css" type="text/css" rel="stylesheet"/>'."\n";
		}

		if(!defined('BXSLIDER'.$pars['id'])){
			define('BXSLIDER_'.$pars['id'],true);

			$res .= '<script type="text/javascript">
					(function($){
						$(".bxslider_'.$pars['id'].'").bxSlider({ '.(isset($slider['settings'])?$slider['settings']:'').' });
					})(jQuery);
	    		</script>';
		}
	}


	return $res;
}

function Gallery($params){
	$res = '';
	parse_str($params, $pars);
	if (isset($pars['id'])){
		if(PAGE_TYPE=='admin') return '[:Gallery '.$params.':]';

		global $meta;
		$slider = $meta->val($pars['id'],'slider');

		if(@$pars['ul']!='off')
			$res .= '<ul class="bxGallery">'."\n";

		if (isset($slider['slides']) && count($slider['slides'])>0){
			foreach($slider['slides'] as $slide){
				$res .= '	<li class="slide">';

				if (empty($slide['url'])){ $slide['url'] = $slide['pic']; }

				if(@$pars['a']!='off')
					$res .= '<a href="'.$slide['url'].'" target="_blank" data-lightbox="slider'.$pars['id'].'" data-title="'.$slide['title'].'">';
				if(_checkURL($slide['pic']))
					$res .= '<img src="'.$slide['pic'].'" title=""/>';

				if(!empty($slide['title']))
					$res .= '<div class="slides-content">'.$slide['title'].'</div>';

				if(@$pars['a']!='off')
					$res .= '</a>';

				$res .= '</li>'."\n";
			}
		}
		if(@$pars['ul']!='off')
			$res .= '</ul>'."\n";
	}

	return $res;
}

