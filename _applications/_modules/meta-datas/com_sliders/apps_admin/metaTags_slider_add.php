<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/
global $meta;

if(isset($_POST['sliderNewSubmit']) && isset($_POST['slider']['title'])){
	if ($meta->add_update($meta->sliderMax + 1, $_POST['slider'], 'slider')){
		$PData->content(_lang('Запись сохранена.').' <a href="'.getURL('admin','slider-edit','id='.($meta->sliderMax + 1)).'">'._lang('Добавить слайды').'</a>','message',TRUE);
		return;
	}
}

if (!isset($_POST['slider']['settings'])){
	$_POST['slider']['settings'] = "adaptiveHeight: true,\nauto: true,\ncaptions: true";
}

global $list;


$PData->content(_lang('Создание слайдера'),'title');

$PData->content('
	<form class="list" method="POST">
			<p>'._lang('Название').'</p>
			<input type="text" class="input-xlarge" name="slider[title]" value="'.@$_POST['slider']['title'].'"/>

			<p>'._lang('Описание').'</p>
			<textarea type="text" class="input-xlarge" name="slider[desc]">'.@$_POST['slider']['desc'].'</textarea>

			<p>'._lang('Настройки отображения').'</p>
			<textarea type="text" class="input-xlarge" name="slider[settings]">'.@$_POST['slider']['settings'].'</textarea>

		<input class="button" type="submit" value="'._lang('Сохранить').'" name="sliderNewSubmit"/>
	</form>
	
');