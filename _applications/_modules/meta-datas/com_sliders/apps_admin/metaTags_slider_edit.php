<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/
global $meta;

if(!isset($_REQUEST['id'])) $PData->redirect(getURL('admin'),'403');

if(isset($_POST['sliderEditSubmit']) && isset($_POST['slider'])){
	
	$i = 0;
	while(isset($_POST['slider']['slides'][$i]['title'])){
		$_POST['slider']['slides'][$i]['title'] = urlencode($_POST['slider']['slides'][$i]['title']);
		$i++;
	}
 	if ($meta->add_update($_REQUEST['id'], $_POST['slider'], 'slider')){
 		
 		$i = 0;
		while(isset($_POST['slider']['slides'][$i]['title'])){
			
			$_POST['slider']['slides'][$i]['title'] = urldecode($_POST['slider']['slides'][$i]['title']);
			$i++;
		}
		$meta->change($_REQUEST['id'], $_POST['slider'], 'slider');
 		
 		$PData->content(_lang('Запись сохранена'),'message',TRUE);
 	}
}else{
	$_POST['slider'] = $meta->val($_REQUEST['id'],'slider');
}

$slidesText = '';
$js_arr = '';

if (isset($_POST['slider']['slides']) && count($_POST['slider']['slides'])>0){
	$js_arr = '';
	foreach($_POST['slider']['slides'] as $key=>$val){
		if ($js_arr){ $js_arr.=','; }
		// _dump(str_replace("\n", " ", htmlspecialchars($val['title'])));
		$js_arr.='{pic:"'.$val['pic'].'", url:"'.$val['url'].'", title:"'.str_replace(array("\r","\n"), " ", _protect($val['title'])).'"}';
	}
}


global $list;


$PData->content( _lang('Редактирование слайдера'), 'title' );

$PData->content('
<script type="text/javascript">
	var slides = [' . $js_arr . '];

	function clickTable(){
		var id = $(this).parent().find(".chSlide").val();
//		popupJS(createEditFormText({pic: slides, url: el.find("input[name=\'slider[slides][url][]\']").val(), title: el.find("textarea[name=\'slider[slides][title][]\']").val(), id: el.find(".chSlide").val() }), addSlideToArr);
		popupJS(createEditFormText({pic: slides[id].pic, url: slides[id].url, title: slides[id].title, id: id }), addSlideToArr);
	}

	$(document).ready(function(){
		refreshSlides();
	});

	function openKCFinder(field) {
	    window.KCFinder = {
	        callBack: function(url) {
	            field.value = url;
	            window.KCFinder = null;
	        }
	    };
	    window.open(\'/new-site/plugins/kcfinder/browse.php?type=files&dir=files\', \'kcfinder_textbox\',
	        \'status=0, toolbar=0, location=0, menubar=0, directories=0, \' +
	        \'resizable=1, scrollbars=0, width=800, height=600\'
	    );
	}
		
	function addSlide(){
		popupJS(createEditFormText({pic:"",url:"",title:"",id:""}), addSlideToArr);
	}

	function createEditFormText(slide){
		console.log(slide);
		return "<div>"+
			"<p>"+
			"	<label>'._lang('Картинка').'</label>"+
			"	<input name=\"pic\" type=\"text\" readonly=\"readonly\" onclick=\"openKCFinder(this)\" value=\""+(slide.pic?slide.pic:"'._lang('Нажмите для выбора картинки').'")+"\" style=\"width:100%;cursor:pointer\" />"+
			"</p>"+
			"<p>"+
			"	<label>'._lang('Ссылка').'</label>"+
			"	<input type=\"text\" class=\"input-xlarge\" name=\"url\" value=\""+(slide.url?slide.url:"")+"\" style=\"width:100%;\"/>"+
			"</p>"+
			"<p>"+
			"	<label>'._lang('Подпись').'</label>"+
			"	<textarea class=\"input-xlarge\" name=\"title\">"+(slide.title?slide.title:"")+"</textarea>"+
			"</p>"+
			"<input type=\"hidden\" name=\"id\" value=\""+(slide.id?slide.id:"")+"\">"+
			"</div>";
	}

	function addSlideToArr(val){
		var slide = {};
		var id = false;
		for (i=0; i<val.length; i++){
			if (val[i].name!="id"){
				slide[val[i].name] = val[i].value;
			}else{
				id = val[i].value;
			}
		}
		if (id){
			slides[id] = slide;
		}else{
			slides.push(slide);
		}
		
//		console.log(slides);
		refreshSlides();
	}
		
	function delSlide(idx){
		slides.splice(idx, 1);
		refreshSlides();
	}
	
	$.fn.reverse = [].reverse;
		
	function delSlides(){
		$(".chSlide:checked").reverse().each(function(){
			var idx = $(this).val();
			slides.splice(idx, 1);
		});
		refreshSlides();
	}

	var fixHelper = function(e, ui) {
	    ui.children().each(function() {
	        $(this).width($(this).width());
	    });
	    return ui;
	};
	
//	var onChangeSlideOrder = function( event, ui ) {
//		console.log(ui);
//	}
	
		
	function refreshSlides(){
		$("#slides").empty();
		$.each(slides, function(idx, val){
			$("#slides").append(buildSlideRow(idx, val));
		});
		$(".tableList0").click(clickTable);
		
		$("#slides").sortable({
			axis: "y",
			cursor: "move",
		    helper: fixHelper,
			placeholder: "placeholderBackground",
//			change: onChangeSlideOrder
		}).disableSelection();
	}

	function buildSlideRow(idx, val){
		return "<tr>"+
				"			<td class=\"col0\">"+
				"				<input type=\"checkbox\" class=\"chSlide\" value=\""+idx+"\">"+
				"				<input type=\"hidden\" name=\"slider[slides]["+idx+"][pic]\" value=\""+val.pic+"\">"+
				"				<input type=\"hidden\" name=\"slider[slides]["+idx+"][url]\" value=\""+val.url+"\">"+
				"				<textarea name=\"slider[slides]["+idx+"][title]\" style=\"display:none;\">"+val.title+"</textarea>"+
				"			</td>"+
				"			<td class=\"col1 tableList0\"><img src=\""+val.pic+"\" style=\"max-width:100px; max-height:100px;\"></td>"+
				"			<td class=\"col2 tableList0\">"+val.url+"</td>"+
				"			<td class=\"col3 tableList0\">"+val.title+"</td>"+
				"		</tr>";
	}
</script>');

$PData->content('
	<script type="text/javascript" src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

	<form class="list" method="POST">
		<p>
			<label>'._lang('Шоткод для вставки Слайдера на страницу').'</label> 
			<input type="text" readonly class="input-xlarge" value="[:Slider id='.$_REQUEST['id'].':]"/>
		</p>
		<p>
			<label>'._lang('Шоткод для вставки Галереи на страницу').'</label> 
			<input type="text" readonly class="input-xlarge" value="[:Gallery id='.$_REQUEST['id'].':]"/>
		</p>
		<p>
			<label>'._lang('Название').'</label> 
			<input type="text" class="input-xlarge" name="slider[title]" value="'.@$_POST['slider']['title'].'"/>
		</p>
		<p>
			<label>'._lang('Описание').'</label>
			<textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="slider[desc]">'.@$_POST['slider']['desc'].'</textarea> 
		</p>
		<p>
			<label>'._lang('Настройки отображения').'</label>
			<textarea type="text" style="width: 300px; height: 100px;" class="input-xlarge" name="slider[settings]">'.@$_POST['slider']['settings'].'</textarea> 
		</p>
		
		<hr>
		<p>
			<label>'._lang('Слайды').'</label>
			<input type="button" class="button" value="'._lang('Добавить').'" onClick="addSlide()" />
			<!-- Button trigger modal -->

			<input type="button" class="button" value="'._lang('Удалить').'" onClick="delSlides()"/>
		    <table id="tableEdit" class="table">
			<thead>
			<tr>
		        <th class="head head0">&nbsp;</th>
				<th class="head head0">'._lang('Картинка').'</th>
				<th class="head head0">'._lang('Ссылка').'</th>
				<th class="head head0">'._lang('Подпись').'</th>
		    </tr>
			</thead>
		    <tbody id="slides"></tbody>
			</table>
		</p>

		<hr>			
		<input class="button" type="submit" value="'._lang('Сохранить').'" name="sliderEditSubmit"/>
	</form>	
		<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
      </div>
      <div class="modal-body" id="myModal_body">
      </div>
    </div>
  </div>
</div>
');