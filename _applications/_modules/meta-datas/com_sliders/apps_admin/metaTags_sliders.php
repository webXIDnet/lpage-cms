<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
 ***/
global $meta;

if (isset($_POST['postDeleteSubmit']) && isset($_POST['_post'])) {

    $where = '';

    if ($meta->delete($_POST['_post'], 'slider')) {
        $PData->content(_lang('Запись удалена'), 'message', TRUE);
    }

}
// $nav = _pagination('_landing_pages','lp_id',$where);

$result = $meta->all_values('slider');

$text = '';

if (is_array($result)) {
    foreach ($result AS $mkey => $mval) {
        if (!isset($_REQUEST['filter']) || $PData->_GET('filter')=='' || (strpos($mkey, $PData->_GET('filter')) !== false))
            $text .= '<tr>
			<td class="td-link">
				<input title="' . _lang('выбрать') . '" type="checkbox" name="_post[' . $mkey . ']"/>
			</td>
			<td class="td-link">' . $mkey . '</td>
			<td class="td-link"><a title="' . _lang('Редактировать') . '" href="' . getURL('admin', 'slider-edit', 'id=' . $mkey) . '">' . $mval['title'] . '</a></td>
			<td class="td-link">' . (isset($mval['slides']) ? count($mval['slides']) : '0') . '</td>
		</tr>';

    }
}

$img_dir = SITE_FOLDER . 'media/_admin/';

$PData->content('Слайдеры', 'title');
$PData->content('
<div class="add-new">
        <a href="' . getURL('admin', 'slider-add') . '">
        <div class="item">
                <img src="' . $img_dir . 'img/add-new.png" alt="add-new">
                <p>' . _lang('Добавить слайдер') . '</p>

        </div>
        </a>
      </div>

	<div class="line"></div>

	<form class="list">
					<label>' . _lang('Поиск по коду') . ':</label>
					<input type="hidden" name="admin" value="' . $PData->_GET('admin') . '"/>
					<input type="text" placeholder="' . _lang('Поиск') . '..." name="filter" value="' . $PData->_GET('filter') . '"/><br/>
					<input type="submit" class="button" value="' . _lang('Искать') . '"/>
	</form>

	<div class="line"></div>

		<form id="tableEdit" method="POST">
		<div class="table-holder">
		    <table id="tableEdit" class="table">
		      <thead>
		        	<th class="table-10">&nbsp;</th>
					<th class="table-10">' . _lang('Код') . '</th>
					<th class="table-40">' . _lang('Название') . '</th>
					<th class="table-40">' . _lang('Колличество слайдов') . '</th>
		      </thead>
		      <tbody>
		        	' . $text . '
		      </tbody>
		    </table>
        </div>
		    <input class="button" type="submit" value="' . _lang('Удалить') . '" name="postDeleteSubmit">
		</form>

');