<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/ 

require_once __DIR__ . '/opay_ecommerce-connect_fn.php';

$LData->addTranslations(array(
	'ua' => array(
		'Транзакция не разрешена банком-эмитентом'=>'Траззакція не дозволена банком-емітером',
		'Недостаточно средств' => 'Не достатньо коштів',
		'Несуществующая карта' => 'Неіснуюча карта',
		'Карта утеряна или украдена' => '',
		'Неверный срок действия карты' => 'Не правильний термін дії карти',
		'Превышен допустимый лимит расходов' => 'Перевищений доступний ліміт витрат',
		'Банк-издатель недоступен' => 'Банк-видавець не доступний',
		'Техническая или коммуникационная проблема' => 'Технічна або комунікаційна проблема',
		'' => '',
		'' => '',
		'' => '',
		'' => '',
		'' => '',
		'' => '',
	)
));


_add_action('defined_modules', 'oPay_eCommerceConnect_defined_modules');

function oPay_eCommerceConnect_defined_modules(){
	
	if(PAGE_TYPE=='admin'){
	
		global $meta;
		
		$oPay = $meta->val('apSet_online-payments','mod_option');
		
		if(!is_array($oPay)){
			$oPay = array();
		}
		
		if(!isset($oPay['eCommerceConnect'])){
			$oPay['eCommerceConnect'] = array(
				'Currency' => '980',
				'MerchantID' => '',
				'TerminalID' => '1'
			);
			$meta->add_update('apSet_online-payments',$oPay,'mod_option');
			unset($oPay);
		}
	}
}


_add_filter('trig_PaymentButtons', 'oPay_eCommerceConnect_trig_PaymentButtons'); //виводить кнопку "оплатити" замість шткоду

_add_filter('online-payments_admin-panel_mod_settings','oPay_eCommerceConnect_adminpanel_mod_settings');	//налаштування в розбілі модулів