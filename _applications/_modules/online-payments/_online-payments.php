<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модуль Онлайн оплати
 *
***/
if (PAGE_TYPE == 'api') {
    require_once 'opay.api.php';
}
require_once __DIR__ . '/opay_fn.php';

$LData->addTranslations(array(
	'ua' => array(
		'Сумма счета'=>'Сума',
		'Описание платежа' => 'Призначення платежу',
		'Выставить счет' => 'Виставити рахунок',
		'Общие настройки'=>'Загальні налаштування',
		'payment_done_status'=>'Статус, який призначається у випадку успішної он-лайн оплати',
		'Данные по заказу' => 'Дані по замовленню',
		'Метод оплаты' => 'Метод оплати',
		'Номер заказа' => 'Номер замовлення',
		'ID заказа в CRM-системе' => 'ID замовлення в CRM-системі',
		'Ошибка' => 'Помилка',
		'Код ошибки' => 'Код помилки',
		'Платеж не прошел' => 'Платіж не пройшов',
		'Клиент НЕ получил уведомление об ошибке' => 'Клієнт НЕ отримав повідомлення про помилку',
		'Клиент получил уведомление об ошибке' => 'Клієнт отримав повідомлення про помилку',
		'Платеж не прошел по причине' => 'Платіж не пройшов через наступну причину',
		'' => '',
	),
	'ru' => array(
		'payment_done_status'=>'Статус, который присваивается в случае успешной оплаты',
	)
));

_add_action('defined_modules', 'oPay_defined_modules');

function oPay_defined_modules(){

	if(PAGE_TYPE=='admin'){
		global 	$meta,
				$sql;

		$module_db = $meta->val('apSet_Online-Payments','mod_option');
		if($module_db){
			$sql->replace_str('_meta_datas', array('md_key','md_value'), 'apSet_Online-Payments', 'apSet_online-payments');
		}

		$sql->db_table_installer('Online-Payments','05.09.2014',
			array(
				'_payments' => array(
					'pm_id' => "`pm_id` int(11) NOT NULL AUTO_INCREMENT",
					'pm_prod' => "`pm_prod` varchar(512) NOT NULL",
					'pm_offer' => "`pm_offer` varchar(512) NOT NULL",
					'pm_price' => "`pm_price` double NOT NULL",
					'pm_currency' => "`pm_currency` varchar(4) NOT NULL",
					'pm_client' => "`pm_client` varchar(512) NOT NULL",
					'pm_email' => "`pm_email` varchar(512) NOT NULL",
					'pm_phone' => "`pm_phone` varchar(250) NOT NULL",
					'pm_log' => "`pm_log` varchar(512) NOT NULL",
					'pm_date' => "`pm_date` int(11) NOT NULL",
					'CREATE' => "`pm_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`pm_id`)"
				)
			)
		);
	}
}

if(!empty($_GET['oPay'])){

	_add_action('ajax_init','oPay_'.$_GET['oPay'].'_ajax_init'); //опрацювання звіту по оплаті від сервісу
}

_add_filter('ord_orders_editing_form','oPay_ord_orders_editing_form');	//форма оплати "Виставити рахунок" на сторінці редагування замовлення

if(isset($_POST['sendPaymentForm'])){
	_add_action('init_processes','oPay_defined_all_processes',9); //відправка даних клієнтові по оплаті ($_POST відправляється зі сторінки редагуання замовлення)
}

if(PAGE_TYPE=='payment'){
	_add_action('init_processes','oPay_paymentPage'); //сторінка онлайн оплати (та, що формується по посиланню)
}

_add_action('getURL_rule', 'oPay_getURL'); //задає правило формування посилання

_add_filter('online-payments_admin-panel_mod_settings','oPay_adminpanel_mod_settings',1);	//налаштування в розбілі модулів