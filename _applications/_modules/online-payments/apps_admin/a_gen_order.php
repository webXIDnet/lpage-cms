<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/

if(empty($_GET['price'])){
	$PData->content('Вы не указали цену! Это вызовет критическую ошибку при оплате.');
}else{
	
	$order_id = array(
		'fullname'=>$_GET['fullname'],
		'email'=>$_GET['email']
	);
		
	$xml='<request>      
	      <version>1.2</version>
	      <merchant_id>i6207963580</merchant_id>
	      <result_url>'.getURL('page','thanks','uv='.urlencode(base64_encode($_GET['fullname']))).'</result_url>
	      <server_url>'.getURL('ajax','orderReceiving').'</server_url>
	      <order_id>'.urlencode( base64_encode( http_build_query($order_id) ) ).'</order_id>
	      <amount>'.str_replace(',','.',$_GET['price']).'</amount>
	      <currency>'.$_GET['currency'].'</currency>
	      <description>'.strTranslit($_GET['prod']. ' - Договор ' . $_GET['offer']).'</description>
	      <default_phone></default_phone>
	      <pay_way>card</pay_way>
	      <goods_id>'.strTranslit($_GET['prod']).'</goods_id>
	</request>';
	
	$merc_sign = 'hiTnPVjycSiM8oFuvVcCIfuVIZdq3oTQdX8';
	
	$sign=base64_encode( sha1($merc_sign . $xml . $merc_sign , 1) );
	$xml_encoded=base64_encode($xml);
	
	$url = json_encode(array(
		'operation_xml'=>$xml_encoded,
		'signature'=>$sign,
		'get'=>$_GET
	));

	$PData->redirect( getURL( 'orders','','uv='.urlencode( base64_encode($url) ) ) );
}

/*
все кроме default_phone - телефон введённый на форме оплаты по умолчанию
goods_id - id товара, используется только для счетчика покупок данного товара
description - описание
order_id - id заказа
*/