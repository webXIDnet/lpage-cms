<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/

/** Пагінація **/

$text = $where = '';

if(!empty($_GET['filter'])){
	$where =
		"WHERE
			pm_desc LIKE '%"._protect(strTranslit($_GET['filter']))."%'
		OR
			pm_email LIKE '%"._protect(strTranslit($_GET['filter']))."%'
		OR
			pm_client LIKE '%"._protect(strTranslit($_GET['filter']))."%'
		OR
			pm_currency LIKE '%"._protect(strTranslit($_GET['filter']))."%'
		OR
			pm_price LIKE '%"._protect(strTranslit($_GET['filter']))."%'
		OR
			pm_prod LIKE '%"._protect(strTranslit($_GET['filter']))."%'
		OR
			pm_offer LIKE '%"._protect(strTranslit($_GET['filter']))."%'";
}


$nav = _pagination('_payments','pm_id',$where);


/** Формування основного списку **/

$result = $sql->query(
	"SELECT pm_id, pm_email, pm_phone, pm_date, pm_client, pm_currency, pm_price, pm_prod, pm_offer, pm_log
	FROM _payments
	$where
	ORDER BY pm_date DESC
	".@$nav['limit']
);

foreach($result AS $cval){

	$stat = '<b style="color: #111;">---</b>';

	if(!empty($cval['pm_log'])){

		parse_str($cval['pm_log']);

		switch($status){
			case'failure':
				$stat = '<b style="color: red;">'.$status.'</b>';
			break;
			case'success':
				$stat = '<b style="color: green;">'.$status.'</b>';
			break;
			case'wait secure':
				$stat = '<b style="color: #111;">'.$status.'</b>';
			break;

		}
	}

	$text .= '
		<tr>
			<td>
				<u>'.$cval['pm_client'].'</u><br/>
				<span style="color:#888">
					'.$cval['pm_email'].'<br/>
					'.$cval['pm_phone'].'
				</span>
			</td>
			<td>
				<span style="color:#888">
					Заказ: <b style="color: #111;">'.$cval['pm_prod'].'</b><br/>
					Договор № <u style="color: #111;">'.$cval['pm_offer'].'</u>
				</span>
			</td>
			<td >

				<span style="color:#888">
					Сумма: <b style="color: #111;">'.$cval['pm_price'].' '.$cval['pm_currency'].'</b><br/>
					Проведена: <u style="color: #111;">'.date('d.m.Y H:i', $cval['pm_date']).'</u><br/>
					№ <u style="color: #111;">'.$cval['pm_id'].'</u><br/>
					Статус: '.$stat.'
				</span>
			</td>
		</tr>
	';
}

$PData->content('Успешно проведенные платежи','title');

$PData->content('
	<form class="list">
		<table >
			<tr>
				<td>
					<label>'._lang('Поиск по заголовку').':</label>
					<input type="text" name="filter" value="'.@$_GET['filter'].'"/><br/>
					<input type="submit" class="btn" value="'._lang('Искать').'"/>
				</td>
			</tr>
		</table>
	</form>
	<table class="table" style="width: 100%;">
		<tbody>
			<tr>
				<th>
					Клиент
				</th>
				<th>
					Заказ
				</th>
				<th>
					Транзакция
				</th>
			</tr>
			'.$text.'
		</tbody>
	</table>
	'


	.
	$nav['html']
);