<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/

function oPay_eCommerceConnect_trig_PaymentButtons($result,$get){//Заміна шоткоду на форму платежу
	
	global 	$user,
			$meta,
			$PData;
	
	parse_str($get,$array);
	
	$text = $article = '';
	$SD_vars = array();
	
	if(!empty($array['lp_id'])){//ID LP, код якого буде відправлено клієнту піля оплати
		$SD_vars['lp_id'] = $array['lp_id'];
	}elseif(!empty($array['p_id'])){//ID поста або сторінки, код якого буде відправлено клієнту піля оплати
		$SD_vars['lp_id'] = $array['p_id'];
	}
		
	if(!empty($_GET['set'])){
		$set = _strCode($_GET['set'],FALSE);
	}
	
	if(isset($_POST['ordersubmit'])){
		
		foreach($set AS $key => $val){
			if(empty($_POST[$key]))$_POST[$key]=$val;
		}
		$set = $_POST;
	}
	
	if(isset($set) && !empty($set['prod_price']) && isset($set['rf_email'])){
		
		$text = '';
		$currency = array(
				'643' => 'RUB',
				'840' => 'USD',
				'978' => 'EUR',
				'980' => 'UAH'
			);
				
		$oPay = $meta->val('apSet_online-payments','mod_option');
		
		if(isset($oPay['eCommerceConnect']) && is_array($oPay['eCommerceConnect'])){
			foreach($oPay['eCommerceConnect'] AS $key => $val){
				$text .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />';
			}
		}
		
		$SD_vars['rf_email'] = $set['rf_email'];
		$set['o_id'] .= '_'.time();
		
		if(stristr(@$set['rf_phone'],'NON_'))$set['rf_phone']='';
		if(stristr(@$set['rf_email'],'NON_'))$set['rf_email']='';
		
		$loc = $user->lang;
		if($loc=='ua')$loc='uk';
		
		$PurchaseTime = date("ymdHis");
		$SD = _strCode($SD_vars, TRUE, TRUE);
		$set['prod_price'] = ($set['prod_price']*100);
		
		$data = $oPay['eCommerceConnect']['MerchantID'].';'.
				$oPay['eCommerceConnect']['TerminalID'].';'.
				$PurchaseTime.';'.
				$set['o_id'].';'.
				$oPay['eCommerceConnect']['Currency'].';'.
				$set['prod_price'].';'.
				$SD .';'; // -строка для подписи
		
		$fp = fopen(__DIR__ . '//signature/'.$oPay['eCommerceConnect']['MerchantID'].'.pem', 'r');
		$priv_key = fread($fp, 8192);
		fclose($fp);
		
		$pkeyid = openssl_get_privatekey($priv_key);
		
		openssl_sign( $data , $signature, $pkeyid);
		openssl_free_key($pkeyid);
		
		$b64sign = base64_encode($signature); //Подпись данных в формате base64 
		
		$text = '
		<h4 class="payment-title">'._lang('Оплата через').' <span>eCommerceConnect</span></h4>
		<p class="payment-desc">
			'._lang('Продукт').': <b>'._lang($set['prod_title']).'</b><br/>
			'._lang('Цена').': <b>'.($set['prod_price']/100).' '._lang(@$currency[$oPay['eCommerceConnect']['Currency']]).'</b><br/>
			'._lang('Клиент').': <b>'.@$set['rf_name'].' '.@$set['rf_phone'].' '.@$set['rf_email'].'</b><br/>
		</p>
		
		<form action="https:/secure.upc.ua/ecgtest/enter" method="POST"> 
			'.$text.'
			'.$article.'
			<input type="hidden" name="Version" value="1">
			<input type="hidden" name="TotalAmount" value="'.$set['prod_price'].'"> 
			<input type="hidden" name="locale" value="'.$loc.'"> 
			<input type="hidden" name="SD" value="'.$SD.'"> 
			<input type="hidden" name="OrderID" value="'.@$set['o_id'].'"> 
			<input type="hidden" name="PurchaseTime" value="'.$PurchaseTime.'"> 
			<input type="hidden" name="PurchaseDesc" value="'.$set['prod_title'].'"> 
			<input type="hidden" name="Signature" value="'.$b64sign.'"> 
			<input type="submit" class="paymentButton" value="'._lang('Оплатить через eCommerceConnect').'"/>
		</form>';
		
	}elseif($user->rights<3){
		global $PData;
		$PData->redirect(HOMEPAGE);
	}
	
	return $result.'<div id="eCommerceConnect" class="payments eCommerceConnect-payment">'.$text.'</div>';
}
function oPay_eCommerceConnect_adminpanel_mod_settings($result,$set){
	//параметри платіжної системи в розділі модулів
	
	$result['title'] .= 
		'<li role="presentation" class="">
		 <a href="#tab_u_eCommerceConnect" aria-controls="home" role="tab" data-toggle="tab">'._lang('eCommerceConnect').'</a>
		 </li>
		 ';
	
	$user_rights = '
		<p>
			
			<label>
				<input class="input" placeholder="'._lang('Ключ').'" type="text" name="newModSet[apSet_online-payments][eCommerceConnect][1]" value=""/>
			</label>
			<label>
				<input class="input" placeholder="'._lang('Значение').'" type="text" name="newModSet[apSet_online-payments][eCommerceConnect][0]" value=""/>
			</label>
			
		</p><input class="button" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit"><br/><hr/>
			<b>'._lang('Ссылка взаимодействия для eCommerceConnect').':</b><br>
			<u>'.getURL('ajax','oPay=eCommerceConnect').'</u> <span>- '._lang('Укажите ссылку в личном кабинете сервиса.').'</span>
		<br/><hr/><br/>';
	
	if(is_array($set)){
		
		foreach($set AS $key_com=>$array){
			if(is_array($array) && $key_com=='eCommerceConnect'){
				foreach($array AS $key=>$val){
					if($key==100){
						$user_rights .= '
						<p>
							<label>
								<b>'._lang($val).'</b> ('.$key.') <label>
								<input disabled="disabled" class="input" type="text"  value="'.$val.'"/>
								<input type="hidden" name="modSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$val.'"/>
							</label>
						</p>
						';
					}else{
						$user_rights .= '
						<p>
							<label>
								<b>'._lang($val).'</b> ('.$key.') <label>
								<input class="input" type="text" name="modSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$val.'"/>
								<input type="checkbox" name="delApSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$key.'"> - '._lang('Удалить').'
							</label>
						</p>
						';
					}
				}
				$user_rights .= '<hr/>';
			}
		}
	
	}
	
	
	$result['text'] .= 
		'<div role="tabpanel" class="tab-pane fade" id="tab_u_eCommerceConnect">
			'.$user_rights.'
		</div>';
		
	return $result;
}

function oPay_eCommerceConnect_ajax_init(){
	//опрацювання отриманих звіту платіжної системи
		
	global $PData;
	
	$array = array('PurchaseTime','ProxyPan','Currency','ApprovalCode','MerchantID','OrderID','Signature','Rrn','XID','Email','SD','TranCode','TerminalID','TotalAmount');
	
	foreach($array AS $val){
		if(!isset($_POST[$val])){
			$PData->redirect(HOMEPAGE);
		}
	}
	
	global $meta;

	$oPay = $meta->val('apSet_online-payments','mod_option');
	$eCommerceConnect = @$oPay['eCommerceConnect'];
	
	$data = $_POST['MerchantID'].';'.
			$_POST['TerminalID'].';'.
			$_POST['PurchaseTime'].';'.
			$_POST['OrderID'].';'.
			$_POST['XID'].';'.
			$_POST['Currency'].';'.
			$_POST['TotalAmount'].';'.
			$_POST['SD'].';'.
			$_POST['TranCode'].';'.
			$_POST['ApprovalCode'].';'; // -строка для подписи
	
	if(stristr($_SERVER['REMOTE_ADDR'],'195.85.198.')){
		
		$return = 
"MerchantID='{$_POST['MerchantID']}'
TerminalID='{$_POST['TerminalID']}'
OrderID='{$_POST['OrderID']}'
Currency='{$_POST['Currency']}'
TotalAmount='{$_POST['TotalAmount']}'
XID='{$_POST['XID']}'
PurchaseTime='{$_POST['PurchaseTime']}'
Response.action='approve'
Response.reason=''
Response.forwardUrl=''";

	}else return;
	
	// $data содержит значение полей для проверки подписи
	$signature = $_POST['Signature'];
	$signature = base64_decode($signature) ;
	// извлечь сертификат 
	$fp = fopen(__DIR__ . '//signature/work-server.cert', "r");
	$cert = fread($fp, 8192); 
	fclose($fp); 
	$pubkeyid = openssl_get_publickey($cert); 
	// проверка подписи
	$ok = openssl_verify($data, $signature, $pubkeyid); 

	if($ok==1){

		$SD_vars = _strCode($_POST['SD'], FALSE, TRUE);
		
		if($_POST['TranCode']=='000'){
			
			global 	$sql;
			
			$text = _lang('Платеж получен');
			
			$OrderID = explode('_',$_POST['OrderID']);
			
			$o_id = $OrderID[0];
			
			$client = $sql->query(
				"SELECT cl_id,cl_email,cl_tel,cl_fullname,o_payment_note,o_prod
				FROM _orders INNER JOIN _clients ON o_cl_id=cl_id
				WHERE o_id='"._protect($o_id)."'
				",
				'value'
			);
			
			if(!is_array($client) || $SD_vars['rf_email']!=$client['cl_email']) $PData->redirect(HOMEPAGE);
			
			$currency = array(
				'643' => 'RUB',
				'840' => 'USD',
				'978' => 'EUR',
				'980' => 'UAH'
			);
			
			$o_payment_note = array(
				'payment_id'=>$_POST['OrderID'],
				'shop_money'=>$_POST['TotalAmount']/100,
				'client_money'=>$_POST['TotalAmount']/100,
				'pay_method'=>_lang('eCommerceConnect - Visa / MasterCard'),
				'pay_status'=>$_POST['TranCode'],
				'currency'=>@$currency[$_POST['Currency']]
			);
			
			
			$client['o_payment_note'] = json_decode($client['o_payment_note'],TRUE);
			
			$client['o_payment_note'][] = $o_payment_note;			
			
			$sql->update('_orders',"o_id='"._protect($o_id)."'",
				array(
					'o_payment_note'=>json_encode($client['o_payment_note']),
					'o_date_done'=>time(),
					'o_status'=>$oPay['settings']['payment_done_status'],
					'o_payment_type'=>1
				)
			);
			
			$text .= '
				<p style="border: 1px solid green;padding: 10px 20px 10px 10px;">
					<b style="color:#000;display: block;text-align: center;padding-bottom: 9px;">'._lang('On-line Платеж').'</b><br/>
					
					'._lang('№ платежа').': <b style="color:#000">'.$o_payment_note['payment_id'].'</b><br/>	
					'._lang('Оплачено').': <b style="color:#000">'.$o_payment_note['client_money'].' '.@$o_payment_note['currency'].'</b><br/>	
					'._lang('Описание платежа').': <b style="color:#000">'.$client['o_prod'].'</b><br/>
					
					'._lang('Метод').': <b style="color:#000">'.$o_payment_note['pay_method'].'</b>
				</p>
			';
			
			if(isset($SD_vars['lp_id'])){
				$cval = $sql->query("
					SELECT lp_html
					FROM _landing_pages
					WHERE lp_id='"._protect($SD_vars['lp_id'])."'",
					'value'
				);
				$text = $cval['lp_html'];
			}elseif($SD_vars['p_id']){
				$cval = $sql->query("
					SELECT p_text
					FROM _posts
					WHERE p_id='"._protect($SD_vars['p_id'])."'",
					'value'
				);
				$text = $cval['p_text'];
			}			
			
			if(stristr($client['cl_tel'],'NON_'))$client['cl_tel']='';
			if(stristr($client['cl_email'],'NON_'))$client['cl_email']='';
			
			$_POST['payment_value'] = array(
				'cl_id'=>$client['cl_id'],
				'o_id'=>$o_id
			);
			
			$text = _sharpCode(
				$text,
				array(
					'#NAME#'=>$client['cl_fullname'],
					'#PHONE#'=>$client['cl_tel'],
					'#EMAIL#'=>$client['cl_email'],
				)
			);
			
			if(_checkEmail($client['cl_email'])){
				_eMail($client['cl_email'],_lang('Оплата проведена успашно'),_shortCode($text),$meta->val('contact_name').' <'.$meta->val('contact_email').'>');
				_eMail($meta->val('contact_email'),_lang('Поступил платеж от')." {$client['cl_fullname']} <{$client['cl_email']}>",_shortCode($text));
			}
			echo $return;
		}else{
			
			$ansverCode = array(
				'100' => 'Транзакция не разрешена банком-эмитентом',
				'103' => 'Транзакция не разрешена банком-эмитентом',
				'104' => 'Транзакция не разрешена банком-эмитентом',
				'105' => 'Транзакция не разрешена банком-эмитентом',
				'106' => 'Транзакция не разрешена банком-эмитентом',
				'107' => 'Транзакция не разрешена банком-эмитентом',
				
				'116' => 'Недостаточно средств',
				
				'111' => 'Несуществующая карта',
				'125' => 'Несуществующая карта',
				'200' => 'Несуществующая карта',
				'202' => 'Несуществующая карта',
				
				'108' => 'Карта утеряна или украдена',
				'208' => 'Карта утеряна или украдена',
				'209' => 'Карта утеряна или украдена',
				
				'101' => 'Неверный срок действия карты',
				'201' => 'Неверный срок действия карты',
				
				'121' => 'Превышен допустимый лимит расходов',
				'123' => 'Превышен допустимый лимит расходов',
				'130' => 'Превышен допустимый лимит расходов',
				
				'290' => 'Банк-издатель недоступен',
				'905' => 'Банк-издатель недоступен',
				'906' => 'Банк-издатель недоступен',
				'907' => 'Банк-издатель недоступен',
				'908' => 'Банк-издатель недоступен',
				'910' => 'Банк-издатель недоступен',
				
				'291' => 'Техническая или коммуникационная проблема',
			);
			
			if(!isset($ansverCode[$_POST['TranCode']]))$ansverCode[$_POST['TranCode']] = _lang('Код ошибки').' #'.$_POST['TranCode']; 
			
			for($i=900;$i<1000;$i++){
				if($i!=905 || $i!=906 || $i!=907 || $i!=908 || $i!=910)
					$ansverCode[$i]='Техническая или коммуникационная проблема';
			}
			
			$text = _lang('Клиент получил уведомление об ошибке');
							
			if(_checkEmail($SD_vars['rf_email'])){
				_eMail($SD_vars['rf_email'],_lang('Платеж не прошел'),
					_lang('Платеж не прошел по причине').': <b>'._lang($ansverCode[$_POST['TranCode']]).'</b><br/><br/>
					---<br/>
					'._lang('С найлучшими пожеланиями,').' '.$meta->val('contact_name').'<br/>'.
					_lang('Email').': '.$meta->val('contact_email')
				);
			}else{
				$text = _lang('Клиент НЕ получил уведомление об ошибке');
			}
			
			$o_id = explode('_',$_POST['OrderID']);
			
			_eMail($meta->val('contact_email'),_lang('Платеж не прошел'),
				'<b>'._lang('Данные по заказу').'</b><br>
				'._lang('Метод оплаты').': '._lang('eCommerceConnect - Visa / MasterCard').'<br>
				'._lang('Номер заказа').': '.$_POST['OrderID'].'<br>
				'._lang('ID заказа в CRM-системе').': '.$o_id[0].'<br>
				'._lang('Ошибка').': '._lang($ansverCode[$_POST['TranCode']]).'<br>
				'._lang('XID').': '.$_POST['XID'].'<br><br>
				<b>'.$text.'</b>'
			);
		}
	}
	openssl_free_key($pubkeyid);
}