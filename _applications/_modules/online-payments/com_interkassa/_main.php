<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/

require_once __DIR__ . '/opay_interkassa_fn.php';

$LData->addTranslations(array(
	'ua' => array(
		'Сумма счета'=>'Сума',
		'Описание платежа' => 'Призначення платежу',
		'Выставить счет' => 'Виставити рахунок',
	)
));

if(PAGE_TYPE=='admin'){
	_add_action('defined_modules', 'oPay_intercassa_defined_modules');
}

function oPay_intercassa_defined_modules(){
	
	global $meta;
	
	$oPay = $meta->val('apSet_online-payments','mod_option');

	if(!is_array($oPay)){
		$oPay = array();
	}
	
	if(!isset($oPay['intercassa'])){
		$oPay['intercassa'] = array(
			'ik_co_id' => '',
			'ik_co_prs_id' => '',
			'ik_cur' => 'USD',
			'ik_exp' => '7',
			'secret_key' => ''
		);
		$meta->add_update('apSet_online-payments',$oPay,'mod_option');
		unset($oPay);
	}	
}

_add_filter('trig_PaymentButtons', 'oPay_Interkassa_trig_PaymentButtons'); //виводить кнопку "оплатити" замість шткоду

_add_filter('online-payments_admin-panel_mod_settings','oPay_Interkassa_adminpanel_mod_settings');	//налаштування в розбілі модулів