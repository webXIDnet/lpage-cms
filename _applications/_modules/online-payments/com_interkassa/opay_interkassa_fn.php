<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/

function oPay_Interkassa_trig_PaymentButtons($result,$get){//Заміна шоткоду на форму платежу
	
	global 	$user,
			$meta;
	
	parse_str($get,$array);
	
	$text = $article = '';
	
	if(!empty($array['lp_id'])){//ID LP, код якого буде відправлено клієнту піля оплати
		$article = 'index" value="'._strCode($array['lp_id']);
	}elseif(!empty($array['p_id'])){//ID поста або сторінки, код якого буде відправлено клієнту піля оплати
		$article = 'cache" value="'._strCode($array['p_id']);
	}
	
	if(!empty($article))$article = '<input type="hidden" name="ik_x_'.$article.'" />';
	
	if(!empty($_GET['set'])){
		$set = _strCode($_GET['set'],FALSE);
	}
	
	if(isset($_POST['ordersubmit'])){
		
		foreach($set AS $key => $val){
			if(empty($_POST[$key]))$_POST[$key]=$val;
		}
		$set = $_POST;
	}
	
	if(isset($set) && !empty($set['prod_price']) && isset($set['rf_email'])){
		
		$current = $text = '';
				
		$oPay = $meta->val('apSet_online-payments','mod_option');
		
		if(isset($oPay['interkassa']) && is_array($oPay['interkassa'])){
			foreach($oPay['interkassa'] AS $key => $val){
				if($key=='ik_exp')$val = date('Y-m-d',time()+$val*24*60*60);
				if($key=='ik_cur')$current = $val ;
				if($key=='ik_co_prs_id' || $key=='payment_done_status' || $key=='secret_key') continue;				
				$text .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />';
			}
		}
		
		if(stristr(@$set['rf_phone'],'NON_'))$set['rf_phone']='';
		if(stristr(@$set['rf_email'],'NON_'))$set['rf_email']='';
		
		$text = '
		<h4 class="payment-title">'._lang('Оплата через').' <span>Interkassa</span></h4>
		<p class="payment-desc">
			'._lang('Продукт').': <b>'._lang($set['prod_title']).'</b><br/>
			'._lang('Цена').': <b>'.$set['prod_price'].' '._lang($current).'</b><br/>
			'._lang('Клиент').': <b>'.@$set['rf_name'].' '.@$set['rf_phone'].' '.@$set['rf_email'].'</b><br/>
		</p>
		
		<form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
			'.$text.'
			<input type="hidden" name="ik_cli" value="'.@$set['rf_name'].' '.@$set['rf_phone'].' '.@$set['rf_email'].'" />
			<input type="hidden" name="ik_pm_no" value="'.@$set['o_id'].'_'.time().'" />
			<input type="hidden" name="ik_am" value="'.$set['prod_price'].'" />
			<input type="hidden" name="ik_desc" value="'.$set['prod_title'].'" />
			<input type="hidden" name="ik_enc" value="utf-8" />
			'.$article.'
			<input type="hidden" name="ik_x_email" value="'.@$set['rf_email'].'" />
		    <input type="submit" class="paymentButton" value="'._lang('Оплатить через Interkassa').'"/>
		</form>
		';
	}elseif($user->rights<3){
		global $PData;
		$PData->redirect(HOMEPAGE);
	}
	
	return $result.'<div id="interkassa" class="payments interkassa-payment">'.$text.'</div>';
}
function oPay_Interkassa_adminpanel_mod_settings($result,$set){
	//параметри платіжної системи в розділі модулів
	
	$result['title'] .= 
		'
		 <li role="presentation" class="">
		 <a href="#tab_u_Interkassa" aria-controls="home" role="tab" data-toggle="tab">'._lang('Interkassa').'
            </a></li>
		 ';
	
	$user_rights = '
		<p>
			
			<label>
				<input class="input" placeholder="'._lang('Ключ').'" type="text" name="newModSet[apSet_online-payments][interkassa][1]" value=""/>
			</label>
			<label>
				<input class="input" placeholder="'._lang('Значение').'" type="text" name="newModSet[apSet_online-payments][interkassa][0]" value=""/>
			</label>
			
		</p><input class="button" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit"><br/><hr/>
			<b>'._lang('Ссылка взаимодействия для Interkassa').':</b><br>
			<u>'.getURL('ajax','oPay=interkassa').'</u> <span>- '._lang('Укажите ссылку в личном кабинете сервиса.').'</span>
		<br/><hr/><br/>';
	
	if(is_array($set)){
		
		foreach($set AS $key_com=>$array){
			if(is_array($array)  && $key_com=='interkassa'){
				foreach($array AS $key=>$val){
					if($key==100){
						$user_rights .= '
						<p>
							<label>
								<b>'._lang($val).'</b> ('.$key.') <label>
								<input disabled="disabled" class="input" type="text"  value="'.$val.'"/>
								<input type="hidden" name="modSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$val.'"/>
							</label>
						</p>
						';
					}else{
						$user_rights .= '
						<p>
							<label>
								<b>'._lang($val).'</b> ('.$key.') <label>
								<input class="input" type="text" name="modSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$val.'"/>
								<input type="checkbox" name="delApSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$key.'"> - '._lang('Удалить').'
							</label>
						</p>
						';
					}
				}
				$user_rights .= '<hr/>';
			}
		}
	
	}
	
	
	$result['text'] .= 
		'
        <div role="tabpanel" class="tab-pane fade" id="tab_u_Interkassa">
			'.$user_rights.'
		</div>';
		
	return $result;
}

function oPay_interkassa_ajax_init(){
	//опрацювання отриманих звіту платіжної системи
	
	global $PData;
	
	$array = array('ik_co_id','ik_co_prs_id','ik_inv_id','ik_inv_st','ik_inv_crt','ik_inv_prc','ik_pm_no','ik_desc','ik_pw_via','ik_am','ik_cur','ik_co_rfn','ik_ps_price','ik_sign');
	
	foreach($array AS $val){
		if(!isset($_POST[$val])){
			$PData->redirect(HOMEPAGE);
		}
	}
	
	global $meta;
		
	$oPay = $meta->val('apSet_online-payments','mod_option');
	$interkassa = @$oPay['interkassa'];
	
	$dataSet = $_POST;
	unset($dataSet['ik_sign']); //удаляем из данных строку подписи
	ksort($dataSet, SORT_STRING); // сортируем по ключам в алфавитном порядке элементы массива
	array_push($dataSet, $interkassa['secret_key']); // добавляем в конец массива "секретный ключ"
	$signString = implode(':', $dataSet); // конкатенируем значения через символ ":"
	$sign = base64_encode(md5($signString, true)); // берем MD5 хэш в бинарном виде по
	
	
	if($sign==$_POST['ik_sign'] && stristr($_SERVER['REMOTE_ADDR'],'85.10.225.')){
		
		if($_POST['ik_inv_st']=='success'){
			
			global 	$sql;
			
			$text = _lang('Платеж получен');
			
			$ik_pm_no = explode('_',$_POST['ik_pm_no']);
			
			$_POST['ik_pm_no'] = $ik_pm_no[0]; 
			
			$client = $sql->query(
				"SELECT cl_id,cl_email,cl_tel,cl_fullname,o_payment_note,o_prod
				FROM _orders INNER JOIN _clients ON o_cl_id=cl_id
				WHERE o_id='"._protect($_POST['ik_pm_no'])."'
				",
				'value'
			);
			
			if(!is_array($client) || $_POST['ik_x_email']!=$client['cl_email']) $PData->redirect(HOMEPAGE);
			
			$o_payment_note = array(
				'payment_id'=>$_POST['ik_inv_id'],
				'shop_money'=>$_POST['ik_co_rfn'],
				'client_money'=>$_POST['ik_am'],
				'pay_method'=>$_POST['ik_pw_via'],
				'pay_status'=>$_POST['ik_inv_st'],
				'currency'=>$_POST['ik_cur']
			);
			
			
			$client['o_payment_note'] = json_decode($client['o_payment_note'],TRUE);
			
			_eMail($client['cl_email'],'test1',print_r($client['o_payment_note'],TRUE));
			
			$client['o_payment_note'][] = $o_payment_note;
			
			_eMail($client['cl_email'],'test2',print_r($client['o_payment_note'],TRUE));
			
			
			$sql->update('_orders',"o_id='"._protect($_POST['ik_pm_no'])."'",
				array(
					'o_payment_note'=>json_encode($client['o_payment_note']),
					'o_date_done'=>strtotime($_POST['ik_inv_prc']),
					'o_status'=>$oPay['settings']['payment_done_status'],
					'o_payment_type'=>1
				)
			);
			
			$text .= '
				<p style="border: 1px solid green;padding: 10px 20px 10px 10px;">
					<b style="color:#000;display: block;text-align: center;padding-bottom: 9px;">'._lang('On-line Платеж').'</b><br/>
					
					'._lang('№ платежа').': <b style="color:#000">'.$o_payment_note['payment_id'].'</b><br/>	
					'._lang('Оплачено').': <b style="color:#000">'.$o_payment_note['client_money'].' '.@$o_payment_note['currency'].'</b><br/>	
					'._lang('Описание платежа').': <b style="color:#000">'.$client['o_prod'].'</b><br/>
					
					'._lang('Метод').': <b style="color:#000">'.$o_payment_note['pay_method'].'</b>
				</p>
			';
			
			if(isset($_POST['ik_x_index'])){
				$cval = $sql->query("
					SELECT lp_html
					FROM _landing_pages
					WHERE lp_id='"._protect((int)_strCode($_POST['ik_x_index'],FALSE))."'",
					'value'
				);
				$text = $cval['lp_html'];
			}elseif(isset($_POST['ik_x_cache'])){
				$cval = $sql->query("
					SELECT p_text
					FROM _posts
					WHERE p_id='"._protect((int)_strCode($_POST['ik_x_cache'],FALSE))."'",
					'value'
				);
				$text = $cval['p_text'];
			}			
			
			if(stristr($client['cl_tel'],'NON_'))$client['cl_tel']='';
			if(stristr($client['cl_email'],'NON_'))$client['cl_email']='';
			
			$_POST['payment_value'] = array(
				'cl_id'=>$client['cl_id'],
				'o_id'=>$_POST['ik_pm_no']
			);
			
			$text = _sharpCode(
				$text,
				array(
					'#NAME#'=>$client['cl_fullname'],
					'#PHONE#'=>$client['cl_tel'],
					'#EMAIL#'=>$client['cl_email'],
				)
			);
			
			if(_checkEmail($client['cl_email'])){
				_eMail($client['cl_email'],_lang('Оплата проведена успашно'),_shortCode($text));
			}			
		}else{
			if(_checkEmail($_POST['ik_x_email'])){
				_eMail($_POST['ik_x_email'],_lang('Платеж не прошел'),
					_lang('По неизвесным причинам платеж не прошел').'<br/>'.
					_lang('Попробуйте еще раз - если ошибка повторится, обратитесь в нашу службу поддержки ').
					$meta->val('contact_email').'<br/><br/>
					
					'._lang('С найлучшими пожеланиями,').'<br/>
					'.$meta->val('contact_name')
				);
			}
		}
	}
}