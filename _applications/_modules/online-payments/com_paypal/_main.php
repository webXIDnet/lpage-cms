<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/

require_once __DIR__.'/opay_paypal_fn.php';
require_once __DIR__.'/rest-api/autoload.php';
$LData->addTranslations(array(
	'ua' => array(
		'Сумма счета'=>'Сума',
		'Описание платежа' => 'Призначення платежу',
		'Выставить счет' => 'Виставити рахунок',
                        'p_succ_url' => 'Сторінка, якщо транзакція успішна',
                        'p_user_id' => 'API ID користувача',
			'p_user_pass' => 'API пароль користувача',			
			'p_sign' => 'API Signature',
                        'p_return_url' => 'API Return URL',
                        'p_cancel_url' => 'API Cancel URL',
                        'p_cur' => 'Валюта (USD, EUR etc.)',
                'Операция проведена успешно!' => 'Операція проведена успішно!',
                'Ваши средства успешно переведены.' => 'Ваші кошти успішно переведені.'
	),
        'ru' => array(
            'p_succ_url' => 'Страница, если транзакция успешна',
            'p_user_id' => 'API ID пользователя',
            'p_user_pass' => 'API пароль пользователя',			
            'p_sign' => 'API Signature',
            'p_return_url' => 'API Return URL',
            'p_cancel_url' => 'API Cancel URL',
            'p_cur' => 'Валюта (USD, EUR etc.)'
        )
));

global $sql;
/*
$sql->query("IF NOT EXISTS (
                SELECT * FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_NAME = '_payments' AND COLUMN_NAME = 'pm_transaction_id'
            )
            BEGIN
                    ALTER TABLE `_payments` ADD `pm_transaction_id` VARCHAR(20)
            END
    ");*/


if(PAGE_TYPE=='admin'){
	_add_action('defined_modules', 'opay_paypal_defined_modules');
}

if (PAGE_TYPE == 'page' || PAGE_TYPE == 'post') {
    _add_action('defined_all_processes', 'oPay_paypal_request_result');
}

function opay_paypal_defined_modules(){
	
	global $meta;
	
	$oPay = $meta->val('apSet_online-payments','mod_option');

	if(!is_array($oPay)){
		$oPay = array();
	}
        
	
	if(!isset($oPay['paypal']))
            {
		$oPay['paypal'] = array(
                        'p_succ_url' => '',
			'p_user_id' => '',
			'p_user_pass' => '',			
			'p_sign' => '',
                        'p_return_url' => '',
                        'p_cancel_url' => '',
                        'p_cur' => '',                        
                        'p_exp' => ''
		);
		$meta->add_update('apSet_online-payments',$oPay,'mod_option');
		unset($oPay);
	}
        /*else
        {
            foreach ($oPay['paypal'] as $key=>$val)
            {
                
            }
        }*/
            
}

_add_filter('trig_PaymentButtons', 'opay_Paypal_trig_PaymentButtons'); //виводить кнопку "оплатити" замість шткоду

_add_filter('online-payments_admin-panel_mod_settings','opay_Paypal_adminpanel_mod_settings');	//налаштування в розбілі модулів