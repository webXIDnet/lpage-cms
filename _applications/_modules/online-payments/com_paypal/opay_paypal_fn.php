<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/

$paypal_config = array(
                'endpoint' => 'https://api-3t.sandbox.paypal.com/nvp',
                'USER' => '',
                'PWD' => '',
                'SIGNATURE' => '',
                'VERSION' => '119',
                            
            );
$paypal_data = array(
                'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
                'PAYMENTREQUEST_0_AMT' => '',
                'PAYMENTREQUEST_0_CURRENCYCODE' => '',
                'PAYMENTREQUEST_0_DESC' => '',
                'RETURNURL' => '',
                'CANCELURL' => ''
);
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
$result_html = '';
$set = '';

function oPay_Paypal_trig_PaymentButtons($result,$get){//Заміна шоткоду на форму платежу
   
	global 	$user, $meta, $paypal_config, $paypal_data, $result_html, $set;
	
	parse_str($get,$array);
	
	$text = $article = '';
	
//        _dump('trig');
//        exit;
      
	if(!empty($array['lp_id'])){//ID LP, код якого буде відправлено клієнту піля оплати
		$article = 'index" value="'._strCode($array['lp_id']);
	}elseif(!empty($array['p_id'])){//ID поста або сторінки, код якого буде відправлено клієнту піля оплати
		$article = 'cache" value="'._strCode($array['p_id']);
	}
	
	if(!empty($article))$article = '<input type="hidden" name="ik_x_'.$article.'" />';
	
	if(!empty($_GET['set'])){
		$set = _strCode($_GET['set'],FALSE);
	}
	
	if(isset($_POST['ordersubmit'])){
		
		foreach($set AS $key => $val){
			if(empty($_POST[$key]))$_POST[$key]=$val;
		}
		$set = $_POST;
	}
	
        
	if(isset($set) && !empty($set['prod_price']) && isset($set['rf_email'])){
		 
		$current = $text = '';

                foreach ($paypal_config as $key=>$val)
                {
                    $text .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />';
                }
                foreach ($paypal_data as $key=>$val)
                {
                    $text .= '<input type="hidden" name="'.$key.'" value="'.$val.'" />';
                }
                
		if(stristr(@$set['rf_phone'],'NON_'))$set['rf_phone']='';
		if(stristr(@$set['rf_email'],'NON_'))$set['rf_email']='';
		
		$text = '
		<h4 class="payment-title">'._lang('Оплата через').' <span>Paypal</span></h4>
		<p class="payment-desc">
			'._lang('Продукт').': <b>'._lang($set['prod_title']).'</b><br/>
			'._lang('Цена').': <b>'.$set['prod_price'].' '._lang($current).'</b><br/>
			'._lang('Клиент').': <b>'.@$set['rf_name'].' '.@$set['rf_phone'].' '.@$set['rf_email'].'</b><br/>
		</p>
		
		<form id="payment" name="payment" method="post" action="" enctype="utf-8">
			'.$text.'
			
			'.$article.'
			
		    <input type="submit" name="paypal_submit" class="paymentButton" value="'._lang('Оплатить через Paypal').'"/>
		</form>
		';
	}elseif($user->rights<3){
		global $PData;
		$PData->redirect(HOMEPAGE);
	}
	
        if ($result_html == '')
            return $result.'<div id="paypal" class="payments paypal-payment">'.$text.'</div>';
        else 
            return $result_html;
}


function oPay_Paypal_adminpanel_mod_settings($result,$set){
	//параметри платіжної системи в розділі модулів
	
	$result['title'] .= 
		'
		<li role="presentation" class="">
		 <a href="#tab_u_Paypal" aria-controls="home" role="tab" data-toggle="tab">'._lang('Paypal').'</a>
		 </li>
		';
	
	$user_rights = '
		<p>
			
			<label>
				<input class="input" placeholder="'._lang('Ключ').'" type="text" name="newModSet[apSet_online-payments][paypal][1]" value=""/>
			</label>
			<label>
				<input class="input" placeholder="'._lang('Значение').'" type="text" name="newModSet[apSet_online-payments][paypal][0]" value=""/>
			</label>
			
		</p><input class="button" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit"><br/><hr/>
			<b>'._lang('Ссылка взаимодействия для Paypal').':</b><br>
			<u>'.getURL('ajax','oPay=paypal').'</u> <span>- '._lang('Укажите ссылку в личном кабинете сервиса.').'</span>
		<br/><hr/><br/>';
//	var_dump($set);
	if(is_array($set)){
            
		foreach($set AS $key_com=>$array){
			if(is_array($array)  && $key_com=='paypal'){                            
				foreach($array AS $key=>$val){
					if($key==100){
						$user_rights .= '
						<p>
							<label>
								<b>'._lang($key).'</b>  <label>
								<input disabled="disabled" class="input" type="text"  value=""/>
								<input type="hidden" name="modSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$val.'"/>
							</label>
						</p>
						';
					}else{
						$user_rights .= '
						<p>
							<label>
								<b>'._lang($key).'</b>  <label>
								<input class="input" type="text" name="modSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$val.'"/>
								<input type="checkbox" name="delApSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$key.'"> - '._lang('Удалить').'
							</label>
						</p>
						';
					}
				}
				$user_rights .= '<hr/>';
			}
		}
	
	}
	
	
	$result['text'] .= 
		'
<div role="tabpanel" class="tab-pane fade" id="tab_u_Paypal">
			'.$user_rights.'
		</div>';
		
	return $result;
}

function oPay_paypal_ajax_init(){
	//опрацювання отриманих звіту платіжної системи
	
	global $PData;
	global $meta;
		
	$oPay = $meta->val('apSet_online-payments','mod_option');
	$paypal = @$oPay['paypal'];
	

		if(isset($_POST['status']) && $_POST['status'] =='success')
        {
			
			global 	$sql;

            $email = isset($_POST['email']) ? $_POST['email'] : '';
            $desc = isset($_POST['desc']) ? $_POST['desc'] : '';
            $amt = isset($_POST['amt']) ? $_POST['amt'] : '';
            $period = isset($_POST['period']) ? $_POST['period'] : '';
            $cur = isset($_POST['cur']) ? $_POST['cur'] : '';
            $transaction_id = isset($_POST['transaction_id']) ? $_POST['transaction_id'] : '';

            $head = '<h4>'._lang('Платеж произведен').'</h4>';
			$text = '
                        <div>
                            <p>
                                '._lang('Вы успешно произвели оплату на сайте'). ' <a href="http://' .$_SERVER['HTTP_HOST'] .'">' .$_SERVER['HTTP_HOST'] .'</a>.
                            </p>
                            <p>
                                '._lang('Название платежа').': '.$desc.'<br>
                                '._lang('Сумма').': '.$desc. ' ' . $cur . ($period ? ' / '.$period : '') . '<br>
                                '._lang('Дата').': '.date('d.m.Y H:i').'<br>
                                '._lang('Вы можете отменить подписку в своем личном кабинете на сайте').' <a href="http://' .$_SERVER['HTTP_HOST'] .'">' .$_SERVER['HTTP_HOST'] .'</a>.
                            </p>
                        </div>';

            if (_checkEmail($email))
                _eMail($email,_lang('Оплата прошла успешно!'),$head.$text);

            $data = array(
                'pm_prod'     => $desc,
                'pm_price'    => $amt,
                'pm_currency' => $cur,
                'pm_email'    => $email,
                'pm_date'     => date('d.m.Y H:i'),
                'pm_transaction_id' => $transaction_id
            );

            $sql->insert('_payments',$data);

            $result['head'] = $head;
            $result['body'] = $text;
                echo json_encode($result);

		}
        /*else{

            $email = $_SESSION['paym_data']['email'];

            $text = '<h4>'._lang('Платеж не произведен').'</h4>
                        <div>
                            <p>
                                '._lang('Во время оплаты произошла ошибка. Повторите, пожалуйста, немного позже.'). '
                            </p>
                            <p>
                                 <a href="http://' .$_SERVER['HTTP_HOST'] .'">' .$_SERVER['HTTP_HOST'] .'</a>.
                            </p>
                        </div>';


            _eMail($email,_lang('Ошибка оплаты!'),$text);

		}*/

}

function oPay_paypal_request_result()
{
    global $user, $meta, $paypal_config,$paypal_data,$paypal_url, $result_html, $set;
     
    @session_start();
    /*Step 1 - setting configs*/
//    _dump('step 1');
//    exit;
    {
        //if method was GET - filling $set var from GET
        if(!empty($_GET['set'])){
                    $set = _strCode($_GET['set'],FALSE);
            }
            //otherwise - filling anyway from POST
            $set = $_POST;
                    
            //get online-payments config        
            $oPay = $meta->val('apSet_online-payments','mod_option');
            if(isset($oPay['paypal']) && is_array($oPay['paypal'])){
                //filling paypal_config array from module config
                    foreach($oPay['paypal'] AS $key => $val){

                            if ($key == 'p_user_id') $paypal_config['USER'] = $val;
                            if ($key == 'p_user_pass') $paypal_config['PWD'] = $val;
                            if ($key == 'p_sign') $paypal_config['SIGNATURE'] = $val;
                            if ($key == 'p_cur') $paypal_data['PAYMENTREQUEST_0_CURRENCYCODE'] = $val;
                            if ($key == 'p_return_url') $paypal_data['RETURNURL'] = $val;
                            if ($key == 'p_cancel_url') $paypal_data['CANCELURL'] = $val;  
                            if ($key == 'p_succ_url') $paypal_config['success_url'] = $val;  
                    }
            }
            if (isset($_SESSION['paym_data']['amt']) && isset($_SESSION['paym_data']['desc']))
            {
                 $paypal_data['PAYMENTREQUEST_0_AMT'] = $_SESSION['paym_data']['amt'];
                 $paypal_data['PAYMENTREQUEST_0_DESC'] = $_SESSION['paym_data']['desc'];
                 $paypal_data['PAYMENTACTION'] = 'Authorization';
            }
            //if in POST or GET array from form exists price and email
            if (isset($set) && !empty($set['prod_price']) && isset($set['rf_email'])) {
                //continue filling
                $current = $text = '';
                if ($set['prod_price']) $paypal_data['PAYMENTREQUEST_0_AMT'] = $set['prod_price'];
                if ($set['prod_title']) $paypal_data['PAYMENTREQUEST_0_DESC'] = $set['prod_title'];

                /*Step 0 - Saving request data*/
    //                _dump('step 0');
    //                exit;
                $paym_data = array();
                if (isset($set['amt']) && $set['amt'] != '') {
                    $paym_data['amt'] = addslashes($set['amt']);
                    $paym_data['cur'] = $paypal_data['PAYMENTREQUEST_0_CURRENCYCODE'];
                }
                else {
                    if (isset($set['amt_per_period'])) {
                        $val = $set['amt_per_period'];
                        switch ($val) {
                            case '10_per_mo' : {
                                $paym_data['period'] = 'Month';
                                $paym_data['amt'] = '10';
                            }
                                break;
                            case '20_per_mo' : {
                                $paym_data['period'] = 'Month';
                                $paym_data['amt'] = '20';
                            }
                                break;
                            case '100_per_year' : {
                                $paym_data['period'] = 'Year';
                                $paym_data['amt'] = '100';
                            }
                                break;
                            default : {
                            $paym_data['period'] = addslashes($set['period']);
                            $paym_data['amt'] = addslashes($set['user-summ']);
                            }
                            break;
                        }
                    }
                    else {
                        $paym_data['error'] = 'Required more data for transaction!';
                    }
                }
    //                _dump($paym_data);exit;
                $paym_data['email'] = $set['rf_email'];
                $paym_data['desc'] = $paypal_data['PAYMENTREQUEST_0_DESC'];
                $_SESSION['paym_data'] = $paym_data;
            }
    }
//    _dump($_SESSION);
    /*Step 2 - setExpressCheckout query*/
//    _dump('step 2'); 
//    exit;
     //if user clicked Submit in form
    if (isset($_POST['paypal_submit']))
    {
        //building GET string for query
        $nvpStr = '&'.  http_build_query($paypal_data);
        //calling SetExpressCheckout method 
        $set_expr_check = api_call('setExpressCheckout', $nvpStr);
        //checking response status
//        _dump($set_expr_check);
//        exit;
        if (strtoupper($set_expr_check['ACK']) == 'SUCCESS')
        {           
            //if config exists and token exists
            if ($paypal_url && $set_expr_check['TOKEN'])
            {
                //do redirect to paypal
                header('Location: '.$paypal_url.'&token='.$set_expr_check['TOKEN']);
                die();
            }
        }
    }
    
    /*Step 3 - GetExpressCheckoutDetails query*/
//    _dump('step 3'); 
//    exit;
    //if token and payerId is in GET array 
    if (isset($_GET['token']) && isset($_GET['PayerID']))
    {         
        
        //building GET string
        $nvpStr = '&'. '&TOKEN='.$_GET['token'];
        //calling PayPal API
        $get_expr_check = api_call('GetExpressCheckoutDetails', $nvpStr);
         /*Step 4 - DoExpressCheckoutPayment or CreateRecurringPaymentsProfile query*/
        //if GetExpressCheckoutDetails response is success

//        exit;
        if (strtoupper($get_expr_check['ACK']) == 'SUCCESS')
        {
            //If 'period' field not saved into session
            // _dump($_SESSION);
            if (!isset($_SESSION['paym_data']['error']))
            {
                //Building API GET string
                $nvpStr = '&'.  http_build_query($get_expr_check);
                //and calling DoExpressCheckoutPayment method (complete transaction)
                $do_expr_check = api_call('DoExpressCheckoutPayment', $nvpStr);

                if (strtoupper($do_expr_check['ACK']) == 'SUCCESS')
                {
//                    $_SESSION['paym_data']['status'] = 'success';
                    $data['status'] = 'success';
                    $data['transaction_id'] = $do_expr_check['PAYMENTINFO_0_TRANSACTIONID'];
                    $save = curl_query(getURL('ajax').'/?oPay=paypal','POST',$data);
                    throw_error($save['head'],$save['body']);
//                    unset($_SESSION['paym_data']);
                }
                else
                {
//                    $_SESSION['paym_data']['status'] = 'failure';
                    $data['status'] = 'failure';
                    $data['transaction_id'] = $do_expr_check['PAYMENTINFO_0_TRANSACTIONID'];
                    $save = curl_query(getURL('ajax').'/?oPay=paypal','POST',$data);
                    throw_error($save['head'],$save['body']);
//                    unset($_SESSION['paym_data']);
                }



                //if session has 'period' field and if it hasn't error (it means user choosed recurring payent)
                if (isset($_SESSION['paym_data']['period']) && strtoupper($do_expr_check['ACK']) == 'SUCCESS')
                {
                    //create data array from config and session data
                    $a = array(
                        'TOKEN'             => $do_expr_check['TOKEN'],
                        'PAYERID'           => $get_expr_check['PAYERID'],
                        'PROFILESTARTDATE'  => date('c'),
                        'DESC'              => $_SESSION['paym_data']['desc'],
                        'BILLINGPERIOD'     => $_SESSION['paym_data']['period'],
                        'BILLINGFREQUENCY'  => '1',
                        'AMT'               => $_SESSION['paym_data']['amt'],
                        'CURRENCYCODE'      => $paypal_data['PAYMENTREQUEST_0_CURRENCYCODE'],
//                        'COUNTRYCODE'       => 'UA',
                        'MAXFAILEDPAYMENTS' => '3'

                    );
                    //create GET string
                    $nvpStr = '&' . http_build_query($a);
                    //calling PayPal API
                    $recur_paym = api_call('CreateRecurringPaymentsProfile', $nvpStr);

                    //if SUCCESS - returning message
                    if (strtoupper($recur_paym['ACK']) == 'SUCCESS')
                    {
//                        $_SESSION['paym_data']['status'] = 'success';
                        $data['status'] = 'success';
                        $data['transaction_id'] = $do_expr_check['PAYMENTINFO_0_TRANSACTIONID'];
                        $save = curl_query(getURL('ajax').'/?oPay=paypal','POST',$data);
                        throw_error($save['head'],$save['body']);
                    }
                    else
                    {
//                        $_SESSION['paym_data']['status'] = 'failure';
                        $data['status'] = 'failure';
                        $data['transaction_id'] = $do_expr_check['PAYMENTINFO_0_TRANSACTIONID'];
                        $save = curl_query(getURL('ajax').'/?oPay=paypal','POST',$data);
                        throw_error($save['head'],$save['body']);
                    }
                }
            }

            
            
        }
        else
        {
//            $_SESSION['paym_data']['status'] = 'failure';
            $data['status'] = 'failure';
            $data['transaction_id'] = $get_expr_check['PAYMENTINFO_0_TRANSACTIONID'];
            $save = curl_query(getURL('ajax').'/?oPay=paypal','POST',$data);
            throw_error($save['head'],$save['body']);
        }
       
    }
    
} 


function curl_query($url,$method = 'POST',$data = array(),$proxy = '')
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    //turning off the server and peer verification(TrustManager Concept).
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    $str = http_build_url($data);


    if (strtoupper($method) == 'POST')
    {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
        curl_setopt($ch, CURLOPT_URL,$url);
    }
    else if (strtoupper($method) == 'GET')
    {
        curl_setopt($ch, CURLOPT_HTTPGET, 1);
        curl_setopt($ch, CURLOPT_URL,$url . '&' . $str);
    }


    //Set proxy
    if(count(explode(':',$proxy)) > 0)
    {
        $proxy = explode(':',$proxy);
        curl_setopt ($ch, CURLOPT_PROXY, $proxy[0] . ":" . $proxy[1]);
    }

    $response = curl_exec($ch);

    $response = json_decode($response);

    return $response;
}
/**'-------------------------------------------------------------------------------------------------------------------------------------------
* api_call: Function to perform the API call to PayPal using API signature
* @methodName is name of API method.
* @nvpStr is nvp string.
* returns an associtive array containing the response from the server.
'-------------------------------------------------------------------------------------------------------------------------------------------
*/
function api_call($methodName,$nvpStr)
{
        //declaring of global variables
        global $paypal_config;
        global $USE_PROXY, $PROXY_HOST, $PROXY_PORT;
//        global $gv_ApiErrorURL;
//        global $sBNCode;
        
        $API_Endpoint = $paypal_config['endpoint'];
        $version = $paypal_config['VERSION'];
        $API_UserName = $paypal_config['USER'];
        $API_Password = $paypal_config['PWD'];
        $API_Signature = $paypal_config['SIGNATURE'];
        
        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        //turning off the server and peer verification(TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        //if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
        //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php
        if($USE_PROXY)
        curl_setopt ($ch, CURLOPT_PROXY, $PROXY_HOST. ":" . $PROXY_PORT);
        //NVPRequest for submitting to server
        $nvpreq="METHOD=" . urlencode($methodName) . "&VERSION=" . urlencode($version) . "&PWD=" . urlencode($API_Password) . "&USER=" . urlencode($API_UserName) . "&SIGNATURE=" . urlencode($API_Signature) . $nvpStr ;
        //setting the nvpreq as POST FIELD to curl
//        _dump(explode('&',$nvpreq));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
        //getting response from server
        $response = curl_exec($ch);
        //convrting NVPResponse to an Associative Array
        $nvpResArray=deformatNVP($response);
        $nvpReqArray=deformatNVP($nvpreq);
//        $_SESSION['nvpReqArray']=$nvpReqArray;
        if (curl_errno($ch))
        {
        // moving to display page to display curl errors
//        $_SESSION['curl_error_no']=curl_errno($ch) ;
//        $_SESSION['curl_error_msg']=curl_error($ch);
        var_dump(curl_errno($ch));
        //Execute the Error handling module to display errors.
        }
        else
        {
        //closing the curl
        curl_close($ch);
        }
    _dump($nvpResArray);
        return $nvpResArray;
}
/*'----------------------------------------------------------------------------------
* This function will take NVPString and convert it to an Associative Array and it will decode the response.
* It is usefull to search for a particular key and displaying arrays.
* @nvpstr is NVPString.
* @nvpArray is Associative Array.
----------------------------------------------------------------------------------
*/
function deformatNVP($nvpstr)
{
//    $ipn = new \PayPal\IPN\PPIPNMessage()
    $intial=0;
    $nvpArray = array();
    while(strlen($nvpstr))
    {
        //postion of Key
        $keypos= strpos($nvpstr,'=');
        //position of value
        $valuepos = strpos($nvpstr,'&') ? strpos($nvpstr,'&'): strlen($nvpstr);
        /*getting the Key and Value values and storing in a Associative Array*/
        $keyval=substr($nvpstr,$intial,$keypos);
        $valval=substr($nvpstr,$keypos+1,$valuepos-$keypos-1);
        //decoding the respose
        $nvpArray[urldecode($keyval)] =urldecode( $valval);
        $nvpstr=substr($nvpstr,$valuepos+1,strlen($nvpstr));
    }
    return $nvpArray;
}

/**
 * Throw error into page
 *
 */
function throw_error($var = null,$title = '',$body = '')
{
    global $result_html;

    $var_dump = '';
    if ($var) $var_dump = '<pre>
                                '.print_r($var,true).'
                            </pre>';

    $result_html = '<div><h3>' . _lang($title) . '</h3><br>
                        ' . _lang($body) . '<br>'.$var_dump.'</div>';

}