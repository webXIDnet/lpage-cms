<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/

function oPay_getURL($atr){
	
	switch($atr['key']){
		case'payment':
			
			if(!empty($atr['get']))$atr['get'] = '?'.$atr['get'];
			
			return HOMEPAGE.$atr['key'].'/'.$atr['val'].$atr['get'];
		break;
		
		default:
			return FALSE;
		
	}	
}
function oPay_paymentPage(){
	define('MODULE_PAGE_TYPE','TRUE');
	global $PData;
	
	$PData->content('Страница on-line оплаты','title');
	$PData->content('[:PaymentButtons:]');
}
function oPay_URL($get){
	if(PAGE_TYPE=='admin') return '[:oPay_URL '.$get.':]';
	parse_str($get,$set);
	return '?set='._strCode($set); 
}
function oPay_defined_all_processes(){
	
	global $PData;
	
	$o_id = $_POST['set']['o_id'];
	$cl_id = 0;
	foreach($_POST['editClient'] AS $key=>$val){
		$cl_id = $key;
	}
	
	$_POST['set']['rf_name'] = @$_POST['editClient'][$cl_id]['fullname'];
	$_POST['set']['rf_phone'] = @$_POST['editClient'][$cl_id]['tel'];
	$_POST['set']['rf_email'] = @$_POST['editClient'][$cl_id]['email'];
//	print_r($_POST);
	if(_checkEmail($_POST['set']['rf_email'])){
		
		$_GET['set'] = _strCode($_POST['set']);
		
		$text = _lang('Ссылка для онлайн оплаты').': <a href="'.getURL('payment',$_GET['set']).'">'._lang('Оплатить').'</a>';
				
		if(_eMail($_POST['set']['rf_email'],_lang('Счет'),$text)){
			$PData->content('Счет отправлен клиенту','message',TRUE);
		}else{
			$PData->content('Счет не был выставлен, проверте коректность указаного Email','message');
		}
	}else{
		$PData->content('Неверный формат почты','message');
	}
}

function oPay_ord_orders_editing_form($result, $var){//Генерація форми виставлення рахунку в адмінці (order_edit)
	
	global $meta;
	
	$text = '<div class="clear"></div>
		<h2 class="h1" style=" text-align: center; ">'._lang('Выставить счет').'</h2>
		<form method="POST">
			<div class="right_colmn">
				
				<input class="input" type="hidden" value="'.$var['o_id'].'" name="set[o_id]"/>
		
				<p>	
					<label class="greyText">
						<input type="checkbox" name="sendPaymentForm" value="true"> '._lang('<b class="blackText">Отправить счет</b> клиенту').'
					</label>
				</p>
			</div>
			<p>
				<b>'._lang('Сумма счета').'</b><br/>
				<input class="input" type="text" value="'.(int)$var['o_price'].'" name="set[prod_price]">
			</p>
			<p>
				<b>'._lang('Описание платежа').'</b><br/>
				<textarea class="input" name="set[prod_title]" style="height:140px;" maxlength="256">'.$var['o_prod'].'</textarea>
			</p>
			
			<div class="clear"></div>
			<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit">
			<style>[value="Save"]{display:none;}</style>
	';
	
	return ''.$text.'';
	
}

function oPay_adminpanel_mod_settings($result,$set){
	//параметри платіжної системи в розділі модулів
	
	$result['title'] .= 
		'
		<li role="presentation" class="active">
		 <a href="#tab_u_0" aria-controls="home" role="tab" data-toggle="tab">'._lang('Настройки').'</a>
		 </li>
		';
	
	$user_rights = '
		<p>
			
			<label>
				<input class="input" placeholder="'._lang('Ключ').'" type="text" name="newModSet[apSet_online-payments][settings][1]" value=""/>
			</label>
			<label>
				<input class="input" placeholder="'._lang('Значение').'" type="text" name="newModSet[apSet_online-payments][settings][0]" value=""/>
			</label>
			
		</p><input class="button" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit"><br/><hr/>
		
		<br/>';
	
	if(is_array($set)){
		
		foreach($set AS $key_com=>$array){
			if(is_array($array)  && $key_com=='settings'){
				foreach($array AS $key=>$val){
					if($key==100){
						$user_rights .= '
						<p>
							<label>
								<b>'._lang($val).'</b> ('._lang($key).') <label>
								<input disabled="disabled" class="input" type="text"  value="'.$val.'"/>
								<input type="hidden" name="modSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$val.'"/>
							</label>
						</p>
						';
					}else{
						$user_rights .= '
						<p>
							<label>
								<b>'._lang($key).'</b> ('._lang($val).') <label>
								<input class="input" type="text" name="modSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$val.'"/>
								<input type="checkbox" name="delApSet[apSet_online-payments]['.$key_com.']['.$key.']" value="'.$key.'"> - '._lang('Удалить').'
							</label>
						</p>
						';
					}
				}
				$user_rights .= '<hr/>';
			}
		}
	
	}
	
	
	$result['text'] .= 
		'<div role="tabpanel" class="tab-pane fade in active" id="tab_u_0">
			'.$user_rights.'
		</div>';
		
	return $result;
}