<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

$csv_array = array();

function ProdFilter()
{
    global $csv_array;
    $temp_array = array();
    $result = '';
    $file = fopen(ROOT_DIR.'/media/files/catalog.csv', 'r');
    if ($file)
    {
        while ($row = fgetcsv($file,10,';'))
        {
            if($row[0] == '_CATEGORY_') {
                continue;
            }
            
            $temp = $row;
            if(!empty($temp[4])){
                $tempval = explode('.',$temp[4]);
                $tempval[count($tempval)-2] .= '-350x350';
                $tempval = implode('.',$tempval); 

                // $check []= _checkURL('http://tocar.com.ua/image/cache/'.$tempval);
                $temp[4] = 'http://tocar.com.ua/image/cache/'.$tempval;
            }
            $temp_array []= $temp; 

        }
        array_splice($temp_array,0,7);
        foreach ($temp_array as $key => $val) {
            $cat = explode('|',$val[0]);
            if (count($cat) > 1)
                $cat = preg_split("/\s/i",$cat[0]);
            $categories []= $cat[0];

            $vol = preg_split("/[\s\|]/i",$val[5]);
            // $vol = explode('|',$val[5]);
            foreach ($vol as $k => $v) {
                if (preg_match('/[0-9]+л/i', $v))
                {
                    $possible_vols []= $v;
                    $val_vols [$v]= $vol[$k+3];
                }
            }
            ksort($val_vols,SORT_NATURAL);
            $temp = array(
                'Объем' => $val_vols
                );
            $vols []= $temp;
            $csv_array [$key]= $val;
            $val_visc = '';
            $visc = preg_split("/[\s\|]/i",$val[6]);
            foreach ($visc as $k => $v) {
                if (preg_match('/[0-9]+W-?/i', $v))
                    {
                        $val_visc = $v;
                        $visces []= $v;
                    }
                if (preg_match('/[0-9]+л/i', $v))
                    {
                        $current_vol = $v;
                    }

            }
            $temp['Вязкость'] = $val_visc;
            $csv_array[$key][5] = $temp;
            $csv_array[$key][6] = array('Объем' => $current_vol);
        }
        $visces = array_diff(array_unique($visces), array(''));
        sort($visces, SORT_NATURAL);
        $possible_vols = array_diff(array_unique($possible_vols), array(''));
        sort($possible_vols, SORT_NATURAL);
        $categories = array_diff(array_unique($categories),array(''));
        // if (_getIP() == '195.12.59.162')
        //     _dump($csv_array);
    }
    // require_once 'views/main.php';
    $result .= require_once 'views/main.php';
    // $result .= require_once 'views/products_list.php';
    return $result;
}

function build_array_from_file(){

    global $csv_array;
    $temp_array = array();
    $result = '';
    $file = fopen(ROOT_DIR.'/media/files/catalog.csv', 'r');
    if ($file)
    {
        while ($row = fgetcsv($file,10,';'))
        {
            $temp = $row;
            if(!empty($temp[4])){
                $tempval = explode('.',$temp[4]);
                $tempval[count($tempval)-2] .= '-350x350';
                $tempval = implode('.',$tempval); 

                // $check []= _checkURL('http://tocar.com.ua/image/cache/'.$tempval);
                $temp[4] = 'http://tocar.com.ua/image/cache/'.$tempval;
            }
            $temp_array []= $temp; 
        }
        array_splice($temp_array,0,7);
        foreach ($temp_array as $key => $val) {
            $cat = explode('|',$val[0]);
            if (count($cat) > 1)
                $cat = preg_split("/\s/i",$cat[0]);
            $categories []= $cat[0];
            if(!empty($val[5])){
                $vol = preg_split("/[\s\|]/i",$val[5]);
                // $vol = explode('|',$val[5]);
                foreach ($vol as $k => $v) {
                    if (preg_match('/[0-9]+л/i', $v))
                    {
                        $possible_vols []= $v;
                        $val_vols [$v]= $vol[$k+3];
                    }
                }
                ksort($val_vols,SORT_NATURAL);
            } 
            
            $temp = array(
                'Объем' => @$val_vols
                );
            $vols []= $temp;
            $csv_array [$key]= $val;
            $val_visc = '';
            if(!empty($val[6])){
                $visc = preg_split("/[\s\|]/i",$val[6]);
                foreach ($visc as $k => $v) {
                    if (preg_match('/[0-9]+W-?/i', $v))
                        {
                            $val_visc = $v;
                            $visces []= $v;
                        }
                    if (preg_match('/[0-9]+л/i', $v))
                        {
                            $current_vol = $v;
                        }
                }
            }
            $temp['Вязкость'] = $val_visc;
            $csv_array[$key][5] = @$temp;
            $csv_array[$key][6] = array('Объем' => @$current_vol);
        }
      
    }
    return $csv_array;

}

function return_products_ajax_init()
{
    global $csv_array,$rate,$meta;

    $csv_array = build_array_from_file();
/*    
if(stristr('ві 1Лодки1 вві',$_POST['oil_type']))
    echo '|||11111|||';
else
    echo '|||'.$_POST['oil_type'].'|||';
*/
    foreach ($csv_array as $row) {
        $category = @$row[0];
        $image = @$row[4];
        $name = @$row[2];
        $price = @$row[3];
        $vol = @$row[6]['Объем'];
        $visc = @$row[5]['Вязкость'];
       /* var_dump(preg_match('/'.$_POST['oil_type'].'/i', $category));
        var_dump(preg_match('/'.$_POST['visc'].'/i', $visc));
        var_dump(preg_match('/'.$_POST['vol'].'/i', $vol));
        var_dump(preg_match('/'.$_POST['search_text'].'/i', $search_text));
        var_dump('***');*/
        if ($_POST['visc'] != '')
        {
            if ($_POST['visc'] == $visc)
                $post_visc = 1;
            else
                $post_visc = 0;
        }
        else
            $post_visc = 1;

        if ($_POST['vol'] != '')
        {
            if ($_POST['vol'] == $vol)
                $post_vol = 1;
            else
                $post_vol = 0;
        }
        else
            $post_vol = 1;

        /*_dump(array(preg_match('/'.$_POST['oil_type'].'/i', str_replace("\n","",$category)),
            $post_visc,
            $vol,
            $_POST['vol'],
            $post_vol,
            preg_match('/'.$_POST['search_text'].'/i', $name)
        ));*/

        if ($name != '' &&
            preg_match('/\.\-/i', $image) == 0 &&
            strripos($category,$_POST['oil_type']) &&
//            preg_match('/'.$_POST['oil_type'].'/i', str_replace("\n","",$category)) > 0 &&
//            preg_match('/'.$_POST['visc'].'/i', $visc) > 0 &&
//            preg_match('/'.$_POST['vol'].'/i', $vol) > 0 &&
            $post_visc == 1 &&
            $post_vol == 1 &&
            preg_match('/'.$_POST['search_text'].'/i', $name) > 0)
        {
//            _dump($row);
            $temp_arr []= array(
                'category' => $category,
                'img' => $image,
                'name' => $name,
                'price' => $price,
                'vol' => $vol,
                'visc' => $visc);
        }
            
    }
    // _dump($temp_arr);
    $result = require_once 'views/list.php';

    echo $result;
}