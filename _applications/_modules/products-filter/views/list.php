<?php

$items_html = '';

if(isset($temp_arr) && ( is_array($temp_arr) || is_object($temp_arr) ) )
foreach ($temp_arr as $row) {
  $price = round($row['price']*$meta->val('rate_of_course'),2);
  $items_html .= <<<HTML
          <div step="1" style="z-index: 1; position: relative; /*float: left;*/ margin:0 auto; width: 400px;border-bottom: 1px solid gray;" class="product-block">
            <div class="undefined"><p><img style="width:200px;" src="{$row['img']}" alt=""></p>

                <div class="goods-block">
                    <p class="goods-title" style="font-weight: bold;">{$row['name']}</p>

                    <p class="goods-info">Вязкость: <strong>{$row['visc']}</strong><br>
                    Обьем: <strong>{$row['vol']}</strong></p>

                    <p class="goods-price">Цена: <strong>{$price} грн.</strong></p>
                    <a prod-title="{$row['name']}" prod-price="{$row['price']}" href="#id" class="buy-button b-but-2 open_popup">Купить</a>
                </div>
            </div>
          </div> 
HTML;
}
if (count($temp_arr) == 0 || !is_array($temp_arr)) 
  $items_html = _lang('Извините, ничего не найдено!');
$list_html = <<<HTML
<div class="product-line" style="max-height: 1000px; overflow:auto;background-color: white; border-radius: 7px;margin-top: 20px;border:none;">         
          
          {$items_html}
      </div>
HTML;

return $list_html;