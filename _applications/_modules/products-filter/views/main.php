<?php 
//категории
$type = $vols_html = $visces_html = '';
foreach ($categories as $cat) {
    $type .= '<option value="'._strtoupper($cat).'">'.$cat.'</option>';
         }
//объем
foreach ($possible_vols as $vol) {
    $vols_html .= '<option value="'.$vol.'">'.$vol.'</option>';
         }
//вязкость
foreach ($visces as $v) {
    $visces_html .= '<option value="'.$v.'">'.$v.'</option>';
         }
$result_main = <<<HTML
<div style="display:inline-block;">
  <div style="color: white; text-align: left;">Категории продукции</div>
  <select class="valid oil_type" name="oil_type" aria-invalid="false">
  <option value=""  disabled selected=""></option>
  <option style="color:grey" value="">Все масла</option>
        {$type}
        <option value="ТРАНС">Трансмиссионные</option>
  </select>
  <div style="color: white; text-align: left; margin-top: 10px;">Типы вязкости</div>
  <select class="valid visc" name="visc" aria-invalid="false">
  <option value=""  disabled selected=""></option>
	<option style="color:grey" value="">Любая вязкость</option>
     {$visces_html}
  </select>
  <div style="color: white; text-align: left; margin-top: 10px;">Все виды фасовки</div>
  <select class="valid vol" name="vol" aria-invalid="false">
  <option value=""  disabled selected=""></option>
	<option style="color:grey" value="">Любой объем</option>
    {$vols_html}
  </select>
  <input type="text" class="search_text" name="search_text" placeholder="Поиск по названию">
</div>
<button prod-price="" prod-title="Найти" class="buy-button prod-search butt b-but-other">Найти</button>
<div class="ajax-cont"></div>
HTML;
return $result_main;