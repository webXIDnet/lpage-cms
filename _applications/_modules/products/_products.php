<?php
/**
 * Products module main file
 */

require_once __DIR__ . '/products_class.php';
$products = new Products();
require_once __DIR__ . '/shortcode.php';

if (PAGE_TYPE == 'admin') {
    _add_action('defined_modules', 'product_defined_modules');
}
if (PAGE_TYPE == 'api') {
    require_once 'products_api.php';
}

function product_defined_modules()
{
    global $sql;

    $sql->db_table_installer('Products', '1.04.2015',
        array(
            '_products'                => array(
                'pr_id'          => "`pr_id` int(11) NOT NULL AUTO_INCREMENT",
                'pr_title'       => "`pr_title` varchar(512) NOT NULL",
                'pr_price'       => "`pr_price` DOUBLE NOT NULL",
                'pr_short_desc'  => "`pr_short_desc` text NOT NULL",
                'pr_desc'        => "`pr_desc` text NOT NULL",
                'pr_url'         => "`pr_url` varchar(512) NOT NULL",
                'pr_image'       => "`pr_image` varchar(512) NOT NULL",
                'pr_c_id'        => "`pr_c_id` int(11) NOT NULL",
                'pr_create_date' => "`pr_create_date` datetime NOT NULL",
                'pr_change_date' => "`pr_change_date` datetime NOT NULL",
                'pr_enable'      => "`pr_enable` int(11) NOT NULL",
                'pr_count'       => "`pr_count` int(11) NOT NULL",
                'pr_lang'        => "`pr_lang` int(11) NOT NULL",
                'CREATE'         => "`pr_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`pr_id`)"
            ),
            '_products_options'        => array(
                'po_id'          => "`po_id` int(11) NOT NULL AUTO_INCREMENT",
                'po_title'       => "`po_title` varchar(512) NOT NULL",
                'po_option_key'  => "`po_option_key` varchar(512) NOT NULL",
                'po_values'      => "`po_values` text NOT NULL",
                'po_type'        => "`po_type` varchar(512) NOT NULL",
                'po_lang'        => "`po_lang` int(11) NOT NULL",
                'CREATE'         => "`po_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`po_id`)"
            ),
            '_products_options_values' => array(
                'pov_id'          => "`pov_id` int(11) NOT NULL AUTO_INCREMENT",
                'pov_po_id'       => "`pov_po_id` int(11) NOT NULL",
                'pov_value'       => "`pov_value` varchar(512) NOT NULL",
                'pov_object_id'   => "`pov_object_id` int(11) NOT NULL",
                'pov_object_type' => "`pov_object_type` varchar(512) NOT NULL",
                'pov_lang'        => "`pov_lang` int(11) NOT NULL",
                'CREATE'          => "`pov_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`pov_id`)"
            ),
            '_products_to_options' => array(
                'pto_id'          => "`pto_id` int(11) NOT NULL AUTO_INCREMENT",
                'pto_po_id'       => "`pto_po_id` int(11) NOT NULL",
                'pto_object_id'   => "`pto_object_id` int(11) NOT NULL",
                'pto_object_type' => "`pto_object_type` varchar(512) NOT NULL",
                'pto_lang'        => "`pto_lang` int(11) NOT NULL",
                'CREATE'          => "`pto_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`pto_id`)"
            )
        )
    );
}

/** Формування меню в адмінці  **/

if (PAGE_TYPE=='admin') {
    _add_filter('adminPanel_sidebar_menu', 'products_admin_sidebar_menu', 21);
    _add_filter('products_admin_sidebar_menu', 'products_admin_sidebar_menu_submenu');
}

function products_admin_sidebar_menu($result)
{

    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Продукты'), //ім"я пункта меню
            'url' => FALSE, //посилання
            'submenu_filter' => 'products_admin_sidebar_menu', //хук для підключення пунктів підменю
            'active_pages' => _run_filter('products_admin_sidebar_menu_active_items',
                    array(
                        'products',
                        'products/list',
                        'products/add',
                        'products/categories',
                        'products/categories/add',
                        'products/options',
                        'products/options/add',
                        'products/export-import'
                    )
                ), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => TRUE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_item','admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100), $menu);

    return $result . @$menu;
}

function products_admin_sidebar_menu_submenu($result){
    global $PData;

    $vars_array = array(
        array(
            'title' => _lang('Товары'), //ім"я пункта меню
            'url' => getURL('admin', 'products/list'), //посилання
            'active_pages' => array('products/list', 'products/add'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => getURL('admin','products/add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
        array(
            'title' => _lang('Категории'), //ім"я пункта меню
            'url' => getURL('admin', 'products/categories'), //посилання
            'active_pages' => array('products/categories', 'products/categories/add'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => getURL('admin','products/categories/add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
        array(
            'title' => _lang('Опции'), //ім"я пункта меню
            'url' => getURL('admin', 'products/options'), //посилання
            'active_pages' => array('products/options', 'products/options/add'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => getURL('admin','products/options/add') //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),

        array(
            'title' => _lang('Экспорт/импорт'), //ім"я пункта меню
            'url' => getURL('admin', 'products/export-import'), //посилання
            'active_pages' => array('products/export-import'), //на яких сторінках має бути активний (розгорнутий)
            'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
        ),
    );

    $menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

    $menu = u_ifRights(array(100), $menu);

    return $result . $menu;
}


/** Роутер Адмінки **/

if (PAGE_TYPE=='admin') {
    _add_action('adminPanel_controller', 'products_init_admin', 1);
}

function products_init_admin()
{
    global $sqlTPL, $PData, $admin, $contTPL, $list, $categories, $products;
    switch (@$_GET['admin']) {
        case 'products/list':
            require_once __DIR__ . '/apps_admin/products_list.php';
            return true;
            break;
        case 'products/add':
            require_once __DIR__ . '/apps_admin/products_add.php';
            return true;
            break;
        case 'products/edit':
            require_once __DIR__ . '/apps_admin/products_edit.php';
            return true;
            break;
        case 'products/categories':
            require_once __DIR__ . '/apps_admin/products_categories_list.php';
            return true;
            break;
        case 'products/categories/add':
            require_once __DIR__ . '/apps_admin/products_categories_add.php';
            return true;
            break;
        case 'products/categories/edit':
            require_once __DIR__ . '/apps_admin/products_categories_edit.php';
            return true;
            break;
        case 'products/options' :
            require_once __DIR__ . '/apps_admin/products_options_list.php';
            return true;
            break;
        case 'products/options/edit' :
            require_once __DIR__ . '/apps_admin/products_options_edit.php';
            return true;
            break;
        case 'products/options/add' :
            require_once __DIR__ . '/apps_admin/products_options_add.php';
            return true;
            break;
        case 'products/export-import':
            require_once __DIR__ . '/apps_admin/products_export_import.php';
            return true;
            break;

    }
}

_add_filter('admin_panel_header', 'products_headers');

function products_headers($result)
{
    $result .= '
         <link href="' . HOMEPAGE . 'js/jquery-ui/jquery-ui.min.css" type="text/css" rel="stylesheet"/>
    <link href="' . HOMEPAGE . 'js/jquery-ui/font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet"/>
    <link href="' . HOMEPAGE . 'js/chosen/chosen.admin.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="' . HOMEPAGE . 'js/chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="' . HOMEPAGE . 'js/jquery-ui/jquery-ui.min.js"></script>
    ';
    return $result;
}
