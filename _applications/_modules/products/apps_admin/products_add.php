<?php

global $sqlTPL, $PData, $admin, $contTPL, $list, $products;
$sqlTPL->getCategories('products');

$PData->content(_lang('Добавить продукт'),'title');

if (isset($_POST['product_add_confirm']) && isset($_POST['productAdd'])) {
    $products->add(@$_POST['productAdd']);
    $PData->content(_lang('Товар успешно добавлен!'), 'message', true);
}

/*
$options = $products->get_options($product['pr_id']);
$options = array_merge($options, $products->get_options($product['pr_c_id']));
$options_values = $products->get_product_options($product['pr_id']);
$options_values = array_merge($options_values, $products->get_product_options($product['pr_c_id']));
$temp = array();
foreach ($options as $val) {
    $temp[$val['po_id']] = $val;
}
$options = $temp;
unset($temp);
$temp = array();
foreach ($options_values as $val) {
    $temp[$val['pov_po_id']] = $val;
}
$options_values = $temp;
unset($temp);
$options_html = '';
//print_r($options_values);

foreach ($options as $option) {
    $options_html .= '<label>'.$option['po_title'].'</label>';
    switch ($option['po_type']) {
        case 'text': {
            $options_html .= '<input type="text" name="options['.$option['po_id'].'][value]" value="'.@$options_values[$option['po_id']]['pov_value'].'">';
        } break;
        case 'textarea': {
            $options_html .= '<textarea name="options['.$option['po_id'].'][value]">'.@$options_values[$option['po_id']]['pov_value'].'</textarea>';
        } break;
        case 'select': {
            $options_html .= '<select name="options['.$option['po_id'].'][value]">';

            foreach (explode("\n",$option['po_values']) as $op_val) {
                if (trim($op_val) == @$options_values[$option['po_id']]['pov_value'])
                    $options_html .= '<option selected value="'.trim($op_val).'">'.trim($op_val).'</option>';
                else
                    $options_html .= '<option value="'.trim($op_val).'">'.trim($op_val).'</option>';
            }
            $options_html .= '</select>';
        } break;
        case 'multiselect': {
            $options_html .= '<select multiple name="options['.$option['po_id'].'][value]">';
            foreach (explode("\n",$option['po_values']) as $op_val) {
                if (trim($op_val) == $options_values[$option['po_id']]['pov_value'])
                    $options_html .= '<option selected value="'.trim($op_val).'">'.trim($op_val).'</option>';
                else
                    $options_html .= '<option value="'.trim($op_val).'">'.trim($op_val).'</option>';
            }
            $options_html .= '</select>';
        } break;
        case 'checkbox': {
            $options_html .= '<input type="checkbox" name="options['.$option['po_id'].'][value]" value="'.$option['po_values'].'">';
        } break;
    }
}*/

$categories_select_list = '';

if (!empty($product['pr_c_id']) && isset($list->category['products']) && is_array($list->category['products'])) {
	$categories_select_list = $contTPL->catHierarchTree($list->category['products'], 0, 0);
}

$html = '<form class="list" method="POST" enctype="multipart/form-data" id="author_form">

		<div style="float: right;width:322px;">
			<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="product_edit_confirm" style="display: none;">
			<p>
			<label><input type="checkbox" class="input-xlarge" name="productAdd[enable]" value="1"> '._lang('Включен').'</label>
		</p>
			<p>
				<label>'._lang('Категория').':</label>
				<select name="productAdd[c_id]">
					'.$categories_select_list.'
				</select>
			</p>
			<p>
				<label>'._lang('Миниатюра').':</label>
				<input type="file" name="images[]" accept="image/jpeg,image/png">
			</p>
			<!-- Filter: Blog_post_Add_Page_2 -->

		</div>
		<p>
			<label>'._lang('Заголовок').'</label>
			<input type="text" class="input-xlarge" name="productAdd[title]" style="width: 500px;" value="">
		</p>
		<p>
			<label>'._lang('Цена').'</label>
			<input type="text" class="input-xlarge" name="productAdd[price]" style="width: 100px;" value="">
			&nbsp;'._lang('грн').'
		</p>
		<p>
			<label>'._lang('Количество').'</label>
			<input type="text" class="input-xlarge" name="productAdd[count]" style="width: 100px;" value="">
			&nbsp;шт
		</p>
		<p>
			<label>'._lang('Артикул (латинськими)').':</label>
			<input type="text" class="input-xlarge" name="productAdd[url]" style="width: 500px;" value="">
		</p>
		<p>
			<label>'._lang('Превью текст').':</label>
			<textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="productAdd[short_desc]"></textarea>
		</p>
		<!-- Filter: Blog_post_Add_Page_1 -->


		<p>
			<label>'._lang('Текст').':</label>
			'.$admin->getWIZIWIG('productAdd[desc]','','elm1').'
		</p>

		<hr>


		<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="product_add_confirm">
	</form>';

$PData->content($html);