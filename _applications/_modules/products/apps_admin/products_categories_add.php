<?php

$sqlTPL->getCategories('products');

if (isset($_POST['new_cat_submit']) && isset($_POST['categoryAdd'])) {
    if ($_POST['categoryAdd']['title'] == '')
        $PData->content(_lang('Заполните хотя бы название!'), 'message', true);
    else
    {
        $categories->add(array_map('_protect',@$_POST['categoryAdd']), 'products');
        $PData->content(_lang('Категория успешно добавлена'), 'message', true);
    }

}
$cats_html = '';
foreach ($categories->get(array('c_type="products"')) as $cat) {
    if ($PData->_GET('search_cat') == $cat['c_id']) $selected = 'selected'; else $selected = '';
    $cats_html .= '<option '.$selected.' value="' . $cat['c_id'] . '">' . $cat['c_title'] . '</option>';
}

$categoryAdd = $PData->_POST('categoryAdd');

if(!isset($categoryAdd['desc'])) {
	$categoryAdd['desc'] = '';
}

$PData->content(_lang('Добавить категорию'), 'title');
$html = '<form class="list" method="POST" enctype="multipart/form-data" id="author_form">

		<p>
			<label>'._lang('Заголовок').'</label>
			<input type="text" class="input-xlarge" name="categoryAdd[title]" style="width:500px;" value="">
		</p>
		<p>
			<label>'._lang('Артикул (латинськими)').':</label>
			<input type="text" class="input-xlarge" name="categoryAdd[url]" style="width:500px;" value="">
		</p>
		<p>
				<label>' . _lang('Родительская категория') . ':</label>
				<select name="categoryAdd[parent_id]">
					<option value="0">'._lang('-- Не указано --').'</option>
				    '.$cats_html.'
				</select>
			</p>
		<!-- Filter: Blog_post_Add_Page_1 -->


		<p>
			<label>' . _lang('Описание') . ':</label>
			' . $admin->getWIZIWIG('categoryAdd[desc]', $categoryAdd['desc'], 'elm1') . '
		</p>



		<input class="btn btn-primary" type="submit" value="Сохранить" name="new_cat_submit">
	</form>';

$PData->content($html);