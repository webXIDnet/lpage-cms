<?php
global $sqlTPL, $PData, $admin, $contTPL, $list, $categories;
$sqlTPL->getCategories('products');
$PData->content(_lang('Редактировать категорию'), 'title');

if (isset($_POST['edit_cat_submit']) && isset($_POST['categoryEdit'])) {
    $categories->update(@$_POST['categoryEdit']);
    $PData->content(_lang('Категория успешно обновлена!'), 'message', true);
}

$category = $categories->get(_protect(@$_GET['id']),'',true);
if (!$category) {
    $PData->content(_lang('Такого продукта не существует!'));
    return false;
}

$cats_html = '';
foreach ($categories->get(array('c_type="products"')) as $cat) {
    if ($cat['c_id'] == $category['c_parent_id']) $selected = 'selected'; else $selected = '';
    $cats_html .= '<option '.$selected.' value="' . $cat['c_id'] . '">' . $cat['c_title'] . '</option>';
}

$html = '<form class="list" method="POST" enctype="multipart/form-data" id="author_form">

		<p>
			<label>'._lang('Заголовок').'</label>
			<input type="text" class="input-xlarge" name="categoryEdit['.$category['c_id'].'][title]" style="width:500px;" value="'.$category['c_title'].'">
		</p>
		<p>
			<label>'._lang('Артикул (латинськими)').':</label>
			<input type="text" class="input-xlarge" name="categoryEdit['.$category['c_id'].'][url]" style="width:500px;" value="'.$category['c_url'].'">
		</p>
		<p>
				<label>' . _lang('Родительская категория') . ':</label>
				<select name="categoryEdit['.$category['c_id'].'][parent_id]">
					<option value="0">'._lang('-- Не указано --').'</option>
				    '.$cats_html.'
				</select>
			</p>
		<!-- Filter: Blog_post_Add_Page_1 -->


		<p>
			<label>' . _lang('Описание') . ':</label>
			' . $admin->getWIZIWIG('categoryEdit['.$category['c_id'].'][desc]', $category['c_desc'], 'elm1') . '
		</p>



		<input class="btn btn-primary" type="submit" value="Сохранить" name="edit_cat_submit">
	</form>';

$PData->content($html);