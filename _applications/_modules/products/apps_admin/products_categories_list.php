<?php
global $PData, $products, $categories;

$PData->content('Список категорий товаров', 'title');


if (isset($_POST['form_flag'])) {
    if (isset($_POST['categories_save_check'])) {
//        print_r($_POST['categoryEdit']);
        $categories->update($_POST['categoryEdit']);
        $PData->content(_lang('Категории успешно обновлены!'),'message');
    }

    if (isset($_POST['categories_move_check'])) {

    }

    if (isset($_POST['categories_del_check'])) {
        $categories->delete(@$_POST['checked']);
        $PData->content(_lang('Категории успешно удалены!'),'message');
    }
}

$filterdata = array();
if (isset($_GET['categories_search_check'])) {
    $s_id = isset($_GET['search_id']) && $_GET['search_id'] != '' ? 'c_id="' . $_GET['search_id'] . '"' : '';
    $s_title = isset($_GET['search_title']) && $_GET['search_title'] != '' ? 'c_title LIKE "%' . $_GET['search_title'] . '%"' : '';
    $filterdata = array($s_id, $s_title);
}
$filterdata []= "c_type='products'";
$nav = _pagination($categories->table,$categories->prefix.'id');
$cats = $categories->get($filterdata,$nav['limit']);

$html = "
    <div>
    <form method='get' action=''>
        <input type='text' name='search_id' value='".$PData->_GET('search_id')."' class='input-xlarge' placeholder='ID'/>
        <input type='text' name='search_title' value='".$PData->_GET('search_title')."' class='input-xlarge' placeholder='" . _lang('Название') . "'/>
         <button class='button-search' type='submit' name='categories_search_check'>" . _lang('Найти') . "</button>
    </div>
    </form>
    <hr>
    <form id='listform' action='' method='post'>
    <input type='hidden' name='form_flag' value='1'>
            <a class='button-add' href='".getURL('admin','products/categories/add')."'>" . _lang('Добавить категорию') . "</a>
            <div style='float:right'>
                <button class='button-save' name='categories_save_check'>" . _lang('Сохранить') . "</button>
                <button class='button-del' name='categories_del_check' style='background-color: #C81414;'>" . _lang('Удалить') . "</button>
            </div>
            <div style='clear:both;'></div>
            <hr>
        <table class='table'>
        <tr>
            <th style='width: 20px;'><input type='checkbox' class='check-all' onchange='javascript: if ($(this).prop(\"checked\") == true) {\$(\".checked\").attr(\"checked\",\"true\")} else {\$(\".checked\").removeAttr(\"checked\");} '></th>
            <th style='width: 20px;'>#</th>
            <th style='width: 20px;'>&nbsp;</th>
            <th>" . _lang('Название') . "</th>
            <th>" . _lang('URL') . "</th>
            <th>" . _lang('Родительская') . "</th>
        </tr>";

foreach ($cats as $row) {
    $parent = $categories->get(array('c_id=' . $row['c_parent_id'], "c_type='products'"),'',true);
    $html .= '
            <tr>
                <td><input type="checkbox" name="checked[]" value="' . _protect($row['c_id']) . '" class="checked"></td>
                <td>' . _protect($row['c_id']) . '</td>
                <td><a class="button-edit small-btn" href="' . getURL('admin', 'products/categories/edit', 'id=' . $row['c_id']) . '">'._lang('Редактировать').'</a></td>
                <td>' . _protect($row['c_title']) . '</td>
                <td>' . _protect($row['c_url']) . '</td>
                <td>' . (is_array($parent) ? _protect($parent['c_title']) : _lang('Корневая')) . '</td>
            </tr>
        ';
}

$html .= "
        </table>
        ".$nav['html']."
        <input type='submit' class='btn btn-primary' name='categories_save_check' value='" . _lang('Сохранить') . "'>
    </form>
";
$PData->content($html);

