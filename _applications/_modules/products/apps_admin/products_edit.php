<?php

global $sqlTPL, $PData, $admin, $contTPL, $list, $products;
$sqlTPL->getCategories('products');

$PData->content(_lang('Редактировать продукт'),'title');

if (isset($_POST['product_edit_confirm']) && isset($_POST['productEdit'])) {
    $products->update(@$_POST['productEdit']);
    $products->update_product_options(_protect(@$_GET['id']),@$_POST['options']);
    $PData->content(_lang('Товар успешно обновлен'), 'message', true);
}

$product = $products->get(_protect(@$_GET['id']),'',true);
if (!$product) {
    $PData->content(_lang('Такого продукта не существует!'));
    return false;
}
$options = $products->get_options($product['pr_id']);
$options = array_merge($options, $products->get_options($product['pr_c_id']));
$options_values = $products->get_product_options($product['pr_id']);
// $options_values = array_merge($options_values, $products->get_product_options($product['pr_c_id']));
$temp = array();
foreach ($options as $val) {
    $temp[$val['po_id']] = $val;
}
$options = $temp;
unset($temp);
$temp = array();
if (is_array($options_values)) {
	foreach ($options_values as $val) {
	    $temp[$val['pov_po_id']] = $val;
	}
}

$options_values = $temp;
unset($temp);
$options_html = '';
// print_r($products->get_product_options($product['pr_id']));
// print_r($products->get_product_options($product['pr_c_id']));
// var_dump($options_values);
foreach ($options as $option) {
    $options_html .= '<label>'.$option['po_title'].'</label>';
    switch ($option['po_type']) {
        case 'text': {
            $options_html .= '<input type="text" name="options['.$option['po_id'].'][value]" value="'.@$options_values[$option['po_id']]['pov_value'].'">';
        } break;
        case 'textarea': {
            $options_html .= '<textarea name="options['.$option['po_id'].'][value]">'.@$options_values[$option['po_id']]['pov_value'].'</textarea>';
        } break;
        case 'select': {
            $options_html .= '<select name="options['.$option['po_id'].'][value]">';

            foreach (explode("\n",$option['po_values']) as $op_val) {
            	// var_dump(trim($op_val));echo '<hr>';
            	// var_dump(@$options_values[$option['po_id']]['pov_value']);
                if (trim($op_val) == @$options_values[$option['po_id']]['pov_value'])
                    $options_html .= '<option selected value="'._protect(trim($op_val)).'">'.trim($op_val).'</option>';
                else
                    $options_html .= '<option value="'._protect(trim($op_val)).'">'.trim($op_val).'</option>';
            }
            $options_html .= '</select>';
        } break;
        case 'multiselect': {
            $options_html .= '<select multiple name="options['.$option['po_id'].'][value]">';
            foreach (explode("\n",$option['po_values']) as $op_val) {
                if (trim($op_val) == @$options_values[$option['po_id']]['pov_value'])
                    $options_html .= '<option selected value="'.trim($op_val).'">'.trim($op_val).'</option>';
                else
                    $options_html .= '<option value="'.trim($op_val).'">'.trim($op_val).'</option>';
            }
            $options_html .= '</select>';
        } break;
        case 'checkbox': {
        	if (isset($options_values[$option['po_id']])) $checked = 'checked'; else $checked = '';
            $options_html .= '<input '.$checked.' type="checkbox" name="options['.$option['po_id'].'][value]" value="'.$option['po_values'].'">';
        } break;
    }
}

$status = $product['pr_enable'] == '1' ? 'checked' : '';
$categories_select_list = '';

if (empty($product['pr_c_id']) ) $product['pr_c_id'] = 0;

if (isset($list->category['products']) && is_array($list->category['products'])) {
	$categories_select_list = $contTPL->catHierarchTree($list->category['products'], 0, $product['pr_c_id']);
}


$html = '<form class="list" method="POST" enctype="multipart/form-data" id="author_form">

		<div style="float: right;width:322px;">
			<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="product_edit_confirm" style="display: none;">
			<p>
			<label><input type="checkbox" '.$status.' class="input-xlarge" name="productEdit['.$product['pr_id'].'][enable]" value="1"> '._lang('Включен').'</label>
		</p>
			<p>
				<label>'._lang('Категория').':</label>
				<select name="productEdit['.$product['pr_id'].'][c_id]">
					'.$categories_select_list.'
				</select>
			</p>
			<p>
				<label>'._lang('Миниатюра').':</label>
				<input type="file" name="images[]" accept="image/jpeg,image/png">
			</p>
			<!-- Filter: Blog_post_Add_Page_2 -->

		</div>
		<p>
			<label>'._lang('Заголовок').'</label>
			<input type="text" class="input-xlarge" name="productEdit['.$product['pr_id'].'][title]" style="width: 500px;" value="'.$product['pr_title'].'">
		</p>
		<p>
			<label>'._lang('Цена').'</label>
			<input type="text" class="input-xlarge" name="productEdit['.$product['pr_id'].'][price]" style="width: 100px;" value="'.$product['pr_price'].'">
			&nbsp;'._lang('грн').'
		</p>
		<p>
			<label>'._lang('Количество').'</label>
			<input type="text" class="input-xlarge" name="productEdit['.$product['pr_id'].'][count]" style="width: 100px;" value="'.$product['pr_count'].'">
			&nbsp;шт
		</p>
		<p>
			<label>'._lang('Артикул (латинськими)').':</label>
			<input type="text" class="input-xlarge" name="productEdit['.$product['pr_id'].'][url]" style="width: 500px;" value="'.$product['pr_url'].'">
		</p>
		<p>
			<label>'._lang('Превью текст').':</label>
			<textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="productEdit['.$product['pr_id'].'][short_desc]">'.$product['pr_short_desc'].'</textarea>
		</p>
		<!-- Filter: Blog_post_Add_Page_1 -->


		<p>
			<label>'._lang('Текст').':</label>
			'.$admin->getWIZIWIG('productEdit['.$product['pr_id'].'][desc]',$product['pr_desc'],'elm1').'
		</p>

		<hr>
        <p>
            '.$options_html.'
        </p>



		<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="product_edit_confirm">
	</form>';

$PData->content($html);