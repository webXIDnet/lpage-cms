<?php

if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
global $sql;
$result = $sql->query("SELECT pr_title,pr_price,pr_desc,pr_short_desc,pr_url,pr_image,pr_enable,pr_count,pr_lang FROM _products ");
	if(isset($_REQUEST['export'])){
		$csv_ar = $csv =
			_lang('pr_title').";".
			_lang('pr_price').";".
			_lang('pr_desc').";".
			_lang('pr_short_desc').";".
			_lang('pr_url').";".
			_lang('pr_image').";".
			_lang('pr_enable').";".
			_lang('pr_count').";".
			_lang('pr_lang')."\n";
		foreach($result AS $cval){
			unset($csv_ar);

			$csv_ar['pr_title'] = $cval['pr_title'];

			$csv_ar['pr_price'] = $cval['pr_price'];
			$cval['pr_desc'] = str_replace("\n",'',$cval['pr_desc']);
            $cval['pr_desc'] = str_replace("\r",'',$cval['pr_desc']);
            $csv_ar['pr_desc'] = str_replace("\t",'',$cval['pr_desc']);
			$cval['pr_short_desc'] = str_replace("\n",'',$cval['pr_short_desc']);
            $cval['pr_short_desc'] = str_replace("\t",'',$cval['pr_short_desc']);
            $csv_ar['pr_short_desc'] = str_replace("\r",'',$cval['pr_short_desc']);

			$csv_ar['pr_url'] = $cval['pr_url'];
			$csv_ar['pr_image'] = $cval['pr_image'];

			$csv_ar['pr_enable'] = $cval['pr_enable'];
			//$csv_ar['Посадка'] = !empty($cval['o_lp_article'])?getURL('lp',$cval['o_lp_article']):'---';

			$csv_ar['pr_count'] = $cval['pr_count'];

			$csv_ar['pr_lang'] = $cval['pr_lang'];
			$csv .= implode(';',$csv_ar)."\n";

		}

		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=export_".date('d.m.Y_H.i').".csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		print iconv('UTF-8','cp1251//TRANSLIT',$csv);
		$PData->phpExit();

	}
    $nd=$sql->query("select NOW() as date_now");

    if(isset($_REQUEST['import'])){
        $row = 1;
        if (!empty($_FILES['uploadfile']['tmp_name'])) {
            if (($handle = fopen($_FILES['uploadfile']['tmp_name'], "r")) !== FALSE) {
                $title_arr=array();
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    $num = count($data);
                    if($row==1){
                        for ($c=0; $c < $num; $c++) {
                            $title_arr[]=iconv('cp1251//TRANSLIT','UTF-8',$data[$c]);
                        }
                        $title_arr[]="pr_create_date";
                        $title_arr[]="pr_change_date";

                    }else{
                        $insert=array();
                        for ($c=0; $c < $num; $c++) {
                            $insert[$title_arr[$c]]=$data[$c];
                        }
                        $insert[$title_arr[$num]]=$nd[0]["date_now"];
                        $insert[$title_arr[$num+1]]=$nd[0]["date_now"];
                        $sql->insert("_products",$insert);
                        unset($insert);
                    }
                    $row++;
                }
                fclose($handle);
            }
        } else {
            $PData->content('Не выбран файл для импорта!', 'message', FALSE);
        }
    }

    if(isset($_REQUEST['import_url'])){
        $row = 1;
        if (($handle = fopen($_FILES['uploadfile']['tmp_name'], "r")) !== FALSE) {
            $title_arr=array();
            $insert_arr=array();
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data);
                if($row==1){
                    for ($c=0; $c < $num; $c++) {
                        $title_arr[]=iconv('cp1251//TRANSLIT','UTF-8',$data[$c]);
                    }
                    $title_arr[]="pr_create_date";
                    $title_arr[]="pr_change_date";
                }else{
                    $insert=array();
                    for ($c=0; $c < $num; $c++) {
                        if($title_arr[$c]=="pr_url")$data[$c]=$data[$c]."_".(time());
                        $insert[$title_arr[$c]]=$data[$c];
                    }
                    $insert[$title_arr[$num]]=$nd[0]["date_now"];
                    $insert[$title_arr[$num+1]]=$nd[0]["date_now"];
                    $sql->insert("_products",$insert);
                    unset($insert);
                }
                $row++;
            }
            fclose($handle);
        }
    }

    $html='<form class="list" method="POST" enctype="multipart/form-data" id="author_form">
		<input type=file name=uploadfile>
        <input class="btn btn-primary" type="submit" value="'._lang('Экспорт').'" name="export">
        <input class="btn btn-primary" type="submit" value="'._lang('Импорт').'" name="import">
        <input class="btn btn-primary" type="submit" value="'._lang('Импорт - уникальный url').'" name="import_url">
	</form>';
    $PData->content($html);
