<?php

global $PData, $products, $categories;
$PData->content(_lang('Список продуктов'), 'title');


if (isset($_POST['form_flag'])) {
    if (isset($_POST['product_save_check'])) {
        if (isset($_POST['productEdit']))
        $products->update($_POST['productEdit']);
        $PData->content(_lang('Товары успешно обновлены!'), 'message');
    }

    if (isset($_POST['product_move_check']) || isset($_POST['product_move_check_jq'])) {
//        $prods =
        foreach ( @$_POST['productEdit'] as $k => $v ) {
            @$_POST['productEdit'][$k]['c_id'] = @$_POST['action_cat'];
        }
        $products->update(@$_POST['productEdit']);
        $PData->content(_lang('Товары успешно перенесены!'), 'message');
    }

    if (isset($_POST['product_copy_check']) || isset($_POST['product_copy_check_jq'])) {
        $products->duplicate(@$_POST['checked'],_protect(@$_POST['action_cat']));
        $PData->content(_lang('Товары успешно продублированы!'), 'message');
    }

    if (isset($_POST['product_del_check'])) {
        $products->delete(@$_POST['checked']);
        $PData->content(_lang('Товары успешно удалены!'), 'message');
    }
}

$filterdata = '';
if (isset($_GET['product_search_check'])) {
    $s_id = isset($_GET['search_id']) && $_GET['search_id'] != '' ? 'pr_id="' . $_GET['search_id'] . '"' : '';
    $s_title = isset($_GET['search_title']) && $_GET['search_title'] != '' ? 'pr_title LIKE "%' . $_GET['search_title'] . '%"' : '';
    $s_cat = isset($_GET['search_cat']) && $_GET['search_cat'] != '' ? 'pr_c_id="' . $_GET['search_cat'] . '"' : '';
    $filterdata = array($s_id, $s_title, $s_cat);
}
$nav = _pagination($products->table,$products->prefix.'id');
$prods = $products->get($filterdata,$nav['limit']);
$cats_html = '';
foreach ($categories->get(array('c_type="products"')) as $cat) {
    if ($PData->_GET('search_cat') == $cat['c_id']) $selected = 'selected'; else $selected = '';
    $cats_html .= '<option '.$selected.' value="' . $cat['c_id'] . '">' . $cat['c_title'] . '</option>';
}
$html = "
    <div>
    <form method='get' action=''>
        <input type='text' name='search_id' value='".$PData->_GET('search_id')."' class='input-xlarge' placeholder='ID'/>
        <input type='text' name='search_title' value='".$PData->_GET('search_title')."' class='input-xlarge' placeholder='" . _lang('Название') . "'/>
        <select name='search_cat' class='input-xlarge chosen-selectbox'>
            <option value=''>"._lang('Все категории')."</option>
            " . $cats_html . "
        </select>
         <button class='button-search' type='submit' name='product_search_check'>" . _lang('Найти') . "</button>
    </div>
    </form>
    <hr>
    <form id='listform' action='' method='post'>
    <input type='hidden' name='form_flag' value='1'>
            <a class='button-add' href='" . getURL('admin', 'products/add') . "'>" . _lang('Добавить продукт') . "</a>

            <div style='float:right'>
                <button class='button-save' name='product_save_check'>" . _lang('Сохранить') . "</button>
                <button class='button-move' name='product_move_check'>" . _lang('Переместить') . "</button>
                <button class='button-copy' name='product_copy_check'>" . _lang('Дублировать') . "</button>
                <button class='button-del' name='product_del_check' style='background-color: #C81414;'>" . _lang('Удалить') . "</button>
            </div>
            <div style='float: right; height: 38px; margin-right: 10px;'>
            <div id='categories_dialog' title='"._lang('Выберите категорию')."'>
                <p style='text-align: center; margin: 0px;'>
                    <select name='action_cat' class='input-xlarge chosen-selectbox1'>
                        <option value='0'>"._lang('Корневая категория')."</option>
                        " . $cats_html . "
                        </select>
                </p>
            </div>
            </div>
            <div style='clear:both;'></div>
            <hr>
        <table class='table'>
        <tr>
            <th><input type='checkbox' class='check-all' onchange='javascript: if ($(this).prop(\"checked\") == true) {\$(\".checked\").attr(\"checked\",\"true\")} else {\$(\".checked\").removeAttr(\"checked\");} '></th>
            <th>#</th>
            <th>&nbsp;</th>
            <th>" . _lang('Изображение') . "</th>
            <th>" . _lang('Название') . "</th>
            <th>" . _lang('Категория') . "</th>
            <th>" . _lang('Цена') . "</th>
            <th>" . _lang('Количество') . "</th>
            <th style='width: 105px;'>" . _lang('Статус') . "</th>
            <th>" . _lang('Дата изменения') . "</th>
        </tr>";

foreach ($prods as $row) {
    $category = $categories->get(array('c_id=' . $row['pr_c_id'], "c_type='products'"),'',true);
    $status = $row['pr_enable'] > 0 ? 'checked' : '';
    $image = json_decode($row['pr_image'], true);
    $image = !empty($image[0]) ? '<img src="' . $image[0] . '">' : '<img src="/media/_admin/no-img.png">';
    $html .= '
            <tr>
                <td>
                    <input type="checkbox" name="checked[]" value="' . _protect($row['pr_id']) . '" class="checked">
                </td>
                <td>
                    ' . _protect($row['pr_id']) . '
                </td>
                <td>
                    <a class="button-edit small-btn" href="' . getURL('admin', 'products/edit', 'id=' . $row['pr_id']) . '">' . _lang('Редактировать') . '</a>
                </td>
                <td>
                    <a href="' . getURL('admin', 'products/edit', 'id=' . $row['pr_id']) . '">' . $image . '</a></td>
                <td>
                    <input type="text" name="productEdit[' . _protect($row['pr_id']) . '][title]" class="input-xlarge" value="' . _protect($row['pr_title']) . '">
                </td>
                <td>
                    ' . _protect($category['c_title']) . '
                </td>
                 <td>
                    <input type="text" name="productEdit[' . _protect($row['pr_id']) . '][price]" class="input-xlarge" value="' . _protect($row['pr_price']) . '">
                </td>
                <td>
                    <input type="text" name="productEdit[' . _protect($row['pr_id']) . '][count]" class="input-xlarge" value="' . _protect($row['pr_count']) . '">
                </td>
                <td>
                    <label><input type="checkbox" name="productEdit[' . _protect($row['pr_id']) . '][enable]" value="1" ' . $status . '> ' . _lang('Включен') . '</label>
                </td>
                <td>
                    ' . date('d.m.Y H:i:s', strtotime($row['pr_change_date'])) . '
                </td>
            </tr>
        ';
}

$html .= "
        </table>
        ".$nav['html']."
        <input type='submit' class='btn btn-primary' name='product_save_check' value='" . _lang('Сохранить') . "'>

    </form>

    <script type='text/javascript'>
        $(document).ready(function(){
            $('#categories_dialog').dialog({
                appendTo: '#listform',
                autoOpen: false,
                resizable: false,
                height: 180,
                modal: true,
                buttons: [{
                    text: 'OK',
                    click: function() {
//                            $('*[name=\"action_cat\"]').css('visibility','hidden').appendTo('#listform');
                            $(this).dialog( 'close' );
                            $('#listform').submit();
                         }
                }]
                  /*buttons: {
                        'OK': function() {
//                            $('*[name=\"action_cat\"]').css('visibility','hidden').appendTo('#listform');
                            $(this).dialog( 'close' );
                            $('#listform').submit();
                         }
                 }*/
            });
            $('.button-move,.button-copy').on('click',function(){
            $('#categories_dialog').dialog('option','buttons', [{
                    text: 'OK',
                    name: $(this).attr('name')+'_jq',
                    value: 1,
                    type: 'submit',
                    click: function() {
//                            $('*[name=\"action_cat\"]').css('visibility','hidden').appendTo('#listform');
//                            $(this).dialog( 'close' );
//                            $('#listform').submit();
                         }
                }]);
                $('#categories_dialog').dialog('open');
                return false;
            });
        });
    </script>
";
$PData->content($html);

