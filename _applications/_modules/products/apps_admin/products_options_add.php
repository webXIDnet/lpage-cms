<?php
global $products, $categories;

if (isset($_POST['new_option']) && isset($_POST['optionAdd'])) {

    if ($_POST['optionAdd']['title'] == '')
        $PData->content(_lang('Заполните хотя бы название!'), 'message', true);
    else
    {

        $data_arr = $_POST;
        /*if (isset($data_arr['optionEditprod'])) {
            @$data_arr['optionAdd']['object_id'] = @$data_arr['optionEditprod']['object_id'];
            @$data_arr['optionAdd']['object_type'] = 'product';
        } else
            if (isset($data_arr['optionEditcat'])) {
                @$data_arr['optionAdd']['object_id'] = @$data_arr['optionEditcat']['object_id'];
                @$data_arr['optionAdd']['object_type'] = 'category';
            }*/
            //print_r($data_arr);exit;
            $insert_id = $products->add_option($data_arr['optionAdd']);
            if (isset($data_arr['optionEditprod'])) {
            $products->link_options(_protect($insert_id), $data_arr['optionEditprod']['object_id'], 'product');
        } 
        if (isset($data_arr['optionEditcat'])) {
            $products->link_options(_protect($insert_id), $data_arr['optionEditcat']['object_id'], 'category');
        }
            /*foreach ($data_arr['optionAdd']['object_id'] as $obj_id) {
                $data = @$data_arr['optionAdd'];
                $data['object_id'] = $obj_id;
                
            }*/
        
        $PData->content(_lang('Опция успешно добавлена'), 'message', true);
    }

}
$cats_html = $prods_html = '';
$p = $products->get();
$c = $categories->get(array($categories->prefix."type='products'"));
foreach ($p as $key => $value) {
    $prods_html .= '<option value="'.$key.'">'.$value[$products->prefix.'title'].'</option>';
}
foreach ($c as $key => $value) {
    $cats_html .= '<option value="'.$key.'">'.$value[$categories->prefix.'title'].'</option>';
}
$PData->content(_lang('Добавить опцию'), 'title');
$html = '<form class="list" method="POST" enctype="multipart/form-data" id="author_form">

		<p>
			<label>'._lang('Название').'</label>
			<input type="text" class="input-xlarge" name="optionAdd[title]" style="width:500px;" value="">
		</p>
		<p>
			<label>'._lang('Ключ').'</label>
			<input type="text" class="input-xlarge" name="optionAdd[option_key]" style="width:500px;" value="">
		</p>
		<p>
			<label>'._lang('Тип').':</label>
			<select name="optionAdd[type]">
			    <option value="text">'._lang('Текстовое поле').'</option>
			    <option value="select">'._lang('Выпадающий список').'</option>
			    <option value="multiselect">'._lang('Список с мультивыбором').'</option>
			    <option value="checkbox">'._lang('Галка').'</option>
			    <option value="textarea">'._lang('Текстовая область').'</option>
            </select>
		</p>
		<p>
			<label>'._lang('Привязать к:').'</label>
			    
		</p>
		<p class="prod-l link">
			<label>'._lang('Выберите продукт:').'</label>
			<select multiple class="ajax-select" name="optionEditprod[object_id][]">
			    <option value="0">-- Не выбрано --</option>
                '.$prods_html.'
            </select>
		</p>
		<p class="cat-l link">
			<label>'._lang('Выберите категорию:').'</label>
			<select multiple class="ajax-select" name="optionEditcat[object_id][]">
			    <option value="0">-- Не выбрано --</option>
                '.$cats_html.'
            </select>
		</p>

		<p>
			<label>'._lang('Значения (для списков - каждое с новой строки)').':</label>
			<textarea name="optionAdd[values]"></textarea>
		</p>

		<!-- Filter: Blog_post_Add_Page_1 -->
<script type="text/javascript">
    $(document).ready(function(){
        $(".link-to-cat").click(function(){
            $("p.link").hide(0);
            $("p.cat-l").show(0);
        });
        $(".link-to-prod").click(function(){
            $("p.link").hide(0);
            $("p.prod-l").show(0);
        });

        $("p.prod-l select").select2({
            /*ajax: {
                url: "/api/products/get_products/",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    return {
                    q: params.term, // search term
                    page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1*/
        });
        $("p.cat-l select").select2({
             /*ajax: {
                url: "/api/categories/get_categories/",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    return {
                    q: params.term, // search term
                    type: "products"
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1*/
        });
//$("p.cat-l select").chosen();
        $("p.link.hidden").hide(0);
    });
</script>



		<input class="btn btn-primary" type="submit" value="Сохранить" name="new_option">
	</form>';

$PData->content($html);