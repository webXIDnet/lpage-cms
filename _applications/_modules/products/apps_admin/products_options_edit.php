<?php

global $sqlTPL, $PData, $admin, $contTPL, $list, $products, $categories;

$PData->content(_lang('Редактировать опцию'),'title');

//get option links and its data
$option = $products->get_option_by_id(_protect(@$_GET['id']));

//updating option
$data = array();
if (isset($_POST['edit_option']) && isset($_POST['optionEdit'])) {
    $data_arr = $_POST;
    // print_r($data_arr);
        if (isset($data_arr['optionEditprod'])) {
            $products->link_options(_protect(@$_GET['id']), $data_arr['optionEditprod']['object_id'], 'product');
        } 
        if (isset($data_arr['optionEditcat'])) {
            $products->link_options(_protect(@$_GET['id']), $data_arr['optionEditcat']['object_id'], 'category');
        }
            $products->update_option($data_arr['optionEdit']);
    $PData->content(_lang('Опция успешно обновлена'), 'message', true);
}
$objects = array(); $cats_list = $prods_list = '';

//set linked products selected
/*if ($option['po_object_type'] == 'product') {
    $link_obj = array();
    foreach ($opt_list as $key => $value) {
        $tempv = $products->get($value[$products->options_prefix.'object_id'], '', true);
        $link_obj [$tempv[$products->prefix.'id']]= $tempv;
    }
    $objects = $products->get();
    foreach ($objects as $obj) {
        if (isset($link_obj[$obj[$products->prefix.'id']])) $selected = 'selected'; else $selected = '';

        $prods_list .= '<option '.$selected.' value="'.$obj[$products->prefix.'id'].'">'.$obj[$products->prefix.'title'].'</option>';
    }
    $objects = $categories->get(array($categories->prefix."type='products'"));
    foreach ($objects as $obj) {
        $cats_list .= '<option value="'.$obj[$categories->prefix.'id'].'">'.$obj[$categories->prefix.'title'].'</option>';
    }
//set linked categories selected
} else if ($option['po_object_type'] == 'category') {
    $link_obj = array();
    foreach ($opt_list as $key => $value) {
        $tempv = $categories->get($value[$products->options_prefix.'object_id'], '', true);
        $link_obj [$tempv[$categories->prefix.'id']]= $tempv;
    }
    $objects = $categories->get(array($categories->prefix."type='products'"));
    foreach ($objects as $obj) {
        if (isset($link_obj[$obj[$categories->prefix.'id']])) $selected = 'selected'; else $selected = '';
        $cats_list .= '<option '.$selected.' value="'.$obj[$categories->prefix.'id'].'">'.$obj[$categories->prefix.'title'].'</option>';
    }
    $objects = $products->get();
    foreach ($objects as $obj) {
        $prods_list .= '<option value="'.$obj[$products->prefix.'id'].'">'.$obj[$products->prefix.'title'].'</option>';
    }
}*/
$opt_links = $products->get_option_links(_protect(@$_GET['id']));
$links = array('products' => array(), 'categories' => array());
foreach ($opt_links as $key => $value) {
    if ($value['type'] == 'product') $links['products'][] = $value['id'];
    if ($value['type'] == 'category') $links['categories'][] = $value['id'];
}
foreach ($products->get() as $key => $value) {
    if (in_array($value[$products->prefix.'id'], @$links['products'])) $selected = 'selected'; else $selected = '';
    $prods_list .= '<option '.$selected.' value="'.$value[$products->prefix.'id'].'">'.$value[$products->prefix.'title'].'</option>';
}
foreach ($categories->get(array($categories->prefix."type='products'")) as $key => $value) {
    if (in_array($value[$categories->prefix.'id'], @$links['categories'])) $selected = 'selected'; else $selected = '';
    $cats_list .= '<option '.$selected.' value="'.$value[$categories->prefix.'id'].'">'.$value[$categories->prefix.'title'].'</option>';
}
//if ID is not set in DB - error
if (!$option) {
    $PData->content(_lang('Такой опции не существует!'));
    return false;
}

//get option info
$option = $products->get_option_by_id(_protect(@$_GET['id']));

//view
$html = '<form class="list" method="POST" enctype="multipart/form-data" id="author_form">

		<p>
			<label>'._lang('Название').'</label>
			<input type="text" class="input-xlarge" name="optionEdit['.$option[$products->options_prefix.'id'].'][title]" style="width:500px;" value="'.$option['po_title'].'">
		</p>
		<p>
			<label>'._lang('Ключ').'</label>
			<input type="text" class="input-xlarge" name="optionEdit['.$option[$products->options_prefix.'id'].'][option_key]" style="width:500px;" value="'.$option['po_option_key'].'">
		</p>
		<p>
			<label>'._lang('Тип').':</label>
			<select name="optionEdit['.$option[$products->options_prefix.'id'].'][type]">
			    <option value="text" '.($option['po_type'] == 'text' ? 'selected' : '').'>'._lang('Текстовое поле').'</option>
			    <option value="select" '.($option['po_type'] == 'select' ? 'selected' : '').'>'._lang('Выпадающий список').'</option>
			    <option value="multiselect" '.($option['po_type'] == 'multiselect' ? 'selected' : '').'>'._lang('Список с мультивыбором').'</option>
			    <option value="checkbox" '.($option['po_type'] == 'checkbox' ? 'selected' : '').'>'._lang('Галка').'</option>
			    <option value="textarea" '.($option['po_type'] == 'textarea' ? 'selected' : '').'>'._lang('Текстовая область').'</option>
            </select>
		</p>
		<p>
			<label>'._lang('Привязать к:').'</label>
			    
		</p>
		<p class="prod-l link">
			<label>'._lang('Выберите продукт:').'</label>
			<select multiple class="ajax-select" '.(count($objects) > 1 ? 'multiple' : '').' name="optionEditprod[object_id][]">
			    <option value="0">-- Не выбрано --</option>
			    '.$prods_list.'
            </select>
		</p>
		<p class="cat-l link">
			<label>'._lang('Выберите категорию:').'</label>
			<select multiple class="ajax-select" '.(count($objects) > 1 ? 'multiple' : '').' name="optionEditcat[object_id][]">
			    <option value="0">-- Не выбрано --</option>
			    '.$cats_list.'
            </select>
		</p>

		<p>
			<label>'._lang('Значения (каждое с новой строки)').':</label>
			<textarea name="optionEdit['.$option[$products->options_prefix.'id'].'][values]">'.$option['po_values'].'</textarea>
		</p>

		<!-- Filter: Blog_post_Add_Page_1 -->

<script type="text/javascript">
    $(document).ready(function(){
        $(".link-to-cat").click(function(){
            $("p.link").hide(0);
            $("p.cat-l").show(0);
        });
        $(".link-to-prod").click(function(){
            $("p.link").hide(0);
            $("p.prod-l").show(0);
        });

        $("p.prod-l select").chosen({
            /*ajax: {
                url: "/api/products/get_products/",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    return {
                    q: params.term, // search term
                    page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1
                */
        });
        $("p.cat-l select").chosen({
             /*ajax: {
                url: "/api/categories/get_categories/",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    return {
                    q: params.term, // search term
                    type: "products"
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
                    cache: true
                },
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1
                */
        });
        $("p.link.hidden").hide(0);
    });
</script>


		<input class="btn btn-primary" type="submit" value="Сохранить" name="edit_option">
	</form>';

$PData->content($html);