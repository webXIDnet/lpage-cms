<?php
global $PData, $products, $categories;

$PData->content(_lang('Список опций товаров'),'title');


if (isset($_POST['form_flag'])) {
    if (isset($_POST['save_check'])) {
//        print_r($_POST['categoryEdit']);
        $categories->update($_POST['categoryEdit']);
        $PData->content(_lang('Опции успешно обновлены!'),'message');
    }



    if (isset($_POST['del_check'])) {
        $products->remove_option(@$_POST['checked']);
        $PData->content(_lang('Опции успешно удалены!'),'message');
    }
}

$filterdata = array();
if (isset($_GET['options_search'])) {
    $s_id = isset($_GET['search_id']) && $_GET['search_id'] != '' ? 'po_id="' . $_GET['search_id'] . '"' : '';
    $s_title = isset($_GET['search_title']) && $_GET['search_title'] != '' ? 'po_title LIKE "%' . $_GET['search_title'] . '%"' : null;
    $filterdata = array($s_id, $s_title);
}
//$filterdata []= "po_type='products'";
$filterdata = array_diff($filterdata,array(''));
$nav = _pagination($products->options_table,$products->options_prefix.'id');
$options = $products->get_options(false, 'all', implode(' AND ',$filterdata),$nav['limit']);

$html = "
    <div>
    <form method='get' action=''>
        <input type='text' name='search_id' value='".$PData->_GET('search_id')."' class='input-xlarge' placeholder='ID'/>
        <input type='text' name='search_title' value='".$PData->_GET('search_title')."' class='input-xlarge' placeholder='" . _lang('Название') . "'/>
         <button class='button-search' type='submit' name='options_search'>" . _lang('Найти') . "</button>
    </div>
    </form>
    <hr>
    <form id='listform' action='' method='post'>
    <input type='hidden' name='form_flag' value='1'>
            <a class='button-add' href='".getURL('admin','products/options/add')."'>" . _lang('Добавить опцию') . "</a>
            <div style='float:right'>
                <button class='button-del' name='del_check' style='background-color: #C81414;'>" . _lang('Удалить') . "</button>
            </div>
            <div style='clear:both;'></div>
            <hr>
        <table class='table'>
        <tr>
            <th style='width: 20px;'><input type='checkbox' class='check-all' onchange='javascript: if ($(this).prop(\"checked\") == true) {\$(\".checked\").attr(\"checked\",\"true\")} else {\$(\".checked\").removeAttr(\"checked\");} '></th>
            <th style='width: 20px;'>#</th>
            <th style='width: 20px;'>&nbsp;</th>
            <th>" . _lang('Название') . "</th>
            <th>" . _lang('Тип') . "</th>
            <th>" . _lang('Привязка') . "</th>
        </tr>";

foreach ($options as $row) {
    $opt_links = $products->get_option_links(_protect($row[$products->options_prefix.'id']));
    $obj_text = $pr_t = $c_t = '';
    $links = array('products' => array(), 'categories' => array());
    // var_dump($opt_links);
    foreach ($opt_links as $key => $value) {
        if ($value['type'] == 'category') {
            $obj = $categories->get($value['id'],'',true);
            // var_dump($obj);
            $links['categories'][] = _protect($obj[$categories->prefix.'title']);
        }
        if ($value['type'] == 'product') {
            $obj = $products->get($value['id'],'',true);
            $links['products'][] = _protect($obj[$products->prefix.'title']);
        }
    }
    if (count($links['products']) > 0)
        $obj_text .= _lang('Продукты').': '.implode(', ',$links['products']).'<br>';
    if (count($links['categories']) > 0)
        $obj_text .= _lang('Категории').': '.implode(', ',$links['categories']);
    $html .= '
            <tr>
                <td><input type="checkbox" name="checked[]" value="' . _protect($row['po_id']) . '" class="checked"></td>
                <td>' . _protect($row['po_id']) . '</td>
                <td><a class="button-edit small-btn" href="' . getURL('admin', 'products/options/edit', 'id=' . $row['po_id']) . '">'._lang('Редактировать').'</a></td>
                <td>' . _protect($row['po_title']) . '</td>
                <td>' . _protect($row['po_type']) . '</td>
                <td>' . @$obj_text . '</td>
            </tr>
        ';
}

$html .= "
        </table>
        ".$nav['html']."
        <input type='submit' class='btn btn-primary' name='save_check' value='" . _lang('Сохранить') . "'>
    </form>
";
$PData->content($html);

