<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 08.07.2015
 * Time: 17:58
 */
$products = $vars_array['products'];
$category = array_pop($vars_array['categories']);
$time = (int)microtime(true);
?>
<h3 id="link-1"><?php echo $category['c_title'] ?>
    <span>
        <a href="#" class="arrow-prev" id="prev-<?php echo $time ?>"></a>
        <a href="#" class="arrow-next" id="next-<?php echo $time ?>"></a>
    </span>
</h3>
<div class="row item-row goods-slider" id="goods-slider-<?php echo $time ?>">
    <?php
    foreach ($products as $product) {
        ?>
        <div class="col15-lg-3">
            <a href="#" class="item good-maker" data-toggle="modal"
               data-target="#good-maker-<?php echo $product['pr_id'] ?>"
               good-title="<?php echo $product['pr_title'] ?>">
                <?php echo $product['pr_title'] ?>
            </a>
        </div>
    <?php
    }
    ?>
</div>
<?php
foreach ($products as $product) {
    ?>

    <div good-title="<?php echo $product['pr_title'] ?>" style="display: none;"
         class="modal fade modal-good good-maker" id="good-maker-<?php echo $product['pr_id'] ?>" tabindex="-1"
         role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 img-holder">
                            <img src="http://vsmetanin.com/media/_smetanin-business/img/good-img.jpg" alt="img"
                                 id="get-img">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                            <p class="good-title" ><?php echo $product['pr_title'] ?></p>
                            <div class="good-desc">
                                <?php echo $product['pr_desc'] ?>
                            </div>
                            <div class="button-holder">
                                <a href="#" class="order-good" data-toggle="modal" data-target="#new-good-order"
                                   data-dismiss="modal">Оставить заявку</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
}
/* bootstrap modality
<!-- Trigger the modal with a button -->
    <button type="button" class="item good-maker" data-toggle="modal" data-target="#myModal-<?php echo $product['pr_id'] ?>"><?php echo $product['pr_title'] ?></button>

<!-- Modal -->
<div id="myModal-<?php echo $product['pr_id'] ?>" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $product['pr_title'] ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $product['pr_desc'] ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
*/
//scrtipt to activate rrows
?>
<script type="text/javascript">
    $("#prev-<?php echo $time ?>").click(function (e) {
        e.preventDefault();
        $('#goods-slider-<?php echo $time ?> .slick-prev').click();
    });
    $("#next-<?php echo $time ?>").click(function (e) {
        e.preventDefault();
        $('#goods-slider-<?php echo $time ?> .slick-next').click();
    });
</script>