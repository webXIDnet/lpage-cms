<?php
class products_api {
    function __constructor() {

    }

    public function get_products() {
        global $sql;
        $data = $sql->query("SELECT * FROM _products WHERE pr_title LIKE '%".@$_GET['q']."%' LIMIT 0,10 ",'array');
        $result = array();
        foreach ($data as $key => $val) {
            $temp = array();
            $temp['id'] = $val['pr_id'];
            $temp['text'] = $val['pr_title'];
            $result []= $temp;
            unset($temp);
        }
        echo json_encode($result);
    }
}
global $api;
$api->accept('products','get_products',true);