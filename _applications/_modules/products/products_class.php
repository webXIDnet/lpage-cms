<?php

/**
 * Products class
 */
class Products
{

    /**
     * Products table's fields prefix
     * @var string
     */
    public $prefix = 'pr_';

    /**
     * Options table prefix
     * @var string
     */
    public $options_prefix = 'po_';

    public $options_values_prefix = 'pov_';

    /**
     * Products table
     * @var string
     */
    public $table = '_products';

    /**
     * Products options table
     * @var string
     */
    public $options_table = '_products_options';

    public $options_values_table = '_products_options_values';

    public $opt_link_table = '_products_to_options';

    public $opt_link_prefix = 'pto_';

    /**
     * @param string $table
     * @param string $prefix
     */
    function __construct($table = '', $prefix = '')
    {
        if ($prefix) {
            $this->prefix = $prefix;
        }
        if ($table) {
            $this->table = $table;
        }
    }

    /**
     * Returns product or products
     * $params format - array( 'id=9','type="***"'...);
     * $sort format - 'ORDER BY id'
     * @param string $params Params array
     * @param string $sort Sort str
     * @param bool $single Single product flag. If true - method returns an array with fields without keys
     * @return array|bool|mixed|mysqli_result|resource|string If $single is false - products array (<prod_id>=><prod array>), else array(<prod_field_key>=><prod_field_val>,...). If products not founded - false
     * @throws Exception If $params is not an array and is not INT
     */
    public function get($params = '', $sort = '', $single = false)
    {
        global $sql;
        $result = '';
        $where = array();
        $where_str = '';
        if (is_array($params)) {
            foreach ($params as $param) {
                $where[] = $param;
            }
        } else if ($params != '') {

            $params = intval($params);
            if (!is_int($params)) throw new Exception('Parameter in product->get must be an INT!');
            else
                $where [] = $this->prefix . "id=" . $params;
        }
        $where = array_diff($where,array(''));
        if (!empty($where)) {
            $where_str = "WHERE " . implode(' AND ', $where);
        } else {
            $where_str = '';
        }
        $result_data = $sql->query("SELECT * FROM " . $this->table . " " . $where_str . " " . $sort, 'array');
        $result = array();
        foreach ($result_data as $row) {
            $result[$row[$this->prefix.'id']] = $row;
        }
        if (!$single)
            return $result;
        else {
            $result = array_shift($result);
            return isset($result) ? $result : false;
        }
    }

    /**
     * Returns products by option value
     * $sort format - 'ORDER BY id'
     * @param string $params Params array
     * @param string $sort Sort str
     * @param bool $single Single product flag. If true - method returns an array with fields without keys
     * @return array|bool|mixed|mysqli_result|resource|string If $single is false - products array (<prod_id>=><prod array>), else array(<prod_field_key>=><prod_field_val>,...). If products not founded - false
     * @throws Exception If $params is not an array and is not INT
     */
    public function get_products_by_option_value($option_id, $option_value, $params = array(), $sort = '', $single = false)
    {
        global $sql;
        $result = '';
        $where = array();
        if (count($params) > 0) {
            $where_str = implode(" AND ", $params) . " AND ";
        } else $where_str = '';
        $query = "SELECT * FROM " . $this->table . " LEFT JOIN ".$this->options_values_table." ON (".$this->options_values_prefix."object_id=".$this->prefix."id) WHERE ".$where_str." ".$this->options_values_prefix."value='".$option_value."' AND ".$this->options_values_prefix."po_id='".$option_id."' " . $sort;
        // var_dump($query);
        $result_data = $sql->query($query, 'array');
        $result = array();
        foreach ($result_data as $row) {
            $result[$row[$this->prefix.'id']] = $row;
        }
        if ($single)
            return is_array($result) ? array_shift($result) : false;
        else    
            return $result;       
    }

    /**
     * Adds product to table
     * @param $data format - array(<prod_field_key>=><prod_field_val>,...) !IMPORTANT! FIELDS WITHOUT PREFIXES!
     * @return int Returs insert id product
     */
    public function add($data,$has_prefix = false)
    {
        global $sql, $admin, $PData;
        if ($has_prefix) {
            foreach ( $data as $k=>$v ) {
                $data[str_replace($this->prefix,'',$k)] = $data[$k];
                unset($data[$k]);
            }

        }
        $id = $sql->insert($this->table, $data, $this->prefix, false, true);
        $postType = 'product';
        $urls = writeImg($postType . $id, 'images', 0, array(0 => 500), false, true, true);
        if ($urls)
            $sql->update($this->table, $this->prefix . "id='" . _protect($id) . "'", array($this->prefix . 'image' => json_encode(array_values($urls))));


        return $id;

    }

    /**
     * Update product (products) by data array
     * @param $data Data array. Format - array (<prod_id>=>
     *                                              array(
     *                                                  <prod_field_key> => <prod_field_val>
     *                                              ),
     *                                              ...
     *                                          )
     * @return bool If success - true
     */
    public function update($data)
    {
        global $sql;
        foreach ($data as $key => $val) {
            if (!isset($val['enable'])) $val['enable'] = 0;
            $val['change_date'] = date('Y-m-d H:i:s');
            $sql->update($this->table, $this->prefix . "id='" . _protect($key) . "'", $val, $this->prefix);
        }
        return true;
    }

    /**
     * Delete product by id's array
     * @param $ids Format - array(1,2,35,...)
     * @return bool If success - true
     */
    public function delete($ids)
    {
        global $sql;

        if (is_array($ids)) {
            foreach ($ids as $id) {
                $sql->delete($this->table, $this->prefix . 'id=' . $id, $this->prefix);
            }
        } else
            $sql->delete($this->table, $this->prefix . 'id=' . $ids, $this->prefix);
        return true;
    }

    /**
     * Duplicate products into another category
     * @param string $ids || array $ids Products id`s array or one product id string
     * @param int $category Category ID to duplicate
     * @return bool True if success
     * @throws Exception
     */
    public function duplicate($ids = '', $category = 0) {
        global $sql;

//        $error = 0;
        if (is_array($ids))
        {
            foreach ( $ids as $id ) {
                $product = $this->get($id,'',true);
                unset($product[$this->prefix.'id']);
                $product[$this->prefix.'c_id'] = $category;
                $this->add($product,true);
            }

        } else
        {
            $product = $this->get($ids,'',true);
            unset($product[$this->prefix.'id']);
            $product[$this->prefix.'c_id'] = $category;
            $this->add($product,true);
        }
        return true;
    }

    /**
     * Returns options array
     * @param bool $object_id
     * @param string $object_type
     * @param string $limit
     * @param string $where_str
     * @return array|bool|mixed|mysqli_result|resource
     */
    public function get_options($object_id = false, $object_type = 'all', $where_str = '1', $limit = "LIMIT 0,10") {
        global $sql;
        if ($where_str == '') $where_str = '1';
        if ($object_type != 'all') {
            $where_str = $this->options_prefix."_object_type='".$object_type."'";
        }

        if ($object_id) {
            $query = "SELECT * FROM ".$this->opt_link_table." LEFT JOIN ".$this->options_table." ON (".$this->opt_link_prefix."po_id=".$this->options_prefix."id)  WHERE ".$where_str." AND ".$this->opt_link_prefix."object_id='".$object_id."' ".$limit;
        } else {
            $query = "SELECT * FROM _products_options  WHERE ".$where_str." ".$limit;
        }

        return $sql->query($query,'array');

    }

    public function get_option_by_id($option_id) {
        global $sql;
        return $sql->query("SELECT * FROM ".$this->options_table." WHERE ".$this->options_prefix."id=".$option_id,'value');
    }

    /**
     * Inserts option into DB
     * @param array $data
     * @param bool $has_prefix
     * @return bool|int|string
     */
    public function add_option($data, $has_prefix = false) {
        global $sql;

        if ($has_prefix) {
            foreach ( $data as $k=>$v ) {
                $data[str_replace($this->prefix,'',$k)] = $data[$k];
                unset($data[$k]);
            }
        }

        $id = $sql->insert($this->options_table,$data,$this->options_prefix,false,true);
        return $id;
    }

    /**
     * Remove option or options
     * @param array|string $ids option ID or option ids array
     * @return bool
     */
    public function remove_option($ids) {
        global $sql;
        if (is_array($ids)) {
            foreach ($ids as $val) {
                $sql->delete($this->options_table,$this->options_prefix.'id='.$val,$this->options_prefix);
            }
        } else {
            $sql->delete($this->options_table,$this->options_prefix.'id='.$ids,$this->options_prefix);
        }
        return true;
    }

    /**
     * Updates option by data from array
     * @param array $data
     * @return bool
     */
    public function update_option($data) {
        global $sql;
        foreach ($data as $key => $val) {
            $sql->update($this->options_table, $this->options_prefix.'id='.$key, $val, $this->options_prefix);
        }
        return true;
    }

    public function update_product_options($product_id, $options_array) {
        global $sql;
        $values = $this->get_product_options($product_id);

        foreach ($values as $key => $value) {
            if (!isset($options_array[$key]))
                $sql->delete($this->options_values_table,$this->options_values_prefix.'po_id='.$key.' AND '.$this->options_values_prefix.'object_id='.$product_id,$this->options_values_prefix);
        }
        if (is_array($options_array))
        foreach ($options_array as $key => $val) {

            $data = $val;
            $data['po_id'] = $key;
            $data['object_id'] = $product_id;
            $data['object_type'] = 'product';
//            var_dump($data['value']);

            if (isset($values[$key]))
                $sql->update($this->options_values_table, $this->options_values_prefix.'po_id='.$key.' AND '.$this->options_values_prefix.'object_id='.$product_id,$data,$this->options_values_prefix);
            else $sql->insert($this->options_values_table, $data, $this->options_values_prefix);
            if (empty($data['value'])) $sql->delete($this->options_values_table,$this->options_values_prefix.'po_id='.$key.' AND '.$this->options_values_prefix.'object_id='.$product_id,$this->options_values_prefix);
        }

    }

    public function get_product_options($product_id, $option_key = false) {
        global $sql;
        $join = $where_str = "";
        if ($option_key) 
            {
                $join .= " LEFT JOIN ".$this->options_table." ON (".$this->options_values_prefix."po_id=".$this->options_prefix."id) ";
                $where_str .= " ".$this->options_prefix."option_key='".$option_key."' AND ";
            }
            $query = "SELECT * FROM ".$this->options_values_table." ".$join." WHERE ".$where_str." ".$this->options_values_prefix."object_id='".$product_id."'";
            // var_dump($query);
        $values = $sql->query($query);
        $result = array();
        foreach ($values as $val) {
            $result[$val[$this->options_values_prefix.'po_id']] = $val;
        }
        if (count($result) > 0)
            return $result;
        else 
            return false;
    }

    public function link_options($option_id, $object_id, $object_type) {
        global $sql;
        $sql->delete($this->opt_link_table, $this->opt_link_prefix.'po_id='.$option_id." AND ".$this->opt_link_prefix."object_type='".$object_type."'");
        if (is_array($object_id)) {
            foreach ($object_id as $key => $value) {
                $sql->insert($this->opt_link_table, array(
                    'po_id' => $option_id,
                    'object_id' => $value,
                    'object_type' => $object_type
                ), $this->opt_link_prefix);
            }            
        } else {
            $sql->insert($this->opt_link_table, array(
                    'po_id' => $option_id,
                    'object_id' => $object_id,
                    'object_type' => $object_type
                ), $this->opt_link_prefix);
        }
    }

    public function get_option_links($option_id) {
        global $sql;
        $result = array();
        foreach ($sql->query("SELECT * FROM ".$this->opt_link_table." WHERE ".$this->opt_link_prefix.'po_id='.$option_id) as $key => $value) {
            $result []= array(
                    'id' => $value[$this->opt_link_prefix.'object_id'],
                    'type' => $value[$this->opt_link_prefix.'object_type']
                );
        }
        return $result;
    }

}