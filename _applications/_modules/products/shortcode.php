<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 08.07.2015
 * Time: 14:02
 */
/*
 * На в ході в шоткод має вказуватися ід категорії, і кількість позицій,
 * які туди потрібно виводити (якщо вказано 0 або не передано параметра,
 *  то виводити всі товари з категорії
 */

/**
 * @param $params - list of parameters, firs cat id, second number of product to show
 */
// [:PRODUCTS_SC cat_id=2&count=3:]
function PRODUCTS_SC($params)
{
    if(PAGE_TYPE=='admin') return '[:PRODUCTS_SC '.$params.':]';
    global $sqlTPL,$PData, $sql, $products, $categories;
    parse_str($params, $data);
    $cat_id = _protect($data['cat_id']);
    $vars_array['categories'] = $categories->get(array($categories->prefix . 'id=' . $cat_id));
    $vars_array['products'] = $products->get(array($products->prefix . 'c_id=' . $cat_id), (isset($data['count']) ? ' ORDER by pr_id ASC LIMIT ' . _protect($data['count']) : ''));

    //echo $list;
    /*
     * Потрібно написати шоткод, який буде виводити пости на сторінку
     */

    //вызывать шаблон для вывода
    return $PData->getModTPL('site/display_products', 'products', @$vars_array);
}
