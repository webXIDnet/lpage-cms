<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла
 *
***/

require_once __DIR__ .'/pf_fn.php';
require_once __DIR__ .'/pf_class.php';


if (PAGE_TYPE == 'api') {
	require_once __DIR__ . '/profile_api_class.php';
	global $api;
	$api->accept('profile', 'get', true);
}


_add_action('defined_modules', 'profile_defined_modules');

function profile_defined_modules(){

    if(PAGE_TYPE=='admin'){
        global $sql, $profile_logs;
$profile_logs = new logs('profile','pl_');
        $result = $sql->db_table_installer('mod_profile','25.01.2015', //перевірка на відповідність поточної БД до останньої версії структури
            array(
                '_profile' => array(
					'pf_id' => "`pf_id` int(11) NOT NULL",
					'pf_fullname' => "`pf_fullname` varchar(255) NOT NULL",
					'pf_email' => "`pf_email` varchar(100) NOT NULL",
					'pf_pass' => "`pf_pass` varchar(50) DEFAULT NULL",
					'pf_type_acc' => "`pf_type_acc` int(2) NOT NULL",
					'pf_pubtel' => "`pf_pubtel` varchar(25) NOT NULL",
					'pf_pubemail' => "`pf_pubemail` varchar(100) NOT NULL",
					'pf_userpic' => "`pf_userpic` varchar(255) NOT NULL",
					'pf_compname' => "`pf_compname` varchar(255) NOT NULL",
					'pf_compadress' => "`pf_compadress` varchar(255) NOT NULL",
					'pf_compemail' => "`pf_compemail` varchar(100) NOT NULL",
					'pf_comptel' => "`pf_comptel` varchar(25) NOT NULL",
					'pf_resperson' => "`pf_resperson` varchar(255) NOT NULL",
					'pf_cl_id' => "`pf_cl_id` int(11) NOT NULL",
                    'CREATE' => "`pf_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`pf_id`)"
                )
            )
        );
    }
}

if(PAGE_TYPE=='admin'){
	_add_hook('adminPanel_controller', 'profile_admin_init');
}

if(PAGE_TYPE!='admin'){
	_add_action('init_processes', 'pf_getAccount',1);
}

if(PAGE_TYPE=='profile'){

	/* Запускає функціі ініціалізаціі профайла */
	//_add_filter('course_after_topics_list','course_theme_insert_test_btn');
	_add_action('init_processes', 'profile_init',1);
	_add_action('init_processes', 'profilePanel_controller',2);
}

_add_action('getURL_rule', 'getProfileUrl', 3);

function profile_admin_init(){

	global	$user,
			$PData,
			$sql,
			$contTPL;

	switch (@$_GET['admin']){
		case 'accounts':
			require_once __DIR__ .'/apps_admin/a_pf_accounts.php';
			return TRUE;
		break;

		default:
			_run_action('profile_admin_controller');
	}

}

function profile_init(){

	global	$user,
			$PData,
			$sql,
			$contTPL;
	$PData->setTPL('tpl_profile','profile'); // файл шаблону особистого кабінету по замовчуванню (./templates/tpl_profile.php)
	// var_dump($_GET['profile']);
	switch (@$_GET['profile']){
		case 'login':
			require_once __DIR__ .'/apps_site/s_pf_login.php';
		break;

		case 'sign-up':
			require_once __DIR__ .'/apps_site/s_pf_sign_up.php';
		break;

		case 'private-room':
			require_once __DIR__ .'/apps_site/s_pf_privateroom.php';
		break;

		case 'allinfo':
			require_once __DIR__ .'/apps_site/s_pf_allinfo.php';
		break;

		case 'card':
			require_once __DIR__ .'/apps_site/s_pf_card.php';
		break;

		case 'payment':
			require_once __DIR__ .'/apps_site/s_pf_payment.php';
		break;

		case 'donated':
			require_once __DIR__ .'/apps_site/s_pf_donated.php';
		break;

		case 'logout':
			require_once __DIR__ .'/apps_site/s_pf_logout.php';
		break;

		case 'topics':
			require_once __DIR__ .'/apps_site/s_pf_topics.php';
		break;

		case 'courses':
			require_once __DIR__ .'/apps_site/s_pf_courses.php';
		break;

		case (preg_match_all('/courses\/(.+)/i', @$_GET['profile'], $c_arr) ? true : false) :
			$c_arr = array_pop($c_arr);
			$course = array_shift($c_arr);
			require_once __DIR__ .'/apps_site/s_pf_courses.php';
		break;

		default:
			if(_run_action('profile_controller')){
				break;
			}

			require_once __DIR__ .'/apps_site/s_pf_main.php'; break;
	}
	_run_action('profile_template');
	$PData->connectTPL();
}

function pf_getAccount(){
	global $profile;
	$profile->getAccount();
}

/** Формування меню в адмінці  **/

if(PAGE_TYPE=='admin'){
	_add_filter('adminPanel_sidebar_menu', 'profile_admin_sidebar_menu', 26);
	_add_filter('profile_admin_sidebar_menu', 'profile_admin_sidebar_menu_submenu');
}

function profile_admin_sidebar_menu($result){

	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Профайлы'), //ім"я пункта меню
			'url' => FALSE, //посилання
			'submenu_filter' => 'profile_admin_sidebar_menu_submenu', //хук для підключення пунктів підменю
			'active_pages' => _run_filter('profile_admin_sidebar_menu_active_items', array('accounts')), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => TRUE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . @$menu;

}

function profile_admin_sidebar_menu_submenu($result)
{
	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Акаунты'), //ім"я пункта меню
			'url' => getURL('admin', 'accounts'), //посилання
			'active_pages' => array('accounts'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . $menu;
}


function profilePanel_controller(){
//	global 	$PData;//,	$user,	$sql,	$contTPL;

//	require_once $PData->dir(__FILE__) . 'apps_site/s_profile_html.php';
}