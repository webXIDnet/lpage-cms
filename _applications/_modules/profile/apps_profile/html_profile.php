<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = profile 
 * 
 * Тут формується HTML код сторінки
 * 
***/

$PData->content='
	<div id="sidebar">
		<ul class="vertMenu">
			'._run_filter('profile_sidebar_menu','').'
		</ul>
		'.@$PData->position['sidebar'].'
	</div>			
	<div id="content">	
		<h1>'._lang(@$PData->position['title']).'</h1>
		'.@$PData->content.'		
	</div>';