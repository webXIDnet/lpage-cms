<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}

$PData->content('main page side', 'sidebar');

$PData->content('
	<form name="profile" method="POST">
		<h3>'._lang('Профиль').'</h3>
		<input type="text" name="name" value="" placeholder="'._lang('Полное имя').'">
		<input type="text" name="mail" value="" placeholder="'._lang('Email').'">
		<h3>'._lang('Компания').'</h3>
		<input type="text" name="compName" value="" placeholder="'._lang('Название').'">
		<input type="text" name="compType" value="" placeholder="'._lang('Тип').'">
		<input type="text" name="compWWW" value="" placeholder="'._lang('Сайт').'">
		<input type="text" name="compPhone" value="" placeholder="'._lang('Телефон').'">
		<input type="text" name="compMail" value="" placeholder="'._lang('Email').'">
		<input type="text" name="compAddr" value="" placeholder="'._lang('Адрес').'">
		
		<h3>'._lang('Изменить пароль').'</h3>
		<input type="text" name="newPass" value="" placeholder="'._lang('Новый пароль').'">
		<input type="text" name="newPass1" value="" placeholder="'._lang('Новый пароль (повтор)').'">
	</form>
');