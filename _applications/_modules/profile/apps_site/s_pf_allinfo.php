<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Форма з усіма полями для користувача.
 *
 ***/
global $profile;
$err_html = '';
if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
	$profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" . _protect($_SESSION['profile_id']) . "'" );
	$profile_user = array_filter($profile_user);
	if ( empty( $profile_user ) ) {
		$PData->content( 'Пройдите Авторизацию', 'message', TRUE );
		$PData->redirect( getURL( 'profile', 'login' ) );
	}
} else {
	$PData->content( 'Пройдите Авторизацию', 'message', TRUE );
	$PData->redirect( getURL( 'profile', 'login' ) );
}

if ( isset( $_POST['cancelAllSubmit'] ) ) {
	$PData->redirect( getURL( 'profile', 'private-room' ) );
}

if ( isset( $_POST['AllSubmit'] ) ) {
	if ( !empty( $_POST['allinfo'] ) ) {
		$allinfo = array(
				'compname'   => $_POST['allinfo']['comname'],
				'compadress' => $_POST['allinfo']['comaddr']
		);
		if($profile_user[0]['pf_type_acc'] == 1) {
			$allinfo_person = array(
					'pubtel'   => $_POST['allinfo']['pubtel'],
					'pubemail' => $_POST['allinfo']['pubemail']
			);
			$allinfo = $allinfo + $allinfo_person;
		}
		if($profile_user[0]['pf_type_acc'] == 2) {
			$allinfo_comp = array(
					'resperson'  => $_POST['allinfo']['comperson'],
					'compemail'  => $_POST['allinfo']['commail'],
					'comptel'    => $_POST['allinfo']['comphone']
			);
			$allinfo = $allinfo + $allinfo_comp;
		}
		if ( !empty( $_FILES['userpic'] ) ) {
			$userpic_dir  = 'media/avatars/';
			$userpic_path = $userpic_dir . basename( $_FILES['userpic']['name'] );
			if ( move_uploaded_file( $_FILES['userpic']['tmp_name'], $userpic_path ) ) {
				$allinfo_userpic = array( 'userpic' => $userpic_path );
				$allinfo = $allinfo + $allinfo_userpic;
			}
		}
        $allinfo['additional_fields'] = json_encode($_POST['additional_fields']);
        $errors = 0;

        if (!empty($_POST['additional_fields'])) {
            foreach ($_POST['additional_fields'] as $field) {
                if (empty($field['value'])) $errors++;
            }
        }
        if (empty($_POST['allinfo']['fullname']) || empty($_POST['allinfo']['email']) || empty($_POST['allinfo']['pubtel'])) {
            $errors++;
        }
        if ($errors == 0) {
            $pf_id = 'pf_id='. _protect($_SESSION['profile_id']);
            $sql->update( '_profile',$pf_id, $allinfo, 'pf_' );
            $profile->update(array('rights' => '1'));
            $PData->redirect( getURL( 'profile', '' ) );
        } else {
            $err_html = '<div class="alert alert-danger" style="margin-top: 20px;" role="alert">'._lang('Вы не заполнили обязательные поля!').'</div>';
        }



	}
}

$PData->content( 'main page side', 'sidebar' );

$additional_fields = get_additional_fields();
$add_f_html = ''; $i = 0;
foreach ($additional_fields as $field) {
    if ($field['type'] == 'textarea')
        $add_f_html .= '
        <div class="form-group">
            <input type="hidden" name="additional_fields['.$i.'][type]" value="'.$field['type'].'">
            <input type="hidden" name="additional_fields['.$i.'][name]" value="'.$field['name'].'">
            <input type="hidden" name="additional_fields['.$i.'][key]" value="'.$field['key'].'">
            <label for="'.$field['key'].'">'.$field['name'].'</label>
            <textarea id="'.$field['key'].'" name="additional_fields['.$i.'][value]" class="form-control">'.$field['value'].'</textarea>
        </div>
        ';
    else
        $add_f_html .= '
        <div class="form-group">
            <input type="hidden" name="additional_fields['.$i.'][type]" value="'.$field['type'].'">
            <input type="hidden" name="additional_fields['.$i.'][name]" value="'.$field['name'].'">
            <input type="hidden" name="additional_fields['.$i.'][key]" value="'.$field['key'].'">
            <label for="'.$field['key'].'">'.$field['name'].'</label>
                <input type="'.$field['type'].'" id="'.$field['key'].'" placeholder="'.$field['name'].'" name="additional_fields['.$i.'][value]" value="'.$field['value'].'" class="form-control" />

        </div>
        ';
    $i++;
}


$PData->content($err_html.'
<div class="" style="width: 300px;margin: 0 auto 30px">
        <h3>'._lang('Личный кабинет').'</h3>
<form action="" method="post">
    <div class="form-group">
			<input type="text" name="allinfo[fullname]" class="form-control" value="'.$profile_user[0]['pf_fullname'].'" placeholder="' . _lang( 'ФИО' ) . '" />
    </div>
    <div class="form-group">
			<input type="text" name="allinfo[email]" class="form-control" value="'.$profile_user[0]['pf_email'].'" placeholder="' . _lang( 'Email' ) . '" />
    </div>
    <div class="form-group">
            <input type="text" name="allinfo[pubtel]" class="form-control" value="' . $profile_user[0]['pf_pubtel'] . '" placeholder="' . _lang( 'Номер Телефона' ) . '" />
    </div>
    '.$add_f_html.'
    <div class="text-center"><input type="submit" name="AllSubmit" value="'._lang('Сохранить').'" class="btn btn-primary" /></div>
</form>
</div>

');