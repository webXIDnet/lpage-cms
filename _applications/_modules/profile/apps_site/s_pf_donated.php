<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
  $profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'" );
  if ( empty( $profile_user ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  } else {
   /* $donated = $sql->query("SELECT pm_prod,pm_price,pm_email,pm_date,lp_article
        FROM _payments AS p, _landing_pages AS l ON p.pm_lp_id = l.lp_id
				WHERE pm_cl_id = '" .  _protect($profile_user[0]['pf_cl_id']) . "'" );*/

    $donated = $sql->query("SELECT pm_prod,pm_price,pm_email,pm_date,lp_article
        FROM _payments INNER JOIN _landing_pages ON pm_lp_id = lp_id
				WHERE pm_cl_id = '" .  _protect($profile_user[0]['pf_cl_id']) . "'" );
  }
}else{
	$PData->content( 'Пройдите Авторизацию', 'message', TRUE );
	$PData->redirect( getURL( 'profile', 'login' ) );
}

$PData->content('Архив Пожертвованей','title');

$PData->content(
	'<ul class="listmenu">
    	'.pf_sidebar().'
    </ul>'
,'sidebar');

if(isset($donated)){
	
  $PData->content( '
    <table>
      <thead>
        <tr>
        <th>' . _lang( '№' ) . '</th>
        <th>' . _lang( 'Проект' ) . '</th>
        <th>' . _lang( 'Пожертвовано' ) . '</th>
        <th>' . _lang( 'Paypal аккаунт' ) . '</th>
        <th>' . _lang( 'Дата/Время' ) . '</th>
        </tr>
      </thead>
        <tbody>');

  for($i = 0; $i < count($donated); $i++) {
  	
    $PData->content( '
        <tr>
        <td>' . $i . '</td>
        <td><a href="'.getURL('lp',$donated[$i]['lp_article']).'">' . $donated[$i]['pm_prod'] . '</a></td>
        <td>' . $donated[$i]['pm_price'] . $donated[$i]['pm_currency'] . '</td>
        <td>' . $donated[$i]['pm_email'] . '</td>
        <td>' . $donated[$i]['pm_date'] . '</td>
        </tr>' );
  }
  
  $PData->content( '
      </tbody>
      <tfoot>
        <tr>
        <td>...</td>
        </tr>
      </tfoot>
    </table>
  ');
}else{
  $PData->content( _lang("Пусто"));
}

/*$PData->content('
<section class="profile">
                <div class="units-row">
                    <div class="unit-40 listmenu">
                            <ul>
                                <li>
                                     <a href="'.getURL('profile', 'private-roome').'">Profile</a>
                                </li>
                                <li>
                                    <a href="'.getURL('profile', 'payment').'">Payment setting</a>
                                </li>
                                <li>
                                    <a href="#">Donated projects</a>
                                </li>
                                <li>
                                    <a href="#">Donated graph</a>
                                </li>
                                <li>
                                    <a href="#">Help</a>
                                </li>
                            </ul>
                        </div>
                    <div class="unit-50">
                           <span class="nameform">'._lang('Данные вашей карты').'</span>
                         <form id="card-form" name="card" method="POST" class="forms">
                          <label>
                            <input type="text" name="card[card_adress]" value="'.$profile_user[0]['pf_card_adress'].'" placeholder="'._lang('Платежный Адрес').'" class="width-100"/>
                          </label>
                          <label>
                            <input type="text" name="card[card_type]" value="'.$profile_user[0]['pf_card_type'].'" placeholder="'._lang('Тип Кредитной Карты').'" class="width-100"/>
                          </label>
                          <label>
                            <input type="text" name="card[card_number]" value="'.$profile_user[0]['pf_card_number'].'" placeholder="'._lang('Номер Кредитной Карты').'" class="width-100"/>
                          </label>
                          <label>
                            <input type="text" name="card[card_owner]" value="'.$profile_user[0]['pf_card_owner'].'" placeholder="'._lang('Имя Вледельца Карты').'" class="width-100"/>
                          </label>
                          <input type="submit" name="CardSubmit" value="'._lang('Сохранить').'" class="btn btn-blue">
                          <input type="submit" name="cancelСardSubmit" value="'._lang('Пропустить').'" class="btn">
                        </form>
                    </div>
                    <div class="unit-10"></div>

                </div>
            </section>');*/
$PData->content('
<div class="hide">
    <div id="change-card">
        <form id="card-form" name="card" method="POST" class="forms">
        <label>
          <input type="text" name="card[card_adress]" value="'.$profile_user[0]['pf_card_adress'].'" placeholder="'._lang('Платежный Адрес').'" class="width-100"/>
        </label>
        <label>
          <input type="text" name="card[card_type]" value="'.$profile_user[0]['pf_card_type'].'" placeholder="'._lang('Тип Кредитной Карты').'" class="width-100"/>
        </label>
        <label>
          <input type="text" name="card[card_number]" value="'.$profile_user[0]['pf_card_number'].'" placeholder="'._lang('Номер Кредитной Карты').'" class="width-100"/>
        </label>
        <label>
          <input type="text" name="card[card_owner]" value="'.$profile_user[0]['pf_card_owner'].'" placeholder="'._lang('Имя Вледельца Карты').'" class="width-100"/>
        </label>
        <input type="submit" name="CardSubmit" value="'._lang('Сохранить').'" class="btn btn-blue">
        <input type="submit" name="cancelСardSubmit" value="'._lang('Пропустить').'" class="btn">
      </form>
    </div>
</div>');
$PData->content('
<script type="text/javascript">
$(document).ready(function () {
  console.log ( "#someButton was clicked" );
  		$("#card-form").submit(function () {
			console.log("Submit");
			});
});
</script>');
