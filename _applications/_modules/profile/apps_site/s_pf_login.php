<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Форма Авторизації.
 *
 ***/
global $profile;
if ( !empty($_SESSION['profile_id']) && $_SESSION['profile_id'] != 0 ) {
	$login_check = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" . _protect($_SESSION['profile_id']) . "'" );
	if ( empty( $login_check ) ) {
		$PData->redirect( getURL( 'profile', 'login' ) );
	} else {
		$PData->redirect( getURL( 'profile', 'allinfo' ) );
	}
}

if ( isset( $_POST['logSubmit'] ) ) {
	if ( empty( $_POST['pf_log']['email'] ) || empty( $_POST['pf_log']['pass'] ) ) {
		$PData->content( 'Введите логин или пароль!', 'message');
	} else {
		$login_check = $sql->query( "
				SELECT pf_id
				FROM _profile
				WHERE pf_email = '" . _protect($_POST['pf_log']['email']) . "' AND pf_pass = '" . _protect(md5($_POST['pf_log']['pass'])) . "'" );
		if ( $login_check ) {
			$_SESSION['profile_id'] = $login_check[0]['pf_id'];

            if (!empty($profile->additional_fields)) {
                $PData->redirect( getURL('profile','') );
            } else {
                $PData->redirect( getURL('profile','allinfo') );
            }

		} else {
			$PData->content( 'Нерпавильный логин или пароль!', 'message');
		}
	}
}

$PData->content('
    <div class="text-center" style="width: 300px;margin: 0 auto 30px">
        <h3>'._lang('Личный кабинет').'</h3>
        <form name="login" method="POST">
          <div class="form-group">
            <input type="text" name="pf_log[email]" class="form-control" placeholder="'._lang('Логин/E-mail').'">
          </div>
          <div class="form-group">
            <input type="password" name="pf_log[pass]" class="form-control" placeholder="'._lang('Пароль').'">
          </div>
          

          <button type="submit" name="logSubmit" class="btn btn-primary">'._lang('Войти').'</button>
        </form>
    </div>


');
