<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = profile 
 * 
 * Тут формується остаточній HTML код сторінки
 * 
***/

if (empty($_SESSION['profile_id']) || $_SESSION['profile_id'] == 0 ) {
	$PData->redirect(getURL('profile','login'),'301');
}

$PData->connectTPL('profile/tpl_default_page');