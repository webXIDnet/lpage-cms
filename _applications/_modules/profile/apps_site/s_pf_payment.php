<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
  $profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'" );
  if ( empty( $profile_user ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  }
} else {
  $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
  $PData->redirect( getURL( 'profile', 'login' ) );
}


if ( isset( $_POST['CardSubmit'] ) ) {

  if ( empty( $_POST['card']['card_adress'] ) || empty( $_POST['card']['card_type'] ) || empty( $_POST['card']['card_number'] ) || empty( $_POST['card']['card_owner'] ) ) {
    $PData->content(
        'Нужно заполнить все поля с *',
        'message'
    );
  } else {

    $card_array = array(
        'card_adress' => $_POST['card']['card_adress'],
        'card_type'   => $_POST['card']['card_type'],
        'card_number' => $_POST['card']['card_number'],
        'card_owner'  => $_POST['card']['card_owner']
    );

    $pf_id = 'pf_id='. $_SESSION['profile_id'];
    $sql->update( '_profile', $pf_id, $card_array, 'pf_' );
    $PData->redirect(getURL('profile','private-room'));

  }
}

$PData->content('
<section class="profile">
                <div class="units-row">
                    <div class="unit-40 listmenu">
                            <ul>
                                <li>
                                     <a href="'.getURL('profile', 'private-room').'">Profile</a>
                                </li>
                                <li>
                                    <a href="'.getURL('profile', 'payment').'">Payment setting</a>
                                </li>
                                <li>
                                    <a href="#">Donated projects</a>
                                </li>
                                <li>
                                    <a href="#">Donated graph</a>
                                </li>
                                <li>
                                    <a href="#">Help</a>
                                </li>
                            </ul>
                        </div>
                    <div class="unit-50">
                        <span class="nameform">'._lang('Данные вашей карты').'</span>
                        <div class="units-row">
                            <div class="unit-100">'._lang('Платежный Адрес').'</div>
                            <div class="unit-100">'.$profile_user[0]['pf_card_adress'].'</div>
                        </div>
                        <div class="units-row">
                            <div class="unit-100">'._lang('Тип Кредитной Карты').'</div>
                            <div class="unit-100">'.$profile_user[0]['pf_card_type'].'</div>
                        </div>
                        <div class="units-row">
                            <div class="unit-100">'._lang('Номер Кредитной Карты').'</div>
                            <div class="unit-100">'.$profile_user[0]['pf_card_number'].'</div>
                        </div>
                        <div class="units-row">
                            <div class="unit-100">'._lang('Имя Вледельца Карты').'</div>
                            <div class="unit-100">'.$profile_user[0]['pf_card_owner'].'</div>
                        </div>
                        <div class="units-row">
                          <a href="#change-card" class="btn open_popup">'._lang('Изменить').'</a>
                        </div>
                    </div>
                    <div class="unit-10"></div>

                </div>
            </section>');
/*$PData->content('
<section class="profile">
                <div class="units-row">
                    <div class="unit-40 listmenu">
                            <ul>
                                <li>
                                     <a href="'.getURL('profile', 'private-roome').'">Profile</a>
                                </li>
                                <li>
                                    <a href="'.getURL('profile', 'payment').'">Payment setting</a>
                                </li>
                                <li>
                                    <a href="#">Donated projects</a>
                                </li>
                                <li>
                                    <a href="#">Donated graph</a>
                                </li>
                                <li>
                                    <a href="#">Help</a>
                                </li>
                            </ul>
                        </div>
                    <div class="unit-50">
                           <span class="nameform">'._lang('Данные вашей карты').'</span>
                         <form id="card-form" name="card" method="POST" class="forms">
                          <label>
                            <input type="text" name="card[card_adress]" value="'.$profile_user[0]['pf_card_adress'].'" placeholder="'._lang('Платежный Адрес').'" class="width-100"/>
                          </label>
                          <label>
                            <input type="text" name="card[card_type]" value="'.$profile_user[0]['pf_card_type'].'" placeholder="'._lang('Тип Кредитной Карты').'" class="width-100"/>
                          </label>
                          <label>
                            <input type="text" name="card[card_number]" value="'.$profile_user[0]['pf_card_number'].'" placeholder="'._lang('Номер Кредитной Карты').'" class="width-100"/>
                          </label>
                          <label>
                            <input type="text" name="card[card_owner]" value="'.$profile_user[0]['pf_card_owner'].'" placeholder="'._lang('Имя Вледельца Карты').'" class="width-100"/>
                          </label>
                          <input type="submit" name="CardSubmit" value="'._lang('Сохранить').'" class="btn btn-blue">
                          <input type="submit" name="cancelСardSubmit" value="'._lang('Пропустить').'" class="btn">
                        </form>
                    </div>
                    <div class="unit-10"></div>

                </div>
            </section>');*/
$PData->content('
<div class="hide">
    <div id="change-card">
        <form id="card-form" name="card" method="POST" class="forms">
        <label>
          <input type="text" name="card[card_adress]" value="'.$profile_user[0]['pf_card_adress'].'" placeholder="'._lang('Платежный Адрес').'" class="width-100"/>
        </label>
        <label>
          <input type="text" name="card[card_type]" value="'.$profile_user[0]['pf_card_type'].'" placeholder="'._lang('Тип Кредитной Карты').'" class="width-100"/>
        </label>
        <lab