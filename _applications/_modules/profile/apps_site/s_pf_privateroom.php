<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Перша сторінка приватного кабінету
 *
 ***/

global $profile;

if ( isset( $_POST['roomSubmit'] ) ) {

    if ( !empty( $_POST['room'] ) ) {

        if(!empty($_POST['room']['pass'])){

	        if ($_POST['room']['pass'] == $_POST['room']['confirmpass']) {

	            $_POST['room']['pass'] = md5( $_POST['room']['pass'] );
	            
	        }else{

	            $PData->content('Введите новый пароль занового и убедитесь в правильном заполнении','message',TRUE);
	        }
	    }else{
            unset($_POST['room']['pass']);
        }
	    unset($_POST['room']['confirmpass']);

        $profile->update($_POST['room']);
    }
    $errors = 0;
    if (!empty($_POST['additional_fields'])) {
        foreach ($_POST['additional_fields'] as $field) {
            if (empty($field['value'])) $errors++;
        }
    }

    if ($errors == 0) {
        $profile->update(array('rights' => '1'));
    } else {
        $profile->update(array('rights' => '0'));
    }
}

if( isset( $_POST['changePhotoSubmit'] ) ){

    if( isset( $_FILES['change_photo'] ) ){

        $valid_formats = array( "jpg", "JPG", "png", "gif", "bmp", "jpeg", "JPEG" );
        list( $txt, $ext ) = explode( ".", $_FILES['change_photo']['name'] );
        
        if ( in_array( $ext, $valid_formats ) ) {
        
            if ( $_FILES['change_photo']['size'] < ( 1024 * 1024 ) ) {
        
                $userpic_dir  = 'media/avatars/';
                $userpic_path = $userpic_dir . basename( $_FILES['change_photo']['name'] );
        
                if ( move_uploaded_file( $_FILES['change_photo']['tmp_name'], $userpic_path ) ) {
                    
                    $profile->update( array( 'userpic' => $userpic_path ) );
        
                } else $PData->content('failed','message');
        
            } else $PData->content('Image file size max 1 MB','message');
        
        } else $PData->content('Invalid file format..','message');

    } else $PData->content('Please select image..!','message');
}

$pf_userpic = '<img alt="photo" src="'.SITE_FOLDER.'media/avatars/no_image.jpg" id="preview" style="width: 210px;">';

if(_checkURL(SITE_FOLDER.$profile->userpic) && !empty($profile->userpic)){
    $pf_userpic = '<img alt="photo" src="'.SITE_FOLDER.$profile->userpic.'" id="preview" style="width: 210px;">';
}

$PData->content('Личные данные','title');

$PData->content(
	'<ul class="listmenu">
    	'.pf_sidebar().'
    </ul>'
,'sidebar');

$PData->content(
'<div class="col-md-4 photo">'
	.$pf_userpic.
	'<br/><a href="#callback" data-toggle="modal" data-target="#change_foto" class="open_popup">'._lang('Изменить фото').'</a>
	<p>
        <input type="checkbox" name="checkbox-2" id="checkbox-2">
        <label for="checkbox-2">'._lang('Подписаться на новости').'</label>
    </p>
</div>
<div class="col-md-8">
    <form method="post" action="" class="forms">

        <div class="form-group full-w">
            <label>
                <input type="text" name="room[fullname]" value="'.$profile->fullname.'" placeholder="' . _lang( 'ФИО' ) . '" class="form-control" />
            </label>
        </div>
        <div class="form-group full-w">
            <label>
                <input type="email" name="room[email]" value="'.$profile->email.'" placeholder="' . _lang( 'Email' ) . '" class="form-control" />
            </label>
        </div>
');
                                
if($profile->type_acc == 1) {
    $PData->content( '
    <div class="form-group full-w">
        <label>
            <input type="text" name="room[pubtel]" value="' . $profile->pubtel . '" placeholder="' . _lang( 'Номер Телефона' ) . '" class="form-control" />
        </label>

	');
}

$additional_fields = get_additional_fields();
$add_f_html = ''; $i = 0;
foreach ($additional_fields as $field) {
    if ($field['type'] == 'textarea')
        $add_f_html .= '
        <div class="form-group full-w">
            <input type="hidden" name="additional_fields['.$i.'][type]" value="'.$field['type'].'">
            <input type="hidden" name="additional_fields['.$i.'][name]" value="'.$field['name'].'">
            <input type="hidden" name="additional_fields['.$i.'][key]" value="'.$field['key'].'">
            <label for="'.$field['key'].'">'.$field['name'].'</label>
            <textarea id="'.$field['key'].'" name="additional_fields['.$i.'][value]" class="form-control">'.$field['value'].'</textarea>
        </div>
        ';
    else
        $add_f_html .= '
        <div class="form-group full-w">
            <input type="hidden" name="additional_fields['.$i.'][type]" value="'.$field['type'].'">
            <input type="hidden" name="additional_fields['.$i.'][name]" value="'.$field['name'].'">
            <input type="hidden" name="additional_fields['.$i.'][key]" value="'.$field['key'].'">
            <label for="'.$field['key'].'">'.$field['name'].'</label>
                <input type="'.$field['type'].'" id="'.$field['key'].'" placeholder="'.$field['name'].'" name="additional_fields['.$i.'][value]" value="'.$field['value'].'" class="form-control" />

        </div>
        ';
    $i++;
}


$PData->content('

        '
);
	
if($profile->type_acc == 2) {
	$PData->content( '
    <div class="form-group full-w">
        <label>
            <input type="text" name="room[resperson]" value="' . $profile->resperson . '" placeholder="' . _lang( 'Имя Преставителя Компании' ) . '" class="form-control" />
        </label>
    </div>
    <div class="form-group full-w">
        <label>
            <input type="email" name="room[compemail]" value="' . $profile->compemail . '" placeholder="' . _lang( 'Email Компании' ) . '" class="form-control" />
        </label>
    </div>
    <div class="form-group full-w">
        <label>
            <input type="text" name="room[comptel]" value="' . $profile->comptel . '" placeholder="'._lang('Номер Телефона Компании').'" class="form-control" />
        </label>
    </div>'
	);
}

$PData->content( $add_f_html.'
        <div class="form-group full-w">
            <span class="nameform">'._lang('Изменить пароль').'</span>
        </div>
        <div class="form-group full-w">
            <label>
                <input type="password" name="room[pass]" placeholder="' . _lang( 'Новый Пароль' ) . '" class="form-control" />
            </label>
        </div>
        <div class="form-group full-w">
            <label>
                <input type="password" name="room[confirmpass]" placeholder="' . _lang( 'Пароль еще раз' ) . '" class="form-control" />
            </label>
        </div>

            <input type="submit" name="roomSubmit" value="'._lang('Изменить').'" class="btn btn-primary" />
    </form>
</div>
');

$PData->content('
<!-- Modal -->
<div class="modal fade" id="change_foto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">'._lang('Изменить фото').'</h4>
      </div>
      <div class="modal-body">
        <form method="post"  action="" enctype="multipart/form-data" class="forms" id="imageform">
            <p>'._lang('Выбрать новое фото').'</p>
            <input type="hidden" name="MAX_FILE_SIZE" value="4194304" />
            <div class="form-group full-w">
                <label>
                    <input name="change_photo" type="file" class="form-control" id="change_photo"/>
                </label>
            </div>
             <input type="submit" class="btn btn-primary" value="'._lang('Изменить Фото').'" name="changePhotoSubmit"  id="buttonForm" />
        </form>
        <img id="loading" src="/images/loader.gif" style="display:none;" />
        <p id="message"></p>
        <p id="result"></p>
      </div>

    </div>
  </div>
</div>
');