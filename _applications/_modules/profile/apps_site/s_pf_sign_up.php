<?php if ( !defined( 'HOMEPAGE' ) ) {  header( 'HTTP/1.0 404 not found' );  exit(); }
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Форма Регістрації.
 *
 ***/

if ( isset( $_POST['regPersonSubmit'] ) ) {

  if ( isset( $_POST['reg_person'] ) &&  $_POST['reg_person']['type'] == 1 ) {
    if ( empty( $_POST['reg_person']['fullname'] ) || empty( $_POST['reg_person']['email'] ) || empty( $_POST['reg_person']['pass'] ) ) {
      $PData->content(
          'Нужно заполнить все поля с *',
          'message'
      );
    } else {
     $client_email = $sql->query( "
				SELECT cl_id
				FROM _clients
				WHERE cl_email = '" .  _protect($_POST['reg_person']['email']) . "'" );

      if ( !empty( $client_email ) ) {
        $reg_profile = array(
            'fullname'   => $_POST['reg_person']['fullname'],
            'pass'  => md5( $_POST['reg_person']['pass'] ),
            'type_acc'  => $_POST['reg_person']['type'] ,
            'email' => $_POST['reg_person']['email'],
            'cl_id' => $client_email[0]['cl_id']
        );

        $sql->insert( '_profile', $reg_profile, 'pf_' );
      } else {
        $reg_client = array(
            'fullname'   => $_POST['reg_person']['fullname'],
            'email' => $_POST['reg_person']['email'],
            'tel'   => 'NON_' . time()
        );
        $cl_id      = $sql->insert( '_clients', $reg_client, 'cl_', FALSE, TRUE );

        $reg_profile = array(
            'fullname'   => $_POST['reg_person']['fullname'],
            'pass'  => md5( $_POST['reg_person']['pass'] ),
            'type_acc'  => $_POST['reg_person']['type'] ,
            'email' => $_POST['reg_person']['email'],
            'cl_id' => $cl_id
        );
        $sql->insert( '_profile', $reg_profile, 'pf_' );
      }
//      $PData->content( 'Запись создана успешно!', 'message', TRUE );
      $PData->redirect(getURL('profile','login'));
    }
  }
}

if ( isset( $_POST['regCompanySubmit'] ) ) {

  if ( isset( $_POST['reg_comp'] ) &&  $_POST['reg_comp']['type'] == 2 ) {
    if ( empty( $_POST['reg_comp']['fullname'] ) || empty( $_POST['reg_comp']['email'] ) || empty( $_POST['reg_comp']['pass'] ) ) {
      $PData->content(
          'Нужно заполнить все поля с *',
          'message'
      );
    } else {
      $client_email = $sql->query( "
				SELECT cl_id
				FROM _clients
				WHERE cl_email = '" .  _protect($_POST['reg_comp']['email']) . "'" );

      if ( !empty( $client_email ) ) {
        $reg_profile = array(
            'fullname'   => $_POST['reg_comp']['fullname'],
            'email' => $_POST['reg_comp']['email'],
            'pass'  => md5( $_POST['reg_comp']['pass'] ),
            'type_acc'  => $_POST['reg_comp']['type'] ,
            'compname'  => $_POST['reg_comp']['comname'] ,
            'cl_id' => $client_email[0]['cl_id']
        );

        $sql->insert( '_profile', $reg_profile, 'pf_' );
      } else {
        $reg_client = array(
            'fullname'   => $_POST['reg_comp']['fullname'],
            'email' => $_POST['reg_comp']['email'],
            'tel'   => 'NON_' . time()
        );
        $cl_id      = $sql->insert( '_clients', $reg_client, 'cl_', FALSE, TRUE );

        $reg_profile = array(
            'fullname'   => $_POST['reg_comp']['fullname'],
            'email' => $_POST['reg_comp']['email'],
            'pass'  => md5( $_POST['reg_comp']['pass'] ),
            'type_acc'  => $_POST['reg_comp']['type'] ,
            'compname'  => $_POST['reg_comp']['comname'] ,
            'cl_id' => $cl_id
        );
        $sql->insert( '_profile', $reg_profile, 'pf_' );
      }
//      $PData->content( 'Запись создана успешно!', 'message', TRUE );
      $PData->redirect(getURL('profile','login'));
    }
  }
}


$PData->content('main page side', 'sidebar');

$PData->content('
<!--div class="units-row">
  <div class="unit-25">&nbsp;</div>
	<div class="tabs-person unit-50">
	<form name="profile" method="POST" class="forms text-centered">
		<p class="h3">'._lang('Физ. лицо').'</p>
		<input type="hidden" name="reg_person[type]" value="1"/>
		<label>
		  <input type="text" name="reg_person[fullname]" value="" placeholder="'._lang('Полное имя').'" class="width-100" />
		</label>
		<label>
		  <input type="text" name="reg_person[email]" value="" placeholder="'._lang('Email').'" class="width-100" />
		</label>
		<label>
		  <input type="text" name="reg_person[pass]" value="" placeholder="'._lang('Пароль').'" class="width-100" />
		</label>
		<input type="submit" name="regPersonSubmit" value="'._lang('Подтвердить').'" class="btn btn-blue" />
		<input type="submit" name="cancelPersonSubmit" value="'._lang('Отменить').'" class="btn" />
	</form>
	</div>
	<div class="unit-25">&nbsp;</div>
</div-->
<div class="units-row">
<div class="unit-25">&nbsp;</div>
	<div class="tabs-company unit-50">
	<form name="profile" method="POST" class="forms text-centered">
		<p class="h3">'._lang('Компания').'</p>
		<input type="hidden" name="reg_comp[type]" value="2">
		<label>
		  <input type="text" name="reg_comp[fullname]" value="" placeholder="'._lang('Полное имя').'"  class="width-100" >
		</label>
		<label>
  		<input type="text" name="reg_comp[email]" value="" placeholder="'._lang('Email').'" class="width-100" />
		</label>
		<label>
		  <input type="text" name="reg_comp[comname]" value="" placeholder="'._lang('Имя компании').'"  class="width-100" />
		</label>
		<label>
		  <input type="text" name="reg_comp[pass]" value="" placeholder="'._lang('Пароль').'"  class="width-100" />
		</label>
		  <input type="submit" name="regCompanySubmit" value="'._lang('Зарегистрироваться').'"  class="btn btn-blue" />
		  <div class="forgot-password"><a href="'.getURL('profile', 'login').'">'._lang('Войти').'</a></div>
  		<!--input type="submit" name="cancelCompanySubmit" value="'._lang('Отменить').'" class="btn" /-->
	</form>
	</div>
	<div class="unit-25">&nbsp;</div>
</div>
');