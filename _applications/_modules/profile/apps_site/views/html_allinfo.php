<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Форма з усіма полями для користувача.
 *
 ***/
if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
	$profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" . _protect($_SESSION['profile_id']) . "'" );
	$profile_user = array_filter($profile_user);
	if ( empty( $profile_user ) ) {
		$PData->content( 'Пройдите Авторизацию', 'message', TRUE );
		$PData->redirect( getURL( 'profile', 'login' ) );
	}
} else {
	$PData->content( 'Пройдите Авторизацию', 'message', TRUE );
	$PData->redirect( getURL( 'profile', 'login' ) );
}

if ( isset( $_POST['cancelAllSubmit'] ) ) {
	$PData->redirect( getURL( 'profile', 'private-room' ) );
}

if ( isset( $_POST['AllSubmit'] ) ) {
	if ( !empty( $_POST['allinfo'] ) ) {
		$allinfo = array(
				'compname'   => $_POST['allinfo']['comname'],
				'compadress' => $_POST['allinfo']['comaddr']
		);
		if($profile_user[0]['pf_type_acc'] == 1) {
			$allinfo_person = array(
					'pubtel'   => $_POST['allinfo']['pubtel'],
					'pubemail' => $_POST['allinfo']['pubemail']
			);
			$allinfo = $allinfo + $allinfo_person;
		}
		if($profile_user[0]['pf_type_acc'] == 2) {
			$allinfo_comp = array(
					'resperson'  => $_POST['allinfo']['comperson'],
					'compemail'  => $_POST['allinfo']['commail'],
					'comptel'    => $_POST['allinfo']['comphone']
			);
			$allinfo = $allinfo + $allinfo_comp;
		}
		if ( !empty( $_FILES['userpic'] ) ) {
			$userpic_dir  = 'media/avatars/';
			$userpic_path = $userpic_dir . basename( $_FILES['userpic']['name'] );
			if ( move_uploaded_file( $_FILES['userpic']['tmp_name'], $userpic_path ) ) {
				$allinfo_userpic = array( 'userpic' => $userpic_path );
				$allinfo = $allinfo + $allinfo_userpic;
			}
		}
		$pf_id = 'pf_id='. _protect($_SESSION['profile_id']);
		$sql->update( '_profile',$pf_id, $allinfo, 'pf_' );

		$PData->redirect( getURL( 'profile', 'card' ) );
	}
}

$PData->content( 'main page side', 'sidebar' );

$PData->content('
<div class="units-row">
  <div class="unit-25">&nbsp;</div>
	<div class="unit-50">
	<form name="profile" method="POST" class="forms text-centered" enctype="multipart/form-data">
		<label>
			<input type="text" name="allinfo[fullname]" value="'.$profile_user[0]['pf_fullname'].'" placeholder="' . _lang( 'ФИО' ) . '" class="width-100" />
		</label>
		<label>
			<input type="text" name="allinfo[email]" value="'.$profile_user[0]['pf_email'].'" placeholder="' . _lang( 'Email' ) . '" class="width-100" />
		</label>
		<h3>'._lang('Дополнительная информация').'</h3>');
		if($profile_user[0]['pf_type_acc'] == 1) {
			$PData->content( '
				<label>
					<input type="text" name="allinfo[pubtel]" value="' . $profile_user[0]['pf_pubtel'] . '" placeholder="' . _lang( 'Номер Телефона' ) . '" class="width-100" />
				</label>
				<label>
					<input type="text" name="allinfo[pubemail]" value="' . $profile_user[0]['pf_pubemail'] . '" placeholder="' . _lang( 'Дополнительный Email' ) . '" class="width-100" />
				</label>' );
		}
$PData->content('
		<label>
			<input type="file" name="userpic" value="" placeholder="'._lang('Авка').'" class="width-100" />
		</label>
		<h3>'._lang('Компания').'</h3>
		<label>
			<input type="text" name="allinfo[comname]" value="'.$profile_user[0]['pf_compname'].'" placeholder="' . _lang( 'Имя Компании' ) . '" class="width-100" />
		</label>
		<label>
			<input type="text" name="allinfo[comaddr]" value="'.$profile_user[0]['pf_compadress'].'" placeholder="' . _lang( 'Адрес Компании' ) . '" class="width-100" />
		</label>' );

		if($profile_user[0]['pf_type_acc'] == 2) {
			$PData->content( '
				 <label>
							<input type="tel" name="allinfo[comperson]" value="' . $profile_user[0]['pf_resperson'] . '" placeholder="' . _lang( 'Имя Преставителя Компании' ) . '" class="width-100" />
				 </label>
				 <label>
							<input type="email" name="allinfo[commail]" value="' . $profile_user[0]['pf_compemail'] . '" placeholder="' . _lang( 'Email Компании' ) . '" class="width-100" />
				 </label>
				 <label>
							<input type="text" name="allinfo[comphone]" value="' . $profile_user[0]['pf_comptel'] . '" placeholder="'._lang('Номер Телефона Компании').'" class="width-100" />
				 </label>' );
		}
$PData->content('
					<input type="submit" name="AllSubmit" value="'._lang('Подтвердить').'" class="btn btn-blue width-40" />
					<input type="submit" name="cancelAllSubmit" value="'._lang('Пропустить').'" class="btn width-40" />
	</form>
	</div>
	<div class="unit-25">&nbsp;</div>
</div>
');