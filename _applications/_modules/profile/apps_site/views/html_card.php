<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Форма введення інформації по карті користувача.
 *
 ***/
if ( !empty($_SESSION['profile_id']) && $_SESSION['profile_id'] != 0 ) {
  $login_check = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" . _protect($_SESSION['profile_id']) . "'" );
  if ( empty( $login_check ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  }
} else {
  $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
  $PData->redirect( getURL( 'profile', 'login' ) );
}

if ( isset( $_POST['cancelСardSubmit'] ) ) {
  $PData->redirect(getURL('profile','private-room'));
}

if ( isset( $_POST['CardSubmit'] ) ) {

  if ( empty( $_POST['card']['card_adress'] ) || empty( $_POST['card']['card_type'] ) || empty( $_POST['card']['card_number'] ) || empty( $_POST['card']['card_owner'] ) ) {
    $PData->content(
        'Нужно заполнить все поля с *',
        'message'
    );
  } else {

    $card_array = array(
        'card_adress' => $_POST['card']['card_adress'],
        'card_type'   => $_POST['card']['card_type'],
        'card_number' => $_POST['card']['card_number'],
        'card_owner'  => $_POST['card']['card_owner']
    );

    $pf_id = 'pf_id='. $_SESSION['profile_id'];
    $sql->update( '_profile', $pf_id, $card_array, 'pf_' );
    $PData->redirect(getURL('profile','private-room'));

  }
}

$PData->content('
<div class="units-row">
  <div class="unit-25">&nbsp;</div>
	<div class="unit-50">
      <form name="card" method="POST" class="forms">
        <label>
          <input type="text" name="card[card_adress]" value="" placeholder="'._lang('Платежный Адрес').'" class="width-100"/>
        </label>
        <label>
          <input type="text" name="card[card_type]" value="" placeholder="'._lang('Тип Кредитной Карты').'" class="width-100"/>
        </label>
        <label>
          <input type="text" name="card[card_number]" value="" placeholder="'._lang('Номер Кредитной Карты').'" class="width-100"/>
        </label>
        <label>
          <input type="text" name="card[card_owner]" value="" placeholder="'._lang('Имя Вледельца Карты').'" class="width-100"/>
        </label>
        <input type="submit" name="CardSubmit" value="'._lang('Сохранить').'" class="btn btn-blue">
        <input type="submit" name="cancelСardSubmit" value="'._lang('Пропустить').'" class="btn">
      </form>
  </div>
	<div class="unit-25">&nbsp;</div>
</div>

');