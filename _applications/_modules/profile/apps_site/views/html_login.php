<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Форма Авторизації.
 *
 ***/

if(!isset($_SESSION)){
	session_start();
}
if ( !empty($_SESSION['profile_id']) && $_SESSION['profile_id'] != 0 ) {
	$login_check = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" . _protect($_SESSION['profile_id']) . "'" );
	if ( empty( $login_check ) ) {
		$PData->content( 'Пройдите Авторизацию', 'message', TRUE );
		$PData->redirect( getURL( 'profile', 'login' ) );
	} else {
		$PData->content( 'Вы уже авторизованы', 'message', TRUE );
		$PData->redirect( getURL( 'profile', 'allinfo' ) );
	}
}

if ( isset( $_POST['logSubmit'] ) ) {
	if ( empty( $_POST['pf_log']['email'] ) || empty( $_POST['pf_log']['pass'] ) ) {
		$PData->content( 'Введите логин или пароль!', 'message', TRUE );
	} else {
		$login_check = $sql->query( "
				SELECT pf_id
				FROM _profile
				WHERE pf_email = '" . _protect($_POST['pf_log']['email']) . "' AND pf_pass = '" . _protect(md5($_POST['pf_log']['pass'])) . "'" );
		if ( $login_check ) {
			$_SESSION['profile_id'] = $login_check[0]['pf_id'];

			$PData->redirect( getURL('profile','') );

		} else {
			$PData->content( 'Нерпавильный логин или пароль!', 'message', TRUE );
		}
	}
}

$PData->content('
	<form name="login" method="POST" class="forms text-centered">
		<h3>'._lang('Личный кабинет').'</h3>
		<label for="login">
			<input type="text" name="pf_log[email]" value="" placeholder="'._lang('Логин/Email').'" class="width-100" />
		</label>
		<label for="password">
			<input type="password" name="pf_log[pass]" value="" placeholder="'._lang('Пароль').'" class="width-100" />
		</label>
		<input type="submit" name="logSubmit" value="'._lang('Войти').'">
		<div class="forgot-password"><a href="#">'._lang('Востановить пароль').'</a></div>
	</form>
');