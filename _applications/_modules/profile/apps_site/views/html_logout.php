<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}

/*setcookie('user','0',(time()-3600));
unset($_COOKIE['user']);*/

if ( !empty($_SESSION['profile_id']) && $_SESSION['profile_id'] != 0 ) {
  session_destroy();
  $_SESSION['profile_id'] = 0;
}

$PData->redirect( getURL( 'profile', 'login' ) );