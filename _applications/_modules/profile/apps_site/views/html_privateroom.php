<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Перша сторінка приватного кабінету
 *
 ***/

if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
  $profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'" );
   $profile_user = array_filter($profile_user);
  if ( empty( $profile_user ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  }
} else {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
}

if ( isset( $_POST['roomSubmit'] ) ) {
    if ( !empty( $_POST['room'] ) ) {
       /* $room = array(
            'compname'   => $_POST['room']['comname'],
            'compadress' => $_POST['room']['comaddr']
        );
        if ($_POST['room']['newpass'] == $_POST['room']['confirmpass']) {
           $room_pass = array(
              'pass' => md5( $_POST['room']['newpass'] )
           );
           $room = $room + $room_pass;
        }
        if($profile_user[0]['pf_type_acc'] == 1) {
            $room_person = array(
                'pubtel'   => $_POST['room']['pubtel'],
                'pubemail' => $_POST['room']['pubemail']
            );
            $room = $room + $room_person;
        }
        if($profile_user[0]['pf_type_acc'] == 2) {
            $room_comp = array(
                'resperson'  => $_POST['room']['comperson'],
                'compemail'  => $_POST['room']['commail'],
                'comptel'    => $_POST['room']['comphone']
            );
            $room = $room + $room_comp;
        }*/
        if ($_POST['room']['pf_pass'] == $_POST['room']['confirmpass']) {
            $_POST['room']['pf_pass'] = md5( $_POST['room']['pf_pass'] );
            unset($_POST['room']['confirmpass']);
        } else {
            $PData->content('Введите новый пароль занового и убедитесь в правильном заполнении','message',TRUE);
        }
        $post = $_POST['room'];
        $profile_user[0] = array_merge($profile_user[0], $post);

        $pf_id = 'pf_id='. _protect($_SESSION['profile_id']);
        $sql->update( '_profile',$pf_id,  $profile_user[0] );
    }
}

if ( isset( $_POST['changePhotoSubmit'] ) ) {
    if ( isset( $_FILES['change_photo'] ) ) {
        $valid_formats = array( "jpg", "png", "gif", "bmp", "jpeg" );
        list( $txt, $ext ) = explode( ".", $_FILES['change_photo']['name'] );
        if ( in_array( $ext, $valid_formats ) ) {
            if ( $_FILES['change_photo']['size'] < ( 1024 * 1024 ) ) {
                $userpic_dir  = 'media/avatars/';
                $userpic_path = $userpic_dir . basename( $_FILES['change_photo']['name'] );
                if ( move_uploaded_file( $_FILES['change_photo']['tmp_name'], $userpic_path ) ) {
                    $profile_user[0]['pf_userpic'] = $userpic_path;

                    $allinfo_userpic = array( 'userpic' => $userpic_path );
                    $pf_id        = 'pf_id=' . _protect($_SESSION['profile_id']);
                    $sql->update( '_profile', $pf_id, $allinfo_userpic, 'pf_' );
                } else return "failed";
            } else return "Image file size max 1 MB";
        } else return "Invalid file format..";
    } else return "Please select image..!";
}

$pf_userpic = '<img alt="photo" src="'.SITE_FOLDER.'media/avatars/no_image.jpg" id="preview">';

if(_checkURL(SITE_FOLDER.$profile_user[0]['pf_userpic'])){
    $pf_userpic = '<img alt="photo" src="'.SITE_FOLDER.$profile_user[0]['pf_userpic'].'" id="preview">';
}

$PData->content('Личные данные','title');

$PData->content(
	'<ul class="listmenu">
    	'.pf_sidebar().'
    </ul>'
,'sidebar');

$PData->content(
'<div class="unit-30 photo">'
	.$pf_userpic.
	'<br/><a href="#callback" class="open_popup">Change a photo</a>
    <ul class="forms-list">
        <li>
            <input type="checkbox" name="checkbox-1" id="checkbox-1">
            <label for="checkbox-1">In list of donors</label>
        </li>
        <li>
            <input type="checkbox" name="checkbox-2" id="checkbox-2">
            <label for="checkbox-2">Subscribe on news</label>
        </li>
    </ul>
</div>
<div class="unit-60">
    <form method="post" action="" class="forms">
        <label>
        	<span class="nameform">Profile</span>
        <label>
            <input type="text" name="room[pf_fullname]" value="'.$profile_user[0]['pf_fullname'].'" placeholder="' . _lang( 'ФИО' ) . '" class="width-100" />
        </label>
        <label>
            <input type="email" name="room[pf_email]" value="'.$profile_user[0]['pf_email'].'" placeholder="' . _lang( 'Email' ) . '" class="width-100" />
        </label>
');
                                
if($profile_user[0]['pf_type_acc'] == 1) {
    $PData->content( '
        <label>
            <input type="text" name="room[pf_pubtel]" value="' . $profile_user[0]['pf_pubtel'] . '" placeholder="' . _lang( 'Номер Телефона' ) . '" class="width-100" />
        </label>
        <label>
            <input type="email" name="room[pf_pubemail]" value="' . $profile_user[0]['pf_pubemail'] . '" placeholder="' . _lang( 'Дополнительный Email' ) . '" class="width-100" />
        </label>
	');
}

$PData->content('
        </label>
        <label>
            <span class="nameform second">Company</span>
        <label>
            <input type="text" name="room[pf_compname]" value="'.$profile_user[0]['pf_compname'].'" placeholder="' . _lang( 'Имя Компании' ) . '" class="width-100" />
        </label>
        <label>
            <input type="text" name="room[pf_compadress]" value="'.$profile_user[0]['pf_compadress'].'" placeholder="' . _lang( 'Адрес Компании' ) . '" class="width-100" />
        </label>'
);
	
if($profile_user[0]['pf_type_acc'] == 2) {
	$PData->content( '
        <label>
            <input type="text" name="room[pf_resperson]" value="' . $profile_user[0]['pf_resperson'] . '" placeholder="' . _lang( 'Имя Преставителя Компании' ) . '" class="width-100" />
        </label>
        <label>
            <input type="email" name="room[pf_compemail]" value="' . $profile_user[0]['pf_compemail'] . '" placeholder="' . _lang( 'Email Компании' ) . '" class="width-100" />
        </label>
        <label>
            <input type="text" name="room[pf_comptel]" value="' . $profile_user[0]['pf_comptel'] . '" placeholder="'._lang('Номер Телефона Компании').'" class="width-100" />
        </label>'
	);
}

$PData->content( '
        <label class="greyblock">
            <span class="nameform">Change password</span>
            <label>
                <input type="password" name="room[pf_pass]" placeholder="' . _lang( 'Новый Пароль' ) . '" class="width-100" />
            </label>
            <label>
                <input type="password" name="room[confirmpass]" placeholder="' . _lang( 'Пароль еще раз' ) . '" class="width-100" />
            </label>
        </label>
            <input type="submit" name="roomSubmit" value="'._lang('Изменить').'" class="btn" />
    </form>
</div>
');

$PData->content('
<div class="hide">
	<div id="callback">
        <form method="post"  action="" enctype="multipart/form-data" class="forms" id="imageform">
            <p>'._lang('Выбрать новое фото').'</p>
            <input type="hidden" name="MAX_FILE_SIZE" value="4194304" />
            <label>
                <input name="change_photo" type="file" class="width-100" id="change_photo"/>
            </label>
             <input type="submit" value="'._lang('Изменить Фото').'" name="changePhotoSubmit"  id="buttonForm" />
        </form>
        <img id="loading" src="/images/loader.gif" style="display:none;" />
        <p id="message"></p>
        <p id="result"></p>
	</div>
</div>');