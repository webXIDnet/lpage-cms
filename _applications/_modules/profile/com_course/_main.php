<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * Компонента "Курсів" в Адмін Панелі
 *
 ***/

require_once 'co_fn.php'; //підключення файлу функції компоненти

_add_action('defined_modules', 'course_defined_modules');

function course_defined_modules(){

	if(PAGE_TYPE=='admin'){
		global $sql;

		$result = $sql->db_table_installer('com_course','26.01.2015', //перевірка на відповідність поточної БД до останньої верстії структури
			array(
				'_courses_tests' => array(
					'ct_id' => "`ct_id` int(11) NOT NULL AUTO_INCREMENT",
					'ct_parent_id' => "`ct_parent_id` int(11) NOT NULL",
					'ct_type' => "`ct_type` varchar(100) NOT NULL",
					'ct_question' => "`ct_question` text NOT NULL",
					'ct_answers' => "`ct_answers` text NOT NULL",
					'CREATE' => "`ct_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`ct_id`)"
				),
				'_courses_answers' => array(
					'cta_id' => "`cta_id` int(11) NOT NULL AUTO_INCREMENT",
					'cta_date' => "`cta_date` datetime NOT NULL",
					'cta_pf_id' => "`cta_pf_id` int(11) NOT NULL",
					'cta_ct_id' => "`cta_ct_id` int(11) NOT NULL",
					'cta_mark' => "`cta_mark` varchar(255) NOT NULL",
					'CREATE' => "`cta_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`cta_id`)"
				),
				'_courses_results' => array(
					'cr_id' => "`cr_id` int(11) NOT NULL AUTO_INCREMENT",
					'cr_profile_id' => "`cr_profile_id` int(11) NOT NULL",
					'cr_parent_id' => '`cr_parent_id` int(11) NOT NULL',
					'cr_type' => "`cr_type` varchar(50) NOT NULL",
					'cr_date' => "`cr_date` datetime NOT NULL",
					'cr_name' => "`cr_name` text NOT NULL",
					'cr_result' => "`cr_result` text NOT NULL",
					'CREATE' => "`cr_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`cr_id`)"
				)
			)
		);

		$result = $sql->db_table_installer('com_course_profile','25.01.2015', //перевірка на відповідність поточної БД до останньої верстії структури
			array(
				'_profile' => array(
					'pf_create_date' => "`pf_create_date` int(11) NOT NULL",
					'pf_end_date' => "`pf_end_date` int(11) NOT NULL",
					'pf_esse' => "`pf_esse` text NOT NULL",
					'pf_esse_done_date' => "`pf_esse_done_date` int(11) NOT NULL",
					'CREATE' => "`pf_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`pf_id`)"
				)
			)
		);
	}
}

if(PAGE_TYPE=='admin'){
	_add_filter('profile_admin_sidebar_menu_submenu', 'profile_courses_admin_sidebar_menu', 26);
	_add_filter('profile_admin_sidebar_menu_active_items', 'profile_admin_sidebar_menu_active_items');

}

function profile_courses_admin_sidebar_menu($result)
{
	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Курсы'), //ім"я пункта меню
			'url' => getURL('admin', 'courses'), //посилання
			'active_pages' => array('courses'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
		array(
			'title' => _lang('Готовый Эссе'), //ім"я пункта меню
			'url' => getURL('admin', 'esses'), //посилання
			'active_pages' => array('esses'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . $menu;

  $menu = u_ifRights(
	  array(3,4,5,6,10,100),
	  '
		  <li>
			<a href="'.getURL('admin','courses').'">'._lang('Курсы').'</a>
		  </li>
	  <li>
		<a href="'.getURL('admin','esses').'">'._lang('Готовый Эссе').'</a>
	  </li>'
  );

 return $result . @$menu;
}

function profile_admin_sidebar_menu_active_items($result)
{
	$result[] = 'courses';
	$result[] = 'esses';
	return $result;
}

//Додавання кнопки "Список питань" на сторінку теми
_add_filter('Course_edit_theme_before_title','course_insert_quest_btn');
//Додавання кнопки "Список питань" на сторінку модуля
_add_filter('Course_edit_module_before_title','course_insert_quest_btn');
//Додавання пункта меню "Курси" на сторінку профіля користувача
_add_filter('profile_left_menu','course_insert_courses_btn');
//Додавання лінку "Назад" на сторінці редагування питань тесту
_add_filter('Course_test_after_title','course_test_back_btn');


if( isset($_GET['ajax']) && isset($_GET['esse-autosave']) ){
  #Зберігає дані з поля через аякс запит
  _add_action('ajax_init','esse_autosave_ajax_init');
}


/** Роутер Адмінки **/

if (PAGE_TYPE=='admin') {
	_add_hook('adminPanel_controller', 'Course_admin_controller');
}

function Course_admin_controller(){

  global	$PData,
		  $admin,
		  $user,
		  $sql,
		  $sqlTPL,
		  $contTPL,
		  $htmlTPL;
		  // var_dump(@$_GET['admin']);

  switch(@$_GET['admin']){
	case'courses': /* Список курсів */
	  require_once __DIR__ . '/apps_admin/a_co_courses.php';
	  return TRUE;
	  break;
	case'course': /* Список модулів, Редагування курсу */
	  require_once __DIR__ . '/apps_admin/a_co_course_modules.php';
	  return TRUE;
	  break;
	case'module': /* Список тем, Редагування модуля */
	  require_once __DIR__ . '/apps_admin/a_co_module_themes.php';
	  return TRUE;
	  break;
	case'theme': /* Список модулів, Редагування теми */
	  require_once __DIR__ . '/apps_admin/a_co_theme_edit.php';
	  return TRUE;
	  break;
	case'theme-add': /* Додавання нової теми */
	  require_once __DIR__ . '/apps_admin/a_co_theme_add.php';
	  return TRUE;
	  break;
	case'test': /* Список питань тесту */
	  require_once __DIR__ . '/apps_admin/a_co_test.php';
	  return TRUE;
	  break;
	case'edit-question': /* Редагування питання тесту */
	  require_once __DIR__ . '/apps_admin/a_co_question_edit.php';
	  return TRUE;
	case'esses': /* Список ессе */
	  require_once __DIR__ . '/apps_admin/a_co_esses.php';
	  return TRUE;
	  break;
  }
}

/** Роутер Профайла **/

_add_hook('profile_controller', 'private_room_profile_controller');

function private_room_profile_controller(){

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL,
			$htmlTPL;

	switch(@$_GET['profile']){
		case 'courses':
			require_once __DIR__ .'/apps_site/s_co_courses.php';
			return TRUE;
		break;

		case 'modules':
			require_once __DIR__ .'/apps_site/s_co_modules.php';
			return TRUE;
		break;

		case 'themes':
			require_once __DIR__ .'/apps_site/s_co_themes.php';
			return TRUE;
		break;

		case 'theme':
			require_once __DIR__ .'/apps_site/s_co_single_theme.php';
			return TRUE;
		break;

		case 'test':
			require_once __DIR__ .'/apps_site/s_co_test.php';
			return TRUE;
		break;
		case'topics-tests-list':
			require_once __DIR__ . '/apps_site/s_co_topics_tests_list.php';
			return TRUE;
		break;
	}
}