<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Виведення списку курсів
 *
 ***/
if(isset($_POST['newCourse'])) {
	if ( !empty( $_POST['course'] ) && is_array( $_POST['course'] ) ) {
		$course_arr = $_POST['course'];
		if ( !empty( $_FILES['course_img'] ) ) {
			$userpic_dir  = 'media/files/miniatures/';
			$userpic_path = $userpic_dir . basename( $_FILES['course_img']['name'] );
			if ( move_uploaded_file( $_FILES['course_img']['tmp_name'], $userpic_path ) ) {
				$course_img = array( 'img' => $userpic_path );
				$course_arr = $course_arr + $course_img;
			}
		}
		$sql->insert( '_courses', $course_arr, 'co_' );

		$PData->content( 'Запись Сохранена', 'message', TRUE );
	} else {
		$PData->content( 'Необходимо заполнить информацию по новому курсу', 'message', TRUE );

	}
}

$courses_list = $sql->query("SELECT *	FROM _courses	ORDER BY co_title ASC	");
$PData->content('
	<form class="list" method="POST"  enctype="multipart/form-data">
		<table>
			<tr>
				<td>
					<label><b>+ '._lang('Добавить Курс').'</b>:</label>
					<input placeholder="'._lang('Заголовок').'" type="text" name="course[title]" value=""/>
					<input placeholder="'._lang('Цена').'" type="text" name="course[price]" value=""/><br/>
					<textarea placeholder="'._lang('Описание').'" type="textarea" name="course[desc]" value=""></textarea>
					<input placeholder="'._lang('Изображение').'" type="file" name="course_img" value=""/>
					<br/>
					<input type="submit" class="btn" name="newCourse" value="'._lang('Добавить').'"/>
				</td>
			</tr>
		</table>
	</form>
	<hr/>
');
$PData->content('
<table class="table">
		      <thead>
		        <tr>
							<th class="numer">'._lang('Номер Курса').'</th>
							<th>'._lang('Название').'</th>
							<th>'._lang('Цена').'</th>
							<th>'._lang('Описание').'</th>
							<th>'._lang('Изображение').'</th>
		        </tr>
		      </thead>
		      <tbody>
');
if(isset($courses_list)){
	$i = 1;
	foreach ( $courses_list as $course ) {
		$PData->content('
			<tr>
				<td>'.$i++.'</td>
				<td><a title="'._lang('Список Модулей').'" href="'.getURL('admin','course','id='.$course['co_id']).'">'.$course['co_title'].'</a></td>
				<td>'.$course['co_price'].'</td>
				<td>'.$course['co_desc'].'</td>
				<td>'.$course['co_img'].'</td>
			</tr>
');
	}
}

$PData->content('Курсы','title');

$PData->content('
		      </tbody>
		    </table>
');