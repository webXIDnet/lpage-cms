<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Список Ессе
 *
***/

global $meta;

if(isset($_POST['delSubmit']) && isset($_POST['itemID'])){
	$where = '';

	foreach ($_POST['itemID'] as $key => $val) {
		if(!empty($where))$where .= ' OR ';
		$where .= "pf_id='"._protect($key)."'";

	}
	if(!empty($where))
		$sql->delete('_profile',$where);

}elseif(isset($_POST['formsubmit-edit'])){

	foreach($_POST['editAccount'] as $account_id => $array) {

		$array['end_date'] = strtotime($array['end_date']);
		$array['create_date'] = strtotime($array['create_date']);

		if(!empty($array['create_date']) && empty($array['end_date']))
			$array['end_date'] = $array['create_date'] + $meta->val('days_of_access')*24*60*60;

		$sql->update('_profile',"pf_id='"._protect($account_id)."'",$array,'pf_');
		break;
	}
}

/** Пагінація **/

$parent_select = $text = $where = '';
$managers = array();

$where = "WHERE pf_esse_done_date>'1000'";

$nav = _pagination(
	'_profile',
	'pf_id',
	$where);


/** Отримання даних про замовлення з бази **/

$result = $sql->query("
	SELECT *
	FROM _profile
	{$where}
	"._run_filter('a_profile_setORDER_BY','ORDER BY pf_esse_done_date DESC')."

	".@$nav['limit']."
");


if(!is_array($result) || empty($result)){

	if(empty($_GET['tab'])){
		$text = _lang('<b>Заказов больше нет</b><br/>Отличная работа! =)');
	}else{
		$text = _lang('Не найдено! =\ ');
	}
}else{

	foreach($result AS $key => $cval){
		$text .= pf_accounts_editing_form($cval);
	}

	$text = '
	<form method="POST">
		<table id="tableEdit" class="table">
			<tbody>
				<tr>
					<th id="head-tableList" class="head head0 tableList0">'._lang('Выбрать').'</th>
					<th id="head-tableList" class="head head1 tableList0">'._lang('Клиент').'</th>
					<th id="head-tableList" class="head head2 tableList1">'._lang('Доступ').'</th>
					<th id="head-tableList" class="head head4 tableList2">'._lang('Эссе').'</th>
				</tr>
				'.$text.'
			</tbody>
		</table>
		<input type="submit" name="delSubmit" value="'._lang('Удалить').'">
	</form>
	<style>
	.head0{width: 10%;max-width:80px;}
	.head1,.head3{width: 20%;}
	.head2,.head4{width: 25%;}
	</style>
	';
}


/** __________ Формування основного HTML коду __________ **/


$PData->content('Эссе','title');


/** // Отримуємо список Посадок і міток для Фільтра **/


$PData->content(

		'


		<hr/>
		<br/>
		<div class="well">
		    '.$text.'
		</div>


<script type="text/javascript">

</script>
'.$nav['html']);
