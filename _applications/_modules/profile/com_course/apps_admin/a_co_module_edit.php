<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Редагування модуля тестування
 * 
***/

if(isset($_POST['editCat']) && is_array($_POST['editCat'])){

	if(empty($_POST['editCat']['url'])){
		$url = $_POST['editCat']['url'] = _textToURL($_POST['editCat']['title']);
	}else{
		$url = $_POST['editCat']['url'] = _textToURL($_POST['editCat']['url']);
	}
	
	if(!isset($_POST['category_type'])) $_POST['category_type']='course1';
	
	$cval = $sql->query("
		SELECT c_id
		FROM _categories 
		WHERE c_url='"._protect($url)."' AND c_type='"._protect(@$_POST['category_type'])."' AND c_id!='"._protect($_GET['module'])."'
	",'value');
			
	while($cval){

		$_POST['newCat']['url']=$url.'-'.$i;
		$i++;
		
		$cval = $sql->query("
			SELECT c_id 
			FROM _categories 
			WHERE c_url='"._protect($_POST['newCat']['url'])."' AND c_type='"._protect(@$_POST['category_type'])."' AND c_id!='"._protect($_GET['module'])."'
		",'value');

	}
	
	$array = $_POST['editCat'];
	$array['title'] = trim($array['title']);
	$array['parent_id'] = (int)$array['parent_id'];	
	
	$sql->update('_categories',"c_id='"._protect($_GET['module'])."'",$array,'c_');
	
}


/** Формування списку категорій **/

$sqlTPL->getCategories();

global $list;

if(!isset($list->category_index[@$_GET['module']])){
	$PData->content('Редактирование модуля','title');
	$PData->content('Не верная ссылка на страницу редактирования категории');
	return;
}

$cval = $list->category_index[$_GET['module']];

if(is_array($list->category[$cval['c_type']])){
	$parent_select = $contTPL->catHierarchTree($list->category[$cval['c_type']],0,$cval['c_parent_id'],$cval['c_id']);
}

$PData->content(_lang('Редактирование модуля').' ('.$cval['c_type'].')','title');

$PData->content('

	<form class="list" method="POST">
		<table>
			<tr>
				<td>
					<label><b>'._lang('Заголовок').'</b>:</label>
					<input placeholder="'._lang('Заголовок').'" type="text" name="editCat[title]" value="'.$cval['c_title'].'"/>
					
					<label><b>'._lang('Текстовая ссылка').'</b>:</label>
					<input placeholder="'._lang('Текстовая ссылка').'" type="text" name="editCat[url]" value="'.$cval['c_url'].'"/>
					
					<label><b>'._lang('Родительская категория').'</b>:</label>
					<select name="editCat[parent_id]">
						<option value="">- '._lang('Родительская категория').' -</option>
						'.$parent_select.'
					</select>
                    
                    <!-- Filter: app_admin_editCat_addField -->
					'._run_filter('app_admin_editCat_addField', '', $cval).'
					<label><b>'._lang('Тип категории').'</b>: '._lang($cval['c_type']).'</label>
					<input type="hidden" name="category_type" value="'.$cval['c_type'].'"/>
							
					<br/><br/>
					<input type="submit" class="btn" value="'._lang('Сохранить').'"/>
				</td>
			</tr>
		</table>
	</form>
	<hr/>

');