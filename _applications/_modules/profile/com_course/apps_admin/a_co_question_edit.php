<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Додавання тесту
 * 
***/

$sqlTPL->getCategories('course*');

global $list;

$postType='course';

if(!empty($_GET['type']))
    $postType = $_GET['type'];





//Захист від невірного запиту без параметру
if (!isset($_GET['id']))
{
    header("Status: 404 Not Found");
    echo '<h2>'._lang('Страница не существует').'</h2>';
    exit();
}

$query = $sql->query("SELECT * FROM _courses_tests WHERE ct_id='".$_GET['id']."'",'value');

//Захист від неіснуючого ID
if (count($query) == 0)
{
    header("Status: 404 Not Found");
    echo '<h2>'._lang('Страница не существует').'</h2>';
    exit();
}

if (isset($_POST['postAdd']) && is_array($_POST['postAdd']))
{

    $insert_data = array();
    $insert_data['question'] = addslashes($_POST['postAdd']['text']);

    /*foreach ($_POST['postAdd']['answers'] as $answer) {
        $insert_data []= $answer;
    }*/
    // $insert_data['type'] = _protect($_GET['type']);
    // $insert_data['parent_id'] = _protect($_GET['id']);
    echo '<pre>';print_r($_POST);echo '</pre>';
    $insert_data['answers'] = json_encode($_POST['postAdd']['answers']);
    $sql->update('_courses_tests','ct_id='.$_GET['id'],$insert_data,'ct_');

    $PData->content( _lang('Вопрос сохранен'), 'message' );
    // print_r($insert_data['answers']);
}

$query = $sql->query("SELECT * FROM _courses_tests WHERE ct_id='".$_GET['id']."'",'value');


$answers = '';
$i = 1;
// print_r(json_decode($query['ct_answers']));
if (isset($query))
foreach (json_decode($query['ct_answers'],true) as $answer) {
    $answers .= '
        <tr class="answer">
                <td class="number">
                    '.$i.'
                </td>         
                <td>
                    <textarea type="text" style="width: 500px; height: 50px;" class="input-xlarge text" name="postAdd[answers]['.$i.'][text]">'.$answer['text'].'</textarea>
                </td> 
                <td>
                    <label><input class="right-flag" name="postAdd[answers]['.$i.'][right]" '.(isset($answer['right']) ? 'checked' : '').' type="checkbox"> '._lang('Вариант верный').'</label>
                </td>
                <td>
                    <input class="btn btn-black del-answer" type="button" value="'._lang('Удалить').'"/>
                </td>
            </tr> 
    ';
    $i++;
}

$PData->content( 'Редактирование вопроса', 'title' );


$PData->content('
    <form class="list" method="POST" enctype="multipart/form-data" id="author_form">        
        
           
        
        <!-- Filter: Blog_post_Add_Page_1 -->
        '._run_filter('Blog_post_Add_Page_1').'
        
        <p> 
            <label>'._lang('Текст вопроса').':</label>
            '.$admin->getWIZIWIG('postAdd[text]',$query['ct_question'],'elm1').'
        </p>
        <p>
            '._lang('Варианты ответов:').'
        </p>
        <p>  
        <table class="table answer-table"> 
            <tr>
                <th>#</th>
                <th>'._lang('Текст ответа').'</th>
                <th></th>
                <th></th>
            </tr>
            '.$answers.' 
            <tr class="add-answer-button">
                <td class="number"></td>         
                <td><input class="btn btn-black add-answer" type="button" value="'._lang('Добавить ответ').'"/></td> 
                <td></td>
                <td></td>
            </tr>              
        </table>
        <script type="text/javascript" src="/js/test.js"></script>
        </p>
        
        <!-- Filter: Blog_post_Add_Page_3 -->
        '._run_filter('Blog_post_Add_Page_3').'
        
        <input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="postNewsubmit"/>
    </form>
    <hr>
');