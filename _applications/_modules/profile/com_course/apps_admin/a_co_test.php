<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Додавання тесту
 * 
***/

$sqlTPL->getCategories('course*');

global $list;

$postType='course';

if(!empty($_GET['type']))
    $postType = $_GET['type'];


if (!isset($_GET['type']) || !isset($_GET['id']))
{
    header("Status: 404 Not Found");
    echo '<h2>'._lang('Страница не существует').'</h2>';
    exit();
}

if (isset($_POST['del_quest']) && isset($_POST['quest_id']))
{
    $data = array();
    foreach ($_POST['quest_id'] as $id) {
        $data []= 'ct_id='.$id; 
    }
    // print_r(implode(' OR ', $data));
    $sql->delete('_courses_tests',implode(' OR ', $data),'ct_');
}

// var_dump($_POST);

if (isset($_POST['postAdd']) && is_array($_POST['postAdd']))
{

    // echo '<pre>';print_r($_POST['postAdd']['text']);echo '</pre>';
    $insert_data = array();
    $insert_data['question'] = addslashes($_POST['postAdd']['text']);

    /*foreach ($_POST['postAdd']['answers'] as $answer) {
        $insert_data []= $answer;
    }*/
    $insert_data['type'] = _protect($_GET['type']);
    $insert_data['parent_id'] = _protect($_GET['id']);
    $insert_data['answers'] = json_encode($_POST['postAdd']['answers']);
    $sql->insert('_courses_tests',$insert_data,'ct_');

    $PData->content( _lang('Вопрос добавлен'), 'message' );

    // print_r($insert_data['answers']);
}

$type = $_GET['type'];
$id = $_GET['id'];

$questions = '';
$query = $sql->query("SELECT * FROM _courses_tests WHERE ct_type='".$type."' AND ct_parent_id='".$id."' ORDER BY ct_id ASC ".@$nav['limit']."");
// var_dump(count($query));
if (isset($query) && count($query) > 0)
{
    $i = 1;
    foreach ($query as $row) 
    {
        $questions .= '
            <tr>
                <td>                    
                        <input type="checkbox" name="quest_id[]" value="'.$row['ct_id'].'">        
                </td>
                <td style="text-align:center;">'.$i++.'</td>
                <td><a href="'.getURL('admin','edit-question','id='.$row['ct_id']).'" style="text-decoration:none;">'.(strlen(nl2br($row['ct_question'])) > 150 ? substr(nl2br($row['ct_question']),0,200).'...' : nl2br($row['ct_question'])).'</a></td>
              
            </tr>
        ';
    }
}
else
{
    $questions .= '
            <tr>                
                <td colspan="5" style="text-align: center;">'._lang('Нет вопросов').'</td>               
            </tr>
    ';
}

$PData->content( 'Редактирование вопросов теста', 'title' );

$PData->content(_run_filter('Course_test_after_title'));

$PData->content('
        <form method="POST">
            <table class="table"> 
                <tr>
                    <th style="width:20px;">
                        
                    </th>
                    <th style="width:50px;text-align:center;">
                        #
                    </th>
                    <th>
                        '._lang('Текст вопроса').'
                    </th>
                    
                </tr>
                    '.$questions.'                
            </table>
            <input class="btn btn-black del-question" type="submit" name="del_quest" value="'._lang('Удалить').'"/>
        </form>
            <hr>

');

$PData->content('
    <form class="list" method="POST" enctype="multipart/form-data" id="author_form">        
        
        <p>
            <h3>'._lang('Добавить вопрос').'</h3> 
        </p>        
        
        <!-- Filter: Blog_post_Add_Page_1 -->
        '._run_filter('Blog_post_Add_Page_1').'
        
        <p> 
            <label>'._lang('Текст вопроса').':</label>
            '.$admin->getWIZIWIG('postAdd[text]',@$_POST['postAdd']['text'],'elm1').'
        </p>
        <p>
            '._lang('Варианты ответов:').'
        </p>
        <p>  
        <script type="text/javascript" src="/js/test.js"></script>
        <table class="table answer-table"> 
            <tr>
                <th>#</th>
                <th>'._lang('Текст ответа').'</th>
                <th></th>
                <th></th>
            </tr>
            <tr class="answer">
                <td class="number">
                    1
                </td>         
                <td>
                    <textarea type="text" style="width: 500px; height: 50px;" class="input-xlarge text" name="postAdd[answers][1][text]"></textarea>
                </td> 
                <td>
                    <label><input class="right-flag" name="postAdd[answers][1][right]" type="checkbox"> '._lang('Вариант верный').'</label>
                </td>
                <td>
                    <input class="btn btn-black del-answer" type="button" value="'._lang('Удалить').'"/>
                </td>
            </tr> 
            <tr class="add-answer-button">
                <td class="number"></td>         
                <td><input class="btn btn-black add-answer" type="button" value="'._lang('Добавить ответ').'"/></td> 
                <td></td>
                <td></td>
            </tr>           
        </table>  
            
        </p>    
        
        
        <input class="btn btn-primary" type="submit" value="'._lang('Добавить').'" name="postNewsubmit"/>
    </form>
    
');

$PData->content('
    <!-- Filter: Blog_post_Add_Page_3 -->
        '._run_filter('Blog_post_Add_Page_3').'
    ');