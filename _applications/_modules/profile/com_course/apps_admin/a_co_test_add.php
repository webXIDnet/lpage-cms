<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Додавання тесту
 * 
***/

$sqlTPL->getCategories('course*');

global $list;

$postType='course';

if(!empty($_GET['type']))
    $postType = $_GET['type'];


$PData->content( 'Редактирование страницы', 'title' );


$PData->content('
    <form class="list" method="POST" enctype="multipart/form-data" id="author_form">        
        
        <p>
            <label>'._lang('Заголовок').'</label> 
            <input type="text" class="input-xlarge" name="postAdd[title]" value="'.@$_POST['postAdd']['title'].'"/>
        </p>
        <p>
            <label>'._lang('Артикул (латинськими)').':</label>
            <input type="text" class="input-xlarge" name="postAdd[article]" value="'.@$_POST['postAdd']['article'].'"/>
        </p>
        
        <!-- Filter: Blog_post_Add_Page_1 -->
        '._run_filter('Blog_post_Add_Page_1').'
        
        <p>
            <label>'._lang('Вопрос').':</label>
            '.$admin->getWIZIWIG('postAdd[text]',@$_POST['postAdd']['text'],'elm1').'
        </p>
        <p>
            '._lang('Варианты ответов:').'
        </p>
        <p>  
        <table class="table answer-table"> 
            <tr>
                <th>#</th>
                <th>'._lang('Текст ответа').'</th>
                <th></th>
                <th></th>
            </tr>
            <tr class="answer">
                <td class="number">
                    1
                </td>         
                <td>
                    <textarea type="text" style="width: 500px; height: 50px;" class="input-xlarge" name="postAdd[q1]">'.@$_POST['postAdd']['preview'].'</textarea>
                </td> 
                <td>
                    <label><input type="checkbox"> '._lang('Вариант верный').'</label>
                </td>
                <td>
                    <input class="btn btn-black" type="button" value="'._lang('Удалить').'"/>
                </td>
            </tr>            
        </table>
        <script type="text/javascript" src="/js/test.js"></script>
        </p>
        <p>
            <input class="btn btn-black add-answer" type="button" value="'._lang('Добавить ответ').'"/>
        </p>
        <!-- Filter: Blog_post_Add_Page_3 -->
        '._run_filter('Blog_post_Add_Page_3').'
        
        <input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="postNewsubmit"/>
    </form>
');