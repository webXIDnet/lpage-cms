<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Додавання теми
 * 
***/

$sqlTPL->getCategories('course*');

global $list;

$postType='course';

if(!empty($_GET['type']))
	$postType = $_GET['type'];

if(isset($_POST['postNewsubmit'])){

	if(!empty($_POST['postAdd']['title'])){
	
		$mas=$_POST['postAdd'];
		
		if(empty($mas['article'])){
	
			$url=$mas['article'] = _textToURL($mas['title']);
			$i=1;
	
			$cval = $sql->query("
				SELECT p_id 
				FROM _posts 
				WHERE p_article='"._protect($url)."' AND p_cat_id>0
			",'value');
			
			while($cval){
	
				$mas['article']=$url.'-'.$i;
				$i++;
				
				$cval = $sql->query("
					SELECT p_id 
					FROM _posts 
					WHERE p_article='"._protect($mas['article'])."' AND p_cat_id>0",'value');
			}
		}

		$mas['pablic_date']=time();
		$mas = _run_filter('Blog_post_insert_array',$mas);
		$id = $sql->insert('_posts',$mas,'p_',FALSE,TRUE);
		
		_run_action('Blog_post_insert_include_id',$_REQUEST + array('id'=>$id));
		
		if( writeImg($postType.$id,'images') ){
			
			$newImgURL = _getRandStr();
		
			while(_checkURL(HOMEPAGE.'media/files/miniatures/'.$newImgURL.'.png')){
				
				$newImgURL = _getRandStr();
				
			}
			
			$array=array(330=>'media/files/miniatures/'.$newImgURL.'.png');			

			$sql->update('_posts',"p_id='"._protect($id)."'",array('p_imgs'=>json_encode($array)));
			
			$PData->content('Обрезать изображение','title');
			
			$PData->content(
			
				cropImgForm(HOMEPAGE.'media/files/miniatures/big/post'.$id.'.png',
					$array=array(
						330=>CODE_FOLDER.'../media/files/miniatures/'.$newImgURL.'.png'
					),
					getURL('admin','post-edit','id='.$id)
				)
			);
			
			$admin->getAdminTheme('small');
		}
		$PData->content(_lang('Запись сохранена').'. <a href="'.getURL('admin','post-edit','id='.$id).'">'._lang('Редактировать страницу').'</a>','message',TRUE);
		return;
	}else{
		$PData->content('Нужно указать Заголовок','message');
	
	}
}elseif(isset($_POST['small'])){
	
	$imgs = json_decode( urldecode($_POST['imgs']), TRUE );
				
	if(is_array($imgs)){
		
		foreach($imgs AS $key=>$val){
		
			$PData->content('Обрезать изображение','title');
				
			$PData->content(
			
				cropImgForm(HOMEPAGE.'media/files/miniatures/big/post'.$_GET['post'].'.png',
					$array=array(
						$key=>CODE_FOLDER.'../'.$val
					)
				)
			);
			$admin->getAdminTheme('small');
			
		}
	}
}elseif(isset($_POST['revomeImages'])){
	
	unlink(CODE_FOLDER.'../media/files/miniatures/big/post'.$_GET['post'].'.png');
	
	$imgs = json_decode( urldecode($_POST['imgs']), TRUE );
				
	if(is_array($imgs)){
		
		foreach($imgs AS $key=>$val){
		
			unlink(CODE_FOLDER.'../'.$val);
			
		}
	}
}

$PData->content( 'Редактирование страницы', 'title' );


$PData->content('
	<form class="list" method="POST" enctype="multipart/form-data" id="author_form">
		
		<div style="float: right;width:322px;">
			<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="postNewsubmit" style="display: none;"/>
			<p>
				<label>'._lang('Категория').':</label>
				<select name="postAdd[cat_id]">
					<option value="'.$_GET['moduleid'].'">'.$_GET['course'].'</option>
				</select>
			</p>
			<p>
				<label>'._lang('Миниатюра').':</label>
				<input type="file" name="images[]" accept="image/jpeg,image/png"/>
			</p>
			<!-- Filter: Blog_post_Add_Page_2 -->
			'._run_filter('Blog_post_Add_Page_2').'
		</div>
		<p>
			<label>'._lang('Заголовок').'</label> 
			<input type="text" class="input-xlarge" name="postAdd[title]" value="'.@$_POST['postAdd']['title'].'"/>
		</p>
		<p>
			<label>'._lang('Артикул (латинськими)').':</label>
			<input type="text" class="input-xlarge" name="postAdd[article]" value="'.@$_POST['postAdd']['article'].'"/>
		</p>
		<p>
			<label>'._lang('Превью текст').':</label>
			<textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="postAdd[preview]">'.@$_POST['postAdd']['preview'].'</textarea>
		</p>
		<!-- Filter: Blog_post_Add_Page_1 -->
		'._run_filter('Blog_post_Add_Page_1').'
		
		<p>
			<label>'._lang('Текст').':</label>
			'.$admin->getWIZIWIG('postAdd[text]',@$_POST['postAdd']['text'],'elm1').'
		</p>
		<!-- Filter: Blog_post_Add_Page_3 -->
		'._run_filter('Blog_post_Add_Page_3').'
		
		<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="postNewsubmit"/>
	</form>
');