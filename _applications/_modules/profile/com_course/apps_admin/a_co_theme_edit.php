<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Додавання постів
 * 
***/

global $sqlTPL,$contTPL;
$sqlTPL->getCategories('course%','LIKE');

global $list;

$postType='post';

if(!empty($_GET['type']))
	$postType = $_GET['type'];

if(isset($_POST['postNewsubmit'])){

	if(!empty($_POST['postEdit']['title'])){
	
		$mas=$_POST['postEdit'];
		
		if(empty($mas['article'])){
	
			$url=$mas['article'] = _textToURL($mas['title']);
			$i=1;
	
			$cval = $sql->query("SELECT `p_id` FROM `_posts` WHERE p_article='"._protect($url)."' AND p_cat_id>0 AND p_id!='"._protect($_GET['id'])."'",'value');
			
			while($cval){
	
				$mas['article']=$url.'-'.$i;
				$i++;
				
				$cval = $sql->query("SELECT `p_id` FROM `_posts` WHERE p_article='"._protect($mas['article'])."' AND p_cat_id>0 AND p_id!='"._protect($_GET['id'])."'",'value');
	
			}
	
		}
		
		$sql->update('_posts',"p_id='"._protect($_GET['id'])."'",$mas,'p_');
		
		_run_action('Blog_post_update_include_id',$_REQUEST);
		
		//$PData->content('Запись сохранена. <a target="_blank" href="'.getURL('page',$mas['article'],'category='.$list->category_index[$mas['cat_id']]['c_title']).'">'._lang('Посмотреть страницу').'</a>','message',TRUE);
		
		unset($mas);
		unset($_POST['postEdit']);
			
	}else{
	
		$PData->content('Нужно указать Заголовок','message');
	
	}
}

$cval = $sql->query("
	SELECT *
	FROM _posts 
	WHERE p_id='"._protect(@$_GET['id'])."'
",'value');

if($cval){
	
	$PData->content( 'Редактирование страницы', 'title' );
	$PData->content('
		<form class="list" method="POST" enctype="multipart/form-data">
			<input style="display:none;" type="submit" name="postNewsubmit"/>
			<div style="float: right;width:322px;">
				<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="postNewsubmit" style="display: none;"/>	
				<p>
					<label>'._lang('Шоткод презентации').':</label>asdasd
					<input type="text" name="postEdit[imgs]" value="'.$cval['p_imgs'].'"/>
		
				</p>
			</div>
			<div>
				'.
				_run_filter('Course_edit_theme_before_title')
				.'	
				<p>
					<label>'._lang('Заголовок').'</label> 
					<input type="text" class="input-xlarge" name="postEdit[title]" value="'.$cval['p_title'].'"/>
				</p>
				<p>
					<label>'._lang('Артикул (латинськими)').':</label>
					<input type="text" class="input-xlarge" name="postEdit[article]" value="'.$cval['p_article'].'"/>
				</p>
				
				<p>
					<label>'._lang('Код видео').':</label>
					<textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="postEdit[preview]">'.$cval['p_preview'].'</textarea>
				</p>
				
				<!-- Filter: Blog_post_Edit_Page_1 -->
				'._run_filter('Blog_post_Edit_Page_1','',$cval).'
				
			</div>
			<div class="clear"></div>
			<p>
				<label>'._lang('Текст').'</label>
				'.$admin->getWIZIWIG('postEdit[text]',$cval['p_text'],'elm1').
				'
			</p>
			<!-- Filter: Blog_post_Edit_Page_3 -->
			'._run_filter('Blog_post_Edit_Page_3','',$cval).'
			
			<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="postNewsubmit"/>
		</form>
	');
	
}else{
	
	$PData->redirect('','404');
	
}