<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Додавання модулів
 *
 ***/
if(isset($_POST['editCourse'])) {
  if ( !empty( $_POST['course'] ) && is_array( $_POST['course'] ) ) {
    $sql->update( '_courses', "co_id='" . _protect( $_GET['id'] ) . "'", $_POST['course'], 'co_' );
  }
}

$course = $sql->query("SELECT *	FROM _courses WHERE co_id='"._protect(@$_GET['id'])."'");

/** Формування списку модулів **/

$modules_list = $sql->query("SELECT *	FROM _categories WHERE c_type='course"._protect(@$_GET['id'])."'");


$PData->content(_lang('Редактирование Курса'),'title');

$PData->content('
	<form class="list" method="POST"  enctype="multipart/form-data">
		<table>
		    <tbody>
          <tr>
            <td>
              <input placeholder="'._lang('Заголовок').'" type="text" name="course[title]" value="'.$course[0]['co_title'].'">
              <input placeholder="'._lang('Цена').'" type="text" name="course[price]" value="'.$course[0]['co_price'].'"><br/>
              <textarea placeholder="'._lang('Описание').'" type="textarea" name="course[desc]">'.$course[0]['co_desc'].'</textarea>
              <input placeholder="'._lang('Изображение').'" type="file" name="course[img]" value=""/>
              <br/>
              <input type="submit" class="btn" name="editCourse" value="'._lang('Сохранить').'"/>
            </td>
          </tr>
			  </tbody>
		</table>
	</form>
	<hr/>
');
$PData->content('
<table class="table">
		      <thead>
		        <tr>
							<th class="numer">'._lang('Номер Курса').'</th>
							<th>'._lang('Название').'</th>
		        </tr>
		      </thead>
		      <tbody>
');
if(isset($modules_list)){
  $i = 1;
  foreach ( $modules_list as $module ) {
    $PData->content('
			<tr>
				<td>'.$i++.'</td>
				<td><a title="'._lang('Список подулей').'" href="'.getURL('admin','module','module='.$module['c_id']).'">'.$module['c_title'].'</a></td>
			</tr>
');
  }
}
$PData->content('
		      </tbody>
		    </table>
');