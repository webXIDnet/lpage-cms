<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Редагування курсу, Додавання модуля, Виведення списку модулів, які відносятся до курсу
 *
 ***/
/* Обробка данних з форми - Редагування курсу */
if(isset($_POST['editCourse'])) {
	if ( !empty( $_POST['course'] ) && is_array( $_POST['course'] ) ) {
		$course_arr = $_POST['course'];
		if ( !empty( $_FILES['course_img'] ) ) {
			$userpic_dir  = 'media/files/miniatures/';
			$userpic_path = $userpic_dir . basename( $_FILES['course_img']['name'] );
			if ( move_uploaded_file( $_FILES['course_img']['tmp_name'], $userpic_path ) ) {
				$course_img = array( 'img' => $userpic_path );
				$course_arr = $course_arr + $course_img;
			}
		}
		$sql->update( '_courses', "co_id='" . _protect( $_GET['id'] ) . "'", $course_arr, 'co_' );
	}
}
/* Витягування данних про курсу */
$course = $sql->query("SELECT *	FROM _courses WHERE co_id='"._protect(@$_GET['id'])."'");
$category_type=$course[0]['co_title'];


/* Обробка данних з форми - Додавання нового модуля */
if(isset($_POST['newCat']) && is_array($_POST['newCat'])){

	if(empty($_POST['newCat']['url'])){
		$url = $_POST['newCat']['url'] = _textToURL($_POST['newCat']['title']);
	}else{
		$url = $_POST['newCat']['url'] = _textToURL($_POST['newCat']['url']);
	}

	$cval = $sql->query("
		SELECT c_id
		FROM _categories
		WHERE c_url='"._protect($url)."' AND c_type='"._protect($category_type)."'
	",'value');

	while($cval){

		$_POST['newCat']['url']=$url.'-'.$i;
		$i++;

		$cval = $sql->query("
			SELECT c_id
			FROM _categories
			WHERE c_url='"._protect($_POST['newCat']['url'])."' AND c_type='"._protect($category_type)."'
		",'value');
	}
	$_POST['newCat']['title'] = trim($_POST['newCat']['title']);
	$array = $_POST['newCat'];
	$array['type']=$category_type;
	$sql->insert('_categories',$array,'c_');

	$PData->content('Запись Сохранена','message',TRUE);
}

/** Редагування вказаного курсу **/
$PData->content(_lang('Редактирование Курса'),'title');
$PData->content('
	<form class="list" method="POST"  enctype="multipart/form-data">
		<table>
		    <tbody>
          <tr>
            <td>
              <input placeholder="'._lang('Заголовок').'" type="text" name="course[title]" value="'.$course[0]['co_title'].'">
              <input placeholder="'._lang('Цена').'" type="text" name="course[price]" value="'.$course[0]['co_price'].'"><br/>
              <textarea placeholder="'._lang('Описание').'" type="textarea" name="course[desc]">'.$course[0]['co_desc'].'</textarea>
              <input placeholder="'._lang('Изображение').'" type="file" name="course_img" value=""/>
              <br/>
              <input type="submit" class="btn" name="editCourse" value="'._lang('Сохранить').'"/>
            </td>
          </tr>
			  </tbody>
		</table>
	</form>
	<hr/>
');

/** Створення нового модуля **/
$PData->content('
	<form class="list" method="POST">
		<table>
			<tr>
				<td>
					<label><b>+ '._lang('Добавить модуль').'</b>:</label>
					<input placeholder="'._lang('Заголовок').'" type="text" name="newCat[title]" value=""/>
					<input placeholder="'._lang('Текстовая ссылка').'" type="text" name="newCat[url]" value=""/>
					<!-- Filter: app_admin_newCat_addField -->
					'._run_filter('app_admin_newCat_addField', '', $category_type).'
					<br/>
					<input type="submit" class="btn" name="newModule" value="'._lang('Добавить').'"/>
				</td>
			</tr>
		</table>
	</form>
	<hr/>
');
/** Формування списку модулів **/
$modules_list = $sql->query("SELECT *	FROM _categories WHERE c_type='"._protect($category_type)."'");
$PData->content('
<table class="table">
		      <thead>
		        <tr>
							<th class="numer">'._lang('Номер').'</th>
							<th>'._lang('Название Модуля').'</th>
		        </tr>
		      </thead>
		      <tbody>
');
if(isset($modules_list)){
  $i = 1;
  foreach ( $modules_list as $module ) {
    $PData->content('
			<tr>
				<td>'.$i++.'</td>
				<td><a title="'._lang('Список Тем').'" href="'.getURL('admin','module','module='.$module['c_id'].'&course='.$course[0]['co_title']).'">'.$module['c_title'].'</a></td>
			</tr>
');
  }
}
$PData->content('
		      </tbody>
		    </table>
');