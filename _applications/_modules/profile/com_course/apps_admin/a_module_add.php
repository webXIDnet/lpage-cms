<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Додавання категорій товарів
 *
 ***/


$category_type='posts';

if(isset($_GET['category_type']) && !empty($_GET['category_type']))
	$category_type=$_GET['category_type'];

$_GET['category_type'] = $category_type;

/** Опрацювання Пост запитів **/

if(isset($_POST['delCat']) && is_array($_POST['delCat'])){

	$where2 = $where1 = $where = '';

	foreach($_POST['delCat'] AS $key=>$val){

		if(!empty($where)){

			$where .= ' OR ';
			$where1 .= ' OR ';
			$where2 .= ' OR ';

		}

		$where .= "c_id='"._protect($key)."'";
		$where1 .= "p_cat_id='"._protect($key)."'";
		$where2 .= "c_parent_id='"._protect($key)."'";

	}

	if(!empty($where)){

		$sql->update( '_posts', $where1, array('p_cat_id'=>0) );
		$sql->update( '_categories', $where2, array('c_parent_id'=>0) );
		$sql->query("DELETE FROM `_categories` WHERE {$where}",'query');

		$PData->content('Запись удалена','message',TRUE);

	}else{

		$PData->content('Произошел сбой алгоритма. Запись небыла удалена. Обратитесь пожалуйста к разработчикам.','message');

	}

}elseif(isset($_POST['newCat']) && is_array($_POST['newCat'])){


	if(empty($_POST['newCat']['url'])){
		$url = $_POST['newCat']['url'] = _textToURL($_POST['newCat']['title']);
	}else{
		$url = $_POST['newCat']['url'] = _textToURL($_POST['newCat']['url']);
	}

	$cval = $sql->query("
		SELECT c_id
		FROM _categories
		WHERE c_url='"._protect($url)."' AND c_type='"._protect($category_type)."'
	",'value');

	while($cval){

		$_POST['newCat']['url']=$url.'-'.$i;
		$i++;

		$cval = $sql->query("
			SELECT c_id
			FROM _categories
			WHERE c_url='"._protect($_POST['newCat']['url'])."' AND c_type='"._protect($category_type)."'
		",'value');

	}

	$_POST['newCat']['title'] = trim($_POST['newCat']['title']);

	$array = $_POST['newCat'];
	$array['type']=$category_type;

	$sql->insert('_categories',$array,'c_');

	$PData->content('Запись Сохранена','message',TRUE);

}


/** Формування списку батьківських категорій **/

$sqlTPL->getCategories(@$category_type);


/** Пагінація **/

$parent_select=$text='';

$where = "WHERE c_parent_id='0' AND c_type='"._protect(@$category_type)."'";

if(!empty($_GET['filter'])){
	$where .= " AND c_title LIKE '%"._protect($_GET['filter'])."%'";
}

$where = _run_filter('Blog_category_QUERY_WHERE',$where,@$category_type);

$nav = _pagination(
		'_categories',
		'c_id',
		$where,
		getURL('admin','categories',http_build_query($_GET))
);


/** Формування основного списку категорій **/
$result = $sql->query("
	SELECT *
	FROM _categories
	{$where}
	ORDER BY c_title ASC
	".@$nav['limit']."
");


$text = $contTPL->catHierarchTree($result,0,'','','table');

global $list;

/** Отримання ієрархії категорій для селекта **/

$parent_select = $contTPL->catHierarchTree(@$list->category[$category_type]);


/** Формування основного HTML коду **/

$PData->content('Категории ('.$category_type.')','title');

$PData->content('

	<form class="list" method="POST">
		<table>
			<tr>
				<td>
					<label><b>+ '._lang('Добавить категорию').'</b>:</label>
					<input placeholder="'._lang('Заголовок').'" type="text" name="newCat[title]" value=""/>
					<input placeholder="'._lang('Текстовая ссылка').'" type="text" name="newCat[url]" value=""/>
					<select name="newCat[parent_id]">
						<option value="">- '._lang('Родительская категория').' -</option>
						'.$parent_select.'
					</select>
					<!-- Filter: app_admin_newCat_addField -->
					'._run_filter('app_admin_newCat_addField', '', $category_type).'
					<br/>
					<input type="submit" class="btn" value="'._lang('Добавить').'"/>
				</td>
			</tr>
		</table>
	</form>
	<hr/>

');

$PData->content('
		<form class="list">
			<table>
				<tr>
					<td>
						<label><b>'._lang('Поиск').'</b>:</label>
						<input type="hidden" name="admin" value="'.@$_GET['admin'].'"/>
						<input placeholder="'._lang('Заголовок').'" type="text" name="filter" value="'.@$_GET['filter'].'"/><br/>
						<input type="submit" class="btn" value="'._lang('Искать').'"/>
					</td>
					<!-- Filter: app_admin_newCat_addField -->
					'._run_filter('app_admin_filter_of_Cat_addField', '', $category_type).'
				</tr>
			</table>
		</form>
		<hr/>
		<br/>
		<div class="well">
		<form class="list" method="POST">
		    <table class="table">
		      <thead>
		        <tr>
		        	<th class="numer">'._lang('Выбрать').'</th>
					<th>'._lang('Заголовок').'</th>
					<th>'._lang('Текстовая ссылка').'</th>
					<th>'._lang('Доп. данные').'</th>
		        </tr>
		      </thead>
		      <tbody>
		        	'.$text.'
		      </tbody>
		    </table>
		    <input class="btn btn-primary" type="submit" value="'._lang('Удалить').'" name="postssubmit">
		</form>
	</div>

'.$nav['html']);
