<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Редагування модуля тестування, Додавання нової теми, Виведення списку тем
 *
***/

if(isset($_POST['editCat']) && is_array($_POST['editCat'])){

	if(empty($_POST['editCat']['url'])){
		$url = $_POST['editCat']['url'] = _textToURL($_POST['editCat']['title']);
	}else{
		$url = $_POST['editCat']['url'] = _textToURL($_POST['editCat']['url']);
	}

	if ( !isset( $_POST['category_type'] ) ) $_POST['category_type'] = $_GET['course'];

	$cval = $sql->query("
		SELECT c_id
		FROM _categories
		WHERE c_url='"._protect($url)."' AND c_type='"._protect(@$_POST['category_type'])."' AND c_id!='"._protect($_GET['module'])."'
	",'value');

	while($cval){
		$_POST['newCat']['url']=$url.'-'.$i;
		$i++;
		$cval = $sql->query("
			SELECT c_id
			FROM _categories
			WHERE c_url='"._protect($_POST['newCat']['url'])."' AND c_type='"._protect(@$_POST['category_type'])."' AND c_id!='"._protect($_GET['module'])."'
		",'value');

	}

	$array = $_POST['editCat'];
	$array['title'] = trim($array['title']);
	$array['parent_id'] = (int)$array['parent_id'];

	$sql->update('_categories',"c_id='"._protect($_GET['module'])."'",$array,'c_');

}
/** Формування списку модулів **/
$sqlTPL->getCategories($_GET['course']);

global $list;

if(!isset($list->category_index[@$_GET['module']])){
	$PData->content('Редактирование модуля','title');
	$PData->content('Не верная ссылка на страницу редактирования категории');
	return;
}

$cval = $list->category_index[$_GET['module']];

if(is_array($list->category[$cval['c_type']])){
	$parent_select = $contTPL->catHierarchTree($list->category[$cval['c_type']],0,$cval['c_parent_id'],$cval['c_id']);
}

$PData->content(_lang('Редактирование модуля').' (Курс - '.$cval['c_type'].')','title');

$PData->content('
	<form class="list" method="POST">
		<table>
			<tr>
				<td>
					<label><b>'._lang('Заголовок').'</b>:</label>
					<input placeholder="'._lang('Заголовок').'" type="text" name="editCat[title]" value="'.$cval['c_title'].'"/>

					<label><b>'._lang('Текстовая ссылка').'</b>:</label>
					<input placeholder="'._lang('Текстовая ссылка').'" type="text" name="editCat[url]" value="'.$cval['c_url'].'"/>
                    <!-- Filter: app_admin_editCat_addField -->
					'._run_filter('app_admin_editCat_addField', '', $cval).'
					<label><b>'._lang('Тип Модуля').'</b>: '._lang($cval['c_type']).'</label>
					<input type="hidden" name="category_type" value="'.$cval['c_type'].'"/>
					<br/><br/>
					<input type="submit" class="btn" value="'._lang('Сохранить').'"/>
					<a href="'.getURL('admin','theme-add','moduleid='.$cval['c_id'].'&course='.$cval['c_type']).'" class="btn">'._lang('Добавить Тему').'</a>
				</td>
			</tr>
		</table>
	</form>
	<hr/>
');

/** Пагінація **/
$text = $where = '';

/** Формування основного списку **/
$i=0;
if($cval['c_type']){
	switch($cval['c_type']){

		case $cval['c_type']:
			$where = "
				INNER JOIN _categories
				ON p_cat_id=c_id
				WHERE p_cat_id='"._protect($cval['c_id'])."' AND c_type='"._protect($cval['c_type'])."' ".$where;
			break;
		default:
			$where = _run_filter('Blog_posts_MAIN_QUERY_WHERE','',$cval['c_type']);
	}
}

if(!empty($_GET['filter'])){
	if(!empty($where))$where.=' AND ';
	$where .= " p_title LIKE '%"._protect($_GET['filter'])."%'";
}

if(!empty($_GET['filter_cat'])){
	if(!empty($where))$where.=' AND ';
	$where .= " p_cat_id = '"._protect($_GET['filter_cat'])."'";
}

$where = _run_filter('Blog_posts_QUERY_WHERE',$where,$cval['c_type']);

$nav = _pagination('_posts','p_id',$where);

$themes = $sqlTPL->getPosts('10_posts', $where.' '.$nav['limit'] );

if(is_array($themes)){
	foreach($themes AS $theme){
		$i++;
		$text.='<tr >
			<td>
				<input type="checkbox" name="del_post['.$theme['p_id'].']"/> <a href="'.getURL('post',$theme['p_article']).'" target="_blank">'.$theme['p_id'].'</a>
			</td>
			<td>
				<a title="'._lang('Редактировать').'" href="'.getURL('admin','theme-edit','id='.$theme['p_id']).'">
					'.$theme['p_title'].'</a>
			</td>
			<td>
				'.$theme['c_title'].'
			</td>
			<td class="greyText">
				'._lang('Дата изменения').': <span class="blackText">'.date(_lang('Y-m-d'),$theme['p_pablic_date']).'</span><br/>
				<!-- Filter: Blog_posts_table_additional_data -->
				'._run_filter('Blog_posts_table_additional_data').'
				<!-- // Filter: Blog_posts_table_additional_data -->
			</td>
		</tr>';
	}
	$text =
			'<form class="list" method="POST">
		    <table class="table">
		      <thead>
		        <tr >
		        	<th class="numer">'._lang('Выбрать').'</th>
					<th>'._lang('Заголовок').'</th>
					<th>'._lang('Модуль').'</th>
					<th>'._lang('Доп. данные').'</th>
		        </tr>
		      </thead>
		      <tbody>
		        	'.$text.'
		      </tbody>
		    </table>
		    <input class="btn btn-primary" type="submit" value="'._lang('Удалить').'" name="postssubmit">
		</form>';

}else{
	$text = _lang('Не найдено! =\ ');
}

$PData->content('
	<div class="well">
		'.$text.'
	</div>
'.$nav['html']);

