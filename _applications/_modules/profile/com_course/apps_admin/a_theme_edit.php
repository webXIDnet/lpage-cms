<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Додавання постів
 * 
***/

$sqlTPL->getCategories('posts');

global $list;

$postType='post';

if(!empty($_GET['type']))
	$postType = $_GET['type'];

if(isset($_POST['postNewsubmit'])){

	if(!empty($_POST['postEdit']['title'])){
	
		$mas=$_POST['postEdit'];
		
		if(empty($mas['article'])){
	
			$url=$mas['article'] = _textToURL($mas['title']);
			$i=1;
	
			$cval = $sql->query("SELECT `p_id` FROM `_posts` WHERE p_article='"._protect($url)."' AND p_cat_id>0 AND p_id!='"._protect($_GET['id'])."'",'value');
			
			while($cval){
	
				$mas['article']=$url.'-'.$i;
				$i++;
				
				$cval = $sql->query("SELECT `p_id` FROM `_posts` WHERE p_article='"._protect($mas['article'])."' AND p_cat_id>0 AND p_id!='"._protect($_GET['id'])."'",'value');
	
			}
	
		}
	
		//$mas['pablic_date']=time();
		
		if( writeImg($postType.$_GET['id'],'images') ){
			
			$newImgURL = _getRandStr();
		
			while(_checkURL(HOMEPAGE.'media/files/miniatures/'.$newImgURL.'.png')){
				
				$newImgURL = _getRandStr();
				
			}
			
			$array=array(330=>'media/files/miniatures/'.$newImgURL.'.png');			
			$mas['imgs']=json_encode($array);
			
			$PData->content('Обрезать изображение','title');
			
			$PData->content(
			
				cropImgForm(HOMEPAGE.'media/files/miniatures/big/post'.$_GET['id'].'.png',
					$array=array(
						330=>CODE_FOLDER.'../media/files/miniatures/'.$newImgURL.'.png'
					)
				)
			);
			
			$mas = _run_filter('Blog_post_update_array',$mas);
			
			$sql->update('_posts',"p_id='"._protect($_GET['id'])."'",$mas,'p_');
			
			$admin->getAdminTheme('small');
		}
		
		$mas = _run_filter('Blog_post_update_array',$mas);
		
		$sql->update('_posts',"p_id='"._protect($_GET['id'])."'",$mas,'p_');
		
		_run_action('Blog_post_update_include_id',$_REQUEST);
		
		$PData->content('Запись сохранена. <a target="_blank" href="'.getURL('page',$mas['article'],'category='.$list->category_index[$mas['cat_id']]['c_title']).'">'._lang('Посмотреть страницу').'</a>','message',TRUE);
		
		unset($mas);
		unset($_POST['postEdit']);
			
	}else{
	
		$PData->content('Нужно указать Заголовок','message');
	
	}
}elseif(isset($_POST['small'])){
	
	$imgs = json_decode( urldecode($_POST['imgs']), TRUE );
	
	if(_checkURL(HOMEPAGE.'media/files/miniatures/big/post'.$_GET['id'].'.png')){
		
		if(!is_array($imgs)){
			
			$newImgURL = _getRandStr();
		
			while(_checkURL(HOMEPAGE.'media/files/miniatures/'.$newImgURL.'.png')){
				
				$newImgURL = _getRandStr();
				
			}
			
			$imgs=array(330=>'media/files/miniatures/'.$newImgURL.'.png');
						
			$sql->update('_posts',"p_id='"._protect($_GET['id'])."'",array('p_imgs'=>json_encode($imgs)));
			
		}
			
		foreach($imgs AS $key=>$val){
		
			$PData->content('Обрезать изображение','title');
				
			$PData->content(
			
				cropImgForm(HOMEPAGE.'media/files/miniatures/big/post'.$_GET['id'].'.png',
					$array=array(
						$key=>CODE_FOLDER.'../'.$val
					)
				)
			);
			$admin->getAdminTheme('small');
			
		}
		
	}
}elseif(isset($_POST['revomeImages'])){
	
	unlink(CODE_FOLDER.'../media/files/miniatures/big/post'.$_GET['id'].'.png');
	
	$imgs = json_decode( urldecode($_POST['imgs']), TRUE );
				
	if(is_array($imgs)){
		
		foreach($imgs AS $key=>$val){
		
			unlink(CODE_FOLDER.'../'.$val);
			
		}
	}
}

$cval = $sql->query("
	SELECT *
	FROM _posts 
	WHERE p_id='"._protect(@$_GET['id'])."'
",'value');

if($cval){
	
	$PData->content( 'Редактирование страницы', 'title' );
	$PData->content('
		<form class="list" method="POST" enctype="multipart/form-data">
			<input style="display:none;" type="submit" name="postNewsubmit"/>
			<div style="float: right;width:322px;">
				<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="postNewsubmit" style="display: none;"/>
				<p>
					<label>'._lang('Категория').':</label>
					<select name="postEdit[cat_id]">
						'.$contTPL->catHierarchTree(@$list->category['posts'],0,$cval['p_cat_id']).'
					</select>
				</p>
				
				<p>
					<label>'._lang('Миниатюра').':</label>
					<input type="file" name="images[]" accept="image/jpeg,image/png"/>
		','main',FALSE,FALSE);
				
		if(_checkURL(HOMEPAGE.'media/files/miniatures/big/post'.$cval['p_id'].'.png',FALSE)){
			
			if(!empty($cval['p_imgs'])){
				
				$PData->content('<input type="hidden" name="imgs" value="'.urlencode($cval['p_imgs']).'" />','main',FALSE,FALSE);
				
			}
			
			$img = json_decode($cval['p_imgs'],TRUE);
			
			$PData->content('
					
					<a target="_blank" href="'.HOMEPAGE.'media/files/miniatures/big/post'.$cval['p_id'].'.png">
						<img src="'.HOMEPAGE.$img[330].'"/></a><br/>
					<input type="submit" name="small" value="'._lang('Изменить миниатюру').'" /> 
					<input style="background-color:rgb(204, 0, 0);" type="submit" name="revomeImages" value="'._lang('Удалить').'" />
			','main',FALSE,FALSE);
			
		}
		
		$PData->content('
				</p>
				<!-- Filter: Blog_post_Edit_Page_2 -->
				'._run_filter('Blog_post_Edit_Page_2','',$cval).'
			</div>
			<div>
				<p>
					<label>'._lang('Заголовок').'</label> 
					<input type="text" class="input-xlarge" name="postEdit[title]" value="'.$cval['p_title'].'"/>
				</p>
				<p>
					<label>'._lang('Артикул (латинськими)').':</label>
					<input type="text" class="input-xlarge" name="postEdit[article]" value="'.$cval['p_article'].'"/>
				</p>
				
				<p>
					<label>'._lang('Превью текст').':</label>
					<textarea type="text" style="width: 300px; height: 140px;" class="input-xlarge" name="postEdit[preview]">'.$cval['p_preview'].'</textarea>
				</p>
				
				<!-- Filter: Blog_post_Edit_Page_1 -->
				'._run_filter('Blog_post_Edit_Page_1','',$cval).'
				
			</div>
			<div class="clear"></div>
			<p>
				<label>'._lang('Текст').'</label>
				'.$admin->getWIZIWIG('postEdit[text]',$cval['p_text'],'elm1').
				'
			</p>
			<!-- Filter: Blog_post_Edit_Page_3 -->
			'._run_filter('Blog_post_Edit_Page_3','',$cval).'
			
			<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="postNewsubmit"/>
		</form>
	');
	
}else{
	
	$PData->redirect('','404');
	
}