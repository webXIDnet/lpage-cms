<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Перша сторінка приватного кабінету
 *
 ***/
if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
  $profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'" );
  $profile_user = array_filter($profile_user);
  $modules = $sql->query( "
				SELECT *
				FROM _categories
				WHERE c_type = 'course1'" );
  if ( empty( $profile_user ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  }
} else {
  $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
  $PData->redirect( getURL( 'profile', 'login' ) );
}

$PData->content('Модули','title');

$PData->content(
	'<ul class="listmenu">
    	'.pf_sidebar().'
    </ul>'
,'sidebar');
   
if(isset($modules) && is_array($modules)) {
  	$i=1;
	foreach($modules as  $module) {
		$PData->content( '
		    <div class="units-row">
		        <div class="unit-10">' . $i++ . '</div>
		        <div class="unit-90"> <a href="'.getURL('profile', 'themes', 'module='.$module['c_id']).'">' . $module['c_title'] . '</a></div>
		    </div>
		');
	}
}