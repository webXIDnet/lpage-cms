<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Перша сторінка приватного кабінету
 *
 ***/
// print_r($_SESSION['profile_id']);
if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
  $profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'" );
  $profile_user = array_filter($profile_user);
  $theme = $sql->query( "
				SELECT * FROM _posts WHERE p_id = '" . _protect($_GET['theme']) . "'",'value' );
  if ( empty( $profile_user ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  }
} else {
  $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
  $PData->redirect( getURL( 'profile', 'login' ) );
}

$PData->content($theme['p_title'],'title');

$PData->content(
	'<ul class="listmenu">
    	'.pf_sidebar().'
    </ul>'
,'sidebar');

$_GET['module'] = $theme['p_cat_id'];

$PData->content(_run_filter('course_before_topic','',$theme).'<div class="topic-content">');

if (isset($theme)){
	$PData->content($theme['p_preview'],'video');
	$PData->content($theme['p_imgs'],'slider');
    $PData->content($theme['p_text']);
    
    $next_topis = $sql->query( "
		SELECT p_id,p_title 
		FROM _posts 
		WHERE p_id > '" . _protect($_GET['theme']) . "' AND p_cat_id = '"._protect($theme['p_cat_id'])."' 
		LIMIT 1",
		'value');
}

$PData->content(_run_filter('course_after_topic','',$theme).'<div style="float:right;"><a class="button blue" href="http://dba.mim.kiev.ua/_media/files/Ichak-Kalderon-Adizes_Idealnyy-rukovoditel-Pochemu-im-nelzya-stat-i-chto-iz-etogo-sleduet.pdf" target="_blank">
            		Скачать дополнительные материалы
            	</a></div>');

if(isset($next_topis)){
	if(is_array($next_topis)){
		$PData->content('<a class="next-topis" href="'.getURL('profile','theme','theme='.$next_topis['p_id']).'"><b>'._lang('Следующая тема >>').'</b></a>');
	}else{
		$PData->content('<a class="next-topis" href="'.getURL('profile','test','module='.$theme['p_cat_id']).'"><b>'._lang('Пройти модульное тестирование >>').'</b></a>');
	}
}
$PData->connectTPL('profile/tpl_topic');