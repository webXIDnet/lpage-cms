<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Перша сторінка приватного кабінету
 *
 ***/
// print_r($_SESSION['profile_id']);
global $profile;
if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
  $profile_user = $sql->query( "
                SELECT *
                FROM _profile
                WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'" );
  $profile_user = array_filter($profile_user);

  $test = array();
  $parent = array();
  $parent_type = '';
  if (isset($_GET['theme']))
  {
        $parent_type = 'theme';
        $test = $sql->query( "SELECT * FROM _courses_tests WHERE ct_parent_id = '" . _protect($_GET['theme']) . "' AND ct_type='theme'",'array' );
        $parent = $sql->query( "SELECT * FROM _posts WHERE p_id = '" . _protect($_GET['theme']) . "'",'value' );
  }

    if (isset($_GET['module']))
    {
        $parent_type = 'module';
        $test = $sql->query( "SELECT * FROM _courses_tests WHERE ct_parent_id = '" . _protect($_GET['module']) . "' AND ct_type='module'",'array' );
        $parent = $sql->query( "SELECT * FROM _categories WHERE c_id = '" . _protect($_GET['module']) . "'",'value' );
    }
  if ( empty( $profile_user ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  }
} else {
  $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
  $PData->redirect( getURL( 'profile', 'login' ) );
}

if (isset($_POST['save_test'])){
	
            $total_arr = array();
            // echo '<pre>';print_r($_POST['answers']);echo '</pre>';
            // _dump($test);
            // _dump($_POST['answers']);
            // _dump('11');
            $test_result = array();

            foreach ($test as $question) {

                $question['answers_array'] = json_decode($question['ct_answers'],true);
                    // _dump($val);
                    $sel = count($_POST['answers'][$question['ct_id']]);
                    $match = 0;
                    // _dump($question['answers_array']);
                    // _dump(recarray($question['answers_array']));
                    $right = count(recarray($question['answers_array'], 'right'));
                    // _dump($val);
                    $total = count($question['answers_array']);
                    foreach ($_POST['answers'][$question['ct_id']] as $skey => $sval) {
                        
                        if (isset($question['answers_array'][$skey]['right']) && count($_POST['answers'][$question['ct_id']]) > 1)
                            $match++;
                        else if (isset($question['answers_array'][$sval]['right']))
                            $match++;
                    }
    
                    $data = array();
                    $data['date'] = date('Y-m-d h:i:s');
                    $data['pf_id'] = $profile->id;
                    $data['ct_id'] = $question['ct_id'];
    
                            /*_dump(array(
                                                'selected' => $sel,
                                                'matched' => $match,
                                                'right' => $right,
                                                'total' => $total
                                            ));*/
    
                    $data['mark'] = json_encode(array(
                                                'selected' => $sel,
                                                'matched' => $match,
                                                'right' => $right,
                                                'total' => $total
                                            ));
                     // _dump($key.'=>'.$val);
                    // _dump($_POST);
                    
                    $answ = $sql->query("SELECT * FROM _courses_answers WHERE cta_ct_id='".$question['ct_id']."'",'value');
                     // _dump($answ,true);
                    if (isset($answ) && $answ != '')
                    {
                        // _dump('upd',true);
                        $sql->update("_courses_answers",'cta_ct_id='.$question['ct_id'],$data,'cta_');
                    }
                    else
                    {
                        // _dump('ins',true);
                        $sql->insert("_courses_answers",$data,'cta_');
                    }
    
                    $total_arr []= array(
                        'selected' => $sel,
                        'matched' => $match,
                        'right' => $right,
                        'total' => $total
                    );

            }
            /*if(isset($_POST['answers']) && is_array($_POST['answers'])){
	            foreach ($_POST['answers'] as $key => $val){
	            	  
	                $question = array();
	                foreach ($test as $row) {
	                    if ($row['ct_id'] == $key)
	                        $question = $row;
	                }
                    
                    $test_result['parent_id'] = $parent['c_id'];
                    $test_result['type'] = $question['ct_type'];

	                $question['answers_array'] = json_decode($question['ct_answers'],true);
	                // _dump($val);
	                $sel = count($_POST['answers'][$key]);
	                $match = 0;
	                // _dump($question['answers_array']);
	                // _dump(recarray($question['answers_array']));
	                $right = count(recarray($question['answers_array'], 'right'));
	                // _dump($val);
	                $total = count($question['answers_array']);
	                foreach ($val as $skey => $sval) {
	                    
	                    if (isset($question['answers_array'][$skey]['right']) || isset($question['answers_array'][$sval]['right'])) $match++;
	                }
	
	                $data = array();
	                $data['date'] = date('Y-m-d h:i:s');
	                $data['pf_id'] = $profile->id;
	                $data['ct_id'] = $question['ct_id'];
	
	                        _dump(array(
	                                            'selected' => $sel,
	                                            'matched' => $match,
	                                            'right' => $right,
	                                            'total' => $total
	                                        ));
	
	                $data['mark'] = json_encode(array(
	                                            'selected' => $sel,
	                                            'matched' => $match,
	                                            'right' => $right,
	                                            'total' => $total
	                                        ));
                     // _dump($key.'=>'.$val);
                    // _dump($_POST);
                    
	                $answ = $sql->query("SELECT * FROM _courses_answers WHERE cta_ct_id='".$question['ct_id']."'",'value');
                     // _dump($answ,true);
	                if (isset($answ) && $answ != '')
                    {
                        // _dump('upd',true);
	                    $sql->update("_courses_answers",'cta_ct_id='.$question['ct_id'],$data,'cta_');
                    }
	                else
                    {
                        // _dump('ins',true);
	                    $sql->insert("_courses_answers",$data,'cta_');
                    }
	
	                $total_arr []= array(
	                    'selected' => $sel,
	                    'matched' => $match,
	                    'right' => $right,
	                    'total' => $total
	                );
	
	            }
			}*/
			
            $total = count($total_arr);
            $correct = 0;
            $inaccur = 0;
            $incorr = 0;
            
            foreach ($total_arr AS $row) {
                // $total++;

                if ($row['right'] == $row['matched'])
                    $correct++;
                //     else
                // if ($row['matched'] > 0)
                //     $inaccur++;
            }

            // $incorr = $total - ($inaccur+$correct);
            $incorr = $total - $correct;
            $data = array();
                    $data['date'] = date('Y-m-d h:i:s');
                    $data['parent_id'] = _protect($parent['c_id']);
                    $data['type'] = _protect($parent_type);
                    $data['name'] = _protect($parent['c_title']);
                    $data['profile_id'] = _protect($profile->id);
                               
                    $data['result'] = json_encode(array(
                                                'correct' => _protect($correct),
                                                'total' => _protect($total)
                                            ));

            $test_result_arr = $sql->query("SELECT * FROM _courses_results WHERE cr_parent_id='"._protect($parent['c_id'])."' AND cr_profile_id='"._protect($profile->id)."' ",'value');
                     // _dump($answ,true);
                    if (isset($test_result_arr) && $test_result_arr != '')
                    {
                        // _dump('upd',true);
                        $sql->update("_courses_results","cr_parent_id='"._protect($parent['c_id'])."' AND cr_profile_id='"._protect($profile->id)."", $data,'cr_');
                    }
                    else
                    {
                        // _dump('ins',true);
                        $sql->insert("_courses_results",$data,'cr_');
                    }

            $PData->content( '
                <div class="units-row">
                    <div><h3>'._lang('Результаты теста').'</h3></div>
                    <div>'.
                        '<p>'._lang('Вопросов:').'&nbsp;'.$total.'</p>'.
                        '<p>'._lang('Верно:').'&nbsp;'.$correct.'</p>'.
                        // '<p>'._lang('Неточно:').'&nbsp;'.$inaccur.'</p>'.
                        '<p>'._lang('Неверно:').'&nbsp;'.$incorr.'</p>'.
                    '</div>
                   
                </div>
                
            ');
}

if (!empty($_GET['theme'])){
	$PData->content('Контрольная','title');
}elseif(!empty($_GET['module'])){
	$PData->content('Модульное тестирование','title');
}

$PData->content(
	'<ul class="listmenu">
    	'.pf_sidebar().'
    </ul>'
,'sidebar');

$PData->content(_run_filter('course_before_test_body','',$test).'<div class="test-body"><form method="POST">');


$send_btn = '';
if (isset($test) && !isset($_POST['save_test'])){
	
    $i = 1;
    foreach ($test as $question){
    	
        $answers = json_decode($question['ct_answers'],true);
        $answers_html = '';
        $r = 0;
        if(is_array($answers)){
	        foreach ($answers as $row) {
	            if (isset($row['right'])) $r++;
	        }
	    }
        
		$c = 1;
        
        if(is_array($answers)){
	        foreach ($answers as $row) {
	            $answers_html .= '
	                    <div>
	                        <label><input name="answers['.$question['ct_id'].']['.($r > 1 ? $c++ : '1').']" 
	                        '.($r > 1 ? '' : ' value="'.$c++.'" ').'
	                        type="'.($r > 1 ? 'checkbox' : 'radio').'"> '.$row['text'].'</label>
	                    </div>
	            ';
	        }
	    }
	    
        $PData->content( '
            <div class="units-row">
                <div><h3>'._lang('Вопрос').' № '.$i++.'</h3></div>
                <div>
                    '.$question['ct_question'].'
                </div>
                <div> 
                    '.$answers_html.'                             
                </div>
            </div>
            <hr>
             
        ');
    }

    if (count($test) > 0)
    	$send_btn = '<input type="submit" class="btn" name="save_test" value="'._lang('Отправить').'">';
	else
    	$send_btn = _lang('На данный момент вопросов в этой работе пока нет.');
}


  
$PData->content($send_btn.'</form></div>'._run_filter('course_after_test_body','',$test));

function recarray($ar, $searchfor) {
	$result = array();

    foreach($ar as $k => $v) {
    	if ($k == $searchfor) $result[] = $v;
        if (is_array($ar[$k]))  $result = array_merge($result,recarray($v, $searchfor));
    }
    return $result;
}