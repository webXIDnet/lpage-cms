<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Перша сторінка приватного кабінету
 *
 ***/
if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
  $profile_user = $sql->query( "
				SELECT * FROM _profile WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'" );
  $profile_user = array_filter($profile_user);
  $themes = $sql->query( "
				SELECT *
				FROM _posts
				WHERE p_cat_id = '" . _protect($_GET['module']) . "'" );
  if ( empty( $profile_user ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  }
} else {
  $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
  $PData->redirect( getURL( 'profile', 'login' ) );
}

$PData->content('
<section class="profile">
                <div class="units-row">
                     <div class="unit-40 listmenu">
                            <ul>
                            '._run_filter('profile_left_menu','
                                <li>
                                    <a href="'.getURL('profile', 'private-room').'">'._lang('Profile').'</a>
                                </li>
                                <li>
                                    <a href="#">'._lang('Payment setting').'</a>
                                </li>
                                <li>
                                    <a href="'.getURL('profile', 'donated').'">'._lang('Donated projects').'</a>
                                </li>
                                <li>
                                    <a href="#">'._lang('Donated graph').'</a>
                                </li>
                                <li>
                                    <a href="'.getURL('profile', 'modules').'">'._lang('Modules').'</a>
                                </li>
                                <li>
                                    <a href="#">'._lang('Help').'</a>
                                </li>').'
                            </ul>
                        </div>
                    <div class="unit-50">');
if(isset($themes) && is_array($themes)) {
  $i=1;
  foreach($themes as  $theme) {
    $PData->content( '
                        <div class="units-row">
                            <div class="unit-10">' . $i++ . '</div>
                            <div class="unit-90"> <a href="'.getURL('profile', 'theme').'">' . $theme['p_title'] . '</a></div>
                        </div>
                    ');
  }
}
$PData->content( '
                    </div>
                    <div class="unit-10"></div>
                </div>
            </section>');