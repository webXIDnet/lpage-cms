<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Перша сторінка приватного кабінету
 *
 ***/

global $sqlTPL;
$sqlTPL->getCategories('course%','LIKE');

global $list;

if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ) {
  $profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'" );
  $profile_user = array_filter($profile_user);
  $themes = $sql->query( "
				SELECT *
				FROM _posts
				WHERE p_cat_id = '" . _protect($_GET['module']) . "'" );
  if ( empty( $profile_user ) ) {
    $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
    $PData->redirect( getURL( 'profile', 'login' ) );
  }
} else {
  $PData->content( 'Пройдите Авторизацию', 'message', TRUE );
  $PData->redirect( getURL( 'profile', 'login' ) );
}


$PData->content('Проверочные контрольные','title');

$PData->content(
	'<ul class="listmenu">
    	'.pf_sidebar().'
    </ul>'
,'sidebar');



$PData->content(_run_filter('course_before_topics_tests_list','',$themes).'<div class="topics-tests-list">');

if(isset($themes) && is_array($themes)) {
  $i=1;
  foreach($themes as  $theme) {
    $PData->content( '
                        <div class="units-row">
                            <div class="unit-10">' . $i++ . '</div>
                            <div class="unit-90"> <a href="'.getURL('profile','test','theme='.$theme['p_id'].'&module='.$_GET['module']).'">' . $theme['p_title'] . '</a></div>
                        </div>
                    ');
  }
}
$PData->content('</div>'._run_filter('course_after_topics_tests_list','',$themes));