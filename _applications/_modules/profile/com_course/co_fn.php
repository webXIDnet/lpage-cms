<?php

function course_theme_insert_test_btn($result,$get){


    $param = '';
    if (isset($_GET['theme'])) 
        {
            $param = 'theme='.$_GET['theme'];
            $title = _lang('Начать контрольную');
        }

    if (isset($_GET['module'])) 
        {
            $param = 'module='.$_GET['module'];
            $title = _lang('Начать тест');
        }
    $result .= '<a class="btn" href="'.getURL('profile','test',$param).'">'.$title.'</a>';

    return $result;
}

function course_insert_quest_btn($result,$get){
  

    if (isset($_GET['module']))
    {
        $result .= '
                    
                    <p>
                        <a href="'.getURL('admin','test','type=module&id='.$_GET['module']).'" class="btn btn-black" style="text-decoration: none;">'._lang('Список вопросов').'</a>
                    </p>';
    }

    if (isset($_GET['id']))
    {
        $result .= '
                    
                    <p>
                        <a href="'.getURL('admin','test','type=theme&id='.$_GET['id']).'" class="btn btn-black" style="text-decoration: none;">'._lang('Список вопросов').'</a>
                    </p>';
    }

    
    return $result;

}

function course_test_back_btn($result,$get) 
{
    switch ($_GET['type'])
    {
        case 'module':
            $result .= '
                    <p>
                        <a href="'.getURL('admin','module','module='.$_GET['id']).'" >&larr; '._lang('Назад').'</a>
                        
                    </p>
                    ';
            break;
        case 'theme':
            $result .= '
                    <p>
                        <a href="'.getURL('admin','theme','id='.$_GET['id']).'" >&larr; '._lang('Назад').'</a>
                    </p>
                    ';
            break;
        default: $result .= ''; break;
    }
        
    
    return $result;
}

function course_insert_courses_btn($result,$get) 
{

    $result .= '
            <!--<li>
                <a href="'.getURL('profile','courses').'">'._lang('Курсы').'</a>
            </li>-->
            <li>
                <a href="'.getURL('profile', 'modules').'">'._lang('Modules').'</a>
            </li>
    ';
    return $result;

}
function courses_esse_WISIWIG($vars){
    
    if(PAGE_TYPE=='admin') return '[:courses_esse_WISIWIG '.$vars.':]';
    global  $user,
            $profile,
            $PData;

    if( !$profile->isLoggedIn() || !$profile->fullAccess()){
        $PData->redirect(getURL('profile','login'),'301');
    }
    //_dump($profile);
    if(!empty($profile->esse_done_date) && $profile->esse_done_date>1000){
        return _lang('<div class="esse-done"><h4>Ваша работа провераеться.</h4></div>');

    }

    if(isset($_POST['save-esse']) && !empty($_POST['esse'])){

        $array = array(
            'esse' => $_POST['esse'],
            'esse_done_date' => time()
        );

        $profile->update($array);
    }

    if(empty($vars))$vars = 'esse';
    $text = '';
    
    if(!defined('CKEEDITOR')){
                
        define('CKEEDITOR','TRUE');
        require_once CODE_FOLDER.'../plugins/ckeditor/index.php';
        $text = '<script type="text/javascript">
            ckecke_marker = cke = [];
        </script>';
    
    }
        
    return $text.'
    <form method="POST">
        <textarea id="'.$vars.'" class="'.$vars.'" name="'.$vars.'" rows="10" cols="80" style="width:80%">'.$profile->esse.'</textarea>
        <input type="submit" name="save-esse" value="'._lang('Отправить работу на проверку').'">
    </form>
    <script type="text/javascript">

        HOMEPAGE=\''.HOMEPAGE.'\';
        LANG=\''.$user->lang.'\';
        ckecke_marker[\'time\'] = 0;
        ckecke_marker[\'text\'] = \'\';
        cke[\''.$vars.'\'] = CKEDITOR.replace( \''.$vars.'\', {
                // Load the German interface.
                language: \'ru\',
                extraPlugins: \'autosave\',
                autosaveTargetUrl: \''.getURL('ajax', 'esse-autosave').'\',
                delay_time: 2
                //uiColor: \'#556665\'
            }            
        );

        $(\'body\').keypress(function(){
            $(\'.footer\').html(cke[\''.$vars.'\'].getData());
        });
        
    </script>';
    
}
function esse_autosave_ajax_init(){
    if(!empty($_GET['esse'])){
        global $profile;
        $profile->update( array( 'esse'=>$_GET['esse'] ) );
    }
}
function MODULES_TESTING_RESULTS($vars){
    
    if(PAGE_TYPE=='admin') return '[:MODULES_TESTING_RESULTS '.$vars.':]';

    global  $profile,
            $PData,
            $sql;

    if( !$profile->isLoggedIn() || !$profile->fullAccess()){
        $PData->redirect(getURL('profile','login'),'301');
    }
    
    $result = $sql->query(
        "SELECT *
        FROM _courses_results WHERE cr_profile_id='"._protect($profile->id)."'"
    );

   // _dump($result);
    $return = '';
   foreach ($result as $row) {
        $module_title = $row['cr_name'];
        $res = json_decode($row['cr_result'],true);
        $qvestions = $res['total']; #кількість питань
        $right_ans = $res['correct']; #Кількість правильних відповідей

        $return .= '
            <h3>'._lang('Тест к Модулю').' "'.$module_title.'"</h3>
            <div class="progress clear-margin">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: '.round( ($right_ans/$qvestions)*100, 2 ).'%">
                    '.round( ($right_ans/$qvestions)*100, 2 ).'% ( '.$right_ans.'/ '.$qvestions.' )</div>
            </div>';
   }
    if(empty($return)){
        $return = _lang('Не пройдено ни одного тестирования.').' <a href="'.getURL('page','testy').'">'._lang('Пройти тест >>>').'</a>';
    }
    return $return;

}

function MODULES_TESTS_LIST($vars){

    if(PAGE_TYPE=='admin') return '[:MODULES_TESTS_LIST '.$vars.':]';

     parse_str($vars, $vars);

    global  $profile,
            $PData,
            $sql,
            $sqlTPL;

    if( !$profile->isLoggedIn() || !$profile->fullAccess()){
        $PData->redirect(getURL('profile','login'),'301');
    }
    
    $sqlTPL->getCategories('course'.$vars['course_id']);
    global $list;
    
    $text = '';
    $count = 1;

    foreach ($list->category['course'.$vars['course_id']] as $key => $val) {
        $text .= '
            <div class="units-row">
                <div class="unit-10">'.$count.'.</div>
                <div class="unit-90"> <a href="'.getURL('profile','test','module='.$val['c_id']).'">'._lang('Тест к Модулю').' "'.$val['c_title'].'"</a></div>
            </div>';
        $count++;
    }

    return $text;

}