<?php

function course_theme_insert_test_btn($result,$get){


    $param = '';
    if (isset($_GET['theme'])) 
        {
            $param = 'theme='.$_GET['theme'];
            $title = _lang('Начать контрольную');
        }

    if (isset($_GET['module'])) 
        {
            $param = 'module='.$_GET['module'];
            $title = _lang('Начать тест');
        }
    $result .= '<a class="btn" href="'.getURL('profile','test',$param).'">'.$title.'</a>';

    return $result;
}

function course_insert_quest_btn($result,$get){
  

    if (isset($_GET['module']))
    {
        $result .= '
                    
                    <p>
                        <a href="'.getURL('admin','test','type=module&id='.$_GET['module']).'" class="btn btn-black" style="text-decoration: none;">'._lang('Список вопросов').'</a>
                    </p>';
    }

    if (isset($_GET['id']))
    {
        $result .= '
                    
                    <p>
                        <a href="'.getURL('admin','test','type=theme&id='.$_GET['id']).'" class="btn btn-black" style="text-decoration: none;">'._lang('Список вопросов').'</a>
                    </p>';
    }

    
    return $result;

}

function course_test_back_btn($result,$get) 
{
    switch ($_GET['type'])
    {
        case 'module':
            $result .= '
                    <p>
                        <a href="'.getURL('admin','module','module='.$_GET['id']).'" >&larr; '._lang('Назад').'</a>
                        
                    </p>
                    ';
            break;
        case 'theme':
            $result .= '
                    <p>
                        <a href="'.getURL('admin','theme','id='.$_GET['id']).'" >&larr; '._lang('Назад').'</a>
                    </p>
                    ';
            break;
        default: $result .= ''; break;
    }
        
    
    return $result;
}

function course_insert_courses_btn($result,$get) 
{

    $result .= '
            <!--<li>
                <a href="'.getURL('profile','courses').'">'._lang('Курсы').'</a>
            </li>-->
            <li>
                <a href="'.getURL('profile', 'modules').'">'._lang('Modules').'</a>
            </li>
    ';
    return $result;

}