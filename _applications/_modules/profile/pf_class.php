<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Клас модуля Profile
 * 
***/

class Profile{
	
	public function getAccount(){

		global 	$PData,
				$sql;

		if ( $this->isLogin() ) {

			$profile_user = $sql->query( "
				SELECT *
				FROM _profile
				WHERE pf_id = '" .  _protect($_SESSION['profile_id']) . "'",'value');

			//$profile_user = array_filter($profile_user);
						
			if ( !empty( $profile_user) && is_array($profile_user) ) {

				foreach ($profile_user as $key => $val) {
					$key = str_replace('pf_', '', $key);
					$this -> $key = $val;
				}

				if(!$this->fullAccess() && ( ( @$_GET['page']!='dostup-ogranichen' && PAGE_TYPE=='page') || (PAGE_TYPE=='profile' && @$_GET['profile']!='private-room' && @$_GET['profile']!='logout' ) ) ){
			        $PData->redirect(getURL('page','dostup-ogranichen'),'301');
			    }elseif($this->fullAccess() && PAGE_TYPE=='page' && $_GET['page']=='dostup-ogranichen'){
			    	$PData->redirect( getURL( 'profile', '' ) );
			    }

			}else{
			   $PData->redirect( getURL( 'profile', 'login' ) );
			}

		}elseif( PAGE_TYPE=='profile' && @$_GET['profile']!='login' && @$_GET['profile']!='sign-up' ){

			$PData->redirect( getURL( 'profile', 'login' ) );

		}
	}

	public function isLogin(){

		if ( !empty( $_SESSION['profile_id'] ) && $_SESSION['profile_id'] != 0 ){
			return TRUE;
		}
		return FALSE;
		
	}

	public function fullAccess(){
		if (time()>$this->end_date) {
			return FALSE;
		}
		return TRUE;
	}

	public function update($array, $prefix = 'pf_'){

		if ( is_array($array) ) {

			foreach ($array as $key => $val) {

				if(!isset($this->$key)) unset($array[$key]);
				else $this->$key = $val;
			}

			if(!empty($array)){

				global $sql;
				$sql->update('_profile', "pf_id='"._protect($this->id)."'", $array,$prefix);
				return TRUE;
			}
		}
		return FALSE;
	}

	public function rights_access($rights_array, $return_value = true, $redirect_flag = false) {
        global $PData;
        if (is_array($rights_array)) {
            if (in_array(@$this->rights,$rights_array)) {
                return $return_value;
            } else if ($redirect_flag) {
                $PData->redirect($redirect_flag);
                exit;
            } else {
                return false;
            }
        } else {
            if ($rights_array == @$this->rights)
                return $return_value;
            else if ($redirect_flag) {
                $PData->redirect($redirect_flag);
                exit;
            } else return false;
        }

    }

    public function payment_access($product_id, $previous_product_id = false) {
    	global $sql, $PData;
    	$product = $sql->query("SELECT * FROM _products LEFT JOIN _products_options_values ON (pr_id=pov_object_id) WHERE pr_id='".$product_id."' AND pov_object_type='product'", 'value');

    	if (count($sql->query("SELECT * FROM _profile_logs WHERE pl_type='payment_success' AND pl_pf_id='".$this->id."' AND pl_pr_id='".$previous_product_id."'")) > 0 || count($sql->query("SELECT * FROM _products_options_values LEFT JOIN _products_options ON (pov_po_id=po_id) WHERE pov_object_type='product' AND pov_object_id='".$product_id."' AND po_option_key='opened'")) > 0) {
    		return true;
    	} else return false;
    }

    public function view_access($product_id) {
    	global $sql;
		if (count($sql->query("SELECT * FROM _profile_logs WHERE pl_type='payment_success' AND pl_pf_id='".$this->id."' AND pl_pr_id='".$product_id."'")) > 0) {
    		return true;
    	} else return false;
    }
}

$profile = new Profile();