<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();}
/**
 *
 * PAGE_TYPE = profile
 * Модуль Профайла. Глобальні функції маюуля.
 *
 ***/

function getProfileUrl($atr){

	switch($atr['key']){
		case'profile':
			
			if(!empty($atr['get']))$atr['get'] = '?'.$atr['get'];
			if(!empty($atr['val']))$atr['val'] = $atr['val'].'/';
			
			return HOMEPAGE.$atr['key'].'/'.$atr['val'].$atr['get'];
			break;

		default:
			return FALSE;
	}
}
function change_photo(){
	global $sql;
	if ( isset( $_GET['change-photo'] ) ) {

			if ( isset( $_FILES['change_photo'] ) ) {
				var_dump($_GET['change-photo'] );
				var_dump($_FILES['change_photo'] );
				die();
				$valid_formats = array( "jpg", "png", "gif", "bmp", "jpeg" );
				list( $txt, $ext ) = explode( ".", $_FILES['change_photo']['name'] );
				if ( in_array( $ext, $valid_formats ) ) {
					if ( $_FILES['change_photo']['size'] < ( 1024 * 1024 ) ) {
						$userpic_dir  = 'media/avatars/';
						$userpic_path = $userpic_dir . basename( $_FILES['change_photo']['name'] );
						if ( move_uploaded_file( $_FILES['change_photo']['tmp_name'], $userpic_path ) ) {
							$allinfo_userpic = array( 'userpic' => $userpic_path );
							$pf_id        = 'pf_id=' . $_SESSION['profile_id'];
							$sql->update( '_profile', $pf_id, $allinfo_userpic, 'pf_' );
							//return '<img src="'.$allinfo_userpic.'" class="preview>';
							$json['img'] = $_FILES['change_photo']['name'];
						} else $json['error'] = "failed";
					} else $json['error'] = "Image file size max 1 MB";
				} else $json['error'] = "Invalid file format..";
			} else $json['error'] = "Please select image..!";
			echo json_encode( $json );
	}
}

function pf_sidebar(){//Формує бокове меню
	return _run_filter('profile_left_menu',
		'<li>
             <a href="'.getURL('profile', 'private-room').'">Profile</a>
        </li>
        <li>
            <a href="'.getURL('profile', 'payment').'">Payment setting</a>
        </li>
        <li>
            <a href="'.getURL('profile', 'donated').'">Donated projects</a>
        </li>
        <li>
            <a href="#">Donated graph</a>
        </li>
        <li>
            <a href="#">Help</a>
        </li>'
	);
}

function pf_accounts_editing_form($cval){
	
	global $user;

	$pf_id = $cval['pf_id'];

	#Формування форми редагування даниз акаунта
	if($user->rights>2){
		$form = '<div class="tableEdit_form">'.
					
				_run_filter('pf_accounts_editing_form_account_information',
					
					'<h2 class="h1" style=" text-align: center; ">'._lang('Пользователь').'</h2>
					
					<div class="right_colmn">
						<p class="right-fields field-1" style="margin-top:0;">
							<b>'._lang('Аватар').'</b>:<br>
							<img src="'.SITE_FOLDER.$cval['pf_userpic'].'" style="max-width:300px;max-height:300px;"/>
						</p>
						
					</div>
					<p class="left-fields field-5">	
						<label>
							<b>'._lang('ФИО').'</b>:<br>
							<input class="input" type="text" name="editAccount['.$pf_id.'][fullname]" value="'.$cval['pf_fullname'].'"/>
						</label>
					</p>
					<p class="left-fields field-6">	
						<label>
							<b>'._lang('Email').'</b>:<br>
							<input class="input" type="text" name="editAccount['.$pf_id.'][email]" value="'.$cval['pf_email'].'"/>
						</label>
					</p>
					<p class="left-fields field-7">	
						<label>
							<b>'._lang('Доступ открыт').'</b>:<br>
							<input class="input" type="date" name="editAccount['.$pf_id.'][create_date]" value="'.(!empty($cval['pf_create_date'])?date('Y-m-d',$cval['pf_create_date']):'').'"/>
						</label>
					</p>
					<p class="left-fields field-8">	
						<label>
							<b>'._lang('Аккаунт доступный до').'</b>:<br>
							<input class="input" type="date" name="editAccount['.$pf_id.'][end_date]" value="'.(!empty($cval['pf_end_date'])?date('Y-m-d',$cval['pf_end_date']):'').'"/>
						</label>
					</p>
					<p class="left-fields field-9">
						'._lang('Осталось').': <b style="color:#000">'._lang($cval['pf_end_date']>$cval['pf_create_date']
						?
							round( ($cval['pf_end_date'] - time())/(60*60*24) )
						: 
							0 ).' '._lang('дней').'</b><br/>
						'._lang('Результаты тестирования').': <b style="color:#000">---</b> %
					</p>
					',
					array(
						'cval' => $cval,
						'pf_id' => $pf_id
					)
				)
					.'
					<div class="clear"></div>
					<h2 class="h1" style=" text-align: center; ">'._lang('Эссе').'</h2>
					<div class="left-fields field-5">	
						'.$cval['pf_esse'].'
					</div>
					<br/><br/>
				</div>';
	}
	
	#Дані, що виводяться у списку акаунтів у адмінці
	$text = '
		<tr>
			<td id="tableList0" name="pf_id" class=" col0 tableList0" style>
				<label>
					'.u_ifRights( 
						array(6,10,100), 
						'<input type="checkbox" name="itemID['.$pf_id.']">'
					).'
					<span title="'._lang('Внутренний номер заказа').'">'.$cval['pf_id'].'</span>
				</label>
				'.$form.'
			</td>
			
			<td id="tableList1" name="o_id" class=" col col1 tableList1" style="color:#888">
				
				<b style="color:#000">'.$cval['pf_fullname'].'</b><br/>
				'._lang('Email').': <span style="color:#000">'.$cval['pf_email'].'</span><br/>
				
			</td>
			<td id="tableList2" name="o_prod" class=" col col2 tableList2" style="color:#888">

				'._lang('Доступ открыт с').': <b style="color:#000">'.
					(
						!empty($cval['pf_create_date'])
						?
							date(_lang('d.m.Y H:i'),$cval['pf_create_date'])
						:
							'---'
					).
					'</b><br/>
				'._lang('Доступ открыт до').': <b style="color:#000">'.
					(
						!empty($cval['pf_end_date'])
						?
							date(_lang('d.m.Y H:i'),$cval['pf_end_date'])
						:
							'---'
					).'</b><br/>
				'._lang('Осталось').': <b style="color:#000">'.
					_lang(
						$cval['pf_end_date']>$cval['pf_create_date']
						?
							round( ($cval['pf_end_date'] - time())/(60*60*24) )
						: 
							0 
					).' дней</b><br/>
			</td>
			<td id="tableList3" name="o_lp_id" class=" col col3 tableList3" style="color:#888">
				'.(
					!empty($cval['pf_esse'])
					?
						(
							!empty($cval['pf_esse_done_date']) && $cval['pf_esse_done_date']>1000
							?
								'<b style="color:#000;">'._lang('Подано на проверку').'</b>'
							:
								_lang('Что-то есть!')
						)
					:
						_lang('---')
				).'
			</td>
		</tr>
	'; 

	return $text;
}

/**
 * Перевіряє, чи користувач залогований.
 * Якщо ні, переадресовує на cторінку логвування
 * Використовується у шоткоді [:IsLoggedIn:]
 *
 * @return FALSE
 *
 * @category Profile
 * @property ShortCode
 * @since 1.2.4
 * @author Pavlo Matsura 
 */
function IsLoggedIn() {
	global 	$profile,
			$PData;
	
	if(!$profile->isLoggedIn()){
		$PData->redirect(getURL('profile','login'), '301');
	}
}

//_add_action('page_init','profile_page_init');
function profile_page_init($var) {

    return _shortCode($var['p_text']);
}
$accepted_rights = 0;
function ProfileAccess($vars) {
    global $profile, $accepted_rights;
    parse_str(html_entity_decode($vars),$accepted_rights);

    _add_filter('page_init','profile_page_init_filter',20);
    if (isset($accepted_rights['days_of_access']))
    return SimpleCounter_custom($accepted_rights['days_of_access']);
}

function profile_page_init_filter($res,$var){
    global $profile, $accepted_rights;
//    print_r((($accepted_rights['days_of_access']*86400) + $profile->create_date) - time());
//    print_r($accepted_rights);
//    print_r($profile->create_date);
    if (!$profile->rights_access(explode(',',$accepted_rights['rights'])) || (
            isset($accepted_rights['days_of_access']) &&
        (
            (($accepted_rights['days_of_access']*86400) + $profile->create_date) - time()
        ) < 0)) {
        $res['p_text'] = sc_snippetCode('%_no_access_'.$res['p_article'].'_%');
        unset($accepted_rights);
    }
    return $res;
}

function SimpleCounter_custom($days_of_access) {
global $PData, $profile;
    if(PAGE_TYPE!='admin'){

        global 	$lp,
                  $JQUERY_COUNTDOWN;
        $date = $profile->create_date+($days_of_access*86400);
//        print_r($date);
//        $d = str_replace('_', ' ', $date);
        $text = '';
        $result = '';

        if(!defined('JQUERY_COUNTDOWN')) {

            $result.= '
				<script src="'.SITE_FOLDER.'plugins/countdown-timer/jquery.countdown.js"></script>';
            $JQUERY_COUNTDOWN = 0;
            define('JQUERY_COUNTDOWN',$JQUERY_COUNTDOWN);
        }

        $id = 'countdown'.$JQUERY_COUNTDOWN;

        if(!defined('JQUERY_COUNTDOWN'.$date)){

            $JQUERY_COUNTDOWN++;

            $id = 'countdown'.$JQUERY_COUNTDOWN;

            $PData->position['head'] .= '
				<script>var TIME_LINIT'.$JQUERY_COUNTDOWN.'='.(int)($date-time()).';</script>
				<script>

					$(document).ready(function(){
						i=0;
						$(\'.'.$id.'\').each(function(){
							i++;
							$(this).attr(\'id\',\''.$id.'\'+i);
							var note = $(\'#note\'),
							ts = new Date(2012, 0, 1),
							newYear = true;

							if((new Date()) > ts){
								// Задаем точку отсчета для примера. Пусть будет очередной Новый год или дата через 10 дней.
								// Обратите внимание на *1000 в конце - время должно задаваться в миллисекундах
								ts = (new Date()).getTime()+ TIME_LINIT'.$JQUERY_COUNTDOWN.'*1000;
								newYear = false;
							}



							$(\'#'.$id.'\'+i).countdown({
								timestamp	: ts,
								callback	: function(days, hours, minutes, seconds){
									var message = "";

									message += "Дней: " + days +", ";
									message += "часов: " + hours + ", ";
									message += "минут: " + minutes + " и ";
									message += "секунд: " + seconds + " <br />";

									if(newYear){
										message += "осталось до Нового года!";
									}
									else {
										message += "осталось до момента через 10 дней!";
									}

									note.html(message);
								}
							});
						});
					});

				</script>
			';
//            define('JQUERY_COUNTDOWN'.$date,'1');
        }


         $result .= '<!-- '.$date.' -->

			<div class="'.$id.'"></div>

			<div class="cl">
				<span>'._lang('дней').'</span><span>'._lang('часов').'</span><span>'._lang('минут').'</span><span>'._lang('секунд').'</span>
			</div>'.$text.'
			<script>

			</script>';
//        var_dump($result);
        return $result;
    }

    return '[:SimpleCounter '.$days_of_access.' :]';
}

function ConsultForm() {
    global $profile;
    $result = '
        <form method="post" action="'.getURL('post','zapis-na-konsultatsiyu-s-instruktorom','category=stranitsy-blagodarnosti').'">
        <input type="hidden" name="rf_name" value="'.$profile->fullname.'">
        <input type="hidden" name="rf_email" value="'.$profile->email.'">
        <input type="hidden" name="rf_phone" value="'.$profile->pubtel.'">
          <div class="form-group">
            <label for="conn_type">'._lang('Вид консультации').'</label>
            <select name="field['._lang('Вид кансультации').']" class="form-control">
                <option value="Личная встреча в Киеве">Личная встреча в Киеве</option>
                <option value="Skype консультация">Skype консультация</option>
            </select>
          </div>
          <div class="form-group">
            <label for="time">'._lang('Дополнительная информация').'</label> ('._lang('дата и время встречи, Skype ID, дополнительные пожелания').')
            <textarea class="form-control" id="time" name="note"></textarea>
          </div>
          <input type="hidden" name="prod_title" value="'._lang('Заявка на консультацию [mini-distance]').'">
          <input type="hidden" name="action" value="consult">
          <button type="submit" name="ordersubmit" class="btn btn-primary">Отправить</button>
        </form>
    ';
    return $result;
}

function get_additional_fields() {
    global $profile;
    $result = '';
    if (isset($_POST['roomSubmit']) || isset($_POST['AllSubmit'])) {
//        print_r($_POST);
        $errors = 0;
        if (!empty($_POST['additional_fields'])) {
            foreach ($_POST['additional_fields'] as $field) {
                if (empty($field['value'])) $errors++;
            }
        }
//        if (empty($_POST['']))

        if ($errors == 0) {
            $fields = json_encode($_POST['additional_fields']);
            $profile->update(array(
                'additional_fields' => $fields
            ));
        }

    }

        $fields_array = json_decode($profile->additional_fields,true);
        if (!is_array($fields_array)) {
            $fields_array = array(
                array(
                    'key' => 'company',
                    'type' => 'text',
                    'name' => _lang('Компания'),
                    'value' => ''
                ),
                array(
                    'key' => 'job',
                    'type' => 'text',
                    'name' => _lang('Должность'),
                    'value' => ''
                ),
                array(
                    'key' => 'career',
                    'type' => 'text',
                    'name' => _lang('Род деятельности'),
                    'value' => ''
                ),
            );
        }


            /*$result = '<form method="post" action="">';
            $i = 0;
            foreach ($fields_array as $val) {

                if ($val['type'] == 'textarea')
                    $result .= '
                  <div class="form-group">
                    <input type="hidden" name="additional_fields['.$i.'][type]" value="'.$val['type'].'">
                    <input type="hidden" name="additional_fields['.$i.'][name]" value="'.$val['name'].'">
                     <textarea class="form-control" id="time" name="additional_fields['.$i.'][value]">'.$val['value'].'</textarea>
                  </div>';
                else
                    $result .= '
                  <div class="form-group">
                    <input type="hidden" name="additional_fields['.$i.'][type]" value="'.$val['type'].'">
                    <input type="hidden" name="additional_fields['.$i.'][name]" value="'.$val['name'].'">
                    <input type="'.$val['type'].'"placeholder="' . $val['name'] . '" name="additional_fields['.$i.'][value]" value="'.$val['value'].'">
                  </div>';
                $i++;
            }
            $result .= '
              <button type="submit" name="add_form_submit" class="btn btn-primary">Отправить</button>
        </form>
        ';*/

//        else
        {
//
        }


    return $fields_array;
}

function ExamForm() {
global $profile;
    $result = '';
    if (isset($_POST['add_form_submit'])) {
//        print_r($_POST);
        $fields = '';

        foreach ($_POST['exam_fields'] as $row) {
            $fields .= '<p><strong>'.$row['name'].'</strong></p>';
            $fields .= '<p>'.$row['value'].'</p><p></p>';
        }


        $profile->update(array(
            'esse' => $fields
        ));
    }

    $fields_array = array(
        array(
            'key' => 'field1',
            'type' => 'textarea',
            'name' => _lang('Что полезного Вы извлекли для себя из курса обучения "Работа с возражениями"?'),
            'value' => ''
        ),
        array(
            'key' => 'field2',
            'type' => 'textarea',
            'name' => _lang('Научились ли Вы чему-то новому?'),
            'value' => ''
        ),
        array(
            'key' => 'field3',
            'type' => 'textarea',
            'name' => _lang('Насколько применимы те знания, которые Вы получили в ходе курса обучения, в Вашей работе?'),
            'value' => ''
        ),
        array(
            'key' => 'field4',
            'type' => 'textarea',
            'name' => _lang('Напишите общее впечатление о курсе: теория + примеры и методичка + встреча/скайп-конференция с инструктором.'),
            'value' => ''
        )
    );

    if ($profile->esse == '') {
        $result = '<form method="post" action="">';
        $i = 0;
        foreach ($fields_array as $val) {

            if ($val['type'] == 'textarea')
                $result .= '
                  <div class="form-group">
                    <label for="'.$val['key'].'">'.$val['name'].'</label>
                    <input type="hidden" name="exam_fields['.$i.'][name]" value="'.$val['name'].'">
                     <textarea class="form-control" id="'.$val['key'].'" name="exam_fields['.$i.'][value]">'.$val['value'].'</textarea>
                  </div>';
            else
                $result .= '
                  <div class="form-group">
                  <label for="'.$val['key'].'">'.$val['name'].'</label>
                  <input type="hidden" name="exam_fields['.$i.'][name]" value="'.$val['name'].'">
                    <input type="'.$val['type'].'" id="'.$val['key'].'" class="form-control" placeholder="' . $val['name'] . '" name="exam_fields['.$i.'][value]" value="'.$val['value'].'">
                  </div>';
            $i++;
        }
        $result .= '
              <button type="submit" name="add_form_submit" class="btn btn-primary">Отправить</button>
        </form>
        ';
    } else {
        _add_filter('page_init','profile_page_init_filter_exam',20);
    }



    return $result;
}

function profile_page_init_filter_exam($res, $var) {
    $res['p_text'] = sc_snippetCode('%_exam_success_%');
    return $res;
}


if (isset($_POST['addClientProfile'])) {
//    add_client_profile();
    _add_action('defined_modules','add_cl_profile');

}

if (isset($_POST['ordersubmit']) && isset($_POST['action']) == 'consult') {
//    add_client_profile();
    _add_action('defined_modules','upd_profile_rights');

}

function upd_profile_rights() {
    global $profile;
    if (!empty($_POST['field']) && !empty($_POST['note']))
        $profile->update(array('rights' => '2'));
}

function add_cl_profile() {

        global $sql, $meta;


        $cl_id = 0;
        foreach ($_POST['editClient'] AS $key => $val) {
            $cl_id = $key;
        }
        $pass = _getRandStr(8);
        $profile_array = array(
            'fullname'         => $_POST['editClient'][$cl_id]['fullname'],
            'email'       => $_POST['editClient'][$cl_id]['email'],
            'pass'        => md5($pass),
            'type_acc'    => '1',
            'pubtel'      => $_POST['editClient'][$cl_id]['tel'],
            'cl_id'       => $cl_id,
            'create_date' => time(),
            'end_date'    => time() + $meta->val('days_of_access') * 24 * 60 * 60,
            'rights'      => '0'
        );

        $sql->insert('_profile', $profile_array, 'pf_');
        $text =
            '<p>' . _lang('Вы получили доступ к сайту дистанционного обучения') . '</p>
        <p>' . _lang('Ваши данные') . ':</p>
        <p>' . _lang('Логин') . ':&nbsp;' . $profile_array['email'] . '</p>
        <p>' . _lang('Пароль') . ':&nbsp;' . $pass . '</p>
        <p><a href="'.HOMEPAGE.'profile/login/">' . _lang('Перейти на сайт') . '</a></p>
    ';
        _eMail($profile_array['email'], _lang('Доступ к сайту дистанционного обучения.'), $text);

}


