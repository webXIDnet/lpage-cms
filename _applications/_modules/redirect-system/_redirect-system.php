<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/ 
 
require_once __DIR__ . '/reds_fn.php';

$LData->addTranslations(array(
	'ua' => array(
		'Краткие ссылки для переадресации'=>'Короткі посилання для переадресації',
		'Cсылка' => 'Посилання',
		'Сокращенная ссылка'=>'Скорочене посилання'
	)
));



_add_filter('redirect-system_admin-panel_mod_settings','redS_adminpanel_mod_settings');


if(PAGE_TYPE=='redirect'){
	_add_action('init_processes','redS_init_processes');	
}

_add_action('getURL_rule', 'redS_getURL');

_add_action('defined_modules', 'redirect_system_defined_modules',3);

function redirect_system_defined_modules(){
	if(PAGE_TYPE=='admin'){
		global 	$meta,
				$sql;
		
		$module_db = $meta->val('apSet_Redirect-system','mod_option');
		if($module_db){
			$sql->replace_str('_meta_datas', array('md_key','md_value'), 'apSet_Redirect-system', 'apSet_redirect-system');
		}
	}
}