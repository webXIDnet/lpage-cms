<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Онлайн оплати
 *  
***/

function redS_init_processes(){
	global 	$meta,
			$PData;
	
	$url = HOMEPAGE;
	$redS = $meta->val('apSet_redirect-system','mod_option');
	
	if(!empty($_GET['redirect'])){
		if($u = array_keys($redS['URLs'], $_GET['redirect'])){
			$url = $u[0];
			if(str_replace('http://','',$url)==$url)$url = 'http://'.$url;
		}
	}
	
	$PData->redirect($url,'301');
}

function redS_adminpanel_mod_settings($result,$set){
	
	$result['title'] .= 
		'<li switchtab="tab_u_1" class="active"><a>'._lang('Краткие ссылки для переадресации').'</a></li>';
	
	$user_rights = '
		<p>
			
			<label>
				<input class="input" placeholder="'._lang('Ключ').'" type="text" name="newModSet[apSet_redirect-system][URLs][0]" value="'._getRandStr(6).'"/>
			</label>
			<label>
				<input class="input" placeholder="'._lang('Cсылка').'" type="text" name="newModSet[apSet_redirect-system][URLs][1]" value=""/>
			</label>
			
			<hr/><br/>';
	
	if(is_array($set)){
		
		foreach($set AS $key_com=>$array){
			if(is_array($array)){
				foreach($array AS $key=>$val){
					{
						$user_rights .= '
						<p>
							
								<b title="'._lang('Сокращенная ссылка').'">'.getURL('redirect',$val).'</b> -> '.$key.' 
							<label>
								<input class="input" type="text" name="modSet[apSet_redirect-system]['.$key_com.']['.$key.']" value="'.$val.'"/>
								<input type="checkbox" name="delApSet[apSet_redirect-system]['.$key_com.']['.$key.']" value="'.$key.'"> - '._lang('Удалить').'  
							</label>
							
						</p>
						';
					}
				}
				$user_rights .= '<hr/>';
			}
		}
	
	}
	
	
	$result['text'] .= 
		'<div id="tab_u_1" class="tab_box">
			'.$user_rights.'
		</div>';
		
	return $result;
}
function redS_getURL($atr){
	
	switch($atr['key']){
		case'redirect':
			
			if(!empty($atr['get']))$atr['get'] = '?'.$atr['get'];
			
			return HOMEPAGE.'r/'.$atr['val'].$atr['get'];
		break;
		
		default:
			return FALSE;
		
	}	
}