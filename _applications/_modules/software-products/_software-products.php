<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Продажу програмного забезпечення
 *  
***/ 

require_once __DIR__ . '/sw_fn.php';

/** Формує підпункти розділу "Software-Products" меню адмінки у правій колонці **/

_add_filter('adminPanel_sidebar_menu', 'Software_Products_AP_sidebar_menu');

function Software_Products_AP_sidebar_menu($result){//модифікація меню адмін панелі в правій колонці
	return $result . u_ifRights(
		 array(100),
		'
		<div id="menu" class="'._getActive(@$_GET['admin'],'posts').'">
			<a id="menua" href="'.getURL('admin','software').'">'._lang('Software Products').'</a>
			<ul class="submenu">
				<li>
					<a href="'.getURL('admin','sw-add').'">+ '._lang('Добавить Продукт').'</a>
				</li>
				<li>
					<a href="'.getURL('admin','software').'">'._lang('Все Продукты').'</a>
				</li>
				<li>
					<a href="'.getURL('admin','categories','category_type=software').'">
						'._lang('Категории').'
					</a>
				</li>
			</ul>
		</div>
		
		'
	);
}

/** Роутер Адмінки **/

_add_hook('adminPanel_controller', 'Software_Products_admin_controller');

function Software_Products_admin_controller(){
	
	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL;
	
	
	switch(@$_GET['admin']){

		/** Landing Pages **/
		
		case'sw-add':
		
			require_once __DIR__ . '/apps_admin/a_sw_add.php';
			return TRUE;
			
		break;
		case'sw-edit':
		
			require_once __DIR__ . '/apps_admin/a_sw_edit.php';
			return TRUE;
			
		break;
		case'software':
		
			require_once __DIR__ . '/apps_admin/a_software.php';
			return TRUE;
			
		break;
		
	}
}


/** Опрацювання даних про  **/

_add_action('ajax_init', 'Software_Products_ajax_init');

function Software_Products_ajax_init(){
	
	if(isset($_GET[''])){
		
	}
	
}