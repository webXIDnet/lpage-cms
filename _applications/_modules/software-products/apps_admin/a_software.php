<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/

if(isset($_POST['postDeleteSubmit']) && isset($_POST['_post'])){

	$where='';

	foreach($_POST['_post'] AS $key=>$val){

		if( !empty($where) ) $where.=' OR ';

		$where .= "sw_id='"._protect($key)."'";

	}

	$sql->query( "DELETE FROM _landing_pages WHERE $where;", 'query' );

	$PData->content('Запись удалена','message',TRUE);

}elseif(isset($_POST['postsCopySubmit']) && isset($_POST['_post'])){

	$where='';

	foreach($_POST['_post'] AS $key=>$val){

		$cval = $sql->query("
			SELECT *
			FROM _landing_pages
			WHERE sw_id='"._protect($key)."'
		",'value');

		if(isset($cval) && is_array($cval)){

			unset($cval['sw_id']);

			$cval['sw_article'] .= '-copy';
			$cval['sw_title'] .= '-copy';

			$cval1 = $sql->query("
				SELECT sw_id
				FROM _landing_pages
				WHERE sw_article='"._protect($cval['sw_article'])."' AND sw_cat_id="._protect($cval['sw_cat_id'])."
			",'value');

			while($cval1){

				$cval['sw_article'] .= '-copy';
				$cval['sw_title'] .= '-copy';

				$cval1 = $sql->query("
					SELECT sw_id
					FROM _landing_pages
					WHERE sw_article='"._protect($cval['sw_article'])."' AND sw_cat_id="._protect($cval['sw_cat_id'])."
				",'value');

			}


			$cval['sw_public_date'] = time();

			$sql->insert('_landing_pages',$cval);

			$PData->content('Запись  скопирована','message',TRUE);

		}else{
			$PData->content('Невозможно скопировать запись','message',FALSE);
		}

	}
}

$where = $w = '';


if(!empty($_GET['filter'])){
	$w = "sw_title LIKE '%"._protect($_GET['filter'])."%'";
}

if(!empty($_GET['filter_cat'])){
	if(!empty($w))$w.=' AND ';
	$w = $w." sw_cat_id = '"._protect($_GET['filter_cat'])."'";
}

if(!empty($w)){
	$where = "WHERE ".$w;
}

$nav = _pagination('_software','sw_id',$where);

$result = $sql->query("
	SELECT *
	FROM _software
	{$where}
	".@$nav['limit']."
");

$sqlTPL->getCategories('software');

global $list;

$text='';
$i = 0;
foreach($result AS $cval){

	$i++;
	$class = '';
	if(!_checkURL($cval['sw_url'])){
		$class = 'redText';
	}

	$text.='<tr class="'.$class.'">
		<td >
			<input title="'._lang('выбрать').'" type="checkbox" name="_post['.$cval['sw_id'].']"/> &nbsp;&nbsp; '.$cval['sw_id'].'
		</td>
		<td >
			<a title="'._lang('Редактировать').'" href="'.getURL('admin','sw-edit','id='.$cval['sw_id']).'">
				'.$cval['sw_title'].'</a>
		</td>
		<td >
			'.@$list->category_index[$cval['sw_cat_id']]['c_title'].'
		</td>
		<td style="text-align:center;">
			'.$cval['sw_d_count'].'
		</td>
		<td style="text-align:center;">
			'.$cval['sw_version'].'
		</td>
	</tr>';

}

$PData->content('Software Products','title');

$PData->content('
	<a href="'.getURL('admin','sw-add').'" class="boxPic add_post">
		'._lang('Добавить Продукт').'
	</a>
	<a href="'.getURL('admin','categories','category_type=software').'" class="boxPic cat">
		'._lang('Категории').'
	</a>

	<div class="clear"></div>
	<hr/>

	<form class="list">
		<table width="100%">
			<tr>
				<td>
					<label>'._lang('Поиск по заголовку').':</label>
					<input type="text" name="filter" value="'.@$_GET['filter'].'"/><br/>
					<input type="submit" class="btn" value="'._lang('Искать').'"/>
				</td>
				<td>
					<label>'._lang('Категории').'</label>
					<select name="filter_cat">
						<option value="">- '._lang('Категория не выбрана').' -</option>
						'.$contTPL->catHierarchTree(@$list->category['software'],0,@$_GET['filter_cat']).'
					</select>
				</td>
			</tr>
		</table>
	</form>
	<hr/>

	<div class="well">
		<form class="list" method="POST">
		    <table class="table">
		      <thead>
		        <tr>
		        	<th class="numer">'._lang('ID').'</th>
					<th>'._lang('Заголовок').'</th>
					<th>'._lang('Категория').'</th>
					<th>'._lang('Количество загрузок').'</th>
					<th>'._lang('Версия').'</th>
		        </tr>
		      </thead>
		      <tbody>
		        	'.$text.'
		      </tbody>
		    </table>
		    <input style="float:right;" class="btn btn-primary" type="submit" value="'._lang('Удалить').'" name="postDeleteSubmit">
			<input class="btn btn-primary" type="submit" value="'._lang('Копировать').'" name="postsCopySubmit">

		</form>
	</div>

'.$nav['html']);