<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/

$sqlTPL->getCategories('software');

global $list;

if(isset($_POST['swNewsubmit'])){

	if( !empty($_POST['swAdd']['title']) && ( !empty($_POST['swAdd']['url']) || isset($_FILES['file_url']["tmp_name"]) ) ){
		$mas=$_POST['swAdd'];
		
		if(isset($_FILES['file_url']["tmp_name"]) && is_uploaded_file($_FILES['file_url']["tmp_name"])){
			$file = pathinfo($_FILES['file_url']['name']);
			$url = $file_name = date('d-m-Y').'_'._textToURL($_POST['swAdd']['title']).'_'._textToURL($_POST['swAdd']['version']).'_'._getRandStr();
			
			while(is_file(CODE_FOLDER.'../media/files/sw-products/'.$url.'.'.$file['extension'])){
				$url = $file_name.'_'._getRandStr();
			}
			$mas['url'] = HOMEPAGE.'media/files/sw-products/'.$url.'.'.$file['extension'];
			
			if(!move_uploaded_file($_FILES['file_url']['tmp_name'], CODE_FOLDER.'../media/files/sw-products/'.$url.'.'.$file['extension'])){
				$mas['url']='';
			}
		}elseif(!empty($_POST['swAdd']['url'])){
			$mas['url'] = $_POST['swAdd']['url'];
		}
		
		if(!empty($mas['url'])){
			
			$sql->insert('_software',$mas,'sw_');
			
			$PData->content('Запись сохранена','message',TRUE);
			
			unset($mas);
			unset($_POST['swAdd']);
			
		}else{
			$PData->content('Возникли проблемы с загрузкой файла. Попробуйте еще раз, если ошибка повторится - обратитесь за помощью к специалисту','message');
		}
	}else{
	
		$PData->content('Нужно заполнить все объязательные поля!','message');
	
	}
}

/** __________ Формування основного HTML коду __________ **/

$PData->content('Создание Landing Page','title');

$PData->content('
	<form class="list" method="POST" enctype="multipart/form-data">
	<p class="right_colmn" style="margin-top: 0;">
		<label>'._lang('Категория').'</label> 
		<select name="swAdd[cat_id]">
			'.$contTPL->catHierarchTree(@$list->category['software'],0,@$_POST['swAdd']['cat_id']).'
		</select>
	</p>
	<p>
		<label>'._lang('Заголовок').' *</label> 
		<input type="text" class="input-xlarge" name="swAdd[title]" value="'.@$_POST['swAdd']['title'].'"/>
	</p>
	<p>
		<label>'._lang('Cсылка на архив').' *</label> 
		<input type="text" class="input-xlarge" name="swAdd[url]" value="'.@$_POST['swAdd']['article'].'"/>
		'._lang('или').'
		<input type="file" class="input-xlarge" name="file_url" value="'.@$_POST['swAdd']['article'].'"/><br/>
		<span class="greyText">! - '._lang('если указана ссылка для скачивания и выбран файл - в базу будет сохранена ссылка').'</span>
	</p>
	<p>
		<label>'._lang('Версия').'</label> 
		<input type="text" class="input-xlarge" name="swAdd[version]" value="'.@$_POST['swAdd']['version'].'"/>
	</p>
	
	<div class="clear"></div>
	<br/><br/>
	<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="swNewsubmit"/>
</form>
');