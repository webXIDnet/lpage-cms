<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/

if(empty($_GET['id']))$PData->redirect('','404');

$sqlTPL->getCategories('software');

global $list;

if(isset($_POST['swNewsubmit'])){

	if( !empty($_POST['swEdit']['title']) && ( !empty($_POST['swEdit']['url']) || ( isset($_FILES['file_url']["tmp_name"]) && is_uploaded_file($_FILES['file_url']["tmp_name"]) ) ) ){
		$mas=$_POST['swEdit'];
		
		if(isset($_POST['url_del'])){
			
			if(stristr($_POST['old_file_url'],HOMEPAGE.'media/files/')){
				$url = str_replace(HOMEPAGE,CODE_FOLDER.'../',$_POST['old_file_url']);
				if(is_file($url))unlink($url);
				unset($mas['url']);
			}else{
				$PData->content('Скрипт не может удалить файл, размещенный на другом сервере','message');
			}
		}
		
		if(isset($_FILES['file_url']["tmp_name"]) && is_uploaded_file($_FILES['file_url']["tmp_name"])){
			
			$file = pathinfo($_FILES['file_url']['name']);
			$url = $file_name = 
				date('d-m-Y').'_'._textToURL($_POST['swEdit']['title']).'_'.
				_textToURL($_POST['swEdit']['version']).'_'._getRandStr();
			
			while(is_file(CODE_FOLDER.'../media/files/sw-products/'.$url.'.'.$file['extension'])){
				$url = $file_name.'_'._getRandStr();
			}
			
			$mas['url'] = HOMEPAGE.'media/files/sw-products/'.$url.'.'.$file['extension'];
			
			if(!move_uploaded_file($_FILES['file_url']['tmp_name'],CODE_FOLDER.'../media/files/sw-products/'.$url.'.'.$file['extension'])){
				$mas['url']='';
			}
			
		}elseif(!empty($_POST['swEdit']['url'])){
			$mas['url'] = $_POST['swEdit']['url'];
		}
		
		if(!empty($mas['url'])){
			
			$sql->update('_software',"sw_id='"._protect($_GET['id'])."'",$mas,'sw_');
			
			$PData->content('Запись сохранена','message',TRUE);
			
			unset($mas);
			unset($_POST['swEdit']);
			
		}else{
			$PData->content('Возникли проблемы с загрузкой файла. Попробуйте еще раз, если ошибка повторится - обратитесь за помощью к специалисту','message');
		}
		
	}else{
		$PData->content('Нужно заполнить все объязательные поля!','message');
	}
}


/** __________ Формування основного HTML коду __________ **/

$cval = $sql->query("
	SELECT *
	FROM _software
	WHERE sw_id='"._protect(@$_GET['id'])."'
",'value');

if(is_array($cval)){
	$PData->content(_lang('Редактирование записи').' (Software Product)','title');
	
	if(!_checkURL($cval['sw_url'])){
		$cval['sw_url'] = '';
		$PData->content('Не указана ссылка для скачивания файла','message');
	}
	
	$PData->content('
		<form class="list" method="POST" enctype="multipart/form-data">
		<p class="right_colmn" style="margin-top: 0;">
			<label><b>'._lang('Категория').'</b></label> 
			<select name="swEdit[cat_id]">
				'.$contTPL->catHierarchTree(@$list->category['software'],0,$cval['sw_cat_id']).'
			</select>
		</p>
		<p>
			<label><b>'._lang('Заголовок').' *</b></label> 
			<input type="text" class="input-xlarge" name="swEdit[title]" value="'.$cval['sw_title'].'"/>
		</p>
		<p>
			<label><b>'._lang('Cсылка на архив').' *</b></label> 
			<input type="text" class="input-xlarge" name="swEdit[url]" value="'.$cval['sw_url'].'"/><br/>
			'._lang('Загрузить новый файл').'
			<input type="file" class="input-xlarge" name="file_url" accept="application/x-compressed, application/x-zip-compressed"/>
			<input type="hidden" class="input-xlarge" name="old_file_url" value="'.$cval['sw_url'].'"/>
			<input type="checkbox" class="input-xlarge" name="url_del" value="'.$cval['sw_url'].'"/> - '._lang('Удалить текущий файл').'
		</p>
		<p>
			<label><b>'._lang('Версия').'</b></label> 
			<input type="text" class="input-xlarge" name="swEdit[version]" value="'.$cval['sw_version'].'"/>
		</p>
		
		<div class="clear"></div>
		<br/><br/>
		<input class="btn btn-primary" type="submit" value="'._lang('Сохранить').'" name="swNewsubmit"/>
	</form>
	');
}else{
	$PData->redirect('','404');
}