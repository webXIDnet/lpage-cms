<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Продажу програмного забезпечення
 *  
***/

function LicenseKey($get){
	
	parse_str($get);
	
	if(!empty($_POST['payment_value']) && is_array($_POST['payment_value']) && !empty($sw_id)){
		
		global $sql;
		$text = '';
		
		//Генерація ліцензійного ключа
		$key = _run_filter('Software_LicenseKey', _getRandStr(10).'-'._getRandStr(10));
		
		$cval = $sql->query("
			SELECT l_key
			FROM _sw_licenses 
			WHERE l_key='"._protect($key)."'
		",'value');
				
		while($cval){
			$key = _run_filter('Software_LicenseKey', _getRandStr(10).'-'._getRandStr(10));
			$cval = $sql->query("
				SELECT l_key
				FROM _sw_licenses 
				WHERE l_key='"._protect($key)."'
			",'value');
		}
		
		//Отримання даних про програмне забезпечення
		$cval = $sql->query(
			"SELECT sw_version
			FROM _software
			WHERE sw_id='"._protect($sw_id)."'
		",'value');
		
		if(isset($cval) && is_array($cval)){
			
			if(empty($duration))$duration=365;
			
			$create_date = strtotime(date('Y-m-d'));
			$end_date = strtotime(date('Y-m-d'))+$duration*60*60*24;
			
			//Запис в базу ліцензійного ключа
			$sql->insert('_sw_licenses',array(
					'l_key' => $key,
					'l_sw_id' => $sw_id,
					'l_cl_id' => $_POST['payment_value']['cl_id'],
					'l_o_id' => $_POST['payment_value']['o_id'],
					'l_version' => $cval['sw_version'],
					'l_create_date' => $create_date,
					'l_end_date' => $end_date
				)
			);
			
			$text = '<b>'._lang('Лицензионный ключ').':</b> '.$key.'<br/><br/>
			<b>'._lang('Ключ будет действителен до').':</b> '.date('d.m.Y',$end_date).'';
		}else{
			global $meta;
			$text = '<b>'._lang('Ключ не был сгенерирован. Обратитесь в службу поддержки').':</b> '.$meta->val('contact_email').'';
		}
		
		return $text;
	}
}