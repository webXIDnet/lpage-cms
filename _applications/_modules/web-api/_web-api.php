<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Веб API
 *  
***/ 


if(PAGE_TYPE=='admin'){
	_add_filter('meta_datas_SQL_query','LPage_LKey_meta_datas_SQL_query');
}

if(isset($_GET['api'])){
	_add_action('ajax_init','web_api_ajax_init');
}

function LPage_LKey_meta_datas_SQL_query($result){
	if(!empty($result))$result .= ' OR ';
	$result .= " md_type='LPage_LKey' ";
	return $result;
}

function web_api_ajax_init(){
	
	if(!empty($_GET['tab'])){
		
		global 	$sqlTPL,
				$user,
				$sql,
				$PData;
				
		$text = '';
		
		switch($_GET['tab']){
			case'support':
				
				$text ='
					<p>
						<label>
							'._lang('Email').':<br/>
							<input type="email" name="email" value="'.@$_POST['email'].'" required/>
						</label>
					</p>
					<p>
						<label>
							'._lang('Тема письма').':<br/>
							<input type="text" name="subject" value="'.@$_POST['subject'].'" required/>
						</label>
					</p>
					<p>
						<label>
							'._lang('Сообщение').':<br/>
							<textarea style="height: 100px;" class="input" name="text" required>'.@$_POST['text'].'</textarea>
						</label>
					</p>';
					
				
				if(isset($_POST['ordersubmit'])){
					if(empty($_POST['subject']) || empty($_POST['text']) || empty($_POST['email'])){
						
						$text = 
						'<form method="POST" action="'.PAGE_URL.'" >'.$text.
							'<input type="hidden" name="version" value="'.$_GET['v'].'"/>
							<input type="hidden" name="site" value="'.@$_SERVER['HTTP_REFERER'].'"/>
							<input type="hidden" name="lang" value="'.$_GET['lang'].'"/>
							
							<input type="submit" name="ordersubmit" value="'._lang('Отправить').'"/>
						</form>';
						
						echo '<b>'._lang('Нужно заполнить все поля').'</b>'.$text;
						
					}else{
						
						global 	$meta;
							
						if(_checkEmail($meta->val('contact_email'))){
							$text ='
								<p>
									<b>'._lang('Тема письма').':</b><br/>
									'.@$_POST['subject'].'
								</p>
								<p>
									<b>'._lang('Сообщение').':</b><br/>
									'.@$_POST['text'].'
								</p>
								<p>
									<b>'._lang('Email').':</b><br/>
									'.@$_POST['email'].'
								</p>
								<p>
									<b>'._lang('Сайт').':</b><br/>
									'.@$_POST['site'].'
								</p>
								<p>
									<b>'._lang('Версия движка').':</b><br/>
									'.@$_POST['version'].'
								</p>
								<p>
									<b>'._lang('Язык').':</b><br/>
									'.@$_POST['lang'].'
								</p>';
							
							_eMail($meta->val('contact_email'), _lang('Обращение в тех. поддержку'), $text);//*/
							echo 
								'<h1>'._lang('Данные успешно отправлены. Спасибо за обращение.').'</h1>
								<p>'._lang('Мы свяжемся с Вами в ближайшее время.').'</p>
								<script type="text/javascript">window.setTimeout(\'window.close()\', 5000);</script>';
							
						}
					}
					UCode::phpExit();
				}
				
				$text = 
				'<form method="POST" action="'.PAGE_URL.'" target="_blank" >'.
					$text.
					'<input type="hidden" name="version" value="'.$_GET['v'].'"/>
					<input type="hidden" name="site" value="'.@$_SERVER['HTTP_REFERER'].'"/>
					<input type="hidden" name="lang" value="'.$_GET['lang'].'"/>
					
					<input type="submit" name="ordersubmit" value="'._lang('Отправить').'"/>
				</form>';
				
			break;
			case'license-agreement':
				$page = $sqlTPL->getPosts( 'page', $_GET['tab'], 'value' );
				if(!empty($page)){
					$text = $page['p_text'];
				}
			break;
			case'updates':
				$is_cl = TRUE;
				//перевірка наявності реферального посилання - користувач має вводити ключ лише через адмінку
				
				
				if(isset($_POST['usersubmit'])){
					$cl_site = str_replace('admin/information', '', dirname(@$_SERVER['HTTP_REFERER']) );
					
					if(empty($_POST['site']) && !_checkURL($cl_site) || $cl_site=='localhost' ){
						$text = '<b style="color:red;">'._lang('Ключ можно вводить только в АдминПанеле вашего сайта.').'</b>'.''; break;
					}
					
					$cval = $sql->query(
						"SELECT l_site
						FROM _sw_licenses INNER JOIN _clients ON cl_id = l_cl_id
						WHERE l_key = '"._protect($_POST['license_key'])."'",
						'value'
					);
					
					if(is_array($cval)){
						
						$array = array();
						
						if(!empty($cval['l_site'])){
							if(!stristr($cval['l_site'],'|'.$cl_site.'|')){
								$array = array(
									'l_site'=>$cval['l_site'].$cl_site.'|'
								);
							}
						}else{
							$array = array(
								'l_site'=>'|'.$cl_site.'|'
							);
							
						}
						
						if(!empty($array))
							$sql->update('_sw_licenses',"l_key='"._protect($_POST['license_key'])."'",$array);
						
						$PData->redirect($_POST['site']);
						
					}else{
						$text = '<b style="color:red;">'._lang('Вы ввели неверный лицензионный ключ.').'</b>'.'
							<b>'._lang('Введите лицензионный ключ').'</b><br/>
							<form method="POST" action="'.PAGE_URL.'">
								<input type="text" name="license_key" value=""/>
								<input type="hidden" name="version" value="'.$_POST['version'].'"/>
								<input type="hidden" name="site" value="'.$_POST['site'].'"/>
								<input type="hidden" name="lang" value="'.$_POST['lang'].'"/>
								
								<input type="submit" name="usersubmit" value="'._lang('Отправить').'"/>
							</form>
						
						';
					}
					
				}else{//перевірка клієнта за адресою сайту
				
					$cl_site = str_replace('admin/information', '', dirname(@$_SERVER['HTTP_REFERER']) );
					
				//	print_r($host);
				//	$cl_site = @$host['host'];
					
					if(!empty($cl_site) && _checkURL($cl_site) || $cl_site=='localhost' ){//отримання даних про клієнта, який вже ввів  ліцензіцний ключ в адмінці
						$cval = $sql->query(
							"SELECT l_key, cl_fullname, l_version,sw_version
							FROM 
								_software INNER JOIN _sw_licenses ON sw_id = l_sw_id 
								INNER JOIN _clients ON cl_id = l_cl_id
							WHERE l_site LIKE '%|"._protect($cl_site)."|%'
							ORDER BY l_end_date DESC
							",
							'value'
						);
						
						if(is_array($cval)){
							if( $cval['sw_version']==$_GET['v']){
								
								$text = _lang('Вы пользуетесь последней версией').' LPageCMS v'.$cval['l_version'];
								
							}else{
								$text = '<h3>'._lang('Доступно обновление').'</h3>'.
								_lang('Вы можете обновить LPageCMS до версии v').$cval['sw_version'].
								'<form method="POST">
									<input type="hidden" name="lk" value="'._strCode($cval['l_key']).'"/>
									<input type="submit" name="update" value="'._lang('Обновить').'"/>
								</form>';
							}
							
						}else{
							$text = '
							<b>'._lang('Введите лицензионный ключ').'</b><br/>
							<form method="POST" action="'.PAGE_URL.'">
								<input type="text" name="license_key" value=""/>
								<input type="hidden" name="version" value="'.$_GET['v'].'"/>
								<input type="hidden" name="site" value="'.$cl_site.'admin/information/?tab=updates"/>
								<input type="hidden" name="lang" value="'.$_GET['lang'].'"/>
								
								<input type="submit" name="usersubmit" value="'._lang('Отправить').'"/>
							</form>';
						}
					}else{
						$text = '<b style="color:red;">'._lang('Ключ можно вводить только в АдминПанеле вашего сайта.').'</b>'.'';
					}
				}
			break;
			
			case'new-version': // /api.js?tab=new-version&v=1.05
				
				if(!empty($_GET['lk'])){//отримання даних про клієнта, який вже ввів  ліцензіцний ключ в адмінці
					
					$cval = $sql->query(
						"SELECT l_key,sw_url,sw_version
						FROM 
							_software INNER JOIN _sw_licenses ON sw_id = l_sw_id
						WHERE l_key = '"._protect(_strCode($_GET['lk'],FALSE))."'
						ORDER BY l_end_date DESC
						",
						'value'
					);
					
					if(is_array($cval)){
						if($cval['sw_version']>$_GET['v']){

							Download($cval['sw_url'],'LPageCMS_update.zip');
							
							UCode::phpExit();
						}	
					}					 
					UCode::phpExit();
				}else{
					
				}
					
				
			break;
		}
		
		$text = str_replace("'","\'",$text);		
		$text = str_replace("\t",'',$text);
		$text = str_replace("\r",'',$text);
		$text = str_replace("\n",'\');document.writeln(\'',$text);
		$text = str_replace("document.writeln('');",'',$text);
		
		echo 'document.writeln(\''.$text.'\');';
	}
	UCode::phpExit();
}

function Download($file,$name){
        if (!_checkURL($file)){
			header ("HTTP/1.0 404 Not Found");        
        } 
 
        $fd = @fopen($file, "rb"); 

        if (!$fd){
            header ("HTTP/1.0 403 Forbidden"); 
            exit; 
        } 

        header("HTTP/1.1 200 OK"); 

        header("Content-Disposition: attachment; filename=".$name); 
         
        header("Content-Type: application/downloads"); 

		while(!feof($fd)){
            $content = fread($fd, 4096); 
        	print($content); 
        }
};