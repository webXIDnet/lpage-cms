<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Admin Panel
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

define('LPAGE_CMS_VERSION', 'LPage CMS v1.2.8');

require_once __DIR__ . '/ap_fn.php';
require_once __DIR__ . '/ap_class.php';

if ($components = glob(__DIR__ . '/com_*/')) {
	foreach($components AS $component){
		if(file_exists($component . '_main.php')){
			require_once $component . '_main.php';
		}
	}
}

if (PAGE_TYPE=='admin') {
	_add_action('defined_modules','admin_defined_modules', 1); #Check login
	_add_action('init_page', 'adminPanel_init',1);
}

function admin_defined_modules()
{
	global $admin;
	$admin = new getAdminPamel();
}


function adminPanel_init()
{

	global 	$user,
			$admin,
			$PData,
			$sql;

	/** Підгрузка теми адмінки **/
	$PData->setTPL('tpl_default','admin-panel', TRUE);

	if($user->rights==0){

		if (!isset($_GET['admin']) || empty($_GET['admin'])) {
			$PData->content($PData->getModTPL('login', 'admin-panel', '', TRUE));
		} elseif ($_GET['admin']=='sign-up') {
			$PData->content($PData->getModTPL('sign-up', 'admin-panel', '', TRUE));
		} elseif($_GET['admin']=='lost-pass') {

			if(isset($_POST['repass'])){
				if(_checkEmail($_POST['repass'])){
					$cval = $sql->query(
						"SELECT u_id
						FROM _users
						WHERE u_email='"._protect($_POST['repass'])."'
						",
						'value'
					);
					if(is_array($cval)){
						$pass = _getRandStr(8);
						$sql->update('_users',"u_id='"._protect($cval['u_id'])."'",array('u_pass'=>md5($pass)));

						_eMail($_POST['repass'],_lang('Востановление пароля'),
							_lang('Ваш новый пароль').': '.$pass.'<br/><br/>
							'._lang('Вы можете поменять пароль на удобный для вас здесь').': <a href="'.getURL('admin','profile').'">'._lang('Ссылка').'</a>
							'
						);
						$PData->content('На вашу почту отправлено новый пароль. Поменяйте его сразу после первого входа.','message',TRUE);

					}else{
						$PData->content('Не верный Логин','message');
					}
				}else{
					$PData->content('<b>'._lang('Неверный формат почты').'</b><br/>'._lang('Если ваш Логин не являеться Email\'ом, обратитесь за помощью к администратору сайта'),'message');
				}
			} else {
				$PData->redirect(HOMEPAGE, '404');
			}

			$PData->content($PData->getModTPL('last-pass', 'admin-panel', '', TRUE));
		}

		$PData->setTPL('tpl_mini','admin-panel', TRUE);
		$PData->connectTPL();
	}else{
		if($user->rights<2){
			$PData->redirect();
		}
	}
}


_add_action('init_page', 'adminPanel_controller', 2);

function adminPanel_controller()
{

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL;

	switch(@$_GET['admin']){
			
		/** Кабінет **/
		
		case'modules':
		
			require_once __DIR__.'/apps_admin/system/a_modules.php';
		
		break;
		
		case'information':
		
			require_once __DIR__.'/apps_admin/system/a_information.php';
		
		break;
		
		default:
		
			if(_run_action('adminPanel_controller')){
				break;
			}
		
			require_once __DIR__.'/apps_admin/a_homepage.php';
		

		
	}


	/** SideBar **/
	$PData->content(_run_filter('adminPanel_sidebar_menu',''), 'sidebar');


}


_add_filter('admin-panel_admin-panel_mod_settings','ap_adminpanel_mod_settings');	//налаштування в розбілі модулів

function ap_adminpanel_mod_settings($result, $set)
{
	if(isset($_POST['runSystemTestings'])){
		$_SESSION['testing'] = 1;
		global $PData;
		$PData->redirect(PAGE_URL);
	}

	$result['title'] .=
		' <li  class="active">
 <a href="#tab_u_testing" aria-controls="home" role="tab" data-toggle="tab">'._lang('Тестирование').'</a></li> ';

	$result['text'] .=
		'
<div role="tabpanel" class="tab-pane fade in active" id="tab_u_testing">
			<input type="submit" class="button" value="'._lang('Запустить тестирование системы').'" name="runSystemTestings">
		</div>';

	return $result;
}