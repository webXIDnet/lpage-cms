<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Admin Panel
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

class getAdminPamel{
	public $lang;//поточна мова контенту в адмінці

	public function __construct(){

		global  $user;

		$this->lang = $user->lang;

        if(empty($user->lang)) $this->lang = 'ru';

		$m=date('m')+1;
		$Y=date('Y');

		if($m==13){
			$m='01';
			$Y=$Y+1;
		}

		if(!empty($_GET['adminLang'])){

			global $PData;

			$this->lang = $_GET['adminLang'];

			setcookie('adminLang', $this->lang, strtotime($Y.'-'.$m.date('-d')),'/');

			$PData->redirect(str_replace('adminLang='.$_COOKIE['adminLang'],'',PAGE_URL));

		}elseif(!empty($_COOKIE['adminLang'])){

			$this->lang = $_COOKIE['adminLang'];

		}else{
			setcookie('adminLang', $user->lang, strtotime($Y.'-'.$m.date('-d')),'/');
		}
	}

	public function SignUp(){
		return'<div class="row-fluid">
			    <div class="dialog">
			        <div class="block">
			            <p class="block-heading">Sign Up</p>
			            <div class="block-body">
			                <form>
			                    <label>First Name</label>
			                    <input type="text" class="span12">
			                    <label>Last Name</label>
			                    <input type="text" class="span12">
			                    <label>Email Address</label>
			                    <input type="text" class="span12">
			                    <label>Username</label>
			                    <input type="text" class="span12">
			                    <label>Password</label>
			                    <input type="password" class="span12">
			                    <button type="submit" class="btn btn-primary pull-right">Sign Up!</button>
                    			<!--label class="remember-me"><input type="checkbox"> I agree with the <a href="terms-and-conditions.html">Terms and Conditions</a></label-->
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <!--p><a href="privacy-policy.html">Privacy Policy</a></p-->
    </div>
</div><style>body{padding:0;}</style>';
	}

	public function LostPass(){
        global $PData;
        return'

		<div id="wrapper">
	        <div id="zamok"></div>
	        <form action="" method="POST">
	        <div id="autbg">
	            <div id="autT">'._lang('Востановление пароля').'</div>
	            <p class="autT">'.@$PData->position['message'].'</p>
                <div id="autIcon"></div><input name="repass" type="text" placeholder="'._lang('Email / Логин').'" id="login" class="span12"></input>
                <input type="submit"  value="' . _lang('Отправить') . '" id="autButton">

	        </div>

	        </form>
	        <div class="additionLinks">
	        	<a href="'.getURL('admin','').'">'._lang('Авторизироваться').'</a>
	        </div>
	    </div>
	    <style>body{padding:0;}</style>';
	}

	public function Login(){
        global $PData;
        return'
		<div id="wrapper">
	        <div id="zamok">
		        <form action="" method="POST">
		        <div id="autbg">
		            <div id="autT">'._lang('Авторизация').'</div>
		            <p class="autT">'.@$PData->position['message'].'</p>
		            <div id="autIcon"></div><input type="text" id="login" value="" placeholder="'._lang('Email / Логин').'" name="login[email]">
		            <div class="clear"></div>
		            <div id="keyIcon"></div><input type="password" id="login" style="margin-top: 0px;" value="" placeholder="'._lang('Password').'" name="login[password]">
		            <input type="submit" value="'._lang('Войти').'" id="autButton">
		        </div>

		        </form>
		        <div class="additionLinks">
		        	<a href="'.getURL('admin','lost-pass').'">'._lang('Востановить пароль').'</a>
		        </div>
	        </div>
	    </div>
		<style>body{padding:0;}</style>
		';
	}

	public function getAdminTheme($theme='big'){

		global 	$PData,
				$user;

		switch($theme){

			case'big':

				$geo = '';
				$url = PAGE_URL;

				if(stristr($url,'?')) $url .= '&';
				else $url .= '?';

				foreach(getLanguage() AS $key=>$val){

					$geo .= '<li><a href="'.$url.'adminLang='.$key.'">'.$val.'</a></li>';

				}

				$version = explode('v',LPAGE_CMS_VERSION);
				$version = $version[count($version)-1];
				$PData->content='
	<div id="header">
		<ul id="cmsVersion" class="menu">
			<li class="icons icon_logo" title="'._lang('Перейти на сайт').'">
				<a href="//lpagecms.com/" target="_blank">
					'.LPAGE_CMS_VERSION.'
				</a>
			</li>
		</ul>
        <ul id="system-menu" class="menu">
        	<li class="icons icon_system"	>
				<a>
					'._lang('Система').'
				</a>
				<ul class="submenu">
					'.
					u_ifRights(
						array(6,100),
						'<li class="icons icon_mod">
							<a href="'.getURL('admin','modules').'">
								'._lang('Модули').'</a>
						</li>'
					)
					.'
					<li class="icons icon_store">
						<a href="'.getURL('admin','information','tab=modules').'">
							'._lang('Модуль-банк').'</a>
					</li>

					<li class="icons icon_update">
						<a href="'.getURL('admin','information','tab=updates').'">
							'._lang('Обновления').'</a>
					</li>
					<li class="icons icon_support">
						<a href="'.getURL('admin','information','tab=support').'">
							'._lang('Тех. поддержка').'</a>
					</li>
					<li class="icons icon_info">
						<a href="'.getURL('admin','information').'">
							'._lang('Лицензия').'</a>
					</li>
				</ul>
			</li>
		</ul>

        <ul id="bgNav1" class="menu user_menu">
            <li><a href="'.getURL('admin','profile').'" title="'._lang('Личные данные').'" class="acount"></a></li>
            <li><a href="'.getURL('admin','','logout').'" title="'._lang('Выйти').'" class="logOut"></a></li>
        </ul>
    </div>
	<div id="sidebar">
		'.@$PData->position['sidebar'].'
		<!--script type="text/javascript" src="//lpage.webxid.net/api.js?v='.$version.'&lang='.$user->lang.'"></script-->
	</div>
	<div id="content">
		<h1>'._lang(@$PData->position['title']).'</h1>
		'.@$PData->content.'
	</div>
				';
			break;

			case'small':
				$PData->content='
				<div id="content">
			        <div id="bgUR"></div>
			        <div id="bgTC"></div>
			        <div id="bgTL"></div>
			        <div id="bgLC"></div>
			        <div id="bgBL"></div>
			        <div id="bgBC"></div>
			        <div id="bgBR"></div>
			        <div id="bgCL"></div>
			        <div id="bgRB"></div>

        			<!--<h1>'.@$PData->position['title'].'</h1>-->
        			'.@$PData->content.'
				</div>
				<style>
				#content{
					margin: 30px auto;
					width:500px;
				}
				</style>
				';
			break;


		}

		//require_once __DIR__ . '/ap_theme.php';
		$PData->setTPL('tpl_mini','admin-panel');
	}


	public function getWIZIWIG($name, $value='', $class='elm1', $type='text'){



		switch($type){
			case'code':

				if(!defined('CODEMIRROR')){

					define('CODEMIRROR','TRUE');
					require_once CORE_FOLDER . '../plugins/code-mirror/index.php';

				}

				return'
					<textarea id="'.$class.'" class="'.$class.'" name="'.$name.'" rows="3" cols="10" style="width:80%;">'.$value.'</textarea>
					<script type="text/javascript">

						var editor = CodeMirror.fromTextArea(document.getElementById("'.$class.'"), {
					      lineNumbers: true,
					      mode: "text/html",
					      matchBrackets: true
					    });

					</script>';

			break;

			default:
				$text = '';
				if(!defined('EDITOR')){

					define('EDITOR','TRUE');
					require_once CORE_FOLDER.'../plugins/summernote/index.php';
				}

				return $text.'
					<textarea id="'.$class.'" class="'.$class.'" name="'.$name.'" rows="3" cols="10" style="width:80%">'.$value.'</textarea>
					<script type="text/javascript">
						$(".'.$class.'").summernote({
							lang: "ru-RU",
							height: 200
						});

					</script>';
		}

	}

	public function getArticle($str='')
	{
		if(empty($str)){

			global $sql;

			$article = $str = _getRandStr();

			$i=1;

			$cval = $sql->query("SELECT `w_id` FROM `_wares` WHERE w_article='"._protect($article)."'",'value');

			while($cval){

				$str=$article.'-'._getRandStr($i);
				$i++;
				$cval = $sql->query("SELECT `w_id` FROM `_wares` WHERE w_article='"._protect($str)."'",'value');

			}

		}

		return $str;
	}
}


