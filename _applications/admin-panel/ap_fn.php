<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Admin Panel
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */


function ap_mod_editing_form($mod){

	global $meta;
	$mod = strtolower($mod);

	$text = _run_filter($mod.'_admin-panel_mod_settings',array('title'=>'','text'=>''),$meta->val('apSet_'.$mod,'mod_option'));

	if(is_array($text) && (!empty($text['text']) || !empty($text['title']))){
			return '
			<div class="modal fade module-modal' . $mod . '" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title">'.$mod.'</h4>
                        </div>
                        <div class="modal-body">
                            <div role="tabpanel" class="tab-box">
                            <ul class="nav nav-tabs" role="tablist">
                                '.$text['title'].'
                              </ul>
                               <div class="tab-content">
                                    <form method="POST" role="form">
                                        '.$text['text'].'
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
		    </div>
';
	}
	return '';
}

function getURL($key,$val=FALSE,$get='')
{#функція формує УРЛ

  switch($key){

    case'homepage':
      return HOMEPAGE.(!empty($get)?'?'.$get:'');
    break;

    case'post':

      if(!empty($get)){

        parse_str($get,$get);

      }

      $c = '';
      if (isset($get['category'])){
        $c=@$get['category'];
      }


      if(isset($get['category'])){

        unset($get['category']);

      }else{
        $c = 'category';
      }

      if (is_array($get)) {
        $get=@http_build_query($get);
      }

      return HOMEPAGE.$c.'/'.$val.'.html'.(!empty($get)?'?'.$get:'');

    break;

    case'category':

      if(!empty($get)){
        parse_str($get,$get);
      }

      $get=@http_build_query($get);

      return HOMEPAGE.$key.'/'.(!empty($val)?$val.'/':'').(!empty($get)?'?'.$get:'');

    break;

    case'page':

      !empty($get)?$get='?'.$get:'';
      return HOMEPAGE.'p/'.$val.'/'.$get;

    break;

    case'admin':
      if(!empty($get))$get='?'.$get;
      if($val)$val.='/';
      return HOMEPAGE.'admin/'.$val.$get;
    break;
    case'orders':

      if(!empty($get))$get='?'.$get;
      if($val)$val.='/';
      return HOMEPAGE.$key.'/'.$val.$get;

    break;

    case'crop':

      if($get!='')$get='&'.$get;
      return HOMEPAGE.'crop/?url='.$val.$get;

    break;

    case'ajax':

      if($val=='sendOrderForm'){

        if(!empty($get)) parse_str($get,$get);

        $id=@$get['id'];

        if(isset($get['id'])) unset($get['id']);

        $get=@http_build_query($get);

        if(!empty($id))$id='='.$id;

        if(!empty($get))$get='&'.$get;

        return HOMEPAGE.'ajax/?'.$val.@$id.$get;

      }else{
        if(!empty($get) && !empty($val))$get='&'.$get;
        return HOMEPAGE.'ajax/?'.$val.$get;
      }

    break;

    default:

      if($url = _run_action( 'getURL_rule', array('key' => $key,'val' => $val,'get' => $get) )){
        return $url;
      }else{
        if($val!==FALSE)$val='='.$val;
        else $val='';
        if(!empty($get))$get='&'.$get;
        return HOMEPAGE.'?'.$key.$val.$get;
      }

  }
}