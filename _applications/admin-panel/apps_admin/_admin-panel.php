<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Модуль Адміністративної Панелі
 *  
***/

define('LPAGE_CMS_VERSION', 'LPageCMS v1.2.7');

require_once __DIR__ . '/ap_fn.php';
require_once __DIR__ . '/ap_class.php';


$admin = new getAdminPamel;

$admin->lang();

function adminPanel_init(){
	
	global 	$user,
			$admin,
			$PData,
			$sql;
	
	/** Підгрузка теми адмінки **/
	$PData->setTPL('tpl_default','admin-panel');

	if($user->rights==0){
	
		if(@$_GET['admin']=='sign-up'){
		
			$PData->content($PData->getModTPL('sign-up', 'admin-panel'));
		
		}elseif(@$_GET['admin']=='lost-pass'){
			
			if(isset($_POST['repass'])){
				if(_checkEmail($_POST['repass'])){
					$cval = $sql->query(
						"SELECT u_id
						FROM _users
						WHERE u_email='"._protect($_POST['repass'])."'
						",
						'value'
					);
					if(is_array($cval)){
						$pass = _getRandStr(8);
						$sql->update('_users',"u_id='"._protect($cval['u_id'])."'",array('u_pass'=>md5($pass)));
						
						_eMail($_POST['repass'],_lang('Востановление пароля'),
							_lang('Ваш новый пароль').': '.$pass.'<br/><br/>
							'._lang('Вы можете поменять пароль на удобный для вас здесь').': <a href="'.getURL('admin','profile').'">'._lang('Ссылка').'</a>
							'
						);
						$PData->content('На вашу почту отправлено новый пароль. Поменяйте его сразу после первого входа.','message',TRUE);
						
					}else{
						$PData->content('Не верный Логин','message');
					}
				}else{
					$PData->content('<b>'._lang('Неверный формат почты').'</b><br/>'._lang('Если ваш Логин не являеться Email\'ом, обратитесь за помощью к администратору сайта'),'message');
				}
			}
			
			$PData->content($PData->getModTPL('last-pass', 'admin-panel'));
		
		}else{
			
			$PData->content($PData->getModTPL('login', 'admin-panel'));
			
		}
		
		
		$PData->setTPL('tpl_mini','admin-panel');
		$PData->connectTPL();		
	
	}else{
		
		if($user->rights<2)$PData->redirect();
		
	}
}

_add_action('init_processes', 'adminPanel_init',1);
_add_action('init_processes', 'adminPanel_controller',2);
_add_filter('admin-panel_admin-panel_mod_settings','ap_adminpanel_mod_settings');	//налаштування в розбілі модулів