<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin 
 * 
 * Тут формується HTML код сторінки
 * 
***/

define('APP_ADMIN_FOLDER', __DIR__ . '/');


/** Роутер адмінки **/

switch(@$_GET['admin']){
		
	/** Кабінет **/
	
	case'modules':
	
		require_once APP_ADMIN_FOLDER.'system/a_modules.php';
	
	break;
	
	case'information':
	
		require_once APP_ADMIN_FOLDER.'system/a_information.php';
	
	break;
	
	default:
	
		if(_run_action('adminPanel_controller')){
			break;
		}
	
		require_once APP_ADMIN_FOLDER.'a_homepage.php';
	

	
}

/** Формування лівого меню **/


/** Прапва колонка **/
$PData->content(_run_filter('adminPanel_sidebar_menu',''), 'sidebar');
