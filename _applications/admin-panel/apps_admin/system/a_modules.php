<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
 ***/

global $meta;

if (isset($_POST['formsubmit-edit']) && ((isset($_POST['modSet']) && is_array($_POST['modSet'])) || (isset($_POST['newModSet']) && is_array($_POST['newModSet'])))) {

    /*
     * Save pairs key=>value
     * 0=value
     * 1=key
     */

    if (is_array($_POST['newModSet'])) {
        foreach ($_POST['newModSet'] AS $mod => $array) {

            if (!empty($_POST['newModSet'][$mod])) {
                foreach ($_POST['newModSet'][$mod] AS $key_com => $val) {

                    if (!empty($val[0]) && !empty($val[1])) {

                        $_POST['modSet'][$mod][$key_com][$val[1]] = $val[0];

                    } elseif (!empty($val[0]) || !empty($val[1])) {

                        $PData->content(_lang('Параметр') . ' <b>' . $key_com . '</b> ' . _lang('не сохратнен') . '!', 'message');

                    }

                }
                $PData->content('Запись сохранена', 'message', TRUE);
                unset($_POST['newModSet']);
            }
        }
    }

    if (isset($_POST['delApSet']) && is_array($_POST['delApSet'])) {
        foreach ($_POST['delApSet'] AS $mod => $array) {

            if (is_array($_POST['delApSet'][$mod])) {
                foreach ($_POST['delApSet'][$mod] AS $key_com => $val) {

                    foreach ($val AS $key => $v) {

                        unset($_POST['modSet'][$mod][$key_com][$key]);
                        $PData->content('Запись удалена', 'message', TRUE);

                    }
                }
            }
        }
    }

    if (isset($_POST['modSet']) && is_array($_POST['modSet'])) {
        foreach ($_POST['modSet'] AS $mod => $array) {
            foreach ($_POST['modSet'][$mod] AS $key => $val) {
                ksort($_POST['modSet'][$mod][$key]);
            }

            if (!$meta->add_update($mod, $_POST['modSet'][$mod], 'mod_option')) {
                $PData->content(_lang('Параметр') . ' <b>' . $mod . '</b> ' . _lang('не сохратнен') . '!', 'message');
            }
            $PData->content('Запись сохранена', 'message', TRUE);
            unset($_POST['modSet']);
        }
    }
}

$text = '';


foreach ($PData->GetSettings('set_modules') AS $key => $val) {

    $class = 'greyText';
    $page_type = $com_ = '';

    if (!empty($val) && is_array($val)) {
        foreach ($val AS $type) {
            if (!empty($page_type)) $page_type .= '<br/>';
            $page_type .= '- ' . $type;
        }
    } else {
        foreach ($PData->GetSettings('set_page_types') AS $type => $val) {
            if (!empty($page_type)) $page_type .= '<br/>';
            $page_type .= '- ' . $type;
        }

    }

    if (file_exists(APP_FOLDER . '_modules/' . $key . '/_' . $key . '.php')) {
        $class = 'blackText';
        if ($components = glob(APP_FOLDER . '_modules/' . $key . '/com_*/')) {
            foreach ($components AS $component) {
                if (file_exists($component . '_main.php')) {
                    $component = preg_replace("/(.*)com_/i", '', $component);
                    if (!empty($com_)) $com_ .= '<br/>';


                    $com_ .= '- ' . ucfirst(substr($component, 0, strlen($component) - 1));
                }
            }
        }
        require_once APP_FOLDER . '_modules/' . $key . '/_' . $key . '.php';
    }

    $text .=
        '<tr class="' . $class . '">
			<td  class="td-link" data-toggle="modal" data-target=".module-modal' . $key . '">
				' . $key . '
			</td>
			<td class="td-link" data-toggle="modal" data-target=".module-modal' . $key . '">
				' . $page_type . '
			</td>
			<td class="td-link" data-toggle="modal" data-target=".module-modal' . $key . '">
				' . $com_ . '
			</td>
		</tr>
		<tr>
		<form enctype="multipart/form-data" method="POST">
		' .
        ap_mod_editing_form($key) .
        '

        </form>
		</tr>
		';
}


$text = '

	<table id="tableEdit" class="table">
		<tbody>
			<tr>
				<th id="head-tableList" class="head head1 tableList1">' . _lang('Название модуля') . '</th>
				<th id="head-tableList" class="head head2 tableList2">' . _lang('Типы страниц') . '</th>
				<th id="head-tableList" class="head head3 tableList3">' . _lang('Компоненты') . '</th>
			</tr>
			' . $text . '
		</tbody>
	</table>

';

$PData->content('Установленные модули', 'title');

$PData->content($text);