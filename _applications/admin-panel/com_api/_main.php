<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Main API module file
 */

require_once __DIR__ .'/api_class.php';

if (PAGE_TYPE=='api') {
	global $api;

	$api = new api();

	_add_action('init_page','api_init_page');
}


/**
 * API initialization, init API object
 */
function api_init_page() {
	global $api;

	$api->init_api();
}





