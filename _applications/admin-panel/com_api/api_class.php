<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file

/**
 * Main API class
 */
class api
{
	# Array of allowed modules and methods or functions
	private $allowed = array();

	# Array with names of class, method and vars or function and vars
	private $vars;

	/**
	 * Creating object
	 */
	function __construct()
	{
		$this->vars = $this->get_method_from_url();
	}

	/**
	 * Class initialization and execute function
	 */
	public function init_api()
	{
		global $PData;

		foreach($PData->GetSettings('set_modules') AS $key => $val){
			if (in_array(PAGE_TYPE, $val) || empty($val) ) {
				if (!file_exists(APP_FOLDER . '_modules/' . $key . '/' . $key . '_api.php')) {
					$key = strtolower($key);
				}

				if (file_exists(APP_FOLDER . '_modules/' . $key . '/' . $key . '_api.php')) {
					if ($components = glob(APP_FOLDER . '_modules/' . $key . '/com_*/')) {
						foreach($components AS $component){
							if(file_exists($component . '_api.php')){
								require_once $component . '_api.php';
							}
						}
					}
					require_once APP_FOLDER . '_modules/' . $key . '/' . $key . '_api.php';
				}
			}
		}

		if ($this->vars[0] == '1') {
			$this->call_func($this->vars[1]);
		} else {
			$vars_0 = $vars_1 = '';

			if (isset($this->vars[0])) {
				$vars_0 = $this->vars[0];
			}

			if (isset($this->vars[1])) {
				$vars_1 = $this->vars[1];
			}

			$this->call_method($vars_0, $vars_1);
		}
	}

	/**
	 * Execution function named $value if its allowed
	 * @param $value
	 */
	private function call_method($module = '', $method = '')
	{
		global $PData;
		$vars = '';

		if(isset($this->vars[2])) {
			$vars = $this->vars[2];
		}

		$errors = array();
		if ($module) {
			if (is_array($this->allowed) && array_key_exists($module, $this->allowed)) {
				if (in_array($method, $this->allowed[$module])) {
					$class_name = $module . '_api';

					if (class_exists($class_name)) {
						$obj = new $class_name();

						if (method_exists($obj, $method)) {
							$obj->$method($vars);
						} else {
							$errors[] = 'Method '.$method.' is not exists!';
						}
					} else {
						$errors[] = 'Class '.$class_name.' is not exists!';
					}
				} else {
					$errors[] = 'Method '.$method.' is not allowed in '.$module.'!';
				}
			} else {
				$errors[] = 'Module '.$module.' is not allowed!';
			}
		} else {
			if (method_exists($this, $method)) {
				$this->$method($vars);
			} else {
				$errors[] = 'Method '.$method.' is not exists!';
			}
		}

		if (count($errors) > 0) {
			echo implode('<br>',$errors).'<br>';
		}

		$PData->phpExit();
	}


	/**
	 * Execution function named $value if its allowed
	 * @param $value
	 */
	private function call_func($value)
	{
		if (!isset($this->allowed['function']) || !is_array($this->allowed['function']) || empty($this->allowed['function'])){
			global $PData;
			echo 'Function '.$this->vars[1].' is not allowed!';
			$PData->phpExit();
		}

		$vars = '';

		if (isset($this->vars[2])) {
			$vars = $this->vars[2];
		}

		if (in_array($value, $this->allowed['function'])) {
			if (function_exists($value)){
				$value($vars);
			}
		}
	}

	/**
	 * Checking module and method existing, if $add true - add
	 * @param string $module Module or Component name
	 * @param string $method Method name
	 * @param bool $add Adding flag
	 * @return bool
	 */
	public function in_allowed($module = '', $method = '')
	{
		$this->allowed[$module][] = $method;
		return TRUE;
	}

	/**
	 * Split an url to array with specialchars filtering from $_GET['api']
	 * @return array - array with values of class and method or function, what API should run
	 */
	private function get_method_from_url()
	{
		global $PData;

		$result = array();
		$url_vars = array_values(array_diff(preg_split('[/|\?]', $PData->_GET('api')), array('')));

		foreach ($url_vars as $item) {
			$result [] = htmlspecialchars($item);
		}

		return $result;
	}
}