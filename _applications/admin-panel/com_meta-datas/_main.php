<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модуль Підключення метатегів
 *
 * v1.1
 *
***/

require_once __DIR__ . '/md_class.php';

_add_action('defined_modules', 'metadatas_init',1);

function metadatas_init(){

	global $meta;

	$meta = new MetaDatas;

	if(PAGE_TYPE=='admin'){

		global $sql;

		$sql->db_table_installer('Meta-Datas','05.09.2014',
			array(
				'_meta_datas' => array(
					'md_id' => "`md_id` int(11) NOT NULL AUTO_INCREMENT",
					'md_key' => "`md_key` varchar(300) NOT NULL",
					'md_value' => "`md_value` text NOT NULL",
					'md_type' => "`md_type` varchar(20) NOT NULL DEFAULT 'option'",
					'CREATE' => "`md_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`md_id`)"
				)
			)
		);
	}


}

_add_action('init_done', 'metatags_protect');

function metatags_define($var){ # Опрацьовує строку і робить заміну HTML заготовок
	global $meta;

	metatags_protect();

	$res = "\n";
	if ($meta->title){ $res.='<title>'.$meta->title.'</title>'."\n"; }
	if ($meta->desc){ $res.='<meta name="description" content="'.$meta->desc.'" />'."\n"; }
	if ($meta->keys){ $res.='<meta name="keywords" content="'.$meta->keys.'" />'."\n"; }
	$res.="\n";

	return $res.$var;
}

_add_filter('LPage_lp_head', 'metatags_define');

function metatags_protect(){
	global $meta;

	$meta->title = str_replace('"','&quot;',$meta->title);
	$meta->title = str_replace("'",'&lsquo;',$meta->title);

	$meta->desc = str_replace('"','&quot;',$meta->desc);
	$meta->desc = str_replace("'",'&lsquo;',$meta->desc);

	$meta->keys = str_replace('"','&quot;',$meta->keys);
	$meta->keys = str_replace("'",'&lsquo;',$meta->keys);

}

if (PAGE_TYPE=='admin') {
	_add_filter('adminPanel_sidebar_menu', 'meta_datas_admin_sidebar_menu_elenemts',25);
}

function meta_datas_admin_sidebar_menu_elenemts($result)
{//модифікація меню адмін панелі в правій колонці
	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Элементы сайта'), //ім"я пункта меню
			'url' => FALSE, //посилання
			'submenu_filter' => 'meta_datas_admin_sidebar_menu_elenemts', //хук для підключення пунктів підменю
			'active_pages' => _run_filter('meta_datas_admin_sidebar_menu_elenemts_active_items', array()), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => TRUE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . @$menu;
}


if (PAGE_TYPE=='admin') {
	_add_filter('adminPanel_sidebar_menu', 'meta_datas_admin_sidebar_menu_settings',100);
	_add_action('adminPanel_controller', 'meta_datas_admin_controller');
}

function meta_datas_admin_sidebar_menu_settings($result)
{//модифікація меню адмін панелі в правій колонці
	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Настройки'), //ім"я пункта меню
			'url' => getURL('admin','settings'), //посилання
			'submenu_filter' => 'meta_datas_admin_sidebar_menu_settings', //хук для підключення пунктів підменю
			'active_pages' => _run_filter('meta_datas_admin_sidebar_menu_settings_active_items', array('settings')), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_item', 'admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . @$menu;

	return $result . u_ifRights(
		 array(100),
		'<div id="menu" class="'._getActive(@$_GET['admin'],'settings').'">
			<a href="'.getURL('admin','settings').'" id="menua">'._lang('Настройки').'</a>
		</div>'
	);
}

function meta_datas_admin_controller(){ //доповнення роутера AdminPanel'і

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL;

	switch(@$_GET['admin']){
		case'settings':
			require_once __DIR__ . '/a_admin_settings.php';
			return TRUE;
		break;

	}
}