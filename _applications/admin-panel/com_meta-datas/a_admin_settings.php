<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/

global $meta;

if(isset($_POST['metaEditsubmit'])){

	foreach($_POST['meta'] AS $key=>$val){

		$array = _print_r_reverse($val);
		if(is_array($array ))
			$val = $array;

		if(!$meta->add_update($key,$val))
			$PData->content(_lang('Параметр').' <b>'.$key.'</b> '._lang('не сохратнен').'!','message');

	}
	unset($_POST['meta']);

	$PData->content('Запись сохранена','message',TRUE);
}


$text = '';

$result = $meta->all_values();

if($result){
	foreach($result AS $key=>$val){

		if(is_array($val))$val =  print_r($val, true);
		$text .= '
				<p><b>'.$key.'</b></p>
				<textarea  type="text" name="meta['.$key.']">'._unprotect($val).'</textarea>
			';
	}
}

$PData->content('Настройки','title');

$PData->content('
<form class="list" method="POST">
	'.$text.'
	<input class="button" type="submit" value="'._lang('Сохранить').'" name="metaEditsubmit"/>
</form>');