<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модель Мета Даних
 *
 * v1.1
 *
***/

class MetaDatas{

	public $title = '';
	public $desc = '';
	public $keys = '';

	public $sliderMax = 0;

	private $all;

	public function __construct(){

		global 	$sql;
		$where = '';

		$where = _run_filter('meta_datas_SQL_query',$where);

		if(!empty($where )) $where = ' OR '.$where;

		$result = $sql->query("
			SELECT *
			FROM _meta_datas
			WHERE md_type='option' OR md_type='slider' OR md_type='mod_option' OR md_type='meta_tag' {$where}
		");

		foreach($result AS $cval){

			switch($cval['md_type']){

				case'meta_tag':
					$val = json_decode($cval['md_value'], TRUE);
					$this->all[$cval['md_type']][$cval['md_key']] = $val;

					if($cval['md_key']==_protect(PAGE_URL) && is_array($val)){

						if(!empty($val['title']))
							$this->title = $val['title'];

						if(!empty($val['desc']))
							$this->desc = $val['desc'];

						if(!empty($val['keys']))
							$this->keys = $val['keys'];

					}

				break;

				case'slider':
					$val = json_decode($cval['md_value'], TRUE);
					if(empty($val)){ $val = $cval['md_value']; }

					if ($cval['md_key']>$this->sliderMax){ $this->sliderMax = $cval['md_key']; }

					$i = 0;
					while(isset($val['slides'][$i]['title'])){
						$val['slides'][$i]['title'] = urldecode($val['slides'][$i]['title']);
						$i++;
					}

					$this->all[$cval['md_type']][$cval['md_key']] = $val;
				break;

				default:

					$val = json_decode($cval['md_value'], TRUE);

					if(empty($val)){
						$val = $cval['md_value'];
					}

					$this->all[$cval['md_type']][$cval['md_key']] = $val;

				break;
			}
		}
	}

	public function val($key, $type='option'){

		if(isset($this->all[$type][$key])) return $this->all[$type][$key];
		return FALSE;
	}

	public function all_values($type='option'){

		if(isset($this->all[$type])) return $this->all[$type];
		return FALSE;

	}

	public function change($key, $val, $type='option'){

		if(isset($this->all[$type][$key])) {
			$this->all[$type][$key]=$val;
			return $this->all[$type][$key];
		}
		return FALSE;
	}

	public function add_update($key, $value, $type='option'){

		global 	$sql;

		if(isset($this->all[$type][$key])){

			$this->all[$type][$key] = $value;

			if(is_array($value)) $value = json_encode($value,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

			$sql->update(
				'_meta_datas',
				"md_key='"._protect($key)."' AND md_type='"._protect($type)."'",
				array('md_value'=>$value)
			);

			return TRUE;

		}else{

			$this->all[$type][$key] = $value;

			if(is_array($value)) $value = json_encode($value,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

			$array = array(
				'key' => $key,
				'value' => $value,
				'type' => $type
			);

			if( $sql->insert('_meta_datas',$array,'md_') ) {

				return TRUE;
			}
		}

		return FALSE;
	}

	public function delete($key, $type='option'){


		global 	$sql;

		if(is_array($key)){

			$where = '';

			foreach($key AS $key=>$val){

				if(!isset($this->all[$type][$key]))continue;

				if(!empty($where)){
					$where .= ' OR ';
				}

				$where .= "md_key='"._protect($key)."'";
				unset($this->all[$type][$key]);
			}

			if(!empty($where)){

				$where = " ( $where ) AND md_type='"._protect($type)."'";

				$sql->delete('_meta_datas',$where);
			}
			return TRUE;

		}else{

			if(!isset($this->all[$type][$key]))return FALSE;

			$where = " md_key='"._protect($key)."' AND md_type='"._protect($type)."'";

			$sql->delete('_meta_datas',$where);

			unset($this->all[$type][$key]);

			return TRUE;

		}
		return FALSE;
	}
}