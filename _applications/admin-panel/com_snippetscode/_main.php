<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Компонента Шматків HTML коду для Landing Page
 *
***/


/** Коректування запиту в таблицю _meta_datas **/


_add_filter('meta_datas_SQL_query', 'com_snippetsCode_meta_datas_SQL_query');


function com_snippetsCode_meta_datas_SQL_query($result)
{
	if(!empty($result)) $result .= ' OR ';

	return  $result . " md_type='snippetCode' ";
}

/** Ініціалізація після підключення всіх доступних мобулів **/

if(PAGE_TYPE=='admin'){
	_add_action('defined_modules', 'com_snippetsCode_defined_modules');
}

function com_snippetsCode_defined_modules()
{
	global $admin;
	//оновлення структури таблиці _meta_datas
	global $sql; $sql->db_table_installer('snippetsCode','05.09.2014',
		array(
			'_meta_datas' => array(
				'md_id' => "`md_id` int(11) NOT NULL AUTO_INCREMENT",
				'md_key' => "`md_key` varchar(300) NOT NULL",
				'md_value' => "`md_value` text NOT NULL",
				'md_type' => "`md_type` varchar(20) NOT NULL DEFAULT 'option'",
				'CREATE' => "`md_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`md_id`)"
			)
		)
	);
}

/** Формування підменю Сайдбару Адмінки **/

if (PAGE_TYPE=='admin') {
	_add_filter('meta_datas_admin_sidebar_menu_elenemts', 'meta_datas_com_snippetscode_admin_sidebar_menu_elenemts');
	_add_filter('meta_datas_admin_sidebar_menu_elenemts_active_items', 'meta_datas_com_snippetscode_admin_sidebar_menu_elenemts_active_items');
}

function meta_datas_com_snippetscode_admin_sidebar_menu_elenemts($result)
{
	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Виджеты'), //ім"я пункта меню
			'url' => getURL('admin', 'snippets-code'), //посилання
			'active_pages' => array('snippets-code'), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => FALSE //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_submenu_item','admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(100), $menu);

	return $result . $menu;
}

function meta_datas_com_snippetscode_admin_sidebar_menu_elenemts_active_items($result)
{//перелік сторінок, для яких має бути активний блок меню Елементи сайту
	$result[] = 'snippets-code';
	return $result;
}

/** Роутер Адмінки **/

if (PAGE_TYPE=='admin') {
	_add_action('adminPanel_controller', 'com_snippetsCode_admin_controller');
}

function com_snippetsCode_admin_controller()
{
	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL,
			$htmlTPL;

	switch(@$_GET['admin']){
		case'snippets-code':
			require_once __DIR__ . '/a_snippetscode.php';
			return TRUE;

			break;
	}
}

function snippet($var)
{ # шоткод сныпета
	if(PAGE_TYPE=='admin')return '[:snippet '.$var.':]';
	global $meta;

	return sc_snippetCode(@$meta->val($var,'snippetCode'));
}

function sc_snippetCode($var)
{ # Опрацьовує строку і робить заміну HTML заготовок
	global 	$PData,
			$meta;

	preg_match_all(
		"/\%_(.*)_\%/Uis",
		$var,
		$ar
	);

	$text='';

	if(!empty($ar[1]) && is_array($ar[1])){
		foreach($ar[1] AS $key=>$val){
			$snip = $meta->val($val,'snippetCode');
			if(empty($snip))$snip = _lang('Не правильная метка HTML кода');
			$var=str_replace($ar[0][$key],$snip,$var);
		}
		$var = sc_snippetCode($var);
	}
	return $var;
}