<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Додавання рефералів
 *
***/

global $meta, $PData;

if(isset($_POST['delRowSubmit']) && isset($_POST['checkRow']) && is_array($_POST['checkRow'])){ //Видалення записів

	if($meta->delete($_POST['checkRow'],'snippetCode')){

		$PData->content('Запись удалена','message',TRUE);

	}else{

		$PData->content('Произошел сбой алгоритма. Запись небыла удалена. Обратитесь пожалуйста к разработчикам.','message');

	}

}elseif(!empty($_POST['meta']['key'])){ //Створення нових записів

	$_POST['meta']['val'] = str_replace('textаrea','textarea', $_POST['meta']['val']);

	if(!$meta->add_update($_POST['meta']['key'], @$_POST['meta']['val'],'snippetCode')){
		$PData->content(_lang('Запись').' <b>'.$_POST['meta']['key'].'</b> '._lang('не сохратнена').'!','message');
	}else{
		$PData->content('Запись сохранена','message',TRUE);
	}

}


/** Пагінація **/

$text = '';
$text_modals = '';

/** Формування основного списку категорій **/

$result = $meta->all_values('snippetCode');

if($result){
	$i = 0;
	foreach($result AS $key=>$val){

		if(!empty($_GET['filter']) && !stristr($key,$_GET['filter']) && !stristr($val,$_GET['filter']) )continue;

		if(is_array($val))$val =  print_r($val, true);
		$text .= '

		<div class="item">
            <input style="margin-bottom: 0px;" type="checkbox" value="'.$key.'" name="checkRow['.$key.']"/>
            <span data-toggle="modal" data-target=".vidjet-'.str_replace(" ",'',$key).'" style="cursor: pointer;">'.$key.'</span>

            <!-- vidjet modal -->

          </div>
		';
        $text_modals.='<div class="modal fade vidjet-'.str_replace(" ",'',$key).'" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">'.$key.'</h4>
                  </div>
                  <div class="modal-body">
                    <form method="POST" enctype="multipart/form-data">
                      <input type="text" value="'.$key.'" name="meta[key]"/>
                      <textarea type="text" name="meta[val]">'._unprotect($val).'</textarea>
                      <input class="button" type="submit" value="Save">
                    </form>
                  </div>
                </div>
              </div>
            </div>';
		$i++;
		if($i==4){
			$text .= '<div class="clear"></div>';
			$i=0;
		}
	}
}


/** Формування основного HTML коду **/

$PData->content('Виджеты','title');

$PData->content('
	<form class="list" method="POST">
					<p><b>+ '._lang('Добавить').'</b>:</p>
					<p class="grey"> '.   _lang('Используется : %_метка виджета_%').'</p>
					<input placeholder="'._lang('Метка').'" type="text" name="meta[key]" value=""/>
					<textarea placeholder="'._lang('HTML код').'" type="text" name="meta[val]"></textarea>
					<input type="submit" class="button" value="'._lang('Добавить').'"/>
	</form>

	<div class="line"></div>

	<form class="list">
					<p><b>'._lang('Поиск').':</b></p>
					<input type="hidden" name="admin" value="'.$PData->_GET('admin').'"/>
					<input placeholder="'._lang('Название').'" type="text" name="filter" value="'.$PData->_GET('filter').'"/>
					<input type="submit" class="button" value="'._lang('Искать').'"/>

	</form>
	<hr/>
	<br/>
	<form id="tableEdit1" class="well" method="POST">
		'.$text.'
		<input type="submit" name="delRowSubmit" class="button" value="'._lang('Удалить').'"/>
	</form>
	'.$text_modals.'
');
//todo fix