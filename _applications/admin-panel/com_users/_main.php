<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Users
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */


require_once __DIR__ . '/u_fn.php';
require_once __DIR__ . '/u_class.php';



_add_action('defined_modules', 'users_defined_modules', 1);

function users_defined_modules()
{
	global $user;
	$user = new getUser;

	if(PAGE_TYPE=='admin'){
		global 	$meta,
				$sql;

		$module_db = $meta->val('apSet_Users','mod_option');
		if($module_db){
			$sql->replace('_meta_datas', array('md_key','md_value'), 'apSet_Users', 'apSet_users');
		}

		$sql->db_table_installer('Users','30.08.2015',
			array(
				'_users' => array(
					'u_id' => "`u_id` int(11) NOT NULL AUTO_INCREMENT",
					'u_email' => "`u_email` varchar(50) DEFAULT NULL",
					'u_fullname' => "`u_fullname` varchar(50) NOT NULL",
					'u_network_id' => "`u_network_id` varchar(50) NOT NULL",
					'u_network' => "`u_network` varchar(20) NOT NULL",
					'u_photo' => "`u_photo` varchar(300) NOT NULL",
					'u_profile' => "`u_profile` varchar(300) NOT NULL",
					'u_ip' => "`u_ip` varchar(15) NOT NULL",
					'u_pass' => "`u_pass` varchar(50) DEFAULT NULL",
					'u_rights' => "`u_rights` double DEFAULT '1'",
					'u_lang' => "`u_lang` varchar(50) NOT NULL",
					'CREATE' => "`u_id` int(11) NOT NULL AUTO_INCREMENT, PRIMARY KEY (`u_id`)"
				)
			)
		);
	}
}


/** Формування меню в адмінці  **/

if (PAGE_TYPE=='admin') {
	_add_filter('adminPanel_sidebar_menu', 'users_admin_sidebar_menu',50);
}

function users_admin_sidebar_menu($result)
{//модифікація меню адмін панелі в правій колонці

	global $PData;

	$vars_array = array(
		array(
			'title' => _lang('Пользователи'), //ім"я пункта меню
			'url' => getURL('admin','users'), //посилання
			'submenu_filter' => 'users_admin_sidebar_menu', //хук для підключення пунктів підменю
			'active_pages' => _run_filter('users_admin_sidebar_menu_active_items', array('users')), //на яких сторінках має бути активний (розгорнутий)
			'icon_link' => FALSE, //іконка справа, якщо FALSE - жодної; TRUE - стрілочка; посилання(string) - плюс, який є анкором на передане посилання.
			'left_icon_class' => 'glyphicon glyphicon-user' //
		),
	);

	$menu = $PData->getModTPL('sidebar/sidebar_menu_item','admin-panel', $vars_array, TRUE);

	$menu = u_ifRights(array(6, 100), $menu);

	return $result . @$menu;
}

/** Роутер Адмінки **/

if (PAGE_TYPE=='admin') {
	_add_action('adminPanel_controller', 'users_admin_controller');
}

function users_admin_controller()
{ //доповнення роутера AdminPanel'і

	global 	$PData,
			$admin,
			$user,
			$sql,
			$sqlTPL,
			$contTPL;

	switch(@$_GET['admin']){
		case'users':
			require_once __DIR__ . '/apps_admin/a_users.php';
			return TRUE;
		break;
		case'profile':
			require_once __DIR__ . '/apps_admin/a_profile.php';
			return TRUE;
		break;
	}
}