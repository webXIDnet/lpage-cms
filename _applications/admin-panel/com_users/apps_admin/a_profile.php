<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
***/

if(isset($_POST['userEditsubmit'])){

	if(!empty($_POST['user']['email'])){

		if($_POST['user']['pass']==$_POST['pass_2']){

			$message = array();

			if(empty($_POST['user']['pass'])) {
				unset($_POST['user']['pass']);
			} else {
				$_POST['user']['pass'] = md5($_POST['user']['pass']);
				$message[] = 'Пароль изменен';
			}

			$_POST['user'] = _run_filter('User_profile_update_array',$_POST['user']);
			$_POST['user']['w:id'] = $user->id;

			$sql->update('_users',"u_id= :id",$_POST['user'],'u_');

			$user->fullname=$_POST['user']['fullname'];
			$user->email=$_POST['user']['email'];

			$message[] = 'Запись сохранена';

			foreach ($message as $key => $value) {
				$PData->content($value,'message', TRUE);
			}

		}else{

			$PData->content('Пароли не совпадают','message');

		}
	}else{

		$PData->content('Вы не указали Логин','message');

	}
}

$cval = (object)$sql->query("
	SELECT u_fullname,u_email
	FROM _users
	WHERE u_id = '"._protect($user->id)."'
",'value');

$PData->content('Личные данные','title');

$PData->content('
<form class="list" method="POST">
	<p>
		<h3>'._lang('Имя').'</h3>
		<input type="text" name="user[fullname]" value="'.$cval->u_fullname.'"/>
	</p>
	<p>
		<h3>'._lang('Email (логин)').'</h3>
		<input type="text" name="user[email]" value="'.$cval->u_email.'"/>
	</p>
	<p>
		<h3>'._lang('Пароль').'</h3>
		<input type="password" name="user[pass]" value=""/>
	</p>
	<p>
		<h3>'._lang('Пароль, еще раз').'</h3>
		<input type="password" name="pass_2" value=""/>
	</p>
	'._run_filter('User_profile_edit_form').'

	<input type="submit" class="button" value="Сохранить" name="userEditsubmit"/>
</form>');