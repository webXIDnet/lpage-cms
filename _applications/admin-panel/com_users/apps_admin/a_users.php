<?php
if (!defined('HOMEPAGE')) {
    header('HTTP/1.0 404 not found');
    exit();
} // захист від прямих звернень до файлу
/**
 * PAGE_TYPE = admin
 *
 * Тут формується HTML код сторінки
 *
 ***/


if (isset($_POST['delSubmit']) && isset($_POST['itemID']) && is_array($_POST['itemID'])) {

    $where = $where1 = '';
    $att_array = array();

    foreach ($_POST['itemID'] AS $key => $val) {

        if (!empty($where)) {
            $where .= ' OR ';
        }

        $where .= "u_id='" . _protect($key) . "'";
    }

    if (!empty($where)) {
        $sql->delete('_users', $where);

        $PData->content('Запись удалена', 'message', TRUE);

    } else {

        $PData->content(
            'Произошел сбой алгоритма. Запись небыла удалена.
			<br/>Код ошибки: <b>#del_users</b>',
            'message'
        );

    }

} elseif (isset($_POST['newItem'])) {

    if (empty($_POST['newItem']['fullname']) || empty($_POST['newItem']['email'])) {
        $PData->content(
            'Нужно заполнить все поля с *',
            'message'
        );
    } else {

        if (empty($_POST['newItem']['rights'])) {
            $_POST['newItem']['rights'] = 1;
        }

        if (empty($_POST['newItem']['pass'])) {
            $_POST['newItem']['pass'] = 12345678;
        }

        $array = array(
            'fullname' => $_POST['newItem']['fullname'],
            'pass' => md5($_POST['newItem']['pass']),
            'email' => $_POST['newItem']['email'],
            'rights' => $_POST['newItem']['rights']
        );

        $sql->insert('_users', $array, 'u_');

        $PData->content('Запись создана успешно!', 'message', TRUE);

    }

} elseif (isset($_POST['formsubmit-edit']) && is_array($_POST['editItem'])) {

    $u_id = '';

    foreach ($_POST['editItem'] AS $key => $val) {
        $u_id = $key;
        break;
    }

    if (!empty($_POST['editItem'][$u_id]['pass'])) {
        $_POST['editItem'][$u_id]['pass'] = md5($_POST['editItem'][$u_id]['pass']);
    } else {
        unset($_POST['editItem'][$u_id]['pass']);
    }

    $sql->update('_users', "u_id='" . _protect($u_id) . "'", $_POST['editItem'][$u_id], 'u_');

    $PData->content('Запись сохранена', 'message', TRUE);

}
/** Права */
global $meta;
$select_rights = '';
$rights = $meta->val('apSet_users', 'mod_option');

if (isset($rights['rights']) && is_array($rights['rights'])) {
    foreach ($rights['rights'] AS $key => $val) {

        if ($user->rights != 100 && $user->rights <= $key) continue;

        $selected = '';
        if (isset($_POST['newItem']['rights']) && $_POST['newItem']['rights'] == $key) $selected = 'selected="selected"';

        $select_rights .= '<option ' . $selected . ' value="' . $key . '">' . _lang($val) . ' (' . $key . ')</option>';
    }
}

/** Пагінація **/

$parent_select = $text = $where = '';

if (!empty($_GET['find_rights'])) {

    $where = "	u_rights = '" . _protect($_GET['search']) . "'";

} elseif (!empty($_GET['search'])) {

    $where = " u_fullname LIKE '%" . _protect($_GET['search']) . "%'
		OR
			u_email LIKE '%" . _protect($_GET['search']) . "%'
		OR
			u_rights = '" . _protect($_GET['search']) . "'";

}

if ($user->rights != 100) {
    if (!empty($where)) {
        $where = "({$where}) AND ";
    }
    $where = " u_rights > '" . _protect($user->rights) . "' ";
}

if (!empty($where)) {
    $where = 'WHERE ' . $where;
}

if (!isset($_GET['search'])) {
    $_GET['search'] = '';
}

$pagination = _pagination(
    '_users',
    'u_id',
    $where,
    getURL('admin', 'users', 'search=' . $_GET['search']));


/** Формування основного списку категорій **/

$result = $sql->query("
	SELECT *
	FROM _users
	{$where}
	ORDER BY u_fullname ASC
	" . @$pagination['limit'] . "
");

if (!is_array($result) || empty($result)) {

    $text = '<b>' . _lang('Пользователей не найдено') . '</b><br/> =\ ';

} else {

    foreach ($result AS $cval) {

        $u_id = $cval['u_id'];

        $text .=
            '
<tr>
            <td>
              <label>
                <input type="checkbox" name="itemID[' . $u_id . ']"> ' . $u_id . '
              </label>
            </td>
            <td class="td-link" data-toggle="modal" data-target=".user-modal' . $u_id . '">
              <b>' . $cval['u_fullname'] . '</b>
            </td>
            <td class="td-link" data-toggle="modal" data-target=".user-modal' . $u_id . '">
              <span class="grey">Email:</span> ' . $cval['u_email'] . '
            </td>
            <td class="td-link" data-toggle="modal" data-target=".user-modal' . $u_id . '">
              ' . $cval['u_rights'] . '
            </td>
          </tr>

<tr>' .
            u_clients_editing_form($cval) . '
			</tr>';
    }

    $text = '
	<form method="POST">
	<div class="table-holder">
		<table id="tableEdit" class="table">
			<thead>
					<th id="head-tableList" class="table-10">' . _lang('Выбрать') . '</th>
					<th id="head-tableList" class="table-40">' . _lang('ФИО') . '</th>
					<th id="head-tableList" class="table-40">' . _lang('Email / Логин') . '</th>
					<th id="head-tableList" class="table-10">' . _lang('Права') . '</th>
				</thead>
            <tbody>
				' . $text . '
			</tbody>
		</table>
		</div>
		<input type="submit" class="button" name="delSubmit" value="' . _lang('Удалить') . '"/>
	</form>
	';
}

if (!isset($_POST['newItem']['email'])){
    $_POST['newItem']['email'] = '';
}

if (!isset($_POST['newItem']['fullname'])){
    $_POST['newItem']['fullname'] = '';
}

/** Формування основного HTML коду **/

$PData->content('Пользователи', 'title');

$PData->content('
	<form class="list" method="POST">
					<p><b>+ ' . _lang('Добавить Пользователя') . ':</b></p>
					<select name="newItem[rights]">
							' . $select_rights . '
                    </select>
					<input placeholder="' . _lang('Email / Логин') . ' *" type="text" name="newItem[email]" value="' . $_POST['newItem']['email'] . '"/>
					<input placeholder="' . _lang('ФИО') . ' *" type="text" name="newItem[fullname]" value="' . @$_POST['newItem']['fullname'] . '"/>
					<input placeholder="' . _lang('Пароль (12345678)') . '" type="text" name="newItem[pass]" value=""/>
					<input type="submit" class="button" value="' . _lang('Добавить') . '"/>
	</form>
	<div class="line"></div>
	<!-- Пошук -->
	<form class="list">
		<p><b>' . _lang('Поиск') . ':</b></p>
		<input placeholder="' . _lang('ФИО / email / уровень доступа') . '" type="text" name="search" value="' . @$_GET['search'] . '"/>
<input type="checkbox" ' . (isset($_GET['find_rights']) && $_GET['find_rights'] == 'true' ? 'checked="checked"' : '') . ' name="find_rights" value="true"/>
			<label>
				- ' . _lang('Искать только по уровню доступа') . '
			</label>
			<br>
		<input type="submit" class="button" value="' . _lang('Искать') . '"/>
	</form>
	<!-- // Пошук -->
	<div class="line"></div>
	    ' . $text . '

' . $pagination['html']);
