<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модуль ініціалізації користувачів
 *
 * Клас
 *
***/

class getUser{
	public $geo; #регіон
	public $GMT; #часовий пояс
	public $lang; #Мова користувача
	public $lang_id;
	public $rights = 0;

	function __construct()
	{
		if(isset($_GET['logout'])){

			global $PData;

		    $this->define($_SESSION['id']);

		    _run_action('admin_user_logout', $this->email);

		    setcookie('user','0',(time()-3600));
		    unset($_COOKIE['user']);
		    session_destroy();

		    $_SESSION['id']=0;
		    $url = parse_url(PAGE_URL);

		    parse_str($url['query'],$get);
		    unset($get['logout']);

		    $url['scheme'] .= '://';
		    $url['query'] = @http_build_query($get);

		    if (!empty($url['query'])){
		    	$url['query'] = '?'.$url['query'];
		    }

		    $url = implode('',$url);
		    $PData->redirect($url);
		}

		// Ідентифікація користувача
		if (isset($_POST['token'])) {// Вхід/реєстрація нового користувача

			$s = file_get_contents('http://u-login.com/token.php?token='.$_POST['token'].'&host='.$_SERVER['HTTP_HOST']);
			$user_val = json_decode($s, true);
			$this->define($user_val,TRUE);

		    _run_action('admin_user_logged',$user_val);

		} elseif (isset($_POST['login'])) {// перевірка чи зареєстрований
			$this->define($_POST['login']);
		    _run_action('admin_user_logged',$_POST['login']);
		} elseif (!empty($_COOKIE['user'])) {// перевірка чи зареєстрований

			$this->define($_COOKIE['user']);

		} elseif ($_SESSION['id']>0) {// перевірка чи має доступ до адмінки

			$this->define($_SESSION['id']);

		} else {
			$this->define(0);
		}
	}

	public function define($user_val,$switch=FALSE){#Вертає дані користувача

		global $sql;

		if($switch){

			$mas='';

			$cval = $sql->query("
				SELECT u_id,u_fullname,u_profile,u_photo,u_email,u_rights,u_lang
				FROM _users
				WHERE (u_network_id='"._protect($user_val['uid'])."' AND u_network='"._protect($user_val['network'])."');
			",'value');

			if(is_array($cval)){

				$mas=$cval;

			}

			if(!is_array($mas)){

				$mas['u_network_id']=$user_val['uid'];
				$mas['u_network']=$user_val['network'];
				$mas['u_email']=$user_val['email'];
				$mas['u_fullname']=$user_val['first_name'].(!empty($user['last_name'])?' '.$user['last_name']:'');
				$mas['u_photo']=$user_val['photo'];
				$mas['u_profile']=$user_val['profile'];
				$mas['u_rights']=1;
				$mas['u_id']=$sql->insert('_users', $mas,'',FALSE,TRUE);

			}

			$this->id=$mas['u_id'];
			$this->fullname=$mas['u_fullname'];
			$this->photo=$mas['u_photo'];
			$this->email=$mas['u_email'];
			$this->profile=$mas['u_profile'];
			$this->rights=$mas['u_rights'];
			$this->lang=$mas['u_lang'];
			$this->areas=json_decode($mas['u_areas'],TRUE);

			$m=date('m')+1;
			$Y=date('Y');
			if($m==13){
				$m='01';
				$Y=$Y+1;
			}

			setcookie('user', $this->id, strtotime($Y.'-'.$m.date('-d')),'',HOMEPAGE);

		}else{

			$cval='';

			if(isset($_POST['login'])){

				$cval = $sql->query("
					SELECT u_id,u_fullname,u_profile,u_photo,u_email,u_rights,u_lang
					FROM _users
					WHERE u_email='"._protect($user_val['email'])."' AND u_pass='"._protect(md5($user_val['password']))."';
				",'value');

			}elseif(@$user_val>0){

				$cval = $sql->query("
					SELECT u_id,u_fullname,u_profile,u_photo,u_email,u_rights,u_lang
					FROM _users
					WHERE u_id='"._protect($user_val)."';
				",'value');

			}

			if(is_array($cval)){

				$this->id=$cval['u_id'];
				$this->fullname=$cval['u_fullname'];
				$this->photo=$cval['u_photo'];
				$this->email=$cval['u_email'];
				$this->profile=$cval['u_profile'];
				$this->lang=$cval['u_lang'];
				$this->rights=$cval['u_rights'];

				$m=date('m')+1;
				$Y=date('Y');
				if($m==13){
					$m='01';
					$Y=$Y+1;
				}

				@setcookie('user', $this->id, strtotime($Y.'-'.$m.date('-d')),'',HOMEPAGE);
			}else{

				if(isset($_POST['login']) || @$user_val>0){

					global $PData;
					$PData->content('Неверный логин или пароль','message');
				}

				$this->rights=0;

			}

		}

		if($this->rights>1) {
            $_SESSION['id'] = $this->id;

        } else
			$_SESSION['id']=0;

	}

	public function geo($GMT='',$geo=''){

		if(!empty($geo))
			$this->geo=$geo;

		if(!empty($GMT))
			$this->GMT=$GMT;

	}

}