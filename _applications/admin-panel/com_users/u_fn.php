<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Функції модуля користувачів
 *
***/

function u_ifRights($array,$return=''){//допомагає визначити час виконання частини коду

	global $user;

	if(in_array($user->rights,$array)){
		return $return;
	}

	return '';
}
function u_getUsers($key,$where=''){//отримує список користувачів

	global 	$sql,	$users_list;

	if(!isset($users_list[$key])){

		if(!empty($where))
			$where = 'WHERE '.$where;

		$list = $sql->query("
			SELECT *
			FROM _users
			{$where}
		");

		if(is_array($list)){
			foreach($list AS $cval){
				$users_list[$key][$cval['u_id']] = $cval;
			}
		}else{
			return FALSE;
		}
	}
	return @$users_list[$key];
}

function u_clients_editing_form($cval){

	if(empty($cval))return '';

	$u_id = $cval['u_id'];

	global 	$meta,
			$user;
	$select_rights = '';
	$rights = $meta->val('apSet_users','mod_option');

	if(isset($rights['rights']) && is_array($rights['rights'])){
		foreach($rights['rights'] AS $key => $val){

			if($user->rights!=100 && $user->rights<=$key) continue;

			$selected = '';
			if($cval['u_rights']==$key)$selected = 'selected="selected"';

			$select_rights .= '<option '.$selected.' value="'.$key.'">'.$val.' ('.$key.')</option>';
		}
	}

	$text = '
<div class="modal fade user-modal' . $u_id . '" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">'._lang('Пользователь').'</h4>
                </div>
                <div class="modal-body">
                  <form method="">

                  '.
        u_ifRights(
            array(100),
            '
                    <p><b>'._lang('Уровень доступа').'</b></p>
                    <select  name="editItem['.$u_id.'][rights]">
                      <option style="color:#c30;" value="-1">- '._lang('Заблокировать пользователя').' -</option>
						'.$select_rights.'
                    </select>
                    ').'
                  <p><b>'._lang('Email / Логин').':</p></b>
					<input type="text" name="editItem['.$u_id.'][email]" value="'.$cval['u_email'].'"/>


					<p><b>'._lang('ФИО').':</p></b>
					<input type="text" name="editItem['.$u_id.'][fullname]" value="'.$cval['u_fullname'].'"/>

                  '.
        u_ifRights(
            array(100),
            '
					<p><b>'._lang('Задать новий Пароль').':</p></b>
					<input type="text" name="editItem['.$u_id.'][pass]" value=""/>

			').'
                  <input class="button" name="formsubmit-edit" type="submit" value="Save">
                  </form>
                </div>
              </div>
            </div>
          </div>

		';
	return $text;
}

_add_filter('users_admin-panel_mod_settings','Users_adminpanel_mod_settings');

function Users_adminpanel_mod_settings($result,$set){

	$result['title'] .=
		'<li switchtab="tab_u_1" class="active">
<a>'._lang('Уровни доступа').'</a>
</li>';

	$user_rights = '
            <div class="form-group">
                <label>'._lang('Новый тип пользователя').'</b></label>
                <input class="input form-control" min="0" max="100" placeholder="'._lang('Уровень доступа').'" data-bind="value:replyNumber" type="number" name="newModSet[apSet_users][rights][1]" value=""/>
                <span class="help-block"> - '._lang('Только цифры').'</span>
            </div>
            <div class="form-group">
				<input class="input  form-control" placeholder="'._lang('Название доступа').'" type="text" name="newModSet[apSet_users][rights][0]" value=""/>
				<span class="help-block"> - '._lang('Например: Администратор').'</span>
			</div>
		<input class="button" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit">
		<hr/>';

	if(is_array($set)){
		foreach($set AS $key_com=>$array){
			if(is_array($array)){
				foreach($array AS $key=>$val){
                    $user_rights .='<div class="form-group">
                    <label><b>'._lang($val).'</b> ('.$key.')</label>';
					if($key==100){
						$user_rights .= '
								<input disabled="disabled" class="input form-control" type="text"  value="'.$val.'"/>
								<input class="form-control" type="hidden" name="modSet[apSet_users]['.$key_com.']['.$key.']" value="'.$val.'"/>
						';
					}else{
						$user_rights .= '
								<input class="input form-control" type="text" name="modSet[apSet_users]['.$key_com.']['.$key.']" value="'.$val.'"/>
								<div class="checkbox">
                                    <label>
                                        <input class="" for="'.$key_com.$key.'" type="checkbox" name="delApSet[apSet_users]['.$key_com.']['.$key.']" value="'.$key.'"> - '._lang('Удалить').'
                                    </label>
								</div>
						';
					}
				}
				$user_rights .= '</div><hr/>';
			}
		}
	}

	$result['text'] .=
		'<div role="tabpanel" class="tab-pane fade in active" id="tab_u_1">
			'.$user_rights.'
			<input class="button" type="submit" value="'._lang('Сохранить').'" name="formsubmit-edit">
		</div>';

	return $result;
}