<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

if ( (isset($vars_array['atr']) && !$vars_array['atr'] ) || !isset($vars_array['atr'])){
	$vars_array['atr'] = 'danger';
} else {
	$vars_array['atr'] = 'success';
}

if (!isset($vars_array['val'])){
	$vars_array['val'] = '';
}

?>

<div class="alert alert-<?php echo $vars_array['atr']; ?> alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="fa fa-info-circle"></i>  <?php echo $vars_array['val']; ?>
</div>