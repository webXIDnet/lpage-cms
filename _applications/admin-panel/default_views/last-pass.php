<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу ?>

<div class="col-lg-5 col-md-5 col-sm-5 lock">
	<div class="zamok"></div>
</div>
<div class="col-lg-7 col-md-7 col-sm-7 login">
	<form method="POST">
		<p><?php echo _lang('Востановление пароля'); ?></p>

		<div class="row">
			<label for="login"><i class="fa fa-user fa-lg"></i></label>
			<input type="text" id="login" name="repass" placeholder="<?php echo _lang('Email / Логин'); ?>">
		</div>

		<input type="submit" value="<?php echo _lang('Отправить'); ?>">
	</form>
	<div class="iforget">
		<a href="<?php echo getURL('admin',''); ?>"><?php echo _lang('Авторизироваться'); ?></a>
	</div>
</div>