<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

global $PData;

$login = $PData->_POST('login');
if (isset($login['email'])) {
	$login = $login['email'];
}

?>

	<div class="col-lg-5 col-md-5 col-sm-5 lock">
		<div class="zamok"></div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 login">
		<form method="POST">
			<p><?php echo _lang('Авторизация'); ?></p>

			<div class="row">
				<label for="login"><i class="fa fa-user fa-lg"></i></label>
				<input type="text" id="login" name="login[email]" placeholder="<?php echo _lang('Email / Логин'); ?>" value="<?php echo $login; ?>">
			</div>

			<div class="row">
				<label for="password"><i class="fa fa-lock fa-lg"></i></label>
				<input type="password" name="login[password]" id="password" placeholder="<?php echo _lang('Password'); ?>">
			</div>

			<input type="submit" value="<?php echo _lang('Войти'); ?>">
		</form>
		<div class="iforget">
			<a href="<?php echo getURL('admin','lost-pass'); ?>"><?php echo _lang('Востановить пароль'); ?></a>
		</div>
	</div>