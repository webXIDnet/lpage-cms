<?php

if (!empty($vars_array) && is_array($vars_array)) {
	foreach ($vars_array as $item_val) {

		$submenu = _run_filter($item_val['submenu_filter']);

		$item_class = 'link';
		$active_item_class = 'open';

		if (empty($submenu)) {
			$item_class = 'link-static';
			$active_item_class = 'active';
		}


?>

	<li class="<?php echo (in_array($_GET['admin'], $item_val['active_pages']) ? $active_item_class : ''); ?>">
		<div class="<?php echo $item_class; ?>">
		<?php
				if ($item_val['url']) {
					echo '<a href="'.$item_val['url'].'">'.$item_val['title'].'</a>';
				} else {
					echo $item_val['title'];
				}
		?>
			
		<?php
				if (!empty($item_val['icon_link']) && is_string($item_val['icon_link'])) {
					echo '<a href="'.$item_val['icon_link'].'" class="plus" title="'._lang('Создать').'"></a>';
				} elseif ($item_val['icon_link']) {
					echo '<i class="fa fa-angle-right"></i>';
				}
		?>
		</div>

<?php
		if (!empty($submenu)) {
?>
			<ul class="submenu" style="display: <?php echo (in_array($_GET['admin'], $item_val['active_pages']) ? 'block' : 'none'); ?>;">
				<?php echo @$submenu; ?>
			</ul>
<?php
		}
?>

	</li>

<?php

	}
}

