<?php

if (!empty($vars_array) && is_array($vars_array)) {
	foreach ($vars_array as $item_val) {
?>

	<li class="<?php echo (in_array($_GET['admin'], $item_val['active_pages'])?'active':'');?>">
		<a href="<?php echo $item_val['url'] ?>"><?php echo $item_val['title'] ?></a>
		<?php
			if (!empty($item_val['icon_link']) && is_string($item_val['icon_link'])) {
				echo '<a href="'.$item_val['icon_link'].'" class="plus" title="'._lang('Создать').'"></a>';
			}
		?>
	</li>

<?php

	}
}

