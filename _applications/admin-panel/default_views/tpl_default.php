<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

global $user;

?><!DOCTYPE html/>
<html lang="<?php echo $user->lang; ?>">
<head>

	<title><?php echo @$PData->position('title');?></title>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<link href="<?php echo SITE_FOLDER; ?>media/_admin/favicon.ico" rel="shortcut icon" />

	<!-- css -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?php echo SITE_FOLDER; ?>media/_admin/css/bootstrap.css?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"/>
	<link rel="stylesheet" href="<?php echo SITE_FOLDER; ?>media/_admin/css/style.css?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"/>
	<link rel="stylesheet" href="<?php echo SITE_FOLDER; ?>media/_admin/style.of.customer.css?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>" />

	<!-- js -->
	<script src="<?php echo SITE_FOLDER; ?>media/_admin/js/jquery-1.11.2.min.js?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"></script>
	<script src="<?php echo SITE_FOLDER; ?>media/_admin/js/bootstrap.js?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"></script>
	<script src="<?php echo SITE_FOLDER; ?>media/_admin/js/nav.js?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"></script>
    <script src="<?php echo SITE_FOLDER; ?>media/_admin/js/jq.scr.js"></script>
    <link rel="stylesheet" href="<?php echo SITE_FOLDER; ?>media/_admin/css/jcrop.css"/>
	<script type="text/javascript" src="<?php echo SITE_FOLDER; ?>media/_admin/jq.scr.of.customer.js"></script>
	<script type="text/javascript">
		HOMEPAGE='<?php echo SITE_FOLDER; ?>';
		LANG='<?php echo $user->lang; ?>';
	</script>

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?php echo _run_filter('admin_panel_header'); ?>
</head>
<body>

	<!-- header -->
	<header class="container-fluid">
		<nav class="navbar navbar-default">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-nav">
					<span class="sr-only"><?php echo _lang('Меню'); ?></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="http://lpagecms.com/" target="_blank"><?php echo LPAGE_CMS_VERSION; ?></a>
			</div>

			<div class="collapse navbar-collapse" id="header-nav">
				<ul class="nav navbar-nav">
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						<i class="icon icon-system"></i>Система <span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
<?php
							echo  u_ifRights(
								array(6,100),
								'<li><a href="'.getURL('admin','modules').'"><i class="icon icon-mod"></i>'._lang('Модули').'</a></li>'
							);
?>
							<li><a href="<?php echo getURL('admin','information','tab=modules'); ?>"><i class="icon icon-bank"></i><?php echo _lang('Модуль-банк'); ?></a></li>
							<li><a href="<?php echo getURL('admin','information','tab=updates'); ?>"><i class="icon icon-update"></i><?php echo _lang('Обновления'); ?></a></li>
							<li><a href="<?php echo getURL('admin','information','tab=support'); ?>"><i class="icon icon-sup"></i><?php echo _lang('Тех. поддержка'); ?></a></li>
							<li><a href="<?php echo getURL('admin','information'); ?>"><i class="icon icon-lic"></i><?php echo _lang('Лицензия'); ?></a></li>
						</ul>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo getURL('admin','profile'); ?>" class="profile" title="<?php echo _lang('Личные данные'); ?>"></a></li>
					<li><a href="<?php echo getURL('admin','','logout'); ?>" class="logout" title="<?php echo _lang('Выйти'); ?>"></a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- // -->

	<!-- sidebar -->
	<section class="sidebar">
			<ul id="accordion" class="accordion">
				<?php echo @$PData->position('sidebar'); ?>
		</ul>
		<!--script type="text/javascript" src="//lpagecms.com/api.js?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>&lang=<?php echo $user->lang;?>&p=sb"></script-->
	</section>
	<div class="sidebar-button">
		<a href="#" title="<?php echo _lang('Меню'); ?>"><i class="fa fa-chevron-right"></i></a>
	</div>
    <!-- // -->

	<!-- content -->
	<section class="container content">
        <?php
        if(!empty($PData->message)){
            echo '
			<div class="messageBox">

			   '.$PData->message.'
			</div>';
        }
        ?>
		<h1><?php echo @$PData->position('title'); ?></h1>
		<?php echo @$PData->content; ?>
	</section>
	<!-- // -->

	<!-- copy -->
	<section class="copy">
		<p><?php echo date('2013 - Y')?> &copy; LPageCMS.com</p>
		<!--script type="text/javascript" src="//lpagecms.com/api.js?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>&lang=<?php echo $user->lang;?>&p=cr"></script-->
	</section>
	<!-- // -->
<script type='text/javascript'>

    $(document).ready(function(){

        /*$('.chosen-selectbox').chosen({

        });
        $('.chosen-selectbox').chosen({

        });*/

        $('.button-add').button({
            icons:{
                primary: 'fa fa-plus'
            },
            text: false
        });
        $('.button-search').button({
            icons:{
                primary: 'fa fa-search'
            },
            text: false
        });
        $('.button-edit').button({
            icons:{
                primary: 'fa fa-pencil'
            },
            text: false
        });
        $('.button-save').button({
            icons:{
                primary: 'fa fa-floppy-o'
            },
            text: false
        });
        $('.button-move').button({
            icons:{
                primary: 'fa fa-share'
            },
            text: false
        });
        $('.button-copy').button({
            icons:{
                primary: 'fa fa-files-o'
            },
            text: false
        });
        $('.button-del').button({
            icons:{
                primary: 'fa fa-trash-o'
            },
            text: false
        });
    });
</script>
</body>
</html>