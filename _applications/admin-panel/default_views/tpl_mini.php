<?php if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу

global $user;

?><!DOCTYPE html/>
<html lang="<?php echo $user->lang; ?>">
<head>

	<title><?php echo @$PData->position['title'];?></title>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>

	<link href="<?php echo SITE_FOLDER; ?>media/_admin/favicon.ico" rel="shortcut icon" />

	<!-- css -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?php echo SITE_FOLDER; ?>media/_admin/css/bootstrap.css?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"/>
	<link rel="stylesheet" href="<?php echo SITE_FOLDER; ?>media/_admin/css/style.css?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"/>
	<link rel="stylesheet" href="<?php echo SITE_FOLDER; ?>media/_admin/style.of.customer.css?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>" />

	<!-- js -->
	<script src="<?php echo SITE_FOLDER; ?>media/_admin/js/jquery-1.11.2.min.js?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"></script>
	<script src="<?php echo SITE_FOLDER; ?>media/_admin/js/bootstrap.js?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"></script>
	<script src="<?php echo SITE_FOLDER; ?>media/_admin/js/nav.js?v=<?php echo str_replace('LPage CMS v','',LPAGE_CMS_VERSION);?>"></script>
    <script src="<?php echo SITE_FOLDER; ?>media/_admin/js/jq.scr.js"></script>
	<script type="text/javascript" src="<?php echo SITE_FOLDER; ?>media/_admin/jq.scr.of.customer.js"></script>
	<script type="text/javascript">
		HOMEPAGE='<?php echo SITE_FOLDER; ?>';
		LANG='<?php echo $user->lang; ?>';
	</script>
    <link rel="stylesheet" href="<?php echo SITE_FOLDER; ?>media/_admin/css/jcrop.css"/>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>


	<!-- login -->
	<section class="loginform">
	<?php
			if(!empty($PData->message)){
				echo '
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6" style="float:none; clear:both; margin:auto;">

					   '.$PData->message.'
					</div>
				</div>';
			}
			?>
		<div class="row">
			<h1><?php echo @$PData->position['title']; ?></h1>
			<?php echo @$PData->content; ?>
		</div>
	</section>
	<!-- // -->

	<!-- copy -->
	<section class="copy">
		<p><?php echo date('2013 - Y')?> &copy; LPageCMS.com</p>
	</section>
	<!-- // -->

</body>
</html>