<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Core controller
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

/** Define constant PAGE_TYPE **/
foreach($PData->GetSettings('set_page_types') AS $key => $val){
	if( ( isset($_GET[$key]) || (empty($_GET) && $key == 'homepage') ) && !defined('PAGE_TYPE')){

		define('PAGE_TYPE', $key); // тип сторінки

		if(is_array($val) && !empty($val)){
			define('PAGE_FOLDER', $val[0].'/');

			if (!empty($val[1])) {
				define('PAGE_FILE', $val[1] . '.php');
			} else {
				define('PAGE_FILE', $val[0] . '.php');
			}
			break;
		}
	}
}

if(!defined('PAGE_TYPE')) $PData->redirect('','404');


/** Connection classes **/
foreach($PData->GetSettings('set_classes') AS $key => $val){
	if( in_array(PAGE_TYPE, $val) || empty($val) ){
		if(file_exists(CORE_FOLDER . 'classes/' . $key . '.php')){
			require_once CORE_FOLDER . 'classes/' . $key . '.php';
		}
	}
}


/** Connection libreries **/
foreach($PData->GetSettings('set_libs') AS $key => $val){
	if(in_array(PAGE_TYPE, $val) || empty($val) ){
		if(file_exists(CORE_FOLDER . 'libreries/' . $key . '/_' . $key . '.php')){
			require_once CORE_FOLDER . 'libreries/' . $key . '/_' . $key . '.php';
		}
	}
}

_run_action('defined_core');


/** Connection additional files **/

foreach($PData->GetSettings('set_include_files') AS $key => $val){
	if($key == 'applications' && defined('APP_FOLDER')){ #Require files from applications folder
		foreach($val AS $file){
			if(file_exists(APP_FOLDER . $file)){
				require_once APP_FOLDER . $file;
			}
		}

	}elseif($key == 'page_files' && defined('PAGE_FOLDER')){ #Require files from Page Type folder
		foreach($val AS $file){
			if(file_exists(APP_FOLDER . PAGE_FOLDER . $file)){
				require_once APP_FOLDER . PAGE_FOLDER . $file;
			}

		}

	}elseif($key == 'links'){ #Require files by absolute server path
		foreach($val AS $file){
			if(file_exists($file)){
				require_once $file;
			}
		}
	}
}

_run_action('defined_additional_files');


/** Connection modules **/
foreach($PData->GetSettings('set_modules') AS $key => $val){
	if (in_array(PAGE_TYPE, $val) || empty($val) ) {
		if (!file_exists(APP_FOLDER . '_modules/' . $key . '/_' . $key . '.php')) {
			$key = strtolower($key);
		}

		if (file_exists(APP_FOLDER . '_modules/' . $key . '/_' . $key . '.php')) {
			if ($components = glob(APP_FOLDER . '_modules/' . $key . '/com_*/')) {
				foreach($components AS $component){
					if(file_exists($component . '_main.php')){
						require_once $component . '_main.php';
					}
				}
			}
			require_once APP_FOLDER . '_modules/' . $key . '/_' . $key . '.php';
		}
	}
}

_run_action('defined_modules');

_run_action('init_page');

/** Core router **/
if(defined('APP_FOLDER') && defined('PAGE_FOLDER') && defined('PAGE_FILE') && !defined('MODULE_PAGE_TYPE')){
	if(file_exists(APP_FOLDER . PAGE_FOLDER . PAGE_FILE)){
		require_once APP_FOLDER . PAGE_FOLDER . PAGE_FILE;
	}else{
		$PData->redirect('','404');
	}
}

_run_action('init_done');

#Connect page template
$PData->connectTPL();