<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Core functions of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

//допомагає визначити час виконання частини коду
function _countTime($val=FALSE)
{
	global $countTime;

	if($val){
		if(!isset($countTime[$val]['start'])){
			$countTime[$val]['start']=_mtime();
		}else{
			@$countTime[$val]['time'] += (_mtime() - $countTime[$val]['start']);
		}

	}else{
		echo '<div style="padding:20px;border:1px solid black;position:absolute;z-index:999;background-color:white;">';

		foreach($countTime AS $key=>$val){
			echo '
			<b>'.$key.':</b> '.$val['time'].' s </br></br>';
		}

		echo '</div>';
	}
}

/**
 * Add hook of action type
 * @param string $key - key of action
 * @param string $function - function, what will initialized
 * @param integer $priority - priority functions initialisation (default:10)
 *
 * @return boolean/string/array
 */
function _add_action($key, $function, $priority=10)
{
	return _add_hook($key, $function, 'action', $priority);
}

/**
 * Add hook of filter type
 * @param str $key - key of action
 * @param str $function - function, what will initialized
 * @param int $priority - priority functions initialisation (default:10)
 *
 * @return boolean/string/array
 */
function _add_filter($key, $function, $priority=10)
{
	return _add_hook($key, $function, 'filter', $priority);
}

function _add_hook($key, $function, $hook_type='action', $priority=10, $sort='')
{
	global $hook;

	switch($hook_type){
		case'action':case'filter':
			if(!empty($sort)){
				$hook[$hook_type][$key][$priority][$sort] = $function;
			}else{
				$hook[$hook_type][$key][$priority][] = $function;
			}
			break;

		default:
			return false;
	}
}

# Initializing all functions, what was added to current action
function _run_action($key, $accepted_vars='')
{
	return _run_hook($key, '', $accepted_vars, 'action');
}

# запускає хук в середині функції
function _run_filter($key, $result='', $accepted_vars='')
{
	return _run_hook($key, $result, $accepted_vars, 'filter');
}

# запускає хук в середині функції
function _run_hook($key, $result='', $accepted_vars='', $hook_type='filter')
{
	global $hook;

	$functions = '';

	switch($hook_type){
		case'action':case'filter':
			if(isset($hook[$hook_type][$key])){
				$functions = $hook[$hook_type][$key];
			}elseif($hook_type=='filter'){
				return $result;
			}else {
				return false;
			}
			break;

		default:
			return false;
	}

	if(is_array($functions)){
		ksort($functions);

		foreach($functions as $function){
			if(is_array($function)){
				foreach($function as $fk=>$fn){
					if($fn && function_exists($fn)){
						switch($hook_type){
							case'action':
								if (is_array($fn)) {
									$result = $fn($accepted_vars);
								} else {
									$result .= $fn($accepted_vars);
								}
								break;

							case'filter':
								$result = $fn($result, $accepted_vars);
								break;
						}
					}
				}
			}
		}
	}
	return $result;
}

function _cache( $step=FALSE, $minutes=1 )
{
	if(!$step) return FALSE;

	$minutes = $minutes*60;

	if(!defined('CACHE_TIME') && $step!='end' ){
		define('CACHE_TIME',$minutes);
	}elseif(defined('CACHE_TIME')){
		$minutes = @CACHE_TIME;
	}

	$time=floor( time()/$minutes ) * $minutes;
	$file_name = CORE_FOLDER.'logs/cache/'.urlencode(md5(PAGE_URL.$time)).'.cache';
	$old_time = (floor( time()/$minutes ) * $minutes) - $minutes;
	$old_file_name = CORE_FOLDER.'logs/cache/'.urlencode(md5(PAGE_URL.$old_time)).'.cache';

	if( $step == 'start' ){
		if( file_exists( $file_name ) ){
			readfile($file_name);
			exit();
		}

		ob_start();

	}elseif( $step == 'end' && defined('CACHE_TIME')){
		if (date('H')<=3) {
			_unDir(CORE_FOLDER.'logs/cache/',FALSE);
		} else {
			unlink($old_file_name);
		}

		$buffer = ob_get_contents();

		ob_end_flush();

		$fp = fopen($file_name, 'w');
		fwrite($fp, $buffer);
		fclose($fp);
	}
}

#функція перевіряє, чи головна сторінка
function _isHomepage()
{
	if(substr(PAGE_URL,0,strrpos(PAGE_URL,substr(PAGE_URL,-1)))==HOMEPAGE || PAGE_URL==HOMEPAGE || empty($_GET)){
		return true;
	}
}

#Send mail
function _eMail($mail, $title, $text,$from='')
{
	if (empty($from)) {
		$from=_getDomane(HOMEPAGE).' <no-replay@'._getDomane(HOMEPAGE).'>';
	}

	$headers  = "Content-type: text/html; charset=utf-8 \r\n";
	$headers .= "From: $from\r\n";
	//	$headers .= "Bcc: no-reply1@m-mba.com.ua\r\n";
	$mails = explode(',',$mail);
	$errors = 0;

	if (count($mails) > 1) {
		foreach ( $mails as $row ) {
			if(!mail(trim($row), $title, $text, $headers) || !_checkEmail(trim($row))) $errors++;
		}
	} else {
		if(!mail(trim($mails[0]), $title, $text, $headers) || !_checkEmail(trim($mails[0]))) $errors++;
	}

	if ($errors > 0) {
		return false;
	} else {
		return true;
	}
}

#вертае False при хибному значенні пошти.
function _checkEmail($email,$switch=FALSE)
{
	$emails = explode(',',$email);
	$errors = 0;

	if (count($emails) > 1)	{
		foreach ( $emails as $mail ) {
			if (!filter_var(trim($mail), FILTER_VALIDATE_EMAIL)) {
				$errors++;
			}
		}
	} else {
		if (!filter_var(trim($emails[0]), FILTER_VALIDATE_EMAIL)) {
			$errors++;
		}
	}
	if ($errors > 0) {
		return false;
	} else {
		return true;
	}
}

function _checkURL($url, $alt = FALSE)
{
	$check = @get_headers($url);

	if($check[0]=='HTTP/1.1 200 OK'){
		return $url;
	}
	return$alt;
}

#повертає масив, де перший елемент, домен
function _getDomane($val, $array = FALSE)
{
	$url = parse_url($val);

	if($array) {
		return $url;
	} else {
		return @$url['host'];
	}
}

function _getIP()
{
   	if(isset($_SERVER['HTTP_X_REAL_IP'])) {
   		return $_SERVER['HTTP_X_REAL_IP'];
   	}
	return $_SERVER['REMOTE_ADDR'];
}

function _getActive($param, $val = FALSE, $return = 'active')
{
	if($val && $param==$val){
		return $return;
	}
	return FALSE;
}

function _mtime()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function _textToURL($title, $switch = TRUE)
{
	$title = trim($title);

	if (!$switch) {//Тренслітерує строку
		$tranclit = array(
			"А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Ґ"=>"g","Д"=>"d","Е"=>"e","Є"=>"ye","Ё"=>"e","Ж"=>"zh","З"=>"z","И"=>"i","І"=>"i","Ї"=>"yi","Й"=>"j","К"=>"k", "Л"=>"l","М"=>"m","Н"=>"n","О"=>"o","П"=>"p", "Р"=>"r","С"=>"s","Т"=>"t","У"=>"u","Ф"=>"f", "Х"=>"h","Ц"=>"ts","Ч"=>"ch","Ш"=>"sh","Щ"=>"sch", "Ы"=>"y","Э"=>"e","Ю"=>"yu","Я"=>"ya",
	  "Ь"=>"","Ъ"=>"",
	  		"а"=>"a","б"=>"b","в"=>"v","г"=>"g","ґ"=>"g","д"=>"d","е"=>"e","є"=>"ye","ё"=>"e","ж"=>"zh","з"=>"z","и"=>"i","і"=>"i","ї"=>"yi","й"=>"j","к"=>"k", "л"=>"l","м"=>"m","н"=>"n","о"=>"o","п"=>"p", "р"=>"r","с"=>"s","т"=>"t","у"=>"u","ф"=>"f", "х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch", "ы"=>"y","э"=>"e","ю"=>"yu","я"=>"ya",
	 "ь"=>"", "Ь"=>"","Ъ"=>"",

			") "=>"", " ("=>"", "!"=>"", " - "=>"-", " "=>"-", "$"=>"", "@"=>"", "!"=>".", "?"=>".", "&"=>"",
			"="=>"", "|"=>"", "/"=>"", "\\"=>"", "#"=>"", "\""=>"", "'"=>"", ";"=>"", ":"=>"", ", "=>"-", ","=>"",
			"."=>".", "+"=>"_", "("=>"", ")"=>"", "*"=>"", "^"=>"", "№"=>"", ">"=>"", "<"=>"", "%"=>"", "`"=>"",
			']'=>'', '['=>'', '{'=>'', '}'=>'',"»"=>'',"«"=>''
		);
		return strtr($title, $tranclit);

	} else {//мало б перекладає строку, але не робить цього (
		$t = @json_decode(@file_get_contents('http://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20130926T104523Z.30991179ca1db141.c9a1ee38c42a525b5365a590fa78bc877ad4a489&text='.urlencode('Мама').'&lang=en'),TRUE);

		if ($t) {
			$title = $t['text'];
		}
		return _textToURL($title,FALSE);
	}
}

function _protect($str, $html = FALSE)
{
	if ($html == 'input') {
		$str=str_replace('"','&quot;',$str);
	} elseif ($html) {
		$str = htmlspecialchars($str);
	}
	return addslashes($str);
}

function _unprotect($str,$type='textarea')
{
	switch($type){
		case'input':
			$str = str_replace('"','&quot;',$str);
			break;

		case'textarea':
			$str = str_replace('textarea','textаrea',$str);
			break;

		default:
			$str = str_replace('\"','"',$str);
			$str = str_replace("\'","'",$str);
	}
	return $str;
}

#кодує(по замовчуванню) і розкодовує строку
function _strCode($url, $encode = TRUE, $json = TRUE)
{
	if ($json) {
		if ($encode) {
			return urlencode( base64_encode( json_encode($url) ) );
		} else {
			return json_decode( base64_decode( urldecode($url) ), TRUE );
		}
	} else {
		if ($encode) {
			return urlencode( base64_encode($url) );
		} else {
			return base64_decode( urldecode($url) );
		}
	}
}

/**
 * Run function with the same name like name of shortcodes and replace it by result of function work
 * @param  string $var string with sortcodes, like 'My name is [:UserName:]'
 * @return string
 */
function _shortCode($var)
{
	global $PData;

	preg_match_all("/\[\:(.*)\:\]/Uis", $var, $ar);

	$text='';

	foreach($ar[1] AS $key=>$val){
		$var=str_replace($ar[0][$key],$PData->runShortCode($val),$var);
	}
	return $var;
}

function _sharpCode($str, $array)
{
	if (is_array($array)) {
		foreach($array AS $key=>$val){
			$str = str_replace($key,$val,$str);
		}
	}
	return $str;
}

function _unDir($dir,$delete_dir=TRUE)
{
	if ($objs = glob($dir."*")) {
		foreach($objs as $obj) {
			is_dir($obj) ? _unDir($obj.'/') : unlink($obj);
		}
	}

	if(is_dir($dir) && $delete_dir){
		rmdir($dir);
	}
}

//формує випадкову строку
function _getRandStr($long = 6, $type = 'str&int')
{
	$array = array(
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
		'0','1','2','3','4','5','6','7','8','9'
	);

	switch($type){
		case'str':
			$array = array(
				'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
			);
			break;
		case'int':
			$array = array(
				'0','1','2','3','4','5','6','7','8','9'
			);
			break;
	}

	shuffle($array);

	return substr(implode('',$array),0,$long);
}

function _inputValue($str)
{
	return str_replace('"','&quot;',$str);
}

/**
 * Convert result of print_r back to array
 * @param  string $in
 * @return array
 *
 * @link http://php.net/manual/en/function.print-r.php#93529
 */
function _print_r_reverse($in)
{
    $lines = explode("\n", trim($in));

    if (trim($lines[0]) != 'Array') {
        // bottomed out to something that isn't an array
        return $in;

    } else {
        // this is an array, lets parse it
        if (preg_match("/(\s{5,})\(/", $lines[1], $match)) {

            // this is a tested array/recursive call to this function
            // take a set of spaces off the beginning
            $spaces = $match[1];
            $spaces_length = strlen($spaces);
            $lines_total = count($lines);

            for ($i = 0; $i < $lines_total; $i++) {
                if (substr($lines[$i], 0, $spaces_length) == $spaces) {
                    $lines[$i] = substr($lines[$i], $spaces_length);
                }
            }
        }

        array_shift($lines); // Array
        array_shift($lines); // (
        array_pop($lines); // )

        $in = implode("\n", $lines);

        // make sure we only match stuff with 4 preceding spaces (stuff for this array and not a nested one)
        preg_match_all("/^\s{4}\[(.+?)\] \=\> /m", $in, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

        $pos = array();
        $previous_key = '';
        $in_length = strlen($in);

        // store the following in $pos:
        // array with key = key of the parsed array's item
        // value = array(start position in $in, $end position in $in)
        foreach ($matches as $match) {
            $key = $match[1][0];
            $start = $match[0][1] + strlen($match[0][0]);
            $pos[$key] = array($start, $in_length);

            if ($previous_key != '') {
            	$pos[$previous_key][1] = $match[0][1] - 1;
            }

            $previous_key = $key;
        }

        $ret = array();

        foreach ($pos as $key => $where) {
            // recursively see if the parsed out value is an array too
            $ret[$key] = _print_r_reverse(substr($in, $where[0], $where[1] - $where[0]));
        }

        return $ret;
    }
}

function _makeBackup($folder='', $file_name='')
{
	if (empty($folder)) {
		$folder = CORE_FOLDER.'../media/files/backups/';
	}

	if (empty($file_name)) {
		$file_name = 'backup_'.date('d.m.Y_H:i').'_'.str_replace(' ','_',LPAGE_CMS_VERSION).'.zip';
	}

	$name_arch = $folder . $file_name;
	$files_to_arch = array();

	require_once CORE_FOLDER . 'classes/pcl-zip.lib.php';

	for($d = @opendir(CORE_FOLDER.'../'); $file = @readdir($d);){
	    if($file!='.' && $file!='..'){
	        $files_to_arch[]= $file;
	    }
	}

	chdir($folder);

	$archive = new PclZip($name_arch);
	$v_list = $archive->create(implode(',', $files_to_arch));
}

function _download($file, $name)
{
        if (!_checkURL($file)){
			header ("HTTP/1.0 404 Not Found");
        }

        $fd = @fopen($file, "rb");

        if (!$fd){
            header ("HTTP/1.0 403 Forbidden");
            exit;
        }

        header("HTTP/1.1 200 OK");
        header("Content-Disposition: attachment; filename=".$name);
        header("Content-Type: application/downloads");

		while(!feof($fd)){
            $content = fread($fd, 4096);
        	print($content);
        }
}

/**
 * Print string in draggable popup window
 * @param  string  $str       - string, what will print
 * @param  string  $var_title - title of window
 * @param  boolean $exit      - does script need to exit?
 * @param  boolean $details   - print string throught pring_r (default) or var_dump
 * @return srting
 */
function _dump($str, $var_title = '', $exit = FALSE, $details = FALSE)
{

    if (!$details){

        $buff = print_r($str, TRUE);

    }else{

        ob_start();
        var_dump($str);
        $buff = ob_get_clean();
    }
    $dumps_html = '
    <style type="text/css">
        .dump-cl {
            min-height: 50px !important;
            min-width: 80px !important;
            height: auto !important;
            width: auto !important;
        }
    </style>
    <div class="dump-cl" style="min-width: 100px;max-width: 50%;top:0;left:0;padding-bottom:20px;border:1px solid black;position:absolute;z-index:999999;background-color:white;"><div class="dump-h" style="background-color: grey;height: 50px;"><span style="cursor: pointer; color: white; font-size: 15px; padding-top: 11px; display: inline-block;">'.$var_title.'</span><span style="cursor:pointer;color:white;font-size: 35px;float: right;margin-right: 10px;">-</span></div><div class="dump-cont"><pre>'.
	$buff.
	'</pre></div></div>';

    if (!defined('_DUMPS_')){

        define('_DUMPS_','1');
        $dumps_html .= '<script type="text/javascript">
    	if(!window.jQuery){
			document.writeln(
				\'<sc\'+\'ript sr\'+\'c="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"><\'+\'/\'+\'sc\'+\'ript\'+\'>\'
			);
			document.writeln(
                \'<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css">\'+
				\'<sc\'+\'ript sr\'+\'c="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"><\'+\'/\'+\'sc\'+\'ript\'+\'>\'
			);
		}
		/*if (typeof jQuery == "function" && (\'ui\' in jQuery) && jQuery.ui && (\'version\' in jQuery.ui))
        {

        }*/
	</script>
	<script type="text/javascript">
	    jQuery(document).ready(function(){
	        jQuery( ".dump-cl" ).draggable({ handle: ".dump-h" });
	        jQuery(".dump-h span").click(function(){
	            jQuery(this).parents(".dump-cl").find(".dump-cont").toggle("slow");
	        });
	    });

	    </script>';
    }

    echo $dumps_html;

    if ($exit) {
    	exit();
    }
}

/**
 * Set exceptions of $_GET variables for define homepage
 * @param  string  $page_type 		- Page Type of homepage
 * @param  string  $val      		- value of variable $_GET[PAGE_TYPE]
 * @param  boolean/array $array 	- array with $_GET exceptions
 *
 * @return boolean            		- TRUE - homepage, FALSE - not hompage
 */
function _define_homepage_vars($page_type, $val, $array=FALSE)
{
	$get = @$_GET;
	$g = array();

	if(is_array($array)){
		foreach($array AS $v){//отримуємо перелік доступних змінних і записуємо в тимчасовий масив
			if(isset($_GET[$v])){
				$g[$v] = $_GET[$v];
				unset($_GET[$v]);
			}
		}
	}

	if(_isHomepage() && empty($_GET[$page_type]) && !defined('PAGE_TYPE')){ //перевіряємо, чи головна сторынка
		define('PAGE_TYPE',$page_type);
		$_GET[$page_type]=$val;
	}else{
		$_GET = array_merge($_GET, $get); //повертаэмо назад значення масиву $_GET
		return FALSE;
	}

	$_GET = array_merge($_GET, $get); //повертаэмо назад значення масиву $_GET

	return TRUE;
}

/**
 * Generate HTML of page nagination
 * @param  string $table - table name of rows, which will be divided
 * @param  string $col   - column name for count all rows
 * @param  string $where - query condition
 * @param  string $url   - URL of current page
 * @return array        - return array with 2 elements: limit - limit of query; html - html code of pagination
 */
function _pagination($table, $col, $where='', $url = CURRENT_PAGE)
{
	global $sql;

	if(!isset($_GET['amount'])){
		$_GET['amount']=10;
	}

	$limit=$_GET['amount'];
	if(!isset($_GET['nav-page'])){
		$_GET['nav-page']=1;
	}

	if($_GET['nav-page']>1){
		$limit=$limit*($_GET['nav-page']-1).', '.$limit;
	}

	$limit='LIMIT '._protect($limit);

	$img='';
	$count=0;
	$cval=$sql->query("SELECT COUNT($col) FROM $table $where;",'value');

	if($cval){
		$count=ceil(@$cval['COUNT('.$col.')']/@$_GET['amount']);
	}

	$text='
		<nav class="pagination-holder">
        <ul class="pagination">';

	if($count>1){

		$nav_page = 1;
		if($_GET['nav-page']-3>5){
			$text .= '
				<li>
					<a  href="'.$url.'">
						<<</a>
				</li>';
			$nav_page = 5;
		}

		if(stristr($url,'?')) $url .= '&';
		else $url .= '?';

		for($i=$nav_page;$i<=$count;$i++){
			$text.='
				<li class="'._getActive($i,$_GET['nav-page']).'">
					<a  href="'.$url.'nav-page='.$i.'">
						'.$i.'</a>
				</li>';
		}

	}

	$text.='</ul></nav>';

	return array('html'=>$text,'limit'=>$limit);
}