<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Core class of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

class UCode
{
	#Array with all settings from configuration file
	protected $settings;
	#Timer execution speed of a script
	protected $countTime;
	#Name of template
	protected $tlp = '';
	protected $get;
	protected $post;

	public $content;
	public $message;
	public $position;

	public function __construct()
	{
		$this->get = $_GET;
		$this->post = $_POST;

		$this->GetSettings();

		if (isset($this->settings['cache']) && $this->settings['cache']) {
			//todo потрібно зробити систему кешування HTML сторінки, у _fn.php є недопрацьована функція _cache();
		}
	}

	/**
	 * Method return value or values array
	 * @param bool /string $key
	 * - default value - FALSE, require configuration file and write settings in property $this->settings
	 * - key of array $this->settings[$key],
	 *
	 * @return bool/string/array
	 * - in successful case return TRUE or property $this->settings[$key]
	 * - in failure case return FALSE
	 *
	 **/
	public function GetSettings($key = false)
	{
		if ( ! $this->checkFile(APP_FOLDER . '_config/_settings.php')) {
			UCode::phpExit('Not isset configuration file', 'all');
		} elseif ( !$key) {
			$this->settings = require_once APP_FOLDER . '_config/_settings.php';
			return true;
		} elseif (isset($this->settings[ $key ])) {
			return $this->settings[ $key ];
		}

		return false;
	}

	/**
	 * changing property $this->settings
	 * @param string $key - key
	 * @param string $value - value
	 *
	 * @return FALSE
	 *
	 * @since UCode v1.2.6
	 *
	 **/
	public function setSetting($key, $value='')
	{
		$this->settings[$key] = $value;
	}

	#check for a file on a server
	public function checkFile($url, $alt = false)
	{
		if (file_exists($url)) {
			return $url;
		}

		return $alt;
	}

	/**
	 * Termination of the script
	 *
	 * @return FALSE
	 *
	 **/
	static function phpExit($message = '', $type = 'all')
	{
		$message = trim($message);

		if ( ! empty($message)) {
			echo $message;
		}

		switch ($type) {
			case'all':case'sql':
				global $db;

				if (is_array($db)) {
					MYSQL_CLOSE();
				}
				break;
		}

		switch ($type) {
			case'all':case'php':
				exit();
				break;
		}
	}

	/**
	 * Termination of the script
	 *
	 * @return FALSE
	 *
	 **/
	public function redirect($url = HOMEPAGE, $header_code = '')
	{
		$this->phpExit('', 'sql');

		switch ($header_code) {
			case'404':
				header('HTTP/1.0 404 not found');

				if (isset($this->settings[ 'error' . $header_code ]) && ! empty($this->settings[ 'error' . $header_code ])) {
					header('Location: ' . $this->settings[ 'error' . $header_code ]);

				} else {
					header('Location: ' . $url);
				}

				echo '<h1>ERROR 404 - page not found</h1>';
				break;

			case'301':
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $url);

				break;

			default:
				header('Location: ' . $url);
				break;
		}
		exit();
	}

	/**
	 * Check existing of function and Run it, if TRUE
	 * @param  string $str 		string with name of function and paraneters of this function
	 * @example
	 * 		$str = '_getRandStr 7';
	 * 		$this->runShortCode($str);
	 * 		//will run function _getRandStr(7);
	 *
	 * @return string       	result of functionn work or shortcode
	 *
	 */
	public function runShortCode($str)
	{
		$var   = trim($str);
		$array = explode(' ', $var);

		if (function_exists($array[0])) {

			$var = trim(str_replace($array[0], '', $var));

			return $array[0]($var);

		} else {
			return '[:' . $str . ':]';
		}
	}

	/**
	 * Set content vars for front-end template
	 * @param  string/array  $val       value of var
	 * @param  string  $position  key of var
	 * @param  boolean $atr       true or false character of message
	 * @param  boolean $shortCode should function run shortcode in $val
	 *
	 * @return false
	 *
	 */
	public function content($val, $position = 'main', $atr = false, $shortCode = true)
	{
		if ($shortCode) {
			$val = _shortCode(_lang($val));
		} else {
			$val = _lang($val);
		}

		switch ($position) {
			case'main':
				@$this->content .= $val;
				break;

			case'message':
				@$this->message .= $this->getModTPL('information-message', 'admin-panel', array('val' => $val, 'atr' => $atr), TRUE);
				break;

			default:
				if (!isset($this->position[ $position ])){
					$this->position[ $position ] = '';
				}

				$this->position[ $position ] .= $val;
				break;
		}
	}

	public function position($key, $return = '')
	{
		if (isset($this->position[$key])) {
			return $this->position[$key];
		}
		return $return;
	}

	/**
	 * Include template file for module
	 * @param string $tpl_file_name - template file name
	 * @param string $module_name - module name, folder, where is template
	 * @param array $vars_array - array of variables, what use in template
	 * @example
	 *    echo $this -> getModTPL('posts_list', 'blog', array('title'=>'My new news') );
	 * 		//will return a content of file VIEW_FOLDER . 'modules/blog/posts_list.php' , if it exists
	 *		//else APP_FOLDER . '_modules/blog/default_views/posts_list.php', if it exists
	 *		//esle return FALSE
	 *		//in this files will be available $vars_array. In this case $vars_array['title'] will be equal 'My new news'
	 *
	 * @return string
	 *
	 * @since UCode v1.2.4
	 *
	 **/
	public function getModTPL($tpl_file_name, $module_name, $vars_array='', $application = FALSE)
	{
		$template_view = VIEW_FOLDER . 'modules/' . $module_name. '/' . $tpl_file_name . '.php';
		$default_view = APP_FOLDER . '_modules/' . $module_name. '/default_views/' . $tpl_file_name . '.php';

		if ($application) {
			$template_view = VIEW_FOLDER . $module_name. '/' . $tpl_file_name . '.php';
			$default_view = APP_FOLDER . $module_name. '/default_views/' . $tpl_file_name . '.php';
		}

		if (file_exists( $template_view )) {
			ob_start();
			require $template_view;
			$tpl_file_name = ob_get_contents();
			ob_end_clean();
		} elseif(file_exists( $default_view)) {
			ob_start();
			require $default_view;
			$tpl_file_name = ob_get_contents();
			ob_end_clean();
		} else {
			return FALSE;
		}

		return $tpl_file_name;
	}

	/**
	 * Set property $this -> tlp
	 * @param string $tpl_file_name - template file name
	 * @example
	 * 	$this -> setTPL('post');
	 *	$this -> connectTPL();
	 * 		//will print a content of file VIEW_FOLDER . 'post.php', if it exists and return TRUE
	 *		//else VIEW_FOLDER . '/' . $module_name . '/' . $tpl_file_name . '.php', if it exists and return TRUE
	 *		//else APP_FOLDER . '_modules/' . $module_name. '/default_views/' . $tpl_file_name . '.php', if it exists and return TRUE
	 *		//else return FALSE
	 *
	 * @return boolean
	 *
	 * @since UCode v1.2.4
	 *
	 **/
	public function setTPL($tpl_file_name, $module_name = FALSE, $application = FALSE)
	{
		$template_view = VIEW_FOLDER . $tpl_file_name . '.php';
		$template_mod_view = VIEW_FOLDER . 'modules/' . $module_name . '/' . $tpl_file_name . '.php';
		$default_view = APP_FOLDER . '_modules/' . $module_name. '/default_views/' . $tpl_file_name . '.php';

		if ($application) {
			$template_mod_view = VIEW_FOLDER .  $module_name . '/' . $tpl_file_name . '.php';
			$default_view = APP_FOLDER . $module_name. '/default_views/' . $tpl_file_name . '.php';
		}

		if (file_exists($template_view) && !$module_name) {
			$this->tlp = $template_view;
		} elseif (file_exists($template_mod_view)) {
			$this->tlp = $template_mod_view;
		} elseif (file_exists($default_view)) {
			$this->tlp = $default_view;
		} else {
			return FALSE;
		}
		return TRUE;
	}


	/**
	 * Connect main or additional template file
	 * @param  boolean/string $tpl_file_name template file name
	 *
	 * @return boolean
	 *
	 * @since UCode v1.2.3
	 *
	 */
	public function connectTPL($tpl_file_name = false)
	{
		if ( ! $tpl_file_name) {

			global $meta, $PData;

			if ( ! empty($this->tlp) && is_string($this->tlp) && file_exists($this->tlp)) {
				require_once $this->tlp;
				$this->phpExit();
			} elseif (file_exists(VIEW_FOLDER . mb_strtolower(PAGE_TYPE) . '.php')) {
				require_once VIEW_FOLDER . mb_strtolower(PAGE_TYPE) . '.php';
				$this->phpExit();
			} elseif (file_exists(VIEW_FOLDER . 'default_tpl.php')) {
				require_once VIEW_FOLDER . 'default_tpl.php';
				$this->phpExit();
			}

			return false;
		} else {

			if (file_exists(VIEW_FOLDER . $tpl_file_name . '.php')) {
				require_once VIEW_FOLDER . $tpl_file_name . '.php';
			} else {
				return false;
			}

			return true;
		}
	}

	/**
	 * Counting time of a script execution speed
	 * @param bool /string $val
	 *  	- default FALSE, will print a collected intervals
	 *   	- key of code parth
	 * @example
	 *    $PData -> countTime('Time of testing');
	 *        //some code
	 *    $PData -> countTime('Time of testing');
	 *    $PData -> countTime();
	 *
	 * @return bool/string
	 *   	- In successful case, start to collecte time of current code parth or print collected intervals by each of code parths
	 *  	- In failure case returns FALSE
	 *
	 * @since UCode v1.2.3
	 *
	 **/
	public function countTime($val = false)
	{
		if ($val) {
			if ( ! isset($this->countTime[ $val ]['start'])) {
				$this->countTime[ $val ]['start'] = _mtime();
			} else {
				@$this->countTime[ $val ]['time'] += (_mtime() - $this->countTime[ $val ]['start']);
			}
		} else {
			if (is_array($this->countTime)) {
				echo '<div style="padding:20px;border:1px solid black;display:table;margin-bottom:30px;background-color:white;">';

				foreach ( $this->countTime AS $key => $val ) {
					echo '
					<b>' . $key . ':</b> ' . $val['time'] . ' s </br></br>';
				}

				echo '</div>';
			} else {
				return false;
			}
		}
	}

	/**
	 * Write data in temp file
	 * @param string $string - string, what will be write in file
	 * @param string $file_name - temp file name
	 * @example
	 * 		$this -> writeLog('Hello World!','temp/'.date('Y-m-d_H-i-s').'.htm');
	 *
	 * @return bool/string - in seccessful case return count of wrote bytes else return FALSE
	 *
	 * @since UCode v1.2.3
	 *
	 **/
	public function writeLog($string, $file_name = 'log')
	{
		switch ($file_name) {
			case 'log':
				$file_name = CORE_FOLDER . 'logs/temp/' . date('Y-m-d_H-i-s') . '.log';
				break;
			case 'cache':
				$file_name = CORE_FOLDER . 'logs/cache/' . time() . '.cache';
				break;
			default:
				$file_name = CORE_FOLDER . 'logs/' . $file_name;
				break;
		}

		if ( ! file_exists(dirname($file_name))) {
			mkdir(dirname($file_name), 0777);
		}

		$fp = fopen($file_name, 'w');
		$result = fwrite($fp, $string);
		fclose($fp);

		return $result;
	}

	/**
	 * Get or set value of $_GET variable
	 * @param  string/int $key - key of $_GET array
	 * @param  string  $val - value of $this->get[$key]
	 * @param  boolean $set - if TRUE, than will do $this->get[$key] = $val;
	 * @return $this->get[$key] - return value of property
	 */
	public function _GET($key, $val = '', $set = FALSE)
	{
		if ($set) {
			$this->get[$key] = $val;
		}

		if (isset($this->get[$key])) {
			return $this->get[$key];
		}
		return '';
	}

	/**
	 * Get or set value of $_POST variable
	 * @param  string/int $key - key of $_POST array
	 * @param  string  $val - value of $this->post[$key]
	 * @param  boolean $set - if TRUE, than will do $this->post[$key] = $val;
	 * @return $this->post[$key] - return value of property
	 */
	public function _POST($key, $val = '', $set = FALSE)
	{
		if ($set) {
			$this->post[$key] = $val;
		}

		if (isset($this->post[$key])) {
			return $this->post[$key];
		}
		return '';
	}
}

$PData = new UCode();