<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * Клас тестування системи
 *
 * @author PavloM
 * @since UCode v1.2.3
***/
class Testings extends UCode{

	/**
	 * Запускає метод тестування модулів
	 *
	 * @return bool
	 * @author PavloM
 	 * @since UCode v1.2.3
	 **/
	public function __construct()
	{
		global $PData;
		$set_modules = $PData->GetSettings('set_modules');

		$this->coreTesting();

		foreach ($set_modules AS $key => $val) {

			if (is_array($val)) {

				if (!file_exists(APP_FOLDER . '_modules/' . $key . '/' . $key . '_test_class.php')) {
					$key = strtolower($key);
				}

				if (file_exists(APP_FOLDER . '_modules/' . $key . '/' . $key . '_test_class.php')) {

					if ($components = glob(APP_FOLDER . '_modules/' . $key . '/com_*/')) {

						foreach ($components AS $component) {

							if (file_exists($component . '_test_class.php')) {
								require_once $component . '_test_class.php';
							}

						}

					}

					require_once APP_FOLDER . '_modules/' . $key . '/' . $key . '_test_class.php';
				}

			}
		}

	}

	/**
	 * Виводить результат тестування на сторінку у наступному стандарті
	 * Format:
	 * 	[class->method] => [description] => array(
	 * 		[expected result]
	 *	 	[returned result]
	 * 	)
	 * 	[function] => [description] => array(
	 * 		[expected result]
	 *		[returned result]
	 * 	)
	 *
	 * @return bool
	 * @example $this->testResult('UCode::__construct',__FILE__.' line '.__LINE__, FALSE, FALSE);
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function testResult($fn_name, $desc, $expected, $returned)
	{
		$color = '';

		if ($expected != $returned) {
			$color = 'style="color:red;"';
		}

		if ($expected===TRUE) {
			$expected = 'TRUE';
		} elseif($expected===FALSE) {
			$expected = 'FALSE';
		}

		if ($returned===TRUE) {
			$returned = 'TRUE';
		} elseif($returned===FALSE) {
			$returned = 'FALSE';
		}

		if (is_array($returned) || is_object($returned)){
			$returned = print_r($returned, TRUE);
		}

		if (is_array($expected) || is_object($expected)){
			$expected = print_r($expected, TRUE);
		}

		echo '
<pre '.$color.'>
  \'<b>'.$fn_name.'</b>\' => '.$desc.'
	  <i>expected</i>: '.$expected.'
	  <u>returned</u>: <b>'.$returned.'</b>
  </pre>';
	}

	/**
	 * Метод тестує ядро
	 *
	 * @return FALSE
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function coreTesting()
	{

		/* == Перевірка налаштувань ==*/



		$this->test_include_settings_file();
		$this->test_include_settings_db();
		$this->test_include_settings_page_types();
		$this->test_include_settings_include_files();
		$this->test_include_settings_classes();
		$this->test_include_settings_libs();
		$this->test_include_settings_modules();

	}

	/**
	 * Перерка підключення файлу з налаштуваннями
	 *
	 * @return FALSE
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function test_include_settings_file()
	{
		global $PData;

		echo '<h2>Перевірка підключення і коректності даних у файлі _application/_settings.php</h2>';

		$this->testResult(
				'UCode::GetSettings',
				'Підключення файлу _application/_settings.php',
				TRUE,
				file_exists(APP_FOLDER.'_config/_settings.php')
			);
	}

	/**
	 * Перерка на наявність ключів до бази даних
	 *
	 * @return FALSE
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function test_include_settings_db()
	{

		global $PData;

		echo '<h3>Ключі до бази даних</h3>';

		$db = $PData->GetSettings('set_db');

		$this->testResult(
				'UCode::GetSettings->set_db',
				'Підключення файлу з ключами до бази даних',
				TRUE,
				(is_array($db) && !empty($db))
			);

		if (is_array($db)){

			$this->testResult(
				'UCode::GetSettings->set_db',
				'Кількість елементів у масиві з ключами до бази даних',
				4,
				@count($db)
			);

			foreach ($db as $key => $value) {
				$this->testResult(
						'UCode::GetSettings->set_db',
						'Перевірка на порожність ключа до бази даних <u>'.$key.'</u>',
						TRUE,
						!empty($value)
					);
			}
		}

	}

	/**
	 * Перерка коректність вказаних типів сторінок
	 *
	 * @return FALSE
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function test_include_settings_page_types()
	{
		global $PData;

		echo '<h3>Типи сторінок</h3>';

		$set_page_types = $PData->GetSettings('set_page_types');

		$this->testResult(
				'UCode::GetSettings->set_page_types',
				'Масив з списком типів сторінок підключено',
				TRUE,
				(is_array($set_page_types) && !empty($set_page_types))
			);

		if (is_array($set_page_types)){

			$this->testResult(
				'UCode::GetSettings->set_page_types',
				'Наявність визначених типів сторінок',
				TRUE,
				(@count($set_page_types)>0)
			);

			foreach ($set_page_types as $key => $page_type) {

				$this->testResult(
						'UCode::GetSettings->set_page_types',
						'Тип сторінки - порожня строка',
						FALSE,
						empty($key)
					);
				$this->testResult(
						'UCode::GetSettings->set_page_types',
						'Дозволена кількість параметрів - <u>'.$key.'</u>',
						TRUE,
						(count($page_type)<3 && count($page_type)>=0)
					);
			}

		}

	}

	/**
	 * Перерка коректного підключення додаткових файлів
	 *
	 * @return FALSE
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function test_include_settings_include_files()
	{
		global $PData;

		$set_include_files = $PData->GetSettings('set_include_files');

		if (is_array($set_include_files) && !empty($set_include_files)) {
			echo '<h3>Додаткові файли з дерикторії аплікацій</h3>';

			foreach ($set_include_files as $key => $value) {
				$this->testResult(
						'UCode::GetSettings->set_include_files',
						'Не правильно вказаний тип підключення користувацьких файлів',
						FALSE,
						empty($key)
					);
			}

			if (is_array($set_include_files['applications'])) {

				foreach ($set_include_files['applications'] AS $file) {
					$this->testResult(
							'UCode::GetSettings->set_include_files',
							'Не правильно підключений файл <u>'.APP_FOLDER . $file.'</u>',
							FALSE,
							!file_exists(APP_FOLDER . $file)
						);
				}

			}

			if (is_array($set_include_files['page_files'])) { //файли в папці сторінки
				$set_page_types = $PData->GetSettings('set_page_types');

				foreach ($set_include_files['page_files'] AS $file) {

					foreach ($set_page_types as $key => $value) {

						if (empty($value)) {
							continue;
						}

						$PAGE_FOLDER = $key;

						if (!empty($value[0])){
							$PAGE_FOLDER = $value[0];
						}

						$this->testResult(
								'UCode::GetSettings->set_include_files',
								'Не правильно підключений файл <u>'.APP_FOLDER . $PAGE_FOLDER .'/' . $file.'</u>',
								FALSE,
								!file_exists(APP_FOLDER . $PAGE_FOLDER .'/' . $file)
							);
					}

				}

			}

			if (is_array($set_include_files['links'])) {

				foreach($set_include_files['links'] AS $file){
					$this->testResult(
							'UCode::GetSettings->set_include_files',
							'Не правильно підключений файл <u>'.$file.'</u>',
							FALSE,
							!file_exists($file)
						);
				}

			}

		}

	}

	/**
	 * Перерка коректного підключення системних класів
	 *
	 * @return FALSE
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function test_include_settings_classes()
	{
		global $PData;

		$set_classes = $PData->GetSettings('set_classes');

		if (is_array($set_classes) && !empty($set_classes)) {
			echo '<h3>Системні класи</h3>';

			foreach ($set_classes as $key => $value) {
				$this->testResult(
						'UCode::GetSettings->set_classes',
						'Не правильно підключений системний клас - <u>'. $key . '</u>',
						FALSE,
						!file_exists(CORE_FOLDER . 'classes/' . $key . '.php')
					);
			}

		}

	}

	/**
	 * Перерка коректного підключення користувацьких бібліотек
	 *
	 * @return FALSE
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function test_include_settings_libs()
	{
		global $PData;

		$set_classes = $PData->GetSettings('set_libs');

		if (is_array($set_classes) && !empty($set_classes)) {
			echo '<h3>Бібліотеки</h3>';

			foreach ($set_classes as $key => $value) {
				$this->testResult(
						'UCode::GetSettings->set_classes',
						'Не правильно підключена бібліотека - <u>'. $key . '</u>',
						FALSE,
						!file_exists(APP_FOLDER . '_libs/' . $key . '/_' . $key . '.php')
					);
			}

		}

	}

	/**
	 * Перерка коректного підключення модулів
	 *
	 * @return FALSE
	 * @author PavloM
	 * @since UCode v1.2.3
	 **/
	public function test_include_settings_modules()
	{
		global $PData;

		$set_classes = $PData->GetSettings('set_modules');

		if (is_array($set_classes) && !empty($set_classes)) {
			echo '<h3>Модулі</h3>';

			foreach ($set_classes as $key => $value) {
				$this->testResult(
						'UCode::GetSettings->set_modules',
						'Не правильно підключений модуль - <u>'. $key . '</u>',
						FALSE,
						!file_exists(APP_FOLDER . '_modules/' . $key . '/_' . $key . '.php')
					);
			}

		}

	}
}

/*=================== Умова, при якій запускається тестувння ===================*/

if (isset($_SESSION['testing']) && $_SESSION['testing'] == 1) {
	_add_action('defined_core', 'start_code_testings');
}


/*=================== Функції, що використовуються при тестуванні ===================*/

/**
 * Запускає тестування коду після ініціалізації класів ядра
 *
 * @return string
 * @author PavloM
 * @since UCode v1.2.3
 **/
function start_code_testings(){
	global 	$PData;

	ob_start();

	echo '<h1 align="center">Testing results</h1>';

	$PData -> countTime('Time of testing');

	$Testings = new Testings();

	$PData -> countTime('Time of testing');
	$PData -> countTime();

	unset($_SESSION['testing']);

	echo '<form method="POST"> &emsp; <a href="'.PAGE_URL.'">~ Return on the page ~</a> &emsp; &emsp; <input type="submit" value="Test it again" name="runSystemTestings"></form>';

	$buffer = ob_get_contents();
	ob_end_flush();

	$PData -> writeLog($buffer,'testings/'.date('Y-m-d_H-i-s').'.htm');
	$PData -> phpExit();
}