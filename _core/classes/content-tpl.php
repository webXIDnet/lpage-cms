<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Робочий клас
 *
 ***/

class ContentTPL {

    public function getCommentsTree($comments,$lvl,$switch=FALSE){//формує дерево коментарів

        global $contTPL;
        global $user;
        $text='';

        if($switch){//виводить основну розмітку навколо дерева
            $text.='<ul class="comments">'.$contTPL->getCommentsTree($comments,$lvl).'
			<div class="new_com">'.@$_COOKIE["comment_no_author_text"].

                (@$user->rights<1?
                    '

				<div class="text-wapper" id="comment_no_author" style="padding-left: 0;">
					<form class="add_comment" method="GET">
						<input type="hidden" name="comment-parent2" id="comment-parent2" value="0"/>
						<textarea name="comment-text2" id="comment-text2"></textarea>
						<input onclick="submitForm2();" class="get_login_window" name="submit-comment2" type="submit" value="'._lang('Отправить').'"/>
					</form>
				</div>
                <script type="text/javascript">
	function submitForm2() {
		document.cookie = "comment_no_author_parrent="+document.getElementById("comment-parent2").value;
        document.cookie = "comment_no_author_text="+document.getElementById("comment-text2").value;
	}
    </script>
                ':

                    '<a class="author-ava" href="'.@$user->profile.'" target="_blank" title="'._lang('Ваша страница').'"><img width="50px" height="50px" src="'.@$user->photo.'" alt="'.@$user->fullname.'" align="left" /></a>
				<div class="text-wapper">
					<form class="add_comment" method="POST">
						<input type="hidden" name="comment-parent" value="0"/>
						<textarea name="comment-text"></textarea>
						<input  name="submit-comment" type="submit" value="'._lang('Отправить').'"/>
					</form>
				</div>'
                )

                .'
				</div>
			</ul>
			<script type="text/javascript">
				$(\'.answer\').css({display:\'none\'});
			</script>';
            return$text;
        }

        if(is_array($comments)){//формує дерево коменратів

            foreach($comments AS $key=>$val){

                $text.='<li id="comment_'.$val['com_id'].'" class="'.$val['u_network'].'">
				<div class="author-ava">
					<a href="'.$val['u_profile'].'" target="_blank" title="'._lang('Перейти на страницу пользователя').'"><img width="50px" height="50px" src="'.$val['u_photo'].'" alt="'.$val['u_fullname'].'" align="left" /></a>'.
                    (@$user->rights==3?

                        '<form class="del_comment" method="POST">
							<input name="comment-remove['.$val['com_id'].']" type="submit" value="'._lang('Удалить').'"/>
					</form>':
                        '')

                    .'
				</div>
				<div class="text-wapper">
					<a class="author-name" href="'.$val['u_profile'].'" target="_blank" title="'._lang('Перейти на страницу пользователя').'"><span class="name">'.$val['u_fullname'].'</span></a>

					<span class="text">'.preg_replace("/(([a-z]+:\/\/)?(?:[a-zа-я0-9@:_-]+\.)+[a-zа-я0-9]{2,4}(?(2)|\/).*?)([-.,:]?(?:\\s|\$))/is",'<a class="twit-url" href="$1" target="_blank" ref="nofollow">['._lang('ссылка').']</a>$3', $val['com_text']).'</span>
					<div class="text-footer">
						<span class="date" title="'.userDate($val['com_date'],'d.m.Y H:i:s',-1).'">
							'. _lang('Оставлен').' '.backCountDate($val['com_date'],-1).'
						</span> <span id="add_com_wap'.$val['com_id'].'">|
							<a id="'.$val['com_id'].'" class="add_com">'._lang('Ответить').'</a>
						</span>
					</div>
				</div>
				<div id="com_form'.$val['com_id'].'" class="new_com answer">'.

                    ($user->rights<1?

                        '<a class="get_login_window">'._lang('Войти через соц. сеть, чтобы оставить комментарий').'</a>':
                        '<a class="author-ava" href="'.$user->profile.'" target="_blank" title="'._lang('Ваша страница').'"><img width="50px" height="50px" src="'.$user->photo.'" alt="'.$user->fullname.'" align="left" /></a>

					<div class="text-wapper">
						<form class="add_comment" method="POST">
							<input type="hidden" name="comment-parent" value="'.$val['com_id'].'"/>
							<textarea name="comment-text"></textarea>
							<input name="submit-comment" type="submit" value="'._lang('Отправить').'"/>
						</form>
					</div>'
                    )

                    .'</div>
				';

                if(!empty($val['child'])){

                    $text.='<ul class="comments level-'.$lvl.'">';
                    $text.=$contTPL->getCommentsTree($val['child'],$lvl+1);
                    $text.='</ul>';

                }

                $text.='</li>';
            }

        }

        return $text;
    }

    public function catHierarchTree($array,$level=0,$actir_cat_id='',$display_non_cat_id=-1,$theme='select'){ //вертає іерархічну структуру категорій

        $text='';

        if($level==0){

            if(is_array($array)){

                foreach($array AS $cat){

                    $text .= $this->catHierarchTree($cat,$level+1,$actir_cat_id,$display_non_cat_id,$theme);

                }

            }

        }elseif(is_array($array)){


            if($display_non_cat_id==@$array['c_id'])return'';

            global $list;
            $pre='';

            switch($theme){

                case'full_list':

                    for($i=1; $i<$level;$i++){
                        $pre .= '- ';
                    }

                    $select='';
                    if($actir_cat_id==@$array['c_id']){
                        $select='selected="selected"';
                    }

                    $add = '';
                    if(!empty($array['c_lang'])){
                        $add = ' ('.$array['c_lang'].')';
                    }


                    $text='<option '.$select.' value="'.@$array['id'].'">'.$pre.@$array['c_title'].$add.'</option>';

                    break;

                case'select':

                    for($i=1; $i<$level;$i++){
                        $pre .= '- ';
                    }

                    $select='';
                    if($actir_cat_id==@$array['c_id']){
                        $select='selected="selected"';
                    }

                    $add = '';
                    if(!empty($array['c_lang'])){
                        $add = ' ('.$array['c_lang'].')';
                    }


                    $text='<option '.$select.' value="'.@$array['c_id'].'">'.$pre.@$array['c_title'].$add.'</option>';

                    break;

                case'query':

                    $text = " w_category='"._protect($array['c_id'])."'";

                    if($level>1)$text = ' OR '.$text ;



                    break;

                case'table':

                    for($i=1; $i<$level;$i++){
                        $pre .= '|&rarr; ';
                    }

                    $text.='
					<tr>
						<td>
							<input type="checkbox" name="delCat['.$array['c_id'].']"/> &nbsp;&nbsp;'.$array['c_id'].'
						</td>
						<td>
							'.$pre.'<a title="'._lang('Редактировать').'" href="'.getURL('admin','category-edit','id='.$array['c_id']).'">
								'.$array['c_title'].'</a>
						</td>
						<td>
							'.$array['c_url'].'
						</td>
						<td>
							<!-- Filter: app_admin_CatList_addData -->
							'._run_filter('app_admin_CatList_addData', '', $array).'
						</td>
					</tr>';

                    break;

            }

            if(isset($array['c_id']) && isset($list->category_index[$array['c_id']]['child']) && is_array($list->category_index[$array['c_id']]['child'])){

                foreach($list->category_index[@$array['c_id']]['child'] AS $child){

                    $text.=$this->catHierarchTree($child,$level+1,$actir_cat_id,$display_non_cat_id,$theme);

                }
            }
        }
        return $text;
    }

    public function questionHierarchTree($arrays,$level=0,$theme='select'){ //вертає іерархічну структуру категорій

        $text='';

        if(is_array($arrays)){

            global $list;
            $pre='';

            switch($theme){

                case'table':

                    for($i=1; $i<$level;$i++){
                        $pre .= '|&rarr; ';
                    }
                    foreach($arrays as $array)
                        $text.='
					<tr>
						<td>
							'.$array['question_user_name'].'
						</td>
						<td>
							'.$array['question_text'].'
						</td>
						<td>
                            '.$array['question_user_email'].'
						</td>
                        <td>
							<input type="checkbox" '.(($array['question_status_id']==1)?'checked':'').' name="publishedQuestion['.$array['question_id'].']"/> &nbsp;&nbsp;
						    <input type="hidden" name="publishedQuestion_all['.$array['question_id'].']" value="text">
						</td>
                        <td>
                            '.$pre.'<a title="'._lang('Ответить').'" href="'.getURL('admin','FAQs_send_answer','question='.$array['question_id']).'">'._lang('Ответить').'</a>
						</td>
					</tr>';

                    break;

                case'table_answer':

                    for($i=1; $i<$level;$i++){
                        $pre .= '|&rarr; ';
                    }
                    foreach($arrays as $array)
                        $text.='
					<tr>
						<td>
							'.$array['question_user_name'].'
						</td>
						<td>
							'.$array['question_text'].'
						</td>
                        <td>
							'.$array['answer_text'].'
						</td>
                        <td>
							<input type="checkbox" '.(($array['question_status_id']==1)?'checked':'').' name="publishedQuestion['.$array['question_id'].']"/> &nbsp;&nbsp;
						    <input type="hidden" name="publishedQuestion_all['.$array['question_id'].']" value="text">
						</td>
					</tr>';

                    break;

            }
        }
        return $text;
    }

    public function PostsList($posts){

    }
}

class SQLTPL{

    public function getAllCategories($type='',$compare='='){#вертає список категорій

        global $list, $meta;

        if(isset($list->category[$type]))return FALSE;

        global $sql;

        // $qtype = ' c_lang='. ($lang != '' ? $lang : $meta->val('md_lang')).' ';

        if(is_array($type)){

            foreach($type AS $val){

                if(!empty($qtype))$qtype.=' OR ';

                $qtype .= "c_type ".$compare." '"._protect($val)."'";

            }

        }elseif($type){

            $qtype = "c_type ".$compare." '"._protect($type)."'";

        }

        if(!empty($qtype)){
            $qtype = "WHERE ".$qtype;
        }

        $qtype = _run_filter('getCategories_SQL_query',$qtype);

        $result = $sql->query("
			SELECT *
			FROM _categories
			{$qtype}
			ORDER BY `c_parent_id` ASC, c_title
		");

        foreach($result AS $cval){

            if($cval['c_parent_id'] == 0) {
                $list->category[$cval['c_type']][] = $cval;
                $list->category[$cval['c_type']][sizeof($list->category[$cval['c_type']])-1]['child'] = array();
                $list->category_index[$cval['c_lang']][$cval['c_id']] = &$list->category[$cval['c_type']][sizeof($list->category[$cval['c_type']])-1];
            }else{
                $list->category_index[$cval['c_parent_id']]['child'][] = $cval;
                $list->category_index[$cval['c_id']] = &$list->category_index[$cval['c_parent_id']]['child'][sizeof($list->category_index[$cval['c_parent_id']]['child'])-1];
            }

        }
    }

    public function getCategories($type='',$compare='='){#вертає список категорій

        global $list, $meta;

        if(isset($list->category[$type]))return FALSE;

        global $sql;

        // $qtype = ' c_lang='. ($lang != '' ? $lang : $meta->val('md_lang')).' ';

        if(is_array($type)){

            foreach($type AS $val){

                if(!empty($qtype))$qtype.=' OR ';

                $qtype .= "c_type ".$compare." '"._protect($val)."'";

            }

        }elseif($type){

            $qtype = "c_type ".$compare." '"._protect($type)."'";

        }

        if(!empty($qtype)){
            $qtype = "WHERE ".$qtype;
        }

        $qtype = _run_filter('getCategories_SQL_query',$qtype);

        $result = $sql->query("
			SELECT *
			FROM _categories
			{$qtype}
			ORDER BY `c_parent_id` ASC, c_title
		");

        foreach($result AS $cval){

            if($cval['c_parent_id'] == 0) {
                $list->category[$cval['c_type']][$cval['c_lang']][] = $cval;
                $list->category[$cval['c_type']][$cval['c_lang']][sizeof($list->category[$cval['c_type']][$cval['c_lang']])-1]['child'] = array();
                $list->category_index[$cval['c_lang']][$cval['c_id']] = &$list->category[$cval['c_type']][$cval['c_lang']][sizeof($list->category[$cval['c_type']][$cval['c_lang']])-1];
            }else{
                $list->category_index[$cval['c_lang']][$cval['c_parent_id']]['child'][] = $cval;
                $list->category_index[$cval['c_lang']][$cval['c_id']] = &$list->category_index[$cval['c_lang']][$cval['c_parent_id']]['child'][sizeof($list->category_index[$cval['c_lang']][$cval['c_parent_id']]['child'])-1];
            }

        }
    }

    public function getPosts($key = 'all_posts',$where='WHERE p_cat_id>0', $type='array', $custom_filter = ''){

        global 	$sql,
                  $list;

        if(!isset($list->posts[$key]) && $type=='array'){

            $result = $sql->query("
				SELECT *
				FROM _posts
				{$where} "._protect($custom_filter).""
            );

            if(is_array($result)){
                foreach($result AS $k => $post){

                    $list->posts[$key][$post['p_id']] = $post;
                    $list->posts[$key][$post['p_id']]['p_imgs'] = json_decode($post['p_imgs'],TRUE);

                    if(is_array($list->posts[$key][$post['p_id']]['p_imgs'])){
                        foreach($list->posts[$key][$post['p_id']]['p_imgs'] AS $i => $imgs){
                            if(is_array($imgs)){
                                foreach($imgs AS $i_1 => $img){
                                    $list->posts[$key][$post['p_id']]['p_imgs'][$i_1] = HOMEPAGE . $img;
                                }
                            }else{
                                $list->posts[$key][$post['p_id']]['p_imgs'][$i] = HOMEPAGE . $imgs;
                            }
                        }
                    }
                }
            }
            if(isset($list->posts[$key])){
                return $list->posts[$key];
            }else{
                return FALSE;
            }

        }elseif($type=='value'){

            if(!empty($where)){
                if($key=='post'){
                    $where = "
						INNER JOIN _categories
						ON p_cat_id=c_id
						WHERE p_cat_id>0 AND p_article='"._protect($where)."' "._protect($custom_filter)."";

                }elseif($key=='page'){
                    $where = "
						WHERE p_cat_id=-1 AND p_article='"._protect($where)."' "._protect($custom_filter)."";

                }elseif(!empty($where)){
                    $where = "
						WHERE {$where} "._protect($custom_filter)."";
                }
            }

            $post = $sql->query("
				SELECT *,P.id as pid
				FROM _posts AS P
				{$where} "._protect($custom_filter)."",
                'value'
            );

            if(is_array($post)){

                $list->posts[$post['p_id']] = $post;
                $list->posts[$post['p_id']]['p_imgs'] = json_decode($post['p_imgs'],TRUE);

                if(is_array($list->posts[$post['p_id']]['p_imgs'])){
                    foreach($list->posts[$post['p_id']]['p_imgs'] AS $i => $imgs){
                        if(is_array($imgs)){
                            foreach($imgs AS $i_1 => $img){
                                $list->posts[$post['p_id']]['p_imgs'][$i_1] = HOMEPAGE . $img;
                            }
                        }else{
                            $list->posts[$post['p_id']]['p_imgs'][$i] = HOMEPAGE . $imgs;
                        }
                    }
                }
                return $list->posts[$post['p_id']];

            }else{
                return FALSE;
            }

        }elseif(isset($list->posts[$key])){
            return $list->posts[$key];
        }else{
            return FALSE;
        }
    }

}

$sqlTPL = new SQLTPL;
$contTPL = new ContentTPL;