<?php 
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 * 
 * Класи
 *  
***/

class HTMLTemplates{
	
	public function table($array,$title='',$class='',$id='',$settings='',$button_value='Сохранить'){
		
		$text = '';
		
		if(is_array($array)){
			
			$col_id = 0;
			$text = '
			<form id="'.$id.'" method="POST" class="form-'.$class.'">
				<table class="'.$class.'">
				<tbody>';
			
			if(is_array($title)){
				
				foreach($title AS $c => $col){
					
					$text .= '<th id="head-'.$id.'" class="head head'.$c.' '.$id.$c.'">'.$col.'</th>';
										
				}
				
			}
			
			foreach($array AS $row){
				
				$c=0;
				$text .= '<tr>';
				
				foreach($row AS $key=>$col){
					
					$t = $col;
					$cl = '';
					
					if($c==0 && $settings['delRow']==1){
						$t = '<input type="checkbox" name="delRow['.$col.']"/> &nbsp;&nbsp;<span>'.$col.'</span>';
						$cl = 'numer';
					}
					
					$text .= '<td id="'.$id.$col_id.'" name="'.$key.'" class="'.$cl.' col col'.$c.' '.$id.$c.'">'.$t.'</td>';
					$col_id++;
					$c++;
										
				}
				
				$text .= '</tr>';
				
			}
			
			$text .= '</tbody>
				</table>
				<input class="btn btn-primary" type="submit" value="'._lang($button_value).'" name="formsubmit-'.$id.'">
			</form>';
			
		}
		
		return $text;
	}
}

$htmlTPL = new HTMLTemplates();