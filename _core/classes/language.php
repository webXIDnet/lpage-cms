<?php
if(!defined('HOMEPAGE')){header('HTTP/1.0 404 not found');exit();} // захист від прямих звернень до файлу
/**
 *
 * Модуль Підключення мовного файлу
 *
***/

class Language{
	public $translate;
	public $user_trans;

	public function GetTranslation($key=FALSE){

		global 	$user,
				$PData;

		if(!empty($user->lang)){
			$lang = $user->lang;
		}else{
			$user->lang = $lang = $PData->GetSettings('lang');
		}

		if(empty($this->translate) && !is_array($this->translate)){

			global $PData;

			if($PData->checkFile(APP_FOLDER.'_languages/location_'.$lang.'.php')){
				$this->translate = require_once APP_FOLDER.'_languages/location_'.$lang.'.php';
			}
		}

		if($key){
			return @$this->translate[$key];
		}
	}
	public function GetUserTranslation($key=FALSE){

		global 	$user,
				$PData;

		if(!empty($user->lang)){
			$lang = $user->lang;
		}else{
			$user->lang = $lang = $PData->GetSettings('lang');
		}

		if($key){
			return @$this->user_trans[$lang][$key];
		}
	}
	public function addTranslations($array){
		foreach($array AS $lang => $arr){
			$this->add($lang,$arr);
		}
	}

	public function add($lang,$key,$val=''){

		if(is_array($key)){
			foreach($key AS $key=>$val){
				$this->user_trans[$lang][$key]=$val;
			}
		}else{
			$this->user_trans[$lang][$key]=$val;
		}
	}
}

$LData = new Language();

function _lang($str,$echo=FALSE){

	global $LData;

	$translate = '';//$LData->GetTranslation($str);
	$user_trans = '';//$LData->GetUserTranslation($str);

	if($user_trans){
		if(!$echo)return $user_trans ;
		echo $user_trans ;
	}elseif($translate){
		if(!$echo)return $translate;
		echo $translate;
	}else{
		if(!$echo)return $str;
		echo $str;
	}
}