<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * SQL queries
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

class SQLQuery
{
	public $pdo;

	public function __construct()
	{
		global $PData;

		$db = $PData->GetSettings('set_db');
		$dsn = "mysql:host=".$db['host'].";dbname=".$db['name'].";charset=UTF8";
		$opt = array(
		    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		);

		try {
    		$this->pdo = new PDO($dsn, $db['user'], $db['pass'], $opt);
		} catch (PDOException $error) {
			_dump(iconv('cp1251', 'utf-8', $error->getMessage()) ."\n".$error->getTrace()[0]['function'] . '() -> <b>' . $error->getFile() . '</b> in line <b>' . $error->getLine() . "</b>" , 'SQL connection error', 1);
		}

	}

	public function SQLConnect()
	{
		global $PData;

		$db = $PData->GetSettings('set_db');

		mysql_connect($db['host'],$db['user'],$db['pass'],true)or die (mysql_error());
		mysql_select_db($db['name']);
		mysql_query("SET NAMES utf8");

	}

	/**
	 * Make sql query throught PDO
	 * @param  string  $query string of query
	 * @param  string  $type  type of return vars
	 * @param  boolean $vars  array of vars in query
	 * @example
	 * 	$this->query('SELECT id, name FROM users WHERE email != ?', $type='array:FETCH_KEY_PAIR', $vars=array('my@email.com'));
	 *
	 * @return array/object
	 *
	 * @since UCode v1.2.6
	 */
	public function query($query, $type='array', $vars_array=FALSE)
	{
		$result = $this->pdo->prepare($query);


		if (is_array($vars_array)) {
			foreach ($vars_array AS $key=>$value) {
				if (!$result->bindValue(':'.$key, $value)) {
					_dump('Ошибка при подготовке переменной '.$key.'=>'.$value."\n".$query, 'SQL query error', 1);
				}
			}
		}

		if (!$result->execute()) {
			_dump('Ошибка при выполнении метода  execute()'."\n".$query, 'SQL query error', 1);
		}

		$fetch = explode(':', $type);
		$type = @$fetch[0];
		if (isset($fetch[1])) {
			$fetch = $fetch[1];
		} else {
			$fetch = '';
		}

		switch($type){
			case'query':
				return $result;

				break;

			case'array':
				if (empty($fetch)) {
					$fetch = 'FETCH_BOTH';
				}

				$array=array();

				while($cval = $result->fetch(constant('PDO::'.$fetch))){
					$array[] = $cval;
				}

				return $array;

				break;

			case'value':
				if (empty($fetch)) {
					$fetch = 'FETCH_ASSOC';
				}

				return $result->fetch(constant('PDO::'.$fetch));

				break;
		}

		return $result;
	}

	/**
	 * Update $vars_array in $table where $where
	 * @param  string $table - table name
	 * @param  string $where - condition for updation.
	 * @param  array $vars_array - all variables of query
	 * @param  string $prefix  - column name prefix of updating columns
	 * @example
	 * 		$vars_array = array(
	 *	    	'w:p_id' => 1, //variable from WHERE in query
	 *	    	'w:p_lang' => '%ru%',
	 *		    'title' => 'My post'
	 *      );
	 * 		$this -> update('_posts', "p_id=:p_id AND p_lang LIKE :p_lang", $vars_array, $prefix='p_');
	 *
	 * @return boolean
	 *
	 * @since UCode v1.2.6
	 *
	 */
	public function update($table, $where_query, $vars_array, $prefix='')
	{
		$update_query = '';
		$query_vars = array();

		if (is_array($vars_array)) {
			$i = 0;
			foreach ($vars_array AS $key=>$value) {
				$var = explode(':', $key);

				if (count($var)==1) {
					if (!empty($update_query)) {
						$update_query .= ',';
					}

					$update_query .= "$prefix$key = :val$i";
					$query_vars[':val'.$i] = $value;
				} else {
					$key = $var[1];
					$vars_array[$key] = $value;
					$query_vars[':'.$key] = $value;
				}

				unset($var);
				$i++;
			}
		}

		$query = "UPDATE $table SET $update_query WHERE $where_query;";
		$result = $this->pdo->prepare($query);
		$result->execute($query_vars);

		return TRUE;
	}

	/**
	 * Insert or Replayce $vars_array in $table where $where
	 * @param  string $table - table name
	 * @param  string $where - condition for insert.
	 * @param  array $vars_array - all variables of query
	 * @param  string $prefix  - column name prefix of inserting columns
	 * @example
	 * 		$vars_array = array(
	 *	    	'lang' => 2,
	 *		    'title' => 'My post'
	 *      );
	 * 		$this -> insert('_posts', $vars_array, $prefix='p_');
	 *
	 * @return boolean
	 *
	 * @since UCode v1.2.6
	 *
	 */
	public function insert($table, $vars_array, $prefix='', $replayce=FALSE, $return_id=FALSE)
	{

		$update_query = $columns = $col_values = '';
		$query_vars = array();

		if (is_array($vars_array)) {
			$i = 0;
			foreach ($vars_array AS $key => $value) {
				if (!empty($columns)) {
					$columns .= ', ';
				}

				$columns .= $prefix.$key;


				if (!empty($col_values)) {
					$col_values .= ',';
				}

				$col_values .= ":val$i";


				if (!empty($update_query)) {
					$update_query .= ', ';
				}

				$update_query .= "$prefix.$key = :val$i";
				$query_vars[':val'.$i] = $value;
				$i++;
			}
		}

		if($replayce){
			$query = "INSERT INTO $table ($columns) VALUES ($col_values) ON DUPLICATE KEY UPDATE $update_query;";
		}else{
			$query = "INSERT INTO $table
			($columns)
			VALUES ($col_values);";
		}

		$result = $this->pdo->prepare($query);

		if (!$result->execute($query_vars)) {
			_dump('Ошибка при выполнении метода  execute()'."\n".$query.' in table '. $table, 'SQL insert error', 1);
		}

		if($return_id){
			return $this->pdo->lastInsertId();
		}

		return TRUE;
	}

	/**
	 * Delate rows in $table
	 * @param  string $table - table name
	 * @param  string $where - condition for delete.
	 * @param  array $vars_array - all variables of query
	 * @param  string $prefix  - column name prefix of deleting columns
	 * @example
	 * 		$vars_array = array(
	 *	    	'lang' => 2,
	 *		    'title' => 'My post'
	 *      );
	 * 		$this -> delete('_posts', 'p_id = :p_id AND p_lang = :p_lang', $vars_array, $prefix='p_');
	 *
	 * @return boolean
	 *
	 * @since UCode v1.2.6
	 *
	 */
	public function delete($table, $where, $vars_array=FALSE, $prefix='')
	{
		$query_vars = array();

		if (is_array($vars_array)) {
			foreach ($vars_array AS $key => $value) {
				$query_vars[':'.$prefix.$key] = $value;
			}
		}

		if(!empty($where)) {
			$query = "DELETE FROM $table WHERE {$where}";
		}

		$result = $this->pdo->prepare($query);

		if (!$result->execute($query_vars)) {
			_dump('Ошибка при выполнении метода  execute()'."\n".$query, 'SQL delete error', 1);
		}

		return TRUE;
	}

	/**
	 * Replace columns in all $table
	 * @param  string $table - table name
	 * @param  string $cols - updating columns.
	 * @param  string $str - searching string
	 * @param  string $replace_str - replacing string
	 * @param  string $prefix  - column name prefix of updating columns
	 * @example
	 * 		$cols = array(
	 *	    	'lang', 'title'
	 *      );
	 * 		$this -> replace(''_posts, $cols, $search_str = 'ru', $replace_str = 'ua', $prefix = 'p_');
	 *
	 * @return boolean
	 *
	 * @since UCode v1.2.6
	 *
	 */
	public function replace($table, $cols=array(), $search_str, $replace_str, $prefix = '')
	{

		$replace = '';
		$query_vars = array();
		$i = 0 ;

		foreach($cols AS $val){
			if(!empty($replace)) {
				$replace .= ', ';
			}

			$replace .= ":val$i = REPLACE(:val$i, :val".$i."_1, :val".$i."_2)";
			$query_vars[':val'.$i] = $prefix.$val;
			$query_vars[':val'.$i.'_1'] = $search_str;
			$query_vars[':val'.$i.'_2'] = $replace_str;
			$i++;
		}

		if(!empty($replace)){
			$query = "UPDATE $table SET $replace;";
			$result = $this->pdo->prepare($query);

			if (!$result->execute($query_vars)) {
				_dump('Ошибка при выполнении метода  execute()'."\n".$query, 'SQL replace error', 1);
			}
			return TRUE;
		}
		return FALSE;
	}

	/**
     * @param $module
     * @param $version_date
     * @param $tables_columns
     * @return bool
     */
    public function db_table_installer($module, $version_date, $tables_columns)
    {
		global 	$meta,
				$sql,
                $PData;

		$module_db = $meta->val('install_db','mod_option');
		//echo $module;
		if(!isset($module_db[$module]) || @$module_db[$module]!=$version_date){

			foreach($tables_columns AS $table => $columns){

				$queries = '';
				if(!empty($columns['CREATE']) && !empty($columns['KEY'])){
                    /*print_r("CREATE TABLE IF NOT EXISTS `"._protect($table)."`
						(".@$columns['CREATE']. (@$columns['KEY'] ? ', '.@$columns['KEY'] : '') .")
						ENGINE=InnoDB DEFAULT CHARSET=utf8;
						");*/
					$sql->query(
						"CREATE TABLE IF NOT EXISTS `"._protect($table)."`
						(".@$columns['CREATE']. ($columns['KEY'] ? ', '.@$columns['KEY'] : '') . ")
						ENGINE=InnoDB DEFAULT CHARSET=utf8;
						",
						'query'
					);
				} else {
					continue;
				}

				$keys = @$columns['KEY'] ? @$columns['KEY'] : '';
				$columns['CREATE'] = '';
				unset($columns['CREATE']);
                $columns['KEY'] = '';
                unset($columns['KEY']);
                $additional_commands=isset($columns['ADDITIONAL_COMMANDS'])? $columns['ADDITIONAL_COMMANDS']: false;
                $columns['ADDITIONAL_COMMANDS'] = '';
                unset($columns['ADDITIONAL_COMMANDS']);

                $db = $PData->GetSettings('set_db');
				$result = $sql->query(
					"SELECT COLUMN_NAME
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE table_name = '"._protect($table)."' AND table_schema = '"._protect($db['name'])."'"
				);


				$tables_columns = array();

				foreach($result AS $cval){
					foreach($cval AS $val){
						$tables_columns[$val]='1';
					}
				}

				foreach($columns AS $key => $val){
					if(!isset($tables_columns[$key])){
						if(!empty($queries)) $queries .= ",\n";
						$queries .= " ADD ".$val;
					}
				}

				if(!empty($queries)){
					$queries = "ALTER TABLE "._protect($table)." ".$queries . ($keys != '' ? ", ADD ".$keys : "") . ";";
//                    print_r($queries);exit;
					$sql->query($queries,'query');
				}
                if($additional_commands){
                    foreach($additional_commands AS $query) {
                        $sql->query($query, 'query');
                    }
                }

				$module_db[$module] = $version_date;

				$meta->add_update('install_db', $module_db, 'mod_option');
			}
			return TRUE;
		}
		return FALSE;
	}
}

$sql = new SQLQuery();

