<?php
/**
 * Created for LPageCMS.
 * User: BoHand
 * Date: 16.06.2015
 * Time: 17:29
 * Version 0.9
 * Youtube functionality
 */
class YouTubeClass
{
    /**
     * @param $link   - link to video something like this 'https://www.youtube.com/watch?v=JCYIwiT0X98'
     * @param $width  - width of videoPlayer
     * @param $height - height of videoPlayer
     * @return string - html code
     */
    public function getVideoYoutubePlayer($id, $width = 'auto', $height = 'auto')
    {

        // if (PAGE_TYPE == 'admin') {
        //     return '[:videoYoutube ' . $link . ':]';
        // }
        // $link = parse_url($link);
        // parse_str($link['query'], $get);
        // $video_id = $get['v'];

        // @set_time_limit(0);
        // $id = $video_id; //The youtube video ID
//        $types_array = array(
//            'video/mp4',
//            'video/webm',
//        ); //the MIME type of the video
        parse_str(file_get_contents('http://www.youtube.com/get_video_info?video_id=' . $id . ''), $info);
        $streams = explode(',', $info['url_encoded_fmt_stream_map']);
        $video_source = '';
        foreach ($streams as $stream) {
            parse_str($stream, $real_stream);
            if (!isset($real_stream['type'])) {
                $video_html = '<div style="background-color: #E5E5E5; text-align: center; font-size: 25px; border-radius: 10px; width: 100%; height: 200px; line-height: 200px;">Это видео в обработке</div>';
                return $video_html;
            }
            $stream_type = $real_stream['type'];
            if (strpos($real_stream['type'], ';') !== false) {
                $tmp = explode(';', $real_stream['type']);
                $stream_type = $tmp[0];
                unset($tmp);
            }
            if ($real_stream['itag'] == 22) {
                $video_source .= '<source data-res="HD" type="' . $stream_type . '" src="' . $real_stream['url'] . '"/>'
                    . "\n";
            }
            if ($real_stream['itag'] == 18) {
                $video_source
                    .=
                    '<source data-default="true" data-res="SD" type="' . $stream_type . '" src="' . $real_stream['url']
                    . '" />' . "\n";
            }
        }

        $plugin_data_dir = '//' . $_SERVER['SERVER_NAME'] . '/plugins/youtube-video-player';
        $video_html = '<video id="video_' . $id
            . '" class="video-js vjs-default-skin" controls preload="none" poster="http://img.youtube.com/vi/'
            . $id . '/hqdefault.jpg" width="' . $width . '" height="' . $height . '" class="video-js vjs-default-skin">
                        ' . $video_source . '
                        </video>
                        <link href="' . $plugin_data_dir . '/css/video-js.min.css" rel="stylesheet">
                        <script src="' . $plugin_data_dir . '/js/video.dev.js"></script>

                        <link href="' . $plugin_data_dir . '/css/video-js-resolutions.css" rel="stylesheet" type="text/css" />
                        <script type="text/javascript" src="' . $plugin_data_dir . '/js/video-js-resolutions.js"></script>
                        <script type="text/javascript">
                            vjs(\'video_' . $id . '\', {
                                plugins: {
                                    resolutions: true
                                }
                            });
                        </script>';
        return $video_html;
    }

    /**
     * @param $videoPath - path to video file on server
     * @param $videoTitle - video title
     * @param $videoDescription - video description
     * @param $videoPrivacy - Valid values are 'public', 'private', and 'unlisted'. https://developers.google.com/youtube/v3/docs/videos/insert
     * @param $videoCategory - video category
     * @param $videoTags - string contained tags separated by ',' something like this 'teg1,teg2'
     * @return string contain YouTube video id
     */
    public function uploadVideo($videoPath,$videoTitle,$videoDescription='',$videoPrivacy='private',$videoCategory='22',$videoTags='')
    {
        $videoTags = explode(',',$videoTags);//convert to array
        set_include_path(
            get_include_path() . PATH_SEPARATOR . ROOT_FOLDER . '/_applications/_libs/GoogleApiLib'
        );
        require_once 'autoload.php';
        //load configs
        global $meta;
        $meta->getAll();
        $config = $meta->val('YouTubeConfig', 'mod_option');
        $config = $config['config'];
        $key = $meta->val('YouTube_AccessToken', 'mod_option');

        $application_name = $config['APPNAME'];
        $client_secret = $config['CLIENT_SECRET'];
        $client_id = $config['CLIENT_ID'];
        $scope = array('https://www.googleapis.com/auth/youtube.upload', 'https://www.googleapis.com/auth/youtube',
                       'https://www.googleapis.com/auth/youtubepartner');
        try {
            // Client init
            $client = new Google_Client();
            $client->setApplicationName($application_name);
            $client->setClientId($client_id);
            $client->setAccessType('offline');
            $client->setAccessToken(json_encode($key));
            $client->setScopes($scope);
            $client->setClientSecret($client_secret);
            if ($client->getAccessToken()) {
                /**
                 * Check to see if our access token has expired. If so, get a new one and save it to file for future use.
                 */

                if ($client->isAccessTokenExpired()) {
//                    $newToken = json_decode($client->getAccessToken());
                    $client->refreshToken($meta->val('YouTube_RefreshToken', 'mod_option'));
                    $meta->add_update('YouTube_AccessToken', $client->getAccessToken(), 'mod_option');
                }
                $youtube = new Google_Service_YouTube($client);
                // Create a snipet with title, description, tags and category id
                $snippet = new Google_Service_YouTube_VideoSnippet();
                $snippet->setTitle($videoTitle);
                $snippet->setDescription($videoDescription);
                $snippet->setCategoryId($videoCategory);
                $snippet->setTags($videoTags);

                // Create a video status with privacy status. Options are "public", "private" and "unlisted".
                $status = new Google_Service_YouTube_VideoStatus();
                $status->setPrivacyStatus($videoPrivacy);

                // Create a YouTube video with snippet and status
                $video = new Google_Service_YouTube_Video();
                $video->setSnippet($snippet);
                $video->setStatus($status);

                // Size of each chunk of data in bytes. Setting it higher leads faster upload (less chunks,
                // for reliable connections). Setting it lower leads better recovery (fine-grained chunks)
                $chunkSizeBytes = 1 * 1024 * 1024;

                // Setting the defer flag to true tells the client to return a request which can be called
                // with ->execute(); instead of making the API call immediately.
                $client->setDefer(true);

                // Create a request for the API's videos.insert method to create and upload the video.
                $insertRequest = $youtube->videos->insert('status,snippet', $video);

                // Create a MediaFileUpload object for resumable uploads.
                $media = new Google_Http_MediaFileUpload(
                    $client,
                    $insertRequest,
                    'video/*',
                    null,
                    true,
                    $chunkSizeBytes
                );
                $media->setFileSize(filesize($videoPath));


                // Read the media file and upload it chunk by chunk.
                $status = false;
                $handle = fopen($videoPath, 'rb');
                while (!$status && !feof($handle)) {
                    $chunk = fread($handle, $chunkSizeBytes);
                    $status = $media->nextChunk($chunk);
                }

                fclose($handle);

                /**
                 * Video has successfully been upload, now lets perform some cleanup functions for this video
                 */
                if ($status->status['uploadStatus'] == 'uploaded') {
                    // Actions to perform for a successful upload
                    //echo "Ulpoad Sucsess VideId=" . $status['id'];
                    return $status['id'];
                }
                // If you want to make other calls after the file upload, set setDefer back to false
                // $client->setDefer(true);
                // return true;
            } else {
                // @TODO Log error
                echo 'Problems creating the client';
                return false;
            }
        } catch (Google_Service_Exception $e) {
            print 'Caught Google service Exception ' . $e->getCode() . ' message is ' . $e->getMessage();
            print 'Stack trace is ' . $e->getTraceAsString();
            return false;
        } catch (Exception $e) {
            print 'Caught Google service Exception ' . $e->getCode() . ' message is ' . $e->getMessage();
            print 'Stack trace is ' . $e->getTraceAsString();
            return false;
        }
    }
}
