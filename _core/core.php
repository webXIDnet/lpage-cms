<?php if ( ! defined('HOMEPAGE')) {header('HTTP/1.0 404 not found');exit();} #protection against direct appeals to file
/**
 * Core of UpperCode
 *
 * @link http://ucode.lpagecms.com/
 * @license http://ucode.lpagecms.com/license.md
 * @copyright Copyright (c) 2013 Pavlo Matsura (http://fb.com/PavloMatsura)
 * @author Pavlo Matsura <webxid@ukr.net>
 *
 */

# URL of current page
define('PAGE_URL', 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);

# URL of current page without $_GET variables
$url = explode('?',PAGE_URL);
define('CURRENT_PAGE', array_shift($url));
unset($url);

# Absolute server path to the core folder
define('CORE_FOLDER', __DIR__ . '/');

# Framework vercion
define('UPPERCODE_VERSION', 'UpperCode v1.2.7');

#Set session
session_start();
if(!isset($_SESSION['id']) || empty($_SESSION['id'])){
	$_SESSION['id']='0';
}


global $hook;

$hook = array(
	'action'=>array(),
	'filter'=>array()
);

require_once CORE_FOLDER.'_fn.php'; #connection core functions

set_error_handler('ucode_error_treker');

function ucode_error_treker($errno, $errstr, $errfile, $errline, $err_5)
{
	_dump(array($errno, $errstr, $errfile.':'. $errline, //$err_5
		), 'Error: '.$errstr, 1);
}

require_once CORE_FOLDER.'_ucode.php'; #connection core class
require_once CORE_FOLDER.'_controller.php'; #connection core controller


#Script exit
$PData->phpExit();