<?php

#Print all errors
ini_set("display_errors",1);
error_reporting(E_ALL);
ini_set('default_charset', 'UTF-8');

#URL of homepage
define('HOMEPAGE', 'http://'.$_SERVER['SERVER_NAME'].'/');

#URL of the basic site folder
define('SITE_FOLDER', 'http://'.$_SERVER['SERVER_NAME'].'/');

#Absolute server path to the application folder
define('APP_FOLDER', __DIR__.'/_applications/');

#Absolute server path to the custom template folder
define('VIEW_FOLDER', __DIR__.'/templates/');

#Require the framework
require_once '_core/core.php';
