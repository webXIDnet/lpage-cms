$(function() {
	var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;

		var links = this.el.find('.link');
		
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
			$this = $(this),
			$next = $this.next();

		$next.slideToggle();
		$this.parent().toggleClass('open');

		if (!e.data.multiple) {
			$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
		};
	}	

	var accordion = new Accordion($('#accordion'), false);
});

$(document).ready(function() {
	var live = false;
	$('.sidebar-button').click(function(event) {
		if (live == false) {
			$('.sidebar').animate({left: 0}, 400);
			$('.sidebar-button a i').css({
				'-webkit-transform' : 'rotate(180deg)',
						'transform' : 'rotate(180deg)',
					'-ms-transform' : 'rotate(180deg)',
					 '-o-transform' : 'rotate(180deg)'
			});
			live = true;
		} else {
			$('.sidebar').animate({left: '-100%'}, 400);
			$('.sidebar-button a i').css({
				'-webkit-transform' : 'rotate(0deg)',
						'transform' : 'rotate(0deg)',
					'-ms-transform' : 'rotate(0deg)',
					 '-o-transform' : 'rotate(0deg)'
			});
			live = false;
		}
	});	
});