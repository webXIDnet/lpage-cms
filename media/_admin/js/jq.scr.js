function updateCoords(c)
{
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
}

function checkCoords()
{
    if (parseInt($('#w').val())>0) return true;
    alert('Please select a crop region then press submit.');
    return false;
}

(function($){



$(function(){
/* --------------------------------- */

$(document).ready(function(){
    
	
	    
    $('#sidebar>div').each(function(){
    	if($(this).find('ul.submenu li').length>0){
    		$(this).addClass('parent-menu');
    	}
    }).ready(function(){
    	 $('#sidebar>div.parent-menu>a').click(function(){
    	 		
    	 		if($(this).parent().hasClass('menu-active')){
					if($(this).parent().children('ul.submenu').length>0){
		    		$(this).click(function(){return false;}).parent().removeClass('menu-active').find('ul.submenu').slideUp('slow');
		    	}
		    	}else{
		    		if($(this).parent().children('ul.submenu').length>0){
			    		$(this).click(function(){return false;}).parent().addClass('menu-active').find('ul.submenu').slideDown('slow');
			    	}
		    	}
		    	return false;
			}
		);
		jQuery('#sidebar>div a').each(function(){
			str = location.href;
			url = jQuery(this).attr('href');
					
			if(str==url){
				//jQuery(this).parents('.next').addClass('next-active').show('slow').prev('li').addClass('active');
				jQuery(this).addClass('active').parents('.parent-menu').addClass('menu-active').find('ul.submenu').slideDown('slow');
			}			
		});
    });
    
    $('.close').click(function(){
    	$(this).parents('.messageBox').remove();
    });
    
    $('#tableList .col').click(function(){
    	if($(this).hasClass('numer')) return;
    	
    	$text = '';
    	
    	$(this).parent().children('td').each(function(){
    		
    		if($(this).hasClass('numer')){
    		
				$text += '<input type="hidden" name="editRowId" value="'+$(this).children('span').text()+'">';
    		
			}else{
    		
				$text += '<b>'+$(this).attr('name')+':</b><br/><textarea class="input" name="editRow['+$(this).attr('name')+']">'+$(this).text()+'</textarea><br/>';
    		
			}
    	
		});
    	
    	popup($text);
    	
    });
    
    
    $('#tableEdit .col').click(function(){
    	if(!$(this).parent().find(".tableEdit_form").length) return;
		text = $(this).parent().find(".tableEdit_form").html();
		popup(text);
		
		$('.tabs_block .tab_box').each(function(){
			$eq = $(this).index();
			if($eq==1){
				$(this).addClass('active');
				id = $(this).attr('id');
				$('[switchtab="'+id+'"]').addClass('active');
			}
			else return;
		});
		
		$('.tabs_block .tabs_list li').click(function(){
			id = $(this).attr('switchtab');
			$(this).attr('switched','1');
			$(this).parent().children(".active").removeClass('active');
			$(this).addClass('active');
			$(this).parents('.tabs_block').children(".tab_box").fadeOut(0).hide(0);
			$(this).parents('.tabs_block').children('#'+id).fadeIn(500).show(0);
			return false;
		}).ready(function(){
			$(this).delay(1000).removeAttr('switched');
		});
	});
    
    
    $('.tabs_block .tabs_list li').click(function(){
		id = $(this).attr('switchtab');
		$(this).parent().children(".active").removeClass('active');
		$(this).addClass('active');
		$(this).parents('.tabs_block').children(".tab_box").fadeOut(0).hide(0);
		$(this).parents('.tabs_block').children('#'+id).fadeIn(500).show(0);
		return false;
	});
    

	if($('#cropbox').length>0){
	
		$('#cropbox').Jcrop({
			minSize: [310,210],
			onSelect: updateCoords
		});
		
	}
	if($('#cropboxCategory').length>0){
		$('#cropboxCategory').Jcrop({
			aspectRatio: 310/210,
			minSize: [310,210],
			onSelect: updateCoords
		});
	}


});
/* --------------------------------- */


/* --------------------------------- */ 
/**
 * Converts the given data structure to a JSON string.
 * Argument: arr - The data structure that must be converted to JSON
 * Example: var json_string = array2json(['e', {pluribus: 'unum'}]);
 * 			var json = array2json({"success":"Sweet","failure":false,"empty_array":[],"numbers":[1,2,3],"info":{"name":"Binny","site":"http:\/\/www.openjs.com\/"}});
 * [url]http://www.openjs.com/scripts/data/json_encode.php[/url]
 */
function array2json(arr) {
    var parts = [];
    var is_list = (Object.prototype.toString.apply(arr) === '[object Array]');

    for(var key in arr) {
    	var value = arr[key];
        if(typeof value == "object") { //Custom handling for arrays
            if(is_list) parts.push(array2json(value)); /* :RECURSION: */
            else parts[key] = array2json(value); /* :RECURSION: */
        } else {
            var str = "";
            if(!is_list) str = '"' + key + '":';

            //Custom handling for multiple data types
            if(typeof value == "number") str += value; //Numbers
            else if(value === false) str += 'false'; //The booleans
            else if(value === true) str += 'true';
            else str += '"' + value + '"'; //All other things
            // :TODO: Is there any more datatype we should be in the lookout for? (Functions?)

            parts.push(str);
        }
    }
    var json = parts.join(",");
    
    if(is_list) return '[' + json + ']';//Return numerical JSON
    return '{' + json + '}';//Return associative JSON
}

});
/*******************************************************************************************/
})(jQuery); // confine scope
function popup(text){
	jQuery('body').append(
		'<div class="popup">'+
			'<div class="xxx">'+
			'</div>'+
			'<div class="window">'+
				'<span class="xxx">x</span>'+
				'<form method="POST" enctype="multipart/form-data">'+
					text+
					'<input class="btn btn-primary" type="submit" value="Save" name="formsubmit-edit">'+
				'</form>'+
			'</div>'+
		'</div>'
	);
	
	$('html, body').animate({scrollTop:0}, 500);
	
	jQuery('.xxx').click(function(){
		jQuery('.popup').remove();
	});
}

function popupJS(text, func){
    $('#myModal').modal('show');
    console.log(text);
        $('#myModal_body').html(
                '<form method="POST" enctype="multipart/form-data">'+
                    text+
                '<button id="myModal_save" type="button" class="button" data-dismiss="modal" type="submit" name="formsubmit-edit" >Сохранить</button>\
                </form>'
            );

    
    $('#myModal_save').click(function() {
        $('#myModal').modal('hide');
        func($('#myModal_body FORM').serializeArray());
        $('#myModal_body FORM').remove();
    });
}