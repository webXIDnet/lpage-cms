OpenPopUp=1;
history = [];
history[0]='';
history[1]='';

history_start = '';

;(function($){ // secure $ jQuery alias
/**
 * @version 1.0 14/06/2011
 * @author webXID webxid.22web.net
 * @copyright Copyright (C) 2011
 *
 */
$(function(){
/* ---------------------- */

$(document).ready(function(){
	clearHTMLforHistory();
	history_start = $('[gp="GP_temp"]').html();
	
	
	
	$('body').append(
		'<ul gp="ToolsBar">'+
			'<li gp="SCRsteps"><a gp="BackToStart" class="ScrSt_active" title="Вернуть к первоначальному варианту"></a><!--a gp="BackMove" class="ScrSt_active" title="Вернуть"></a><a gp="ForwMove" title="Вперед"></a--></li>'+
			'<li><a gp="SaveAllHTML">Сохранить</a></li>'+		
		'</ul>'
	);
	
	Run();
	
	BackMove();
	$('[gp="Block"],[gp="Element"]').click(function(){
		BackMove();
	});
	
});
/* -----------//----------- */
function Run($type){
		
	$('a').click(function(e){e.preventDefault();});
	
	MoveTabs('element');
	NavElement();
	NavBlock();
	
	if($type!='hover'){
		HoverBlock();
		HoverElement();
	
			BackMove();
	}
	
	$('[gp="SaveAllHTML"]').click(function(){
		
		if($(this).attr('step')!=1){
			
			$(this).attr('step','1');
			
			
			$this = $(this);
			
			$($this).css({
				background:'url('+HOMEPAGE+'media/_admin/waiting.gif) no-repeat center top',
				width:'91px',
				height:'30px',
				display:'inline-block',
				padding:'0',
				textIndent:'-9999px'
			});
			
			$tpl = $text = '';
			
			$window_scroll = $(window).scrollTop();
			
			$('html, body').animate({scrollTop:0}, 0).ready(function(){
				
				clearHTMLforHistory();
				$('[gp="GP_temp"] [gp="ToolsBar"]').remove();
				
				$tpl =  $('[gp="GP_temp"]').html();//отримується код сторінки Source
				$i=0;
				$t=0;
			
				$('[gp="GP_temp"] *').removeAttr('gp').removeAttr('gptype').removeAttr('gpedit').ready(function(){
					
					$text = $('[gp="GP_temp"]').html(); //отримується код сторінки Public
					
					$.post(HOMEPAGE+'ajax/?SaveTPL='+LP_ID,
						{
							tpl:$tpl//,
							//text:$text
						},
						function(data) {
							$msg = data; 
							$.post(HOMEPAGE+'ajax/?SaveTPL='+LP_ID,
								{
									//tpl:$tpl,
									text:$text
								},
								function(data) {
														
									popup($msg+'<br/>'+data,$window_scroll+50,300);
									$($this).removeAttr('style').attr('step','0');;
									$('html, body').animate({scrollTop:$window_scroll}, 500);
									Run();
								}
							).error(function(data){
								popup($msg+'<br/>'+'Ошибка отправки Кода страницы',$window_scroll+50,300);
								$($this).removeAttr('style').attr('step','0');;
								$('html, body').animate({scrollTop:$window_scroll}, 500);
							});
						//	popup(data,50,300);
						//	$($this).removeAttr('style').attr('step','0');;
						//	$('html, body').animate({scrollTop:0}, 500);
						//	Run();
						}
					).error(function(data){
						popup('Ошибка отправки Кода шаблона',$window_scroll+50,300);
						$($this).removeAttr('style').attr('step','0');;
						$('html, body').animate({$window_scroll:0}, 500);
					});
					
				});
			});
		}
	});
	
}
/* -----------//----------- */
function BackMove(){
		
	clearHTMLforHistory();
	
	body = $('[gp="GP_temp"]').html();
	
	$('[gp="BackToStart"].ScrSt_active').click(function(){
		
		$step = $(this).attr('step')*1;
		if($step==1){
		
			$(this).attr('step','0');
			
			$('body').html(history_start+'<ul gp="ToolsBar">'+
				'<li gp="SCRsteps"><a gp="BackToStart" class="ScrSt_active" title="Вернуть к первоначальному варианту"></a><!--a gp="BackMove" class="ScrSt_active" title="Вернуть"></a><a gp="ForwMove" title="Вперед"></a--></li>'+
				'<li><a gp="SaveAllHTML">Сохранить</a></li>'+			
			'</ul>').ready(function(){
				
				history[0]='';
				history[1]='';
				Run();
				
			});
			
	

		}		
	});	
	
	
	if(history[0]=='' || history[0]=='undefined'){
		
		history[0] = body;
		$('[gp="SCRsteps"] [gp="BackToStart"]').addClass('ScrSt_active').attr('step','1');
		
	}else if(history[0]!=body && (history[1]=='' || history[1]=='undefined')){
		
		history[1] = body;
		$('[gp="SCRsteps"] [gp="BackToStart"]').addClass('ScrSt_active').attr('step','1');
		
	}else if(history[1]!='' && history[1]!='undefined' && history[1]!=body){
		
		history[0] = history[1];
		history[1] = body;
		$('[gp="SCRsteps"] [gp="BackToStart"]').addClass('ScrSt_active').attr('step','1');		
		
	}
	/*
	$('[gp="BackMove"].ScrSt_active').click(function(){
		
		$step = $(this).attr('step')*1;
		if($step==1){
		
			$(this).attr('step','0');

			
			$('body').html(history[0]+'<ul gp="ToolsBar">'+
				'<li gp="SCRsteps"><a gp="BackMove" class="ScrSt_active" title="Вернуть"></a><!--a gp="ForwMove" title="Вперед"></a--></li>'+
				'<li><a gp="SaveAllHTML">Сохранить</a></li>'+		
			'</ul>').ready(function(){
				body = history[1];
				history[1] = history[0];
				history[0] = body;
				Run();
			});
			
	

		}		
	});	
	*/
}
/* -----------//----------- */
function clearHTMLforHistory($clear_all){
		
	b = '';
	
	$('[gp="GP_temp"]').remove();
		
	$('body').append('<div gp="GP_temp">'+$('body').html()+'</div>').ready(function(){
		
		$('[gp="BlCol"], [gp="Element"]').css({zIndex:'9'});
		
	}).ready(function(){
	
		$('.ui-sortable-placeholder, #rh-snippet-end, #rh-snippet, #fancybox-tmp, #fancybox-loading, #fancybox-overlay, #fancybox-wrap, #lightboxOverlay, #lightbox').remove();
		
		$('[gp="GP_temp"] [gp="ToolsBar"], [gp="GP_temp"] [gp="PopUp"], [gp="GP_temp"] [gp="ElNav"], [gp="GP_temp"] [gp="BlNav"], [gp="GP_temp"] .cke_dialog_background_cover, [gp="GP_temp"] .cke_reset_all, [gp="GP_temp"] .colorpicker').remove().ready(function(){
			
			$('[id="undefined"]').removeAttr('id');
			$('[gp="GP_temp"] *').removeClass('undefined').removeClass('gpHover').removeClass('ui-sortable').removeAttr('step').ready(function(){
				if($clear_all=='all'){
					$('[gp="GP_temp"] *').removeAttr('gp').removeAttr('gptype').removeAttr('gpedit');
				}
										
				return $('[gp="GP_temp"]').html();
				
			});
		});
	});
	
}
/* -----------//----------- */
function MoveTabs($type,$this){
	
//	if(!$this)
//		$this = '[gp="BlCol"]';
		
	switch($type){
		case'block':
			$('[gp="Block"]').parent().sortable({
				connectWith: '[gp="Block"]',
			    handle: '[gp="BlNav"]'	
			});
			
		break;
		case'element':
			$('[gp="Element"]').parent().sortable({
				//connectWith: '[gp="BlCol"]',				
				handle: '[gp="body"]'
			});
		break;
	}
}
/* -----------//----------- */
function retFalse($this){
	$($this).click(function(e){
		e.preventDefault();
		return false;
	});
}
/* -----------//----------- */
function scrollToPopUp(){$top = $(window).scrollTop()+0;return false}
/* -----------//----------- */
function HoverElement(){
	$('[gp="Element"]').hover(
		function(){
			$(this).addClass('gpHover');
			
			del = '<a href="#delete" gp="elAct">Х</a> ';
			
			if($(this).parent().children('[gp="Element"]').length==1){
				del = '';
			}
			
			$(this).append(
				'<div gp="ElNav">'+
					'<a href="#add" gp="elAct" title="Добавить новый Элемент">+</a> '+
					'<a href="#copy" gp="elAct" title="Копировать">C</a> '+
					del+
				'</div>'
			);
			Run('hover');
		},
		function(){
			$('[gp="ElNav"]').remove();
			$(this).removeClass('gpHover');
			$(this).css('z-index','1');
		}
	);
}
/* -----------//----------- */
function HoverBlock(){
	HoverSteps('[gp="Block"]');
	$('[gp="Block"]').hover(
		function(){
			
			$(this).addClass('gpHover');
			
			if($(this).attr('step')!='2'){
				
							
				del = '<a href="#delete" gp="blAct" title="Удалить">X</a> ';
				
				t = $(this).attr('gptype');
				if(t!='' && t!='undefined' && $('[gp="Block"][gptype="'+t+'"]').length<=2){
					del = '';
				}
				
				edit = '<a href="#edit" gp="blAct" title="Редактировать">E</a> ';
				if($(this).attr('gpedit')=='false'){
					edit = '';
				}
				
				if($(this).attr('gp')!='BlHidden'){
					$(this).append(
						'<div gp="BlNav">'+
							'<a href="#add" gp="blAct" title="Добавить блок ниже">+</a> '+
							edit +
							'<a href="#move" gp="blAct" title="Переместить блок">M</a> '+
							'<a href="#copy" gp="blAct" title="Клонировать">C</a> '+
							'<a href="#hide" gp="blAct" title="Спрятать">H</a> '+
							del+
						'</div>'
					);
					MoveTabs('block');
				}
			}
			
			$(this).attr('step','2');
			//$(this).css('z-index','10');
			Run('hover');			
		},
		function(){
			//$(this).css('z-index','9');
			$('[gp="BlNav"]').remove();
			$(this).removeClass('gpHover');
		}
	);
}
/* -----------//----------- */
function NavElement(){
	
	$('[gp="Element"] [gp="body"]').click(
		function(){
			if($(this).parent().attr('step')!='2')
				AddElement($(this).parents('[gp="Element"]'),'edit');
			$(this).parent().attr('step','2');
		}
	);
	$('[gp="Element"] [gp="body"]').mousemove(
		function(){
			$(this).parent().attr('step','1');
		}
	);
	
	$('[href="#add"][gp="elAct"]').click(
		function(){
			if($(this).attr('step')!='2')
				AddElement($(this).parents('[gp="Element"]'),'add');
			$(this).attr('step','2');
		}
	);
		
	ActionSteps('[href="#add"][gp="elAct"]');
	ActionSteps('[href="#copy"][gp="elAct"]');
	
	$('[href="#copy"][gp="elAct"]').click(
		function(){
			if($(this).attr('step')!='2')
				CopyElement($(this).parents('[gp="Element"]'));
			$(this).attr('step','2');
			
		}
	);
	$('[href="#delete"][gp="elAct"]').click(
		function(){
			$parent = $(this).parents('[gp="BlCol"]');
			numb = $(this).parents('[gp="BlCol"]').children('[gp="Element"]').length;
			
			$(this).parents('[gp="Element"]').remove();
		/*	if(numb==1)$($parent).remove();*/
		}
	);
}
/* -----------//----------- */
function NavBlock(){
/* ________ ADD _________*/
	$('[href="#add"][gp="blAct"]').click(
		function(){
			AddBlock($('.gpHover'));
		}
	);
/* ________ //ADD _________*/
/* ________ EDIT _________*/
	$('[href="#edit"][gp="blAct"]').click(
		function(){
			if($('.gpHover').attr('step')!='2')
				AddBlock($('.gpHover'),'edit');
				
			$('.gpHover').attr('step','2');
			
		}
	);
	$('[href="#hide"][gp="blAct"]').click(
		function(){
			$('[gp="BlNav"]').remove();
			$('.gpHover').append(
				'<div gp="hidden"></div>'
			).attr(
				'gp','BlHidden'
			).animate({
				height:'0px'
			},500);
			
			$('[gp="BlHidden"]').hover(
				function(){
					$('[gp="BlHidden"]:hover').append(
						'<div gp="BlNav">'+
							'<a href="#move" gp="blAct" title="Переместить блок">M</a> '+
							'<a href="#show" gp="blAct" title="Раскрыть">S</a> '+
						'</div>'
					);
					$('[href="#show"][gp="blAct"]').click(function(){
						
						$('.gpHover').attr('gp','Block').css({height:'auto'});
						$('[gp="BlNav"]').remove();
						$('[gp="hidden"]').remove();
					});
				},
				function(){
					$('[gp="BlNav"]').remove();
				}
			);
		}
	);
	
	ActionSteps('[href="#edit"][gp="blAct"]');
	
/* ________ //EDIT _________*/
/* ________ CLONE _________*/
	$('[href="#copy"][gp="blAct"]').click(
		function(){
			if($('.gpHover').attr('step')!='2')
				CopyBlock($('.gpHover'));
			$('.gpHover').attr('step','2');
		}
	);
	
	ActionSteps('[href="#copy"][gp="blAct"]');
/* ________ //CLONE _________*/
/* ________ DELETE _________*/
	$('[href="#delete"][gp="blAct"]').click(
		function(){
			numb = $('[gp="Block"]').length;
			
			if(numb==1){
				$('.gpHover').replaceWith('<div id="DefaultBlock" gp="Block" class="block"><div>Для начала, добавъте новый блок</div></div>');
				Run();
			}else{
				$('.gpHover').remove();
			}
		}
	);
/* ________ //DELETE _________*/

}
/* -----------//----------- */
function ActionSteps($this){
	
	$($this).mousedown(
		function(){
			$($this).attr('step','1');
			
		}
	);
	
	$($this).mouseout(
		function(){
			$($this).removeAttr('step');
		}
	);
}
/* -----------//----------- */
function HoverSteps($this){
	$($this).mousemove(
		function(){
			$('.gpHover').attr('step','1');
		}
	);
	
	$($this).mouseout(
		function(){
			$('.gpHover').removeAttr('step');
		}
	);
}
/* -----------//----------- */
function CopyBlock($this){
	html = checkAttr($($this).html());
	id = checkAttr($($this).attr('id'));
	cl = checkAttr($($this).attr('class'));
	tp = checkAttr($($this).attr('gptype'));
	ed = checkAttr($($this).attr('gpedit'));
	$($this).after('<div gp="Block" gptype="'+tp+'" gpedit="'+ed+'" id="'+id+'" class="'+cl+'">'+html+'</div>');
	$('.gpHover').removeClass('gpHover');
	$('[gp="BlNav"]').remove();
	
	Run();
	BackMove();
}
/* -----------//----------- */
function CopyElement($this){
	html = checkAttr($($this).html());
	id = checkAttr($($this).attr('id'));
	cl = checkAttr($($this).attr('class'));
	tp = checkAttr($($this).attr('gptype'));
	$($this).after('<div gp="Element" gptype="'+tp+'" id="'+id+'" class="'+cl+'">'+html+'</div>');
	MoveTabs('element');
	
	Run();
	BackMove();
}
/* -----------//----------- */
function AddBlock($this,$type){
	
	$($this).stop();
		
	if($('[gp="PopUp"]').length)return;
	
	$this = $($this).addClass('gpEdition');
	
	numb_col = $($this).children().children('[gp="BlCol"]').length;
	cont = gl_title = gl_bg = gl_textcolor = '';
	ar_html = [];
	
	if(numb_col>=1 && $type=='edit'){
		gl_bg = $($this).css('background-color');
		gl_textcolor = $($this).css('color');
		
		if(gl_bg=='rgba(0, 0, 0, 0)') gl_bg = '';
		else gl_bg = rgb2hex(gl_bg);
		
		if(gl_textcolor=='rgba(0, 0, 0, 0)') gl_textcolor = '';
		else gl_textcolor = rgb2hex(gl_textcolor);
		
		i=1;
		
		ar_width = [];
		ar_bg = [];
		
		b_wid = $($this).children().width(); 
		
		$($this).children().children('[gp="BlCol"]').each(function(){
			
			w = $(this).width();
			
			ar_width[i] = (w/b_wid)*100 
			ar_html[i] = $(this).html();
			
			i++;
		});
		
		
		if(!ar_width[i]){
			ar_width[i]='';
		}else{
			if(ar_width[i].replace('%', '')!=ar_width[i])
				ar_width[i] = ar_width[i].replace('%', '');
		}
		
		for(i=1;i<=numb_col;i++){
			
			
			
			cont += 
			'<div class="gpBox"><h3 gp="SubTitle">Колонка '+i+'</h3>'+
				'<button gp="RemoveColumn" class="gpXXX">x</button>'+
				'<div class="gpCol gpCol-2"><b>Ширина колонки (%)</b><br/>'+
					'<input id="amount'+i+'" name="width-'+i+'" value="'+ar_width[i]+'" type="text" />'+
					'<div id="slider-range-max'+i+'" name="'+i+'"></div>'+
				'</div>'+
			'</div>';
		}
	}
	
	$text =
	'<h3 gp="PopTitle">Параметры блока</h3>'+
		
	'<div id="tabs" gp="AddBlocks">'+
		'<ul>'+
		//	'<li><a href="#tabs-1">Слайдер</a></li>'+
		//	'<li><a href="#tabs-2">Галерея</a></li>'+
			'<li><a href="#tabs-3">Колонки</a></li>'+
			'<li><a href="#tabs-4">Настройки блока</a></li>'+
		'</ul>'+
	 /* 	'<div id="tabs-1">'+
	       	'<p>Слайдер'+
			'</p>'+
	  	'</div>'+
	  	'<div id="tabs-2">'+
	       	'<p>Галерея'+
			'</p>'+
	  	'</div>'+ */
		'<form>'+
		  	'<div id="tabs-3">'+
				cont+
				'<button gp="AddColumn" class="gpAdd">+ Добавить колонку</button>'+
				'<button class="gpSave">Сохранить</button>'+
		  	'</div>'+
	  	'</form>'+
	  	'<form id="imageform" method="post" enctype="multipart/form-data" action="http://kudypity.loc/ajaximage.php">'+
		  	'<div id="tabs-4">'+
		  		'<div class="gpCol gpCol-2">'+
					'<h4 style="margin-bottom: 5px;">Код цвета текста</h4>'+
					'<input id="gpBlockColor" class="gpBlockColor" name="gpBlockColor" placeholder="Код фонового текста" value="'+gl_textcolor+'"/>'+
				'</div>'+
				
		  		'<div class="gpCol gpCol-2">'+
			  		'<h4 style="margin-bottom: 5px;">Код фонового цвета</h4>'+
					'<input id="gpBlockBgColor" class="gpBlockBgColor" name="gpBlockBgColor" placeholder="Код фонового цвета" value="'+gl_bg+'"/>'+
				'</div>'+
				
				'<h4 style="margin-bottom: 5px;">Загрузите фоновое изображение</h4>'+
				'<div id="preview"></div>'+
				'<div id="imageloadstatus" style="display:none">'+
					'<img src="'+HOMEPAGE+'media/_admin/loader.gif" alt="Uploading...."/>'+
				'</div>'+
				'<div id="imageloadbutton">'+
					'<input type="file" name="photoimg" id="photoimg" /> '+
					'<button name="gpBlockBgDel" style="margin-left: 20px;">Удалить фон</button>'+
				'</div>'+
				'<br/>'+
				'<div class="gpCol gpCol-2">'+
					'<h4 style="margin-bottom: 5px;">Заливка</h4>'+
					'<select class="gpBlockBgRepeat" name="gpBlockBgRepeat">'+
						'<option value="repeat repeat">залить все</option>'+
						'<option value="repeat no-repeat">залить по горизонтали</option>'+
						'<option value="no-repeat repeat">залить по вертикаль</option>'+
						'<option value="no-repeat no-repeat">не заливать</option>'+
					'</select>'+
				'</div>'+
				
				'<div class="gpCol gpCol-2">'+
					'<h4 style="margin-bottom: 5px;">Позиция фонового изображения</h4>'+
					'<input type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="left top"/> '+
					'<input checked="checked"  type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="center top"/> '+
					'<input type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="right top"/>'+
					'</br>'+
					'<input type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="left center"/> '+
					'<input type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="center center"/> '+
					'<input type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="right center"/>'+
					'</br>'+
					'<input type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="left bottom"/> '+
					'<input type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="center bottom"/> '+
					'<input type="radio" class="gpBlockBgPos" name="gpBlockBgPos" value="right bottom"/>'+
				'</div>'+
		  	'</div>'+
		'</form>'+ 
	'</div>';
	
	popup($text,$(window).scrollTop()+50,'500');
	
	$('#gpBlockBgColor, #gpBlockColor').ColorPicker({/*палітра вибору кольору*/
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		}
	}).bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});
	
	if(numb_col>=1 && $type=='edit'){
		for(i=1;i<=numb_col;i++){/*повзунок вибору ширини колонки*/
	
			 $( "#slider-range-max"+i ).slider({
				range: "max",
				min: 0,
				max: 100,
				step: 5,
				value: $( "#amount"+i ).val(),
				slide: function( event, ui ) {
					$("#amount"+$(this).attr('name')).val(ui.value);
				}
		    });
		    $( "#amount"+i ).val( $( "#slider-range-max"+i ).slider( "value" ));
		    $( "#amount"+i).keyup(function(){
		    	n = $(this).attr('id').replace('amount','');
		    	
		    	$( "#slider-range-max"+n).slider({
					value: $( "#amount"+n).val()
		    	});
		    });
		    
		}
	}
	
	$('[name="gpBlockBgDel"]').click(function(){/*видаляє налаштування фону*/
		$($this).css({
			backgroundColor:'currentcolor',
			backgroundImage:'ivory',
			backgroundRepeat:'inherit',
			backgroundPosition:'inherit',
			color:'initial'});
		$('input.gpBlockBgColor[name="gpBlockBgColor"], input.gpBlockColor[name="gpBlockColor"]').val('');
	});
	
	$('[gp="PopUp"]').hover(function(){/*отримання кольору фону*/
		$($this).css({backgroundColor:'#'+$('input.gpBlockBgColor[name="gpBlockBgColor"]').val()});
		$($this).css({color:'#'+$('input.gpBlockColor[name="gpBlockColor"]').val()});
	});
	$('.gpBlockBgPos[name="gpBlockBgPos"]').click(function(){/*позиціювання фонового зображення*/
		$($this).css({backgroundPosition:$(this).val()});
	});
	$('.gpBlockBgRepeat[name="gpBlockBgRepeat"]').change(function(){/*заливка фонового зображення*/
		$($this).css('background-repeat',$(this).val());
	});	
	$('#photoimg').die('click').live('change',function(){/*завантаження фонового зображення*/ 
				    
		$("#imageform").ajaxForm({ 
			beforeSubmit:function(a,f,o){
				o.dataType = "html";
				$("#imageloadstatus").show();
				$("#imageloadbutton").hide();
			}, 
			success:function(data){
				
				$pos = 'center top';
				$bgcolor = checkAttr($('.gpBlockBgColor[name="gpBlockBgColor"]').val());
				$color = checkAttr($('.gpBlockColor[name="gpBlockColor"]').val());
				$repeat = checkAttr($('.gpBlockBgRepeat[name="gpBlockBgRepeat"]').val());
				
				$('.gpBlockBgPos[name="gpBlockBgPos"]').each(function(){
					
					if($(this).attr('checked')=='checked'){
						$pos = $(this).val();
					}
					
				});
				$($this).css({
					color:'#'+$color,
					backgroundImage:'url('+data+')',
					backgroundPosition:$pos,
					backgroundColor:'#'+$bgcolor,
					backgroundRepeat:$repeat
				});
				
				$("#imageloadstatus").hide();
				$("#imageloadbutton").show();
			}, 
			error:function(e){ 
				$("#imageloadstatus").hide();
				$("#imageloadbutton").show();
			} 
		}).submit();
	});
	
	$('[gp="Window"] .gpXXX').click(function(){/* закривае попап вікно*/
		$(this).parent().remove();
	});
	
	$('[gp="AddBlocks"]').tabs(); /*перемикач закладок*/
	
	$('[gp="AddColumn"]').click(function(){/*додає нові колонки*/
		numb = $('[gp="AddBlocks"] .gpBox').length+1;
				
		$('[gp="AddColumn"]').before(
			'<div class="gpBox"><h3 gp="SubTitle">Колонка '+numb+'</h3>'+
				'<button gp="RemoveColumn" class="gpXXX">x</button>'+
				'<div class="gpCol gpCol-2"><b>Ширина колонки (%)</b><br/>'+					
					'<input id="amount'+numb+'" name="width-'+numb+'" value="100" type="text" />'+
					'<div id="slider-range-max'+numb+'"></div>'+
					
				'</div>'+
				
				
				
			'</div>'
		);
		$('[gp="Window"] .gpXXX').click(function(){
			$(this).parent().remove();
		});
		
		
		$( "#slider-range-max"+numb ).slider({/*повзунок вибору ширини колонки*/
			range: "max",
			min: 0,
			max: 100,
			step: 5,
			value: $( "#amount"+numb ).val(),
			slide: function( event, ui ) {
				$("#amount"+numb).val(ui.value);
			}
	    });
	    $( "#amount"+numb ).val( $( "#slider-range-max"+numb ).slider( "value" ));
	    
	    $( "#amount"+numb ).keyup(function(){
	    	$( "#slider-range-max"+numb ).slider({
				value: $( "#amount"+numb ).val()
	    	});
	    });
		
	});
	
	$('.gpSave').click(function(){
		
		numb = $('[gp="AddBlocks"] .gpBox').length;
		
		if(numb>0){
		
			$text = '';
			n = 1;
			BlCol_class = [];
			BlCol_id = [];
			
			$($this).children().children('[gp="BlCol"]').each(function(){
				BlCol_class[n] = checkAttr($(this).attr('class'));
				BlCol_id[n] = checkAttr($(this).attr('id'));
				n++;
			});
						
			for(i=1;i<=numb;i++){
				
				width = bg = ''; 
				
				width = checkAttr($('input[name="width-'+i+'"]', this.form).val());
				bg = checkAttr($('input[name="bg-'+i+'"]', this.form).val());
				
				style = '';
				
				if(width!=''){
					if(width.replace('px', '')!=width || width.replace('%', '')!=width){
						style += 'width:'+width+';';
					}else{
						style += 'width:'+width+'%;';
					}
				}
				
				if($type=='edit' && ar_html[i]){
					$text +=                                                
						'<div gp="BlCol" id="'+checkAttr(BlCol_id[i])+'" class="'+checkAttr(BlCol_class[i])+'" style="'+style+'">'+
							ar_html[i]+
						'</div>';
				}else{
					$text += 
						'<div gp="BlCol" id="'+checkAttr(BlCol_id[i])+'" class="'+checkAttr(BlCol_class[i])+'" style="'+style+'">'+
							'<div id="DefaultElement" gp="Element">'+
								'<div gp="body">Добавить Элемент'+
								'</div>'+
							'</div>'+
						'</div>';
				}
			}
			
			Bl_class = checkAttr($($this).attr('class'));
			Bl_id = checkAttr($($this).attr('id'));
			Bl_type = checkAttr($($this).attr('gptype'));
			
			wap_class = checkAttr($($this).children('div:first-child').attr('class'));
			wap_id = checkAttr($($this).children('div:first-child').attr('id'));
			
			if($type=='edit'){
				$($this).replaceWith(
					'<div gp="Block" gptype="'+Bl_type+'" id="'+Bl_id+'" class="'+Bl_class+'">'+
						'<div id="'+wap_id+'" class="'+wap_class+'">'+
							$text+
						'</div>'+
					'</div>'
				);
			}else{
				$($this).after(
					'<div gp="Block" gptype="'+Bl_type+'" id="'+Bl_id+'" class="'+Bl_class+'">'+
						'<div id="'+wap_id+'" class="'+wap_class+'">'+
							$text+
						'</div>'+
					'</div>'
				);
			}
			
			MoveTabs('element');
			
			$('[gp="Element"] [gp="body"]').click(
				function(){
					AddElement($(this).parent(),'edit');
				}
			);
		
			if($($this).attr('id')=='DefaultBlock'){
				$($this).remove();
			}else{
				$('.gpEdition').removeClass('gpEdition');
			}
			
			Run('edit');
		
		}
		$('[gp="PopUp"]').remove();
		
	});
	
	retFalse('button');
//	retFalse('#wrapper a');
	return false;
}
/* -----------/----------- */
/* -----------//----------- */
function AddElement($this,$type){
	$($this).stop();
	if($('[gp="PopUp"]').length)return;
	OpenPopUp++;
	
	$($this).addClass('gpEdition');
	html = '';
	$el_type = checkAttr($($this).attr('gptype'));
	
	switch($el_type){
		case'form':
			$el_type = 1
		break;
		case'html':
			$el_type = 2;
		break;
		default:
			$el_type = 0;
	}
	
	
	if($type=='edit')
		html = $($this).children('[gp="body"]').html();
	
	
	$text =
	//'<h3 gp="PopTitle">Выберете тип нового блока</h3>'+
	'<div id="tabs" gp="EditBlocks">'+
		'<ul>'+
			'<li><a href="#tabs-1">Текстовое поле</a></li>'+
			'<li style="display:none;"><a href="#tabs-2">Форма</a></li>'+
/*			'<li><a href="#tabs-3">HTML-поле</a></li>'+*/
		'</ul>'+
	'<form>'+
	  	'<div id="tabs-1" aria-hidden="true" aria-hidden="true">'+
	       	
				'<textarea name="text" id="ckeditor'+OpenPopUp+'" rows="10">'+html+'</textarea>'+
			
			'<a name="text" class="gpSave" >Сохранить</a>'+
	  	'</div>'+
	  	
	'</form>'+
	
	'<form>'+
	  	'<div id="tabs-2" aria-expanded="true" aria-hidden="false">'+
	       	'<textarea name="text" id="ckeditor_form'+OpenPopUp+'" rows="10">'+html+'</textarea>'+
	       	'<a name="form" class="gpSave">Сохранить</a>'+
	  	'</div>'+
	'</form>'+
	
/*	'<form>'+
	  	'<div id="tabs-3">'+
			
			'<textarea name="text" id="CodeMirror" style="width:80%" >'+html+'</textarea>'+
			
			'<a name="html" class="gpSave">Сохранить</a>'+
	  	'</div>'+
	  		
	'</form>'+
*/	
	'</div>';
	
	popup($text,$(window).scrollTop()+50,$('[gp="Block"]>div:first-child').css('width'));
	
	var CKeditor = [];
	
	//CKEDITOR.disableAutoInline = true;
	
//	CKeditor['text'] = CKEDITOR.inline( $($this).attr('id') );
	
	CKeditor['text'] = CKEDITOR.replace('ckeditor'+OpenPopUp+'',{toolbar:'Text'});
			
	CKeditor['form'] = CKEDITOR.replace('ckeditor_form'+OpenPopUp+'',{toolbar:'Form'});		

/*	var editor = CodeMirror.fromTextArea(document.getElementById("CodeMirror"), {
        lineNumbers: true,
        extraKeys: {"Ctrl-Space": "autocomplete"}
      });
*/
	$('[gp="EditBlocks"]').tabs({active:$el_type});
	

	$('.gpSave').click(function(){
		
		$text = '';
		$el_type = checkAttr($(this).attr('name'));
		
		switch($el_type){
			case'text':
				if(!CKeditor['text'])
					$text = $('textarea', this.form).val();
				else
					$text = CKeditor['text'].getData(); 
			break;
			case'form':
				if(!CKeditor['form'])
					$text = $('textarea', this.form).val();
				else
					$text = CKeditor['form'].getData();
			break;
			case'html':
				$text = $('textarea', this.form).val();
			break;
			default: 
				$text ='ERROR "invalid element type"';
				$el_type = ''; 	
		}
		
		
		BoCol_class = $($this).children('[gp="body"]').attr('class');
		BoCol_id = $($this).children('[gp="body"]').attr('id');
		
		ElCol_class = $($this).attr('class');
		ElCol_id = $($this).attr('id');
		
		if($type=='edit'){
			$($this).attr('gptype',$el_type);
			$($this).children('[gp="body"]').replaceWith(
				'<div gp="body" id="'+BoCol_id+'" class="'+BoCol_class+'">'+
					$text+
				'</div>'
			);
			
		}else{
			$($this).after(
				'<div gp="Element" gptype="'+$el_type+'" id="'+ElCol_id+'" class="'+ElCol_class+'">'+
					'<div gp="body" id="'+BoCol_id+'" class="'+BoCol_class+'">'+
						$text+
					'</div>'+
				'</div>'
			);
		}
		MoveTabs('element');
		Run('edit');
		
		
		if($($this).attr('id')=='DefaultElement' && $type=='edit'){
			$($this).removeAttr('id');
		}		
		
		$('.gpEdition').removeClass('gpEdition');
		$('[gp="PopUp"],.colorpicker').replaceWith('');
		
	});
	
	retFalse('button');
//	retFalse('#wrapper a');
	return false;
}
/* -----------/----------- */
function popup($text,$top,$width){	
	
	$('body').append('<div gp="PopUp"><div gp="PPBg"></div>'+
		
		'<div style="margin-top:'+$top+'px;margin-bottom:50px;max-width:'+$width+';min-width:80%" gp="Window" >'+
			'<span class="xxx">x</span>'+
			$text+
		
		'</div>'+
		
	'</div>');
	
	$('[gp="PPBg"], [gp="Window"] .xxx').click(function(){
		$('.gpEdition').removeClass('gpEdition');
		$('[gp="PopUp"]').replaceWith('');
	});
	scrollToPopUp();
	
}
/* -----------//----------- */
function removeEditor() {
	if ( !editor )
		return;

	// Retrieve the editor contents. In an Ajax application, this data would be
	// sent to the server or used in any other way.
	document.getElementById( 'editorcontents' ).innerHTML = html = editor.getData();
	document.getElementById( 'contents' ).style.display = '';

	// Destroy the editor.
	editor.destroy();
	editor = null;
}
/* -----------//----------- */
function checkAttr($val){
	if($val && $val!='' && $val!='undefined')return $val;
	return '';
}
/* -----------//----------- */
function rgb2hex(rgb){
 rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
 return ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[3],10).toString(16)).slice(-2);
}

/* -----------//----------- */
});   
/*******************************************************************************************/
})(jQuery); // confine scope