$(document).ready(function() {
    var saverURL = window.location.href;
   //var saverURL = 'saver.php'; // Путь к php обработчику
                               // туда прилетит отредактированный html в ввиде $_POST['update']
                               // возвращать ничего не надо

   // режем в переменную код редактора
   var controlData = $('body').html();
   var from = controlData.search('<div start-control="true"></div>'); 
   var to = controlData.search('<div end-control="true"></div>');
   controlData = controlData.substring(from,to);

   // режем в переменную <body> контент
   var content  = $('body').html();
   var from = content; 
   var to = content.search('<div start-control="true"></div>');
   content = content.substring(from,to);

   // Пихаем код страницы в редактор
   $('#code-editor').val(content);

   // Добавляем возможность менять инфу
   $('body').attr('contenteditable', 'true');

   // возвращает исходный вариант при нажатии кнопки "отмена"
   $('#go-back').click(function(event) {
      event.preventDefault()
      location.reload();
   });

   // вызываем редактор кода
   var codeHolder = false;

   $('#show-code').on('shown.bs.modal', function (e) {

      if (codeHolder == false) {        
         var editor = CodeMirror.fromTextArea(document.getElementById("code-editor"), {
            lineNumbers: true,
            mode: "text/html"
         });

         codeHolder = true;
      }

      // Сохраняем код (html)
      $('#editor-save').click(function(event) {
        var newData = editor.getValue();
        
        swal({
          title: "Сохранить изменения?",   
          type: "warning",   
          showCancelButton: true,
          cancelButtonText: "Нет",
          confirmButtonText: "Да",
          confirmButtonColor: "#A5DC86",
          closeOnConfirm: false,   
          showLoaderOnConfirm: true, }, 
          function(){
            $.post(saverURL, {update: newData}, function(data){});   
            setTimeout(function(){
              swal({
                title: "Успешно!",  
                type: "success",   
                showCancelButton: false,   
                confirmButtonColor: "#86CCEB",   
                confirmButtonText: "Закрыть", 
                closeOnConfirm: false,   
                closeOnCancel: false }, 
                function(isConfirm){location.reload();});  
            }, 2000); 
          });
        
        $('.sweet-alert').attr('contenteditable', 'false');

      });

   });

   // Сохраняем код (visual)
   $('#save').click(function(event) {
      event.preventDefault();

      var update = $('body').html();
      var from = update.search('<'); 
      var to = update.search('<div start-control="true"></div>');
      update = update.substring(from,to);

      swal({
        title: "Сохранить изменения?",   
        type: "warning",   
        showCancelButton: true,
        cancelButtonText: "Нет",
        confirmButtonText: "Да",
        confirmButtonColor: "#A5DC86",
        closeOnConfirm: false,   
        showLoaderOnConfirm: true, }, 
        function(){
          $.post(saverURL, {update: update}, function(data){});   
          setTimeout(function(){
            swal({
              title: "Успешно!",   
              text: "Данные успешно сохранены",   
              type: "success",   
              showCancelButton: false,   
              confirmButtonColor: "#86CCEB",   
              confirmButtonText: "Закрыть", 
              closeOnConfirm: false,   
              closeOnCancel: false }, 
              function(isConfirm){location.reload();});  
          }, 2000); 
        });

      $('.sweet-alert').attr('contenteditable', 'false');

   });

   // hide me
   $('#hide-me').html('<i class="fa fa-caret-left"></i>');
   $('#hide-me').click(function(event) {
      event.preventDefault();
      var target = $(this);

      if(!target.hasClass('is-hidden')) {
         target.addClass('is-hidden');
         $('#edit-panel').animate({'left' : '-180px'}, 400);
         target.animate({'left' : '0px'}, 400);
         target.html('<i class="fa fa-caret-right"></i>');
         target.attr('title', 'Развернуть');
      } else {
         target.removeClass('is-hidden');
         $('#edit-panel').animate({'left' : '0px'}, 400);
         target.animate({'left' : '180px'}, 400);
         target.html('<i class="fa fa-caret-left"></i>');
         target.attr('title', 'Свернуть');
      }
   });

});

// file manager
function openKCFinder(field) {
    window.KCFinder = {
        callBack: function(url) {
            window.KCFinder = null;

            $('#file-url').html('');

            var url = url;

            $('#file-url').html(url);
            $('#file-url').fadeIn('400');
            fileControl();

            $("#url-copy").zclip({
               path:'js/ZeroClipboard.swf',
               copy:$('#file-url').text(),
               afterCopy:function(){}
            });
        }
    };
    window.open(HOMEPAGE+'plugins/kcfinder/browse.php?type=files&dir=files/public', 'kcfinder_textbox',
        'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +
        'resizable=1, scrollbars=0, width=800, height=600'
    );
}


// функции редактора
function fileControl() {
   var totalWidth = $('body').width();

   if (totalWidth > 750) {
      var checkWidth = $('#file-url').width();
      $('#file-control').css({
         left: 200 + checkWidth,
         top: '0px'
      });
      $('#file-url').css({
         left: '180px',
         top: '0px',
         display: 'block'
      });
      $('#file-control').fadeIn('400');
   } else {
      $('#file-control').css({
         left: '0',
         top: '90px',
         display: 'block'
      });
      $('#file-url').css({
         left: '0',
         top: '45px',
         display: 'block'
      });
   }
   
}

function closeControl() {
   $('#file-control').fadeOut('400');
   $('#file-url').fadeOut('400');
}