// get template function  
var tmpl = $.summernote.renderer.getTemplate();

// add a button   
$.summernote.addPlugin({
    buttons : {
       // "save"  is button's namespace.      
       "save" : function(lang, options) {
           // make icon button by template function          
           return tmpl.iconButton('fa fa-floppy-o', {
               // callback function name when button clicked 
               event : 'save',
               // set data-value property                 
               value : 'save', 
               title: 'Сохранить',               
               hide : false
           });           
       }

    }, 

    events : {
       "save" : function(event, editor, layoutInfo) {
         // Get current editable node
        var $editable = layoutInfo.editable();
        // var counter = 5;
        livesave($($editable).html(), $($editable).attr('data-id'));
        // console.log($($editable).html());
        // Call insertText with 'hello'
        // editor.insertText($editable, 'hello ');
       }
    }     
});